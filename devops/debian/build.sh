#!/bin/bash

set -e

NormalColor='\033[0;39m'
BlueColor='\033[1;94m'

print_cmd_info()
{
	echo -e ${BlueColor}$1${NormalColor}
}

SOURCE_FOLDER="/graftonite"
BUILD_FOLDER="/build"

# out=$(docker pull debian:latest)

GRAFTONITE_IMAGE="graftonite_debian"
# OR_IMAGE_PATH="/graftonite_debian.tar"
DOCKER_CONT="graftonite_debian_container"
IMAGE_PATH="/graftonite_debian.tar"
# DOCKER_IMAGE="dkfjsdklfsd$1"

echo $(basename $0)
print_cmd_info ${PWD}

# -----------------------
# PREPARE IMAGE
# -----------------------
if test -f ${IMAGE_PATH}
then
	print_cmd_info "Loading graftonite image from archive..."
	docker load < ${IMAGE_PATH}
else
	DEBIAN_IMAGE="debian:latest"
	print_cmd_info "Loading graftonite image from ${DEBIAN_IMAGE}..."
	docker pull ${DEBIAN_IMAGE}
	docker tag ${DEBIAN_IMAGE} ${GRAFTONITE_IMAGE}
fi
docker image ls -a

# DOCKER_IMAGE=$1
# if [[ -z "${DOCKER_IMAGE}" ]]
# then
# # 	GRAFTONITE_IMAGE="debian:10"
# 	print_cmd_info "Loading graftonite image..."
# 	docker pull debian:10
# 	docker tag debian:10 ${GRAFTONITE_IMAGE}
# else
# 	docker load < ${IMAGE_PATH}
# fi
# print_cmd_info "Debian image used: ${DOCKER_IMAGE}"

# if [[ ${DOCKER_IMAGE} = "^/*" ]];
#  then
# 	print_cmd_info "Loading graftonite image..."
# 	docker load < ${IMAGE_PATH}
# fi

print_cmd_info "Starting new container (${DOCKER_CONT}) from image (${GRAFTONITE_IMAGE})..."
docker run -dit --name ${DOCKER_CONT} ${GRAFTONITE_IMAGE}

print_cmd_info "Adding necessary packages..."
docker exec ${DOCKER_CONT} apt-get --yes -qq update
docker exec ${DOCKER_CONT} apt-get --yes -qq install gcc g++ cmake ccache clang clang-tidy qtbase5-dev

print_cmd_info "Saving container into ${GRAFTONITE_IMAGE} image..."
docker commit ${DOCKER_CONT} ${GRAFTONITE_IMAGE}
docker save ${GRAFTONITE_IMAGE} > ${IMAGE_PATH}

print_cmd_info "Stopping docker container & image..."
docker container stop ${DOCKER_CONT}
docker container rm ${DOCKER_CONT}
docker image rm ${GRAFTONITE_IMAGE}

print_cmd_info "Listing docker container & image..."
docker container ls -a
docker image ls -a

# -----------------------
# BUILD
# -----------------------

print_cmd_info "Loading graftonite image..."
docker load < ${IMAGE_PATH}
print_cmd_info "Starting container: ${DOCKER_CONT}..."
docker run -dit --name ${DOCKER_CONT} ${GRAFTONITE_IMAGE}

print_cmd_info "Creating sources folder..."
docker exec ${DOCKER_CONT} mkdir -p ${SOURCE_FOLDER}
print_cmd_info "Copying sources..."
docker cp . ${DOCKER_CONT}:${SOURCE_FOLDER}/


print_cmd_info "Creating make file..."
docker exec ${DOCKER_CONT} ls -l
docker exec ${DOCKER_CONT} ls -l ${SOURCE_FOLDER}
docker exec ${DOCKER_CONT} cmake -DENABLE_WERROR=ON -S ${SOURCE_FOLDER} -B ${BUILD_FOLDER}
print_cmd_info "Building..."
docker exec ${DOCKER_CONT} make -j4 -C ${BUILD_FOLDER} --no-print-directory
# # 	&& make install ${BUILD_FOLDER}

print_cmd_info "Saving container into ${GRAFTONITE_IMAGE} image..."
docker commit ${DOCKER_CONT} ${GRAFTONITE_IMAGE}
docker save ${GRAFTONITE_IMAGE} > ${IMAGE_PATH}


print_cmd_info "Checking file system..."
docker exec ${DOCKER_CONT} ls -l
docker exec ${DOCKER_CONT} ls -l ${BUILD_FOLDER}

print_cmd_info "Stopping docker container & image..."
docker container stop ${DOCKER_CONT}
docker container rm ${DOCKER_CONT}
docker image rm ${GRAFTONITE_IMAGE}



#
# docker exec ${DOCKER_CONT} ls -l
# # docker exec graphtonite_debian cd
# docker exec ${DOCKER_CONT} ls -l ${SOURCE_FOLDER}
# #
#
#
#
# # Build
# # docker exec graphtonite_debian rm -rf ${BUILD_FOLDER}
# docker exec ${DOCKER_CONT} cmake -DENABLE_WERROR=ON -S ${SOURCE_FOLDER} -B ${BUILD_FOLDER}
# docker exec ${DOCKER_CONT} make -j4 ${BUILD_FOLDER}/
# # 	&& make install ${BUILD_FOLDER}
#
# docker exec ${DOCKER_CONT} ls -l ${BUILD_FOLDER}
# #
# docker commit ${DOCKER_CONT} ${DOCKER_IMAGE}
# docker save ${DOCKER_IMAGE} > ${IMAGE_PATH}
#
# docker stop ${DOCKER_CONT}
# docker rm ${DOCKER_CONT}
# docker rm ${DOCKER_IMAGE}
#
# docker load < ${IMAGE_PATH}
# docker run -dit --name ${DOCKER_CONT} ${DOCKER_IMAGE}
#
# docker exec ${DOCKER_CONT} ls -l /
# docker exec ${DOCKER_CONT} apt search cmake
# docker stop ${DOCKER_CONT}
