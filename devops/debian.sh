 
#!/bin/bash

SOURCE_FOLDER="/sources"
BUILD_FOLDER="/build"

PROJECT_NAME=$1
VERSION=$2
PACKAGE_NAME="${PROJECT_NAME}-${VERSION}"

## BUILDING

# Getting dependencies
apt-get --yes update
apt-get --yes install cmake gcc g++ openssl libssl-dev libmosquittopp-dev qtbase5-dev

# Build
mkdir -p ${BUILD_FOLDER} \
	&& cd ${BUILD_FOLDER} \
	&& cmake -DENABLE_WERROR=ON ${SOURCE_FOLDER} \
	&& make -j4 \
	&& make install


## DEBIAN PACKAGING

DEBIAN_RES="${SOURCE_FOLDER}/devops/debian"

# mkdir /<package_name-version>
mkdir -p /${PACKAGE_NAME}/usr/local/bin
echo $PROJECT_NAME
echo $PACKAGE_NAME
echo $PWD
cp ${DEBIAN_RES}/graftonite.sh "/${PACKAGE_NAME}/usr/local/bin/${PROJECT_NAME}.sh"

mkdir -p /${PACKAGE_NAME}/DEBIAN
# cp ${DEBIAN_RES}/control /${PACKAGE_NAME}/DEBIAN
# or
touch /${PACKAGE_NAME}/DEBIAN/control
echo "Package: Graftonite
Version: 1.0
Maintainer: Me
Architecture: all
Description: This is Graftonite" > /${PACKAGE_NAME}/DEBIAN/control

# Post-installation script
# cp ./postinst.sh /${PACKAGE_NAME}/DEBIAN

dpkg-deb --build /${PACKAGE_NAME}


