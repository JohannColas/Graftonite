#include "Graftonite.h"

// Qt dependencies
#include <QApplication>
#include <QPushButton>
#include <QSplitter>
#include <QStackedWidget>
#include <QVBoxLayout>

// Project dependencies
#include "ProjectManager/ProjectModel.h"
#include "ProjectManager/ProjectView.h"

// Frames
#include "Welcome/WelcomeFrame.h"
#include "Settings/SettingsFrame.h"

// Editors
#include "DataSheetManager/DataEditor.h"
#include "GraphManager/GraphEditor.h"
#include "BasicWidgets/ImageViewer.h"

//
#include "Commons/Key.h"
#include "Commons/App.h"
#include "Commons/Files.h"


#include <QDebug>



// Grafonite destructor
Graftonite::~Graftonite()
{
	delete splitter;
//	delete _layout;
//	delete cont_main;
//	delete wid_welcome;
//	delete wid_settings;
//	delete wid_data;
//	delete wid_graph;
//	delete wid_empty;
}



// Grafonite constructor
Graftonite::Graftonite( QWidget* parent ) :
    QWidget(parent)
{
	// Window settings
	setWindowTitle( "Graftonite" );
	setWindowIcon( QIcon("icons/Dark Theme/graftonite.svg") );

	// Window style
	qApp->setStyleSheet( qApp->styleSheet()+
						 ".SidebarButton{text-align:left;font-weight:bold;icon-size:30px;}"+
						 ".SettingsSection{font: bold 12px;}" );


	// Create welcome button
	pb_welcome = new QPushButton( "Welcome", this );
	pb_welcome->setProperty( "class", "SidebarButton" );
	pb_welcome->setObjectName( "pb_welcome" );
	pb_welcome->setFlat( true );
	pb_welcome->setIcon( QIcon::fromTheme( "welcome", QIcon("icons/Dark Theme/welcome.svg")) );
	connect( pb_welcome, &QPushButton::released,
	         this, &Graftonite::on_pb_welcome_released );

	// Create project explorer button
	pb_project_explorer = new QPushButton( "Project Explorer", this );
	pb_project_explorer->setProperty( "class", "SidebarButton" );
	pb_project_explorer->setObjectName( "pb_project_explorer" );
	pb_project_explorer->setFlat( true );
	pb_project_explorer->setIcon( QIcon::fromTheme( "project_explorer", QIcon("icons/Dark Theme/project_explorer.svg")) );

	// Create project explorer tree view
	_projects_model = new ProjectModel(this);
	_projects_view = new ProjectView(this);
	_projects_view->setProperty( "class", "ProjectTree" );
	_projects_view->setObjectName( "tree_project" );
	_projects_view->setModel(_projects_model);
	connect( _projects_model, &ProjectModel::currentProjectItemChanged,
	         this, &Graftonite::onCurrentProjectItemChanged );

	// Create settings button
	pb_settings = new QPushButton( "Settings", this );
	pb_settings->setProperty( "class", "SidebarButton" );
	pb_settings->setObjectName( "pb_settings" );
	pb_settings->setFlat( true );
	pb_settings->setIcon( QIcon::fromTheme( "settings", QIcon("icons/Dark Theme/settings.svg")) );
	connect( pb_settings, &QPushButton::released,
	         this, &Graftonite::on_pb_settings_released );


	// Create sidebar layout and widget + add sidebar elements
	_layout = new QVBoxLayout( this );
	_layout->addWidget( pb_welcome );
	_layout->addWidget( pb_project_explorer );
	_layout->addWidget( _projects_view );
	_layout->addWidget( pb_settings );
	wid_sidebar = new QWidget(this);
	wid_sidebar->setLayout(_layout);
	QRect rect = wid_sidebar->geometry();
	rect.setWidth(250);
	wid_sidebar->setGeometry( rect );


	// Create Frames and Editors
	wid_welcome = new WelcomeFrame(this);
	wid_data = new DataEditor(this);
	wid_graph = new GraphEditor(this);
	wid_settings = new SettingsFrame(this);
	wid_image = new ImageViewer(this);
	wid_empty = new QWidget(this);


	// Create stack widget and add Frames/Editors
	cont_main = new QStackedWidget(this);
	cont_main->addWidget(wid_welcome);
	cont_main->addWidget(wid_data);
	cont_main->addWidget(wid_graph);
	cont_main->addWidget(wid_settings);
	cont_main->addWidget(wid_image);
	cont_main->addWidget(wid_empty);
	cont_main->show();


	// Create splitter widget and add sidebar widget and stack widget
	splitter = new QSplitter(this);
	splitter->setChildrenCollapsible( false );
	splitter->addWidget(wid_sidebar);
	splitter->addWidget(cont_main);
	connect( splitter, &QSplitter::splitterMoved,
	         this, &Graftonite::onSplitterMoved );


	// Create main layout and add splitter widget
	QVBoxLayout* lay_main = new QVBoxLayout(this);
	lay_main->addWidget( splitter );
	this->setLayout( lay_main );


	// Restore the previous state
	showMaximized();
}



void Graftonite::on_pb_welcome_released()
{
	cont_main->setCurrentWidget( wid_welcome );
}



void Graftonite::on_pb_settings_released()
{
	cont_main->setCurrentWidget( wid_settings );
}



void Graftonite::onSplitterMoved( int pos, int index )
{
	if ( pos < _save_sidebar_width+2 )
	{
		_sidebar_is_collapsed = true;
		pb_welcome->setText( "" );
		pb_project_explorer->setText( "" );
		pb_settings->setText( "" );
		_projects_view->setAbleToHide( true );
	}
	else if ( _sidebar_is_collapsed && pos > _save_sidebar_width+2 )
	{
		_sidebar_is_collapsed = false;
		pb_welcome->setText( "Welcome" );
		pb_project_explorer->setText( "Project Explorer" );
		pb_settings->setText( "Settings" );
		_projects_view->setAbleToHide( false );
	}
}



void Graftonite::onCurrentProjectItemChanged( ProjectItem* item )
{
	if ( item == nullptr )
		return;

	Key type  = item->type();
	QString path = _projects_model->fullPath(item);

	if ( type == Key::Project )
	{
		cont_main->setCurrentWidget( wid_empty );
	}
	else if ( type == Key::Folder )
	{
		cont_main->setCurrentWidget( wid_empty );
	}
	else if ( type == Key::DataSheet )
	{
		cont_main->setCurrentWidget( wid_data );
		wid_data->setDataPath( path );
	}
	else if ( type == Key::Graph )
	{
		cont_main->setCurrentWidget( wid_graph );
		wid_graph->setGraphPath( path );
//		QString prj_path = tree_project->currentProject( item )->path();
//		wid_graph->setAvailableData( Project( prj_path ).availableData() );
	}
	else if ( type == Key::Image )
	{
		cont_main->setCurrentWidget( wid_image );
		wid_image->loadFile( path );
	}
	else
		cont_main->setCurrentWidget( wid_empty );
}



void Graftonite::closeEvent( QCloseEvent* event )
{
	_projects_model->saveProjects();
}



