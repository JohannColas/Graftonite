#ifndef GRAPHVIEW_H
#define GRAPHVIEW_H
/**********************************************/
#include <QGraphicsView>
#include <QMenu>
#include <QContextMenuEvent>
/**********************************************/
class GraphModel;
class GraphicsItem;
/**********************************************/
/**********************************************/
/**********************************************/
class GraphView
		: public QGraphicsView
{
	Q_OBJECT
private:
	GraphModel* _model = nullptr;
//	GraphItem* graphItem = nullptr;
//	QList<LayerItem*> layerItems;
//	TitleItem* titleItem = nullptr;
//	QList<AxisItem*> axisItems;
//	QList<CurveItem*> curveItems;
//	QList<ShapeItem*> shapeItems;
	QMenu* contextMenu = nullptr;
	QAction* acFitToScene = new QAction("Fit to Scene", this);
	QAction* acSaveGraph = new QAction("Save Graph", this);
	QAction* acExportGraph = new QAction("Export Graph", this);
	QGraphicsRectItem* _selectionRect = nullptr;
	bool _gridVisible = true;
	int _gridSize = 10;
	QPoint _old_pos;
	bool _movable = false;
	double _scale = 1;
public:
	~GraphView();
	GraphView( QWidget* parent = nullptr );
	void init();
	/******************************************/
	void setModel( GraphModel* model );
	/******************************************/
public slots:
	void save();
//	void update( GraphSettings* settings );
	void clearAllItems();
	void translateScene( const QPointF& translation, const QPoint& cursor_pos = {-1,-1} );
	void fitToViewport();
	void resizeView();
	void exportGraph();
	/******************************************/
	void updateFromModel();
	/******************************************/
private:
	void zoomAt( const QPoint& centerPos, double factor );
	/******************************************/
private slots:
	void onItemDataChanged(/* GraphicsItem* item */);
	/******************************************/
	void keyPressEvent( QKeyEvent* event ) override;
	void mousePressEvent( QMouseEvent* event ) override;
	void mouseReleaseEvent( QMouseEvent* event ) override;
	void mouseMoveEvent( QMouseEvent* event ) override;
	void wheelEvent( QWheelEvent* event ) override;
	void resizeEvent( QResizeEvent* event ) override;
	void contextMenuEvent( QContextMenuEvent* event ) override;
	void paintEvent( QPaintEvent* event ) override;
	/******************************************/
signals:
	void resized();
	void saveGraph();

	void currentGraphicsItemChanged( GraphicsItem* item );
	/******************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // GRAPHVIEW_H
