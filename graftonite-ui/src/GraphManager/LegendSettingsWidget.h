#ifndef LEGENDSETTINGSWIDGET_H
#define LEGENDSETTINGSWIDGET_H
/**********************************************/
#include "../Settings/SettingsWidget.h"
/**********************************************/
/**********************************************/
/**********************************************/
class LegendSettingsWidget
        : public SettingsWidget
{
	Q_OBJECT
public:
	LegendSettingsWidget( QWidget *parent = nullptr );
	/******************************************/
	void connectSettings( Settings* settings ) override;
	/******************************************/
signals:
	/******************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LEGENDSETTINGSWIDGET_H
