#include "GraphModel.h"
/**********************************************/
#include <QItemSelectionModel>
/**********************************************/
#include "GraphicsItem.h"
#include "GraphItem.h"
#include "FrameItem.h"
#include "AxisItem.h"
#include "PlotItem.h"
#include "LegendItem.h"
#include "TitleItem.h"
#include "ShapeItem.h"
/**********************************************/
/**********************************************/
/**********************************************/
GraphModel::~GraphModel()
{

}
/**********************************************/
GraphModel::GraphModel( QObject* parent )
    : QAbstractItemModel(parent)
{
	_root_item    = new GraphicsItem( "", Key::Root, nullptr );
	_graph_item   = new GraphItem();
	connect( &_graph_item->settings(), &Settings::changed,
	         this, &GraphModel::itemDataChanged );
	_root_item->appendChild( _graph_item );

	_frames_item  = new GraphicsItem( "Frames",  Key::Frames,  _graph_item );
	_frames_item->appendChild( new FrameItem() );

	connect( &_frames_item->childAt(0)->settings(), &Settings::changed,
	         this, &GraphModel::itemDataChanged );

	_axes_item    = new GraphicsItem( "Axes",    Key::Axes,   _graph_item );
	_axes_item->appendChild( new AxisItem( "Axis1" ) );
	_axes_item->appendChild( new AxisItem("Axis2") );
	_axes_item->childAt(0)->addSetting( Key::Type, Key::Y);
	_axes_item->childAt(0)->updateGraphItem();
	connect( &_axes_item->childAt(0)->settings(), &Settings::changed,
	         this, &GraphModel::itemDataChanged );
	connect( &_axes_item->childAt(1)->settings(), &Settings::changed,
	         this, &GraphModel::itemDataChanged );
	_plots_item   = new GraphicsItem( "Plots",   Key::Plots,   _graph_item );
	_legends_item = new GraphicsItem( "Legends", Key::Legends, _graph_item );
	_titles_item  = new GraphicsItem( "Titles",  Key::Titles,  _graph_item );
	_shapes_item  = new GraphicsItem( "Shapes",  Key::Shapes,  _graph_item );
	_graph_item->appendChild( _frames_item );
	_graph_item->appendChild( _axes_item );
	_graph_item->appendChild( _plots_item );
	_graph_item->appendChild( _legends_item );
	_graph_item->appendChild( _titles_item );
	_graph_item->appendChild( _shapes_item );
}
/**********************************************/
/**********************************************/
QModelIndex GraphModel::index( int row, int column, const QModelIndex& parent_index ) const
{
	GraphicsItem* parent_item = this->itemAt(parent_index);

	GraphicsItem* child_item = parent_item->childAt(row);
	if ( child_item == nullptr )
		return QModelIndex();

	return createIndex(row, column, child_item);
}
/**********************************************/
QModelIndex GraphModel::parent( const QModelIndex& child_index ) const
{
	if ( !child_index.isValid() )
		return QModelIndex();

	GraphicsItem* child_item = this->itemAt(child_index);
	GraphicsItem* parent_item = child_item->parent();

	if ( parent_item == nullptr )
		return QModelIndex();

	return createIndex(parent_item->row(), 0, parent_item);
}
/**********************************************/
/**********************************************/
int GraphModel::rowCount( const QModelIndex& parent_index ) const
{
	GraphicsItem* parent_item = this->itemAt(parent_index);
	return (parent_item == nullptr ? 0 :parent_item->childCount());
}
/**********************************************/
int GraphModel::columnCount( const QModelIndex& parent_index ) const
{
	return 1;//3;
}

bool GraphModel::insertRows( int row, int count, const QModelIndex& parent_index )
{
//	GraphicsItem* parent_item = itemAt(parent_index);

	beginInsertRows(parent_index, row, row+count-1);
	for ( int it = 0; it < count; ++it )
	{
//		GraphicsItem* newItem = new GraphicsItem("", Key::NoType, parent_item);
//		if ( !parent_item->insertChild(row+it, newItem) )
//			break;
	}
	endInsertRows();
	return true;
}

bool GraphModel::removeRows( int row, int count, const QModelIndex& parent_index )
{
//	GraphicsItem* parent_item = itemAt(parent_index);

	beginRemoveRows(parent_index, row, row+count-1);
	for ( int it = 0; it < count; ++it )
	{
//		if ( !parent_item->deleteChild(row+it) )
//			break;
	}
	endRemoveRows();
	return true;
}

bool GraphModel::insertColumns( int column, int count, const QModelIndex& parent_index )
{
	return false;
}

bool GraphModel::removeColumns( int column, int count, const QModelIndex& parent_index )
{
	return false;
}
/**********************************************/
/**********************************************/
Qt::ItemFlags GraphModel::flags( const QModelIndex& index ) const
{
	if (!index.isValid())
		return Qt::NoItemFlags;

	Qt::ItemFlags defaultFlags = QAbstractItemModel::flags(index);

	if ( index.parent().isValid() && index.parent().internalPointer() != _graph_item )
		defaultFlags = defaultFlags | Qt::ItemIsEditable;

	return defaultFlags;
}
/**********************************************/
/**********************************************/
QVariant GraphModel::data( const QModelIndex& index, int role ) const
{
	if ( !index.isValid() )
		return QVariant();

	if ( role == Qt::DisplayRole || role == Qt::EditRole )
	{
		GraphicsItem* item = itemAt(index);
		if ( item == nullptr )
			return QVariant();
		return item->name();
	}
	else if ( role == Qt::DecorationRole )
	{
		if ( index.column() == 0 )
		{
			GraphicsItem* item = itemAt(index);
			if ( item == nullptr )
				return QVariant();
			if ( item->itemType() == Key::Graph )
				return QIcon("icons/Dark Theme/graph.svg");
			else if ( item->itemType() == Key::Frames )
				return QIcon("icons/Dark Theme/frame.svg");
			else if ( item->itemType() == Key::Axes )
				return QIcon("icons/Dark Theme/axis.svg");
			else if ( item->itemType() == Key::Plots )
				return QIcon("icons/Dark Theme/plot.svg");
			else if ( item->itemType() == Key::Legends )
				return QIcon("icons/Dark Theme/legend.svg");
			else if ( item->itemType() == Key::Titles )
				return QIcon("icons/Dark Theme/title.svg");
			else if ( item->itemType() == Key::Shapes )
				return QIcon("icons/Dark Theme/shapes.svg");
		}
	}

	return QVariant();
}
/**********************************************/
bool GraphModel::setData( const QModelIndex& index, const QVariant& value, int role )
{
	if ( !index.isValid() )
		return false;

	switch (role)
	{
		case Qt::DisplayRole:
		case Qt::EditRole:
//			ProjectItem* item = projectItemAt(index);
//			if ( item == nullptr || item == _rootItem )
//				return false;
//			if (index.column() == 0)
//			{
//				item->setName( value.toString() );
//			}
//			else if (index.column() == 1)
//			{
//				item->setPath( value.toString() );
//			}
//			emit dataChanged(index, index);
//		return true;
		break;
	}

	return false;
}
/**********************************************/
/**********************************************/
void GraphModel::setItemSelectionModel( QItemSelectionModel* selection )
{
	_selection_model = selection;
	connect( _selection_model, &QItemSelectionModel::currentRowChanged,
	         this, &GraphModel::onSelectionChanged );
}
/**********************************************/
/**********************************************/
void GraphModel::openGraph()
{

}
/**********************************************/
void GraphModel::resetGraph()
{

}
/**********************************************/
/**********************************************/
void GraphModel::addFrame()
{
	beginInsertRows( indexOf(_frames_item), _frames_item->childCount(), _frames_item->childCount());
	FrameItem* newchild = new FrameItem(_frames_item);
	_frames_item->appendChild( newchild );
	endInsertRows();
}
/**********************************************/
void GraphModel::addAxis()
{
	beginInsertRows( indexOf(_axes_item), _axes_item->childCount(), _axes_item->childCount());
	AxisItem* newchild = new AxisItem(_axes_item);
	_axes_item->appendChild( newchild );
	endInsertRows();
}
/**********************************************/
void GraphModel::addPlot()
{
	beginInsertRows( indexOf(_plots_item), _plots_item->childCount(), _plots_item->childCount());
	PlotItem* newchild = new PlotItem(_plots_item);
	_plots_item->appendChild( newchild );
	endInsertRows();
}
/**********************************************/
void GraphModel::addLegend()
{
	beginInsertRows( indexOf(_legends_item), _legends_item->childCount(), _legends_item->childCount());
	LegendItem* newchild = new LegendItem(_legends_item);
	_legends_item->appendChild( newchild );
	endInsertRows();
}
/**********************************************/
void GraphModel::addTitle()
{
	beginInsertRows( indexOf(_titles_item), _titles_item->childCount(), _titles_item->childCount());
	TitleItem* newchild = new TitleItem(_titles_item);
	_titles_item->appendChild( newchild );
	endInsertRows();
}
/**********************************************/
void GraphModel::addShape()
{
	beginInsertRows( indexOf(_shapes_item), _shapes_item->childCount(), _shapes_item->childCount());
	ShapeItem* newchild = new ShapeItem(_shapes_item);
	_shapes_item->appendChild( newchild );
	endInsertRows();
}
/**********************************************/
/**********************************************/
void GraphModel::removeFrames()
{
	if ( _current_item == nullptr )
		return;
	int row = _frames_item->indexOf(_current_item) ;
	if ( row != -1 )
	{
		beginRemoveRows(indexOf(_frames_item), row, row );
		_frames_item->deleteChild(row);
		endRemoveRows();
	}
}
/**********************************************/
void GraphModel::removeAxes()
{
	if ( _current_item == nullptr )
		return;
	int row = _axes_item->indexOf(_current_item) ;
	if ( row != -1 )
	{
		beginRemoveRows(indexOf(_axes_item), row, row );
		_axes_item->deleteChild(row);
		endRemoveRows();
	}
}
/**********************************************/
void GraphModel::removePlots()
{
	if ( _current_item == nullptr )
		return;
	int row = _plots_item->indexOf(_current_item) ;
	if ( row != -1 )
	{
		beginRemoveRows(indexOf(_plots_item), row, row );
		_plots_item->deleteChild(row);
		endRemoveRows();
	}
}
/**********************************************/
void GraphModel::removeLegends()
{
	if ( _current_item == nullptr )
		return;
	int row = _legends_item->indexOf(_current_item) ;
	if ( row != -1 )
	{
		beginRemoveRows(indexOf(_legends_item), row, row );
		_legends_item->deleteChild(row);
		endRemoveRows();
	}
}
/**********************************************/
void GraphModel::removeTitles()
{
	if ( _current_item == nullptr )
		return;
	int row = _titles_item->indexOf(_current_item) ;
	if ( row != -1 )
	{
		beginRemoveRows(indexOf(_titles_item), row, row );
		_titles_item->deleteChild(row);
		endRemoveRows();
	}
}
/**********************************************/
void GraphModel::removeShapes()
{
	if ( _current_item == nullptr )
		return;
	int row = _shapes_item->indexOf(_current_item) ;
	if ( row != -1 )
	{
		beginRemoveRows(indexOf(_shapes_item), row, row );
		_shapes_item->deleteChild(row);
		endRemoveRows();
	}
}

void GraphModel::deleteItems()
{
//	QList<GraphicsItem*> selected_items = _selected_items;

	if ( _current_item == nullptr ||
	     _current_item == _graph_item ||
	     _current_item == _frames_item ||
	     _current_item == _axes_item ||
	     _current_item == _plots_item ||
	     _current_item == _legends_item ||
	     _current_item == _titles_item ||
	     _current_item == _shapes_item )
		return;
	int row = _shapes_item->indexOf(_current_item) ;
	if ( row != -1 )
	{
		beginRemoveRows(indexOf(_shapes_item), row, row );
		_shapes_item->deleteChild(row);
		endRemoveRows();
	}
}
/**********************************************/
/**********************************************/
void GraphModel::onSelectionChanged( const QModelIndex& index, const QModelIndex& previous )
{
	_current_item = itemAt(index);

	emit currentGraphicsItemChanged( _current_item );
}

GraphicsItem* GraphModel::itemAt( const QModelIndex& index ) const
{
	if ( index.isValid() )
	{
		GraphicsItem* item = static_cast<GraphicsItem*>(index.internalPointer());
		if ( item != nullptr )
			return item;
	}
	return _root_item;
}

QModelIndex GraphModel::indexOf( GraphicsItem* item ) const
{
	if ( item == nullptr )
		return index(0,0);

	QList<GraphicsItem*> _items;
	GraphicsItem* tmp = item;
	while ( tmp != _root_item )
	{
		_items.push_front(tmp);
		tmp = tmp->parent();
	}
	QModelIndex idx;
	for ( GraphicsItem* it : _items )
		idx = index(it->row(), 0, idx );
	return idx;
}

GraphItem* GraphModel::graphItem()
{
	return _graph_item;
}

int GraphModel::framesCount() const
{
	return _frames_item->childCount();
}

FrameItem* GraphModel::frameItemAt( int index )
{
	return static_cast<FrameItem*>(_frames_item->childAt(index));
}

int GraphModel::axesCount() const
{
	return _axes_item->childCount();
}

AxisItem* GraphModel::axisItemAt( int index )
{
	return static_cast<AxisItem*>(_axes_item->childAt(index));
}

void GraphModel::onCurrentGraphicsItemChanged( GraphicsItem* item )
{
	_selection_model->setCurrentIndex( indexOf( item ), QItemSelectionModel::SelectCurrent );
}
/**********************************************/
/**********************************************/
