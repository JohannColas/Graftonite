#ifndef GRAPHTREEVIEW_H
#define GRAPHTREEVIEW_H
/**********************************************/
/**********************************************/
#include <QWidget>
#include <QMenu>
/**********************************************/
/**********************************************/
class QVBoxLayout;
class QLabel;
class QTreeView;
class GraphModel;
/**********************************************/
/**********************************************/
/**********************************************/
class GraphTreeView
        : public QWidget
{
	Q_OBJECT
	// Menu
	QMenu* contextMenu = nullptr;
	QAction* ac_newframe = nullptr;
	QAction* ac_newaxis = nullptr;
	QAction* ac_newplot = nullptr;
	QAction* ac_newlegend = nullptr;
	QAction* ac_newtitle = nullptr;
	QAction* ac_newshape = nullptr;
	QAction* ac_resetgraph = nullptr;
	QAction* ac_delete = nullptr;
public:
	explicit GraphTreeView( QWidget* parent = nullptr );
	/******************************************/
	void setModel( GraphModel* model );
	/******************************************/
private:
	QVBoxLayout* _lay_main = nullptr;
	QLabel* _label = nullptr;
	QTreeView* _graph_tree = nullptr;
	/******************************************/
private slots:
	void leaveEvent( QEvent* event ) override;
	void contextMenuEvent( QContextMenuEvent* event ) override;
	/******************************************/
signals:
	void leaved();
	void newFrame();
	void newAxis();
	void newPlot();
	void newLegend();
	void newTitle();
	void newShape();
	void resetGraph();
	void deleteItem();
	/******************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // GRAPHTREEVIEW_H
