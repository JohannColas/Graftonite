#include "TitleSettingsWidget.h"
/**********************************************/
/**********************************************/
/**********************************************/
/**********************************************/
TitleSettingsWidget::TitleSettingsWidget( QWidget* parent )
    : SettingsWidget(parent)
{
	addLineEdit( Key::Name, "Title :" );
}
/**********************************************/
/**********************************************/
void TitleSettingsWidget::connectSettings( Settings* settings )
{
	SettingsWidget::connectSettings(settings);

}
/**********************************************/
/**********************************************/
