#include "GraphExportDialog.h"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QFormLayout>
#include <QLabel>
#include <QPushButton>
#include <QLineEdit>
#include <QComboBox>

GraphExportDialog::GraphExportDialog( QWidget* parent )
	: Dialog(parent)
{
	setFixedSize( 400, 300 );

	lb_path = new QLabel("Path:");
	pb_path = new QPushButton("[NoPath]");
	lb_name = new QLabel("Graph name:");
	le_name = new QLineEdit;
	lb_format = new QLabel("Graph format:");
	cb_format = new QComboBox;

	lay_settings = new QFormLayout;
	lay_settings->addRow( lb_path, pb_path );
	lay_settings->addRow( lb_name, le_name );
	lay_settings->addRow( lb_format, cb_format );
	lay_settings->addItem( new QSpacerItem(0,0,QSizePolicy::Minimum,QSizePolicy::Expanding) );

	pb_ok = new QPushButton("ok");
	pb_cancel = new QPushButton("cancel");
	lay_diagbuttons = new QHBoxLayout;
	lay_diagbuttons->addStretch();
	lay_diagbuttons->addWidget(pb_ok);
	lay_diagbuttons->addWidget(pb_cancel);

	lb_title = new QLabel("Graph Export Dialog");
	lb_title->setAlignment(Qt::AlignCenter);

	lay_main = new QVBoxLayout;
	lay_main->addWidget(lb_title);
	lay_main->addLayout(lay_settings);
	lay_main->addLayout(lay_diagbuttons);

	setLayout( lay_main );

	connect( pb_ok, &QPushButton::released,
			 this, &GraphExportDialog::onOk );
	connect( pb_cancel, &QPushButton::released,
			 this, &GraphExportDialog::onCancel );
}

void GraphExportDialog::onOk()
{
	this->close();

}

void GraphExportDialog::onCancel()
{
	this->close();
}
