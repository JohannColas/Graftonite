#include "GraphItem.h"
/**********************************************/
#include "GraphView.h"
/**********************************************/
#include "FrameItem.h"
#include "AxisItem.h"
#include "PlotItem.h"
#include "LegendItem.h"
#include "TitleItem.h"
#include "ShapeItem.h"
/**********************************************/
/**********************************************/
/**********************************************/
GraphItem::~GraphItem()
{

}
GraphItem::GraphItem( QGraphicsItem* parent )
	: GraphicsItem(parent)
{
	init();
}
/**********************************************/
/**********************************************/
GraphItem::GraphItem( const QString& name, QGraphicsItem* parent )
	: GraphicsItem(name, parent)
{
	init();
}
void GraphItem::init()
{
	_item_type = Key::Graph;

	// Defaut Settings
	GraphicsItem::addSetting( Key(Key::Background, Key::Color), QColor(0,0,0,0));
	GraphicsItem::addSetting( Key(Key::Borders, Key::Color), QColor(0,0,0,0));

	connect( &_settings, &Settings::changed,
			 this, &GraphItem::updateGraphItem );
}
/**********************************************/
/**********************************************/
void GraphItem::addSetting( const Key& key, const QVariant& value )
{
	_settings.addSetting( key, value );
}
/**********************************************/
/**********************************************/
void GraphItem::updateGraphItem()
{
	bool hide = false; _settings.getBool( hide, Key::Hide );
	if ( !hide )
	{
		QPen  pen = QPen(Qt::NoPen);
		_settings.getPen( pen, Key::Borders );
		QBrush brush = _settings.getBrush(Key::Background);
		if ( graph_rect == nullptr )
		{
			graph_rect = new QGraphicsRectItem;
			addToGroup( graph_rect );
		}
		graph_rect->setPen( pen );
		graph_rect->setBrush( brush );
		graph_rect->setRect( this->boundingRect() );
		graph_rect->setZValue( 0 );
	}
	else
	{
		if ( graph_rect != nullptr )
		{
			removeFromGroup( graph_rect );
			delete graph_rect;
			graph_rect = nullptr;
		}
	}
	emit changed();
}
/**********************************************/
/**********************************************/
