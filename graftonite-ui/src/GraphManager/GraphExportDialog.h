#ifndef GRAPHEXPORTDIALOG_H
#define GRAPHEXPORTDIALOG_H

#include "../BasicWidgets/Dialog.h"

class QVBoxLayout;
class QHBoxLayout;
class QFormLayout;
class QLabel;
class QPushButton;
class QComboBox;
class QLineEdit;

class GraphExportDialog
		: public Dialog
{
	Q_OBJECT
public:
	GraphExportDialog( QWidget* parent = nullptr );


protected slots:
	void onOk();
	void onCancel();

protected:
	QVBoxLayout* lay_main = nullptr;
	QLabel* lb_title = nullptr;

	QFormLayout* lay_settings = nullptr;
	QLabel* lb_path = nullptr;
	QPushButton* pb_path = nullptr;
	QLabel* lb_name = nullptr;
	QLineEdit* le_name = nullptr;
	QLabel* lb_format = nullptr;
	QComboBox* cb_format = nullptr;

	QHBoxLayout* lay_diagbuttons = nullptr;
	QPushButton* pb_ok = nullptr;
	QPushButton* pb_cancel = nullptr;
};

#endif // GRAPHEXPORTDIALOG_H
