#ifndef FRAMEITEM_H
#define FRAMEITEM_H
/**********************************************/
#include "GraphicsItem.h"
/**********************************************/
/**********************************************/
/**********************************************/
class FrameItem
		: public GraphicsItem
{
	Q_OBJECT
	QGraphicsRectItem* _frame = nullptr;
//	Rect _rect = {100,100,1200,800};
public:
	~FrameItem();
	FrameItem( QGraphicsItem* parent = nullptr );
	FrameItem( const QString& name, QGraphicsItem* parent = nullptr );
	void init();
	/******************************************/
	void addSetting( const Key& key, const QVariant& value ) override;
	/******************************************/
	Rect rect() const;
	/******************************************/
public slots:
	void updateGraphItem() override;
	/******************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // FRAMEITEM_H
