#ifndef GRAPHEDITOR_H
#define GRAPHEDITOR_H
/**********************************************/
#include <QWidget>
/**********************************************/
class QTreeWidgetItem;
class QStackedWidget;
class QSplitter;
class QVBoxLayout;
/**********************************************/
class GraphSettingsWidget;
class FrameSettingsWidget;
class AxisSettingsWidget;
class PlotSettingsWidget;
class LegendSettingsWidget;
class TitleSettingsWidget;
class ShapeSettingsWidget;
/**********************************************/
class GraphModel;
class GraphicsItem;
class GraphTreeView;
class GraphTree;
class GraphView;
class GraphItem;
/**********************************************/
class ProjectItem;
/**********************************************/
/**********************************************/
/**********************************************/
class GraphEditor
        : public QWidget
{
	Q_OBJECT
public:
	~GraphEditor();
	GraphEditor( QWidget *parent = nullptr );
	/******************************************/
	void setGraphPath( const QString& path );
	void setGraphFile( ProjectItem* graph_item );
	void setAvailableData( const QStringList& availableData );
	/******************************************/
private slots:
	void graphChanged();
	void onCurrentGraphicsItemChanged( GraphicsItem* item );
	void closeEvent( QCloseEvent* event ) override;
	/******************************************/
	void on_pb_newLayer_released();
	void on_pb_newAxis_released();
	void on_pb_newCurve_released();
	void on_pb_remove_released();
	/******************************************/
	void newFrame();
	void newAxis();
	void newPlot();
	void newLegend();
	void newTitle();
	void newShape();
	void resetGraph();
	void deleteItem();
	/******************************************/
	void saveGraph();
	/******************************************/
	void on_pb_save_released();
	/******************************************/
	void hideEvent( QHideEvent* event ) override;
	/******************************************/
private:
	GraphModel* _graph_model = nullptr;
	GraphTreeView* _graph_tree_view = nullptr;
	QVBoxLayout* lay_main = nullptr;
	GraphView* view_graph = nullptr;
	QStackedWidget* cont_settings = nullptr;
	QSplitter* splitter = nullptr;
	QSplitter* sp_sidebar = nullptr;
	/******************************************/
	GraphSettingsWidget* wid_graph = nullptr;
	FrameSettingsWidget* wid_frame = nullptr;
	AxisSettingsWidget* wid_axis = nullptr;
	PlotSettingsWidget* wid_plot = nullptr;
	LegendSettingsWidget* wid_legend = nullptr;
	TitleSettingsWidget* wid_title = nullptr;
	ShapeSettingsWidget* wid_shape = nullptr;
	/******************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // GRAPHEDITOR_H
