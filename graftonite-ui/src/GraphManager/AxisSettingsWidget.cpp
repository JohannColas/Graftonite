#include "AxisSettingsWidget.h"
/**********************************************/
#include <QFormLayout>
#include <QTabWidget>
#include <QLabel>
#include <QTextEdit>
/**********************************************/
#include "../BasicWidgets/ComboBox2.h"
#include "../BasicWidgets/TextEditor.h"
#include "../BasicWidgets/PenWidgets.h"
#include "GraphItem.h"
#include "FrameItem.h"
/**********************************************/
#include "../Commons/Settings.h"
#include "../Settings/SettingItem.h"
/**********************************************/
/**********************************************/
/**********************************************/
AxisSettingsWidget::AxisSettingsWidget( QWidget* parent )
    : SettingsWidget(parent)
{
	addLineEdit( Key::Name, "Axis :" );
//	addWidget( Key::Position, "pos2", nullptr);

	wid_general = new SettingsWidget(this);
	cb_type = wid_general->addKeySelector( Key(Key::Type), "Type :" );
	cb_type->setData( {{"X", Key::X},{"Y", Key::Y},{"Polar", Key::Polar}} );
	connect( cb_type, &ComboBox2::valueChanged,
			 this, &AxisSettingsWidget::onTypeChanged );
	cb_pos = wid_general->addKeySelector( Key(Key::Position), "Position :" );
	wid_general->addDoubleEdit( Key(Key::Shift), "Shift :" );
	cb_pos->setData( x_data );
	cb_frames = wid_general->addComboBox( Key(Key::Frame), "Parent Frame :" );
	wid_general->addDoubleEdit( Key(Key::Min), "Min :" );
	wid_general->addDoubleEdit( Key(Key::Max), "Max :" );
	cb_scale = wid_general->addKeySelector( Key(Key::Scale), "Scale :" );
	cb_scale->setData( {{"Linear",Key::Linear},{"Log",Key::Log},{"Log10",Key::Log10},{"LogX",Key::LogX},{"Reciprocal",Key::Reciprocal},{"Reciprocal Offset",Key::OffsetReciprocal}} );
	wid_general->addDoubleEdit( Key(Key::Scale, Key::Offset), "Scale Offset :" );
	wid_general->addSeparator();
	wid_general->addSection( "Line Style" );
	PenModifier* mod = wid_general->addPenEditor( Key(Key::Line,Key::Style), "Line Style" );
	mod->desactivateJoin();

	wid_ticks = new SettingsWidget(this);
	cb_ticks_pos = wid_ticks->addKeySelector( Key(Key::Ticks, Key::Position), "Position :" );
	QList<QPair<QString,QVariant>> lst = x_data;
	lst.append( o_data );
	cb_ticks_pos->setData( lst );
	wid_ticks->addSeparator();
	wid_ticks->addSection( "Major" );
	wid_ticks->addIntegerEdit( Key(Key::Ticks, Key::Size), "Size :" );
	wid_ticks->addDoubleEdit( Key(Key::Ticks, Key::Increment), "Increment :" );
	wid_ticks->addIntegerEdit( Key(Key::Ticks, Key::Numbers), "Numbers :" );
	wid_ticks->addSeparator();
	wid_ticks->addSection( "Minor" );
	wid_ticks->addIntegerEdit( Key(Key::Minor, Key::Ticks, Key::Size), "Size :" );
	wid_ticks->addDoubleEdit( Key(Key::Minor, Key::Ticks, Key::Increment), "Increment :" );
	wid_ticks->addIntegerEdit( Key(Key::Minor, Key::Ticks, Key::Numbers), "Numbers :" );

	wid_labels = new SettingsWidget(this);
	cb_labels_pos = wid_labels->addKeySelector( Key(Key::Labels, Key::Position), "Position :" );
	cb_labels_pos->setData( lst );
	wid_labels->addAnchorSelector( Key(Key::Labels,Key::Anchor), "Anchor :" );
	wid_labels->addComboBox( Key(Key::Labels,Key::Rotation), "Rotation :" );

	wid_title = new SettingsWidget(this);
	cb_title_pos = wid_title->addKeySelector( Key(Key::Title, Key::Position), "Position :" );
	cb_title_pos->setData( lst );
	wid_title->addAnchorSelector( Key(Key::Title,Key::Anchor), "Anchor :" );
	wid_title->addDoubleEdit( Key(Key::Title,Key::Rotation), "Rotation :" );
	wid_title->addSeparator();
	wid_title->addSection( "Title Text :");
	wid_title->addSetting( Key(Key::Title,Key::Text), "Title", "", SettingItem::TextEdit  );

	wid_grids = new SettingsWidget(this);
	cb_grids_pos = wid_grids->addKeySelector( Key(Key::Grids, Key::Position), "Position :" );
	lst.push_front({"No Grids",Key::NoGrids});
	cb_grids_pos->setData( lst );
	wid_grids->addSeparator();
	wid_grids->addSection( "Major Grids Style" );
	mod = wid_grids->addPenEditor( Key(Key::Grids,Key::Style), "Grids Style" );
	mod->desactivateJoin();
	wid_grids->addSeparator();
	wid_grids->addSection( "Minor Grids Style" );
	mod = wid_grids->addPenEditor( Key(Key::Minor,Key::Grids,Key::Style), "Grids Style" );
	mod->desactivateJoin();
	mod->desactivateCap();

	tab_elements = new QTabWidget(this);
	tab_elements->addTab( wid_general, "General" );
	tab_elements->addTab( wid_ticks, "Ticks" );
	tab_elements->addTab( wid_labels, "Labels" );
	tab_elements->addTab( wid_title, "Title" );
	tab_elements->addTab( wid_grids, "Grids" );

	lay_main->addRow( tab_elements );

}
void AxisSettingsWidget::setGraphItem( GraphItem* graph )
{
	_graph = graph;
	onFramesChanged();
//	connect( _graph, &GraphItem::framesChanged,
//			 this, &AxisSettingsWidget::onFramesChanged );
}
/**********************************************/
/**********************************************/
void AxisSettingsWidget::connectSettings( Settings* settings )
{
	SettingsWidget::connectSettings( settings );
	wid_general->connectSettings( settings );
	wid_ticks->connectSettings( settings );
	wid_labels->connectSettings( settings );
	wid_title->connectSettings( settings );
	wid_grids->connectSettings( settings );
//	if ( _settings != nullptr )
//	{
//		disconnect( wid_general, &SettingsWidget::settingChanged,
//		            _settings, &Settings::changeSetting );
//		disconnect( wid_ticks, &SettingsWidget::settingChanged,
//		            _settings, &Settings::changeSetting );
//		disconnect( wid_labels, &SettingsWidget::settingChanged,
//		            _settings, &Settings::changeSetting );
//		disconnect( wid_title, &SettingsWidget::settingChanged,
//		            _settings, &Settings::changeSetting );
//		disconnect( wid_grids, &SettingsWidget::settingChanged,
//		            _settings, &Settings::changeSetting );
//	}
//	SettingsWidget::connectSettings(settings);
//	if ( _settings != nullptr )
//	{
//		connect( wid_general, &SettingsWidget::settingChanged,
//		         _settings, &Settings::changeSetting );
//		connect( wid_ticks, &SettingsWidget::settingChanged,
//		         _settings, &Settings::changeSetting );
//		connect( wid_labels, &SettingsWidget::settingChanged,
//		         _settings, &Settings::changeSetting );
//		connect( wid_title, &SettingsWidget::settingChanged,
//		         _settings, &Settings::changeSetting );
//		connect( wid_grids, &SettingsWidget::settingChanged,
//		         _settings, &Settings::changeSetting );
//		wid_general->updateSettings( _settings );
//		wid_ticks->updateSettings( _settings );
//		wid_labels->updateSettings( _settings );
//		wid_title->updateSettings( _settings );
//		wid_grids->updateSettings( _settings );
//	}
	this->updateSettings( settings );
}

void AxisSettingsWidget::updateSettings(Settings* settings)
{
	SettingsWidget::updateSettings( settings );
	wid_general->updateSettings( settings );
	wid_ticks->updateSettings( settings );
	wid_labels->updateSettings( settings );
	wid_title->updateSettings( settings );
	wid_grids->updateSettings( settings );
}
/**********************************************/
/**********************************************/
void AxisSettingsWidget::onTypeChanged( const QVariant& value )
{
	Key key = value;
	QList<QPair<QString,QVariant>> ax_lst;
	QList<QPair<QString,QVariant>> tk_lst;
	QList<QPair<QString,QVariant>> lb_lst;
	QList<QPair<QString,QVariant>> tl_lst;
	QList<QPair<QString,QVariant>> gd_lst;
	if ( key == Key::X )
	{
		ax_lst.append( x_data );
		ax_lst.append( {"Center", Key::Center} );
		tk_lst.append( x_data );
		tk_lst.append( {"Center", Key::Center} );
		tk_lst.append( o_data );
		lb_lst.append( x_data );
		lb_lst.append( o_data );
		tl_lst.append( {{"Top-Left", Key::TopLeft},{"Top", Key::Top},{"Top-Right", Key::TopRight},{"Bottom-Left", Key::BottomLeft},{"Bottom", Key::Bottom},{"Bottom-Right", Key::BottomRight}} );
		_settings->addSetting( Key(Key::Labels,Key::Anchor), Key::Top );
		_settings->addSetting( Key(Key::Title,Key::Anchor), Key::Top );
	}
	else if ( key == Key::Y )
	{
		ax_lst.append( y_data );
		ax_lst.append( {"Center", Key::Center} );
		tk_lst.append( y_data );
		tk_lst.append( {"Center", Key::Center} );
		tk_lst.append( o_data );
		lb_lst.append( y_data );
		lb_lst.append( o_data );
		tl_lst.append( {{"Top-Left", Key::TopLeft},{"Left", Key::Left},{"Bottom-Left", Key::BottomLeft},{"Top-Right", Key::TopRight},{"Right", Key::Right},{"Bottom-Right", Key::BottomRight}} );
		_settings->addSetting( Key(Key::Labels,Key::Anchor), Key::Right );
		_settings->addSetting( Key(Key::Title,Key::Anchor), Key::Bottom );
	}
	else if ( key == Key::Polar )
	{
		ax_lst = polar_data;
		tk_lst.append( o_data );
	}
	cb_pos->setData( ax_lst );
	cb_ticks_pos->setData( tk_lst );
	cb_labels_pos->setData( lb_lst );
	cb_title_pos->setData( tl_lst );
	gd_lst = ax_lst;
	gd_lst.push_front( {"Both Sides", Key::BothSides} );
	gd_lst.push_front( {"No Grids", Key::NoGrids} );
	cb_grids_pos->setData( gd_lst );
}
/**********************************************/
/**********************************************/
void AxisSettingsWidget::onFramesChanged()
{
	cb_frames->clear();
//	for ( FrameItem* frame : _graph->frames() )
//		cb_frames->addItem(frame->name());
}
/**********************************************/
/**********************************************/
