#include "FrameItem.h"
/**********************************************/
#include "GraphView.h"
/**********************************************/
/**********************************************/
/**********************************************/
FrameItem::~FrameItem()
{

}
FrameItem::FrameItem( QGraphicsItem* parent )
	: GraphicsItem(parent)
{
	init();
}
FrameItem::FrameItem( const QString& name, QGraphicsItem* parent )
	: GraphicsItem(name, parent)
{
	init();
}
void FrameItem::init()
{
	_item_type = Key::Frame;
	_settings.addSetting( Key(Key::Background, Key::Color), QColor(0,0,0,0) );

	connect( &_settings, &Settings::changed,
			 this, &FrameItem::updateGraphItem );
}
/**********************************************/
/**********************************************/
void FrameItem::addSetting( const Key& key, const QVariant& value )
{
//	if ( key == Key::Name ||
//		 key == Key::Template ||
//		 key == Key::Position ||
//		 key == Key::Anchor ||
//		 key == Key::Shift ||
//		 key == Key::Size ||
//		 key.isLineStyle({Key::Borders}, false) ||
//		 key.isFillStyle(Key::Background)
//		 )
//	{
//		GraphicsItem::addSetting( key, value );
//	}
//	else if ( key == Key::Hide )
//	{
//		if ( value.toBool() )
//		{
//			if ( _settings.hasKey(Key(Key::Background, Key::Color)) )
//				GraphicsItem::addSetting( Key(Key::Background, Key::Color), "transparent");
//			if ( _settings.hasKey(Key(Key::Borders, Key::Color)) )
//				GraphicsItem::addSetting( Key(Key::Borders, Key::Color), "black");
//		}
//		GraphicsItem::addSetting( key, value );
//	}
//	else if ( key == Key::Background )
//	{
//	}
//	else if ( key == Key::Borders )
//	{
//	}
}
Rect FrameItem::rect() const
{
	Rect rect = {100,100,1200,800};
	//		// Get Size
	//		QSizeF size = {1200,775};
	//	//	get( size, Key::Size );
	//		// Get Pos
	//		QPointF pos = {150,100}; //Element::getPosition( pos );
	//		Key anchor = Key::TopLeft; //get( anchor, Key::Anchor );
	//	//	QPointF diff = size.point(anchor);
	//		QPointF shift(0,0); //get(shift, Key::Shift);
	//		pos -= /*diff */- shift;
	//		// Set BoundingRect
	//	//	_boundingRect = {pos,size};
	return rect;
}
/**********************************************/
/**********************************************/
void FrameItem::updateGraphItem()
{
//	//	QRectF bounding_rect;
//		// Get Size
//		QSizeF size = {1200,775};
//	//	get( size, Key::Size );
//		// Get Pos
//		QPointF pos = {150,100}; //Element::getPosition( pos );
//		Key anchor = Key::TopLeft; //get( anchor, Key::Anchor );
//	//	QPointF diff = size.point(anchor);
//		QPointF shift(0,0); //get(shift, Key::Shift);
//		pos -= /*diff */- shift;
//		// Set BoundingRect
//	//	_boundingRect = {pos,size};
	bool hide = false; _settings.getBool( hide, Key::Hide );
	// Draw Rect
	if ( !hide )
	{
		QPen pen = QPen(Qt::NoPen);//_settings.getPen(Key::Borders);
		_settings.getPen( pen, Key::Borders );
		QBrush brush = _settings.getBrush(Key::Background);
		if ( _frame == nullptr )
		{
			_frame = new QGraphicsRectItem;
			addToGroup( _frame );
		}
		_frame->setPen( pen );
		_frame->setBrush( brush );
		_frame->setRect( rect() );
	}
	else
	{
		if ( _frame != nullptr )
		{
			removeFromGroup( _frame );
			delete _frame;
			_frame = nullptr;
		}
	}
	emit changed();
}
/**********************************************/
/**********************************************/
