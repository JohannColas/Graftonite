#include "GraphTreeView.h"
/**********************************************/
/**********************************************/
#include <QVBoxLayout>
#include <QLabel>
#include <QTreeView>
#include <QHeaderView>
/**********************************************/
/**********************************************/
#include <QMenu>
#include <QContextMenuEvent>
/**********************************************/
/**********************************************/
#include "GraphModel.h"
/**********************************************/
/**********************************************/
/**********************************************/
/**********************************************/
GraphTreeView::GraphTreeView( QWidget* parent )
    : QWidget(parent)
{
	_label = new QLabel("Graph View");

	_graph_tree = new QTreeView;
	_graph_tree->expandAll();
	_graph_tree->hideColumn( 1 );
	_graph_tree->header()->hide();

	_lay_main = new QVBoxLayout;
	_lay_main->setContentsMargins(0,0,0,0);
	_lay_main->addWidget( _label );
	_lay_main->addWidget( _graph_tree );

	setLayout( _lay_main );


	// Menu
	contextMenu = new QMenu;
	QMenu* newMenu = new QMenu("New", this);
	newMenu->setIcon( QIcon("icons/Dark Theme/new.svg") );
	contextMenu->addMenu(newMenu);

	ac_newframe = new QAction("Frame", this);
	ac_newframe->setIcon( QIcon("icons/Dark Theme/frame.svg") );
	newMenu->addAction( ac_newframe );
	connect( ac_newframe, &QAction::triggered,
			 this, &GraphTreeView::newFrame );
	ac_newaxis = new QAction("Axis", this);
	ac_newaxis->setIcon( QIcon("icons/Dark Theme/axis.svg") );
	newMenu->addAction( ac_newaxis );
	connect( ac_newaxis, &QAction::triggered,
			 this, &GraphTreeView::newAxis );
	ac_newplot = new QAction("Plot", this);
	ac_newplot->setIcon( QIcon("icons/Dark Theme/plot.svg") );
	newMenu->addAction( ac_newplot );
	connect( ac_newplot, &QAction::triggered,
			 this, &GraphTreeView::newPlot );
	ac_newlegend = new QAction("Legend", this);
	ac_newlegend->setIcon( QIcon("icons/Dark Theme/legend.svg") );
	newMenu->addAction( ac_newlegend );
	connect( ac_newlegend, &QAction::triggered,
			 this, &GraphTreeView::newLegend );
	ac_newtitle = new QAction("Title", this);
	ac_newtitle->setIcon( QIcon("icons/Dark Theme/title.svg") );
	newMenu->addAction( ac_newtitle );
	connect( ac_newtitle, &QAction::triggered,
			 this, &GraphTreeView::newTitle );
	ac_newshape = new QAction("Shape", this);
	ac_newshape->setIcon( QIcon("icons/Dark Theme/shapes.svg") );
	newMenu->addAction( ac_newshape );
	connect( ac_newshape, &QAction::triggered,
			 this, &GraphTreeView::newShape );

	contextMenu->addSeparator();

	ac_resetgraph = new QAction("Reset", this);
	ac_resetgraph->setIcon( QIcon("icons/Dark Theme/reset.svg") );
	contextMenu->addAction( ac_resetgraph );
	connect( ac_resetgraph, &QAction::triggered,
			 this, &GraphTreeView::resetGraph );

	contextMenu->addSeparator();

	ac_delete = new QAction("Delete", this);
	ac_delete->setIcon( QIcon("icons/Dark Theme/delete.svg") );
	contextMenu->addAction( ac_delete );
	connect( ac_delete, &QAction::triggered,
			 this, &GraphTreeView::deleteItem );

}
/**********************************************/
/**********************************************/
void GraphTreeView::setModel( GraphModel* model )
{
	_graph_tree->setModel( model );
	model->setItemSelectionModel( _graph_tree->selectionModel() );

	_graph_tree->expandAll();

	connect( this, &GraphTreeView::newFrame,
			 model, &GraphModel::addFrame );
	connect( this, &GraphTreeView::newAxis,
			 model, &GraphModel::addAxis );
	connect( this, &GraphTreeView::newPlot,
			 model, &GraphModel::addPlot );
	connect( this, &GraphTreeView::newLegend,
			 model, &GraphModel::addLegend );
	connect( this, &GraphTreeView::newTitle,
			 model, &GraphModel::addTitle );
	connect( this, &GraphTreeView::newShape,
			 model, &GraphModel::addShape );
	connect( this, &GraphTreeView::resetGraph,
			 model, &GraphModel::resetGraph );
	connect( this, &GraphTreeView::deleteItem,
			 model, &GraphModel::deleteItems );
}
/**********************************************/
/**********************************************/
void GraphTreeView::leaveEvent( QEvent* event )
{

}
/**********************************************/
/**********************************************/
void GraphTreeView::contextMenuEvent( QContextMenuEvent* event )
{
	contextMenu->exec( event->globalPos() );
}
/**********************************************/
/**********************************************/
