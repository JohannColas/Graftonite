#ifndef GRAPHMODEL_H
#define GRAPHMODEL_H
/**********************************************/
#include <QAbstractItemModel>
/**********************************************/
class QItemSelectionModel;
/**********************************************/
#include "../Commons/Key.h"
/**********************************************/
class GraphicsItem;
class GraphItem;
class FrameItem;
class AxisItem;
class PlotItem;
class LegendItem;
class TitleItem;
class ShapeItem;
/**********************************************/
/**********************************************/
/**********************************************/
class GraphModel
        : public QAbstractItemModel
{
	Q_OBJECT
public:
	~GraphModel();
	explicit GraphModel( QObject* parent = nullptr );
	/******************************************/
	//
	QModelIndex index( int row, int column, const QModelIndex& parent_index = QModelIndex() ) const override;
	QModelIndex parent( const QModelIndex& child_index ) const override;
	/******************************************/
	//
	int rowCount( const QModelIndex& parent_index = QModelIndex() ) const override;
	int columnCount( const QModelIndex& parent_index = QModelIndex() ) const override;
	/******************************************/
	bool insertRows( int row, int count, const QModelIndex& parent_index = QModelIndex() ) override;
	bool removeRows( int row, int count, const QModelIndex& parent_index = QModelIndex() ) override;
	bool insertColumns( int column, int count, const QModelIndex& parent_index = QModelIndex() ) override;
	bool removeColumns( int column, int count, const QModelIndex& parent_index = QModelIndex() ) override;
	/******************************************/
	Qt::ItemFlags flags( const QModelIndex& index ) const override;
	/******************************************/
	// Data display methods
	QVariant data( const QModelIndex& index, int role = Qt::DisplayRole ) const override;
	/******************************************/
	// Data modifier methods
	bool setData( const QModelIndex& index, const QVariant& value, int role = Qt::DisplayRole ) override;
	/******************************************/
	void setItemSelectionModel( QItemSelectionModel* selection );
	/******************************************/
private:
	/******************************************/
public slots:
	void openGraph();
	void resetGraph();
	void addFrame();
	void addAxis();
	void addPlot();
	void addLegend();
	void addTitle();
	void addShape();
	void removeFrames();
	void removeAxes();
	void removePlots();
	void removeLegends();
	void removeTitles();
	void removeShapes();
	void deleteItems();
	/******************************************/
private slots:
	void onSelectionChanged( const QModelIndex& index, const QModelIndex& previous );
	/******************************************/
signals:
//	void currentGraphItemChanged( GraphicsItem* item );
	/******************************************/
private:
	GraphicsItem* _current_item = nullptr;
	/******************************************/
	QItemSelectionModel* _selection_model = nullptr;
	QList<GraphicsItem*> _selected_items;
	/******************************************/

	GraphicsItem* _root_item = nullptr;
	GraphItem* _graph_item = nullptr;
	GraphicsItem* _frames_item = nullptr;
	GraphicsItem* _axes_item = nullptr;
	GraphicsItem* _plots_item = nullptr;
	GraphicsItem* _legends_item = nullptr;
	GraphicsItem* _titles_item = nullptr;
	GraphicsItem* _shapes_item = nullptr;

	GraphicsItem* itemAt( const QModelIndex& index ) const;
	QModelIndex indexOf( GraphicsItem* item ) const;

public:
	GraphItem* graphItem();
	int framesCount() const;
	FrameItem* frameItemAt( int index );
	int axesCount() const;
	AxisItem* axisItemAt( int index );

public slots:
	void onCurrentGraphicsItemChanged( GraphicsItem* item );

signals:
	void graphicsItemRemoved( GraphicsItem* item );
	void graphicsItemAdded( GraphicsItem* item );
	void graphicsItemSelected( GraphicsItem* item );
	/******************************************/
	void currentGraphicsItemChanged( GraphicsItem* item );
	/******************************************/
	void itemDataChanged();
	/******************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // GRAPHMODEL_H
