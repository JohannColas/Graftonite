#ifndef SETTINGITEM_H
#define SETTINGITEM_H
/**********************************************/
#include <QObject>
/**********************************************/
#include "../Commons/Key.h"
/**********************************************/
class QLabel;
/**********************************************/
/**********************************************/
/**********************************************/
class SettingItem
        : public QObject
{
	Q_OBJECT
public:
	enum Type {
		Section,
		Widget,
		PushButton,
		CheckBox,
		LineEdit,
		Integer,
		PositiveInteger,
		Double,
		PositiveDouble,
		Date,
		ComboBox,
		Color,
		PenEditor,
		Position,
		Anchor,
		KeySelector,
		TextEdit,
		// AxisType,
		// AxisPosition,
		// AxisFrame,
		// LineDash,
		// LineJoin,
		// LineCap,
		// Anchor,
		// Size,
		// Margins,
		// Spacing, // ??
		// Shift, // ??
		// Graph Format, // ??
		// LinkTo, // ??
		// Layout,
		// FillStyle
		// FontFamily
		// Symbols
		// Alignement
		// Separator
	};
private:
	Key _setting;
	SettingItem::Type _type;
	QLabel* lb_title = nullptr;
	QWidget* wid_modifier = nullptr;
public:
	~SettingItem();
	SettingItem( const Key& setting, const QString& title, const QVariant& value, const SettingItem::Type& type = SettingItem::LineEdit );
	SettingItem( const Key& setting, const QString& title, QWidget* widget );
	SettingItem( const Key& setting, QWidget* widget );
	Key setting() const;
	SettingItem::Type type();
	void setTitle( const QString& title );
	QVariant value() const;
	void setValue( const QVariant& value );
	QLabel* title() const;
	QWidget* modifier() const;
public slots:
	void onEditingFinished();
	void onValueChanged( const QVariant& value );
	void onKeyChanged( const Key& value );
	void onTextChanged( const QString& text );
	void onBoolChanged( bool value );
	void onIntChanged( int value );
	void onDoubleChanged( double value );
	void onColorChanged( const QColor& color );
	void onPenChanged( const QPen& pen );
	void hide();
	void show();
signals:
	void settingChanged( const Key& setting, const QVariant& value );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SETTINGITEM_H
