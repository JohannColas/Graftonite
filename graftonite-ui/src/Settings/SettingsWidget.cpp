#include "SettingsWidget.h"
/**********************************************/
#include <QFormLayout>
#include <QLabel>
#include <QComboBox>
#include "../BasicWidgets/ComboBox2.h"
/**********************************************/
#include "../Commons/Settings.h"
#include "../BasicWidgets/PenWidgets.h"
/**********************************************/
/**********************************************/
/**********************************************/
/**********************************************/
SettingsWidget::~SettingsWidget()
{
	qDeleteAll( _items );
}
SettingsWidget::SettingsWidget( QWidget* parent )
    : QScrollArea(parent)
{
	this->setProperty( "class", "SettingsWidget" );
	this->setWidgetResizable( true );
	wid_cont = new QWidget;
	lay_main = new QFormLayout(wid_cont);
	lay_main->setLabelAlignment( Qt::AlignRight );
	lay_main->setFieldGrowthPolicy( QFormLayout::FieldsStayAtSizeHint );
	lay_main->setSizeConstraint( QLayout::SetMaximumSize );
	wid_cont->setLayout( lay_main );
	this->setWidget(wid_cont);
}
/**********************************************/
/**********************************************/
SettingItem* SettingsWidget::getItem( const Key& setting )
{
	for ( SettingItem* item : _items )
		if ( item->setting() == setting )
			return item;
	return nullptr;
}
/**********************************************/
/**********************************************/
SettingItem* SettingsWidget::addSetting( const Key& setting, const QString& title, const QVariant& value, const SettingItem::Type& type )
{
	SettingItem* item = new SettingItem( setting, title, value, type );
	connect( item, &SettingItem::settingChanged,
			 this, &SettingsWidget::settingChanged );
	_items.append( item );
	if ( type == SettingItem::TextEdit ||
		 type == SettingItem::PenEditor )
		lay_main->addRow( item->modifier() );
	else
		lay_main->addRow( item->title(), item->modifier() );
	return item;
}
void SettingsWidget::addSeparator()
{
	QFrame* line = new QFrame(this);
	line->setFrameShape(QFrame::HLine);
//	line->setFrameShadow(QFrame::Sunken);
	lay_main->addRow( line );
//	_items.append( line );
}
void SettingsWidget::addSection( const QString& title )
{
	QLabel* section = new QLabel;
	section->setProperty("class", "SettingsSection" );
	section->setText( title );
	lay_main->addRow( section );
}
/**********************************************/
/**********************************************/
SettingItem* SettingsWidget::addWidget( const Key& setting, const QString& title, QWidget* widget )
{
	SettingItem* item = new SettingItem( setting, title, widget );
	connect( item, &SettingItem::settingChanged,
			 this, &SettingsWidget::settingChanged );
	_items.append( item );
	lay_main->addRow( item->title(), item->modifier() );
	return item;
}
SettingItem* SettingsWidget::addWidgetRow( const Key& setting, QWidget* widget )
{
	SettingItem* item = new SettingItem( setting, "", widget );
	connect( item, &SettingItem::settingChanged,
			 this, &SettingsWidget::settingChanged );
	_items.append( item );
	lay_main->addRow( item->modifier() );
	return item;
}
/**********************************************/
/**********************************************/
void SettingsWidget::addPushButton( const Key& setting, const QString& title, const QVariant& value )
{
	addSetting( setting, title, value, SettingItem::PushButton );
}
void SettingsWidget::addCheckBox( const Key& setting, const QString& title, const QVariant& value )
{
	addSetting( setting, title, value, SettingItem::CheckBox );
}
void SettingsWidget::addLineEdit( const Key& setting, const QString& title, const QVariant& value )
{
	addSetting( setting, title, value, SettingItem::LineEdit );
}
void SettingsWidget::addIntegerEdit( const Key& setting, const QString& title, const QVariant& value )
{
	addSetting( setting, title, value, SettingItem::Integer );
}
void SettingsWidget::addPositiveIntegerEdit( const Key& setting, const QString& title, const QVariant& value )
{
	addSetting( setting, title, value, SettingItem::PositiveInteger );
}
void SettingsWidget::addDoubleEdit( const Key& setting, const QString& title, const QVariant& value )
{
	addSetting( setting, title, value, SettingItem::Double );
}
void SettingsWidget::addPositiveDoubleEdit( const Key& setting, const QString& title, const QVariant& value )
{
	addSetting( setting, title, value, SettingItem::PositiveDouble );
}
QComboBox* SettingsWidget::addComboBox( const Key& setting, const QString& title, const QVariant& value )
{
	addSetting( setting, title, value, SettingItem::ComboBox );
	SettingItem* item = _items.last();
	return static_cast<QComboBox*>(item->modifier());
}
void SettingsWidget::addColorSelector( const Key& setting, const QString& title, const QVariant& value )
{
	addSetting( setting, title, value, SettingItem::Color );
}
ComboBox2* SettingsWidget::addAnchorSelector( const Key& setting, const QString& title, const QVariant& value )
{
	SettingItem* item = addSetting( setting, title, value, SettingItem::Anchor );
	return static_cast<ComboBox2*>(item->modifier());
}
ComboBox2* SettingsWidget::addKeySelector( const Key& setting, const QString& title, const QVariant& value )
{
	SettingItem* item = addSetting( setting, title, value, SettingItem::KeySelector );
	return static_cast<ComboBox2*>(item->modifier());
}
PenModifier* SettingsWidget::addPenEditor( const Key& setting, const QString& title, const QVariant& value )
{
	SettingItem* item = addSetting( setting, title, value, SettingItem::PenEditor );
	return static_cast<PenModifier*>(item->modifier());
}
/**********************************************/
/**********************************************/
void SettingsWidget::connectSettings( Settings* settings )
{
	if ( _settings != nullptr )
	{
		disconnect( this, &SettingsWidget::settingChanged,
		            _settings, &Settings::changeSetting );
	}
	_settings = settings;
	if ( _settings != nullptr )
	{
		connect( this, &SettingsWidget::settingChanged,
		         _settings, &Settings::changeSetting );
//		updateSettings( _settings );
	}
}
void SettingsWidget::updateSettings( Settings* settings )
{
	for ( SettingItem* item : _items )
		item->setValue( settings->value(item->setting()) );
}
/**********************************************/
/**********************************************/
