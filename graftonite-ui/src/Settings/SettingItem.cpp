#include "SettingItem.h"
/**********************************************/
#include <QCheckBox>
#include <QComboBox>
#include <QDoubleSpinBox>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
// Custom Modifiers
#include "../BasicWidgets/ComboBox2.h"
#include "../BasicWidgets/NumberEdit.h"
#include "../BasicWidgets/ColorSelector.h"
#include "../BasicWidgets/PenWidgets.h"
#include "../BasicWidgets/TextEditor.h"
/**********************************************/
/**********************************************/
/**********************************************/
SettingItem::~SettingItem()
{
	delete lb_title;
	if ( wid_modifier != nullptr )
		delete wid_modifier;
}
SettingItem::SettingItem( const Key& setting, const QString& title, const QVariant& value, const Type& type )
    : QObject()
{
	_setting = setting;
	_type = type;
	lb_title = new QLabel;
	lb_title->setText( title );
	switch (_type)
	{
		case SettingItem::Section:
		{
			break;
		}
		case SettingItem::PushButton:
		{
			QPushButton* pb = new QPushButton;
			pb->setText( value.toString() );
			wid_modifier = pb;
			break;
		}
		case SettingItem::CheckBox:
		{
			QCheckBox* cb = new QCheckBox;
			cb->setChecked( value.toBool() );
			connect( cb, &QCheckBox::toggled,
			         this, &SettingItem::onBoolChanged );
			wid_modifier = cb;
			break;
		}
		case SettingItem::LineEdit:
		{
			QLineEdit* le = new QLineEdit;
			le->setText( value.toString() );
			connect( le, &QLineEdit::editingFinished,
			         this, &SettingItem::onEditingFinished );
			wid_modifier = le;
			break;
		}
		case SettingItem::Integer:
		{
			NumberEdit* sp = new NumberEdit(NumberEdit::Integer);
			sp->setValue( value.toInt() );
			connect( sp, &NumberEdit::intChanged,
			         this, &SettingItem::onIntChanged );
			wid_modifier = sp;
			break;
		}
		case SettingItem::PositiveInteger:
		{
			NumberEdit* sp = new NumberEdit(NumberEdit::PositiveInteger);
			sp->setValue( value.toInt() );
			connect( sp, &NumberEdit::intChanged,
			         this, &SettingItem::onIntChanged );
			wid_modifier = sp;
			break;
		}
		case SettingItem::Double:
		{
			NumberEdit* sp = new NumberEdit(NumberEdit::Double);
			sp->setValue( value.toDouble() );
			connect( sp, &NumberEdit::doubleChanged,
			         this, &SettingItem::onDoubleChanged );
			wid_modifier = sp;
			break;
		}
		case SettingItem::PositiveDouble:
		{
			NumberEdit* sp = new NumberEdit(NumberEdit::PositiveDouble);
			sp->setValue( value.toDouble() );
			connect( sp, &NumberEdit::doubleChanged,
			         this, &SettingItem::onDoubleChanged );
			wid_modifier = sp;
			break;
		}
		case SettingItem::ComboBox:
		{
			QComboBox* cb = new QComboBox;
			cb->setCurrentText( value.toString() );
			connect( cb, &QComboBox::currentTextChanged,
					 this, &SettingItem::onTextChanged );
			wid_modifier = cb;
			break;
		}
		case SettingItem::Anchor:
		{
			ComboBox2* ks = new class ComboBox2;
			ks->setData( {{"Top-Left", Key::TopLeft},
						 {"Top", Key::Top},
						 {"Top-Right", Key::TopRight},
						 {"Left", Key::Left},
						 {"Center", Key::Center},
						 {"Right", Key::Right},
						 {"Bottom-Left", Key::BottomLeft},
						 {"Bottom", Key::Bottom},
						 {"Bottom-Right", Key::BottomRight}} );
			connect( ks, &ComboBox2::valueChanged,
					 this, &SettingItem::onValueChanged );
			wid_modifier = ks;
			break;
		}
		case SettingItem::KeySelector:
		{
			ComboBox2* ks = new ComboBox2;
			connect( ks, &ComboBox2::valueChanged,
					 this, &SettingItem::onValueChanged );
			wid_modifier = ks;
			break;
		}
		case SettingItem::Color:
		{
			ColorSelector* cs = new ColorSelector;
			cs->setColor( value.toString() );
			connect( cs, &ColorSelector::colorChanged,
			         this, &SettingItem::onColorChanged );
			wid_modifier = cs;
			break;
		}
		case SettingItem::PenEditor:
		{
			PenModifier* pen_mod = new PenModifier;
			pen_mod->setPen( value.value<QPen>() );
			connect( pen_mod, &PenModifier::penChanged,
					 this, &SettingItem::onPenChanged );
			wid_modifier = pen_mod;
			break;
		}
		case SettingItem::TextEdit:
		{
			TextEditor* te = new TextEditor;
			te->setText( value.toString() );
			connect( te, &TextEditor::textChanged,
					 this, &SettingItem::onTextChanged );
			wid_modifier = te;
			break;
		}
		default:
		{
			break;
		}
	}
}
SettingItem::SettingItem( const Key& setting, const QString& title, QWidget* widget )
	: QObject()
{
	_setting = setting;
	_type = SettingItem::Widget;
	lb_title = nullptr;
	wid_modifier = widget;
}

SettingItem::SettingItem( const Key& setting, QWidget* widget )
{

}
/**********************************************/
Key SettingItem::setting() const
{
	return _setting;
}
/**********************************************/
SettingItem::Type SettingItem::type()
{
	return _type;
}
/**********************************************/
void SettingItem::setTitle( const QString& title )
{
	lb_title->setText( title );
}
/**********************************************/
QVariant SettingItem::value() const
{
	switch (_type)
	{
		case SettingItem::Section:
		return lb_title->text();
		case SettingItem::PushButton:
		return static_cast<QPushButton*>(wid_modifier)->text();
		case SettingItem::CheckBox:
		return static_cast<QCheckBox*>(wid_modifier)->text();
		case SettingItem::LineEdit:
		return static_cast<QLineEdit*>(wid_modifier)->text();
		case SettingItem::Integer:
		return static_cast<NumberEdit*>(wid_modifier)->text();
		case SettingItem::PositiveInteger:
		return static_cast<NumberEdit*>(wid_modifier)->text();
		case SettingItem::Double:
		return static_cast<NumberEdit*>(wid_modifier)->text();
		case SettingItem::PositiveDouble:
		return static_cast<NumberEdit*>(wid_modifier)->text();
		case SettingItem::ComboBox:
		return static_cast<QComboBox*>(wid_modifier)->currentText();
		case SettingItem::Anchor:
		return static_cast<ComboBox2*>(wid_modifier)->currentValue();
		case SettingItem::KeySelector:
		return static_cast<ComboBox2*>(wid_modifier)->currentValue();
		case SettingItem::Color:
		return static_cast<ColorSelector*>(wid_modifier)->color();
		case SettingItem::PenEditor:
		return static_cast<PenModifier*>(wid_modifier)->pen();
		case SettingItem::TextEdit:
		return static_cast<TextEditor*>(wid_modifier)->text();
		default:
		return QVariant();
	}
}
/**********************************************/
/**********************************************/
void SettingItem::setValue( const QVariant& value )
{
	switch (_type)
	{
		case SettingItem::PushButton:
		{
			static_cast<QPushButton*>(wid_modifier)->setText( value.toString() );
			break;
		}
		case SettingItem::CheckBox:
		{
			static_cast<QCheckBox*>(wid_modifier)->setChecked( value.toBool() );
			break;
		}
		case SettingItem::LineEdit:
		{
			static_cast<QLineEdit*>(wid_modifier)->setText( value.toString() );
			break;
		}
		case SettingItem::Integer:
		{
			static_cast<NumberEdit*>(wid_modifier)->setValue( value.toInt() );
			break;
		}
		case SettingItem::PositiveInteger:
		{
			static_cast<NumberEdit*>(wid_modifier)->setValue( value.toInt() );
			break;
		}
		case SettingItem::Double:
		{
			static_cast<NumberEdit*>(wid_modifier)->setValue( value.toDouble() );
			break;
		}
		case SettingItem::PositiveDouble:
		{
			static_cast<NumberEdit*>(wid_modifier)->setValue( value.toDouble() );
			break;
		}
		case SettingItem::ComboBox:
		{
			static_cast<QComboBox*>(wid_modifier)->setCurrentText( value.toString() );
			break;
		}
		case SettingItem::Anchor:
		{
			static_cast<ComboBox2*>(wid_modifier)->setCurrentValue( value );
			break;
		}
		case SettingItem::KeySelector:
		{
			static_cast<ComboBox2*>(wid_modifier)->setCurrentValue( value );
			break;
		}
		case SettingItem::Color:
		{
			static_cast<ColorSelector*>(wid_modifier)->setColor( value.toString() );
			break;
		}
		case SettingItem::PenEditor:
		{
			static_cast<PenModifier*>(wid_modifier)->setPen( value.value<QPen>() );
			break;
		}
		case SettingItem::TextEdit:
		{
			static_cast<TextEditor*>(wid_modifier)->setText( value.toString() );
			break;
		}
		default:
		break;
	}
}
/**********************************************/
/**********************************************/
QLabel* SettingItem::title() const
{
	return lb_title;
}
/**********************************************/
QWidget* SettingItem::modifier() const
{
	return wid_modifier;
}
/**********************************************/
/**********************************************/
void SettingItem::onEditingFinished()
{
	if ( _type == SettingItem::LineEdit )
	{
		QLineEdit* le = static_cast<QLineEdit*>(wid_modifier);
		emit settingChanged( _setting, le->text() );
	}
}
void SettingItem::onValueChanged( const QVariant& value )
{
	emit settingChanged( _setting, value );
}

void SettingItem::onKeyChanged( const Key& value )
{
	emit settingChanged( _setting, value.toString() );
}
/**********************************************/
void SettingItem::onTextChanged( const QString& text )
{
	emit settingChanged( _setting, text );
}
/**********************************************/
void SettingItem::onBoolChanged( bool value )
{
	emit settingChanged( _setting, value );
}
/**********************************************/
void SettingItem::onIntChanged( int value )
{
	emit settingChanged( _setting, value );
}
/**********************************************/
void SettingItem::onDoubleChanged( double value )
{
	emit settingChanged( _setting, value );
}
/**********************************************/
void SettingItem::onColorChanged( const QColor& color )
{
	emit settingChanged( _setting, color );
}
/**********************************************/
void SettingItem::onPenChanged( const QPen& pen )
{
	emit settingChanged( _setting, pen );
}
/**********************************************/
/**********************************************/
void SettingItem::hide()
{

}
void SettingItem::show()
{

}
/**********************************************/
/**********************************************/
