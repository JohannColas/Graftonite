#ifndef SETTINGSWIDGETS_H
#define SETTINGSWIDGETS_H

#include <QPushButton>
#include <QLabel>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class SettingsButton
		: public QPushButton
{
	Q_OBJECT
public:
	virtual ~SettingsButton ()
	{

	}
	SettingsButton ( QWidget *parent )
		: QPushButton( parent )
	{

	}

};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class SettingsTitle
		: public QLabel
{
	Q_OBJECT
public:
	virtual ~SettingsTitle ()
	{

	}
	SettingsTitle ( QWidget *parent )
		: QLabel( parent )
	{

	}

};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class SettingsSubTitle
		: public QLabel
{
	Q_OBJECT
public:
	virtual ~SettingsSubTitle ()
	{

	}
	SettingsSubTitle ( QWidget *parent )
		: QLabel( parent )
	{

	}

};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SETTINGSWIDGETS_H
