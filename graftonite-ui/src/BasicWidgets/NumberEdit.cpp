#include "NumberEdit.h"
/**********************************************/
#include <QRegularExpressionValidator>
#include <QRegularExpression>
/**********************************************/
/**********************************************/
/**********************************************/
NumberEdit::NumberEdit( QWidget* parent )
    : QLineEdit(parent)
{
	connect( this, &NumberEdit::editingFinished,
	         this, &NumberEdit::onEditingFinished );
}
/**********************************************/
/**********************************************/
NumberEdit::NumberEdit( const Mode& mode, QWidget* parent )
    : QLineEdit(parent)
{
	setMode(mode);
	connect( this, &NumberEdit::editingFinished,
	         this, &NumberEdit::onEditingFinished );
}
/**********************************************/
/**********************************************/
void NumberEdit::setMode( const Mode& mode )
{
	_mode = mode;
	switch (_mode)
	{
		case Integer:
		{
			setValidator( new QRegularExpressionValidator(QRegularExpression("^-?\\d+$"), this) );
			break;
		}
		case PositiveInteger:
		{
			setValidator( new QRegularExpressionValidator(QRegularExpression("\\d+$"), this) );
			break;
		}
		case Double:
		{
			setValidator( new QRegularExpressionValidator(QRegularExpression("^-?\\d*\\.{0,1}\\d+$"), this) );
			break;
		}
		case PositiveDouble:
		{
			setValidator( new QRegularExpressionValidator(QRegularExpression("\\d*\\.{0,1}\\d+$"), this) );
			break;
		}
		case Date:
		{

			break;
		}
		case Time:
		{

			break;
		}
		default:
		{
			break;
		}
	}

//	QRegExp("[1-9][0-9]*")    //  leading digit must be 1 to 9 (prevents leading zeroes).
//	QRegExp("\\d*")           //  allows matching for unicode digits (e.g. for
//	                          //    Arabic-Indic numerals such as ٤٥٦).
//	QRegExp("[0-9]+")         //  input must have at least 1 digit.
//	QRegExp("[0-9]{8,32}")    //  input must be between 8 to 32 digits (e.g. for some basic
//	                          //    password/special-code checks).
//	QRegExp("[0-1]{,4}")      //  matches at most four 0s and 1s.
//	QRegExp("0x[0-9a-fA-F]")  //  matches a hexadecimal number with one hex digit.
//	QRegExp("[0-9]{13}")      //  matches exactly 13 digits (e.g. perhaps for ISBN?).
//	QRegExp("[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}")
//	                          //  matches a format similar to an ip address.
	//	                          //    N.B. invalid addresses can still be entered: "999.999.999.999".

//	Positive Integers:

//	^\d+$

//	Negative Integers:

//	^-\d+$

//	Integer:

//	^-?\d+$

//	Positive Number:

//	^\d*\.?\d+$

//	Negative Number:

//	^-\d*\.?\d+$

//	Positive Number or Negative Number:

//	^-?\d*\.{0,1}\d+$

//	Phone number:

//	^\+?[\d\s]{3,}$

//	Phone with code:

//	^\+?[\d\s]+\(?[\d\s]{10,}$

//	Year 1900-2099:

//	^(19|20)[\d]{2,2}$

//	Date (dd mm yyyy, d/m/yyyy, etc.):

//	^([1-9]|0[1-9]|[12][0-9]|3[01])\D([1-9]|0[1-9]|1[012])\D(19[0-9][0-9]|20[0-9][0-9])$

//	IP v4:

//	^(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]){3}$


}
/**********************************************/
/**********************************************/
void NumberEdit::setValue( int value )
{
	this->setText( QString::number(value) );
}
void NumberEdit::setValue( double value )
{
	this->setText( QString::number(value) );
}
/**********************************************/
/**********************************************/
void NumberEdit::onEditingFinished()
{
	switch (_mode)
	{
		case Integer:
		{
			emit intChanged( this->text().toInt() );
			break;
		}
		case PositiveInteger:
		{
			emit intChanged( this->text().toInt() );
			break;
		}
		case Double:
		{
			emit doubleChanged( this->text().toDouble() );
			break;
		}
		case PositiveDouble:
		{
			emit doubleChanged( this->text().toDouble() );
			break;
		}
		case Date:
		{

			break;
		}
		case Time:
		{

			break;
		}
		default:
		{
			break;
		}
	}
}
/**********************************************/
/**********************************************/
