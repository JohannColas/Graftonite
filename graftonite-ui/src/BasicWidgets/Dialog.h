#ifndef DIALOG_H
#define DIALOG_H

#include <QWidget>



/**
 * @class Dialog
 * @brief
 *
 */
class Dialog
        : public QWidget
{
	Q_OBJECT

public:
	/**
	 * Dialog object constructor
	 *
	 */
	explicit Dialog( QWidget* parent = nullptr );

protected slots:


signals:

};



#endif // DIALOG_H
