#ifndef FORMITEM_H
#define FORMITEM_H
/**********************************************/
#include <QObject>
/**********************************************/
#include "../Commons/Key.h"
/**********************************************/
class QLabel;
class QWidget;
/**********************************************/
/**********************************************/
/**********************************************/
class FormItem
		: public QObject
{
	Q_OBJECT
public:
	enum Type {
		PushButton,
		CheckBox,
		LineEdit,
		SpinBox,
		DoubleSpinBox,
		ComboBox,
	};
	/******************************************/
private:
	QLabel* lb_title = nullptr;
	QWidget* wid_modifier = nullptr;
	Key _setting;
	FormItem::Type _type = FormItem::LineEdit;
	/******************************************/
public:
	~FormItem();
	FormItem( const FormItem::Type type = FormItem::LineEdit, QObject* parent = nullptr );
	QLabel* title();
	QWidget* modifier();
	/******************************************/
private slots:
	void onEditingFinished();
	void onValueChanged( const QString& arg );
	/******************************************/
signals:
	void editingFinished( const QString& arg );
	/******************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // FORMITEM_H
