#include "FormItem.h"
/**********************************************/
#include <QWidget>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QComboBox>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QCheckBox>
/**********************************************/
/**********************************************/
/**********************************************/
FormItem::~FormItem()
{
	delete lb_title;
	delete wid_modifier;
}
/**********************************************/
/**********************************************/
FormItem::FormItem( const Type type, QObject* parent )
	: QObject(parent)
{
	_type = type;
	if ( _type == FormItem::LineEdit )
	{
		QLineEdit* le = new QLineEdit;
		connect( le, &QLineEdit::editingFinished,
				 this, &FormItem::onEditingFinished );
		wid_modifier = le;
	}
	else if ( _type == FormItem::SpinBox )
	{
		QSpinBox* sp = new QSpinBox;
		connect( sp, &QSpinBox::textChanged,
				 this, &FormItem::onEditingFinished );
		wid_modifier = sp;
	}
	else if ( _type == FormItem::DoubleSpinBox )
	{
		QDoubleSpinBox* dsp = new QDoubleSpinBox;
		connect( dsp, &QDoubleSpinBox::textChanged,
				 this, &FormItem::onEditingFinished );
		wid_modifier = dsp;
	}
}
/**********************************************/
/**********************************************/
QLabel* FormItem::title()
{
	return lb_title;
}
/**********************************************/
/**********************************************/
QWidget* FormItem::modifier()
{
	return wid_modifier;
}
/**********************************************/
/**********************************************/
void FormItem::onEditingFinished()
{
	QLineEdit* le = static_cast<QLineEdit*>(wid_modifier);
	emit editingFinished( le->text() );
}
/**********************************************/
/**********************************************/
void FormItem::onValueChanged( const QString& arg )
{
	emit editingFinished( arg );
}
/**********************************************/
/**********************************************/
