#include "Dialog.h"


// Dialog constructor
Dialog::Dialog( QWidget* parent )
    : QWidget(parent)
{
	// Window settings
	setWindowFlags( Qt::Tool
	                | Qt::FramelessWindowHint
	                | Qt::WindowStaysOnTopHint );
	setWindowModality( Qt::WindowModal );

}
