#include "ColorSelector.h"
/**********************************************/
#include <QColorDialog>
#include <QPainter>
/**********************************************/
/**********************************************/
/**********************************************/
ColorSelector::ColorSelector( QWidget* parent )
    : QPushButton(parent)
{
	setFixedWidth(40);
	connect( this, &ColorSelector::released,
	         this, &ColorSelector::onReleasedMouse );
}
/**********************************************/
/**********************************************/
QColor ColorSelector::color() const
{
	return _current_color;
}
void ColorSelector::setColor( const QColor& color )
{
	_current_color = color;
}
void ColorSelector::setColor( const QString& color )
{
	setColor( QColor(color) );
}
/**********************************************/
/**********************************************/
void ColorSelector::onReleasedMouse()
{
	QColorDialog color_diag = QColorDialog(this);
	color_diag.setCurrentColor( _current_color );
	color_diag.setOption( QColorDialog::ShowAlphaChannel, true );
	color_diag.setOption( QColorDialog::DontUseNativeDialog, true );
	color_diag.exec();

	if ( color_diag.selectedColor() != _current_color )
	{
		setColor( color_diag.selectedColor() );
		emit colorChanged( _current_color );
	}
}
/**********************************************/
/**********************************************/
void ColorSelector::paintEvent( QPaintEvent* event )
{
	QPushButton::paintEvent(event);
	int width = this->width() - 10;
	int x = 5;
	int height = this->height() - 10;
	int y = 5;
	QPainter painter(this);
	painter.setBrush(_current_color);
	QPen pen;
	pen.setWidth(0);
	pen.setColor( QColor(0,0,0,0)  );
	painter.setPen( pen );
	painter.drawRect(QRect(x,y,width,height));
}
/**********************************************/
/**********************************************/
