#ifndef WIDGETLISTSELECTOR_H
#define WIDGETLISTSELECTOR_H

#include <QWidget>
#include <QLabel>
#include <QPushButton>
#include <QGridLayout>
#include <QScrollArea>
//#include "Commons/GridLayout.h"
//#include "Commons/ScrollArea.h"

//#include <QDebug>
/**********************************************/
/**********************************************/
/**********************************************/
/* LabelItemList
 *
 * */
class LabelItemList
		: public QLabel
{
	Q_OBJECT
public:
	LabelItemList( const QString& text = "", QWidget* parent = nullptr );
};
/**********************************************/
/**********************************************/
/**********************************************/
/* ButtonlItemList
 *
 * */
class ButtonItemList
		: public QPushButton
{
	Q_OBJECT
private:
	int index = -1;
	/**********************************************/
public:
	ButtonItemList( const QString& text = "", QWidget* parent = nullptr );
	void setSVG( const QString& path );
	int getIndex() const;
	/**********************************************/
public slots:
	void setIndex( int ind );
	void onClicked();
	/**********************************************/
signals:
	void sendIndex( int index );
};
/**********************************************/
/**********************************************/
/**********************************************/
/* WidgetListSelector
 *
 * */
class WidgetListSelector
		: public QWidget
{
	Q_OBJECT
private:
	QScrollArea* list = new QScrollArea;
	QGridLayout* layMain = new QGridLayout;
	QVBoxLayout* layout = new QVBoxLayout;
	QList<QLabel*> _sections;
	QList<ButtonItemList*> _items;
	QList<QWidget*> _widgets;
	int _nbItems = 0;
	int _nbSections = 0;
	int _selectedTab = -1;
	/**********************************************/
public:
	WidgetListSelector( QWidget* parent = nullptr );
	void addItem( const QString& name, QWidget* widget = new QWidget );
	void addSection( const QString& section );
	void setSectionText( int index, const QString& text );
	void setItemText( int index, const QString& text );
	void setItemIcon( int index, const QIcon& icon );
	void setWidget( int index, QWidget* widget );
	/**********************************************/
public slots:
	void changeCurrentWidget( int index );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // WIDGETLISTSELECTOR_H
