#ifndef DATAEDITOR_H
#define DATAEDITOR_H
/**********************************************/
#include <QWidget>
class QSplitter;
class QHBoxLayout;
class QGridLayout;
class QTabWidget;
class QTableView;
class TextEdit;
/**********************************************/
class SettingsWidget;
/**********************************************/
#include "DataSheetModel.h"
/**********************************************/
/**********************************************/
/**********************************************/
class DataEditor
        : public QWidget
{
	Q_OBJECT
public:
	~DataEditor();
	explicit DataEditor( QWidget *parent = nullptr );
	/******************************************/
	void setDataPath( const QString& path );
	/******************************************/
private slots:
	void updateEditor();
	void on_pb_Save_released();
	void on_pb_SaveAsTemplate_released();
	void on_pb_oriData_released();
	/******************************************/
private:
	DataSheetModel* _datasheet_model = nullptr;

	QHBoxLayout* lay_main = nullptr;

	QSplitter* splitter = nullptr;

	QTabWidget* tab_main = nullptr;

	QTableView* table_data = nullptr;
	TextEdit* txe_source_file = nullptr;
	QWidget* wid_analysis = nullptr;

	QWidget* wid_sidebar = nullptr;
	QGridLayout* lay_sidebar = nullptr;

	SettingsWidget* wid_settings = nullptr;
	/******************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // DATAEDITOR_H
