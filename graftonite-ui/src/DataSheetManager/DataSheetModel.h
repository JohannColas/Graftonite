#ifndef DATASHEETMODEL_H
#define DATASHEETMODEL_H
/**********************************************/
#include <QAbstractTableModel>
#include <QItemSelection>
/**********************************************/
class DataSheetItem;
class QItemSelectionModel;
/**********************************************/
#include "../Commons/Settings.h"
/**********************************************/
/**********************************************/
/**********************************************/
class DataSheetModel
        : public QAbstractTableModel
{
	Q_OBJECT
public:
	~DataSheetModel();
	explicit DataSheetModel( QObject* parent = nullptr );
	/******************************************/
	QModelIndex index( int row, int column, const QModelIndex& parent_index = QModelIndex() ) const override;
	int rowCount( const QModelIndex& parent_index = QModelIndex() ) const override;
	int columnCount( const QModelIndex& parent_index = QModelIndex() ) const override;
	/******************************************/
	Qt::ItemFlags flags( const QModelIndex& index ) const override;
	/******************************************/
	bool setHeaderData( int section, Qt::Orientation orientation, const QVariant& value, int role = Qt::EditRole ) override;
	QVariant headerData( int section, Qt::Orientation orientation, int role = Qt::DisplayRole ) const override;
	/******************************************/
	QVariant data( const QModelIndex& index, int role = Qt::DisplayRole ) const override;
	bool setData( const QModelIndex& index, const QVariant& value, int role = Qt::DisplayRole ) override;
	/******************************************/
	void setSelectionModel( QItemSelectionModel* selection_model );
	/******************************************/
	void insertRow();
	void insertColumn();
	void deleteRows();
	void deleteColumns();
	/******************************************/
	void setPath( const QString& path );
	/******************************************/
	void readSettingsFromPath()
	{

	}
	/******************************************/
private slots:
	void onCurrentRowChanged( const QModelIndex& index, const QModelIndex& previous );
	void onCurrentColumnChanged( const QModelIndex& index, const QModelIndex& previous );
	void onSelectionChanged( const QItemSelection& selected, const QItemSelection& deselected );
	/******************************************/
private:
	DataSheetItem* _root_item = nullptr;
	QStringList header = {"Names", "Units", "Comments"};
	/******************************************/
	QItemSelectionModel* _selection_model = nullptr;
	/******************************************/
	QString _path;
	Settings _settings;
	/******************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // DATASHEETMODEL_H
