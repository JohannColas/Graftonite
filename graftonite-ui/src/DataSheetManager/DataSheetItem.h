#ifndef DATASHEETITEM_H
#define DATASHEETITEM_H
/**********************************************/
#include <QVariant>
/**********************************************/
/**********************************************/
/**********************************************/
class DataSheetItem
{
public:
	enum Type
	{
		Data,
		Header
	};
	/******************************************/
	~DataSheetItem();
	DataSheetItem( DataSheetItem* parent = nullptr );
	DataSheetItem( int row, int column, DataSheetItem* parent = nullptr );
	/******************************************/
	DataSheetItem* insertChild( int row, int column );
	DataSheetItem* childAt( int row, int column );
	void setData( const QVariant& data );
	QVariant data() const;
	void clear();
	void setType( const Type& type );
	Type type() const;
	/******************************************/
	int row() const;
	int column() const;
	/******************************************/
private:
	Type _type = Data;
	QVariant _data;
	/******************************************/
	int _row = 0;
	int _column = 0;
	/******************************************/
	DataSheetItem* _parent_item = nullptr;
	QHash<QPair<int,int>,DataSheetItem*> _child_items;
	/******************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // DATASHEETITEM_H
