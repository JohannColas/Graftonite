#include "DataSheetItem.h"
/**********************************************/
/**********************************************/
/**********************************************/
/**********************************************/
DataSheetItem::~DataSheetItem()
{

}
/**********************************************/
DataSheetItem::DataSheetItem( DataSheetItem* parent )
{
	_parent_item = parent;
}
/**********************************************/
DataSheetItem::DataSheetItem( int row, int column, DataSheetItem* parent )
{
	_parent_item = parent;
	_row = row;
	_column = column;
	_data = QString("{%1,%2}").arg(row).arg(column);
}
/**********************************************/
/**********************************************/
DataSheetItem* DataSheetItem::insertChild( int row, int column )
{
	DataSheetItem* child = new DataSheetItem( row, column, this );
	_child_items.insert( {row,column}, child );
	return child;
}
/**********************************************/
DataSheetItem* DataSheetItem::childAt( int row, int column )
{
	QHash<QPair<int,int>,DataSheetItem*>::iterator it = _child_items.find( {row,column} );
	if ( it != _child_items.end() )
		return it.value();
	return nullptr;
}
/**********************************************/
/**********************************************/
void DataSheetItem::setData( const QVariant& data )
{
	_data = data;
}
/**********************************************/
QVariant DataSheetItem::data() const
{
	return _data;
}
/**********************************************/
void DataSheetItem::clear()
{
	for ( DataSheetItem* item : _child_items )
	{
		item->clear();
		if ( item != nullptr )
			delete item;
	}
	_child_items.clear();
}
/**********************************************/
void DataSheetItem::setType(const Type& type)
{
	_type = type;
}
/**********************************************/
DataSheetItem::Type DataSheetItem::type() const
{
	return _type;
}
/**********************************************/
/**********************************************/
int DataSheetItem::row() const
{
	return _row;
}
/**********************************************/
int DataSheetItem::column() const
{
	return _column;
}
/**********************************************/
/**********************************************/
