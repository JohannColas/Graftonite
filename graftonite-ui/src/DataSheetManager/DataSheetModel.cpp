#include "DataSheetModel.h"

#include <QBrush>
#include <QItemSelectionModel>
/**********************************************/
#include "DataSheetItem.h"
/**********************************************/
/**********************************************/
/**********************************************/
DataSheetModel::~DataSheetModel()
{
	_root_item->clear();
	delete _root_item;
}
/**********************************************/
DataSheetModel::DataSheetModel( QObject* parent )
    : QAbstractTableModel(parent)
{
	_root_item = new DataSheetItem;
}
/**********************************************/
/**********************************************/
QModelIndex DataSheetModel::index( int row, int column, const QModelIndex& parent_index ) const
{
	if ( !hasIndex(row, column, parent_index) )
		return QModelIndex();

	DataSheetItem* parent_item = nullptr;
	if ( !parent_index.isValid() )
		parent_item = _root_item;
	else
		parent_item = static_cast<DataSheetItem*>(parent_index.internalPointer());

	DataSheetItem* child_item = parent_item->childAt( row, column );
	if ( child_item == nullptr )
	{
		child_item = parent_item->insertChild( row, column );
	}
	return createIndex(row, column, child_item);
}
/**********************************************/
int DataSheetModel::rowCount( const QModelIndex& parent_index ) const
{
	Q_UNUSED(parent_index)
	return 25;
}
/**********************************************/
int DataSheetModel::columnCount( const QModelIndex& parent_index ) const
{
	Q_UNUSED(parent_index)
	return 6;
}
/**********************************************/
/**********************************************/
Qt::ItemFlags DataSheetModel::flags( const QModelIndex& index ) const
{
	if ( !index.isValid() )
		return Qt::NoItemFlags;

	Qt::ItemFlags flags = QAbstractTableModel::flags(index);

	flags = flags | Qt::ItemIsEditable;

	return flags;
}
/**********************************************/
/**********************************************/
bool DataSheetModel::setHeaderData( int section, Qt::Orientation orientation, const QVariant& value, int role )
{
	return true;
}
/**********************************************/
QVariant DataSheetModel::headerData( int section, Qt::Orientation orientation, int role ) const
{
	if ( role == Qt::DisplayRole || role == Qt::EditRole )
	{
		if ( orientation == Qt::Vertical )
		{
			if ( section < header.size() )
				return header.at(section);
			return QString::number(section-header.size());
		}
		else
		{
			return QChar(section+'A');
		}
	}
	else if ( role == Qt::TextAlignmentRole )
	{
		if ( orientation == Qt::Vertical )
			return QVariant(Qt::AlignRight | Qt::AlignVCenter);
		else
			return Qt::AlignCenter;
	}
	return QVariant();
}
/**********************************************/
/**********************************************/
QVariant DataSheetModel::data( const QModelIndex& index, int role ) const
{
	if ( !index.isValid() )
		return QVariant();
	if ( index.row() >= rowCount() || index.row() < 0 ||
	     index.column() >= columnCount() || index.column() < 0 )
		return QVariant();
	if ( role == Qt::DisplayRole|| role == Qt::EditRole )
	{
		DataSheetItem* item = static_cast<DataSheetItem*>(index.internalPointer());
		if ( item == nullptr )
			return QVariant();
		return item->data();
	}
	else if ( index.row() < header.size() && role == Qt::BackgroundRole )
	{
		return QBrush(QColor(22,22,22));
	}
	else if ( role == Qt::TextAlignmentRole )
	{
		return Qt::AlignCenter;
	}
	return QVariant();
}
/**********************************************/
bool DataSheetModel::setData( const QModelIndex& index, const QVariant& value, int role )
{
	if ( !index.isValid() )
		return false;
	if ( index.row() >= rowCount() || index.row() < 0 ||
	     index.column() >= columnCount() || index.column() < 0 )
		return false;
	if ( role == Qt::EditRole )
	{
		DataSheetItem* item = static_cast<DataSheetItem*>(index.internalPointer());
		if ( item == nullptr )
			return false;
		item->setData(value);
		return true;
	}
	return false;
}
/**********************************************/
/**********************************************/
void DataSheetModel::setSelectionModel( QItemSelectionModel* selection_model )
{
	_selection_model = selection_model;
	connect( _selection_model, &QItemSelectionModel::currentRowChanged,
	         this, &DataSheetModel::onCurrentRowChanged );
	connect( _selection_model, &QItemSelectionModel::currentColumnChanged,
	         this, &DataSheetModel::onCurrentColumnChanged );
	connect( _selection_model, &QItemSelectionModel::selectionChanged,
	         this, &DataSheetModel::onSelectionChanged );
}
/**********************************************/
/**********************************************/
void DataSheetModel::insertRow()
{

}
/**********************************************/
void DataSheetModel::insertColumn()
{

}
/**********************************************/
void DataSheetModel::deleteRows()
{

}
/**********************************************/
void DataSheetModel::deleteColumns()
{

}
/**********************************************/
void DataSheetModel::setPath( const QString& path )
{
	_path = path;
}
/**********************************************/
/**********************************************/
void DataSheetModel::onCurrentRowChanged( const QModelIndex& index, const QModelIndex& previous )
{

}
/**********************************************/
void DataSheetModel::onCurrentColumnChanged( const QModelIndex& index, const QModelIndex& previous )
{
}
/**********************************************/
void DataSheetModel::onSelectionChanged( const QItemSelection& selected, const QItemSelection& deselected )
{
//	QVector<QModelIndex> lst;
//	for ( QModelIndex index : selected.indexes() )
//		if ( index.row() < header.size() )
//			lst.append( index );
//	_selection_model->blockSignals(true);
//	for ( QModelIndex index : lst )
//		_selection_model->setCurrentIndex(index, QItemSelectionModel::Deselect);
////	_selection_model->setCurrentIndex( _selection_model->selectedIndexes().first(), QItemSelectionModel::SelectCurrent );
//	_selection_model->blockSignals(false);

//	qDebug() <<  _selection_model->selectedIndexes();
}
/**********************************************/
/**********************************************/
