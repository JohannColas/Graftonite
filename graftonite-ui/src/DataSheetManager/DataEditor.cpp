#include "DataEditor.h"
/**********************************************/
#include "DataSheetModel.h"
/**********************************************/
#include <QHBoxLayout>
#include <QSplitter>
#include <QTabWidget>
#include <QGridLayout>
#include <QTableView>
#include "../BasicWidgets/TextEdit.h"
#include "../Settings/SettingsWidget.h"
/**********************************************/
#include "../Commons/Files.h"
/**********************************************/
/**********************************************/
DataEditor::DataEditor( QWidget* parent ) :
    QWidget(parent)
{
	_datasheet_model = new DataSheetModel;

	table_data = new QTableView;
	table_data->setModel( _datasheet_model );
	_datasheet_model->setSelectionModel( table_data->selectionModel() );
	table_data->setVerticalScrollMode( QAbstractItemView::ScrollMode::ScrollPerPixel );
	table_data->setHorizontalScrollMode( QAbstractItemView::ScrollMode::ScrollPerPixel );

	txe_source_file = new TextEdit;
	wid_analysis = new QWidget;

	tab_main = new QTabWidget;
	tab_main->addTab( table_data, "Data" );
	tab_main->addTab( txe_source_file, "Source File" );
	tab_main->addTab( wid_analysis, "Analysis" );

	lay_sidebar = new QGridLayout;
	lay_sidebar->setContentsMargins(0,0,0,0);
	wid_sidebar = new QWidget;
	wid_sidebar->setLayout( lay_sidebar );

	wid_settings = new SettingsWidget;
	lay_sidebar->addWidget( wid_settings );

	splitter = new QSplitter;
	splitter->addWidget( tab_main );
	splitter->addWidget( wid_sidebar );

	lay_main = new QHBoxLayout;
	lay_main->addWidget( splitter );
	this->setLayout( lay_main );
}
/**********************************************/
/**********************************************/
DataEditor::~DataEditor()
{
}
/**********************************************/
/**********************************************/
void DataEditor::setDataPath( const QString& path )
{
//	_data.setPath( path );
	updateEditor();
}
/**********************************************/
/**********************************************/
void DataEditor::updateEditor()
{
//	_data.treat();
	QString content;
//	Files::readFile( _data.dataPath(), content );
	txe_source_file->setPlainText( content );
//	ui->table_data->clear();
//	ui->table_data->setRowCount( _data.rowCount() );
//	ui->table_data->setColumnCount( _data.columnCount() );
//	for ( int it = 0; it < _data.data().size(); ++it )
//	{
//		QVector<QString> dataline = _data.data().at(it);
//		for ( int jt = 0; jt < dataline.size(); ++jt )
//		{
//			QTableWidgetItem* newItem = new QTableWidgetItem( dataline.at(jt) );
//			newItem->setTextAlignment( Qt::AlignCenter );
//			ui->table_data->setItem( it, jt, newItem );
//		}
//	}
}
/**********************************************/
/**********************************************/
void DataEditor::on_pb_Save_released()
{
//	_data.saveXML();
}
/**********************************************/
/**********************************************/
void DataEditor::on_pb_SaveAsTemplate_released()
{

}
/**********************************************/
/**********************************************/
void DataEditor::on_pb_oriData_released()
{
//	if ( ui->stack_dataView->currentIndex() == 0 )
//	{
//		ui->pb_oriData->setText( "Hide Original Data" );
//		ui->stack_dataView->setCurrentIndex(1);
//	}
//	else
//	{
//		ui->pb_oriData->setText( "Show Original Data" );
//		ui->stack_dataView->setCurrentIndex(0);
//	}
}
/**********************************************/
/**********************************************/
