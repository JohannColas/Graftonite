#ifndef DATA_H
#define DATA_H
/**********************************************/
#include <QObject>
#include <QDomDocument>
/**********************************************/
/**********************************************/
class QDomElement;
/**********************************************/
/**********************************************/
/**********************************************/
class Data
		: public QObject
{
	Q_OBJECT
	QString _id;
	QString _path;
	QDomDocument _dataDoc;
	QString _dataPath;
	QVector<QVector<QString>> _data;
	int _rowCount = 0;
	int _columnCount = 0;
	/**********************************************/
	QString _separator = "\t";
	QString _decimal = ";";
	bool _skipEmptyParts = true;
	QString _rowToTreat;
	/**********************************************/
public:
	QString separator() const { return _separator; }
	void setSeparator( const QString& sep ) { _separator = sep; emit changed(); }
	QString decimal() const { return _decimal; }
	void setDecimal( const QString& decimal ) { _decimal = decimal; emit changed(); }
	bool skipEmptyParts() const { return _skipEmptyParts; }
	void setSkipEmptyParts( bool skipEmptyParts ) { _skipEmptyParts = skipEmptyParts; emit changed(); }
	QString rowToTreat() const { return _rowToTreat; }
	void setRowToTreat( const QString& rowToTreat ) { _rowToTreat = rowToTreat; emit changed(); }
	/**********************************************/
public:
	Data();
	void setID( const QString& id ) { _id = id; emit changed(); };
	QString id() { return _id; }
	void setPath( const QString& path );
	QString dataPath() { return _dataPath; }
	void setDataPath( const QString& dataPath );
	QVector<QVector<QString>> data() { return _data; }
	int rowCount() { return _rowCount; }
	int columnCount() { return _columnCount; }
	/**********************************************/
	void clear();
	void init();
//	void set( const QDomElement& sets );
//	QDomElement save();
	void saveXML();
	/**********************************************/
	void treat();
	void post_treat();
	/**********************************************/
signals:
	void changed();
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // DATA_H
