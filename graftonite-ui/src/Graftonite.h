/*
 * This file is part of Graftonite Project.
 *
 * See the COPYRIGHT file at the top-level directory of this distribution
 * for details of code ownership.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
/**
 * @file Graftonite.h
 * @brief Programme de tests.
 * @author Johann C.
 * @version
 * @date
 *
 * Description
 *
 */
#ifndef GRAFTONITE_H
#define GRAFTONITE_H

// Qt dependencies
#include <QWidget>

// Qt classes definitions
class QVBoxLayout;
class QSplitter;
class QStackedWidget;
class QPushButton;

// Project classes definitions
class ProjectModel;
class ProjectView;
class ProjectItem;

// Frame classes definitions
class WelcomeFrame;
class SettingsFrame;
class DataEditor;
class GraphEditor;
class ImageViewer;


/**
 * @class Graftonite
 * @brief Main window
 *
 * Grafonite is the main window of the program.
 * This class centralize all the data models and views.
 *
 */
class Graftonite
        : public QWidget
{
	Q_OBJECT

public:
	/**
	 * Graftonite object destructor
	 *
	 */
	~Graftonite();

	/**
	 * Graftonite object constructor
	 *
	 * @param parent
	 *
	 */
	explicit Graftonite( QWidget* parent = nullptr );

private slots:

	/**
	 * @fn
	 * Show the welcome page
	 *
	 */
	void on_pb_welcome_released();

	/**
	 * @fn
	 * Show the settings page
	 *
	 */
	void on_pb_settings_released();

	/**
	 * @fn
	 * Manager the behaviour of the sidebar
	 * When pos < 200, the project tree is hidden, as well as button labels
	 *
	 */
	void onSplitterMoved( int pos, int index );

	/**
	 * @fn
	 * Show the page corresponding of the type of item, and transmit the item to the page (editor)
	 *
	 * @param item
	 *
	 */
	void onCurrentProjectItemChanged( ProjectItem* item );

	/**
	 * @fn
	 * Save the data when the window is close
	 *
	 */
	void closeEvent( QCloseEvent* event ) override;

private:
	QVBoxLayout* _layout = nullptr; ///< Main layout
	QSplitter* splitter = nullptr; ///< Split Sidebar and Content Frame

	// SideBar button
	QWidget* wid_sidebar = nullptr; ///< QWidget which contains the sidebar elements
	bool _sidebar_is_collapsed = false; ///< ###
	int _save_sidebar_width = 200; ///< ###
	QPushButton* pb_welcome = nullptr; ///< QPushButton which shows WelcomeFrame on left button release
	QPushButton* pb_project_explorer = nullptr; ///< QPushButton with no function, yet
	QPushButton* pb_settings = nullptr; ///< QPushButton which shows AppSettingsFrame on left button release

	//
	ProjectModel* _projects_model = nullptr; ///< Manage projects
	ProjectView* _projects_view = nullptr; ///< View for @see ProjectModel()

	//
	QStackedWidget* cont_main = nullptr; ///< Contains all frame widget
	WelcomeFrame* wid_welcome = nullptr; ///< Shows the welcome page
	SettingsFrame* wid_settings = nullptr; ///< Shows the app settings page
	DataEditor* wid_data = nullptr; ///< QWidget which permits to edit @see DataModel()
	GraphEditor* wid_graph = nullptr; ///< QWidget which permits to edit @see GraphModel()
	ImageViewer* wid_image = nullptr; ///< QWidget which permits to edit @see ImageModel()
	QWidget* wid_empty = nullptr;

};



#endif // GRAFTONITE_H
