#include "ProjectView.h"

#include "ProjectModel.h"

#include <QContextMenuEvent>
#include <QMenu>
#include <QMimeData>
#include <QVBoxLayout>


#include <QProxyStyle>

class myViewStyle: public QProxyStyle{
public:
	myViewStyle(QStyle* style = 0);

	void drawPrimitive ( PrimitiveElement element, const QStyleOption * option, QPainter * painter, const QWidget * widget = 0 ) const;
};

myViewStyle::myViewStyle(QStyle* style)
     :QProxyStyle(style)
{}

void myViewStyle::drawPrimitive ( PrimitiveElement element, const QStyleOption * option, QPainter * painter, const QWidget * widget) const{
	if (element == QStyle::PE_IndicatorItemViewItemDrop && !option->rect.isNull()){
		QStyleOption opt(*option);
		opt.rect.setLeft(0);
		if (widget) opt.rect.setRight(widget->width());
		QProxyStyle::drawPrimitive(element, &opt, painter, widget);
		return;
	}
	QProxyStyle::drawPrimitive(element, option, painter, widget);
}




ProjectView::~ProjectView()
{
	delete lay_cont;
	delete wid_cont;
	delete lay_main;
	delete contextMenu;
	delete ac_newproject;
}

ProjectView::ProjectView(QWidget* parent)
    : QWidget(parent)
{
	this->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Expanding );


	_tree_view = new QTreeView;
	_tree_view->setSelectionMode(QAbstractItemView::ExtendedSelection);
	_tree_view->viewport()->setAcceptDrops(true);
	_tree_view->setAcceptDrops(true);
	_tree_view->setDragEnabled(true);
//	_tree_view->setDropIndicatorShown(true);
//	_tree_view->setDragDropMode(QAbstractItemView::DragDropMode::DragDrop);
	_tree_view->setStyle(new myViewStyle(style()));
	_tree_view->setVerticalScrollMode( QAbstractItemView::ScrollPerPixel );
	_tree_view->setHorizontalScrollMode( QAbstractItemView::ScrollPerPixel );
	_tree_view->setHeaderHidden(true);

	lay_cont = new QVBoxLayout;
	lay_cont->addWidget( _tree_view );
	wid_cont = new ProjectWidget;
	wid_cont->setLayout( lay_cont );

	lay_main = new QVBoxLayout;
	lay_main->setContentsMargins(0,0,0,0);
	lay_main->addWidget( wid_cont );
	setLayout( lay_main );


	// Menu
	contextMenu = new QMenu;
	QMenu* newMenu = new QMenu("New", this);
	newMenu->setIcon( QIcon("icons/Dark Theme/new.svg") );
	contextMenu->addMenu(newMenu);

	ac_newproject = new QAction("Project", this);
	ac_newproject->setIcon( QIcon("icons/Dark Theme/graftonite.svg") );
	newMenu->addAction( ac_newproject );
	ac_newfolder = new QAction("Folder", this);
	ac_newfolder->setIcon( QIcon("icons/Dark Theme/folder.svg") );
	newMenu->addAction( ac_newfolder );
	ac_newdata = new QAction("Data Sheet", this);
	ac_newdata->setIcon( QIcon("icons/Dark Theme/data.svg") );
	newMenu->addAction( ac_newdata );
	ac_newgraph = new QAction("Graph", this);
	ac_newgraph->setIcon( QIcon("icons/Dark Theme/graph.svg") );
	newMenu->addAction( ac_newgraph );
	ac_newimage = new QAction("Image", this);
	ac_newimage->setIcon( QIcon("icons/Dark Theme/image.svg") );
	newMenu->addAction( ac_newimage );
	ac_newtextfile = new QAction("Text File", this);
	ac_newtextfile->setIcon( QIcon("icons/Dark Theme/textfile.svg") );
	newMenu->addAction( ac_newtextfile );
	ac_newfile = new QAction("Other File", this);
	ac_newfile->setIcon( QIcon("icons/Dark Theme/file.svg") );
	newMenu->addAction( ac_newfile );
	ac_fromtemplate = new QAction("From Template", this);
	ac_fromtemplate->setIcon( QIcon("icons/Dark Theme/template.svg") );
	newMenu->addAction( ac_fromtemplate );

	ac_open = new QAction("Open", this);
	ac_open->setIcon( QIcon("icons/Dark Theme/open.svg") );
	contextMenu->addAction( ac_open );

	ac_load = new QAction("Load", this);
	ac_load->setIcon( QIcon("icons/Dark Theme/load.svg") );
	contextMenu->addAction( ac_load );

	contextMenu->addSeparator();

	ac_delete = new QAction("Delete", this);
	ac_delete->setIcon( QIcon("icons/Dark Theme/delete.svg") );
	contextMenu->addAction( ac_delete );

	contextMenu->addSeparator();

	ac_save = new QAction("Save", this);
	ac_save->setIcon( QIcon("icons/Dark Theme/save.svg") );
	contextMenu->addAction( ac_save );
	ac_export = new QAction("Export", this);
	ac_export->setIcon( QIcon("icons/Dark Theme/export.svg") );
	contextMenu->addAction( ac_export );
	ac_close = new QAction("Close", this);
	ac_close->setIcon( QIcon("icons/Dark Theme/close.svg") );
	contextMenu->addAction( ac_close );

}

void ProjectView::setModel( QAbstractItemModel* model )
{
	_model = static_cast<ProjectModel*>(model);
	_tree_view->setModel(_model);
	_model->setItemSelectionModel( _tree_view->selectionModel() );

	connect( ac_newproject, &QAction::triggered,
	         _model, &ProjectModel::createProject );
	connect( ac_newfolder, &QAction::triggered,
	         _model, &ProjectModel::addFolder );
	connect( ac_newdata, &QAction::triggered,
	         _model, &ProjectModel::addDataSheet );
	connect( ac_newgraph, &QAction::triggered,
	         _model, &ProjectModel::addGraph );
	connect( ac_newimage, &QAction::triggered,
	         _model, &ProjectModel::addImage );
	connect( ac_open, &QAction::triggered,
	         _model, &ProjectModel::openProject );
	//	connect( ac_load, &QAction::triggered,
	//	         _model, &ProjectModel::loadProject );
	connect( ac_delete, &QAction::triggered,
	         _model, &ProjectModel::removeFile );
	connect( ac_save, &QAction::triggered,
	         _model, &ProjectModel::saveProject );
	connect( ac_export, &QAction::triggered,
	         _model, &ProjectModel::exportFile );
	connect( ac_close, &QAction::triggered,
	         _model, &ProjectModel::closeProject );
}

void ProjectView::setAbleToHide( bool abletohide )
{
	_abletohide = abletohide;
}

void ProjectView::contextMenuEvent( QContextMenuEvent* event )
{
	contextMenu->exec( event->globalPos() );
}

void ProjectView::enterEvent( QEnterEvent* event )
{
	if ( _tree_to_hide )
	{
		wid_cont->show();
		wid_cont->move( this->mapToGlobal( QPoint(0,0) ) );
		wid_cont->setFixedHeight( this->height() );
	}

	QWidget::enterEvent( event );
}

void ProjectView::leaveEvent( QEvent* event )
{
	if ( _tree_to_hide &&
	     !wid_cont->geometry().contains(QCursor::pos()) )
	{
		wid_cont->hide();
	}

	QWidget::leaveEvent( event );
}

void ProjectView::resizeEvent( QResizeEvent* event )
{
	if ( _abletohide )
	{
		_tree_to_hide = true;
		lay_main->removeWidget( wid_cont );
		wid_cont->setWindowFlag( Qt::ToolTip, true );
		wid_cont->hide();
		connect( wid_cont, &ProjectWidget::leaved,
		         wid_cont, &ProjectWidget::hide );
	}
	else
	{
		_tree_to_hide = false;
		lay_main->addWidget( wid_cont );
		wid_cont->setWindowFlag( Qt::ToolTip, false );
		wid_cont->setFixedHeight(QWIDGETSIZE_MAX);
		wid_cont->show();
		disconnect( wid_cont, &ProjectWidget::leaved,
		            wid_cont, &ProjectWidget::hide );

	}

	QWidget::resizeEvent( event );
}

ProjectWidget::ProjectWidget( QWidget* parent )
    : QWidget(parent)
{

}

void ProjectWidget::leaveEvent( QEvent* event )
{
	emit leaved();
	QWidget::leaveEvent( event );
}

//ProjectView::ProjectTreeView::ProjectTreeView( QWidget* parent )
//    : QTreeView(parent)
//{

//}

//void ProjectView::ProjectTreeView::dragEnterEvent( QDragEnterEvent* event )
//{

//}

//void ProjectView::ProjectTreeView::dragLeaveEvent( QDragLeaveEvent* event )
//{

//}

//void ProjectView::ProjectTreeView::dragMoveEvent( QDragMoveEvent* event )
//{

//}
