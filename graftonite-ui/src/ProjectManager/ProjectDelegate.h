#ifndef PROJECTDELEGATE2_H
#define PROJECTDELEGATE2_H

#include <QStyledItemDelegate>

class ProjectDelegate2 : public QStyledItemDelegate
{
	Q_OBJECT
public:
	ProjectDelegate2 ( QObject *parent = nullptr );
	//
	QWidget* createEditor ( QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index ) const override;
	void setEditorData ( QWidget* editor, const QModelIndex& index ) const override;
	void setModelData ( QWidget* editor, QAbstractItemModel* model, const QModelIndex& index ) const override;
	//
	void paint ( QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index ) const override;
	void updateEditorGeometry ( QWidget* editor, const QStyleOptionViewItem& option, const QModelIndex& index ) const override;
};

#endif // PROJECTDELEGATE_H
