#ifndef PROJECTVIEW_H
#define PROJECTVIEW_H
/**********************************************/
#include <QTreeView>
/**********************************************/
class ProjectModel;
class QVBoxLayout;
class ProjectWidget;
/**********************************************/
/**********************************************/
/**********************************************/
class ProjectView
        : public QWidget
{
	Q_OBJECT
public:
	~ProjectView();
	ProjectView( QWidget* parent = nullptr );
	/******************************************/
	void setModel( QAbstractItemModel* model );
	/******************************************/
	void setAbleToHide( bool abletohide );
	/******************************************/
private slots:
	void contextMenuEvent( QContextMenuEvent* event ) override;
	void enterEvent( QEnterEvent* event ) override;
	void leaveEvent( QEvent* event ) override;
	void resizeEvent( QResizeEvent* event ) override;
	/******************************************/
	// Drag&Drop Methods
//	void dragEnterEvent( QDragEnterEvent* event ) override;
//	void dragLeaveEvent( QDragLeaveEvent* event ) override;
//	void dragMoveEvent( QDragMoveEvent* event ) override;
//	void dropEvent( QDropEvent* event ) override;
	/******************************************/
private:
//	class ProjectTreeView
//	        : public QTreeView
//	{
//	public:
//		ProjectTreeView( QWidget* parent = nullptr );
//	private slots:
//		// Drag&Drop Methods
//		void dragEnterEvent( QDragEnterEvent* event ) override;
//		void dragLeaveEvent( QDragLeaveEvent* event ) override;
//		void dragMoveEvent( QDragMoveEvent* event ) override;
//		void dropEvent( QDropEvent* event ) override;
//		/******************************************/
//	};
	QVBoxLayout* lay_main = nullptr;
	ProjectWidget* wid_cont = nullptr;
	QVBoxLayout* lay_cont = nullptr;
	QTreeView* _tree_view = nullptr;
	/******************************************/
	bool _tree_to_hide = false;
	bool _abletohide = false;
	/******************************************/
	ProjectModel* _model = nullptr;
	// Menu
	QMenu* contextMenu = nullptr;
	QAction* ac_newproject = nullptr;
	QAction* ac_newfolder = nullptr;
	QAction* ac_newdata = nullptr;
	QAction* ac_newgraph = nullptr;
	QAction* ac_newimage = nullptr;
	QAction* ac_newtextfile = nullptr;
	QAction* ac_newfile = nullptr;
	QAction* ac_fromtemplate = nullptr;
	QAction* ac_open = nullptr;
	QAction* ac_load = nullptr;
	QAction* ac_save = nullptr;
	QAction* ac_export = nullptr;
	QAction* ac_delete = nullptr;
	QAction* ac_close = nullptr;
	/******************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
class ProjectWidget
        : public QWidget
{
	Q_OBJECT
public:
	ProjectWidget( QWidget* parent = nullptr );
private slots:
	void leaveEvent( QEvent* event ) override;
signals:
	void leaved();

};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PROJECTVIEW_H
