#include "ProjectModel.h"

#include <QFile>
#include <QFileDialog>
#include <QFileInfo>
#include <QIODevice>
#include <QIcon>
#include <QMimeData>
#include <QRegularExpression>

#include "ProjectItem.h"

#include <QItemSelectionModel>

#include "../Commons/Files.h"
/**********************************************/
/**********************************************/
ProjectModel::~ProjectModel()
{
	delete _rootItem;
}
/**********************************************/
/**********************************************/
ProjectModel::ProjectModel( QObject *parent )
    : QAbstractItemModel(parent)
{
	 _rootItem = new ProjectItem;
	 loadProject( "#project-exemple/My First Project.gpj" );
}
/**********************************************/
/**********************************************/
/**********************************************/
// Create index if there is data
QModelIndex ProjectModel::index( int row, int column, const QModelIndex& parent_index ) const
{
	ProjectItem* parent_item = this->projectItemAt(parent_index);

	ProjectItem* child_item = parent_item->childAt(row);
	if ( child_item == nullptr )
		return QModelIndex();

	return createIndex(row, column, child_item);
}
/**********************************************/
// Create index if there is data
QModelIndex ProjectModel::parent( const QModelIndex& child_index ) const
{
	if ( !child_index.isValid() )
		return QModelIndex();

	ProjectItem* child_item = this->projectItemAt(child_index);
	ProjectItem* parent_item = child_item->parent();

	if ( parent_item == nullptr )
		return QModelIndex();

	return createIndex(parent_item->row(), 0, parent_item);
}
/**********************************************/
/**********************************************/
/**********************************************/
// Number of child in item at parent_index
int ProjectModel::rowCount( const QModelIndex& parent_index ) const
{
	ProjectItem* parent_item = this->projectItemAt(parent_index);
	return (parent_item == nullptr ? 0 :parent_item->childCount());
}
/**********************************************/
// Number of column. Fixed to three
int ProjectModel::columnCount( const QModelIndex& parent_index ) const
{
	return 1;//3;
}
/**********************************************/
/**********************************************/
/**********************************************/
// Insert child to item at parent_item
bool ProjectModel::insertRows( int row, int count, const QModelIndex& parent_index )
{
	ProjectItem* parent_item = projectItemAt(parent_index);

	beginInsertRows(parent_index, row, row+count-1);
	for ( int it = 0; it < count; ++it )
	{
		ProjectItem* newItem = new ProjectItem(parent_item);
		if ( !parent_item->insertChild(row+it, newItem) )
			break;
	}
	endInsertRows();
	return true;
}
/**********************************************/
//
bool ProjectModel::removeRows( int row, int count, const QModelIndex& parent_index )
{
	ProjectItem* parent_item = projectItemAt(parent_index);

	beginRemoveRows(parent_index, row, row+count-1);
	for ( int it = 0; it < count; ++it )
	{
		if ( !parent_item->deleteChild(row+it) )
			break;
	}
	endRemoveRows();
	return true;
}
/**********************************************/
//
bool ProjectModel::insertColumns( int column, int count, const QModelIndex& parent_index )
{
	return false;
}
/**********************************************/
//
bool ProjectModel::removeColumns( int column, int count, const QModelIndex& parent_index )
{
	return false;
}
/**********************************************/
/**********************************************/
/**********************************************/
//
Qt::ItemFlags ProjectModel::flags ( const QModelIndex& index ) const
{
	if ( !index.isValid() )
		return Qt::NoItemFlags;

	Qt::ItemFlags flags = QAbstractItemModel::flags(index);

	flags = flags | Qt::ItemIsEditable;

	if ( !index.isValid() )
		flags = flags | Qt::ItemIsDropEnabled;
	else
		flags = flags | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled;

	return flags;
}
/**********************************************/
/**********************************************/
/**********************************************/
//
bool ProjectModel::insertItem( ProjectItem* item, int row, const QModelIndex& parent_index )
{
	ProjectItem* parent_item = projectItemAt(parent_index);

	ProjectItem* new_item = item;
	if ( new_item == nullptr )
		new_item = new ProjectItem(parent_item);

	beginInsertRows(parent_index, row, row);
	const bool result = parent_item->insertChild(row, new_item);
	endInsertRows();
	return result;
}
/**********************************************/
//
bool ProjectModel::removeItem( int row, const QModelIndex& parent_index )
{
	ProjectItem* parent_item = projectItemAt(parent_index);

	beginRemoveRows(parent_index, row, row);
	const bool result = parent_item->removeChild(row);
	endRemoveRows();
	return result;
}
/**********************************************/
//
bool ProjectModel::deleteItem( int row, const QModelIndex& parent_index )
{
	ProjectItem* parent_item = projectItemAt(parent_index);

	beginRemoveRows(parent_index, row, row);
	const bool result = parent_item->deleteChild(row);
	endRemoveRows();

	return result;
}
/**********************************************/
//
void ProjectModel::findItemIndex( ProjectItem* item, QModelIndex& idx, const QModelIndex& index )
{
	if ( index.internalPointer() == item )
		idx = index;

	if ( idx.isValid() )
		return;

	if ( !this->hasChildren(index) || (index.flags() & Qt::ItemNeverHasChildren) )
	{
		return;
	}
	auto rows = this->rowCount(index);
	for ( int it = 0; it < rows; ++it )
		findItemIndex( item, idx, this->index(it, 0, index) );
}
//
bool ProjectModel::isDeeper( const QModelIndex& id1, const QModelIndex& id2 )
{
	return (deep(id1) > deep(id2) || ((deep(id1) == deep(id2)) && (id1.row() > id2.row())));
}
//
unsigned ProjectModel::deep( const QModelIndex& index )
{
	unsigned deep = 0;
	QModelIndex tmp = index;
	while ( tmp.isValid() )
	{
		tmp = tmp.parent();
		++deep;
	}
	return deep;
}
//
QModelIndex ProjectModel::indexOf( ProjectItem* item )
{
	QModelIndex tmp;
	findItemIndex( item, tmp );
	return tmp;
}
/**********************************************/
/**********************************************/
/**********************************************/
//
bool ProjectModel::insertItemAtRow( ProjectItem* item, int row, const QModelIndex& parent, ProjectItem* parent_item )
{
	if ( !parent.isValid() )
		return false;

	ProjectItem* parentitem = parent_item;
	if ( parentitem == nullptr )
		parentitem = projectItemAt(parent);

	beginInsertRows(parent, row, row);
	item->setParent(parentitem);
	bool result = parentitem->insertChild(row,item);
	endInsertRows();
	return result;
}
/**********************************************/
//
bool ProjectModel::removeItemByIndex( const QModelIndex& index )
{
	if ( !index.isValid() || !index.parent().isValid() )
		return false;

	ProjectItem* parentItem = projectItemAt(index.parent());
	int row = index.row();
	beginRemoveRows(index.parent(), row, row);
	bool result = parentItem->removeChild(row);
	endRemoveRows();
	return result;
}
/**********************************************/
/**********************************************/
/**********************************************/
//
QVariant ProjectModel::headerData( int section, Qt::Orientation orientation, int role ) const
{
	if ( orientation == Qt::Horizontal && role == Qt::DisplayRole )
	{
		switch (section)
		{
			case 0:
			return "Name";
			break;
			case 1:
			    return "Path";
			    break;
			case 2:
			    return "Type";
			    break;
		}
	}

	return QAbstractItemModel::headerData ( section, orientation, role );
}
/**********************************************/
//
QVariant ProjectModel::data( const QModelIndex& index, int role ) const
{
	if ( !index.isValid() )
		return QVariant();

	if ( role == Qt::DisplayRole || role == Qt::EditRole )
	{
		ProjectItem* item = this->projectItemAt(index);
		if (index.column() == 0)
		{
			return item->name();
		}
		else if (index.column() == 1)
		{
			return item->path();
		}
		else if (index.column() == 2)
		{
			QString type = Key::toString(item->type());
			type = type.replace( 0, 1, type.at(0).toUpper() );
			return type;
		}
	}
	else if ( role == Qt::DecorationRole )
	{
		if ( index.column() == 0 )
		{
			ProjectItem* item = this->projectItemAt(index);
			Key type = item->type();
			if ( type == Key::Project )
				return QIcon("icons/Dark Theme/graftonite.svg");
			else if ( type == Key::Folder )
				return QIcon("icons/Dark Theme/folder.svg");
			else if ( type == Key::DataSheet )
				return QIcon("icons/Dark Theme/data.svg");
			else if ( type == Key::Graph )
				return QIcon("icons/Dark Theme/graph.svg");
			else if ( type == Key::Image )
				return QIcon("icons/Dark Theme/image.svg");
		}
	}

	return QVariant();
}
/**********************************************/
/**********************************************/
/**********************************************/
//
bool ProjectModel::setData ( const QModelIndex& index, const QVariant& value, int role )
{
	if ( !index.isValid() )
		return false;

	switch (role)
	{
		case Qt::DisplayRole:
		case Qt::EditRole:
			ProjectItem* item = projectItemAt(index);
			if ( item == nullptr || item == _rootItem )
				return false;
			if (index.column() == 0)
			{
				item->setName( value.toString() );
			}
			else if (index.column() == 1)
			{
				item->setPath( value.toString() );
			}
			emit dataChanged(index, index);
		return true;
		break;
	}

	return false;
}
/**********************************************/
/**********************************************/
/**********************************************/
//
QStringList ProjectModel::mimeTypes() const
{
	QStringList types;
	types << "application/projectitem.list";
	types << "text/uri-list";
	return types;
}
/**********************************************/
//
QMimeData* ProjectModel::mimeData( const QModelIndexList& indexes ) const
{
	QMimeData* mimeData = new QMimeData;

	QByteArray encodedData;

	QDataStream stream( &encodedData, QIODevice::WriteOnly );
	QList<QUrl> urls;

	QList<ProjectItem*> doneRows;
	for ( const QModelIndex &index : indexes )
	{
		if ( index.isValid() )
		{
			ProjectItem* item = projectItemAt(index);
			if ( !doneRows.contains(item) )
			{
				stream << item->name() << item->path() << item->id() << item->type();
				urls << item->path();
				doneRows.append(item);
			}
		}
	}

	mimeData->setData( "application/projectitem.list", encodedData );
	mimeData->setUrls( urls );
	return mimeData;
}
/**********************************************/
//
Qt::DropActions ProjectModel::supportedDropActions() const
{
	return (Qt::CopyAction | Qt::MoveAction | Qt::DropAction::TargetMoveAction);
}
/**********************************************/
//
bool ProjectModel::dropMimeData( const QMimeData* mime_data, Qt::DropAction action, int row, int column, const QModelIndex& parent )
{

	ProjectItem* target_item = projectItemAt(parent);

	if ( mime_data->hasFormat("application/projectitem.list") )
	{
		int target_row;
		if ( row != -1 )
			target_row = row;
		else
			target_row = rowCount(parent);

		QModelIndexList indexlist = _selectionModel->selectedRows();
//		qDebug() << indexlist;
		std::sort( indexlist.begin(), indexlist.end(), isDeeper );
//		qDebug() << indexlist;
		for ( ProjectItem* item : selectedProjectItem() )
		{
//			ProjectItem* parentItem = item->parent();
			if ( /*parentItem == _rootItem ||*/ item == target_item )
				continue;

			if ( target_item == _rootItem && item->type() != Key::Project )
				continue;

			QModelIndex item_index = indexOf(item);
//			QModelIndex parent_index = indexOf(parentItem);
//			qDebug() << item_index;

			// Remove item
			if ( !removeItemByIndex( item_index ) )
				continue;
//			qDebug() << "remove step ok !";

			// Insert item
			if ( !insertItemAtRow( item, target_row, parent, target_item ) )
				continue;
//			qDebug() << "insert step ok !";

			++target_row;
		}
		return true;
	}
	else if ( mime_data->hasFormat("text/uri-list") )
	{
		for ( const QUrl& url : mime_data->urls() )
		{
			qDebug() << url.toString();
		}
		return true;
	}
	else
		return false;
}
/**********************************************/
/**********************************************/
/**********************************************/
//
void ProjectModel::setItemSelectionModel( QItemSelectionModel* selection )
{
	_selectionModel = selection;
	connect( _selectionModel, &QItemSelectionModel::currentRowChanged,
	         this, &ProjectModel::onSelectionChanged );
}
/**********************************************/
//
QList<ProjectItem*> ProjectModel::selectedProjectItem()
{
	QList<ProjectItem*> selected_item;
	for ( const QModelIndex& index : _selectionModel->selectedRows(0) )
	{
		selected_item.append( projectItemAt(index) );
	}
	return selected_item;
}
/**********************************************/
/**********************************************/
/**********************************************/
//
bool ProjectModel::addChild( const QString& name, const QString& path, const Key::Keys& type )
{
	QModelIndex index = _selectionModel->currentIndex();
	if ( !index.isValid() )
		return false;
	ProjectItem* parentItem = projectItemAt(index);

	beginInsertRows( index, parentItem->childCount(), parentItem->childCount());
	ProjectItem* newchild = new ProjectItem(name, path, type, parentItem);
	parentItem->appendChild( newchild );
	endInsertRows();
	return true;
}

bool ProjectModel::insertChild( const QString& name, const QString& path, const Key::Keys& type )
{
	QModelIndex index = _selectionModel->currentIndex();
	if ( !index.isValid() )
		return false;
	ProjectItem* item = projectItemAt(index);
	ProjectItem* parentItem = item->parent();
	if ( parentItem == _rootItem )
		return false;
	QModelIndex parentIndex = index.parent();

	int row = index.row();
	this->beginInsertRows( parentIndex, row, row );
	ProjectItem* newchild = new ProjectItem(name, path, type, parentItem);
	bool result = parentItem->insertChild( row, newchild );
	this->endInsertRows();

	return result;
}

QString ProjectModel::fullPath( ProjectItem* item )
{
	if ( item == nullptr )
		return "";
	QString path  = item->path();
	if ( !path.startsWith("/") )
	{
		ProjectItem* project_item = item;
		while ( project_item->parent() != _rootItem && project_item->parent() != nullptr   )
			project_item = project_item->parent();
		QString prj_path;
		if ( project_item != nullptr )
			prj_path = project_item->path();
		path = prj_path + (prj_path.endsWith("/")?"":"/") + path;
	}
	return path;
}
/**********************************************/
/**********************************************/
/**********************************************/
//
void ProjectModel::createProject()
{
	int row = _rootItem->childCount();
	this->beginInsertRows( QModelIndex(), row, row );
	ProjectItem* newproject = new ProjectItem("New Project", "", Key::Project, _rootItem);
	_rootItem->appendChild( newproject );
	this->endInsertRows();
	//    return success;
}
/**********************************************/
//
void ProjectModel::openProject()
{
	QString path = QFileDialog::getOpenFileName(nullptr);
	loadProject(path);
}
/**********************************************/
//
void ProjectModel::saveProject()
{
//	qDebug() << "Saving Projects....";
	for ( int it = 0; it < _rootItem->childCount(); ++it )
	{
		ProjectItem* prj_item = _rootItem->childAt(it);
		QFile prj_file(prj_item->path()+"/"+prj_item->name()+".gpj");
		if ( !prj_file.open( QIODevice::WriteOnly | QIODevice::Text ) )
			return;
		QTextStream prj_data( &prj_file );
		prj_data << "#" << Key::toString(Key::Project).toUpper();
		for ( const Key& key : prj_item->settings().keys() )
			if ( key != Key::Type )
				prj_data << key.toString() << "=" << prj_item->settings().value(key).toString() << "\n";
//		saveProjectChildren( &prj_data, prj_item );
		prj_file.close();
	}
}
/**********************************************/
//
void ProjectModel::closeProject()
{
	QModelIndex index = _selectionModel->currentIndex();
	if ( !index.isValid() )
		return /*false*/;
	ProjectItem* item = projectItemAt(index);
	ProjectItem* parentItem = item->parent();
	if ( parentItem == _rootItem )
	{
		int oldrow = _rootItem->indexOf( item );
		beginRemoveRows(QModelIndex(), oldrow, oldrow);
		/*bool result = */parentItem->removeChild(oldrow);
		endRemoveRows();
	}
}
/**********************************************/
//
void ProjectModel::loadProject( const QString& path )
{
	QRegularExpression re_comment( "^\t*//.*" );
	QRegularExpression re_project_file( "^(?<level>\t*)\\#(?<type>[A-Z]+)" );
//	QString re_beg( "^(?<level>\t*)(?<key>[\\w|\\.]+)=" );
	QRegularExpression re_setting( "^(?<level>\t*)(?<key>[\\w|\\.]+)=(?<value>.*)$" );
	QRegularExpression re_bool( "^(?<bool>(true|false))$" );
	QRegularExpression re_int( "^(?<int>\\d+)$" );
	QRegularExpression re_float( "^(?<float>\\d*(\\.|,){1}(\\d)*)$" );
	QRegularExpression re_string( "^\"(?<string>.*)\"$" );

	QFile file( path );
	if ( file.open(QIODevice::ReadOnly) )
	{
		QList<ProjectItem*> parent = {_rootItem};
		QTextStream stream( &file );
		ProjectItem* current_item = nullptr;
		while ( !stream.atEnd() )
		{
			QString line = stream.readLine();
			if ( line.isEmpty() || re_comment.match(line).hasMatch() )
				continue;

			QRegularExpressionMatch match_project_file = re_project_file.match(line);
			QRegularExpressionMatch match_setting = re_setting.match(line);
			// Create a new current item
			if ( match_project_file.hasMatch() )
			{
				int level = match_project_file.captured("level").length();
				QString type = match_project_file.captured("type").toLower();
				ProjectItem* parent_item = nullptr;
				if ( level+2 > parent.size() )
					parent_item = parent.last();
				else
				{
					parent_item = parent.at(level);
					for ( int jt = level+1; jt < parent.size(); ++jt )
						parent.remove( jt );
				}
				current_item = new ProjectItem(parent_item);
				current_item->settings().addSetting( Key::Type, Key::toKeys(type) );
				parent_item->appendChild(current_item);
				parent.append(current_item);
			}
			// Add setting to current item
			else if ( match_setting.hasMatch() )
			{
				//int level = match_setting.captured("level").length();
				QString key = match_setting.captured("key");
				QString value = match_setting.captured("value");
				QVariant tmp_var;
				if ( re_bool.match(value).hasMatch() )
				{
					tmp_var = QVariant(re_bool.match(value).captured("bool")).toBool();
				}
				else if ( re_int.match(value).hasMatch() )
				{
					tmp_var = re_int.match(value).captured("int").toInt();
				}
				else if ( re_float.match(value).hasMatch() )
				{
					tmp_var = re_float.match(value).captured("float").toDouble();
				}
				else if ( re_string.match(value).hasMatch() )
				{
					tmp_var = re_string.match(value).captured("string");
				}
				if ( current_item != nullptr )
					current_item->settings().addSetting( key, tmp_var );
			}
		}
		file.close();
		if ( parent.size() > 1 && !parent.at(1)->settings().hasKey(Key::Path) )
		{
			parent.at(1)->settings().addSetting( Key::Path, QFileInfo(path).absolutePath() );
			parent.at(1)->setID( Files::generateUniqueID() );
		}
		emit dataChanged(QModelIndex(), QModelIndex());
	}
}
/**********************************************/
//
void ProjectModel::addFolder()
{
	this->addChild( "New Folder", "", Key::Folder );
}
/**********************************************/
//
void ProjectModel::addDataSheet()
{
	this->addChild( "New Data Sheet", "", Key::DataSheet );
}
/**********************************************/
//
void ProjectModel::addGraph()
{
	this->addChild( "New Graph", "", Key::Graph );
}
/**********************************************/
//
void ProjectModel::addImage()
{
	this->addChild( "New Image", "", Key::Image );
}
/**********************************************/
//
void ProjectModel::insertFolder()
{
	this->insertChild( "New Folder", "", Key::Folder );
}
/**********************************************/
//
void ProjectModel::insertDataSheet()
{
	this->insertChild( "New Data Sheet", "", Key::DataSheet );
}
/**********************************************/
//
void ProjectModel::insertGraph()
{
	this->insertChild( "New Graph", "", Key::Graph );
}
/**********************************************/
//
void ProjectModel::insertImage()
{
	this->insertChild( "New Image", "", Key::Image );
}
/**********************************************/
//
void ProjectModel::removeFile()
{
	QModelIndexList selection = _selectionModel->selectedRows(0);

	for ( const QModelIndex& index : selection )
	{
		ProjectItem* item = projectItemAt(index);
		ProjectItem* parentItem = item->parent();
		if ( parentItem == _rootItem )
			continue;

		// Remove item
		int oldrow = index.row();
		beginRemoveRows(index.parent(), oldrow, oldrow);
		/*bool result = */parentItem->removeChild(oldrow);
		endRemoveRows();
	}
}
/**********************************************/
//
void ProjectModel::openFile()
{

}
/**********************************************/
//
void ProjectModel::saveFile()
{

}
/**********************************************/
//
void ProjectModel::closeFile()
{

}
/**********************************************/
//
void ProjectModel::importFile()
{

}
/**********************************************/
//
void ProjectModel::exportFile()
{

}
/**********************************************/
//
void ProjectModel::saveProjects()
{
	qDebug() << "Saving Projects....";
	for ( int it = 0; it < _rootItem->childCount(); ++it )
	{
		ProjectItem* prj_item = _rootItem->childAt(it);
		QFile prj_file(prj_item->path()+"/"+prj_item->name()+".gpj");
		if ( !prj_file.open( QIODevice::WriteOnly | QIODevice::Text ) )
			return;
		QTextStream prj_data( &prj_file );
		prj_data << "#PROJECT\n";
		for ( const Key& key : prj_item->settings().keys() )
			if ( key != Key::Type )
				saveSetting( &prj_data, key, prj_item->settings().value(key) );
//				prj_data << key.toString() << "=\"" << prj_item->settings().value(key).toString() << "\"\n";
		saveProjectChildren( &prj_data, prj_item );
		prj_file.close();
	}
}
/**********************************************/
//
void ProjectModel::saveProjectChildren( QTextStream* out, ProjectItem* item, int level )
{
	for ( int it = 0; it < item->childCount(); ++it )
	{
		ProjectItem* ch_item = item->childAt(it);
		for ( int it = 0; it < level; ++it )
			*out << "\t";
		*out << "#";
		*out << Key(ch_item->type()).toString().toUpper();
		*out << "\n";
		for ( const Key& key : ch_item->settings().keys() )
			if ( key != Key::Type )
			{
				for ( int it = 0; it < level; ++it )
					*out << "\t";
				saveSetting( out, key, ch_item->settings().value(key) );
			}
//				*out << " " << key.toString() << "=\"" << ch_item->settings().value(key).toString() << "\"";
		saveProjectChildren( out, ch_item, level+1 );
	}
}

void ProjectModel::saveSetting( QTextStream* out, const Key& key, const QVariant& value )
{
	*out << key.toString() << "=";
	if ( value.metaType() == QMetaType(QMetaType::QString) )
	{
		*out << "\"" << value.toString() << "\"";
	}
	else if ( value.metaType() == QMetaType(QMetaType::Int) )
		*out << value.toInt();
	else if ( value.metaType() == QMetaType(QMetaType::Double) )
		*out << value.toDouble();
	else if ( value.metaType() == QMetaType(QMetaType::Bool) )
		*out << value.toString();
//	else if ( value.metaType() == QMetaType(QMetaType::Int) )
//		*out << value.toInt();
//	else if ( value.metaType() == QMetaType(QMetaType::Int) )
//		*out << value.toInt();
	*out << "\n";
}
/**********************************************/
/**********************************************/
/**********************************************/
//
void ProjectModel::onSelectionChanged( const QModelIndex& index, const QModelIndex& previous )
{
//	ProjectItem* previousItem = projectItemAt(previous);
//	if ( previousItem->hasChanged() )
//		previousItem->saveTmp();
	emit currentProjectItemChanged( projectItemAt(index) );
}
/**********************************************/
//
ProjectItem* ProjectModel::projectItemAt( const QModelIndex& index ) const
{
	if ( index.isValid() )
	{
		ProjectItem* item = static_cast<ProjectItem*>(index.internalPointer());
		if ( item != nullptr )
			return item;
	}
	return _rootItem;
}
/**********************************************/
//
