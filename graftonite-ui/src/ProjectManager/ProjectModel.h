#ifndef PROJECTMODEL_H
#define PROJECTMODEL_H
/**********************************************/
#include <QAbstractItemModel>
/**********************************************/
class QItemSelectionModel;
/**********************************************/
#include "ProjectItem.h"
/**********************************************/
/**********************************************/
/**********************************************/
class ProjectModel
        : public QAbstractItemModel
{
	Q_OBJECT
public:
	~ProjectModel();
	ProjectModel( QObject* parent = nullptr );
	/******************************************/
	//
	QModelIndex index( int row, int column, const QModelIndex& parent_index = QModelIndex() ) const override;
	QModelIndex parent( const QModelIndex& child_index ) const override;
	/******************************************/
	//
	int rowCount( const QModelIndex& parent_index = QModelIndex() ) const override;
	int columnCount( const QModelIndex& parent_index = QModelIndex() ) const override;
	/******************************************/
	//
	bool insertRows( int row, int count, const QModelIndex& parent_index = QModelIndex() ) override;
	bool removeRows( int row, int count, const QModelIndex& parent_index = QModelIndex() ) override;
	bool insertColumns( int column, int count, const QModelIndex& parent_index = QModelIndex() ) override;
	bool removeColumns( int column, int count, const QModelIndex& parent_index = QModelIndex() ) override;
	/******************************************/
	Qt::ItemFlags flags( const QModelIndex& index ) const override;
	/******************************************/
	//
	bool insertItem( ProjectItem* item = nullptr, int row = 0, const QModelIndex& parent_index = QModelIndex() );
	bool removeItem( int row, const QModelIndex& parent_index = QModelIndex() );
	bool deleteItem( int row, const QModelIndex& parent_index = QModelIndex() );
	QModelIndex indexOf( ProjectItem* item );
	bool insertItemAtRow( ProjectItem* item, int row, const QModelIndex& parent_index, ProjectItem* parent_item = nullptr );
	bool removeItemByIndex( const QModelIndex& index );
	/******************************************/
	// Data display methods
	QVariant headerData( int section, Qt::Orientation orientation, int role = Qt::DisplayRole ) const override;
	QVariant data( const QModelIndex& index, int role = Qt::DisplayRole ) const override;
//	QMap<int,QVariant> itemData( const QModelIndex& index ) const override;
	/******************************************/
	// Data modifier methods
	bool setData( const QModelIndex& index, const QVariant& value, int role = Qt::DisplayRole ) override;
//	bool setItemData( const QModelIndex& index, const QMap<int,QVariant>& roles ) override;
	/******************************************/
	// Drag & Drop methods
	QStringList mimeTypes() const override;
	QMimeData* mimeData( const QModelIndexList &indexes ) const override;
	Qt::DropActions supportedDropActions() const override;
	bool dropMimeData( const QMimeData* mime_data, Qt::DropAction action, int row, int column, const QModelIndex& parent ) override;
	/******************************************/
	//void sort(int column, Qt::SortOrder order = Qt::AscendingOrder) override;
	void setItemSelectionModel( QItemSelectionModel* selection );
	QList<ProjectItem*> selectedProjectItem();
	/******************************************/
	// Child manager
	bool addChild( const QString& name, const QString& path, const Key::Keys& type );
	bool insertChild( const QString& name, const QString& path, const Key::Keys& type );
	/******************************************/
	QString fullPath( ProjectItem* item );
	/******************************************/
public slots:
	// Project methods
	void createProject();
	void openProject();
	void saveProject();
	void closeProject();
	void loadProject( const QString& path );
	// File methods
	void addFolder();
	void addDataSheet();
	void addGraph();
	void addImage();
	void insertFolder();
	void insertDataSheet();
	void insertGraph();
	void insertImage();
	void removeFile();
	void openFile();
	void saveFile();
	void closeFile();
	// Import/Export methods
	void importFile();
	void exportFile();
	/******************************************/
	void saveProjects();
	void saveProjectChildren( QTextStream* out, ProjectItem* item, int level = 1 );
	void saveSetting( QTextStream* out, const Key& key, const QVariant& value );
	/******************************************/
private slots:
	void onSelectionChanged( const QModelIndex& index, const QModelIndex& previous );
	/******************************************/
signals:
	void currentProjectItemChanged( ProjectItem* item );
	/******************************************/
private:
	ProjectItem* _rootItem = nullptr;
	ProjectItem* projectItemAt( const QModelIndex& index ) const;
	void findItemIndex( ProjectItem* item, QModelIndex& idx, const QModelIndex& index = QModelIndex() );
	/******************************************/
	QItemSelectionModel* _selectionModel = nullptr;
	/******************************************/
	static bool isDeeper( const QModelIndex& id1, const QModelIndex& id2 );
	static unsigned deep( const QModelIndex& index );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PROJECTMODEL_H
