#ifndef GEOMETRICOBJECTS_H
#define GEOMETRICOBJECTS_H
/**********************************************/
#include <QPointF>
#include <QLineF>
#include <QRectF>
/**********************************************/
#include "Key.h"
/**********************************************/
class QString;
/**********************************************/
/**********************************************/
class Point : public QPointF
{
public:
	Point();
	Point( double x, double y );
	Point( const Point& point );
	Point( const QPointF& point );
//	Point( const QString& point );
//	friend QDataStream& operator<< ( QDataStream& os, const Point& p )
//	{
//		os << "{" << p.x() << "," << p.y() << "}";
//		return os;
//	}
	Point& operator= ( const Point& point );
	Point& operator+= ( const Point& point );
	Point operator+ ( const Point& point );
	Point& operator-= ( const Point& point );
	Point operator- ( const Point& point );
};
/**********************************************/
/**********************************************/
/**********************************************/
class Anchor
{
public:
	Anchor();
	Anchor( const Key::Keys& anchor );
	bool isLeft() const;
	bool isRight() const;
	bool isHCenter() const;
	bool isTop() const;
	bool isBottom() const;
	bool isVCenter() const;
	bool isBaseline() const;
	Key::Keys _anchor = Key::Top;
};
/**********************************************/
/**********************************************/
/**********************************************/
class Line : public QLineF
{
public:
	enum Orientation
	{
		Vertical,
		Horizontal
	};
public:
	Line();
	Line( const Point& p1, const Point& p2 );
	Line( double x1, double y1, double x2, double y2 );
	Line( double x, double y, double length, const Line::Orientation& orientation = Line::Horizontal );
	Line( const Point& p1, double shift, double length, const Line::Orientation& orientation = Line::Horizontal );
	Line( const QLineF& line );
//	friend QDataStream& operator<< ( QDataStream& os, const Line& l )
//	{
//		os << "{" << l.x1() << "," << l.y1() << "," << l.x2() << "," << l.y2() << "}";
//		return os;
//	}
};
/**********************************************/
/**********************************************/
/**********************************************/
class Size : public QSizeF
{
public:
	Size();
	Size( double width, double height );
	Size( const QString& size );
	Point point( const Key& anchor ) const;
	friend Size operator* ( double d, const Size& size )
	{
		Size new_size;
		new_size.setWidth( d*size.width() );
		new_size.setHeight( d*size.height() );
		return new_size;
	}
	friend Size operator* ( const Size& size, double d )
	{
		return (d*size);
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
class Rect : public QRectF
{
public:
	Rect();
	Rect( double x, double y, double width, double height );
	Rect( const Point& point, const Size& size );
	Rect( const Line& line );
	Rect( const QString& rect );
	Rect( const QRectF& rect );
	Point origin() const;
	Point point( const Key& anchor ) const;
	Line topLine() const;
	Line hCenterLine() const;
	Line bottomLine() const;
	Line leftLine() const;
	Line vCenterLine() const;
	Line rightLine() const;
	Line line( const Key& pos, bool vcenter = false ) const;
	bool contains( const Point& pos );
	bool contains( const Line& line );
	bool contains( const Rect& rect );
	void expandTo( const Point& pos );
	void expandTo( const Line& line );
	void expandTo( const Rect& rect );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // GEOMETRICOBJECTS_H
