#include "XML.h"
/**********************************************/
#include <QFile>
#include <QFileInfo>
#include <QTextStream>
#include <QPoint>
#include "Files.h"
/**********************************************/
/**********************************************/
XML::XML()
{

}
/**********************************************/
/**********************************************/
bool XML::exists()
{
	return QFileInfo(_path).exists();
}
/**********************************************/
/**********************************************/
void XML::setPath(const QString& path)
{
	_path = path;
	QFile file( path );
	// Opening and Checking if the file exists
	if ( !file.open( QIODevice::ReadOnly | QIODevice::Text ) ) return;
	// Settings and Checking if the file containts XML data
	_file.setContent( &file );
	file.close();
}
/**********************************************/
/**********************************************/
QDomElement XML::getGraph() const
{
	return _file.documentElement();
}
/**********************************************/
/**********************************************/
QDomElement XML::GraphElement( const QString& path )
{
//	QDomDocument doc = Files::readXML( path );
//	QFile file( path );
//	// Opening and Checking if the file exists
//	if ( !file.open( QIODevice::ReadOnly | QIODevice::Text ) ) return QDomElement();
//	// Settings and Checking if the file containts XML data
//	doc.setContent( &file );
//	file.close();
	return Files::readXML( path ).documentElement();
}
/**********************************************/
/**********************************************/
QList<QDomElement> XML::getElements()
{
	QList<QDomElement> elements;
	QDomElement el = getGraph().firstChildElement();
	while ( !el.isNull() )
	{
		elements.append( el );
		el = el.nextSiblingElement();
	}
	return elements;
}
/**********************************************/
/**********************************************/
QList<QDomElement> XML::ChildElements( const QDomElement& parent )
{
	QList<QDomElement> elements;
	QDomElement el = parent.firstChildElement();
	while ( !el.isNull() )
	{
		elements.append( el );
		el = el.nextSiblingElement();
	}
	return elements;
}
/**********************************************/
/**********************************************/
QStringList XML::AttributeKeys( const QDomElement& el )
{
	QStringList keys;
	if( el.hasAttributes() )
	{
		QDomNamedNodeMap map = el.attributes();
		for( int i = 0; i < map.length(); ++i )
		{
			if( !(map.item(i).isNull()) )
			{
				QDomNode debug = map.item(i);
				QDomAttr attr = debug.toAttr();
				if( !attr.isNull() )
				{
					keys.append( attr.name() );
				}
			}
		}
	}
	return keys;
}
/**********************************************/
/**********************************************/
void XML::setInt( const QDomElement& el, const QString& atr, int& value )
{
	if ( el.hasAttribute( atr ) )
		value = el.attribute( atr ).toInt();
}
/**********************************************/
/**********************************************/
void XML::setDouble( const QDomElement& el, const QString& atr, double& value )
{
	if ( el.hasAttribute( atr ) )
		value = el.attribute( atr ).toDouble();
}
/**********************************************/
/**********************************************/
void XML::setString( const QDomElement& el, const QString& atr, QString& value )
{
	if ( el.hasAttribute( atr ) )
		value = el.attribute( atr );
}
/**********************************************/
/**********************************************/
void XML::setStringList( const QDomElement& el, const QString& atr, QStringList& value )
{
	if ( el.hasAttribute( atr ) )
		value = el.attribute( atr ).split(",");
}
/**********************************************/
/**********************************************/
void XML::setPoint( const QDomElement& el, const QString& atr, QPointF& value )
{

	if ( !el.hasAttribute( atr ) ) return;
	QStringList str = el.attribute( atr ).split(",");
	if ( str.size() < 2 ) return;
	value.setX( str.at(0).toDouble() );
	value.setY( str.at(1).toDouble() );
}
/**********************************************/
/**********************************************/
int XML::getInt( const QDomElement& el, const QString& atr )
{
	if ( !el.hasAttribute( atr ) ) return int();
	return el.attribute( atr ).toInt();
}
/**********************************************/
/**********************************************/
double XML::getDouble( const QDomElement& el, const QString& atr )
{
	if ( !el.hasAttribute( atr ) ) return double();
	return el.attribute( atr ).toDouble();
}
/**********************************************/
/**********************************************/
QString XML::getString( const QDomElement& el, const QString& atr )
{

	if ( !el.hasAttribute( atr ) ) return QString();
	return el.attribute( atr );
}
/**********************************************/
/**********************************************/
QStringList XML::getStringList( const QDomElement& el, const QString& atr )
{
	if ( !el.hasAttribute( atr ) ) return QStringList();
	return el.attribute( atr ).split(",");
}
/**********************************************/
/**********************************************/
QPointF XML::getPoint( const QDomElement& el, const QString& atr )
{
	if ( !el.hasAttribute( atr ) ) return QPointF();
	QStringList str = el.attribute( atr ).split(",");
	if ( str.size() < 2 ) return QPointF();
	return { str.at(0).toDouble(), str.at(1).toDouble() };
}
/**********************************************/
/**********************************************/
int XML::getInt( const QDomElement& el, const QString& child, const QString& atr )
{
	QDomElement cel = el.firstChildElement(child);
	return getInt( cel, atr );
}
/**********************************************/
/**********************************************/
double XML::getDouble( const QDomElement& el, const QString& child, const QString& atr )
{
	QDomElement cel = el.firstChildElement(child);
	return getDouble( cel, atr );
}
/**********************************************/
/**********************************************/
QString XML::getString( const QDomElement& el, const QString& child, const QString& atr )
{
	QDomElement cel = el.firstChildElement(child);
	return getString( cel, atr );
}
/**********************************************/
/**********************************************/
QStringList XML::getStringList( const QDomElement& el, const QString& child, const QString& atr )
{
	QDomElement cel = el.firstChildElement(child);
	return getStringList( cel, atr );
}
/**********************************************/
/**********************************************/
QPointF XML::getPoint( const QDomElement& el, const QString& child, const QString& atr )
{
	QDomElement cel = el.firstChildElement(child);
	return getPoint( cel, atr );
}
/**********************************************/
/**********************************************/
void XML::save( QDomElement& el, const QString& atr, int value )
{
	el.setAttribute( atr, value );
}
/**********************************************/
/**********************************************/
void XML::save( QDomElement& el, const QString& atr, double value )
{
	el.setAttribute( atr, value );
}
/**********************************************/
/**********************************************/
void XML::save( QDomElement& el, const QString& atr, const QString& value )
{
	el.setAttribute( atr, value );
}
/**********************************************/
/**********************************************/
void XML::save( QDomElement& el, const QString& atr, const QStringList& value )
{
	QString content;
	for ( int it = 0; it < value.size(); ++it )
	{
		content += value.at(it);
		if ( it+1 != value.size() )
			content += ",";
	}
	el.setAttribute( atr, content );
}
/**********************************************/
/**********************************************/
void XML::save( QDomElement& el, const QString& atr, const QPointF& value )
{
	QString content = QString::number(value.x()) + "," + QString::number(value.y());
	el.setAttribute( atr, content );
}
/**********************************************/
/**********************************************/
void XML::save(QDomElement& el, const QString& child, const QString& atr, int value)
{
	el.firstChildElement(child).setAttribute(atr, value);
}
/**********************************************/
/**********************************************/
void XML::save( QDomElement& el, const QString& child, const QString& atr, double value )
{
	el.firstChildElement(child).setAttribute(atr, value);
}
/**********************************************/
/**********************************************/
void XML::save( QDomElement& el, const QString& child, const QString& atr, const QString& value )
{
	el.firstChildElement(child).setAttribute(atr, value);
}
/**********************************************/
/**********************************************/
void XML::save( QDomElement& el, const QString& child, const QString& atr, const QStringList& value )
{
	QString content;
	for ( int it = 0; it < value.size(); ++it )
	{
		content += value.at(it);
		if ( it+1 != value.size() )
			content += ",";
	}
	el.firstChildElement(child).setAttribute(atr, content);
}
/**********************************************/
/**********************************************/
void XML::save( QDomElement& el, const QString& child, const QString& atr, const QPointF& value )
{
	QString content = QString::number(value.x()) + "," + QString::number(value.y());
	el.firstChildElement(child).setAttribute(atr, content);
}
/**********************************************/
/**********************************************/
QString XML::Attribute( const QDomElement& el, const QString& atr )
{
	return el.attribute( atr );
}
/**********************************************/
/**********************************************/
bool XML::saveXML( const QString& path, QDomDocument& xmlDoc )
{
	// Getting root element
	QDomElement el_root = xmlDoc.documentElement();
	// Writing the Settings of XML Files
	QDomProcessingInstruction settings =
			xmlDoc.createProcessingInstruction( "xml", "version=\"1.0\" encoding=\"utf-8\"" );
	xmlDoc.insertBefore( settings, el_root );
	// Create the XML if DONT exists
	QFile xmlFile( path );
	if( !xmlFile.open( QFile::WriteOnly ) )
		return false;
	// Create QTextStream
	QTextStream stream( &xmlFile );
	// Writing the
	xmlDoc.save( stream, 4, QDomDocument::EncodingFromTextStream );
	//Fermeture du fichier
	xmlFile.close();
	return true;
}
/**********************************************/
/**********************************************/
QDomDocument XML::readXML(const QString& path)
{
	// Getting root element
	QDomDocument xmlDoc;
	QFile file( path );
	// Opening and Checking if the file exists
	if ( !file.open( QIODevice::ReadOnly | QIODevice::Text ) )
		return xmlDoc;
	// Settings and Checking if the file containts XML data
	xmlDoc.setContent( &file );
	file.close();
	return xmlDoc;
}
/**********************************************/
/**********************************************/
bool XML::readXML(const QString& path, QDomDocument& contents)
{
	// Getting root element
	QFile file( path );
	// Opening and Checking if the file exists
	if ( !file.open( QIODevice::ReadOnly | QIODevice::Text ) )
		return false;
	// Settings and Checking if the file containts XML data
	contents.setContent( &file );
	file.close();
	if ( contents.isNull() )
		return false;
	return true;
}
/**********************************************/
/**********************************************/
