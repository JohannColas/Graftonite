#ifndef GFT_H
#define GFT_H

#include <QString>



namespace GFT
{
	static inline bool compare( const QString& key1, const QString& key2 )
	{
		return (key1.compare(key2, Qt::CaseInsensitive) == 0);
	}

	enum class SettingKey
	{
		NoKey,
		Unknown,
		Name,
		ID,
		Type,
		FileName,
		Path,
		Working,
		Format,

		// Dimension Settings
		Size,
		Width,
		Height,

		// App Settings
		Lang,
		Theme,
		IconTheme,
		// File Keys
		// Size

		Min,
		Max,
		Scale,

		Position,
		Anchor,
		Color,

		LinkTo,
		Minor,
		Ticks,
		Increment,
		Numbers,
		Labels,
		// Title => already define
		Grids,
		NoGrids,
		// Data Settings
		Coordinates,
		Function,
		// Plot Settings
		XY,
		XYY,
		Axis1,
		Axis2,
		Axis3,
		Data1,
		Data2,
		Data3,
		// Plot Elements
		Area,
		Bars,
		ErrorBars,
		// Legend Settings
		Entry,
		AddEntry,

		// Geometry Keys
		Spacing,
		Margins,
//		Anchor,
		Shift,
		Transform,

		Title,
		Text,
		Image,
		Data,
		Shape,

		// Layout Keys
		Layout,
		// Line Style Settings
		Borders,
		Line,
		Dash,
		Join,
		MiterLimit,
		Cap,
		Gap,
		// Text Style Settings
		Font,
		Family,
		Weight,
		Slant,
		Underline,
		Capitalize,
		ScriptOpt,

		Symbols,
		Covering,
		// Other Settings
		General,
		Style,
		Hide,
		Offset,
		Option,
//		Color,
		Alignment,
		Rotation,
		BeginAngle,
		EndAngle,
		Separator,
		// Project Elements
	};
	static inline QString toString( const SettingKey& key )
	{
		switch (key)
		{
			case SettingKey::NoKey: return "nokey";
			case SettingKey::Name: return "name";
			case SettingKey::ID: return "id";
			case SettingKey::Type: return "type";
			case SettingKey::FileName: return "filename";
			case SettingKey::Path: return "path";
			case SettingKey::Working: return "working";
			case SettingKey::Format: return "format";

			// Dimension Settings
			case SettingKey::Size: return "size";
			case SettingKey::Width: return "width";
			case SettingKey::Height: return "height";

			// App Settings
			case SettingKey::Lang: return "lang";
	//		case SettingKey::Root: return "root";
	//		case SettingKey::Project: return "project";
	//		case SettingKey::Folder: return "folder";
	//		case SettingKey::DataSheet: return "datasheet";
				//		case SettingKey::Graph: return "name";
				//		case SettingKey::Image: return "image";
	//		case SettingKey::TextFile: return "textfile";
	//		case SettingKey::File: return "file";
	//		case SettingKey::PNG: return "png";
	//		case SettingKey::PDF: return "pdf";
	//		case SettingKey::SVG: return "svg";
	//		case SettingKey::PS: return "ps";
	//		case SettingKey::Log: return "log";
				// Element types
				//		case SettingKey::NoType: return "unknown type";
	//		case SettingKey::Current: return "current";
	//		case SettingKey::Graph: return "graph";
	//		case SettingKey::Template: return "template";
	//		case SettingKey::Layer: return "layer";
	//		case SettingKey::Frame: return "frame";
	//		case SettingKey::Axis: return "axis";
	//		case SettingKey::Plot: return "plot";
	//		case SettingKey::Legend: return "legend";
			case SettingKey::Layout: return "layout";
	//		case SettingKey::Vertical: return "vertical";
	//		case SettingKey::Horizontal: return "horizontal";
	//		case SettingKey::Table: return "table";
	//		case SettingKey::Column: return "column";
	//		case SettingKey::Row: return "row";
			case SettingKey::Spacing: return "spacing";
			case SettingKey::Margins: return "margins";
			case SettingKey::Entry: return "entry";
			case SettingKey::AddEntry: return "addentry";
			case SettingKey::Title: return "title";
	//			// Geometry Settings
	//		case SettingKey::X: return "x";
	//		case SettingKey::Y: return "y";
			case SettingKey::Position: return "position";
				//  Settings
				//			case SettingKey::Background: return "background";
				//			case SettingKey::Fill: return "fill";
			case SettingKey::Borders: return "borders";
				// Positions
	//		case SettingKey::Center: return "center";
	//		case SettingKey::Bottom: return "bottom";
	//		case SettingKey::Top: return "top";
	//		case SettingKey::Right: return "right";
	//		case SettingKey::Left: return "left";
	//		case SettingKey::Outside: return "outside";
	//		case SettingKey::Inside: return "inside";
				// Anchors
	//		case SettingKey::BottomLeft: return "bottomleft";
	//		case SettingKey::BottomRight: return "bottomright";
	//		case SettingKey::TopLeft: return "topleft";
	//		case SettingKey::TopRight: return "topright";
	//		case SettingKey::BaselineLeft: return "baselineleft";
	//		case SettingKey::Baseline: return "baseline";
	//		case SettingKey::BaselineRight: return "baselineright";
				// Specific Axis Settings
			case SettingKey::Hide: return "hide";
			case SettingKey::Min: return "min";
			case SettingKey::Max: return "max";
			case SettingKey::Scale: return "scale";
			case SettingKey::Option: return "option";
			case SettingKey::LinkTo: return "linkto";
				// Line Settings
			case SettingKey::Line: return "line";
			case SettingKey::Dash: return "dash";
			case SettingKey::Offset: return "offset";
			case SettingKey::Color: return "color";
			case SettingKey::Join: return "join";
	//		case SettingKey::Round: return "round";
	//		case SettingKey::Bevel: return "bevel";
	//		case SettingKey::Miter: return "miter";
			case SettingKey::MiterLimit: return "miterlimit";
			case SettingKey::Cap: return "cap";
	//		case SettingKey::Butt: return "butt";
	//		case SettingKey::Square: return "square";
				// Ticks Settings
			case SettingKey::Ticks: return "ticks";
			case SettingKey::Increment: return "increment";
			case SettingKey::Numbers: return "numbers";
			case SettingKey::Minor: return "minor";
				// Labels Settings
			case SettingKey::Labels: return "labels";
			case SettingKey::Shift: return "shift";
			case SettingKey::Anchor: return "anchors";
			case SettingKey::Transform: return "transform";
				// Font Settings
			case SettingKey::Font: return "font";
			case SettingKey::Family: return "family";
			case SettingKey::Slant: return "slant";
			case SettingKey::Weight: return "weight";
			case SettingKey::Underline: return "underline";
			case SettingKey::Alignment: return "alignment";
			case SettingKey::ScriptOpt: return "scriptopt";
			case SettingKey::Capitalize: return "capitalization";
				// Title Settings
			case SettingKey::Text: return "text";
				// Grids Settings
			case SettingKey::Grids: return "grids";
				// Plot Settings
			case SettingKey::Axis1: return "axis1";
			case SettingKey::Axis2: return "axis2";
			case SettingKey::Axis3: return "axis3";
			case SettingKey::Coordinates: return "coordinates";
			case SettingKey::Function: return "function";
			case SettingKey::Image: return "image";
			case SettingKey::Data: return "data";
			case SettingKey::Data1: return "data1";
			case SettingKey::Data2: return "data2";
			case SettingKey::Data3: return "data3";
			case SettingKey::Gap: return "gap";
			case SettingKey::Symbols: return "symbols";
			case SettingKey::BeginAngle: return "beginangle";
			case SettingKey::EndAngle: return "endangle";
			case SettingKey::Rotation: return "rotation";
			case SettingKey::Covering: return "covering";
			case SettingKey::Area: return "area";
			case SettingKey::Bars: return "bars";
			case SettingKey::ErrorBars: return "errorbars";
				// Settings
			case SettingKey::Separator: return "separator";
			case SettingKey::Shape: return "shape";
			default: return "unknow";
		}
	}
	static inline SettingKey toSettingKey( const QString& str_key )
	{
		QString key = str_key;
		key = key.replace("-", "");
		// Element types
		if ( compare(key, "name") )
			return SettingKey::Name;
		else if ( compare(key, "id") )
			return SettingKey::ID;
		else if ( compare(key, "filename") )
			return SettingKey::FileName;
		else if ( compare(key, "working") )
			return SettingKey::Working;
		else if ( compare(key, "lang") )
			return SettingKey::Lang;
		else if ( compare(key, "layout") )
			return SettingKey::Layout;
		else if ( compare(key, "spacing") )
			return SettingKey::Spacing;
		else if ( compare(key, "margins") )
			return SettingKey::Margins;
		else if ( compare(key, "entry") )
			return SettingKey::Entry;
		else if ( compare(key, "addentry") )
			return SettingKey::AddEntry;
		else if ( compare(key, "title") )
			return SettingKey::Title;
		else if ( compare(key, "position") )
			return SettingKey::Position;
		else if ( compare(key, "width") )
			return SettingKey::Width;
		else if ( compare(key, "height") )
			return SettingKey::Height;
		else if ( compare(key, "size") )
			return SettingKey::Size;
		else if ( compare(key, "type") )
			return SettingKey::Type;
		else if ( compare(key, "format") )
			return SettingKey::Format;
		else if ( compare(key, "borders") )
			return SettingKey::Borders;
		else if ( compare(key, "hide") )
			return SettingKey::Hide;
		else if ( compare(key, "min") )
			return SettingKey::Min;
		else if ( compare(key, "max") )
			return SettingKey::Max;
		else if ( compare(key, "scale") )
			return SettingKey::Scale;
		else if ( compare(key, "option") )
			return SettingKey::Option;
		else if ( compare(key, "linkto") )
			return SettingKey::LinkTo;
		// Line Settings
		else if ( compare(key, "line") )
			return SettingKey::Line;
		else if ( compare(key, "dash") )
			return SettingKey::Dash;
		else if ( compare(key, "offset") )
			return SettingKey::Offset;
		else if ( compare(key, "color") )
			return SettingKey::Color;
		else if ( compare(key, "join") )
			return SettingKey::Join;
		else if ( compare(key, "miterlimit") )
			return SettingKey::MiterLimit;
		else if ( compare(key, "cap") )
			return SettingKey::Cap;
		// Ticks Settings
		else if ( compare(key, "ticks") )
			return SettingKey::Ticks;
		else if ( compare(key, "increment") )
			return SettingKey::Increment;
		else if ( compare(key, "numbers") )
			return SettingKey::Numbers;
		else if ( compare(key, "minor") )
			return SettingKey::Minor;
		// Labels Settings
		else if ( compare(key, "labels") )
			return SettingKey::Labels;
		else if ( compare(key, "shift") )
			return SettingKey::Shift;
		else if ( compare(key, "anchor") )
			return SettingKey::Anchor;
		else if ( compare(key, "transform") )
			return SettingKey::Transform;
		else if ( compare(key, "font") )
			return SettingKey::Font;
		else if ( compare(key, "family") )
			return SettingKey::Family;
		else if ( compare(key, "slant") )
			return SettingKey::Slant;
		else if ( compare(key, "weight") )
			return SettingKey::Weight;
		else if ( compare(key, "capitalize") )
			return SettingKey::Capitalize;
		else if ( compare(key, "underline") )
			return SettingKey::Underline;
		else if ( compare(key, "alignment") )
			return SettingKey::Alignment;
		else if ( compare(key, "scriptopt") )
			return SettingKey::ScriptOpt;
		// Title Settings
		else if ( compare(key, "text") )
			return SettingKey::Text;
		// Grids Settings
		else if ( compare(key, "grids") )
			return SettingKey::Grids;
		// Plot Settings
		else if ( compare(key, "axis1") )
			return SettingKey::Axis1;
		else if ( compare(key, "axis2") )
			return SettingKey::Axis2;
		else if ( compare(key, "axis3") )
			return SettingKey::Axis3;
		else if ( compare(key, "path") )
			return SettingKey::Path;
		else if ( compare(key, "coordinates") )
			return SettingKey::Coordinates;
		else if ( compare(key, "function") )
			return  SettingKey::Function;
		else if ( compare(key, "image") )
			return  SettingKey::Image;
		else if ( compare(key, "data") )
			return SettingKey::Data;
		else if ( compare(key, "data1") )
			return SettingKey::Data1;
		else if ( compare(key, "data2") )
			return SettingKey::Data2;
		else if ( compare(key, "data3") )
			return SettingKey::Data3;
		else if ( compare(key, "gap") )
			return SettingKey::Gap;
		else if ( compare(key, "symbols") )
			return SettingKey::Symbols;
		else if ( compare(key, "beginangle") )
			return SettingKey::BeginAngle;
		else if ( compare(key, "endangle") )
			return SettingKey::EndAngle;
		else if ( compare(key, "rotation") )
			return SettingKey::Rotation;
		else if ( compare(key, "covering") )
			return SettingKey::Covering;
		else if ( compare(key, "area") )
			return SettingKey::Area;
		else if ( compare(key, "bars") )
			return SettingKey::Bars;
		else if ( compare(key, "errorbars") )
			return SettingKey::ErrorBars;
		else if ( compare(key, "separator") )
			return SettingKey::Separator;
		else if ( compare(key, "shape") )
			return SettingKey::Shape;

		return SettingKey::NoKey;
	}


	enum class SettingType
	{
		NoType,
		Unknown,
		Boolean,
		Integer,
		IntegerList,
		Double,
		DoubleList,
		// Qt types
		String,
		StringList,
		Color,
		Point, PointF,
		Size, SizeF,
		Rect, RectF,
		LineStyle, FillStyle,
		Margins,

		Key,
		KeyList,
		ProjectItemType,
		GraphItemType,
		GraphFormat,
		Position,
		Anchor,
		AxisType,
		AxisScale,
		Layout,
		LineDash,
		LineCap,
		LineJoin
	};
	static inline QString toString( const SettingType& key )
	{
		return "NoType";
	}
	static inline SettingType toSettingType( const QString& str_key )
	{
		return SettingType::NoType;
	}


	enum class SettingValue
	{
		NoValue,
	};
	static inline QString toString( const SettingValue& key )
	{
		return "Setting::NoValue";
	}
	static inline SettingType toSettingValue( const QString& str_key )
	{

		return SettingType::NoType;
	}


	enum class ProjectItemType
	{
		NoType,
		Root,
		Project,
		Folder,
		DataSheet,
		Graph,
		Image,
		TextFile,
		File
	};
	static inline QString toString( const ProjectItemType& key )
	{
		switch (key)
		{
			case ProjectItemType::Root: return "ROOT";
			case ProjectItemType::Project: return "PROJECT";
			case ProjectItemType::Folder: return "FOLDER";
			case ProjectItemType::DataSheet: return "DATASHEET";
			case ProjectItemType::Graph: return "GRAPH";
			case ProjectItemType::Image: return "IMAGE";
			case ProjectItemType::TextFile: return "TEXTFILE";
			case ProjectItemType::File: return "FILE";
			case ProjectItemType::NoType: return "NOTYPE";
			default: return "UNKNOWN";
		}
	}
	static inline ProjectItemType toProjectType( const QString& str_key )
	{
		QString key = str_key;
		key = key.replace("-", "");
		if ( compare(key, "root") )
			return ProjectItemType::Root;
		else if ( compare(key, "project") )
			return ProjectItemType::Project;
		else if ( compare(key, "datasheet") )
			return ProjectItemType::DataSheet;
		else if ( compare(key, "graph") )
			return ProjectItemType::Graph;
		else if ( compare(key, "image") )
			return ProjectItemType::Image;
		else if ( compare(key, "textfile") )
			return ProjectItemType::TextFile;
		else if ( compare(key, "file") )
			return ProjectItemType::File;
		return ProjectItemType::NoType;
	}


	enum class GraphItemType
	{
		NoType,
		Graph,
		Frame,
		Axis,
		Plot,
		Legend,
		Title,
		Shape,
		Template,
		Layer
	};
	static inline QString toString( const GraphItemType& key )
	{
		switch (key)
		{
			case GraphItemType::Graph: return "GRAPH";
			case GraphItemType::Frame: return "FRAME";
			case GraphItemType::Axis: return "AXIS";
			case GraphItemType::Plot: return "PLOT";
			case GraphItemType::Legend: return "LEGEND";
			case GraphItemType::Title: return "TITLE";
			case GraphItemType::Shape: return "SHAPE";
			case GraphItemType::Template: return "TEMPLATE";
			case GraphItemType::Layer: return "LAYER";
			default: return "NOTYPE";
		}
	}
	static inline GraphItemType toGraphItemType( const QString& str_key )
	{
		QString key = str_key;
		key = key.replace("-", "");
		if ( compare(key, "graph") )
			return GraphItemType::Graph;
		if ( compare(key, "frame") )
			return GraphItemType::Frame;
		else if ( compare(key, "axis") )
			return GraphItemType::Axis;
		else if ( compare(key, "plot") )
			return GraphItemType::Plot;
		else if ( compare(key, "legend") )
			return GraphItemType::Legend;
		else if ( compare(key, "title") )
			return GraphItemType::Title;
		else if ( compare(key, "shape") )
			return GraphItemType::Shape;
		else if ( compare(key, "template") )
			return GraphItemType::Template;
		else if ( compare(key, "layer") )
			return GraphItemType::Layer;
		return GraphItemType::NoType;
	}


	enum class GraphFormat
	{
		PNG,
		PDF,
		SVG,
		PS,
		TIFF,
		JPEG,
		BMP,
	};
	static inline QString toString( const GraphFormat& key )
	{
		switch (key)
		{
			case GraphFormat::PDF: return "Format::PDF";
			case GraphFormat::SVG: return "Format::SVG";
			case GraphFormat::PS: return "Format::PS";
			case GraphFormat::TIFF: return "Format::TIFF";
			case GraphFormat::JPEG: return "Format::JPEG";
			case GraphFormat::BMP: return "Format::BMP";
			default: return "Format::PNG";
		}
	}
	static inline GraphFormat toGraphFormat( const QString& str_key )
	{
		QString key = str_key;
		key = key.replace("-", "");
		if ( compare(key, "pdf") )
			return GraphFormat::PDF;
		else if ( compare(key, "svg") )
			return GraphFormat::SVG;
		else if ( compare(key, "ps") )
			return GraphFormat::PS;
		else if ( compare(key, "tiff") )
			return GraphFormat::TIFF;
		else if ( compare(key, "jpeg") )
			return GraphFormat::JPEG;
		else if ( compare(key, "bmp") )
			return GraphFormat::BMP;
		return GraphFormat::PNG;
	}


	enum class Position
	{
		NoPosition,
		Left,
		Right,
		Top,
		Bottom,
		Center,
		TopLeft,
		TopRight,
		BottomLeft,
		BottomRight,
		Outside,
		Inside,
		BaselineLeft,
		Baseline,
		BaselineRight,
		BothSides,
	};
	static inline QString toString( const Position& key )
	{
		switch (key)
		{
			case Position::Left: return "Position::Left";
			case Position::Right: return "Position::Right";
			case Position::Top: return "Position::Top";
			case Position::Bottom: return "Position::Bottom";
			case Position::Center: return "Position::Center";
			case Position::TopLeft: return "Position::TopLeft";
			case Position::TopRight: return "Position::TopRight";
			case Position::BottomLeft: return "Position::BottomLeft";
			case Position::BottomRight: return "Position::BottomRight";
			case Position::Outside: return "Position::Outside";
			case Position::Inside: return "Position::Inside";
			case Position::BaselineLeft: return "Position::BaselineLeft";
			case Position::Baseline: return "Position::Baseline";
			case Position::BaselineRight: return "Position::BaselineRight";
			default: return "Position::NoPostion";
		}
	}
	static inline Position toPosition( const QString& str_key )
	{
		QString key = str_key;
		key = key.replace("-", "");
		if ( compare(key, "left") )
			return Position::Left;
		else if ( compare(key, "right") )
			return Position::Right;
		else if ( compare(key, "top") )
			return Position::Top;
		else if ( compare(key, "bottom") )
			return Position::Bottom;
		else if ( compare(key, "center") )
			return Position::Center;
		else if ( compare(key, "topleft") )
			return Position::TopLeft;
		else if ( compare(key, "topright") )
			return Position::TopRight;
		else if ( compare(key, "bottomleft") )
			return Position::BottomLeft;
		else if ( compare(key, "bottomright") )
			return Position::BottomRight;
		else if ( compare(key, "outside") )
			return Position::Outside;
		else if ( compare(key, "inside") )
			return Position::Inside;
		else if ( compare(key, "baselineleft") )
			return Position::BaselineLeft;
		else if ( compare(key, "baseline") )
			return Position::Baseline;
		else if ( compare(key, "baselineright") )
			return Position::BaselineRight;
		return Position::NoPosition;
	}


	enum class AxisType
	{
		X,
		Y,
		Polar,
		Radar
	};
	static inline QString toString( const AxisType& key )
	{
		switch (key)
		{
			case AxisType::Y: return "AxisType::Y";
			case AxisType::Polar: return "AxisType::Polar";
			case AxisType::Radar: return "AxisType::Radar";
			default: return "AxisType::X";
		}
	}
	static inline AxisType toAxisType( const QString& str_key )
	{
		QString key = str_key;
		key = key.replace("-", "");
		if ( compare(key, "y") )
			return AxisType::Y;
		else if ( compare(key, "polar") )
			return AxisType::Polar;
		else if ( compare(key, "radar") )
			return AxisType::Radar;
		return AxisType::X;
	}


	enum class AxisScale
	{
		Linear,
		Log10,
		Log,
		LogX,
		Reciprocal,
		OffsetReciprocal
	};
	static inline QString toString( const AxisScale& key )
	{
		switch (key)
		{
			case AxisScale::Log10: return "AxisScale::Log10";
			case AxisScale::Log: return "AxisScale::Log";
			case AxisScale::LogX: return "AxisScale::LogX";
			case AxisScale::Reciprocal: return "AxisScale::Reciprocal";
			case AxisScale::OffsetReciprocal: return "AxisScale::OffsetReciprocal";
			default: return "AxisScale::Linear";
		}
	}
	static inline AxisScale toAxisScale( const QString& str_key )
	{
		QString key = str_key;
		key = key.replace("-", "");
		if ( compare(key, "log10") )
			return AxisScale::Log10;
		else if ( compare(key, "log") )
			return AxisScale::Log;
		else if ( compare(key, "logx") )
			return AxisScale::LogX;
		else if ( compare(key, "reciprocal") )
			return AxisScale::Reciprocal;
		else if ( compare(key, "offsetreciprocal") )
			return AxisScale::OffsetReciprocal;
		return AxisScale::Linear;
	}


	enum class Layout
	{
		NoLayout,
		Vertical,
		Horizontal,
		Table,
		Row,
		Column,
	};
	static inline QString toString( const Layout& key )
	{
		switch (key)
		{
			case Layout::Vertical: return "Layout::Vertical";
			case Layout::Horizontal: return "Layout::Horizontal";
			case Layout::Table: return "Layout::Table";
			case Layout::Row: return "Layout::Row";
			case Layout::Column: return "Layout::Column";
			default: return "Layout::NoLayout";
		}
	}
	static inline Layout toLayout( const QString& str_key )
	{
		QString key = str_key;
		key = key.replace("-", "");
		if ( compare(key, "vertical") )
			return Layout::Vertical;
		else if ( compare(key, "horizontal") )
			return Layout::Horizontal;
		else if ( compare(key, "table") )
			return Layout::Table;
		else if ( compare(key, "row") )
			return Layout::Row;
		else if ( compare(key, "column") )
			return Layout::Column;
		return Layout::NoLayout;
	}




}

typedef GFT::SettingKey SettingKey;
typedef GFT::SettingType SettingType;
typedef GFT::SettingValue SettingValue;
typedef GFT::ProjectItemType ProjectItemType;
typedef GFT::GraphItemType GraphItemType;
typedef GFT::GraphFormat GraphFormat;
typedef GFT::Position Position;
typedef GFT::AxisType AxisType;
typedef GFT::AxisScale AxisScale;
typedef GFT::Layout Layout;


#endif // GFT_H
