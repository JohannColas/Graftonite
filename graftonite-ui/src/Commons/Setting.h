#ifndef SETTING_H
#define SETTING_H

#include <QVariant>

#include "GFT.h"
#include <QPen>

#include <QMap>
#include <typeinfo>

class Setting
{
public:
	static QString toString( const SettingKey& key );
	static QString toString( const QList<SettingKey>& key );
	static QList<SettingKey> toKey( const QString& key );
	static SettingKey toSimpleKey( const QString& str_key );
	bool isProjectItemType() const;

	Setting();
	Setting( const QString& line );

	template<class K, class T>
	Setting( const K& key, const T& value )
	{
		setKey(key);
		setValue(value);
	}

	QList<SettingKey> key() const;
	void setKey( const QString& key );
	void setKey( const SettingKey& key );
	void setKey( const QList<SettingKey>& key );
	QString keyToString() const;
	SettingType type() const;
	void setType( const SettingType& type );

	template<typename T>
	static inline SettingType getSettingType( const T& )
	{
		if ( typeid(T) == typeid(bool) )
			return SettingType::Boolean;
		else if ( typeid(T) == typeid(int) )
			return SettingType::Integer;
		else if ( typeid(T) == typeid(double) )
			return SettingType::Double;
		else if ( typeid(T) == typeid(QList<int>) )
			return SettingType::IntegerList;
		else if ( typeid(T) == typeid(QList<double>) )
			return SettingType::DoubleList;
		// Qt Types
		else if ( typeid(T) == typeid(QString) )
			return SettingType::String;
		else if ( typeid(T) == typeid(const char*) )
			return SettingType::String;
		else if ( typeid(T) == typeid(QList<QString>) )
			return SettingType::StringList;
		else if ( typeid(T) == typeid(QColor) )
			return SettingType::Color;
		else if ( typeid(T) == typeid(QPoint) )
			return SettingType::Point;
		else if ( typeid(T) == typeid(QPointF) )
			return SettingType::PointF;
		else if ( typeid(T) == typeid(QSize) )
			return SettingType::Size;
		else if ( typeid(T) == typeid(QSizeF) )
			return SettingType::SizeF;
		else if ( typeid(T) == typeid(QRect) )
			return SettingType::Rect;
		else if ( typeid(T) == typeid(QRectF) )
			return SettingType::RectF;
		else if ( typeid(T) == typeid(QPen) )
			return SettingType::LineStyle;
		else if ( typeid(T) == typeid(QBrush) )
			return SettingType::FillStyle;
		// Graph Setting Types
		else if ( typeid(T) == typeid(SettingKey) )
			return SettingType::Key;
		else if ( typeid(T) == typeid(QList<SettingKey>) )
			return SettingType::KeyList;
		else if ( typeid(T) == typeid(ProjectItemType) )
			return SettingType::ProjectItemType;
		else if ( typeid(T) == typeid(GraphItemType) )
			return SettingType::GraphItemType;
		else if ( typeid(T) == typeid(GraphFormat) )
			return SettingType::GraphFormat;
		else if ( typeid(T) == typeid(Position) )
			return SettingType::Position;
//		else if ( typeid(T) == typeid(Anchor) )
//			return SettingType::Anchor;
		else if ( typeid(T) == typeid(AxisType) )
			return SettingType::AxisType;
		else if ( typeid(T) == typeid(AxisScale) )
			return SettingType::AxisScale;
		else if ( typeid(T) == typeid(Layout) )
			return SettingType::Layout;
//		else if ( typeid(T) == typeid(LineDash) )
//			return SettingType::LineDash;
//		else if ( typeid(T) == typeid(LineCap) )
//			return SettingType::LineCap;
//		else if ( typeid(T) == typeid(LineJoin) )
//			return SettingType::LineJoin;
		return SettingType::Unknown;
	}
	/**
	 * @brief value
	 * @param value
	 * @return
	 *
	 * Type of V specialized
	 * @li SettingKey
	 * @li QList<SettingKey>
	 * @li Position
	 * @li Anchor
	 * @li ProjectItemType
	 * @li GraphItemType
	 * @li GraphFormat
	 * @li AxisType
	 * @li AxisScale
	 * @li Position
	 * @li Anchor
	 *
	 */
	template<typename V>
	bool value( V& value ) const
	{
		if ( Setting::getSettingType(value) == this->type() )
		{
			value = _value.value<V>();
			return true;
		}
		qDebug() << value << QString("type (%1) is not similar to SettingType of the Setting :").arg(typeid(V).name()) << show();
		return false;
	}
	bool value( SettingKey& value ) const
	{
		if ( this->type() == SettingType::Key )
		{
			value = (SettingKey)_value.value<int>();
			return true;
		}
		qDebug() << (int)value << QString("SettingKey is not similar to SettingType of the Setting :") << show();
		return false;
	}
	bool value( QList<SettingKey>& value ) const
	{
		if ( this->type() == SettingType::KeyList )
		{
			QList<int> lst_int = _value.value<QList<int>>();
			value.clear();
			for ( int integer : lst_int )
				value.append( (SettingKey)integer );
			return true;
		}
		qDebug() << QString("QList<SettingKey> is not similar to SettingType of the Setting :") << show();
		return false;
	}
	template<typename V>
	V value()
	{
		V val;
		value(val);
		return val;
	}

	template<typename V>
	void setValue( V& value )
	{
		_type = Setting::getSettingType(value);
		_value = QVariant::fromValue(value);
	}
	void setValue( const char* value )
	{
		_type = SettingType::String;
		_value = QVariant::fromValue(QString(value));
	}
	void setValue( const SettingKey& value )
	{
		_type =	SettingType::Key;
		_value = (int)value;
	}
	void setValue( const QList<SettingKey>& value )
	{
		_type = SettingType::KeyList;
		QList<int> lst_int;
		for ( const SettingKey& key : value )
			lst_int.append( (int)key );
		_value = QVariant::fromValue(lst_int);
	}

	bool toBool( bool* ok = nullptr ) const;
	int toInt( bool* ok = nullptr ) const;
	QList<int> toIntList( bool* ok = nullptr ) const;
	double toDouble( bool* ok = nullptr ) const;
	QList<double> toDoubleList( bool* ok = nullptr ) const;
	QString toString( bool* ok = nullptr ) const;
	QList<QString> toStringList( bool* ok = nullptr ) const;
	QColor toColor( bool* ok = nullptr ) const;
	QPoint toPoint( bool* ok = nullptr ) const;
	QPointF toPointF( bool* ok = nullptr ) const;
	QSize toSize( bool* ok = nullptr ) const;
	QSizeF toSizeF( bool* ok = nullptr ) const;
	QRect toRect( bool* ok = nullptr ) const;
	QRectF toRectF( bool* ok = nullptr ) const;
	QPen toPen( bool* ok = nullptr ) const;
	QBrush toBrush( bool* ok = nullptr ) const;
	QString valueToString() const;
	void read( const QString& line );

	QString show() const;
	QString save() const;

//	SettingKey position() const;
//	SettingKey anchor() const;

	Setting& operator<< ( const QString& str );
	friend QString& operator>> ( QString& str, const Setting& setting )
	{
		return str;
	}
	friend QTextStream& operator<< ( QTextStream& out, const Setting& setting )
	{
		out << setting.show();

		return out;
	}
	bool operator== ( const Setting& setting );
	bool operator== ( const QList<SettingKey>& key );


private:
	QList<SettingKey> _key;
	SettingType _type = SettingType::NoType;
	QVariant _value;
	QVariant _defvalue;
};
typedef QList<SettingKey> Keys;

#endif // SETTING_H
