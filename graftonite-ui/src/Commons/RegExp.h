#ifndef REGEXP_H
#define REGEXP_H

#include <QRegularExpression>

class RegExp
        : public QRegularExpression
{
public:
	enum Type
	{
		NoType,
		Key,
		Position,
		Anchor,
		Boolean,
		Integer,
		Double,
		String,
		List,
		IntegerList,
		DoubleList,
		StringList,
		Color,
	};

	RegExp( const RegExp::Type& type );
	RegExp& setType( const RegExp::Type& type );
	RegExp::Type autoSet( const QString& string );
	bool isKey( const QString& string );

};

#endif // REGEXP_H
