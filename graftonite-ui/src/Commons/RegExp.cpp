#include "RegExp.h"

RegExp::RegExp( const Type& type )
    : QRegularExpression()
{
	setType(type);
}

RegExp& RegExp::setType( const Type& type )
{
	QString pattern;
	switch (type)
	{
		case RegExp::Key:
		{
			pattern = "^Key\\(((a-z)|(A-Z))*\\)$";
			pattern = "^((a-z)|(A-Z))*$";
			break;
		}
		case RegExp::Position:
		{
			pattern = "^Position::(\\w|\\.)$";
			break;
		}
		case RegExp::Anchor:
		{
			pattern = "^Anchor::(\\w|\\.)$";
			break;
		}
		case RegExp::Boolean:
		{
			pattern = "^(?<boolean>(true|false))$";
			break;
		}
		case RegExp::Integer:
		{
			pattern = "^(?<integer>\\d+)$";
			break;
		}
		case RegExp::Double:
		{
			pattern = "^(?<double>\\d*(\\.|,){1}(\\d)*)$";
			break;
		}
		case RegExp::String:
		{
			pattern = "^\"(?<string>.*)\"$";
			break;
		}
		case RegExp::List:
		{
			pattern = "^Anchor::(\\w|\\.)$";
			break;
		}
		case RegExp::IntegerList:
		{
			pattern = "^\\{(?<integer>\\d+?)((\\,)|(\\}$))";
			break;
		}
		case RegExp::DoubleList:
		{
			pattern = "^\\{(?<double>\\d+(\\.|,){0,1}(\\d)*?)((\\,)|(\\}$))";
			break;
		}
		case RegExp::StringList:
		{
			pattern = "^\\{\"(?<string>.*?)\"((\\,)|(\\}$))";
			break;
		}
		case RegExp::Color:
		{
			pattern = "^Color\\(((?<integer>\\d+?)((\\,)|(\\)$))|(?<name>\\w*?)\\)$)";
			break;
		}
		default:
		{
			break;
		}
	}
	this->setPattern( pattern );
	return (*this);
}

RegExp::Type RegExp::autoSet( const QString& string )
{
	return RegExp::NoType;
}

bool RegExp::isKey( const QString& string )
{
	if ( setType(RegExp::Key).match(string).hasMatch() )
		return true;
	return false;
}
