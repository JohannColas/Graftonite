#ifndef FILES_H
#define FILES_H

#include <QString>
#include <QTime>
/*------------------------------*/
class QString;
class QDomDocument;
/*------------------------------*/
/*------------------------------*/
/*------------------------------*/
class Files
{
public:
//	static QString readFile( const QString& path );
	static void readFile( const QString& path, QString& content );
	static QDomDocument readXML( const QString& path );
	static void saveXML( const QString& path, const QDomDocument& document );
	/*------------------------------*/
	static QString localPath();
	static QString tmpPath();
	/*------------------------------*/
	static QString generateUniqueID( int length = 16, const QString& allowchars = QString("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ") );
};
/*------------------------------*/
/*------------------------------*/
/*------------------------------*/
#endif // FILES_H
