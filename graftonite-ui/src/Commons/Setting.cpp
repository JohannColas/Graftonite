#include "Setting.h"

#include <QPen>
//#include <QColor>
////#include <QList>
//#include <QPointF>
//#include <QRectF>
//#include <QSizeF>

QString Setting::toString( const SettingKey& key )
{
	switch (key)
	{
		case SettingKey::Name: return "name";
		case SettingKey::ID: return "id";
//		case SettingKey::Root: return "root";
//		case SettingKey::Project: return "project";
//		case SettingKey::Folder: return "folder";
//		case SettingKey::DataSheet: return "datasheet";
			//		case SettingKey::Graph: return "name";
			//		case SettingKey::Image: return "image";
//		case SettingKey::TextFile: return "textfile";
//		case SettingKey::File: return "file";
		case SettingKey::FileName: return "filename";
//		case SettingKey::PNG: return "png";
//		case SettingKey::PDF: return "pdf";
//		case SettingKey::SVG: return "svg";
//		case SettingKey::PS: return "ps";
		case SettingKey::Working: return "working";
		case SettingKey::Lang: return "lang";
//		case SettingKey::Log: return "log";
			// Element types
			//		case SettingKey::NoType: return "unknown type";
//		case SettingKey::Current: return "current";
//		case SettingKey::Graph: return "graph";
//		case SettingKey::Template: return "template";
//		case SettingKey::Layer: return "layer";
//		case SettingKey::Frame: return "frame";
//		case SettingKey::Axis: return "axis";
//		case SettingKey::Plot: return "plot";
//		case SettingKey::Legend: return "legend";
		case SettingKey::Layout: return "layout";
//		case SettingKey::Vertical: return "vertical";
//		case SettingKey::Horizontal: return "horizontal";
//		case SettingKey::Table: return "table";
//		case SettingKey::Column: return "column";
//		case SettingKey::Row: return "row";
		case SettingKey::Spacing: return "spacing";
		case SettingKey::Margins: return "margins";
		case SettingKey::Entry: return "entry";
		case SettingKey::AddEntry: return "addentry";
		case SettingKey::Title: return "title";
//			// Geometry Settings
//		case SettingKey::X: return "x";
//		case SettingKey::Y: return "y";
		case SettingKey::Position: return "position";
		case SettingKey::Width: return "width";
		case SettingKey::Height: return "height";
		case SettingKey::Size: return "size";
		case SettingKey::Type: return "type";
		case SettingKey::Format: return "format";
			//  Settings
			//			case SettingKey::Background: return "background";
			//			case SettingKey::Fill: return "fill";
		case SettingKey::Borders: return "borders";
			// Positions
//		case SettingKey::Center: return "center";
//		case SettingKey::Bottom: return "bottom";
//		case SettingKey::Top: return "top";
//		case SettingKey::Right: return "right";
//		case SettingKey::Left: return "left";
//		case SettingKey::Outside: return "outside";
//		case SettingKey::Inside: return "inside";
			// Anchors
//		case SettingKey::BottomLeft: return "bottomleft";
//		case SettingKey::BottomRight: return "bottomright";
//		case SettingKey::TopLeft: return "topleft";
//		case SettingKey::TopRight: return "topright";
//		case SettingKey::BaselineLeft: return "baselineleft";
//		case SettingKey::Baseline: return "baseline";
//		case SettingKey::BaselineRight: return "baselineright";
			// Specific Axis Settings
		case SettingKey::Hide: return "hide";
		case SettingKey::Min: return "min";
		case SettingKey::Max: return "max";
		case SettingKey::Scale: return "scale";
		case SettingKey::Option: return "option";
		case SettingKey::LinkTo: return "linkto";
			// Line Settings
		case SettingKey::Line: return "line";
		case SettingKey::Dash: return "dash";
		case SettingKey::Offset: return "offset";
		case SettingKey::Color: return "color";
		case SettingKey::Join: return "join";
//		case SettingKey::Round: return "round";
//		case SettingKey::Bevel: return "bevel";
//		case SettingKey::Miter: return "miter";
		case SettingKey::MiterLimit: return "miterlimit";
		case SettingKey::Cap: return "cap";
//		case SettingKey::Butt: return "butt";
//		case SettingKey::Square: return "square";
			// Ticks Settings
		case SettingKey::Ticks: return "ticks";
		case SettingKey::Increment: return "increment";
		case SettingKey::Numbers: return "numbers";
		case SettingKey::Minor: return "minor";
			// Labels Settings
		case SettingKey::Labels: return "labels";
		case SettingKey::Shift: return "shift";
		case SettingKey::Anchor: return "anchors";
		case SettingKey::Transform: return "transform";
			// Font Settings
		case SettingKey::Font: return "font";
		case SettingKey::Family: return "family";
		case SettingKey::Slant: return "slant";
		case SettingKey::Weight: return "weight";
		case SettingKey::Underline: return "underline";
		case SettingKey::Alignment: return "alignment";
		case SettingKey::ScriptOpt: return "scriptopt";
		case SettingKey::Capitalize: return "capitalization";
			// Title Settings
		case SettingKey::Text: return "text";
			// Grids Settings
		case SettingKey::Grids: return "grids";
			// Plot Settings
		case SettingKey::Axis1: return "axis1";
		case SettingKey::Axis2: return "axis2";
		case SettingKey::Axis3: return "axis3";
		case SettingKey::Coordinates: return "coordinates";
		case SettingKey::Function: return "function";
		case SettingKey::Image: return "image";
		case SettingKey::Data: return "data";
		case SettingKey::Data1: return "data1";
		case SettingKey::Data2: return "data2";
		case SettingKey::Data3: return "data3";
		case SettingKey::Gap: return "gap";
		case SettingKey::Symbols: return "symbols";
		case SettingKey::BeginAngle: return "beginangle";
		case SettingKey::EndAngle: return "endangle";
		case SettingKey::Rotation: return "rotation";
		case SettingKey::Covering: return "covering";
		case SettingKey::Area: return "area";
		case SettingKey::Bars: return "bars";
		case SettingKey::ErrorBars: return "errorbars";
			// Settings
		case SettingKey::Path: return "path";
		case SettingKey::Separator: return "separator";
		case SettingKey::Shape: return "shape";
		default: return "unknow";
	}
}

QList<SettingKey> Setting::toKey( const QString& key )
{

	return {SettingKey::NoKey};
}

SettingKey Setting::toSimpleKey( const QString& str_key )
{
	QString key = str_key;
	key = key.replace("-", "");
	// Element types
	if ( key.compare("name", Qt::CaseInsensitive) == 0 )
		return SettingKey::Name;
	if ( key.compare("id", Qt::CaseInsensitive) == 0 )
		return SettingKey::ID;
	//
//	else if ( key.compare("project", Qt::CaseInsensitive) == 0 )
//		return SettingKey::Project;
//	else if ( key.compare("folder", Qt::CaseInsensitive) == 0 )
//		return SettingKey::Folder;
//	else if ( key.compare("datasheet", Qt::CaseInsensitive) == 0 )
//		return SettingKey::DataSheet;
	//
	else if ( key.compare("filename", Qt::CaseInsensitive) == 0 )
		return SettingKey::FileName;
	else if ( key.compare("working", Qt::CaseInsensitive) == 0 )
		return SettingKey::Working;
	else if ( key.compare("lang", Qt::CaseInsensitive) == 0 )
		return SettingKey::Lang;
//	else if ( key == "png" )
//		return SettingKey::PNG;
//	else if ( key == "pdf" )
//		return SettingKey::PDF;
//	else if ( key == "svg" )
//		return SettingKey::SVG;
//	else if ( key == "ps" )
//		return SettingKey::PS;
//	else if ( key == "cr"||  key == "current" )
//		return SettingKey::Current;
//	else if ( key == "g"|| key == "graph" )
//		return SettingKey::Graph;
//	else if ( key == "tpl"|| key == "template" )
//		return SettingKey::Template;
//	else if ( key == "ly"|| key == "layer" )
//		return SettingKey::Layer;
//	else if ( key == "f" || key == "frame" )
//		return SettingKey::Frame;
//	else if ( key == "a" || key == "axis" )
//		return SettingKey::Axis;
//	else if ( key == "pl" || key == "plot" )
//		return SettingKey::Plot;
//	else if ( key == "lg" || key == "legend" )
//		return SettingKey::Legend;
	else if ( key == "lyt" || key == "layout" )
		return SettingKey::Layout;
//	else if ( key == "ver" || key == "vertical" )
//		return SettingKey::Vertical;
//	else if ( key == "hor" || key == "horizontal" )
//		return SettingKey::Horizontal;
//	else if ( key == "tab" || key == "table" )
//		return SettingKey::Table;
//	else if ( key == "col" || key == "column" )
//		return SettingKey::Column;
//	else if ( key == "row")
//		return SettingKey::Row;
	else if ( key == "spc" || key == "spacing" )
		return SettingKey::Spacing;
	else if ( key == "mgn" || key == "margins" )
		return SettingKey::Margins;
	else if ( key == "en" || key == "entry" )
		return SettingKey::Entry;
	else if ( key == "aen" || key == "addentry" )
		return SettingKey::AddEntry;
	else if ( key == "tl" || key == "title" )
		return SettingKey::Title;
//	else if ( key == "layers" )
//		return SettingKey::Layer;
//	else if ( key == "frames" )
//		return SettingKey::Frame;
//	else if ( key == "axes" )
//		return SettingKey::Axis;
//	else if ( key == "plots" )
//		return SettingKey::Plot;
//	else if ( key == "legends" )
//		return SettingKey::Legend;
	else if ( key == "titles" )
		return SettingKey::Title;
	// Geometry Settings
//	else if ( key == "x" )
//		return SettingKey::X;
//	else if ( key == "y" )
//		return SettingKey::Y;
	else if ( key == "p" || key == "position" )
		return SettingKey::Position;
	else if ( key == "w" || key == "width" )
		return SettingKey::Width;
	else if ( key == "h" || key == "height" )
		return SettingKey::Height;
	else if ( key == "s" || key == "size" )
		return SettingKey::Size;
	else if ( key == "ty" || key == "type" )
		return SettingKey::Type;
	else if ( key == "fm" || key == "format" )
		return SettingKey::Format;
	//  Settings
	//		else if ( key == "bg" || key == "background" )
	//			return SettingKey::Background;
	//		else if ( key == "fl" || key == "fill" )
	//			return SettingKey::Fill;
	else if ( key == "bd" || key == "borders" )
		return SettingKey::Borders;
	// Positions
//	else if ( key == "c" || key == "center" )
//		return SettingKey::Center;
//	else if ( key == "b" || key == "bottom" )
//		return SettingKey::Bottom;
//	else if ( key == "t" || key == "top" )
//		return SettingKey::Top;
//	else if ( key == "l" || key == "left" )
//		return SettingKey::Left;
//	else if ( key == "r" || key == "right" )
//		return SettingKey::Right;
//	else if ( key == "o" || key == "outside" )
//		return SettingKey::Outside;
//	else if ( key == "i" || key == "inside" )
//		return SettingKey::Inside;
//	// Anchors
//	else if ( key == "bl" || key == "bottomleft" )
//		return SettingKey::BottomLeft;
//	else if ( key == "br" || key == "bottomright" )
//		return SettingKey::BottomRight;
//	else if ( key == "tl" || key == "topleft" )
//		return SettingKey::TopLeft;
//	else if ( key == "tr" || key == "topright" )
//		return SettingKey::TopRight;
//	else if ( key == "bsf" || key == "baselineleft" )
//		return SettingKey::BaselineLeft;
//	else if ( key == "bs" || key == "baseline" )
//		return SettingKey::Baseline;
//	else if ( key == "bsr" || key == "baselineright" )
//		return SettingKey::BaselineRight;
	// Specific Axis Settings
	else if ( key == "hd" || key == "hide" )
		return SettingKey::Hide;
	else if ( key == "mn" || key == "min" )
		return SettingKey::Min;
	else if ( key == "mx" || key == "max" )
		return SettingKey::Max;
	else if ( key == "sc" || key == "scale" )
		return SettingKey::Scale;
	else if ( key == "opt" || key == "option" )
		return SettingKey::Option;
//	else if ( key == "lin" || key == "linear" )
//		return SettingKey::Linear;
//	else if ( key == "log10" )
//		return SettingKey::Log10;
//	else if ( key == "log" )
//		return SettingKey::Log;
//	else if ( key == "logx" )
//		return SettingKey::LogX;
//	else if ( key == "rcp" || key == "reciprocal" )
//		return SettingKey::Reciprocal;
//	else if ( key == "orcp" || key == "offsetreciprocal" )
//		return SettingKey::OffsetReciprocal;
	else if ( key == "lk" || key == "linkto" )
		return SettingKey::LinkTo;
	// Line Settings
	else if ( key == "ln" || key == "line" )
		return SettingKey::Line;
	else if ( key == "ds" || key == "dash" )
		return SettingKey::Dash;
	else if ( key == "of" || key == "offset" )
		return SettingKey::Offset;
	else if ( key == "cl" || key == "color" )
		return SettingKey::Color;
	else if ( key == "jn" || key == "join" )
		return SettingKey::Join;
//	else if ( key == "rd" || key == "round" )
//		return SettingKey::Round;
//	else if ( key == "bv" || key == "bevel" )
//		return SettingKey::Bevel;
//	else if ( key == "mt" || key == "miter" )
//		return SettingKey::Miter;
	else if ( key == "ml" || key == "miterlimit" )
		return SettingKey::MiterLimit;
	else if ( key == "cp" || key == "cap" )
		return SettingKey::Cap;
//	else if ( key == "bt" || key == "butt" )
//		return SettingKey::Butt;
//	else if ( key == "sq" || key == "square" )
//		return SettingKey::Square;
	// Ticks Settings
	else if ( key == "tk" || key == "ticks" )
		return SettingKey::Ticks;
	else if ( key == "inc" || key == "increment" )
		return SettingKey::Increment;
	else if ( key == "nb" || key == "numbers" )
		return SettingKey::Numbers;
	else if ( key == "mi" || key == "minor" )
		return SettingKey::Minor;
	// Labels Settings
	else if ( key == "lb" || key == "labels" )
		return SettingKey::Labels;
	else if ( key == "sh" || key == "shift" )
		return SettingKey::Shift;
	else if ( key == "anc" || key == "anchor" )
		return SettingKey::Anchor;
	else if ( key == "tr" || key == "transform" )
		return SettingKey::Transform;
	else if ( key == "ft" || key == "font" )
		return SettingKey::Font;
	else if ( key == "fam" || key == "family" )
		return SettingKey::Family;
	else if ( key == "slt" || key == "slant" )
		return SettingKey::Slant;
	else if ( key == "wg" || key == "weight" )
		return SettingKey::Weight;
	else if ( key == "cpt" || key == "capitalization" )
		return SettingKey::Capitalize;
	else if ( key == "cpt" || key == "underline" )
		return SettingKey::Underline;
	else if ( key == "alg" || key == "alignment" )
		return SettingKey::Alignment;
	else if ( key == "sco" || key == "scriptopt" )
		return SettingKey::ScriptOpt;
	// Title Settings
	else if ( key == "tx" || key == "text" )
		return SettingKey::Text;
	// Grids Settings
	else if ( key == "gd" || key == "grids" )
		return SettingKey::Grids;
	// Plot Settings
	else if ( key == "a1" || key == "axis1" )
		return SettingKey::Axis1;
	else if ( key == "a2" || key == "axis2" )
		return SettingKey::Axis2;
	else if ( key == "a3" || key == "axis3" )
		return SettingKey::Axis3;
	else if ( key == "pth" || key == "path" )
		return SettingKey::Path;
	else if ( key == "cd" || key == "coordinates" )
		return SettingKey::Coordinates;
	else if ( key == "fn" || key == "function" )
		return  SettingKey::Function;
	else if ( key == "img" || key == "image" )
		return  SettingKey::Image;
	else if ( key == "dt" || key == "data" )
		return SettingKey::Data;
	else if ( key == "dt1" || key == "data1" )
		return SettingKey::Data1;
	else if ( key == "dt2" || key == "data2" )
		return SettingKey::Data2;
	else if ( key == "dt3" || key == "data3" )
		return SettingKey::Data3;
	else if ( key == "gp" || key == "gap" )
		return SettingKey::Gap;
	else if ( key == "sy" || key == "symbols" )
		return SettingKey::Symbols;
	else if ( key == "bang" || key == "beginangle" )
		return SettingKey::BeginAngle;
	else if ( key == "eang" || key == "endangle" )
		return SettingKey::EndAngle;
	else if ( key == "rot" || key == "rotation" )
		return SettingKey::Rotation;
	else if ( key == "cov" || key == "covering" )
		return SettingKey::Covering;
	else if ( key == "ar" || key == "area" )
		return SettingKey::Area;
	else if ( key == "br" || key == "bars" )
		return SettingKey::Bars;
	else if ( key == "eb" || key == "errorbars" )
		return SettingKey::ErrorBars;
	else if ( key == "sep" || key == "separator" )
		return SettingKey::Separator;
	else if ( key == "sh" || key == "shape" )
		return SettingKey::Shape;
	return SettingKey::Unknown;
}

bool Setting::isProjectItemType() const
{
	if ( _key.size() == 1 )
	{
//		if ( _key.first() == SettingKey::Project ||
//		     _key.first() == SettingKey::Folder ||
//		     _key.first() == SettingKey::DataSheet ||
//		     _key.first() == SettingKey::Graph ||
//		     _key.first() == SettingKey::Image ||
//		     _key.first() == SettingKey::File )
//			return true;
	}
	return false;
}

Setting::Setting()
{
}

Setting::Setting( const QString& line )
{
	read( line );
}

QList<SettingKey> Setting::key() const
{
	return _key;
}

void Setting::setKey( const QString& key )
{
	_key = Setting::toKey( key );
}

void Setting::setKey( const SettingKey& key )
{
	_key.clear();
	_key.append(key);
}

QString Setting::keyToString() const
{
	QString str = "nokey";
	if ( !_key.isEmpty() )
	{
		str = Setting::toString(_key.first());
		for ( int it = 1; it < _key.size(); ++it )
			str += "." + Setting::toString(_key.at(it));
	}
	return str;
}

SettingType Setting::type() const
{
	return _type;
}

void Setting::setType( const SettingType& type )
{
	_type = type;
}

bool Setting::toBool( bool* ok ) const
{
	bool val = false;
	*ok = value(val);
	return val;
}

int Setting::toInt( bool* ok ) const
{
	int val = 0;
	*ok = value(val);
	return val;
}

QList<int> Setting::toIntList( bool* ok ) const
{
	QList<int> val;
	*ok = value(val);
	return val;
}

double Setting::toDouble( bool* ok ) const
{
	double val = 0;
	*ok = value(val);
	return val;
}

QList<double> Setting::toDoubleList( bool* ok ) const
{
	QList<double> val;
	*ok = value(val);
	return val;
}

QString Setting::toString( bool* ok ) const
{
	QString val;
	*ok = value(val);
	return val;
}

QList<QString> Setting::toStringList( bool* ok ) const
{
	QList<QString> val;
	*ok = value(val);
	return val;
}

QColor Setting::toColor( bool* ok ) const
{
	QColor val;
	*ok = value(val);
	return val;
}

QPoint Setting::toPoint( bool* ok ) const
{
	QPoint val;
	*ok = value(val);
	return val;
}

QPointF Setting::toPointF( bool* ok ) const
{
	QPointF val;
	*ok = value(val);
	return val;
}

QSize Setting::toSize( bool* ok ) const
{
	QSize val;
	*ok = value(val);
	return val;
}

QSizeF Setting::toSizeF( bool* ok ) const
{
	QSizeF val;
	*ok = value(val);
	return val;
}

QRect Setting::toRect( bool* ok ) const
{
	QRect val;
	*ok = value(val);
	return val;
}

QRectF Setting::toRectF( bool* ok ) const
{
	QRectF val;
	*ok = value(val);
	return val;
}

QPen Setting::toPen( bool* ok ) const
{
	QPen val;
	*ok = value(val);
	return val;
}

QBrush Setting::toBrush( bool* ok ) const
{
	QBrush val;
	*ok = value(val);
	return val;
}

QString Setting::valueToString() const
{
	QString str = "[novalue]";

	switch (_type)
	{
		case SettingType::Position:
		{
			str = "Position::";
			QString pos_str = Setting::toString((SettingKey)_value.toInt());
			str += pos_str.replace(0,1,pos_str.at(0).toUpper());
			break;
		}
		case SettingType::Anchor:
		{
			str = "Anchor::";
			QString anc_str = Setting::toString((SettingKey)_value.toInt());
			str += anc_str.replace(0,1,anc_str.at(0).toUpper());
			break;
		}
		case SettingType::Boolean:
			str = _value.toString();
			break;
		case SettingType::Integer:
			str = _value.toString();
			break;
		case SettingType::Double:
			str = _value.toString();
			break;
		case SettingType::String:
			str = "\"" + _value.toString() + "\"";
			break;
		case SettingType::Key:
		{
			str = "Key::";
			QString key_str = Setting::toString((SettingKey)_value.toInt());
			str += key_str.replace(0,1,key_str.at(0).toUpper());
			break;
		}
		case SettingType::KeyList:
		{
			str = "Key(";
			QList<int> int_lst = _value.value<QList<int>>();
			if ( !int_lst.isEmpty() )
			{
				QString key_str = Setting::toString((SettingKey)int_lst.first());
				str += key_str.replace(0,1,key_str.at(0).toUpper());
				for ( int it = 1; it < int_lst.size(); ++it )
				{
					QString key_str = Setting::toString((SettingKey)int_lst.at(it));
					str += "," + key_str.replace(0,1,key_str.at(0).toUpper());
				}
			}
			str += ")";
			break;
		}
		case SettingType::IntegerList:
		{
			QList<int> lst = toIntList();
			str = "{";
			if ( !lst.isEmpty() )
			{
				str += QString::number(lst.first());
				for ( int it = 1; it < lst.size(); ++it )
					str += "," + QString::number(lst.at(it));
			}
			str += "}";
			break;
		}
		case SettingType::DoubleList:
		{
			QList<double> lst = toDoubleList();
			str = "{";
			if ( !lst.isEmpty() )
			{
				str += QString::number(lst.first());
				for ( int it = 1; it < lst.size(); ++it )
					str += "," + QString::number(lst.at(it));
			}
			str += "}";
			break;
		}
		case SettingType::StringList:
		{
			QList<QString> lst = toStringList();
			str = "{";
			if ( !lst.isEmpty() )
			{
				str += "\"" + lst.first() + "\"";
				for ( int it = 1; it < lst.size(); ++it )
					str += ",\"" + lst.at(it) + "\"";
			}
			str += "}";
			break;
		}
		case SettingType::Color:
		{
			QColor col = toColor();
			str = "Color(";
			str += QString::number(col.red()) + ",";
			str += QString::number(col.green()) + ",";
			str += QString::number(col.blue());
			if ( col.alpha() != 255 )
				str += "," +QString::number(col.red());
			str += ")";
			break;
		}
		case SettingType::Point:
		{
			QPoint point = toPoint();
			str = "Point(";
			str += QString::number(point.x()) + ",";
			str += QString::number(point.y());
			str += ")";
			break;
		}
		case SettingType::PointF:
		{
			QPointF point = toPointF();
			str = "PointF(";
			str += QString::number(point.x()) + ",";
			str += QString::number(point.y());
			str += ")";
			break;
		}
		case SettingType::Size:
		{
			QSize size = toSize();
			str = "Size(";
			str += QString::number(size.width()) + ",";
			str += QString::number(size.height());
			str += ")";
			break;
		}
		case SettingType::SizeF:
		{
			QSizeF size = toSizeF();
			str = "SizeF(";
			str += QString::number(size.width()) + ",";
			str += QString::number(size.height());
			str += ")";
			break;
		}
		case SettingType::Rect:
		{
			QRect rect = toRect();
			str = "Rect(";
			str += QString::number(rect.x()) + ",";
			str += QString::number(rect.y()) + ",";
			str += QString::number(rect.width()) + ",";
			str += QString::number(rect.height());
			str += ")";
			break;
		}
		case SettingType::RectF:
		{
			QRectF rect = toRectF();
			str = "RectF(";
			str += QString::number(rect.x()) + ",";
			str += QString::number(rect.y()) + ",";
			str += QString::number(rect.width()) + ",";
			str += QString::number(rect.height());
			str += ")";
			break;
		}
		case SettingType::LineStyle:
		{
			QPen pen = toPen();
			str = "LineStyle(";
			str += "dash:" + QString::number(pen.style()) + ";";
			str += "width:" + QString::number(pen.width()) + ";";
			str += "color:";// + QString::number(rect.y()) + ",";
			QColor col = pen.color();
			str += QString::number(col.red()) + ",";
			str += QString::number(col.green()) + ",";
			str += QString::number(col.blue());
			if ( col.alpha() != 255 )
				str += "," +QString::number(col.red());
			str += ";";
			str += ")";
			break;
		}
		case SettingType::FillStyle:
		{
			QBrush brush = toBrush();
			str = "FillStyle(";
			str += "color:";// + QString::number(rect.y()) + ",";
			QColor col = brush.color();
			str += QString::number(col.red()) + ",";
			str += QString::number(col.green()) + ",";
			str += QString::number(col.blue());
			if ( col.alpha() != 255 )
				str += "," +QString::number(col.red());
			str += ";";
			str += ")";
			break;
		}
		case SettingType::Margins:
			str = "Position::";
			break;
		default:
			break;
	}
	return str;
}

void Setting::read( const QString& line )
{



}

QString Setting::show() const
{
	QString str;
	str = "Setting(";
	str += keyToString() + ",";
	str += QString("Type::");
	switch (_type)
	{
		case SettingType::Position:
		{
			str += "Position";
			break;
		}
		case SettingType::Anchor:
		{
			str += "Anchor";
			break;
		}
		case SettingType::Boolean:
			str += "Boolean";
			break;
		case SettingType::Integer:
			str += "Integer";
			break;
		case SettingType::Double:
			str += "Double";
			break;
		case SettingType::String:
			str += "String";
			break;
		case SettingType::Key:
		{
			str += "Key";
			break;
		}
		case SettingType::KeyList:
		{
			str += "KeyList";
			break;
		}
		case SettingType::IntegerList:
		{
			str += "IntegerList";
			break;
		}
		case SettingType::DoubleList:
		{
			str += "DoubleList";
			break;
		}
		case SettingType::StringList:
		{
			str += "StringList";
			break;
		}
		case SettingType::Color:
		{
			str += "Color";
			break;
		}
		case SettingType::Point:
		{
			str += "Point";
			break;
		}
		case SettingType::PointF:
		{
			str += "PointF";
			break;
		}
		case SettingType::Size:
		{
			str += "Size";
			break;
		}
		case SettingType::SizeF:
		{
			str += "SizeF";
			break;
		}
		case SettingType::Rect:
		{
			str += "Rect";
			break;
		}
		case SettingType::RectF:
		{
			str += "RectF";
			break;
		}
		case SettingType::Margins:
			str += "Position";
			break;
		default:
			str += "Unknown";
			break;
	}

	str += "," + valueToString();
	str += ")";
	return str;
}

QString Setting::save() const
{
	QString str = keyToString() + "=" + valueToString();
	return str;
}

//SettingKey Setting::position() const
//{
//	if ( type() == SettingType::Position )
//		return SettingKey(value().toInt());
//	return SettingKey::NoKey;
//}

//SettingKey Setting::anchor() const
//{
//	if ( type() == SettingType::Anchor )
//		return SettingKey(value().toInt());
//	return SettingKey::NoKey;
//}

bool Setting::operator==( const QList<SettingKey>& key )
{
	if ( this->key().size() == key.size() )
	{
		for ( int it = 0; it < key.size(); ++it )
			if ( this->key().at(it) != key.at(it) )
				return false;
		return true;
	}
	return false;
}

bool Setting::operator==( const Setting& setting)
{
	return false;
}

Setting& Setting::operator<<( const QString& std )
{
	return *this;
}

//void Setting::setValue( const QList<SettingKey>& value )
//{
//	_type = SettingType::KeyList;
//	QList<int> int_lst;
//	for ( const SettingKey& val : value )
//		int_lst += (int)val;
//	_value = QVariant::fromValue<QList<int>>(int_lst);
//}

void Setting::setKey( const QList<SettingKey>& key )
{
	_key = key;
}

QString Setting::toString( const QList<SettingKey>& key )
{
	QString str = "unknow";
	if ( !key.isEmpty() )
	{
		str = Setting::toString(key.first());
		for ( int it = 1; it < key.size(); ++it )
			str += "." + Setting::toString(key.at(it));
	}
	return str;
}
