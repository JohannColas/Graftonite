#include "Singleton.h"

std::mutex Singleton::_mutex;
Singleton* Singleton::_instance = nullptr;

Singleton* Singleton::Instance()
{
	if ( _instance == nullptr )
	{
		std::lock_guard<std::mutex> lock(_mutex);
		if ( _instance == nullptr )
			_instance = new Singleton;
	}
	return _instance;
}

void Singleton::Kill()
{
	delete _instance;
	_instance = nullptr;
}

Singleton::~Singleton()
{

}

Singleton::Singleton()
{

}

Singleton* ProjectManager::sigleton = Singleton::Instance();
