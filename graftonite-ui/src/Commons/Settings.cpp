#include "Settings.h"
/**********************************************/
#include <QPen>
#include <QBrush>
#include <QColor>
/**********************************************/
/**********************************************/
/**********************************************/
Settings::Settings( QObject* parent )
	: QObject{parent}
{
	qDebug() << "Size of Setting :" << sizeof(Setting);
//	_settings2.append( Setting(SettingKey::AddEntry, "") );
//	_settings2.append( Setting(SettingKey::Alignment, 0.55) );
//	_settings2.append( Setting(SettingKey::Area, SettingKey::Bars) );
////	_settings2.append( Setting(SettingKey::Alignment, false) );
//	addSetting2( SettingKey::Alignment, false );
//	qDebug() << _settings2.first().show();
//	qDebug() << _settings2.at(1).show();
//	qDebug() << _settings2.at(3).show();
//	SettingKey key = SettingKey::AddEntry;
//	qDebug() << (int)key;
//	_settings2.at(3).value(key);
//	qDebug() << (int)key;
//	_settings2.at(2).value(key);
//	qDebug() << (int)key;
//	bool de = true;
//	qDebug() << de;
//	_settings2.at(3).value(de);
//	qDebug() << de;

//	qDebug() << "";
}
Settings::Settings( const Settings& settings )
{
	_settings = settings.settings();
}
/**********************************************/
/**********************************************/
void Settings::addSetting( const Key& key, const QVariant& value )
{
	if ( !hasKey(key) )
		_settings.append( {key, value} );
	else if ( key != Key::ID )
		for ( QPair<Key,QVariant>& pair : _settings )
			if ( pair.first == key )
				pair.second = value;
}
void Settings::set( const QVariant& value, const Key& key )
{
	if ( !hasKey(key) )
		_settings.append( {key, value} );
	else if ( key != Key::ID )
		for ( QPair<Key,QVariant>& pair : _settings )
			if ( pair.first == key )
				pair.second = value;
}
/**********************************************/
/**********************************************/
QList<Key> Settings::keys() const
{
	QList<Key> keys;
	for ( const QPair<Key,QVariant>& pair : _settings )
		keys.append( pair.first );
	return keys;
}
/**********************************************/
bool Settings::hasKey( const Key& key ) const
{
	for ( const QPair<Key,QVariant>& pair : _settings )
		if ( pair.first == key )
			return true;
	return false;
}
/**********************************************/
/**********************************************/
QList<QPair<Key,QVariant>> Settings::settings() const
{
	return _settings;
}
/**********************************************/
QVariant Settings::value( const Key& key ) const
{
	for ( const QPair<Key,QVariant>& pair : _settings )
		if ( pair.first == key )
			return pair.second;
	return QVariant();
}
bool Settings::value( QVariant& value, const Key& key ) const
{
	for ( const QPair<Key,QVariant>& pair : _settings )
		if ( pair.first == key )
		{
			value = pair.second;
			return true;
		}
	return false;
}
QString Settings::getString( const Key& key ) const
{
	return value( key ).toString();
}
bool Settings::getKey( Key& value, const Key& key ) const
{
	QVariant var;
	if ( !this->value(var, key) )
		return false;
	value = Key::Keys(var.toInt());
	return true;
}
Key Settings::getKey( const Key& key ) const
{
	Key value = Key::Unknown;
	getKey( value, key );
	return value;
}
bool Settings::getString( QString& value, const Key& key ) const
{
	QVariant var;
	if ( !this->value(var, key) )
		return false;
	value = var.toString();
	return true;
}
bool Settings::getBool( const Key& key ) const
{
	bool value = false;
	getBool( value, key );
	return value;
}
bool Settings::getBool( bool& value, const Key& key ) const
{
	QVariant var;
	if ( !this->value(var, key) )
		return false;
	value = var.toBool();
	return true;
}
int Settings::getInt( const Key& key ) const
{
	return value( key ).toInt();
}
bool Settings::getInt( int& value, const Key& key ) const
{
	QVariant var;
	if ( !this->value(var, key) )
		return false;
	bool ok;
	int val = var.toInt(&ok);
	if ( !ok )
		return false;
	value = val;
	return true;
}
double Settings::getDouble( const Key& key ) const
{
	return value( key ).toDouble();
}
bool Settings::getDouble( double& value, const Key& key ) const
{
	QVariant var;
	if ( !this->value(var, key) )
		return false;
	bool ok = true;
	double val = var.toDouble(&ok);
	if ( !ok )
		return false;
	value = val;
	return true;
}
QColor Settings::getColor( const Key& key ) const
{
	QColor color;
	if ( hasKey(key) )
	{
		color = this->value(key).value<QColor>();
		if ( !color.isValid() )
			color = QColor();
	}
	return color;
}
bool Settings::getColor( QColor& value, const Key& key ) const
{
	QVariant var;
	if ( !this->value(var, key) )
		return false;
	value = var.value<QColor>();
	return true;
}

QList<qreal> Settings::getDash( const Key& key ) const
{
	QList<qreal> dash;
	return dash;
}
bool Settings::getDash( QList<qreal>& value, const Key& key ) const
{
	QVariant var;
	if ( !this->value(var, key) )
		return false;
//	value = var.value<QColor>();
	return true;
}
QPen Settings::getPen( const Key& key ) const
{
	QPen pen = QPen(Qt::NoPen);
	getPen( pen, key );
//	QVariant var = value( key );
//	if ( var.typeId() != QMetaType::QPen )
//		pen = var.value<QPen>();
//	QList<qreal> dash;
//	getDash( dash, Key(key, Key::Dash) );
//	pen.setDashPattern( dash );
//	QColor color;
//	getColor( color, Key(key, Key::Color) );
//	pen.setColor( color );
//	double width = 5;
//	getDouble( width, Key(key, Key::Width) );
//	pen.setWidth( width );

//	int join = Qt::BevelJoin;
//	getInt( join, Key(key, Key::Join) );
//	pen.setJoinStyle(Qt::PenJoinStyle(join));

//	double miterlimit = Qt::BevelJoin;
//	getDouble( miterlimit, Key(key, Key::MiterLimit) );
//	pen.setMiterLimit(miterlimit);

//	int cap = Qt::FlatCap;
//	getInt( cap, Key(key, Key::Cap) );
//	pen.setCapStyle( Qt::PenCapStyle(cap) );

	return pen;
}

bool Settings::getPen( QPen& pen, const Key& key ) const
{
	QVariant var = value( key );
	if ( var.typeId() != QMetaType::QPen )
		return false;
	pen = var.value<QPen>();
	return true;
}
QBrush Settings::getBrush( const Key& key ) const
{
	QBrush brush;
	// BrushStyle
	Qt::BrushStyle style = Qt::SolidPattern;
	// get setting
	brush.setStyle( style );
	QColor color;
	getColor( color, Key(key, Key::Color) );
	brush.setColor( color );
	return brush;
}
/**********************************************/
/**********************************************/
int Settings::DecomposeLine( const QString& line, Settings& settings )
{
	if ( line.startsWith("#") )
	{
		int indent = 0;
		int it;
		int beg_ind = 0;
		for ( it = 0; it < line.length(); ++it )
		{
			if ( line.at(it) == '#' )
				++indent;
			else if ( line.at(it) == ' ' )
				continue;
			else
			{
				beg_ind = it;
				break;
			}
		}
		int end_ind = line.indexOf( ' ', it );
		QString type = line.mid( beg_ind, end_ind-beg_ind ).toLower();
		settings.addSetting( Key::Type, Key::toKeys(type) );
		Settings::DecomposeLine( line.mid(end_ind), settings );
		return indent;
	}
	else
	{
		if ( line.isEmpty() || line.startsWith("\\") )
			return -1;
		QString tmp = line;
		QString key;
		QString value;
		bool search_first_space = false;
		bool search_first_quote_space = false;
		bool search_equal = true;
		for ( int it = 0; it < tmp.size(); ++it )
		{
			QChar c = tmp.at(it);
			if ( search_equal && c == '=' )
			{
				search_equal = false;
				if ( it+1 < tmp.size() && tmp.at(it+1) == '\"' )
				{
					search_first_quote_space = true;
					++it;
				}
				else
					search_first_space = true;
				continue;
			}
			else if ( (search_first_quote_space && c == '\"' && ((it+1 < tmp.size() && tmp.at(it+1) == ' ') || it+1 == tmp.size())) || ( search_first_space && c == ' ' ) )
			{
				search_first_space = false;
				search_first_quote_space = false;
				search_equal = true;
				Key key2 = Key::toKeys(key);
				if ( key2 != Key::Unknown )
					settings.addSetting( key2, value );
				key.clear();
				value.clear();
				continue;
			}

			if ( search_equal && c != ' ' )
				key += c;
			else if ( search_first_quote_space || search_first_space )
				value += c;
		}
		return 1;
	}
	return -1;
}

void Settings::read( const QString& line )
{

}

QString Settings::save( const Key& key, int level )
{

	QString str;
	QTextStream out( &str );
	out << key.toString() << "=";
	QVariant var = value(key);
	if ( var.metaType() == QMetaType(QMetaType::QString) )
	{
		out << "\"" << var.toString() << "\"";
	}
	else if ( var.metaType() == QMetaType(QMetaType::Int) )
		out << var.toInt();
	else if ( var.metaType() == QMetaType(QMetaType::Double) )
		out << var.toDouble();
	else if ( var.metaType() == QMetaType(QMetaType::Bool) )
		out << var.toString();
	else if ( var.metaType() == QMetaType(QMetaType::User) )
		out << var.toInt();
//	else if ( var.metaType() == QMetaType(QMetaType::Int) )
//		out << var.toInt();
	out << "\n";
	return str;
}

void Settings::changeSetting( const Key& key, const QVariant& value )
{
	addSetting( key, value );
	emit changed();
}
/**********************************************/
/**********************************************/
