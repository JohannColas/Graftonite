#ifndef SINGLETON_H
#define SINGLETON_H

#include <mutex>



class Singleton
{
public:
	static Singleton* Instance();
	static void Kill();
protected:
	static std::mutex _mutex;
	static Singleton* _instance;
private:
	~Singleton();
	Singleton();
	Singleton( const Singleton& ) = delete;
	Singleton& operator= ( const Singleton& ) = delete;

};
namespace ProjectManager {

	extern Singleton* sigleton;
}

#endif // SINGLETON_H
