#include "Files.h"
/**********************************************/
/**********************************************/
#include <QDir>
#include <QDomDocument>
#include <QFile>
#include <QTextStream>
/**********************************************/
/**********************************************/
void Files::readFile( const QString& path, QString& content )
{
	// Creating file
	QFile file( path );
	// Opening file
	if ( file.open( QIODevice::ReadOnly | QIODevice::Text ) )
	{
		QTextStream in(&file);
		// Reading file
		content = in.readAll();
		// Closing file du fichier
		file.close();
	}
}
/**********************************************/
/**********************************************/
QDomDocument Files::readXML( const QString& path )
{
	QDomDocument document;
	// Creating file
	QFile file( path );
	// Opening file
	if ( file.open( QIODevice::ReadOnly | QIODevice::Text ) )
	{
		// Reading file
		document.setContent( &file );
		// Closing file du fichier
		file.close();
	}
	return document;
}
/**********************************************/
/**********************************************/
void Files::saveXML( const QString& path, const QDomDocument& document )
{
	// Creating file
	QFile file( path );
	// Opening file
	if( !file.open( QFile::WriteOnly ) ) return ;
	// Creating QTextStream
	QTextStream stream( &file );
	// Writing document in the stream
	document.save( stream, 4, QDomDocument::EncodingFromTextStream );
	// Closing file du fichier
	file.close();
}
/**********************************************/
/**********************************************/
/**********************************************/
QString Files::localPath()
{
	// Create user dir if not exists
	QString configPath = "/.local/share";
#ifdef Q_OS_WIN
	configPath = "/AppData/Roaming";
#endif
	QDir dir( QDir::homePath() + configPath );
	if ( !dir.cd( "Symphony" ) )
		dir.mkdir( "Symphony" );
	dir.cd( "Symphony" );
//	if ( dir.cd( "Symphony" ) )
//		return makePath( dir.path(), file );
	return dir.absolutePath();
}
/**********************************************/
QString Files::tmpPath()
{
	QDir dir( Files::localPath() );
	if ( !dir.cd( "tmp" ) )
		dir.mkdir( "tmp" );
	dir.cd( "tmp" );
	return dir.absolutePath();
}
/**********************************************/
/**********************************************/
#include <random>
/**********************************************/
QString Files::generateUniqueID( int length, const QString& allowchars )
{
	QString id( length ? length : 16, '\0' );
	static thread_local std::default_random_engine random_engine(std::random_device{}());
	static thread_local std::uniform_int_distribution<int> random_dist( 0, allowchars.size()-1 );
	for ( QChar& c : id )
		c = allowchars[random_dist(random_engine)];
	return id;
}
/**********************************************/
/**********************************************/
/**********************************************/
