#ifndef SETTINGS_H
#define SETTINGS_H

// Qt dependencies
#include <QObject>
#include <QPair>
#include <QVariant>

//
#include "Key.h"
#include "Setting.h"



/**
 * @class Settings
 * @brief Store data
 *
 * Collection of @ref Setting.
 * When a setting changes, Settings object sends a changeSetting(const Setting&) and
 * change() signals.
 *
 */
class Settings
		: public QObject
{
	Q_OBJECT
private:
	QList<QPair<Key,QVariant>> _settings; ///< List of all settings
	QList<Setting> _settings2;
	QList<Setting> _default_settings; ///< List of default settings

public:
	/**
	 * Settings object constructor
	 *
	 * @param parent
	 *
	 */
	Settings( QObject* parent = nullptr );
	/**
	 * Settings object copy constructor
	 *
	 */
	Settings( const Settings& settings );

	/**
	 * Add setting to the list
	 *
	 * @param key the key of the Setting
	 * @param value the value of the Setting
	 *
	 */
	void addSetting( const Key& key, const QVariant& value );

	/**
	 * Add setting to the list
	 * If
	 *
	 * @param key the key of the Setting
	 * @param value the value of the Setting
	 *
	 */
	template<typename K, typename V>
	void addSetting2( const K& key, const V& value )
	{
//		if ( !setValueAt(key, value) && key != Key::ID  )
		    _settings2.append( Setting(key, value) );
	}
	/**
	 * @fn
	 * Set the value of the setting at key
	 *
	 * @param key the key of the Setting
	 * @param value the value of the Setting
	 *
	 */
	template<typename K, typename V>
	bool setValueAt( const K& key, const V& value )
	{
		for ( const Setting& set : _settings2 )
		{
//			if ( set.key() == key )
			{
				set.setValue(value);
				return true;
			}
		}
		return false;
	}
	/**
	 * @fn
	 * Get the value of the setting at key
	 *
	 * @param key the key of the Setting
	 * @param value the value of the Setting
	 *
	 */
	template<typename K>
	bool getSetting( const K& key, Setting& setting ) const
	{
		for ( const Setting& set : _settings2 )
		{
			if ( set.key() == key )
			{
				setting = set;
				return true;
			}
		}
		return false;
	}
	/**
	 * @fn
	 * Get the value of the setting at key
	 *
	 * @param key the key of the Setting
	 * @param value the value of the Setting
	 *
	 */
	template<typename K, typename V>
	bool getValue( const K& key, V& value ) const
	{
		Setting setting;
		if ( getSetting(key) )
			return setting.value(value);
		return false;
	}
	void set( const QVariant& value, const Key& key );
	/******************************************/
	QList<Key> keys() const;
	bool hasKey( const Key& key ) const;
	/******************************************/
	QList<QPair<Key,QVariant>> settings() const;
	QVariant value( const Key& key ) const;
	bool value( QVariant& value, const Key& key ) const;
	QString getString( const Key& key ) const;
	bool getKey( Key& value, const Key& key ) const;
	Key getKey( const Key& key ) const;
	bool getString( QString& value, const Key& key ) const;
	bool getBool( const Key& key ) const;
	bool getBool( bool& value, const Key& key ) const;
	int getInt( const Key& key ) const;
	bool getInt( int& value, const Key& key ) const;
	double getDouble( const Key& key ) const;
	bool getDouble( double& value, const Key& key ) const;
	QColor getColor( const Key& key ) const;
	bool getColor( QColor& value, const Key& key ) const;
	QList<qreal> getDash( const Key& key ) const;
	bool getDash( QList<qreal>& value, const Key& key ) const;
	QPen getPen( const Key& key ) const;
	bool getPen( QPen& pen, const Key& key ) const;
	QBrush getBrush( const Key& key ) const;
	/******************************************/
	static int DecomposeLine( const QString& line, Settings& settings );
	/******************************************/
	void read( const QString& line );
	void readUserSettings( const QString& line );
	void readDefaultSettings( const QString& path );
	QString save( const Key& key, int level = 0 );
	/******************************************/
public slots:
	void changeSetting( const Key& key, const QVariant& value );
	/******************************************/
signals:
	void changed();
	/******************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SETTINGS_H
