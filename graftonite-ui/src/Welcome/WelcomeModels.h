#ifndef WELCOMEMODELS_H
#define WELCOMEMODELS_H

#include <QStandardItem>
// #include "../Commons/theme.h"

#include <QDebug>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class WelcomeItem
		: public QStandardItem
{
private:
	QString _path;

public:
	// Destructors
	~WelcomeItem() {
	}
	// -------------------
	// -------------------
	// Constructor
	WelcomeItem( QString text, QString path )
		: QStandardItem( text )
	{
//		setIcon( QIcon(":/GraftoniteIcon.svg") );
		setPath( path );
		setEditable( false );
	}
	// -------------------
	// -------------------
	//
	QString path() const {
		return _path;
	}
	void setPath( QString path ) {
		_path = path;
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class WelcomeModel
		: public QStandardItemModel
{
public:
	// Destructors
	~WelcomeModel () {

	}
	// -------------------
	// -------------------
	// Constructor
	WelcomeModel ()
		: QStandardItemModel()
	{
		WelcomeItem *item = new WelcomeItem( "Project1", "");
		this->appendRow(item);
		//treemodel->appendRow( {new QStandardItem( QIcon(":/GraftoniteIcon.svg"), "Project1" ), new QStandardItem( "Project" )} );
		WelcomeItem *item2 = new WelcomeItem("Project2", "" );
		this->appendRow(item2);
		WelcomeItem *item2b = new WelcomeItem("Project3", "" );
		this->appendRow(item2b);
		WelcomeItem *item3 = new WelcomeItem("Project4", "" );
		this->appendRow(item3);
		WelcomeItem *item4 = new WelcomeItem( "Project5", "" );
		this->appendRow(item4);
		WelcomeItem *item5 = new WelcomeItem( "Project6", "" );
		this->appendRow(item5);
		WelcomeItem *item6 = new WelcomeItem( "Project7", "" );
		this->appendRow(item6);
	}
	// -------------------
	// -------------------
	// Add file
	void add( QString name, int type, QString path )
	{
		Q_UNUSED(name)
		Q_UNUSED(type)
		Q_UNUSED(path)
	}
	// -------------------
	// -------------------
//	void updateIcons( Icons* icons ) {
////		this->rowCount()
//		for ( int it = 0; it < rowCount(); ++it ) {
//			item( it )->setIcon( icons->graftonite() );
//		}
//	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // WELCOMEMODELS_H
