#ifndef GRAPHSETTINGSWIDGET_H
#define GRAPHSETTINGSWIDGET_H
/**********************************************/
#include "../Settings/SettingsWidget.h"
/**********************************************/
/**********************************************/
/**********************************************/
class GraphSettingsWidget
        : public SettingsWidget
{
	Q_OBJECT
public:
	GraphSettingsWidget( QWidget* parent = nullptr );
	/******************************************/
	virtual void connectSettings( Settings* settings );
	/******************************************/
signals:
	/******************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // GRAPHSETTINGSWIDGET_H
