#include "GraphicsItem.h"
/**********************************************/
#include "GraphView.h"
#include <QGraphicsScene>
/**********************************************/
/**********************************************/
/**********************************************/
GraphicsItem::~GraphicsItem()
{
//	QGraphicsItemGroup::removeFromGroup(item_bounding_rect);
//	delete item_bounding_rect;
}
GraphicsItem::GraphicsItem( GraphItem* parent )
    : QObject(), QGraphicsItemGroup()
{
	init();
}
/**********************************************/
GraphicsItem::GraphicsItem( QGraphicsItem* parent )
    : QObject(), QGraphicsItemGroup(parent)
{
	init();
}
/**********************************************/
/**********************************************/
GraphicsItem::GraphicsItem( const QString& name, QGraphicsItem* parent )
    : QObject(), QGraphicsItemGroup(parent)
{
	_settings.addSetting(Key::Name,name);
	init();
}

GraphicsItem::GraphicsItem(const QString& name, const Key& type, GraphicsItem* parent)
{

	_settings.addSetting(Key::Name,name);
	_item_type = type;
	_parent = parent;
	init();
}

void GraphicsItem::init()
{
	connect( &_settings, &Settings::changed,
	         this, &GraphicsItem::onSettingsChanged );
//	connect( &_settings, &Settings::changed,
//	         this->scene(), &QGraphicsScene::update );
}
Key GraphicsItem::itemType() const
{
	return _item_type;
}
QString GraphicsItem::name() const
{
	QString name = "[No name]";
	_settings.getString(name,Key::Name);
	return name;
}
void GraphicsItem::setItemType( const Key& type )
{
	_item_type = type;
}
/**********************************************/
/**********************************************/
/* Settings Management */
/**********************************************/
Settings& GraphicsItem::settings()
{
	return _settings;
}
/**********************************************/
Settings GraphicsItem::settings() const
{
	return _settings;
}
/**********************************************/
void GraphicsItem::addSetting( const Key& key, const QVariant& value )
{
	_settings.addSetting( key, value );
}
/**********************************************/
/**********************************************/
void GraphicsItem::onSettingsChanged()
{
//	bool hide = false; _settings.getBool( hide, Key::Hide );
//	// Draw Rect
//	if ( !hide )
//	{
//		QPen pen = _settings.getPen(Key::Borders);
//		QBrush brush = _settings.getBrush(Key::Background);
//		if ( item_bounding_rect == nullptr )
//			item_bounding_rect = new QGraphicsRectItem;
//		item_bounding_rect->setBrush( brush );
//		item_bounding_rect->setPen( pen );
//		item_bounding_rect->setRect( this->boundingRect() );
//	}

//	qDebug() << this->scene();
	if ( this->scene() != nullptr )
		this->scene()->update();
}
/**********************************************/
/**********************************************/
void GraphicsItem::updateGraphItem()
{

	emit changed();
}
/**********************************************/
/**********************************************/
void GraphicsItem::mouseReleaseEvent( QGraphicsSceneMouseEvent* event )
{
//	QPen pen = QPen(QColor(244,10,10), 3);
//	if ( item_bounding_rect == nullptr )
//		item_bounding_rect = new QGraphicsRectItem;
//	item_bounding_rect->setPen( pen );
//	item_bounding_rect->setRect( this->boundingRect() );
	QGraphicsItem::mouseReleaseEvent( event );
}

GraphicsItem* GraphicsItem::parent()
{
	return _parent;
}

void GraphicsItem::setParent( GraphicsItem* parent )
{
	_parent = parent;
}

void GraphicsItem::appendChild( GraphicsItem* child )
{
	child->setParent(this);
	_children.append( child );
}

int GraphicsItem::indexOf( GraphicsItem* child ) const
{
	return _children.indexOf( child );
}

int GraphicsItem::row() const
{
	if ( _parent == nullptr )
		return 0;
	return _parent->indexOf(const_cast<GraphicsItem*>(this));
}

int GraphicsItem::childCount() const
{
	return _children.count();
}

GraphicsItem* GraphicsItem::childAt( int index ) const
{
	if ( index < 0 || index+1 > childCount() )
		return nullptr;
	return _children.at(index);
}

void GraphicsItem::deleteChild( int index )
{
	if ( index < 0 || index+1 > _children.count() )
		return;
	delete _children.takeAt(index);
}
/**********************************************/
/**********************************************/
