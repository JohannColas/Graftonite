#ifndef GRAPHTREE_H
#define GRAPHTREE_H
/**********************************************/
#include <QTreeWidget>
/**********************************************/
/**********************************************/
/**********************************************/
class GraphTree
		: public QTreeWidget
{
	Q_OBJECT
	// Menu
	QMenu* contextMenu = nullptr;
	QAction* ac_newframe = nullptr;
	QAction* ac_newaxis = nullptr;
	QAction* ac_newplot = nullptr;
	QAction* ac_newlegend = nullptr;
	QAction* ac_newtitle = nullptr;
	QAction* ac_newshape = nullptr;
	QAction* ac_resetgraph = nullptr;
	QAction* ac_delete = nullptr;
	/******************************************/
public:
	GraphTree( QWidget* parent = nullptr );
private slots:
	void leaveEvent( QEvent* event ) override;
	void contextMenuEvent( QContextMenuEvent* event ) override;
	/******************************************/
signals:
	void leaved();
	void newFrame();
	void newAxis();
	void newPlot();
	void newLegend();
	void newTitle();
	void newShape();
	void resetGraph();
	void deleteItem();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // GRAPHTREE_H
