#ifndef GRAPHITEM_H
#define GRAPHITEM_H
/**********************************************/
#include "GraphicsItem.h"
/**********************************************/
class FrameItem;
class AxisItem;
class PlotItem;
class LegendItem;
class TitleItem;
class ShapeItem;
/**********************************************/
/**********************************************/
/**********************************************/
class GraphItem
		: public GraphicsItem
{
	Q_OBJECT
protected:
	QGraphicsRectItem* graph_rect = nullptr;
	Rect _rect = {0,0,1600,1200};
	/******************************************/
public:
	~GraphItem();
	GraphItem( QGraphicsItem* parent = nullptr );
	GraphItem( const QString& name, QGraphicsItem* parent = nullptr );
	/******************************************/
	void init();
	/******************************************/
	//
	void addSetting( const Key& key, const QVariant& value ) override;
	/******************************************/
public slots:
	void updateGraphItem() override;
	/******************************************/
signals:
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // GRAPHITEM_H
