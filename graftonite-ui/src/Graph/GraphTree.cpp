#include "GraphTree.h"
/**********************************************/
#include <QHeaderView>
/**********************************************/
#include <QMenu>
#include <QContextMenuEvent>
/**********************************************/
/**********************************************/
/**********************************************/
GraphTree::GraphTree( QWidget* parent )
	: QTreeWidget(parent)
{

	hideColumn( 1 );
	header()->hide();

	// Menu
	contextMenu = new QMenu;
	QMenu* newMenu = new QMenu("New", this);
	newMenu->setIcon( QIcon("icons/Dark Theme/new.svg") );
	contextMenu->addMenu(newMenu);

	ac_newframe = new QAction("Frame", this);
	ac_newframe->setIcon( QIcon("icons/Dark Theme/frame.svg") );
	newMenu->addAction( ac_newframe );
	connect( ac_newframe, &QAction::triggered,
			 this, &GraphTree::newFrame );
	ac_newaxis = new QAction("Axis", this);
	ac_newaxis->setIcon( QIcon("icons/Dark Theme/axis.svg") );
	newMenu->addAction( ac_newaxis );
	connect( ac_newaxis, &QAction::triggered,
			 this, &GraphTree::newAxis );
	ac_newplot = new QAction("Plot", this);
	ac_newplot->setIcon( QIcon("icons/Dark Theme/plot.svg") );
	newMenu->addAction( ac_newplot );
	connect( ac_newplot, &QAction::triggered,
			 this, &GraphTree::newPlot );
	ac_newlegend = new QAction("Legend", this);
	ac_newlegend->setIcon( QIcon("icons/Dark Theme/legend.svg") );
	newMenu->addAction( ac_newlegend );
	connect( ac_newlegend, &QAction::triggered,
			 this, &GraphTree::newLegend );
	ac_newtitle = new QAction("Title", this);
	ac_newtitle->setIcon( QIcon("icons/Dark Theme/title.svg") );
	newMenu->addAction( ac_newtitle );
	connect( ac_newtitle, &QAction::triggered,
			 this, &GraphTree::newTitle );
	ac_newshape = new QAction("Shape", this);
	ac_newshape->setIcon( QIcon("icons/Dark Theme/shape.svg") );
	newMenu->addAction( ac_newshape );
	connect( ac_newshape, &QAction::triggered,
			 this, &GraphTree::newShape );

	contextMenu->addSeparator();

	ac_resetgraph = new QAction("Reset", this);
	ac_resetgraph->setIcon( QIcon("icons/Dark Theme/reset.svg") );
	contextMenu->addAction( ac_resetgraph );
	connect( ac_resetgraph, &QAction::triggered,
			 this, &GraphTree::resetGraph );

	contextMenu->addSeparator();

	ac_delete = new QAction("Delete", this);
	ac_delete->setIcon( QIcon("icons/Dark Theme/delete.svg") );
	contextMenu->addAction( ac_delete );
	connect( ac_delete, &QAction::triggered,
			 this, &GraphTree::deleteItem );

}
/**********************************************/
/**********************************************/
void GraphTree::leaveEvent( QEvent* event )
{

}
/**********************************************/
/**********************************************/
void GraphTree::contextMenuEvent( QContextMenuEvent* event )
{
	contextMenu->exec( event->globalPos() );
}
/**********************************************/
/**********************************************/
