#include "FrameSettingsWidget.h"
/**********************************************/
#include "../BasicWidgets/PenWidgets.h"
/**********************************************/
/**********************************************/
/**********************************************/
FrameSettingsWidget::FrameSettingsWidget( QWidget* parent )
    : SettingsWidget(parent)
{
	addLineEdit( Key::Name, "Frame :" );
	addCheckBox( Key::Hide, "Hide :" );

	addSeparator();

	addSection( "Geometry" );
	addIntegerEdit( Key::Position, "Position :" );
	addIntegerEdit( Key::Size, "Size :" );

//	addSeparator();

	addSection( "Background" );
	addColorSelector( Key(Key::Background,Key::Color), "Color :" );

//	addSeparator();

	addSection( "Borders" );
	PenModifier* mod_bd = addPenEditor( Key::Borders, "Borders Style" );
	mod_bd->desactivateCap();
}
/**********************************************/
/**********************************************/
void FrameSettingsWidget::connectSettings( Settings* settings )
{
	SettingsWidget::connectSettings(settings);
}
/**********************************************/
/**********************************************/
