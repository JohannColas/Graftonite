#ifndef GRAPHICSITEM_H
#define GRAPHICSITEM_H
/**********************************************/
#include <QObject>
#include <QTreeWidgetItem>
#include <QGraphicsItemGroup>
/**********************************************/
#include "../Commons/Settings.h"
/**********************************************/
#include "../Commons/GeometricObjects.h"
/**********************************************/
class GraphItem;
/**********************************************/
/**********************************************/
/**********************************************/
class GraphicsItem
		: public QObject,
          public QGraphicsItemGroup
{
	Q_OBJECT
protected:
	Settings _settings;
	Key _item_type = Key::Unknown;
//	QGraphicsRectItem* item_bounding_rect = nullptr;
	/******************************************/
public:
	~GraphicsItem();
	GraphicsItem( GraphItem* parent );
	GraphicsItem( QGraphicsItem* parent = nullptr );
	GraphicsItem( const QString& name, QGraphicsItem* parent = nullptr );
	GraphicsItem( const QString& name, const Key& type, GraphicsItem* parent = nullptr );
	void init();
	Key itemType() const;
	QString name() const;
	void setItemType( const Key& type );
	/******************************************/
	GraphicsItem* parent();
	void setParent( GraphicsItem* parent );
	void appendChild( GraphicsItem* child );
	int indexOf( GraphicsItem* child ) const;
	int row() const;
	int childCount() const;
	GraphicsItem* childAt( int index ) const;
	void deleteChild( int index );
	/******************************************/
	// Settings Management
	Settings& settings();
	Settings settings() const;
	virtual void addSetting( const Key& key, const QVariant& value );
	/******************************************/
public slots:
	void onSettingsChanged();
	/******************************************/
	virtual void updateGraphItem();
	/******************************************/
	void mouseReleaseEvent( QGraphicsSceneMouseEvent* event ) override;
signals:
	void changed();
	/******************************************/
private:
	GraphicsItem* _parent = nullptr;
	QList<GraphicsItem*> _children;
	/******************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // GRAPHICSITEM_H
