#ifndef PLOTSETTINGSWIDGET_H
#define PLOTSETTINGSWIDGET_H
/**********************************************/
#include "../Settings/SettingsWidget.h"
/**********************************************/
class QTabWidget;
/**********************************************/
class GraphItem;
/**********************************************/
/**********************************************/
/**********************************************/
class PlotSettingsWidget
        : public SettingsWidget
{
	Q_OBJECT
private:
	QTabWidget* tab_elements = nullptr;
	//
	SettingsWidget* wid_general = nullptr;
	ComboBox2* cb_type = nullptr;
	QComboBox* cb_axis1 = nullptr;
	QComboBox* cb_axis2 = nullptr;
	QComboBox* cb_axis3 = nullptr;
	QComboBox* cb_data1 = nullptr;
	QComboBox* cb_data2 = nullptr;
	QComboBox* cb_data3 = nullptr;
	ComboBox2* cb_pos = nullptr;
	ComboBox2* cb_scale = nullptr;
	SettingsWidget* wid_line = nullptr;
	ComboBox2* cb_ticks_pos = nullptr;
	SettingsWidget* wid_symbols = nullptr;
	ComboBox2* cb_labels_pos = nullptr;
	SettingsWidget* wid_area = nullptr;
	ComboBox2* cb_title_pos = nullptr;
	SettingsWidget* wid_bars = nullptr;
	ComboBox2* cb_grids_pos = nullptr;
	SettingsWidget* wid_errorBars = nullptr;
	SettingsWidget* wid_labels = nullptr;
//	QList<QPair<QString,QVariant>> x_data = {{"Bottom", Key::Bottom},{"Top", Key::Top}};
//	QList<QPair<QString,QVariant>> y_data = {{"Left", Key::Left},{"Right", Key::Right}};
//	QList<QPair<QString,QVariant>> o_data = {{"Outside", Key::Outside},{"Inside", Key::Inside}};
//	QList<QPair<QString,QVariant>> polar_data = {{"Center", Key::Center},{"TopLeft", Key::TopLeft},{"Top", Key::Top}};
	//
	GraphItem* _graph = nullptr;
	/******************************************/
public:
	PlotSettingsWidget( QWidget* parent = nullptr );
	/******************************************/
	void connectSettings( Settings* settings ) override;
	/******************************************/
	void updateSettings( Settings* settings ) override;
	/******************************************/
private slots:
	void onTypeChanged( const QVariant& value );
	void onFramesChanged();
	/******************************************/
signals:
	/******************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PLOTSETTINGSWIDGET_H
