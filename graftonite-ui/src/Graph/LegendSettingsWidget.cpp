#include "LegendSettingsWidget.h"
/**********************************************/
/**********************************************/
/**********************************************/
/**********************************************/
LegendSettingsWidget::LegendSettingsWidget( QWidget* parent )
    : SettingsWidget(parent)
{
	addLineEdit( Key::Name, "Legend :" );
}
/**********************************************/
/**********************************************/
void LegendSettingsWidget::connectSettings( Settings* settings )
{
	SettingsWidget::connectSettings(settings);
}
/**********************************************/
/**********************************************/
