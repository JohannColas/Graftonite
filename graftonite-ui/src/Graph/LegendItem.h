#ifndef LEGENDITEM_H
#define LEGENDITEM_H
/**********************************************/
#include "GraphicsItem.h"
/**********************************************/
/**********************************************/
/**********************************************/
class LegendItem
		: public GraphicsItem
{
public:
	LegendItem( QGraphicsItem* parent = nullptr );
	LegendItem( const QString& name, QGraphicsItem* parent = nullptr );
	void init();
	/**********************************************/
	void addSetting( const Key& key, const QVariant& value ) override;
	/******************************************/
public slots:
	void updateGraphItem() override;
	/******************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LEGENDITEM_H
