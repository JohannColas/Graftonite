#ifndef SHAPESETTINGSWIDGET_H
#define SHAPESETTINGSWIDGET_H
/**********************************************/
#include "../Settings/SettingsWidget.h"
/**********************************************/
/**********************************************/
/**********************************************/
class ShapeSettingsWidget
        : public SettingsWidget
{
	Q_OBJECT
public:
	ShapeSettingsWidget( QWidget* parent = nullptr );
	/******************************************/
	void connectSettings( Settings* settings ) override;
	/******************************************/
signals:
	/******************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SHAPESETTINGSWIDGET_H
