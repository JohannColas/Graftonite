#include "GraphEditor.h"
/**********************************************/
//#include <QTreeWidget>
#include <QStackedWidget>
#include <QSplitter>
#include <QVBoxLayout>
/**********************************************/
#include "GraphSettingsWidget.h"
#include "FrameSettingsWidget.h"
#include "AxisSettingsWidget.h"
#include "PlotSettingsWidget.h"
#include "LegendSettingsWidget.h"
#include "TitleSettingsWidget.h"
#include "ShapeSettingsWidget.h"
/**********************************************/
#include "GraphTree.h"
#include "GraphView.h"
#include "GraphItem.h"
/**********************************************/
#include <QDir>
#include <QElapsedTimer>
#include <QFile>
#include <QTextStream>

#include "../GraphManager/GraphModel.h"
#include "../GraphManager/GraphTreeView.h"

#include "../ProjectManager/ProjectItem.h"
#include "../Commons/Files.h"
/**********************************************/
/**********************************************/
/**********************************************/
GraphEditor::~GraphEditor()
{
	delete _graph_model;
	delete _graph_tree_view;
	delete view_graph;
//	delete tree_graph;
//	qDeleteAll( _items );

	delete wid_graph;
	delete wid_frame;
	delete wid_axis;
	delete wid_plot;
	delete wid_legend;
	delete wid_title;
	delete wid_shape;

	delete cont_settings;

	delete sp_sidebar;

//	delete wid_sidebar;

	delete splitter;

	delete lay_main;
}
/**********************************************/
/**********************************************/
#include <QApplication>
GraphEditor::GraphEditor( QWidget *parent )
    : QWidget(parent)
{
	view_graph = new GraphView(this);

//	tree_graph = new GraphTree(this);
////	tree_graph->hideColumn( 1 );
////	tree_graph->header()->hide();
//	connect( tree_graph, &GraphTree::itemPressed,
//			 this, &GraphEditor::onItemPressed );
//	connect( tree_graph, &GraphTree::newFrame,
//			 this, &GraphEditor::newFrame );
//	connect( tree_graph, &GraphTree::newAxis,
//			 this, &GraphEditor::newAxis );
//	connect( tree_graph, &GraphTree::newPlot,
//			 this, &GraphEditor::newPlot );
//	connect( tree_graph, &GraphTree::newLegend,
//			 this, &GraphEditor::newLegend );
//	connect( tree_graph, &GraphTree::newTitle,
//			 this, &GraphEditor::newTitle );
//	connect( tree_graph, &GraphTree::newShape,
//			 this, &GraphEditor::newShape );
//	connect( tree_graph, &GraphTree::resetGraph,
//			 this, &GraphEditor::resetGraph );
//	connect( tree_graph, &GraphTree::deleteItem,
//			 this, &GraphEditor::deleteItem );

//	_graph_item = new GraphItem("Graph");
//	_graph_item->newFrame("My first Frame");
//	_graph_item->newAxis("AxisX");
//	_graph_item->newPlot("plot1");
//	_graph_item->newPlot("plot2");
//	_graph_item->newLegend("leg1");
//	_graph_item->newTitle("Graph Title");
//	_graph_item->newShape("shp1");
//	view_graph->scene()->addItem( _graph_item );
//	tree_graph->addTopLevelItem(_graph_item);
//	_graph_item->updateGraphItem();
//	connect( _graph_item, SIGNAL(changed()),
//			 view_graph->scene(), SLOT(update()) );

	wid_graph = new GraphSettingsWidget(this);
	wid_frame = new FrameSettingsWidget(this);
	wid_axis = new AxisSettingsWidget(this);
//	wid_axis->setGraphItem( _graph_item );
	wid_plot = new PlotSettingsWidget(this);
	wid_legend = new LegendSettingsWidget(this);
	wid_title = new TitleSettingsWidget(this);
	wid_shape = new ShapeSettingsWidget(this);

	cont_settings = new QStackedWidget(this);
	cont_settings->addWidget( wid_graph );
	cont_settings->addWidget( wid_frame );
	cont_settings->addWidget( wid_axis );
	cont_settings->addWidget( wid_plot );
	cont_settings->addWidget( wid_legend );
	cont_settings->addWidget( wid_title );
	cont_settings->addWidget( wid_shape );

//	lay_sidebar = new QVBoxLayout(this);
//	lay_sidebar->addWidget(tree_graph);
//	lay_sidebar->addWidget(cont_settings);

	_graph_model = new GraphModel;

	connect( _graph_model, &GraphModel::currentGraphicsItemChanged,
	         this, &GraphEditor::onCurrentGraphicsItemChanged );


	_graph_tree_view = new GraphTreeView;
	_graph_tree_view->setModel( _graph_model );

	view_graph->setModel( _graph_model );

	sp_sidebar = new QSplitter(this);
	sp_sidebar->setOrientation( Qt::Vertical );
//	sp_sidebar->addWidget( tree_graph );
	sp_sidebar->addWidget( _graph_tree_view );
	sp_sidebar->addWidget( cont_settings );
//	wid_sidebar->setLayout( lay_sidebar );

	splitter = new QSplitter(this);
	splitter->addWidget( view_graph );
	splitter->addWidget( sp_sidebar );
	splitter->setSizes({(int)(0.7*width()),(int)(0.3*width())});

	lay_main = new QVBoxLayout(this);
	lay_main->addWidget(splitter);
	setLayout(lay_main);

//	tree_graph->expandAll();

	// Set current settings widget
//	this->onItemPressed( _graph_item, 0 );
}
/**********************************************/
/**********************************************/
//void GraphEditor::onItemPressed( QTreeWidgetItem* item, int column )
//{
//	Q_UNUSED( column )

//	if ( item->type() != QTreeWidgetItem::UserType )
//		return;

//	GraphItem* graph_item = static_cast<GraphItem*>(item);

//	SettingsWidget* current_wid = nullptr;
//	Key _type = graph_item->itemType();
//	if ( _type == Key::Graph )
//		current_wid = wid_graph;
//	else if ( _type == Key::Frame )
//		current_wid = wid_frame;
//	else if ( _type == Key::Axis )
//		current_wid = wid_axis;
//	else if ( _type == Key::Plot )
//		current_wid = wid_plot;
//	else if ( _type == Key::Legend )
//		current_wid = wid_legend;
//	else if ( _type == Key::Title )
//		current_wid = wid_title;
//	else if ( _type == Key::Shape )
//		current_wid = wid_shape;
//	if ( current_wid != nullptr )
//	{
//		cont_settings->setCurrentWidget( current_wid );
//		current_wid->connectSettings( &(graph_item->settings()) );
//	}
//}

void GraphEditor::onCurrentGraphicsItemChanged( GraphicsItem* item )
{
	if ( item == nullptr )
		return;

//	GraphicsItem* graph_item = static_cast<GraphicsItem*>(item);

	SettingsWidget* current_wid = nullptr;
	Key _type = item->itemType();
	if ( _type == Key::Graph )
		current_wid = wid_graph;
	else if ( _type == Key::Frame )
		current_wid = wid_frame;
	else if ( _type == Key::Axis )
		current_wid = wid_axis;
	else if ( _type == Key::Plot )
		current_wid = wid_plot;
	else if ( _type == Key::Legend )
		current_wid = wid_legend;
	else if ( _type == Key::Title )
		current_wid = wid_title;
	else if ( _type == Key::Shape )
		current_wid = wid_shape;
	if ( current_wid != nullptr )
	{
		cont_settings->setCurrentWidget( current_wid );
		current_wid->connectSettings( &(item->settings()) );
	}
}
/**********************************************/
/**********************************************/
void GraphEditor::closeEvent( QCloseEvent* event )
{
	QWidget::closeEvent( event );
}
/**********************************************/
/**********************************************/
void GraphEditor::graphChanged()
{
}
/**********************************************/
/**********************************************/
void GraphEditor::on_pb_newLayer_released()
{

}
/**********************************************/
/**********************************************/
void GraphEditor::on_pb_newAxis_released()
{
}
/**********************************************/
/**********************************************/
void GraphEditor::on_pb_newCurve_released()
{
}
/**********************************************/
/**********************************************/
void GraphEditor::on_pb_remove_released()
{
}
/**********************************************/
/**********************************************/
void GraphEditor::newFrame()
{
//	_graph_item->newFrame("New Frame");
//	_graph_item->updateGraphItem();
}
void GraphEditor::newAxis()
{
//	_graph_item->newAxis("New Axis");
//	_graph_item->updateGraphItem();
}
void GraphEditor::newPlot()
{
//	_graph_item->newPlot("New Plot");
//	_graph_item->updateGraphItem();
}
void GraphEditor::newLegend()
{
//	_graph_item->newLegend("New Legend");
//	_graph_item->updateGraphItem();
}
void GraphEditor::newTitle()
{
//	_graph_item->newTitle("New Title");
//	_graph_item->updateGraphItem();
}
void GraphEditor::newShape()
{
//	_graph_item->newShape("New Shape");
//	_graph_item->updateGraphItem();
}
void GraphEditor::resetGraph()
{

}
void GraphEditor::deleteItem()
{

}
/**********************************************/
/**********************************************/
void GraphEditor::setGraphPath( const QString& path )
{
}
void GraphEditor::setGraphFile( ProjectItem* graph_item )
{
	if ( graph_item->type() != Key::Graph )
		return;

	QString tmpfile = Files::tmpPath() + "/" +
	                  graph_item->id();
	if ( QFileInfo(tmpfile).exists() )
	{

	}
	else
	{
		QString sourcefile = graph_item->path();
		if ( QFileInfo(sourcefile).exists() )
		{

		}
		else
		{

		}
	}
}
/**********************************************/
/**********************************************/
void GraphEditor::setAvailableData( const QStringList& availableData )
{
}
/**********************************************/
/**********************************************/
void GraphEditor::saveGraph()
{
}
/**********************************************/
/**********************************************/
void GraphEditor::on_pb_save_released()
{
}

void GraphEditor::hideEvent( QHideEvent* event )
{

}
/**********************************************/
/**********************************************/
