#ifndef SETTINGSFRAME_H
#define SETTINGSFRAME_H

#include <QWidget>
#include "Commons/WidgetListSelector.h"
#include "SettingsWidgets/ThemesColorsSettings.h"
#include "Commons/theme.h"
#include "Commons/lang.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
enum SettingsWidget : char
{
	NONE = '0',
	THEMESCOLORS = 't',
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class SettingsFrame
		: public QWidget
{
	Q_OBJECT
private:
	QVBoxLayout* lay_main = new QVBoxLayout;
	WidgetListSelector* wid_container = new WidgetListSelector;
	SettingsWidget currentWidget = SettingsWidget::NONE;
	ThemesColorsSettings *themesColors = nullptr;

public:
	explicit SettingsFrame( QWidget *parent = nullptr );
	~SettingsFrame();
	void showThemeColors();
	void removeCurrentSettings();
	void updateIcons( Icons* icons );
	void updateLang( Lang* lang );


};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SETTINGSFRAME_H
