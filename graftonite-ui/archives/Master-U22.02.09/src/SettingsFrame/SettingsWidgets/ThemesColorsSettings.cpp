#include "ThemesColorsSettings.h"
/**********************************************/
/**********************************************/
/* */
ThemesColorsSettings::~ThemesColorsSettings() {
}
/**********************************************/
/**********************************************/
/* */
ThemesColorsSettings::ThemesColorsSettings( QWidget *parent )
	: QWidget(parent)
{
//	lay_main->setMargin( 0 );
	lay_main->addWidget( lab_title );
	lab_title->setText( "Theme & Color" );
	lay_main->addWidget( wid_tab );

	wid_tab->addTab( new QWidget, "Page 1" );
	wid_tab->addTab( new QWidget, "Page 2" );
	wid_tab->addTab( new QWidget, "Page 3" );

	setLayout( lay_main );
}
/**********************************************/
/**********************************************/
