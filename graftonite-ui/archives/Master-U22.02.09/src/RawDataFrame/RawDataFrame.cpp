#include "RawDataFrame.h"
#include <QTableWidget>
#include "Commons/FormLayout.h"

/**********************************************/
/**********************************************/
/* */
RawDataFrame::~RawDataFrame() {
}
/**********************************************/
/**********************************************/
/* */
RawDataFrame::RawDataFrame(QWidget *parent)
	: QWidget(parent)
{
	lab_title->setText( "Ttitle" );
	lay_main->addWidget( lab_title, 0, 0 );
	lay_main->addWidget( wid_main, 1, 0 );

	wid_main->addTab( wid_rawData, "RawData" );
	wid_main->addTab( table_data, "Data" );

	wid_rawData->setLayout( lay_rawData );

	lab_filePath->setText( "FilePath : " );
	grp_dataFormat->setTitle( "Data Format" );
	lab_separator->setText( "Separator : " );
	ch_combinedSep->setText( "Combined Separators" );
	ch_useFirstRow->setText( "Use First Row as Header" );
	grp_dataToRead->setTitle( "Data to Read" );
	lab_startRow->setText( "Start Line : " );
	lab_endRow->setText( "End Line : " );
	lay_rawData->addWidget( text_rawData, 0, 0, 4, 1 );
	lay_rawData->addWidget( lab_filePath, 0, 1 );
	lay_rawData->addWidget( le_filePath, 0, 2 );
	lay_rawData->addWidget( but_filePath, 0, 3 );
	lay_rawData->addWidget( grp_dataFormat, 1, 1, 1, 3 );
	FormLayout* lay_dataFormat = new FormLayout;
	lay_dataFormat->addRow( lab_separator, le_separator );
	lay_dataFormat->addRow( ch_combinedSep );
	lay_dataFormat->addRow( ch_useFirstRow );
	grp_dataFormat->setLayout( lay_dataFormat );
	lay_rawData->addWidget( grp_dataToRead, 2, 1, 1, 3 );
	FormLayout* lay_dataToRead = new FormLayout;
	lay_dataToRead->addRow( lab_startRow, sp_startRow );
	lay_dataToRead->addRow( lab_endRow, sp_endRow );
	grp_dataToRead->setLayout( lay_dataToRead );

	lay_rawData->addItem( new QSpacerItem( 0, 0, QSizePolicy::Maximum, QSizePolicy::Expanding ), 3, 1, 1, 3 );

	setLayout( lay_main );

	hide();
}
/**********************************************/
/**********************************************/
/* */
void RawDataFrame::updateIcons( Icons* icons ) {
Q_UNUSED(icons)
}
/**********************************************/
/**********************************************/
/* */
void RawDataFrame::updateLang( Lang* lang ) {
Q_UNUSED(lang)
}
/**********************************************/
/**********************************************/
