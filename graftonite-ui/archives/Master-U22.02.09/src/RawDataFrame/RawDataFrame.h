#ifndef RAWDATAFRAME_H
#define RAWDATAFRAME_H

#include <QWidget>
#include <QGridLayout>
#include <QTableWidget>
#include <QTabWidget>
#include <QLabel>
#include <QTextEdit>
#include <QLineEdit>
#include <QCheckBox>
#include <QPushButton>
#include <QSpinBox>
#include <QGroupBox>
#include <QFormLayout>
#include "Commons/theme.h"
#include "Commons/lang.h"

/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class RawDataFrame : public QWidget
{
	Q_OBJECT
private:
	QGridLayout* lay_main = new QGridLayout;
	QLabel* lab_title = new QLabel;
	QTabWidget* wid_main = new QTabWidget;
	QWidget* wid_rawData = new QWidget;
	QGridLayout* lay_rawData = new QGridLayout;
	QTextEdit* text_rawData = new QTextEdit;
	QLabel* lab_filePath = new QLabel;
	QLineEdit* le_filePath = new QLineEdit;
	QPushButton* but_filePath = new QPushButton;
	QGroupBox* grp_dataFormat = new QGroupBox;
	QLabel* lab_separator = new QLabel;
	QLineEdit* le_separator = new QLineEdit;
	QCheckBox* ch_combinedSep = new QCheckBox;
	QCheckBox* ch_useFirstRow = new QCheckBox;
	QGroupBox* grp_dataToRead = new QGroupBox;
	QLabel* lab_startRow = new QLabel;
	QSpinBox* sp_startRow = new QSpinBox;
	QLabel* lab_endRow = new QLabel;
	QSpinBox* sp_endRow = new QSpinBox;
	QTableWidget* table_data = new QTableWidget;

public:
	explicit RawDataFrame(QWidget *parent = nullptr);
	~RawDataFrame();
	void updateIcons( Icons* icons );
	void updateLang( Lang* lang );

};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // RAWDATAFRAME_H
