QT       += core gui svg charts xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    GraftoniteWindow.cpp \
    GraphFrame/Axis/AxisGridsItem.cpp \
    GraphFrame/Axis/AxisGridsSettingsView.cpp \
    GraphFrame/Axis/AxisLabelsItem.cpp \
    GraphFrame/Axis/AxisLabelsSettingsView.cpp \
    GraphFrame/Axis/AxisLineItem.cpp \
    GraphFrame/Axis/AxisLineSettingsView.cpp \
    GraphFrame/Axis/AxisTicksItem.cpp \
    GraphFrame/Axis/AxisTicksSettingsView.cpp \
    GraphFrame/Axis/AxisTitleItem.cpp \
    GraphFrame/Axis/AxisTitleSettingsView.cpp \
    GraphFrame/AxisItem.cpp \
    GraphFrame/AxisSettings.cpp \
    GraphFrame/AxisSettingsView.cpp \
    GraphFrame/BedItem.cpp \
    GraphFrame/Curve/CurveAreaItem.cpp \
    GraphFrame/Curve/CurveAreaSettingsView.cpp \
    GraphFrame/Curve/CurveBarsItem.cpp \
    GraphFrame/Curve/CurveBarsSettingsView.cpp \
    GraphFrame/Curve/CurveErrorBarsItem.cpp \
    GraphFrame/Curve/CurveErrorBarsSettingsView.cpp \
    GraphFrame/Curve/CurveLabelsItem.cpp \
    GraphFrame/Curve/CurveLabelsSettingsView.cpp \
    GraphFrame/Curve/CurveLineItem.cpp \
    GraphFrame/Curve/CurveLineSettingsView.cpp \
    GraphFrame/Curve/CurveSymbolsItem.cpp \
    GraphFrame/Curve/CurveSymbolsSettingsView.cpp \
    GraphFrame/CurveItem.cpp \
    GraphFrame/CurveSettings.cpp \
    GraphFrame/CurveSettingsView.cpp \
    GraphFrame/GraphItem.cpp \
    GraphFrame/GraphSettings.cpp \
    GraphFrame/GraphSettingsView.cpp \
    GraphFrame/GraphView.cpp \
    GraphFrame/GraphWidget.cpp \
    GraphFrame/LayerItem.cpp \
    GraphFrame/LayerSettings.cpp \
    GraphFrame/LayerSettingsView.cpp \
    GraphFrame/LegendItem.cpp \
    GraphFrame/LegendSettings.cpp \
    GraphFrame/LegendSettingsView.cpp \
    GraphFrame/Misc/DeleteDialog.cpp \
    GraphFrame/Misc/GraphMisc.cpp \
    GraphFrame/Misc/GraphTextItem.cpp \
    GraphFrame/ShapeItem.cpp \
    GraphFrame/ShapeSettings.cpp \
    GraphFrame/ShapeSettingsView.cpp \
    GraphFrame/TitleItem.cpp \
    ProjectExplorer/ProjectTree.cpp \
    ProjectExplorer/projectexplorer.cpp \
    DataFrame/DataFrame.cpp \
    GraphFrame/GraphFrame.cpp \
    PictureFrame/PictureFrame.cpp \
    RawDataFrame/RawDataFrame.cpp \
    SettingsFrame/SettingsFrame.cpp \
    SettingsFrame/SettingsWidgets/ThemesColorsSettings.cpp \
    WelcomeFrame/WelcomeFrame.cpp \
    Commons/selectedelement.cpp \
    Commons/customqwidget.cpp \
    Commons/linedtexteditor.cpp \
    Commons/zoomablelabel.cpp \
    main.cpp

HEADERS += \
    GraftoniteWindow.h \
    GraphFrame/Axis.h \
    GraphFrame/Axis/AxisGridsItem.h \
    GraphFrame/Axis/AxisGridsSettingsView.h \
    GraphFrame/Axis/AxisLabelsItem.h \
    GraphFrame/Axis/AxisLabelsSettingsView.h \
    GraphFrame/Axis/AxisLineItem.h \
    GraphFrame/Axis/AxisLineSettingsView.h \
    GraphFrame/Axis/AxisTicksItem.h \
    GraphFrame/Axis/AxisTicksSettingsView.h \
    GraphFrame/Axis/AxisTitleItem.h \
    GraphFrame/Axis/AxisTitleSettingsView.h \
    GraphFrame/AxisItem.h \
    GraphFrame/AxisSettings.h \
    GraphFrame/AxisSettingsView.h \
    GraphFrame/BedItem.h \
    GraphFrame/Curve.h \
    GraphFrame/Curve/CurveAreaItem.h \
    GraphFrame/Curve/CurveAreaSettingsView.h \
    GraphFrame/Curve/CurveBarsItem.h \
    GraphFrame/Curve/CurveBarsSettingsView.h \
    GraphFrame/Curve/CurveErrorBarsItem.h \
    GraphFrame/Curve/CurveErrorBarsSettingsView.h \
    GraphFrame/Curve/CurveLabelsItem.h \
    GraphFrame/Curve/CurveLabelsSettingsView.h \
    GraphFrame/Curve/CurveLineItem.h \
    GraphFrame/Curve/CurveLineSettingsView.h \
    GraphFrame/Curve/CurveSymbolsItem.h \
    GraphFrame/Curve/CurveSymbolsSettingsView.h \
    GraphFrame/CurveItem.h \
    GraphFrame/CurveSettings.h \
    GraphFrame/CurveSettingsView.h \
    GraphFrame/GraphItem.h \
    GraphFrame/GraphSettings.h \
    GraphFrame/GraphSettingsView.h \
    GraphFrame/GraphView.h \
    GraphFrame/GraphWidget.h \
    GraphFrame/LayerItem.h \
    GraphFrame/LayerSettings.h \
    GraphFrame/LayerSettingsView.h \
    GraphFrame/LegendItem.h \
    GraphFrame/LegendSettings.h \
    GraphFrame/LegendSettingsView.h \
    GraphFrame/Misc/AnchorSelector.h \
    GraphFrame/Misc/AreaDirection.h \
    GraphFrame/Misc/AxisGridsPosSelector.h \
    GraphFrame/Misc/AxisLabelsPosSelector.h \
    GraphFrame/Misc/AxisPosSelector.h \
    GraphFrame/Misc/AxisScaleSelector.h \
    GraphFrame/Misc/AxisTicksPosSelector.h \
    GraphFrame/Misc/AxisTitlePosSelector.h \
    GraphFrame/Misc/AxisTypeSelector.h \
    GraphFrame/Misc/BarsDirection.h \
    GraphFrame/Misc/BrushPattern.h \
    GraphFrame/Misc/ColorButton.h \
    GraphFrame/Misc/ColorDialog.h \
    GraphFrame/Misc/CurveLineTypeSelector.h \
    GraphFrame/Misc/CurveTypeSelector.h \
    GraphFrame/Misc/DeleteDialog.h \
    GraphFrame/Misc/ErrorBarsType.h \
    GraphFrame/Misc/GraphMisc.h \
    GraphFrame/Misc/GraphTextItem.h \
    GraphFrame/Misc/LegendPosSelector.h \
    GraphFrame/Misc/LineCapSelector.h \
    GraphFrame/Misc/LineJoinSelector.h \
    GraphFrame/Misc/LineStyleSelector.h \
    GraphFrame/Misc/LineTypeSelector.h \
    GraphFrame/Misc/NumberDecimalSepSelector.h \
    GraphFrame/Misc/NumberFormatOptionSelector.h \
    GraphFrame/Misc/NumberFormatSelector.h \
    GraphFrame/Misc/SymbolsTypeSelector.h \
    GraphFrame/Misc/TextAlignSelector.h \
    GraphFrame/Misc/TextCapSelector.h \
    GraphFrame/Misc/TextEditor.h \
    GraphFrame/ShapeItem.h \
    GraphFrame/ShapeSettings.h \
    GraphFrame/ShapeSettingsView.h \
    GraphFrame/TitleItem.h \
    ProjectExplorer/ProjectTree.h \
    ProjectExplorer/projectexplorer.h \
    DataFrame/DataFrame.h \
    GraphFrame/GraphFrame.h \
    PictureFrame/PictureFrame.h \
    RawDataFrame/RawDataFrame.h \
    SettingsFrame/SettingsFrame.h \
    SettingsFrame/SettingsWidgets/SettingsWidgets.h \
    SettingsFrame/SettingsWidgets/ThemesColorsSettings.h \
    WelcomeFrame/WelcomeFrame.h \
    WelcomeFrame/WelcomeModels.h \
    WelcomeFrame/WelcomeWidgets.h \
    Widgets/ClickableLabel.h \
    Widgets/PushButtons.h \
    Commons/ColorSelector.h \
    Commons/DoubleSelector.h \
    Commons/ElidedButton.h \
    Commons/FlowLayout.h \
    Commons/FormLayout.h \
    Commons/GridLayout.h \
    Commons/IntegerSelector.h \
    Commons/LineEditor.h \
    Commons/PushButton.h \
    Commons/ScrollArea.h \
    Commons/SettingLabel.h \
    Commons/TabWidget.h \
    Commons/WidgetListSelector.h \
    Commons/lang.h \
    Commons/selectedelement.h \
    Commons/customqwidget.h \
    Commons/linedtexteditor.h \
    Commons/theme.h \
    Commons/zoomablelabel.h

FORMS +=

TRANSLATIONS += \

# Default rules for deployment.
#qnx: target.path = /tmp/$${TARGET}/bin
#else: unix:!android: target.path = /opt/$${TARGET}/bin
#!isEmpty(target.path): INSTALLS += target

RESOURCES +=

QMAKE_CXXFLAGS_WARN_OFF = -Wno-unused-parameter
