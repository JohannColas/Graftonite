#ifndef LINEEDITOR_H
#define LINEEDITOR_H

#include <QLineEdit>

class LineEditor
		: public QLineEdit
{
	Q_OBJECT

public:
	~LineEditor()
	{

	}
	LineEditor( QWidget* parent = nullptr )
		: QLineEdit( parent )
	{
		connect( this, &LineEditor::editingFinished,
				 this, &LineEditor::editingEnded );
	}

public slots:
	void editingEnded()
	{
		emit lineChanged( text() );
	}

signals:
	void lineChanged( const QString& line );
};

#endif // LINEEDITOR_H
