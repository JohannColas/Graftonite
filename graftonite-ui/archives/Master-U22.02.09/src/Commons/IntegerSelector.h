#ifndef INTEGERSELECTOR_H
#define INTEGERSELECTOR_H

#include <QSpinBox>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class IntegerSelector
		: public QSpinBox
{
	Q_OBJECT
public:
	~IntegerSelector()
	{

	}
	IntegerSelector( QWidget* parent = nullptr )
		: QSpinBox( parent )
	{
		setSuffix( " px" );
		setWrapping( true );
		connect( this, SIGNAL( valueChanged(int) ),
				 this, SIGNAL( changed(int) ) );
	}
	void set( int min = 0, int max = 100000, QString suffix = "", int step = 1 )
	{
		setWrapping( false );
		setMinimum( min );
		setMaximum( max );
		setSingleStep( step );
		setSuffix( suffix );
	}
	void setFontSize()
	{
		setWrapping( false );
		setMaximum( 1000 );
		setSuffix( " pt" );
	}
	void setBorderWidth()
	{
		setWrapping( false );
		setMinimum( 0 );
		setMaximum( 1000 );
	}
	void setDimension( int min = 0, int max = 100000 )
	{
		setWrapping( false );
		setMinimum( min );
		setMaximum( max );
		setSuffix( " px" );
	}
	void setOffset()
	{
		setWrapping( false );
		setMinimum( -100000 );
		setMaximum( 100000 );
		setSuffix( " px" );
	}
	void setPerCent()
	{
		setWrapping( false );
		setMinimum( 0 );
		setMaximum( 100 );
		setSuffix( " %" );
	}

signals:
	void changed( int value );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // INTEGERSELECTOR_H
