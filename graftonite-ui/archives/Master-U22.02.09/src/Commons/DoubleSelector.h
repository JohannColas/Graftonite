#ifndef DOUBLESELECTOR_H
#define DOUBLESELECTOR_H

#include <QDoubleSpinBox>

/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class DoubleSelector
		: public QDoubleSpinBox
{
	Q_OBJECT
public:
	~DoubleSelector()
	{

	}
	DoubleSelector( QWidget* parent = nullptr )
		: QDoubleSpinBox( parent )
	{
		setWrapping( true );
		connect( this, SIGNAL( valueChanged(double) ),
				 this, SIGNAL( changed(double) ) );
	}
	void set( int min = 0, int max = 100000, int dec = 0, QString suffix = " px",  int step = 1 )
	{
		setWrapping( false );
		setMinimum( min );
		setMaximum( max );
		setDecimals( dec );
		setSingleStep( step );
		setSuffix( suffix );
	}
	void setOffset()
	{
		setWrapping( true );
		setMinimum( -100000 );
		setMaximum( 100000 );
		setDecimals( 0 );
		setSuffix( " px" );
	}
	void setAngle()
	{
		setWrapping( true );
		setMinimum( -180 );
		setMaximum( 180 );
		setDecimals( 1 );
		setSingleStep( 5 );
		setSuffix( " %" );
	}
	void setAngleD()
	{
		setWrapping( true );
		setMinimum( 0 );
		setMaximum( 360 );
		setDecimals( 1 );
		setSingleStep( 5 );
		setSuffix( " %" );
	}
	void setFontSize()
	{
		setWrapping( false );
		setMaximum( 1 );
		setMaximum( 1000 );
		setSuffix( " pt" );
	}
	void setDimension( int min = 0, int max = 100000, int dec = 0 )
	{
		setWrapping( false );
		setMinimum( min );
		setMaximum( max );
		setDecimals( dec );
		setSuffix( " px" );
	}
	void setPerCent()
	{
		setWrapping( false );
		setMinimum( 0 );
		setMaximum( 100 );
		setDecimals( 0 );
		setSingleStep( 5 );
		setSuffix( " %" );
	}

signals:
	void changed( double value );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // DOUBLESELECTOR_H
