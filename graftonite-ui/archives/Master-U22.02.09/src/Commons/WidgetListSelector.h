#ifndef WIDGETLISTSELECTOR_H
#define WIDGETLISTSELECTOR_H

#include <QWidget>
#include <QLabel>
#include <QPushButton>
#include "Commons/GridLayout.h"
#include "Commons/ScrollArea.h"

//#include <QDebug>
/**********************************************/
/**********************************************/
/**********************************************/
/* LabelItemList
 *
 * */
class LabelItemList
		: public QLabel
{
	Q_OBJECT
public:
	LabelItemList( const QString& text = "", QWidget* parent = nullptr )
		: QLabel( parent )
	{
		setText( text );
		setSizePolicy( QSizePolicy::Preferred,
					   QSizePolicy::Preferred );
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
/* ButtonlItemList
 *
 * */
class ButtonItemList
		: public QPushButton
{
	Q_OBJECT
private:
	int index = -1;

public:
	ButtonItemList( const QString& text = "", QWidget* parent = nullptr )
		: QPushButton( parent )
	{
		setText( text );
		setCheckable( true );
		setChecked( false );
		setSizePolicy( QSizePolicy::Preferred,
					   QSizePolicy::Preferred );
		setIconSize( {16, 16} );
		connect( this, &ButtonItemList::clicked,
				 this, &ButtonItemList::onClicked );
	}
	void setSVG( const QString& path )
	{
		setIcon( QIcon( path ) );
	}
	int getIndex() const {
		return index;
	}

public slots:
	void setIndex( int ind ) {
		index = ind;
	}
	void onClicked()
	{
		emit sendIndex( getIndex() );
	}

signals:
	void sendIndex( int index );
};
/**********************************************/
/**********************************************/
/**********************************************/
/* WidgetListSelector
 *
 * */
class WidgetListSelector
		: public QWidget
{
	Q_OBJECT

private:
	ScrollArea* list = new ScrollArea;
	GridLayout* layMain = new GridLayout;
	QVBoxLayout* layout = new QVBoxLayout;
	QList<QLabel*> _sections;
	QList<ButtonItemList*> _items;
	QList<QWidget*> _widgets;
	int _nbItems = 0;
	int _nbSections = 0;
	int _selectedTab = -1;

public:
	WidgetListSelector( QWidget* parent = nullptr )
		: QWidget( parent )
	{
		setMinimumWidth( 250 );
		setLayout( layMain );
		layMain->setSpacing(0);
//		layMain->setMargin(0);
		QWidget* container = new QWidget;
		QVBoxLayout* layout2 = new QVBoxLayout;
		layout2->addLayout( layout );
		layout2->addItem( new QSpacerItem(0,0,QSizePolicy::Preferred,QSizePolicy::Expanding));
		container->setLayout( layout2 );
		list->setWidget( container );
		list->setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
		list->setMaximumWidth( 250 );
		list->setFrameShape( QFrame::StyledPanel );
		layMain->addWidget( list, 0, 0 );
	}
	void addItem( const QString& name, QWidget* widget = new QWidget )
	{
		ButtonItemList* newItem = new ButtonItemList( name );
		newItem->setIndex( _nbItems );
		++_nbItems;
		connect( newItem, &ButtonItemList::sendIndex,
				 this, &WidgetListSelector::changeCurrentWidget );
		layout->addWidget( newItem );
		_items.append( newItem );
		_widgets.append( widget );
		widget->hide();

		int col = 1;
		layMain->addWidget( widget, 0, col );
	}
	void addSection( const QString& section )
	{
		LabelItemList* newSection = new LabelItemList( section );
		layout->addWidget( newSection );
		_sections.append( newSection );
	}
	void setSectionText( int index, const QString& text )
	{
		if ( index > -1 && index < _nbSections ) {
			_sections.at( index )->setText( text );
		}
	}
	void setItemText( int index, const QString& text )
	{
		if ( index > -1 && index < _nbItems ) {
			_items.at( index )->setText( text );
		}
	}
	void setItemIcon( int index, const QIcon& icon )
	{
		if ( index > -1 && index < _nbItems ) {
			_items.at( index )->setIcon( icon );
		}
	}
	void setWidget( int index, QWidget* widget )
	{
		if ( index < _nbItems ) {
			_widgets.replace( index, widget );
		}
	}

public slots:
	void changeCurrentWidget( int index ) {
		if ( index > -1 && index < _nbItems )
		{
			if ( _selectedTab > -1 && _selectedTab < _nbItems ) {
				_items.at( _selectedTab )->setChecked( false );
				_widgets.at( _selectedTab )->hide();
			}
			if ( index > -1 && index < _nbItems ) {
				_selectedTab = index;
				_items.at( index )->setChecked( true );
				_widgets.at( index )->show();
			}
		}
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // WIDGETLISTSELECTOR_H
