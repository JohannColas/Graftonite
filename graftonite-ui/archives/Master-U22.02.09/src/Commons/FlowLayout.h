/****************************************************************************
**
** Copyright (C) 2015 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/


#ifndef FLOWLAYOUT_H
#define FLOWLAYOUT_H

#include <QWidget>
#include <QLayout>
#include <QStyle>
#include <QDebug>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class FlowLayout
		: public QLayout
{
public:
	~FlowLayout() {
		QLayoutItem *item;
		while ( (item = takeAt(0)) )
			delete item;
	}
	FlowLayout( QWidget *parent )
		: QLayout( parent )
	{
		setContentsMargins( 0, 0, 0, 0 );

	}
	FlowLayout() {
		setContentsMargins( 0, 0, 0, 0 );
//		setMargin(0);
//		setProperty("leftMargin",0);
//		setProperty("RightMargin",0);
//		setProperty("TopMargin",0);
//		setProperty("BottomMargin",0);
	}
	void addItem( QLayoutItem *item ) {
		itemList.append( item );
	}
//	Qt::Orientations expandingDirections() const
//	{
//		return Qt::Orientations::setFlag();
//	}
	bool hasHeightForWidth() const {
		return true;
	}
	int heightForWidth( int width ) const {
		int height = updateLayout( QRect(0, 0, width, 0), true );
		return height;
	}
	int count() const {
		return itemList.size();
	}
	QLayoutItem *itemAt( int index ) const {
		return itemList.value( index );
	}
	QSize minimumSize() const {
		QSize size;
		QLayoutItem *item;
		foreach ( item, itemList )
			size = size.expandedTo( item->minimumSize() );

//		size += QSize( 2*margin(), 2*margin() );
		return size;
	}
	void setGeometry( const QRect &rect ) {
		QLayout::setGeometry( rect );
		updateLayout( rect, false );
	}
	QSize sizeHint() const {
		return minimumSize();
	}
	QLayoutItem *takeAt( int index ) {
		if (index >= 0 && index < itemList.size())
			return itemList.takeAt(index);
		else
			return 0;
	}

private:
	int updateLayout( const QRect &rect, bool testOnly ) const
	{
//		QRect effectiveRect = rect;
		int x = rect.x();
		int y = rect.y();
		int lineHeight = 0;
		QList<int> cuts;
		QList<int> widths;
		int width = 0;
		for ( int it = 0; it < count(); ++it ) {
			if ( width+50 >= rect.width() ) {
				cuts.append( it );
				widths.append( width );
				width = 0;
			}
			width += itemList.at(it)->sizeHint().width();
		}
		widths.append( width );
//		int cutIt = 0;
//		if ( cuts.size() > 0 ) {
//			x = rect.x() + 0.5*(rect.width()-widths.at(0));
//		}
//		else {
//			x = rect.x() + 0.5*(rect.width()-width);
//		}
		for ( int it = 0; it < count(); ++it ) {
			QLayoutItem* item = itemList.at(it);
			int nextX = x + item->sizeHint().width();
			if ( nextX > rect.right() &&
				 lineHeight > 0 ) {
				x = rect.x();
				nextX = x + item->sizeHint().width();
				y = y + lineHeight;
				lineHeight = 0;
			}
//			if ( cutIt < cuts.size() )
//				if ( it == cuts.at(cutIt) )
//				{
//					x = rect.x()+0.5*(rect.width()-widths.at(cutIt+1));

//					nextX = x + item->sizeHint().width();
//					y = y + lineHeight;
//					lineHeight = 0;

//					++cutIt;
//				}

			if ( !testOnly )
				item->setGeometry( QRect( QPoint(x, y),
										  item->sizeHint() ) );

			x = nextX;
			lineHeight = qMax( lineHeight, item->sizeHint().height() );
		}
		return y + lineHeight - rect.y();
	}

	QList<QLayoutItem*> itemList;
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // FLOWLAYOUT_H
