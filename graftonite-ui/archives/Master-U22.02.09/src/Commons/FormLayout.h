#ifndef FORMLAYOUT_H
#define FORMLAYOUT_H

#include <QFormLayout>
#include <QWidget>
#include "GridLayout.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class FormLayout
		: public QFormLayout
{

public:
	~FormLayout()
	{

	}
	FormLayout( QWidget* parent = nullptr )
		: QFormLayout( parent )
	{
		setLabelAlignment( Qt::AlignRight );
		setFieldGrowthPolicy( QFormLayout::FieldsStayAtSizeHint );
//		setSizeConstraint( QLayout::SetNoConstraint );
		//		setProperty( "leftMargin", 0 );
		//		setProperty( "rightMargin", 0 );
		//		setMargin( 0 );
	}

	void addLine( QWidget* label, QWidget* wid1, QWidget* wid2 )
	{
		//		QWidget* wid0 = new QWidget;
		GridLayout* lay0 = new GridLayout;
//		lay0->setMargin( 0 );
		lay0->addWidget( wid1, 0, 0 );
		lay0->addWidget( wid2, 0, 1 );
		//		wid0->setLayout( lay0 );
		addRow( label, lay0 );
	}
	void addLine( QWidget* label, QWidget* wid1, QWidget* wid2, QWidget* wid3  )
	{
		//		QWidget* wid0 = new QWidget;
		GridLayout* lay0 = new GridLayout;
//		lay0->setMargin( 0 );
		lay0->addWidget( wid1, 0, 0 );
		lay0->addWidget( wid2, 0, 1 );
		lay0->addWidget( wid3, 0, 2 );
		//		wid0->setLayout( lay0 );
		addRow( label, lay0 );
	}
	void addFullLine( QWidget* wid0, QWidget* wid1, QWidget* wid2 )
	{
		QWidget* widA = new QWidget;
		GridLayout* lay0 = new GridLayout;
//		lay0->setMargin( 0 );
		lay0->addWidget( wid0, 0, 0 );
		lay0->addWidget( wid1, 0, 1 );
		lay0->addWidget( wid2, 0, 2 );
		widA->setLayout( lay0 );
		addRow( widA );
	}
	void addFullLine( QWidget* wid0, QWidget* wid1, QWidget* wid2, QWidget* wid3 )
	{
		QWidget* widA = new QWidget;
		GridLayout* lay0 = new GridLayout;
//		lay0->setMargin( 0 );
		lay0->addWidget( wid0, 0, 0 );
		lay0->addWidget( wid1, 0, 1 );
		lay0->addWidget( wid2, 0, 2 );
		lay0->addWidget( wid3, 0, 3 );
		widA->setLayout( lay0 );
		addRow( widA );
	}
	void addFullLine( QList<QWidget*> widsList )
	{
		QWidget* widA = new QWidget;
		GridLayout* lay0 = new GridLayout;
//		lay0->setMargin( 0 );
		int it = 0;
		for ( ; it < widsList.size(); ++it )
		{
			lay0->addWidget( widsList.at( it ), 0, it );
		}
		lay0->addItem( new QSpacerItem(0, 0, QSizePolicy::Expanding), 0, it );
		widA->setLayout( lay0 );
		addRow( widA );
	}
	void addFullLine( QWidget* wi0, QList<QWidget*> widsList )
	{
		QWidget* widA = new QWidget;
		GridLayout* lay0 = new GridLayout;
//		lay0->setMargin( 0 );
		int it = 0;
		for ( ; it < widsList.size(); ++it )
		{
			lay0->addWidget( widsList.at( it ), 0, it );
		}
		lay0->addItem( new QSpacerItem(0, 0, QSizePolicy::Expanding), 0, it );
		widA->setLayout( lay0 );
		addRow( wi0, widA );
	}
	void addLineField( QWidget* wid1, QWidget* wid2 )
	{
		//		QWidget* wid0 = new QWidget;
		GridLayout* lay0 = new GridLayout;
//		lay0->setMargin( 0 );
		lay0->addWidget( wid1, 0, 0 );
		lay0->addWidget( wid2, 0, 1 );
		//		wid0->setLayout( lay0 );
		addRow( new QWidget, lay0 );
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // FORMLAYOUT_H
