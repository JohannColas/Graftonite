#ifndef CUSTOMQWIDGET_H
#define CUSTOMQWIDGET_H

#include <QWidget>
#include <QPoint>

/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class customQWidget : public QWidget {
    Q_OBJECT

public:
    customQWidget();
    customQWidget(QWidget *parent);
    //void
    void leaveEvent(QEvent* event);

signals:
    void leaved();

};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // CUSTOMQWIDGET_H
