#ifndef ZOOMSCROLLAREA_H
#define ZOOMSCROLLAREA_H

#include <QObject>
#include <QScrollArea>
#include <QLabel>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class zoomableLabel : public QLabel {
    Q_OBJECT
public:
    zoomableLabel();
    zoomableLabel(QWidget *parent);

public slots:
    void setParentSize(QSize newSize);

private:
    QPoint oldPos;
    QSize parentSize;
    Qt::KeyboardModifiers _modifiers = Qt::ControlModifier;
    bool event(QEvent *myEvent);
    void resizeEvent(QResizeEvent *e);

private slots:
    void zoomLabel(int zoomDirec);
    void moveLabel();
    //void setParentSize(QSize newSize);

signals:
    void moved(QPoint values, QSize maximums);
    void resized(QSize size);


};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ZOOMSCROLLAREA_H
