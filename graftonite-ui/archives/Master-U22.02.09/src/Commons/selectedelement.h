#ifndef SELECTEDELEMENT_H
#define SELECTEDELEMENT_H

#include <QString>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class SelectedElement
{
public:
    SelectedElement();
    QString name;
    QString path;
    QString type;
    void setType(QString newtype);
    QString getType();
    void setName(QString newname);
    QString getName();
    void setPath(QString newpath);
    QString getPath();

};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SELECTEDELEMENT_H
