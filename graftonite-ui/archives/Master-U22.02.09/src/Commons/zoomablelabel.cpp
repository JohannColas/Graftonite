#include "zoomablelabel.h"

#include <QApplication>
#include <QLabel>
#include <QScrollBar>
#include <QMouseEvent>
#include <QDebug>
/**********************************************/
/**********************************************/
/* */
zoomableLabel::zoomableLabel()
	: QLabel()
{
	this->setGeometry(0, 0, 100, 100);
	parentSize.setWidth(0);
	parentSize.setHeight(0);
}
/**********************************************/
/**********************************************/
/* */
zoomableLabel::zoomableLabel(QWidget *parent) : QLabel(parent) {
	this->setGeometry(0, 0, 100, 100);
	parentSize.setWidth(0);
	parentSize.setHeight(0);
}
/**********************************************/
/**********************************************/
/* */
void zoomableLabel::zoomLabel(int zoomDirec) {
	double scaleFactor = 2;
	if (zoomDirec == -1) {
		scaleFactor = 0.5;
	}
	// Get position of the curseur in this widget
	QPoint curPos = this->mapFromGlobal(QCursor::pos());
	curPos += this->pos(); // Ajusting the position of the curseur
	int x = (int)((double)(curPos.x()) + scaleFactor * (double)(this->pos().x() - curPos.x()));
	int y = (int)((double)(curPos.y()) + scaleFactor * (double)(this->pos().y() - curPos.y()));
	int w = (int)(scaleFactor * (double)(this->width()));
	int h = (int)(scaleFactor * (double)(this->height()));
	this->setGeometry(x, y, w, h);
	this->move(x, y);
	emit moved(-this->pos(), this->size() - parentSize + QSize(2, 2));
}

void zoomableLabel::moveLabel() {
	QPoint labelPos = this->mapFromGlobal(QCursor::pos());
	labelPos = labelPos - oldPos + this->pos();
	if (parentSize.width() > this->size().width()) {
		if (labelPos.x() < 0) {
			labelPos.setX(0);
		}
		if (labelPos.x() > parentSize.width() - this->width() - 2) {
			labelPos.setX(parentSize.width() - this->width() - 2);
		}
	}
	else {
		if (labelPos.x() + this->width() + 2 < parentSize.width()) {
			labelPos.setX(parentSize.width() - this->width() - 2);
		}
		if (labelPos.x() > 0) {
			labelPos.setX(0);
		}
	}
	if (parentSize.height() > this->size().height()) {
		if (labelPos.y() < 0) {
			labelPos.setY(0);
		}
		if (labelPos.y() > parentSize.height() - this->height() - 2) {
			labelPos.setY(parentSize.height() - this->height() - 2);
		}
	}
	else {
		if (labelPos.y() + this->height() - 2 < parentSize.height()) {
			labelPos.setY(parentSize.height() - this->height());
		}
		if (labelPos.y() > 0) {
			labelPos.setY(0);
		}
	}
	this->move(labelPos);
	emit moved(-labelPos, this->size() - parentSize + QSize(2, 2));
	//qDebug() << -this->x() << -this->y();
	//qDebug() << "------------------------------------";
}
/**********************************************/
/**********************************************/
/* */
void zoomableLabel::setParentSize(QSize newSize) {
	parentSize = newSize;
}
/**********************************************/
/**********************************************/
/* */
void zoomableLabel::resizeEvent(QResizeEvent *e) {
	emit resized(this->size() - parentSize + QSize(2, 2));
}
/**********************************************/
/**********************************************/
/* */
bool zoomableLabel::event( QEvent *event ) {
	QEvent::Type ev = event->type();
	if ( ev == QEvent::MouseButtonRelease )
	{
		//emit myLabelClicked();
	}
	else if ( ev == QEvent::MouseButtonPress )
	{
		//emit myLabelClicked();
		oldPos = QWidget::mapFromGlobal(QCursor::pos());
	}
	else if ( ev == QEvent::MouseButtonDblClick )
	{
		//emit myLabelClicked();
	}
	else if ( ev == QEvent::MouseMove )
	{
		QMouseEvent* mouse_event = static_cast<QMouseEvent*>(event);
		if (mouse_event->buttons() == Qt::LeftButton) {
			this->moveLabel();
		}
	}
	else if ( ev == QEvent::Wheel )
	{
		if (QApplication::keyboardModifiers() == _modifiers) {
			QWheelEvent* wheel_event = static_cast<QWheelEvent*>(event);
			int zoomDirec = (int)(wheel_event->angleDelta().y()/120);
			this->zoomLabel(zoomDirec);
		}
	}
	return QWidget::event(event);
}
/**********************************************/
/**********************************************/
