#ifndef ELIDEDBUTTON_H
#define ELIDEDBUTTON_H

#include <QPushButton>
#include <QPainter>
#include <QFontMetrics>


/**********************************************/
/**********************************************/
/**********************************************/
class ElidedButton
		: public QPushButton
{
	Q_OBJECT
private:
	bool _elided = false;
	QString _content;

public:
	ElidedButton( QWidget* parent = nullptr )
		: QPushButton( parent ) {

	}

public slots:
	void paintEvent( QPaintEvent *event ) override {
//		QPainter* painter = new QPainter( this );

//		QFontMetrics metrics = QFontMetrics( font() );
//		QString elided  = metrics.elidedText( text(), Qt::ElideRight, width() );

//		painter->drawText( rect(), alignment(), elided );

		QPainter painter( this );
		const QFontMetrics fontMetrics = painter.fontMetrics();
		const int usableWidth = qRound( 0.9 * this->width() );

		const QString elidedText = fontMetrics.elidedText( _content, Qt::ElideRight, usableWidth );
		_elided = ( elidedText != _content );
		QPushButton::setText( elidedText );
		QPushButton::paintEvent( event );
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ELIDEDBUTTON_H
