#include "ProjectTree.h"
#include "Commons/customqwidget.h"

#include <QTreeWidget>
#include <QFile>
#include <QWidget>
#include <QLabel>
#include <QPushButton>
#include <QTextStream>
#include <QMessageBox>
#include <QHBoxLayout>
#include <QCursor>
#include <QSizePolicy>
#include "Commons/theme.h"
/**********************************************/
/**********************************************/
/* */
ProjectTree::ProjectTree(QWidget *parent) : QTreeWidget(parent) {

	// Resize Column(0) to Fit to the Contents
	connect( this, &ProjectTree::itemExpanded,
			 this, &ProjectTree::expandItem );
	connect( this, &ProjectTree::itemCollapsed,
			 this, &ProjectTree::collapseItem );
	//


	acNew = new QAction( QIcon(":/Resource/warning32.ico"), "New", this );
	acNew->setStatusTip( "new sth" );
	connect( acNew, &QAction::triggered,
			 this, &ProjectTree::addNewProject );
	contextMenu = new QMenu;
	contextMenu->addAction( acNew );

	updateProjectTree();

	setContextMenuPolicy(Qt::CustomContextMenu);
	connect( this, &ProjectTree::customContextMenuRequested,
			 this, &ProjectTree::showContextMenu );
}
/**********************************************/
/**********************************************/
/* */
void ProjectTree::updateProjectTree() {
	for (int ip = 0; ip<openedProjects.length(); ip++) {
		QString projectPath = openedProjects.at(0);
		openExistingProject(projectPath);
	}
}
/**********************************************/
/**********************************************/
/* */
void ProjectTree::addNewProject() {
	QTreeWidgetItem* projectItem = new QTreeWidgetItem();
	QString pname = "New Project";
	if (nbrNewProject != 1) {
		pname = pname + " <" + QString::number(nbrNewProject) + ">";
	}
	++nbrNewProject;
	projectItem->setText( 0, pname );
	projectItem->setIcon( 0, _icons->project() );
	projectItem->setText( 1, "" );
	projectItem->setText( 2, "project" );
	projectItem->setText( 3, "" );
	projectItem->setText( 4, "" );
	projectItem->setText( 5, "" );
	addTopLevelItem( projectItem );
}
/**********************************************/
/**********************************************/
/* */
void ProjectTree::openProject( const QString& path ) {
	// Create a document to write XML
	QDomDocument document;
	// Open a file for reading
	QFile file( path );
	if ( !file.open( QIODevice::ReadOnly | QIODevice::Text ) ) {
		return;
	}
	else
	{
		// loading
		if ( !document.setContent( &file ) ) {
			return;
		}
		file.close();
		// Getting root element
		QDomElement dom_element = document.documentElement();
		QString name = dom_element.attributeNode("name").value();
		QString version = dom_element.attributeNode("version").value();
		QString modifTime = dom_element.attributeNode("modificationTime").value();
		QString creaTime = dom_element.attributeNode("creationTime").value();
		QTreeWidgetItem* tmpProject = new QTreeWidgetItem();
		tmpProject->setText( 0, name );
		tmpProject->setText( 1, path );
		tmpProject->setText( 2, "project" );
		tmpProject->setText( 3, creaTime );
		tmpProject->setText( 4, modifTime );
		tmpProject->setText( 5, version );
		addTopLevelItem( tmpProject );
		QDomElement element = dom_element.firstChild().toElement();
		while( !element.isNull() ) {
			if ( element.tagName() == "contents" ) {
				appendFiles( element, tmpProject );
			}
			else if ( element.tagName() == "comment" ) {
				tmpProject->setText( 6, element.text() );
			}
			else if ( element.tagName() == "currentFile" ) {
				tmpProject->setText( 7, element.text() );
			}
			element = element.nextSibling().toElement();
		}
	}
}
/**********************************************/
/**********************************************/
/* */
void ProjectTree::appendFiles( QDomElement elm, QTreeWidgetItem* parent ) {
	QDomNode tmpNode = elm.firstChild();
	while( !tmpNode.isNull() )// Tant que le nœud n'est pas vide.
	{
		if ( tmpNode.toElement().tagName() == "folder" )
		{
			QTreeWidgetItem* tmpFolder= new QTreeWidgetItem();
			tmpFolder->setText( 0, tmpNode.toElement().attribute("name") );
			tmpFolder->setText( 1, "" );
			tmpFolder->setText( 2, "folder" );
			tmpFolder->setText( 3, tmpNode.toElement().attribute("creationTime") );
			tmpFolder->setText( 4, tmpNode.toElement().attribute("modificationTime") );
			parent->addChild( tmpFolder );
			appendFiles( tmpNode.toElement(), tmpFolder );
		}
		else
		{
			QTreeWidgetItem* tmpFile= new QTreeWidgetItem();
			tmpFile->setText( 0, tmpNode.toElement().attribute("name") );
			tmpFile->setText( 1, tmpNode.toElement().attribute("path") );
			tmpFile->setText( 2, tmpNode.toElement().tagName() );
			tmpFile->setText( 3, tmpNode.toElement().attribute("creationTime") );
			tmpFile->setText( 4, tmpNode.toElement().attribute("modificationTime") );
			parent->addChild( tmpFile );
		}
		tmpNode = tmpNode.nextSibling();
	}
}
/**********************************************/
/**********************************************/
/* */
void ProjectTree::openExistingProject(QString projectPath) {
	bool existingProject = false;
	for (int ip = 0; ip<openedProjects.length(); ip++) {
		if (openedProjects.at(ip) == projectPath) {
			existingProject = true;
		}
	}
	if (!existingProject) {
		openedProjects.append(projectPath);
		QStringList filesnames;
		QFile file(projectPath);
		file.open(QIODevice::ReadOnly);
		QTextStream in(&file);
		bool searchFiles = false;
		bool goSearch = true;
		while (!in.atEnd() && goSearch) {
			QString line = in.readLine();
			if (line.contains("#endFiles")) {
				goSearch = false;
				searchFiles = false;
			}
			if (searchFiles && line.contains("#")) {
				int lineCH = line.length() - line.indexOf("#") - 1;
				filesnames.append(line.right(lineCH));
			}
			if (line.contains("#beginFiles")) {
				searchFiles = true;
			}
		}
		file.close();

		QString projectName = projectPath.split("/").at(projectPath.split("/").length()-1);
		projectName.remove(".gpj");
		int itproj = 1;
		for ( int ic = 0; ic < this->topLevelItemCount(); ++ic ) {
			QTreeWidgetItem *child = this->topLevelItem(ic);
			QString exiproj = child->text(0);
			if (exiproj.contains(projectName)) {
				if (exiproj == projectName && itproj == 1) {
					itproj = 2;
				}
				else {
					exiproj.remove(projectName+" <");
					exiproj.remove(">");
					if (exiproj.toInt()+1>itproj) {
						itproj = itproj+1;
					}
				}
			}
		}
		QTreeWidgetItem *projectItem = new QTreeWidgetItem(this);
		if (itproj == 1) {
			projectItem->setText(0, projectName);
		}
		else {
			projectItem->setText(0, projectName+" <"+QString::number(itproj)+">");
		}
		projectItem->setText(1, projectPath);
		projectItem->setText(2, "project");
		//projectItem->setText(2, "project");
		projectItem->setIcon(0, QPixmap("icons/gti_project.png"));

		for (int i=0; i<filesnames.length(); i++) {
			QString filename = filesnames.at(i);
			QString path;
			QString itype = filename.split("#type:").at(1);
			QString strtmp = filename.split("#type:").at(0);
			QString filepath = strtmp.split("#fullpath:").at(0);
			QString filefullpath = strtmp.split("#fullpath:").at(1);

			QStringList filepathdec = filepath.split("/");
			//QMessageBox::information(this, "vgftuf", filepathdec.at(filepathdec.length()-1));
			QString gggf = "";
			for (int j=0; j<filepathdec.length()-1; j++) {
				gggf = gggf + " - " + filepathdec.at(j);
			}
			//QMessageBox::information(this, "vgftuf", gggf);
			QTreeWidgetItem *itemparent = projectItem;
			for (int j=0; j<filepathdec.length()-1; j++) {
				bool childFound = false;
				for (int ic = 0; ic < itemparent->childCount(); ++ic) {
					QTreeWidgetItem *child = itemparent->child(ic);
					if (child->text(0) == filepathdec.at(j)) {
						itemparent = child;
						childFound = true;
					}
				}
				if (!childFound) {
					QTreeWidgetItem *folder = new QTreeWidgetItem(itemparent);
					folder->setText(0, filepathdec.at(j));
					folder->setText(1, "");
					folder->setText(2, "folder");
					folder->setIcon(0, QPixmap("icons/gti_folder.png"));
					itemparent = folder;
				}
			}
			QTreeWidgetItem *item = new QTreeWidgetItem(itemparent);
			item->setText(0, filepathdec.at(filepathdec.length()-1));
			item->setText(1, filefullpath);
			item->setText(2, itype);
			if (itype == "picture") {
				item->setIcon(0, QPixmap("icons/gti_picture.png"));
			}
			else if (itype == "graph") {
				item->setIcon(0, QPixmap("icons/gti_graph.png"));
			}
			else if (itype == "data") {
				item->setIcon(0, QPixmap("icons/gti_data.png"));
			}
		}
	}
	else {
		QMessageBox::information(this, "Information", "Project already opened");
	}
}
/**********************************************/
/**********************************************/
/* */
void ProjectTree::addFileItem() {
}
/**********************************************/
/**********************************************/
/* */
void ProjectTree::addFolderItem() {
}
/**********************************************/
/**********************************************/
/* */
void ProjectTree::deleteItem() {
	this->removeItemWidget(this->currentItem(), 0);
	QTreeWidgetItem *item = this->currentItem();
	if (!item->parent()) {
		if (this->topLevelItemCount() != 1) {
			bool stopiop = false;
			int iop = 0;
			while(iop<openedProjects.length() && !stopiop) {
				if (openedProjects.at(iop) == item->text(1)) {
					openedProjects.removeAt(iop);
					stopiop = true;
				}
				iop++;
			}
			delete item;
		}
	}
	else {
		item->parent()->removeChild(item);
	}
}
/**********************************************/
/**********************************************/
/* */
void ProjectTree::setIcons( Icons* icons ) {
	_icons = icons;
	// Actions
	acNew->setIcon( _icons->newi() );
}
/**********************************************/
/**********************************************/
/* */
void ProjectTree::updateIcons( QTreeWidgetItem* item ) {
	if ( item == nullptr ) {
		for( int it = 0; it < topLevelItemCount(); ++it ) {
			topLevelItem( it )->setIcon( 0, _icons->project() );
			updateIcons( topLevelItem( it ) );
		}
	}
	else {
		for( int it = 0; it < item->childCount(); ++it ) {
			QTreeWidgetItem* tmpItem = item->child( it );
			if ( tmpItem->text( 2 ) == "folder" ) {
				tmpItem->setIcon( 0, _icons->folder() );
			}
			else if ( tmpItem->text( 2 ) == "rawdata") {
				tmpItem->setIcon( 0, _icons->rawData() );
			}
			else if ( tmpItem->text( 2 ) == "data") {
				tmpItem->setIcon( 0, _icons->data() );
			}
			else if ( tmpItem->text( 2 ) == "graph") {
				tmpItem->setIcon( 0, _icons->graph() );
			}
			else if ( tmpItem->text( 2 ) == "picture") {
				tmpItem->setIcon( 0, _icons->picture() );
			}
			updateIcons( tmpItem );
		}
	}
}
/**********************************************/
/**********************************************/
/* */
void ProjectTree::showContextMenu( const QPoint & pos ) {
	QTreeWidgetItem *nd = this->itemAt( pos );
	if ( nd != nullptr )
	{
		qDebug() << pos << nd->text(0);

		QPoint pt( pos );
		contextMenu->exec( this->mapToGlobal( pos ) );
	}
}
/**********************************************/
/**********************************************/
/* */
void ProjectTree::showFile( const QString& path ) {
	QStringList paths = path.split( "/" );
	if ( topLevelItemCount() > 0 )
	{
		QTreeWidgetItem* toShow = nullptr;
		QTreeWidgetItem* tmp = topLevelItem( 0 );
		QString projName = paths.takeFirst();
		bool found = false;
		for ( int it = 0; it < topLevelItemCount(); ++it ) {
			if ( tmp->text( 0 ) == projName ) {
				found = true;
				toShow = tmp;
				break;
			}
			tmp = topLevelItem( it );
		}
		if ( found ) {
			for ( QString elm : paths ) {
				found = false;
				for ( int it = 0; it < toShow->childCount(); ++it ) {
					tmp = toShow->child( it );
					if ( tmp->text( 0 ) == elm ) {
						found = true;
						toShow = tmp;
						break;
					}
				}
				if ( !found ) return;
			}
			setCurrentItem( toShow );
			emit itemClicked( toShow, 0 );
		}
	}

}
/**********************************************/
/**********************************************/
/* */
void ProjectTree::updateLang( Lang* lang ) {
	// Actions
	acNew->setText( lang->get("new") + "..." );
}
/**********************************************/
/**********************************************/
