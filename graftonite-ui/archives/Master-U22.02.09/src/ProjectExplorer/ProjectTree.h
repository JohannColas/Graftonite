#ifndef PROJECTTREE_H
#define PROJECTTREE_H

#include <QTreeWidget>
#include <QWidget>
#include <QPushButton>
#include <QStringList>
#include <QtXml>
#include <QVariant>
#include <QMenu>
#include <QAction>
#include <QResizeEvent>
#include <QHeaderView>
#include "Commons/customqwidget.h"
#include "Commons/theme.h"
#include "Commons/lang.h"

#include <QDebug>

/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
namespace TreeItemType
{
	static const int NONE = 1001;
	static const int PROJECT = 1002;
	static const int FOLDER = 1003;
	static const int RAWDATA = 1004;
	static const int DATA = 1005;
	static const int GRAPH = 1006;
	static const int PICTURE = 1007;
	static const int MAP = 1008;
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class ProjectTree : public QTreeWidget
{
    Q_OBJECT
private:
	Icons* _icons;

public:
	ProjectTree( QWidget *parent = nullptr );
	int nbrNewProject = 1;
	QMenu* contextMenu= nullptr;
	QAction* acNew = nullptr;

	QStringList openedProjects;
    void updateProjectTree();
    void openExistingProject(QString projectPath);

public slots:
    void addNewProject();
	void openProject( const QString& path );
	void appendFiles( QDomElement elm, QTreeWidgetItem* parent );
    void addFileItem();
    void addFolderItem();
	void deleteItem();
	void setIcons( Icons* icons );
	void updateIcons( QTreeWidgetItem *item = nullptr );
	void updateLang( Lang* lang);
	void showContextMenu( const QPoint & pos );
	void showFile( const QString& path );
//	void resizeEvent( QResizeEvent* event ) override {
//		resizeColumnToContents(0);
//		qDebug() << width() <<
//					this->columnWidth(0) <<
//					this->header()->width();
//		QTreeWidget::resizeEvent( event );
//	}
	void collapseItem( const QTreeWidgetItem *item ) {
		resizeColumnToContents(0);
		QTreeWidget::collapseItem( item );
	}
	void expandItem( const QTreeWidgetItem *item ) {
		resizeColumnToContents(0);
		QTreeWidget::expandItem( item );
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PROJECTTREE_H
