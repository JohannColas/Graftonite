#ifndef PROJECTEXPLORER_H
#define PROJECTEXPLORER_H

#include <QWidget>
#include <QLabel>
#include <QList>
#include <QPushButton>

#include "ProjectTree.h"
#include "Commons/selectedelement.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class ProjectExplorer : public QWidget {
    Q_OBJECT

public:
    ProjectExplorer(QWidget *parent);
    QLabel *projectLabel;
    QList<QString> *openedProjects;
    SelectedElement selectedElement;
    ProjectTree *projectsTree;
    QPushButton *newProject;
    QPushButton *openProject;
    QTreeWidgetItem *getSelectedElement();

signals:
    void selectedItemHasChanged();
    void treeHasBeenModified();
    void selectItem();
    void itemSelectionChanged();

public slots:
    void clicSelected();
    void openExistingProject();

};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PROJECTEXPLORER_H
