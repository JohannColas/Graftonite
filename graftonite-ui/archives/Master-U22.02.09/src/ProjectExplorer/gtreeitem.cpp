#include "gtreeitem.h"

gTreeItem::gTreeItem(QString name, QString gtiType) {
    this->setText(name);
    this->setType(gtiType);
    if (type == "project") {
        this->setIcon(QIcon("gti_project.png"));
    }
    else if (type == "folder") {
        this->setIcon(QIcon("gti_folder.png"));
    }
    else if (type == "data") {
        this->setIcon(QIcon("gti_data.png"));
    }
    else if (type == "graph") {
        this->setIcon(QIcon("gti_graph.png"));
    }
    else if (type == "picture") {
        this->setIcon(QIcon("gti_picture.png"));
    }

}

void gTreeItem::setType(QString newtype) {
    type = newtype;
}

QString gTreeItem::getType() {
    return type;
}
