#ifndef GTREEITEM_H
#define GTREEITEM_H

#include <QStandardItem>

class gTreeItem : public QStandardItem {
    Q_OBJECT

public:
    gTreeItem(QString name, QString gtiType);
    QString type;
    void setType(QString newtype);
    QString getType();
};

#endif // GTREEITEM_H
