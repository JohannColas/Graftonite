#include "projectexplorer.h"
#include "ProjectTree.h"


#include <QLabel>
#include <QPushButton>
#include <QTreeWidget>
#include <QHeaderView>
#include <QStandardItem>
#include <QFileSystemModel>
#include <QVBoxLayout>
#include <QMessageBox>
#include <QFileDialog>
/**********************************************/
/**********************************************/
/* */
ProjectExplorer::ProjectExplorer(QWidget *parent) {
    projectLabel = new QLabel("Project Explorer");
    //QPushButton *projectLabel = new QPushButton("Project Explorer");
    newProject = new QPushButton();
    openProject = new QPushButton();
    newProject->setFixedWidth(25);
    openProject->setFixedWidth(25);

    projectsTree = new ProjectTree(parent);
    projectsTree->header()->hide();

    QWidget *projectHeader = new QWidget();
    QHBoxLayout *projectHeaderLay = new QHBoxLayout();
//    projectHeaderLay->setMargin(0);
    projectHeaderLay->setContentsMargins(1, 1, 1, 1);
    projectHeaderLay->addWidget(projectLabel);
    projectHeaderLay->addWidget(newProject);
    projectHeaderLay->addWidget(openProject);
    newProject->setIcon(QPixmap("icons/gti_add.png"));
    openProject->setIcon(QPixmap("icons/gti_open.png"));
    projectHeader->setLayout(projectHeaderLay);

    QVBoxLayout *projectLay = new QVBoxLayout();
//    projectLay->setMargin(0);
    projectLay->setContentsMargins(1, 1, 1, 1);
    projectLay->addWidget(projectHeader);
    projectLay->addWidget(projectsTree);
    this->setLayout(projectLay);

    //connect(projectLabel, SIGNAL(clicked()), this, SLOT(clicSelected()));
    //connect(projectLabel, SIGNAL(clicked()), this, SIGNAL(selectedItemHasChanged()));
    connect(projectsTree, SIGNAL(currentItemChanged(QTreeWidgetItem*, QTreeWidgetItem*)), this, SLOT(clicSelected()));
    connect(newProject, SIGNAL(clicked()), projectsTree, SLOT(addNewProject()));
    connect(openProject, SIGNAL(clicked()), this, SLOT(openExistingProject()));

}
/**********************************************/
/**********************************************/
/* */
void ProjectExplorer::clicSelected() {
    QTreeWidgetItem *item = projectsTree->currentItem();

    selectedElement.setType(item->text(2));
    //QString itype = item->text(1);
    //QString path;
    //for (; item; item = item->parent())
    //   path.prepend(QStringLiteral("/%1").arg(item->text(0)));
    selectedElement.setName(item->text(0));
    selectedElement.setPath(item->text(1));
    //QMessageBox::information(this, "Elément sélectionné", path + " | the type is : " + itype);
    emit selectedItemHasChanged();
}
/**********************************************/
/**********************************************/
/* */
QTreeWidgetItem *ProjectExplorer::getSelectedElement() {
    return projectsTree->currentItem();
}
/**********************************************/
/**********************************************/
/* */
void ProjectExplorer::openExistingProject() {
    //QString patth = QFileDialog::getSaveFileName(this, tr("Save SVG"), patth, tr("SVG files (*.svg)"));
    QString projectPath = QFileDialog::getOpenFileName(this, tr("Open Project"), "/home/johanncolas/Projects/Graftonite", tr("Graftonite Project (*.gpj)"));
    if (projectPath != "") {
        projectsTree->openExistingProject(projectPath);
    }
}
/**********************************************/
/**********************************************/






