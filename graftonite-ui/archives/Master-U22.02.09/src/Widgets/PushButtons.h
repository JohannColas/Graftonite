#ifndef PUSHBUTTONS_H
#define PUSHBUTTONS_H

#include <QObject>
#include <QPushButton>
#include <QToolButton>
#include <QLabel>
#include <QPainter>
#include <QFontMetrics>
#include <QStyleOptionButton>
#include <QStylePainter>
#include <QDebug>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class LeftButton
		: public QPushButton
{
	Q_OBJECT
//	Q_PROPERTY( QString text READ text WRITE setText )
//	Q_PROPERTY( bool isElided READ isElided )

private:
	bool _elided = false;
	QString _content;
//	Qt::TextElideMode _elide_mode;

public:
	virtual ~LeftButton () {

	}
	LeftButton ( QWidget* parent = nullptr )
		: QPushButton( parent )
	{
	}
//	LeftButton( const QString &text, QWidget *parent )
//		: QPushButton(parent), _elided(false), _content(text)
//	{
//		this->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Preferred );
//	}

	void setText( const QString& text ) {
		_content = text;
		this->update();
	}
	const QString& text() const {
		return _content;
	}
	bool isElided() const {
		return _elided;
	}

protected:
	void paintEvent( QPaintEvent *event ) override {

		QPainter painter( this );
		const QFontMetrics fontMetrics = painter.fontMetrics();
		const int usableWidth = qRound( 0.85 * this->width() - this->iconSize().width() );

		const QString elidedText = fontMetrics.elidedText( _content, Qt::ElideRight, usableWidth );
		_elided = ( elidedText != _content );
		QPushButton::setText( elidedText );
		QPushButton::paintEvent( event );
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class SettingsBarButton
		: public QPushButton
{
	Q_OBJECT
public:
	virtual ~SettingsBarButton ()
	{

	}
	SettingsBarButton ( QWidget* parent = nullptr )
		: QPushButton( parent )
	{
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class SettingsBarLabel
		: public QLabel
{
	Q_OBJECT
public:
	virtual ~SettingsBarLabel ()
	{

	}
	SettingsBarLabel ( QWidget* parent = nullptr )
		: QLabel( parent )
	{
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PUSHBUTTONS_H
