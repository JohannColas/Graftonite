#ifndef CLICKABLELABEL_H
#define CLICKABLELABEL_H

#include <QObject>
#include <QLabel>
#include <QMouseEvent>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class ClickableLabel
		: public QLabel
{
	Q_OBJECT
public:
	virtual ~ClickableLabel()
	{

	}
	ClickableLabel( QWidget *parent = nullptr )
		: QLabel( parent )
	{

	}

protected:
	void mousePressEvent( QMouseEvent *event ) override
	{
		if ( event->button() == Qt::LeftButton )
		{
			emit clicked();
		}
	}

signals:
	void clicked();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // CLICKABLELABEL_H
