#include "DataFrame.h"

#include <QStandardItemModel>
#include <QTableView>
#include <QMessageBox>
/**********************************************/
/**********************************************/
/* */
DataFrame::DataFrame(QWidget *parent)
	: QSplitter(parent)
{

//	lay_main->setMargin( 0 );
	lay_main->addWidget( lab_title );
	lab_title->setText( "Title Data" );
	lay_main->addWidget( table_Data );
	wid_main->setLayout( lay_main );


	table_Data->setRowCount( 6 );
	table_Data->setColumnCount( 4 );
	table_Data->setItem(3, 0, new QTableWidgetItem("Un") );
	table_Data->setItem( 3, 2, new QTableWidgetItem("Zéro !!!") );

	addWidget( wid_main );
	addWidget( wid_toolBar );
	setSizes( {this->width() - 200, 200} );

	hide();
}
/**********************************************/
/**********************************************/
/* */
DataFrame::~DataFrame() {
}
/**********************************************/
/**********************************************/
/* */
void DataFrame::updateIcons( Icons* icons ) {
Q_UNUSED(icons)
}
/**********************************************/
/**********************************************/
/* */
void DataFrame::updateLang( Lang* lang ) {
Q_UNUSED(lang)
}
/**********************************************/
/**********************************************/
