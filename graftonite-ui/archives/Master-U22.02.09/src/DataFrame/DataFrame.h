#ifndef DATAFRAME_H
#define DATAFRAME_H

#include <QSplitter>
#include <QWidget>
#include <QLabel>
#include <QTableWidget>
#include <QVBoxLayout>
#include "Commons/theme.h"
#include "Commons/lang.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class DataFrame : public QSplitter
{
	Q_OBJECT
private:
	QWidget* wid_main = new QWidget;
	QVBoxLayout* lay_main = new QVBoxLayout;
	QLabel* lab_title = new QLabel;
	QTableWidget* table_Data = new QTableWidget;
	QWidget* wid_toolBar = new QWidget;

public:
	explicit DataFrame( QWidget *parent = nullptr );
	~DataFrame();
	void updateIcons( Icons* icons );
	void updateLang( Lang* lang );

private slots:

};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // DATAFRAME_H
