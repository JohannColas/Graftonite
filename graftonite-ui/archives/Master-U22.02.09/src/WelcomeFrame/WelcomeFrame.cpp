#include "WelcomeFrame.h"
/**********************************************/
/**********************************************/
/* */
WelcomeFrame::~WelcomeFrame() {
}
/**********************************************/
/**********************************************/
/* */
WelcomeFrame::WelcomeFrame(QWidget *parent)
	: QWidget(parent)
{
	setLayout( lay_main );

	// Title Bar
	lab_projects->setText( "Projects" );
	but_new->setText( "New" );
	but_open->setText( "Open" );
	lay_title->addWidget( lab_projects );
	lay_title->addWidget( but_new );
	lay_title->addWidget( but_open );
	lay_title->addItem( new QSpacerItem( 0, 0, QSizePolicy::Expanding ) );
	lay_main->addLayout( lay_title, 0, 0, 1, 3 );
	// Recent Projects
	lab_recentProjects->setText( "Recent Projects" );
	list_recentProjects->setModel( mod_recentProjects );
	lay_main->addWidget( lab_recentProjects, 1, 0 );
	lay_main->addWidget( list_recentProjects, 2, 0 );
	// Saved Projects
	lab_savedProjects->setText( "Saved Projects" );
	list_savedProjects->setModel( mod_savedProjects );
	lay_main->addWidget( lab_savedProjects, 1, 1 );
	lay_main->addWidget( list_savedProjects, 2, 1 );
	// Sessions
	lab_sessions->setText( "Sessions" );
//	list_sessions->setModel( savedProjects );
	lay_main->addWidget( lab_sessions, 1, 2 );
	lay_main->addWidget( list_sessions, 2, 2 );

	hide();
}
/**********************************************/
/**********************************************/
/* */
void WelcomeFrame::updateIcons( Icons* icons ) {
	but_new->setIcon( icons->newi() );
	but_open->setIcon( icons->open() );
	mod_recentProjects->updateIcons( icons );
	mod_savedProjects->updateIcons( icons );
}
/**********************************************/
/**********************************************/
/* */
void WelcomeFrame::updateLang( Lang* lang ) {
Q_UNUSED(lang)
}
/**********************************************/
/**********************************************/
