#ifndef PICTUREFRAME_H
#define PICTUREFRAME_H

#include <QWidget>
#include "Commons/theme.h"
#include "Commons/lang.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class PictureFrame : public QWidget
{
	Q_OBJECT

public:
	explicit PictureFrame(QWidget *parent = nullptr);
	~PictureFrame();
	void updateIcons( Icons* icons );
	void updateLang( Lang* lang );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PICTUREFRAME_H
