#ifndef GRAFTONITEWINDOW_H
#define GRAFTONITEWINDOW_H

#include <QWidget>
#include <QVBoxLayout>
#include <QSplitter>
#include "Widgets/PushButtons.h"
#include "ProjectExplorer/ProjectTree.h"
#include "WelcomeFrame/WelcomeFrame.h"
#include "RawDataFrame/RawDataFrame.h"
#include "DataFrame/DataFrame.h"
#include "GraphFrame/GraphFrame.h"
#include "GraphFrame/GraphWidget.h"
#include "PictureFrame/PictureFrame.h"
#include "SettingsFrame/SettingsFrame.h"
#include "Commons/theme.h"
#include "Commons/lang.h"

//enum GFTWidget : char
//{
//	WELCOME = 'w',
//	SETTINGS = 's',
//	RAWDATA = 'r',
//	DATA = 'd',
//	GRAPH = 'g',
//	PICTURE = 'p'
//};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class GraftoniteWindow
		: public QWidget
{
	Q_OBJECT

public:
	explicit GraftoniteWindow( QWidget *parent = nullptr );
	~GraftoniteWindow();

public slots:
	void updateIcons();
	void updateLang();

private slots:
	void showWelcomeWidget();
	void showSettingsWidget();
	void showRawDataWidget();
	void showDataWidget();
	void showGraphWidget();
	void showPictureWidget();
	void removeCurrentWidget();
	void TreeFileChanged( QTreeWidgetItem* item , int column );

private:
	QVBoxLayout* mainLayout = new QVBoxLayout;
	QSplitter* splitter = new QSplitter;
	// Sidebar Widgets
	QWidget* sideBar = new QWidget;
	QVBoxLayout* sideLayout = new QVBoxLayout;
	LeftButton* but_welcome = new LeftButton;
	LeftButton* but_projectsExplorer = new LeftButton;
	ProjectTree* tre_projectsExplorer = new ProjectTree;
	LeftButton* but_settings = new LeftButton;
	// Main Widgets
	QWidget* mainFrame = new QWidget;
	QGridLayout* mainFrameLayout = new QGridLayout;
//	GFTWidget current_Widget;
	QWidget* currentWidget = nullptr;
	WelcomeFrame *wid_Welcome = new WelcomeFrame;
	SettingsFrame *wid_Settings = new SettingsFrame;
	RawDataFrame *wid_RawData = new RawDataFrame;
	DataFrame *wid_Data = new DataFrame;
//	GraphFrame *wid_Graph = nullptr;
	GraphFrame* wid_Graph = new GraphFrame;
	PictureFrame *wid_Picture = new PictureFrame;
	//
	Theme* _theme = new Theme;
	Icons* _icons = new Icons;
	Lang* _lang = new Lang;

};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // GRAFTONITEWINDOW_H
