#include "GraftoniteWindow.h"

#include <QDebug>
/**********************************************/
/**********************************************/
/* */
GraftoniteWindow::~GraftoniteWindow() {
	delete mainLayout;
	delete splitter;
	delete _theme;
	delete _icons;
	delete _lang;
}
/**********************************************/
/**********************************************/
/* */
GraftoniteWindow::GraftoniteWindow(QWidget *parent)
	: QWidget( parent )
{
	connect( but_welcome, &LeftButton::clicked,
			 this, &GraftoniteWindow::showWelcomeWidget );
	connect( but_projectsExplorer, &LeftButton::clicked,
			 this, &GraftoniteWindow::removeCurrentWidget );
	tre_projectsExplorer->setSizePolicy( QSizePolicy::Preferred,
										 QSizePolicy::Expanding );
	tre_projectsExplorer->header()->hide();
	tre_projectsExplorer->setSelectionMode(
				QAbstractItemView::ExtendedSelection );
	tre_projectsExplorer->openProject(
	            "/home/colas/Bureau/Graftonite-UI-OLD/src/projectExample.gpj" );
	tre_projectsExplorer->openProject(
	            "/home/colas/Bureau/Graftonite-UI-OLD/src/projectExample.gpj" );
	connect( tre_projectsExplorer, &ProjectTree::itemClicked,
			 this, &GraftoniteWindow::TreeFileChanged );
	connect( but_settings, &LeftButton::clicked,
			 this, &GraftoniteWindow::showSettingsWidget );

	sideLayout->addWidget( but_welcome );
	sideLayout->addWidget( but_projectsExplorer );
	sideLayout->addWidget( tre_projectsExplorer );
	sideLayout->addWidget( but_settings );
	sideBar->setLayout( sideLayout );

	mainFrameLayout->addWidget( wid_Welcome, 0, 0 );
	mainFrameLayout->addWidget( wid_RawData, 0, 0 );
	mainFrameLayout->addWidget( wid_Data, 0, 0 );
	mainFrameLayout->addWidget( wid_Graph, 0, 0 );
	mainFrameLayout->addWidget( wid_Picture, 0, 0 );
	mainFrameLayout->addWidget( wid_Settings, 0, 0 );
	mainFrame->setLayout( mainFrameLayout );

	mainFrame->setSizePolicy( QSizePolicy::Expanding,
							  QSizePolicy::Preferred );

	splitter->setSizes( {200, this->width() - 200} );
	splitter->addWidget( sideBar );
	splitter->addWidget( mainFrame );
	splitter->setCollapsible( 1, false );

//	mainLayout->setMargin( 0 );
	mainLayout->addWidget( splitter );
	setLayout( mainLayout );

	showWelcomeWidget();

	// -----------------------
	// -----------------------
	// Themes
	_theme->apply();
	updateIcons();
	updateLang();
	// -----------------------
	// -----------------------

	tre_projectsExplorer->showFile( "Project/graph4" );

	setMinimumSize( 800, 500 );
	resize( 1000, 625 );
}
/**********************************************/
/**********************************************/
/* */
void GraftoniteWindow::showWelcomeWidget() {
	removeCurrentWidget();
	currentWidget = wid_Welcome;
	wid_Welcome->show();
//	current_Widget = GFTWidget::WELCOME;
}
/**********************************************/
/**********************************************/
/* */
void GraftoniteWindow::showSettingsWidget() {
	removeCurrentWidget();
	currentWidget = wid_Settings;
	wid_Settings->show();
//	current_Widget = GFTWidget::SETTINGS;
}
/**********************************************/
/**********************************************/
/* */
void GraftoniteWindow::showRawDataWidget() {
	removeCurrentWidget();
	currentWidget = wid_RawData;
	wid_RawData->show();
//	current_Widget = GFTWidget::RAWDATA;
}
/**********************************************/
/**********************************************/
/* */
void GraftoniteWindow::showDataWidget() {
	removeCurrentWidget();
	currentWidget = wid_Data;
	wid_Data->show();
//	current_Widget = GFTWidget::DATA;
}
/**********************************************/
/**********************************************/
/* */
void GraftoniteWindow::showGraphWidget() {
	removeCurrentWidget();
	currentWidget = wid_Graph;
	wid_Graph->show();
//	current_Widget = GFTWidget::GRAPH;
}
/**********************************************/
/**********************************************/
/* */
void GraftoniteWindow::showPictureWidget() {
	removeCurrentWidget();
	currentWidget = wid_Picture;
	wid_Picture->show();
//	current_Widget = GFTWidget::PICTURE;
}
/**********************************************/
/**********************************************/
/* */
void GraftoniteWindow::removeCurrentWidget() {
	if ( currentWidget != nullptr )
		currentWidget->hide();
}
/**********************************************/
/**********************************************/
/* */
void GraftoniteWindow::TreeFileChanged( QTreeWidgetItem* item, int column ) {
	Q_UNUSED( column );
	QString type = item->text( 2 );
	qDebug() << "----------------";
	if ( type == "project" ) {
		removeCurrentWidget();
	}
	else if ( type == "folder" ) {
		removeCurrentWidget();
	}
	else if ( type == "rawdata") {
		showRawDataWidget();
	}
	else if ( type == "data") {
		showDataWidget();
	}
	else if ( type == "graph") {
		showGraphWidget();
	}
	else if ( type == "picture") {
		showPictureWidget();
	}
	QTreeWidgetItem* itemtmp = item->parent();
	QString elementsSelectionnes = item->text( 0 );
	while ( itemtmp != nullptr )
	{
		elementsSelectionnes = itemtmp->text( 0 ) + "/" + elementsSelectionnes;
		itemtmp = itemtmp->parent();
	}
	qDebug() << elementsSelectionnes;
}
/**********************************************/
/**********************************************/
/* */
void GraftoniteWindow::updateIcons() {
	_icons->update();
	setWindowIcon( _icons->graftonite() );
	but_welcome->setIcon( _icons->welcome() );
	but_projectsExplorer->setIcon( _icons->projectsExplorer() );
	tre_projectsExplorer->setIcons( _icons );
	tre_projectsExplorer->updateIcons();
	but_settings->setIcon( _icons->settings() );
	wid_Welcome->updateIcons( _icons );
//	wid_Welcome->setIcons( _icons );
//	wid_Welcome->updateIcons();
	wid_Graph->updateIcons( _icons );
}
/**********************************************/
/**********************************************/
/* */
void GraftoniteWindow::updateLang() {
	_lang->update();
	but_welcome->setText( _lang->get("welcome") );
	but_projectsExplorer->setText( _lang->get("projectExplorer") );
	tre_projectsExplorer->updateLang( _lang );
	but_settings->setText( _lang->get("settings") );
	wid_Welcome->updateLang( _lang );
	wid_Settings->updateLang( _lang );
	wid_Data->updateLang( _lang );
	wid_RawData->updateLang( _lang );
	wid_Graph->updateLang( _lang );
	wid_Picture->updateLang( _lang );
}
/**********************************************/
/**********************************************/
