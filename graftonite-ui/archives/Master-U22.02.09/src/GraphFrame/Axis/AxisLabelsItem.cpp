#include "AxisLabelsItem.h"

#include <QFont>
#include <QTextStream>
#include "../Misc/GraphTextItem.h"

#include <cmath>
#include <iostream>
using namespace std;
/**********************************************/
/**********************************************/
/* */
AxisLabelsItem::~AxisLabelsItem() {

}
/**********************************************/
/**********************************************/
/* */
AxisLabelsItem::AxisLabelsItem( QGraphicsItem* parent )
	: QGraphicsItemGroup( parent )
{

//	setFlag(QGraphicsItem::ItemIsSelectable);
}
/**********************************************/
/**********************************************/
/* */
void AxisLabelsItem::update( AxisSettings* settings, const QRectF tickRect )
{
	double labelsPos2 =  tickRect.y() + tickRect.height() + settings->getLblOffset().y();
	if ( settings->getLblPos() != Axis::NONE ) {
		if ( settings->getLblPos() == Axis::TOP )
			labelsPos2 = tickRect.y() - settings->getLblOffset().y();
		else if ( settings->getLblPos() == Axis::LEFT )
			labelsPos2 = tickRect.x() - settings->getLblOffset().x();
		else if ( settings->getLblPos() == Axis::RIGHT )
			labelsPos2 = tickRect.x() + tickRect.width() + settings->getLblOffset().x();

		if ( settings->getTkmaPos().size() != 0 )
		{
			QString prefix = settings->getLblPrefix();
			QString suffix = settings->getLblSuffix();

			for ( int it = 0; it < settings->getTkmaPos().size(); it++ )
			{
				GraphTextItem *newLabel = new GraphTextItem();
				newLabel->setDefaultTextColor( settings->getLblFtColor() );
				newLabel->setFont( settings->getLblFont() );

				QString labelTMP = settings->getLabels().at(it);
				double lblNB = labelTMP.toDouble();
				short nbPrec = settings->getLblNumberPrecision();
				short nbFrmtOpt = settings->getLblNumberFormatOption();

				if ( settings->getLblNumberFormat() == Axis::DECIMAL )
						labelTMP = QString("%1").arg( lblNB, 0, 'f', nbPrec );
				else if ( settings->getLblNumberFormat() == Axis::SCIENTIFIC ) {
						if ( lblNB == 0 )
							labelTMP = "0";
						else
						{
							labelTMP = "";
							if ( lblNB < 0 )
							{
								labelTMP += "-";
								lblNB *= -1;
							}
							double pownb = floor( log10( lblNB ) );
							double nb = lblNB * pow( 10, -pownb );
							QString newNB = QString("%1").arg( nb, 0, 'f', nbPrec );
							if( newNB == "10" )
							{
								newNB = "1";
								pownb += 1;
							}

							switch ( nbFrmtOpt )
							{
								case 0:
									labelTMP += newNB + "e";
								break;
								case 1:
									labelTMP += newNB + "E";
								break;
								case 2:
									if( newNB != "1" )
										labelTMP += newNB + "×";
									labelTMP += "10<sup>";
								break;
								case 3:
									labelTMP += newNB + "D";
								break;
							}
							labelTMP += QString("%1").arg( pownb, 0, 'f', 0);
							if ( nbFrmtOpt == 2 )
								labelTMP += "</sup>";
						}
				}
				else if ( settings->getLblNumberFormat() == Axis::POWEROF ) {
						if ( lblNB == 0 )
							labelTMP = "0";
						else
						{
							double pow = 0;
							switch ( nbFrmtOpt )
							{
								case 0:
									pow = log(lblNB)/log(10);
									labelTMP = QString("10<sup>%1</sup>").arg( pow, 0, 'f', nbPrec );
								break;
								case 1:
									pow = log(lblNB)/log(2);
									labelTMP = QString("2<sup>%1</sup>").arg( pow, 0, 'f', nbPrec );
								break;
								case 2:
									pow = log(lblNB);
									labelTMP = QString("e<sup>%1</sup>").arg( pow, 0, 'f', nbPrec );
								break;
							}
						}
				}
				else if ( settings->getLblNumberFormat() == Axis::MULTIPLE ) {
						if ( lblNB == 0 )
							labelTMP = "0";
						else
						{
							double mult = 0;
							switch ( nbFrmtOpt )
							{
								case 0:
									mult = lblNB/GraphMath::PI;
									labelTMP = QString("%1π").arg( mult, 0, 'f', nbPrec );
								break;
								case 1:
									mult = lblNB/GraphMath::e;
									labelTMP = QString("%1e").arg( mult, 0, 'f', nbPrec );
								break;
							}
						}
				}
				else if ( settings->getLblNumberFormat() == Axis::TIME ) {
				}
				else if ( settings->getLblNumberFormat() == Axis::DATE ) {
				}
				else if ( settings->getLblNumberFormat() == Axis::TIMEDATE ) {

				}

				if ( settings->getLblNumberDecimalSeparator() != 0 )
					labelTMP.replace( '.', settings->getLblNumberDecimalSeparator() );

				newLabel->setHtml( prefix +
//								   "<sup>" +
								   labelTMP +
//								   "</sup>" +
								   suffix );

				if ( settings->getType() == Axis::XAxis )
				{
					newLabel->setPosition( settings->getTkmaPos().at(it) + settings->getLblOffset().x(), labelsPos2, settings->getLblAnchor(), settings->getLblAngle() );
				}
				else if ( settings->getType() == Axis::YAxis )
				{
					newLabel->setPosition( labelsPos2, settings->getTkmaPos().at(it) + settings->getLblOffset().y(), settings->getLblAnchor(), settings->getLblAngle() );
				};

				addToGroup( newLabel );
			}
		}
	}
	//settings->setLabelsRect( boundingRect() );
}
void AxisLabelsItem::mousePressEvent( QGraphicsSceneMouseEvent* event )
{
	qDebug() << "Custom item clicked.";
}

void AxisLabelsItem::hoverEnterEvent( QGraphicsSceneHoverEvent* event )
{
	boundingItem = new QGraphicsRectItem( boundingRect(), this );
	boundingItem->setPen( QColor(255,0,0) );
	boundingItem->update();
	QGraphicsItemGroup::update();
	qDebug() << "AxisLabelsItem Enter Event.";
}

void AxisLabelsItem::hoverLeaveEvent( QGraphicsSceneHoverEvent* event )
{
	if ( boundingItem )
		delete boundingItem;
	QGraphicsItemGroup::update();
	qDebug() << "AxisLabelsItem leave Event.";
}
/**********************************************/
/**********************************************/
