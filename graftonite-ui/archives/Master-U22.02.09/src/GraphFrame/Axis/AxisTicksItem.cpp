#include "AxisTicksItem.h"

#include <cmath>
/**********************************************/
/**********************************************/
/* */
AxisTicksItem::~AxisTicksItem() {
	for( auto elm : this->childItems() )
	{
		removeFromGroup( elm );
		delete elm;
	}
}
/**********************************************/
/**********************************************/
/* */
AxisTicksItem::AxisTicksItem( QGraphicsItem* parent, bool isMajor )
	: QGraphicsItemGroup( parent )
{
	this->isMajor = isMajor;
}
/**********************************************/
/**********************************************/
/* */
void AxisTicksItem::update( AxisSettings* settings, double linePos ) {
	for( auto elm : this->childItems() ) {
		removeFromGroup( elm );
		delete elm;
	}
	double ticksSize;
	QList<double> ticksPos;
	if ( isMajor ) {
		ticksSize = settings->getTkmaSize();
		ticksPos = settings->getTkmaPos();
	}
	else {
		ticksSize = settings->getTkmiSize();
		ticksPos = settings->getTkmiPos();
	}

	if ( settings->getTkPos() != Axis::NONE ) {
		double ticksPos2 = linePos;
		if ( settings->getType() == Axis::XAxis ) {
			if ( settings->getTkPos() == Axis::TOP ) {
				ticksPos2 = linePos - ticksSize;
			}
			else if ( settings->getTkPos() == Axis::CENTER ) {
				ticksPos2 = linePos  - 0.5 * ticksSize;
			}
		}
		else if ( settings->getType() == Axis::YAxis ) {
			ticksPos2 = linePos - ticksSize;
			if ( settings->getTkPos() == Axis::RIGHT ) {
				ticksPos2 = linePos;
			}
			else if (settings->getTkPos() == Axis::CENTER) {
				ticksPos2 = linePos - 0.5 * ticksSize;
			}
		}
		if ( ticksPos.size() != 0 && ticksSize > 0 ) {
			for ( int it = 0; it < ticksPos.size(); it++ ) {
				QGraphicsLineItem *newTick = new QGraphicsLineItem();
				newTick->setPen( settings->getLinePen() );
				if ( settings->getType() == Axis::XAxis ) {
					newTick->setLine( ticksPos.at(it), ticksPos2,
									  ticksPos.at(it), ticksPos2 + ticksSize );
				}
				else if ( settings->getType() == Axis::YAxis ) {
					newTick->setLine( ticksPos2,
									  ticksPos.at(it),
									  ticksPos2 + ticksSize,
									  ticksPos.at(it) );
				}
				addToGroup(newTick);
			}
		}
	}
	//	if ( !_isMinor )
	//	{
	//		axissets->setTicksRect( GraphMath::getContainingRect( axissets->lineRect(), boundingRect() ) );
	//	}
	//	else
	//	{
	//		axissets->setTicksRect( GraphMath::getContainingRect( axissets->ticksRect(), boundingRect() ) );
	//	}
}
/**********************************************/
/**********************************************/
