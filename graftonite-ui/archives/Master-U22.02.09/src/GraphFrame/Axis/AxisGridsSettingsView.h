#ifndef AXISGRIDSSETTINGSVIEW_H
#define AXISGRIDSSETTINGSVIEW_H

#include <QWidget>

#include "../AxisSettings.h"
#include "Commons/theme.h"
#include "Commons/lang.h"
#include "Commons/TabWidget.h"
#include "Commons/ScrollArea.h"
#include "Commons/FormLayout.h"
#include "Commons/GridLayout.h"
#include "Commons/SettingLabel.h"
#include "Commons/LineEditor.h"
#include "Commons/IntegerSelector.h"
#include "../Misc/AxisGridsPosSelector.h"
#include "../Misc/LineStyleSelector.h"
#include "../Misc/LineCapSelector.h"
#include "Commons/ColorSelector.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class AxisGridsSettingsView
		: public ScrollArea
{
	Q_OBJECT
private:
	AxisSettings* settings = nullptr;
	SettingLabel* lab_Grids = new SettingLabel;
	SettingLabel* lab_GridsPos = new SettingLabel;
	AxisGridsPosSelector* sel_GridsPos = new AxisGridsPosSelector;
	SettingLabel* lab_MajGrids = new SettingLabel;
	SettingLabel* lab_MinGrids = new SettingLabel;
	SettingLabel* lab_Style = new SettingLabel;
	SettingLabel* lab_Width = new SettingLabel;
	SettingLabel* lab_Color = new SettingLabel;
	SettingLabel* lab_Cap = new SettingLabel;
	LineStyleSelector* sel_MajStyle = new LineStyleSelector;
	IntegerSelector* sel_MajWidth = new IntegerSelector;
	ColorSelector* sel_MajColor = new ColorSelector;
	LineCapSelector* sel_MajCap = new LineCapSelector;
	LineStyleSelector* sel_MinStyle = new LineStyleSelector;
	IntegerSelector* sel_MinWidth = new IntegerSelector;
	ColorSelector* sel_MinColor = new ColorSelector;
	LineCapSelector* sel_MinCap = new LineCapSelector;

public:
	AxisGridsSettingsView( QWidget* parent = nullptr );
	void setSettings( AxisSettings* sets );
	void updateIcons( Icons* icons );
	void updateLang( Lang* lang );

public slots:
	void update();
	void disableWidget();
	void connectModifiers();
	void disconnectModifiers();
	void updatePosList();
	void posChanged( const Axis::Pos& pos );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // AXISGRIDSSETTINGSVIEW_H
