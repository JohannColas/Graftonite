#include "AxisGridsItem.h"
/**********************************************/
/**********************************************/
/* */
AxisGridsItem::AxisGridsItem( QGraphicsItem* parent, bool isMajor )
	: QGraphicsItemGroup( parent )
{
	this->isMajor = isMajor;
}
/**********************************************/
/**********************************************/
/* */
void AxisGridsItem::update( AxisSettings* settings,
							GraphRect& layerRect,
							double linePos )
{
	if ( settings->getGdPos() != Axis::NONE )
	{
		QPen gridsPen;
		QList<double> gridsPos;
		if ( isMajor )
		{
			gridsPen = settings->getMajorGridsPen();
			gridsPos = settings->getTkmaPos();
		}
		else
		{
			gridsPen = settings->getMinorGridsPen();
			gridsPos = settings->getTkmiPos();
		}
		double gridsPos1 = linePos;
		double gridsPos2 = linePos;
		if ( settings->getType() == Axis::XAxis )
		{
			if ( settings->getGdPos() == Axis::TOP || settings->getGdPos() == Axis::BOTH )
			{
				gridsPos1 = layerRect.y();
			}
			if ( settings->getGdPos() == Axis::BOTTOM || settings->getGdPos() == Axis::BOTH ){
				gridsPos2 = layerRect.y() + layerRect.h();
			}
		}
		else if ( settings->getType() == Axis::YAxis )
		{
			if ( settings->getGdPos() == Axis::LEFT || settings->getGdPos() == Axis::BOTH )
			{
				gridsPos1 = layerRect.x();
			}
			if ( settings->getGdPos() == Axis::RIGHT || settings->getGdPos() == Axis::BOTH ){
				gridsPos2 = layerRect.x() + layerRect.w();
			}
		}
		if ( gridsPos.size() != 0 )
		{
			for ( int it = 0; it < gridsPos.size(); it++ )
			{
				QGraphicsLineItem *newGrid = new QGraphicsLineItem();
				newGrid->setPen( gridsPen );
				if ( settings->getType() == Axis::XAxis )
				{
					newGrid->setLine( gridsPos.at(it), gridsPos1,
									  gridsPos.at(it), gridsPos2 );
				}
				else if ( settings->getType() == Axis::YAxis )
				{
					newGrid->setLine( gridsPos1, gridsPos.at(it),
									  gridsPos2, gridsPos.at(it) );
				}
				addToGroup(newGrid);
			}
		}

	}
}
/**********************************************/
/**********************************************/
