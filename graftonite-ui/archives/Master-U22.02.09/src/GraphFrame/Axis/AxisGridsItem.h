#ifndef AXISGRIDSITEM_H
#define AXISGRIDSITEM_H

#include <QGraphicsItem>

#include "../AxisSettings.h"
#include "../Misc/GraphMisc.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class AxisGridsItem : public QObject, public QGraphicsItemGroup
{
	Q_OBJECT
protected:
	bool isMajor = true;
public:
	AxisGridsItem( QGraphicsItem* parent, bool isMajor );

public slots:
	void update( AxisSettings* settings, GraphRect& layerRect, double linePos );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // AXISGRIDSITEM_H
