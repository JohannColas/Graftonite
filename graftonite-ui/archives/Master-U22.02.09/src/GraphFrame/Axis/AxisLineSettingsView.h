#ifndef AXISLINESETTINGSVIEW_H
#define AXISLINESETTINGSVIEW_H

#include <QWidget>

#include "../AxisSettings.h"
#include "Commons/theme.h"
#include "Commons/lang.h"
#include "Commons/TabWidget.h"
#include "Commons/ScrollArea.h"
#include "Commons/FormLayout.h"
#include "Commons/GridLayout.h"
#include "Commons/SettingLabel.h"
#include "Commons/LineEditor.h"
#include "Commons/IntegerSelector.h"
#include "../Misc/LineStyleSelector.h"
#include "../Misc/LineCapSelector.h"
#include "Commons/ColorSelector.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class AxisLineSettingsView
		: public ScrollArea
{
	Q_OBJECT
private:
	AxisSettings* settings = nullptr;
	SettingLabel* lab_Line = new SettingLabel;
	SettingLabel* lab_Style = new SettingLabel;
	LineStyleSelector* sel_Style = new LineStyleSelector;
	SettingLabel* lab_Width = new SettingLabel;
	IntegerSelector* sel_Width = new IntegerSelector;
	SettingLabel* lab_Color = new SettingLabel;
	ColorSelector* sel_Color = new ColorSelector;
	SettingLabel* lab_Cap = new SettingLabel;
	LineCapSelector* sel_Cap = new LineCapSelector;

public:
	AxisLineSettingsView( QWidget* parent = nullptr );
	void setSettings( AxisSettings* sets );
	void updateIcons( Icons* icons );
	void updateLang( Lang* lang );

public slots:
	void update();
	void disableWidget();
	void connectModifiers();
	void disconnectModifiers();
	void blockChildrenSignals( bool arg );
	void styleChanged(const Qt::PenStyle& style);
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // AXISLINESETTINGSVIEW_H
