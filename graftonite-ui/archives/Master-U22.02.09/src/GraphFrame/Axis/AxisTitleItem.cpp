#include "AxisTitleItem.h"

#include "../Misc/GraphMisc.h"

#include <iostream>
using namespace std;
/**********************************************/
/**********************************************/
/* */
AxisTitleItem::~AxisTitleItem() {

}
/**********************************************/
/**********************************************/
/* */
AxisTitleItem::AxisTitleItem(QGraphicsItem* parent)
	: GraphTextItem( parent )
{

}
/**********************************************/
/**********************************************/
/* */
void AxisTitleItem::update( AxisSettings* settings,
							const QRectF& lineRect,
							const QRectF& ticksRect,
							const QRectF& labelsRect )
{
	//ticksRect;
	double ticksX = ticksRect.x();
	double ticksY = ticksRect.y();
	double ticksW = ticksRect.width();
	double ticksH = ticksRect.height();
	//labelsRect;
	double labelsX = labelsRect.x();
	double labelsY = labelsRect.y();
	double labelsW = labelsRect.width();
	double labelsH = labelsRect.height();

	if ( settings->getTlPos() != Axis::NONE ) {
		double titlePosX = 0, titlePosY = 0;
		if ( settings->getType() == Axis::XAxis )
		{
			titlePosY = GraphMath::max( { ticksY+ticksH, labelsY+labelsH } ) + settings->getTlOffset().y();
			if ( Axis::isLocated( settings->getTlPos(), Axis::TOP ) )
			{
				titlePosY = GraphMath::min( { ticksY, labelsY } ) - settings->getTlOffset().y();
			}
			titlePosX = 0.5 * ( 2 * lineRect.x() +  lineRect.width() )  + settings->getTlOffset().x();
			if ( Axis::isLocated(  settings->getTlPos(), Axis::LEFT ) ) {
				titlePosX = lineRect.x() + settings->getTlOffset().x();
			}
			else if ( Axis::isLocated( settings->getTlPos(), Axis::RIGHT ) ) {
				titlePosX =  lineRect.x() +  lineRect.width() - settings->getTlOffset().x();
			};
		}
		else if ( settings->getType() == Axis::YAxis )
		{
			titlePosX = GraphMath::min( { ticksX, labelsX } ) - settings->getTlOffset().x();
			if ( Axis::isLocated( settings->getTlPos(), Axis::RIGHT ) )
			{
				titlePosX = GraphMath::max( { ticksX+ticksW, labelsX+labelsW } ) + settings->getTlOffset().x();
			}
			titlePosY = 0.5 * ( 2 * lineRect.y() +  lineRect.height() )  + settings->getTlOffset().y();
			if ( Axis::isLocated(  settings->getTlPos(), Axis::TOP ) ) {
				titlePosY = lineRect.y() + settings->getTlOffset().y();
			}
			else if ( Axis::isLocated( settings->getTlPos(), Axis::BOTTOM ) ) {
				titlePosY =  lineRect.y() +  lineRect.height() - settings->getTlOffset().y();
			};
		}
		//setDefaultTextColor( settings->titleFont().color() );
		//setFont( settings->titleFont().getFont() );
		this->setHtml( settings->getTlText() );

		setPosition( titlePosX,
					 titlePosY,
					 settings->getTlAnchor(),  settings->getTlAngle() );
	}
}
/**********************************************/
/**********************************************/
