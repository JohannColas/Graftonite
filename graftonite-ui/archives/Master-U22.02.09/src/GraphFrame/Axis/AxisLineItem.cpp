#include "AxisLineItem.h"
/**********************************************/
/**********************************************/
/* */
AxisLineItem::~AxisLineItem() {
    
}
/**********************************************/
/**********************************************/
/* */
AxisLineItem::AxisLineItem( QGraphicsItem* parent )
	: QGraphicsLineItem( parent )
{

}
/**********************************************/
/**********************************************/
/* */
double AxisLineItem::getPos() const {
	return pos;
}
/**********************************************/
/**********************************************/
/* */
void AxisLineItem::update( AxisSettings* settings, GraphRect& rect ) {
	double x1 = rect.x(),
			x2 = rect.x() + rect.w(),
			y1 = rect.y() + settings->getPosOffset() + rect.h(),
			y2 = 0;
	// ----------------------
	if ( settings->getType() == Axis::XAxis )
	{
		if ( settings->getPos() == Axis::CENTER )
			y1 =  rect.y() + settings->getPosOffset() + 0.5 * rect.h();
		// ----------------------
		else if ( settings->getPos() == Axis::TOP )
			y1 = rect.y() + settings->getPosOffset();
		// ----------------------
		else if ( settings->getPos() == Axis::SPECIFIED )
			y1 =  rect.y() + settings->getPosOffset()  + ( 1.0 - 0.01 * settings->getPosAt() ) * rect.h();
		// ----------------------
		y2 = y1;
		pos = y1;
	}
	else if ( settings->getType() == Axis::YAxis )
	{
		x1 = rect.x() + settings->getPosOffset();
		y1 = rect.y();
		y2 = rect.y() + rect.h();
		// ----------------------
		if ( settings->getPos() == Axis::CENTER )
			x1 = rect.x() + settings->getPosOffset() + 0.5 * rect.w();
		// ----------------------
		else if ( settings->getPos() == Axis::RIGHT )
			x1 = rect.x() + settings->getPosOffset() + rect.w();
		// ----------------------
		else if ( settings->getPos() == Axis::SPECIFIED )
			x1 =  rect.x() + settings->getPosOffset()  + 0.01 * settings->getPosAt() * rect.w();
		// ----------------------
		x2 = x1;
		pos = x1;
	}
	//	// ----------------------
	//	// Set Axis Line
	setPen( settings->getLinePen() );
	setLine (x1, y1, x2, y2 );
	//	settings->setLinePos( pos() );
	//	settings->setLineRect( rect() );
}
/**********************************************/
/**********************************************/
