#include "AxisTitleSettingsView.h"
/**********************************************/
/**********************************************/
/* */
AxisTitleSettingsView::AxisTitleSettingsView( QWidget* parent )
	: ScrollArea( parent )
{
	GridLayout* lay0 = new GridLayout;
	lay0->addWidget( lab_title, 0, 0 );

	FormLayout* layMain= new FormLayout;
	layMain->addRow( lab_Pos, sel_Pos );
	layMain->addRow( lab_Anchor, sel_Anchor );
	sel_XOffset->setDimension( -100000, 100000 );
	sel_YOffset->setDimension( -100000, 100000 );
	layMain->addLine( lab_Offset, sel_XOffset, sel_YOffset );
	sel_Angle->setAngle();
	layMain->addRow( lab_Angle, sel_Angle );
	sel_FtFamily->setMaximumWidth( 150 );
	sel_FtSize->setFontSize();
	//	sel_FtSize->setDecimals( 0 );
	layMain->addFullLine( sel_FtFamily, sel_FtSize, sel_FtCap );
	sel_FtItalic->setCheckable( true );
	sel_FtBold->setCheckable( true );
	sel_FtUnder->setCheckable( true );
	sel_FtIndice->setCheckable( true );
	sel_FtExponant->setCheckable( true );
	layMain->addFullLine( {sel_FtColor, sel_FtItalic, sel_FtBold, sel_FtUnder, sel_FtExponant, sel_FtIndice, sel_Align} );

	lay0->addLayout( layMain, 1, 0 );
	lay0->addWidget( edt_Title, 2, 0 );
	lay0->addItem( new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding), 3, 0, 1, 1 );

	QWidget* graphWid = new QWidget;
	graphWid->setMinimumWidth( 220 );
	graphWid->setLayout( lay0 );
	setWidget( graphWid );
//	setLayout( lay0 );
}
/**********************************************/
/**********************************************/
/* */
void AxisTitleSettingsView::setSettings( AxisSettings* sets ) {
	disconnectModifiers();

	settings = sets;

	connectModifiers();

}
/**********************************************/
/**********************************************/
/* */
void AxisTitleSettingsView::connectModifiers() {
	connect( sel_Pos, &AxisTitlePosSelector::changed,
			 this, &AxisTitleSettingsView::posChanged );
	connect( sel_Anchor, &AnchorSelector::changed,
			 settings, &AxisSettings::setTlAnchor );
	connect( sel_XOffset, &DoubleSelector::changed,
			 settings, &AxisSettings::setTlXOffset );
	connect( sel_YOffset, &DoubleSelector::changed,
			 settings, &AxisSettings::setTlYOffset );
	connect( sel_Angle, &DoubleSelector::changed,
			 settings, &AxisSettings::setTlAngle );
	// Title Text
	connect( edt_Title, &TextEditor::textChanged,
			 this, &AxisTitleSettingsView::titleTextChanged );
	connect( edt_Title, &TextEditor::cursorPositionChanged,
			 this, &AxisTitleSettingsView::titleText_cursorPosChanged );
	connect( sel_FtFamily, &QFontComboBox::currentTextChanged,
			 edt_Title, &TextEditor::changeSelectionFamily );
	connect( sel_FtSize, &IntegerSelector::changed,
			 edt_Title, &TextEditor::changeSelectionSize );
	connect( sel_FtCap, &TextCapSelector::changed,
			 edt_Title, &TextEditor::changeSelectionCap );
	connect( sel_FtColor, &ColorSelector::changed,
			 edt_Title, &TextEditor::changeSelectionColor );
	connect( sel_FtItalic, &QPushButton::toggled,
			 edt_Title, &TextEditor::changeSelectionItalic );
	connect( sel_FtBold, &QPushButton::toggled,
			 edt_Title, &TextEditor::changeSelectionBold );
	connect( sel_FtUnder, &QPushButton::toggled,
			 edt_Title, &TextEditor::changeSelectionUnderline );
	connect( sel_FtIndice, &QPushButton::toggled,
			 this, &AxisTitleSettingsView::ftIndiceChanged );
	connect( sel_FtExponant, &QPushButton::toggled,
			 this, &AxisTitleSettingsView::ftExponantChanged );
	connect( sel_Align, &TextAlignSelector::changed,
			 edt_Title, &TextEditor::changeSelectionAlign );
}
/**********************************************/
/**********************************************/
/* */
void AxisTitleSettingsView::disconnectModifiers() {
	if ( settings != nullptr ) {
		disconnect( sel_Pos, &AxisTitlePosSelector::changed,
					this, &AxisTitleSettingsView::posChanged );
		disconnect( sel_Anchor, &AnchorSelector::changed,
					settings, &AxisSettings::setTlAnchor );
		disconnect( sel_XOffset, &DoubleSelector::changed,
					settings, &AxisSettings::setTlXOffset );
		disconnect( sel_YOffset, &DoubleSelector::changed,
					settings, &AxisSettings::setTlYOffset );
		disconnect( sel_Angle, &DoubleSelector::changed,
					settings, &AxisSettings::setTlAngle );
		// Title Text
		disconnect( edt_Title, &TextEditor::textChanged,
					this, &AxisTitleSettingsView::titleTextChanged );
		disconnect( edt_Title, &TextEditor::cursorPositionChanged,
					this, &AxisTitleSettingsView::titleText_cursorPosChanged );
		disconnect( sel_FtFamily, &QFontComboBox::currentTextChanged,
					edt_Title, &TextEditor::changeSelectionFamily );
		disconnect( sel_FtSize, &IntegerSelector::changed,
					edt_Title, &TextEditor::changeSelectionSize );
		disconnect( sel_FtCap, &TextCapSelector::changed,
					edt_Title, &TextEditor::changeSelectionCap );
		disconnect( sel_FtColor, &ColorSelector::changed,
					edt_Title, &TextEditor::changeSelectionColor );
		disconnect( sel_FtItalic, &QPushButton::toggled,
					edt_Title, &TextEditor::changeSelectionItalic );
		disconnect( sel_FtBold, &QPushButton::toggled,
					edt_Title, &TextEditor::changeSelectionBold );
		disconnect( sel_FtUnder, &QPushButton::toggled,
					edt_Title, &TextEditor::changeSelectionUnderline );
		disconnect( sel_FtIndice, &QPushButton::toggled,
					this, &AxisTitleSettingsView::ftIndiceChanged );
		disconnect( sel_FtExponant, &QPushButton::toggled,
					this, &AxisTitleSettingsView::ftExponantChanged );
		disconnect( sel_Align, &TextAlignSelector::changed,
					edt_Title, &TextEditor::changeSelectionAlign );
	}
}
/**********************************************/
/**********************************************/
/* */
void AxisTitleSettingsView::update() {
	sel_Pos->blockSignals( true );
	sel_Pos->updateItems( settings->getType() );
	sel_Pos->setPos( settings->getTlPos() );
	disableWidget();
	sel_Pos->blockSignals( false );
	sel_Anchor->setAnchor( settings->getTlAnchor() );
	sel_XOffset->setValue( settings->getTlOffset().x() );
	sel_YOffset->setValue( settings->getTlOffset().y() );
	sel_Angle->setValue( settings->getTlAngle());
	edt_Title->setHtml( settings->getTlText() );
}
/**********************************************/
/**********************************************/
/* */
void AxisTitleSettingsView::disableWidget() {
	if ( settings->getLblPos() == Axis::NONE ) {
		lab_Anchor->setDisabled( true );
		sel_Anchor->setDisabled( true );
		lab_Offset->setDisabled( true );
		sel_XOffset->setDisabled( true );
		sel_YOffset->setDisabled( true );
		lab_Angle->setDisabled( true );
		sel_Angle->setDisabled( true );
		lab_Font->setDisabled( true );
		sel_FtFamily->setDisabled( true );
		sel_FtSize->setDisabled( true );
		sel_FtCap->setDisabled( true );
		sel_FtColor->setDisabled( true );
		sel_FtItalic->setDisabled( true );
		sel_FtBold->setDisabled( true );
		sel_FtUnder->setDisabled( true );
		sel_FtExponant->setDisabled( true );
		sel_FtIndice->setDisabled( true );
		sel_Align->setDisabled( true );
		edt_Title->setDisabled( true );
	}
	else {
		lab_Anchor->setDisabled( false );
		sel_Anchor->setDisabled( false );
		lab_Offset->setDisabled( false );
		sel_XOffset->setDisabled( false );
		sel_YOffset->setDisabled( false );
		lab_Angle->setDisabled( false );
		sel_Angle->setDisabled( false );
		lab_Font->setDisabled( false );
		sel_FtFamily->setDisabled( false );
		sel_FtSize->setDisabled( false );
		sel_FtCap->setDisabled( false );
		sel_FtColor->setDisabled( false );
		sel_FtItalic->setDisabled( false );
		sel_FtBold->setDisabled( false );
		sel_FtUnder->setDisabled( false );
		sel_FtExponant->setDisabled( false );
		sel_FtIndice->setDisabled( false );
		sel_Align->setDisabled( false );
		edt_Title->setDisabled( false );
	}
}
/**********************************************/
/**********************************************/
/* */
void AxisTitleSettingsView::updatePosList() {
	sel_Pos->updateItems( settings->getType() );
	sel_Pos->setPos( settings->getTkPos() );
}
/**********************************************/
/**********************************************/
/* */
void AxisTitleSettingsView::blockChildrenSignals( QObject* obj, bool checked ) {
	for( auto elm : obj->children() )
	{
		elm->blockSignals( checked );
		blockChildrenSignals( elm, checked );
	}
}
/**********************************************/
/**********************************************/
/* */
void AxisTitleSettingsView::posChanged( const Axis::Pos& pos ) {
	settings->setTlPos( pos );
	disableWidget();
}
/**********************************************/
/**********************************************/
/* */
void AxisTitleSettingsView::titleTextChanged() {
	QString str = edt_Title->toHtml();
	int beg = str.indexOf("<p ");
	int end = str.lastIndexOf("</p>");
	str = str.mid( beg, end-beg+4 );
	settings->setTlText( str );
}
/**********************************************/
/**********************************************/
/* */
void AxisTitleSettingsView::titleText_cursorPosChanged() {
	blockChildrenSignals( this, true );
	QTextCharFormat currentTCF = edt_Title->currentCharFormat();
	QFont currentFont = currentTCF.font();
	sel_FtFamily->setCurrentFont( currentFont.family() );
	sel_FtSize->setValue( currentFont.pointSize() );
	sel_FtColor->setColor( currentTCF.foreground().color() );
	sel_FtItalic->setChecked( currentFont.italic() );
	sel_FtBold->setChecked( currentFont.bold() );
	sel_FtUnder->setChecked( currentFont.underline() );
	sel_FtExponant->setChecked(
				currentTCF.verticalAlignment() == QTextCharFormat::AlignSuperScript ?
					true : false );
	sel_FtIndice->setChecked(
				currentTCF.verticalAlignment() == QTextCharFormat::AlignSubScript ?
					true : false );
	sel_Align->setAlign( edt_Title->alignment() );
	sel_FtCap->setCap( currentFont.capitalization() );
	blockChildrenSignals( this, false );
}
/**********************************************/
/**********************************************/
/* */
void AxisTitleSettingsView::ftExponantChanged( bool checked ) {
	if( checked )
		sel_FtIndice->setChecked(false );
	edt_Title->changeSelectionExponant( checked );
}
/**********************************************/
/**********************************************/
/* */
void AxisTitleSettingsView::ftIndiceChanged( bool checked ) {
	if( checked )
		sel_FtExponant->setChecked(false );
	edt_Title->changeSelectionIndice( checked );
}
/**********************************************/
/**********************************************/
/* */
void AxisTitleSettingsView::updateIcons( Icons* icons ) {
	sel_FtItalic->setIcon( icons->textItalic() );
	sel_FtBold->setIcon( icons->textBold() );
	sel_FtUnder->setIcon( icons->textUnder() );
	sel_FtExponant->setIcon( icons->textSuper() );
	sel_FtIndice->setIcon( icons->textSub() );
	sel_Align->updateIcons( icons );
}
/**********************************************/
/**********************************************/
/* */
void AxisTitleSettingsView::updateLang( Lang* lang ) {
	lab_title->setText( lang->get("axisTitleSets") );
	lab_Pos->setText( lang->get("position") + " :" );
	sel_Pos->updateLang( lang );
	lab_Anchor->setText( lang->get("anchor") + " :" );
	sel_Anchor->updateLang( lang );
	lab_Offset->setText( lang->get("offsets") + " :" );
	lab_Angle->setText( lang->get("angle") + " :" );
	lab_Font->setText( lang->get("font") );
	sel_FtCap->updateLang( lang );
	sel_Align->updateLang( lang );
}
/**********************************************/
/**********************************************/
