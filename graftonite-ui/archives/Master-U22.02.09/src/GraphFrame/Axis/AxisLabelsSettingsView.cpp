#include "AxisLabelsSettingsView.h"

#include <QSpacerItem>
/**********************************************/
/**********************************************/
/* */
AxisLabelsSettingsView::AxisLabelsSettingsView( QWidget* parent )
	: ScrollArea( parent )
{
	GridLayout* lay0 = new GridLayout;
	lay0->addWidget( lab_Labels, 0, 0 );

	FormLayout* layMain= new FormLayout;
	layMain->addRow( lab_Pos, sel_Pos );
	layMain->addRow( lab_Anchor, sel_Anchor );
	sel_XOffset->set( -100000, 100000 );
	sel_YOffset->set( -100000, 100000 );
	layMain->addLine( lab_Offset, sel_XOffset, sel_YOffset );
	sel_Angle->setAngle();
	layMain->addRow( lab_Angle, sel_Angle );
	// Number Formatting
	layMain->addRow( lab_NbFrt );
	layMain->addLine( lab_Format, sel_Format, lab_FormatOpt, sel_FormatOpt );
	sel_NbPrec->set( -1, 120, "" );
	layMain->addLine( lab_NbPrec, sel_NbPrec, lab_NbDecSep, sel_NbDecSep );
	// Font
	sel_FtSize->setFontSize();
	sel_FtFamily->setMaximumWidth( 150 );
	sel_FtCap->setMaximumWidth( 100 );
	layMain->addFullLine( {sel_FtFamily, sel_FtSize, sel_FtCap} );
	sel_FtItalic->setCheckable( true );
	sel_FtBold->setCheckable( true );
	sel_FtUnder->setCheckable( true );
	layMain->addFullLine( {sel_FtColor, sel_FtItalic, sel_FtBold, sel_FtUnder} );

	lay0->addLayout( layMain, 1, 0 );
	lay0->addItem( new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding), 2, 0, 1, 1 );

	QWidget* graphWid = new QWidget;
	graphWid->setMinimumWidth( 220 );
	graphWid->setLayout( lay0 );
	setWidget( graphWid );
//	setLayout( lay0 );
}
/**********************************************/
/**********************************************/
/* */
void AxisLabelsSettingsView::setSettings( AxisSettings* sets ) {
	disconnectModifiers();

	settings = sets;

	connectModifiers();

}
/**********************************************/
/**********************************************/
/* */
void AxisLabelsSettingsView::connectModifiers() {
	connect( sel_Pos, &AxisLabelsPosSelector::changed,
			 this, &AxisLabelsSettingsView::posChanged );
	connect( sel_Anchor, &AnchorSelector::changed,
			 settings, &AxisSettings::setLblAnchor );
	connect( sel_XOffset, &DoubleSelector::changed,
			 settings, &AxisSettings::setLblXOffset );
	connect( sel_YOffset, &DoubleSelector::changed,
			 settings, &AxisSettings::setLblYOffset );
	connect( sel_Angle, &DoubleSelector::changed,
			 settings, &AxisSettings::setLblAngle );
	// Number Formatting
	connect( sel_Format, &NumberFormatSelector::changed,
			 this, &AxisLabelsSettingsView::numberFormatChanged );
	connect( sel_FormatOpt, &NumberFormatOptionSelector::changed,
			 settings, &AxisSettings::setLblNumberFormatOption );
	connect( sel_NbPrec, &IntegerSelector::changed,
			 settings, &AxisSettings::setLblNumberPrec );
	connect( sel_NbDecSep, &NumberDecimalSepSelector::changed,
			 settings, &AxisSettings::setLblNumberDecimalSeparator );
	connect( sel_Prefix, &LineEditor::lineChanged,
			 settings, &AxisSettings::setLblPrefix );
	connect( sel_Suffix, &LineEditor::lineChanged,
			 settings, &AxisSettings::setLblSuffix );
	// Font
	connect( sel_FtFamily, &QFontComboBox::currentTextChanged,
			 settings, &AxisSettings::setLblFtFamily );
	connect( sel_FtSize, &DoubleSelector::changed,
			 settings, &AxisSettings::setLblFtSize );
	connect( sel_FtColor, &ColorSelector::changed,
			 settings, &AxisSettings::setLblFtColor);
	connect( sel_FtCap, &TextCapSelector::changed,
			 settings, &AxisSettings::setLblFtCap );
	connect( sel_FtItalic, &QPushButton::toggled,
			 settings, &AxisSettings::setLblFtItalic );
	connect( sel_FtBold, &QPushButton::toggled,
			 settings, &AxisSettings::setLblFtBold );
	connect( sel_FtUnder, &QPushButton::toggled,
			 settings, &AxisSettings::setLblFtUnder );
}
/**********************************************/
/**********************************************/
/* */
void AxisLabelsSettingsView::disconnectModifiers() {
	if ( settings != nullptr )
	{
		disconnect( sel_Pos, &AxisLabelsPosSelector::changed,
					this, &AxisLabelsSettingsView::posChanged );
		disconnect( sel_Anchor, &AnchorSelector::changed,
					settings, &AxisSettings::setLblAnchor );
		disconnect( sel_XOffset, &DoubleSelector::changed,
					settings, &AxisSettings::setLblXOffset );
		disconnect( sel_YOffset, &DoubleSelector::changed,
					settings, &AxisSettings::setLblYOffset );
		disconnect( sel_Angle, &DoubleSelector::changed,
					settings, &AxisSettings::setLblAngle );
		// Number Formatting
		disconnect( sel_Format, &NumberFormatSelector::changed,
					this, &AxisLabelsSettingsView::numberFormatChanged );
		disconnect( sel_FormatOpt, &NumberFormatOptionSelector::changed,
					settings, &AxisSettings::setLblNumberFormatOption );
		disconnect( sel_NbPrec, &IntegerSelector::changed,
					settings, &AxisSettings::setLblNumberPrec );
		disconnect( sel_NbDecSep, &NumberDecimalSepSelector::changed,
					settings, &AxisSettings::setLblNumberDecimalSeparator );
		disconnect( sel_Prefix, &LineEditor::lineChanged,
					settings, &AxisSettings::setLblPrefix );
		disconnect( sel_Suffix, &LineEditor::lineChanged,
					settings, &AxisSettings::setLblSuffix );
		// Font
		disconnect( sel_FtFamily, &QFontComboBox::currentTextChanged,
					settings, &AxisSettings::setLblFtFamily );
		disconnect( sel_FtSize, &DoubleSelector::changed,
					settings, &AxisSettings::setLblFtSize );
		disconnect( sel_FtColor, &ColorSelector::changed,
					settings, &AxisSettings::setLblFtColor);
		disconnect( sel_FtCap, &TextCapSelector::changed,
					settings, &AxisSettings::setLblFtCap );
		disconnect( sel_FtItalic, &QPushButton::toggled,
					settings, &AxisSettings::setLblFtItalic );
		disconnect( sel_FtBold, &QPushButton::toggled,
					settings, &AxisSettings::setLblFtBold );
		disconnect( sel_FtUnder, &QPushButton::toggled,
					settings, &AxisSettings::setLblFtUnder );
	}
}
/**********************************************/
/**********************************************/
/* */
void AxisLabelsSettingsView::update() {
	sel_Pos->blockSignals( true );
	sel_Pos->updateItems( settings->getType() );
	sel_Pos->setCurrentPos( settings->getLblPos() );
	disableWidget();
	sel_Pos->blockSignals( false );
	sel_Anchor->setAnchor( settings->getLblAnchor() );
	sel_XOffset->setValue( settings->getLblOffset().x() );
	sel_YOffset->setValue( settings->getLblOffset().y() );
	sel_Angle->setValue( settings->getLblAngle());
	sel_Format->blockSignals( true );
	sel_Format->setFormat( settings->getLblNumberFormat() );
	updateFormatOption();
	sel_Format->blockSignals( false );
	sel_FormatOpt->blockSignals( true );
	if ( sel_FormatOpt->isEnabled() )
		sel_FormatOpt->setOption( settings->getLblNumberFormatOption() );
	sel_FormatOpt->blockSignals( false );
	sel_NbPrec->setValue( settings->getLblNumberPrecision() );
	sel_NbDecSep->setDecimalSeparator( settings->getLblNumberDecimalSeparator() );
	sel_Prefix->setText( settings->getLblPrefix() );
	sel_Suffix->setText( settings->getLblSuffix() );
	QFont font = settings->getLblFont();
	sel_FtFamily->setCurrentFont( font.family() );
	sel_FtSize->setValue( font.pointSize() );
	sel_FtCap->setCap( font.capitalization() );
	sel_FtColor->setColor( settings->getLblFtColor() );
	sel_FtItalic->setChecked( font.italic() );
	sel_FtBold->setChecked( font.bold() );
	sel_FtUnder->setChecked( font.underline() );
}
/**********************************************/
/**********************************************/
/* */
void AxisLabelsSettingsView::disableWidget() {
	if ( settings->getLblPos() == Axis::NONE ) {
		lab_Anchor->setDisabled( true );
		sel_Anchor->setDisabled( true );
		lab_Offset->setDisabled( true );
		sel_XOffset->setDisabled( true );
		sel_YOffset->setDisabled( true );
		lab_Angle->setDisabled( true );
		sel_Angle->setDisabled( true );
		lab_NbFrt->setDisabled( true );
		lab_Format->setDisabled( true );
		sel_Format->setDisabled( true );
		lab_FormatOpt->setDisabled( true );
		sel_FormatOpt->setDisabled( true );
		lab_NbPrec->setDisabled( true );
		sel_NbPrec->setDisabled( true );
		lab_NbDecSep->setDisabled( true );
		sel_NbDecSep->setDisabled( true );
		lab_Font->setDisabled( true );
		sel_FtFamily->setDisabled( true );
		sel_FtSize->setDisabled( true );
		sel_FtCap->setDisabled( true );
		sel_FtColor->setDisabled( true );
		sel_FtItalic->setDisabled( true );
		sel_FtBold->setDisabled( true );
		sel_FtUnder->setDisabled( true );
	}
	else {
		lab_Anchor->setDisabled( false );
		sel_Anchor->setDisabled( false );
		lab_Offset->setDisabled( false );
		sel_XOffset->setDisabled( false );
		sel_YOffset->setDisabled( false );
		lab_Angle->setDisabled( false );
		sel_Angle->setDisabled( false );
		lab_NbFrt->setDisabled( false );
		lab_Format->setDisabled( false );
		sel_Format->setDisabled( false );
		updateFormatOption();
		lab_NbPrec->setDisabled( false );
		sel_NbPrec->setDisabled( false );
		lab_NbDecSep->setDisabled( false );
		sel_NbDecSep->setDisabled( false );
		lab_Font->setDisabled( false );
		sel_FtFamily->setDisabled( false );
		sel_FtSize->setDisabled( false );
		sel_FtCap->setDisabled( false );
		sel_FtColor->setDisabled( false );
		sel_FtItalic->setDisabled( false );
		sel_FtBold->setDisabled( false );
		sel_FtUnder->setDisabled( false );
	}
}
/**********************************************/
/**********************************************/
/* */
void AxisLabelsSettingsView::updatePosList() {
	sel_Pos->updateItems( settings->getType() );
	sel_Pos->setCurrentPos( settings->getTkPos() );
}
/**********************************************/
/**********************************************/
/* */
void AxisLabelsSettingsView::posChanged( const Axis::Pos& pos ) {
	settings->setLblPos( pos );
	disableWidget();
}
/**********************************************/
/**********************************************/
/* */
void AxisLabelsSettingsView::numberFormatChanged( const Axis::NumberFormat& nbFrmt ) {
	settings->setLblNumberFormat( nbFrmt );
	updateFormatOption();
}
/**********************************************/
/**********************************************/
/* */
void AxisLabelsSettingsView::updateFormatOption() {
	sel_FormatOpt->blockSignals( true );
	Axis::NumberFormat nbFrmt = settings->getLblNumberFormat();
	if ( nbFrmt == Axis::DEFAULTNF || nbFrmt == Axis::DECIMAL )
	{
		sel_FormatOpt->clear();
		lab_FormatOpt->setDisabled( true );
		sel_FormatOpt->setDisabled( true );
	}
	else
	{
		sel_FormatOpt->updateOptions( nbFrmt );
		lab_FormatOpt->setDisabled( false );
		sel_FormatOpt->setDisabled( false );
	}
	sel_FormatOpt->blockSignals( false );
}
/**********************************************/
/**********************************************/
/* */
void AxisLabelsSettingsView::updateIcons( Icons* icons ) {
	sel_FtItalic->setIcon( icons->textItalic() );
	sel_FtBold->setIcon( icons->textBold() );
	sel_FtUnder->setIcon( icons->textUnder() );
}
/**********************************************/
/**********************************************/
/* */
void AxisLabelsSettingsView::updateLang( Lang* lang ) {
	lab_Labels->setText( lang->get("axisLabelsSets") );
	lab_Pos->setText( lang->get("position") + " :" );
	sel_Pos->updateLang( lang );
	lab_Anchor->setText( lang->get("anchor") + " :" );
	sel_Anchor->updateLang( lang );
	lab_Offset->setText( lang->get("offsets") + " :" );
	lab_Angle->setText( lang->get("angle") + " :" );
	lab_NbFrt->setText( lang->get("labelsFormating") );
	lab_Format->setText( lang->get("numberFormat") + " :" );
	sel_Format->updateLang( lang );
	lab_FormatOpt->setText( lang->get("numberFormatOpt") + " :" );
	lab_NbPrec->setText( lang->get("numberPrec") + " :" );
	lab_NbDecSep->setText( lang->get("numberDecSep") + " :" );
	sel_NbDecSep->updateLang( lang );
	sel_NbPrec->setSpecialValueText( lang->get("automatic") );
	lab_Font->setText( lang->get("font") );
	sel_FtCap->updateLang( lang );
}
/**********************************************/
/**********************************************/
