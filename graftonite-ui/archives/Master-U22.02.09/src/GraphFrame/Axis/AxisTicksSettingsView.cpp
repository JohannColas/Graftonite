#include "AxisTicksSettingsView.h"
/**********************************************/
/**********************************************/
/* */
AxisTicksSettingsView::AxisTicksSettingsView( QWidget* parent )
	: ScrollArea( parent )
{
	FormLayout* layPos = new FormLayout;
	layPos->addRow( lab_Ticks );
	layPos->addRow( lab_TicksPos, sel_TicksPos );

	GridLayout* lay0 = new GridLayout;
	lay0->addLayout( layPos, 0, 0, 1, 3 );

	lay0->addWidget( lab_MajTicks, 1, 1 );
	lay0->addWidget( lab_MinTicks, 1, 2 );

	lay0->addWidget( lab_Inc, 2, 0 );
	lay0->setAlignment( lab_Inc, Qt::AlignRight );
	lay0->addWidget( sel_MajInc, 2, 1 );
	lay0->addWidget( sel_MinInc, 2, 2 );
	lay0->addWidget( lab_Size, 3, 0 );
	lay0->setAlignment( lab_Size, Qt::AlignRight );
	lay0->addWidget( sel_MajSize, 3, 1 );
	lay0->addWidget( sel_MinSize, 3, 2 );
	lay0->addItem( new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding), 4, 0, 1, 3 );

	QWidget* graphWid = new QWidget;
	graphWid->setMinimumWidth( 220 );
	graphWid->setLayout( lay0 );
	setWidget( graphWid );
//	setLayout( lay0 );
}
/**********************************************/
/**********************************************/
/* */
void AxisTicksSettingsView::setSettings( AxisSettings* sets ) {
	disconnectModifiers();

	settings = sets;

	connectModifiers();

	//update();
}
/**********************************************/
/**********************************************/
/* */
void AxisTicksSettingsView::update() {
	sel_TicksPos->blockSignals( true );
	sel_TicksPos->updateItems( settings->getType() );
	sel_TicksPos->setPos( settings->getTkPos() );
	disableWidget();
	sel_TicksPos->blockSignals( false );
	sel_MajInc->setText( QString::number( settings->getTkmaInc() ) );
	sel_MajSize->setValue( settings->getTkmaSize() );
	sel_MinInc->setText( QString::number( settings->getTkmiInc() ) );
	sel_MinSize->setValue( settings->getTkmiSize() );
}
/**********************************************/
/**********************************************/
/* */
void AxisTicksSettingsView::connectModifiers() {
	connect( sel_TicksPos, &AxisTicksPosSelector::changed,
			 this, &AxisTicksSettingsView::posChanged );
	connect( sel_MajInc, &LineEditor::editingFinished,
			 this, &AxisTicksSettingsView::setMajInc );
	connect( sel_MajSize, &IntegerSelector::changed,
			 settings, &AxisSettings::setTkmaSize );
	connect( sel_MinInc, &LineEditor::editingFinished,
			 this, &AxisTicksSettingsView::setMinInc );
	connect( sel_MinSize, &IntegerSelector::changed,
			 settings, &AxisSettings::setTkmiSize );
}
/**********************************************/
/**********************************************/
/* */
void AxisTicksSettingsView::disconnectModifiers() {
	if ( settings != nullptr ) {
		disconnect( sel_TicksPos, &AxisTicksPosSelector::changed,
					this, &AxisTicksSettingsView::posChanged );
		disconnect( sel_MajInc, &LineEditor::editingFinished,
					this, &AxisTicksSettingsView::setMajInc );
		disconnect( sel_MajSize, &IntegerSelector::changed,
					settings, &AxisSettings::setTkmaSize );
		disconnect( sel_MinInc, &LineEditor::editingFinished,
					this, &AxisTicksSettingsView::setMinInc );
		disconnect( sel_MinSize, &IntegerSelector::changed,
					settings, &AxisSettings::setTkmiSize );
	}
}
/**********************************************/
/**********************************************/
/* */
void AxisTicksSettingsView::disableWidget() {
	if ( settings->getTkPos() == Axis::NONE ) {
		//		lab_MajTicks->setDisabled( true );
		//		lab_MinTicks->setDisabled( true );
		//		lab_Inc->setDisabled( true );
		//		sel_MajInc->setDisabled( true );
		//		sel_MinInc->setDisabled( true );
		lab_Size->setDisabled( true );
		sel_MajSize->setDisabled( true );
		sel_MinSize->setDisabled( true );
	}
	else {
		//		lab_MajTicks->setDisabled( false );
		//		lab_MinTicks->setDisabled( false );
		//		lab_Inc->setDisabled( false );
		//		sel_MajInc->setDisabled( false );
		//		sel_MinInc->setDisabled( false );
		lab_Size->setDisabled( false );
		sel_MajSize->setDisabled( false );
		sel_MinSize->setDisabled( false );
	}
}
/**********************************************/
/**********************************************/
/* */
void AxisTicksSettingsView::updatePosList() {
	sel_TicksPos->updateItems( settings->getType() );
	sel_TicksPos->setPos( settings->getTkPos() );
}
/**********************************************/
/**********************************************/
/* */
void AxisTicksSettingsView::posChanged( const Axis::Pos& pos ) {
	settings->setTkPos( pos );
	disableWidget();
}
/**********************************************/
/**********************************************/
/* */
void AxisTicksSettingsView::setMajInc() {
	QString bound = sel_MajInc->text();
	bool ok;
	double ret = bound.toDouble(&ok);
	if ( ok ) {
		settings->setTkmaInc( ret );
	}
	else {
		sel_MajInc->setText( QString::number( settings->getTkmaInc() ) );
	}
}
/**********************************************/
/**********************************************/
/* */
void AxisTicksSettingsView::setMinInc() {
	QString bound = sel_MinInc->text();
	bool ok;
	double ret = bound.toDouble(&ok);
	if ( ok ) {
		settings->setTkmiInc( ret );
	}
	else {
		sel_MinInc->setText( QString::number( settings->getTkmiInc() ) );
	}
}
/**********************************************/
/**********************************************/
/* */
void AxisTicksSettingsView::updateIcons( Icons* icons ) {
Q_UNUSED(icons)
}
/**********************************************/
/**********************************************/
/* */
void AxisTicksSettingsView::updateLang( Lang* lang ) {
	lab_Ticks->setText( lang->get("axisTicksSets") );
	lab_TicksPos->setText( lang->get("position") + " :" );
	sel_TicksPos->updateLang( lang );
	lab_MajTicks->setText( lang->get("majTicks") );
	lab_MinTicks->setText( lang->get("minTicks") );
	lab_Inc->setText( lang->get("increment") + " :" );
	lab_Size->setText( lang->get("size") + " :" );
}
/**********************************************/
/**********************************************/
