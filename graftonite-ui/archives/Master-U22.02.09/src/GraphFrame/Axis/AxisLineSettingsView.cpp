#include "AxisLineSettingsView.h"
/**********************************************/
/**********************************************/
/* */
AxisLineSettingsView::AxisLineSettingsView( QWidget* parent )
	: ScrollArea( parent )
{
	FormLayout* lay_lineSets = new FormLayout;
	lay_lineSets->addRow( lab_Line );
	lay_lineSets->addRow( lab_Style, sel_Style );
	sel_Width->setDimension( 0, 1000 );
	lay_lineSets->addRow( lab_Width, sel_Width );
	lay_lineSets->addRow( lab_Color, sel_Color );
	lay_lineSets->addRow( lab_Cap, sel_Cap );
	lay_lineSets->addItem( new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding) );

	QWidget* graphWid = new QWidget;
	graphWid->setMinimumWidth( 220 );
	graphWid->setLayout( lay_lineSets );
	setWidget( graphWid );
//	setLayout( lay_lineSets );
}
/**********************************************/
/**********************************************/
/* */
void AxisLineSettingsView::setSettings( AxisSettings* sets ) {
	disconnectModifiers();

	settings = sets;

	connectModifiers();
//	update();
}
/**********************************************/
/**********************************************/
/* */
void AxisLineSettingsView::connectModifiers() {
	connect( sel_Style, &LineStyleSelector::changed,
			 this, &AxisLineSettingsView::styleChanged );
	connect( sel_Width, &IntegerSelector::changed,
			 settings, &AxisSettings::setLnWidth );
	connect( sel_Color, &ColorSelector::changed,
			 settings, &AxisSettings::setLnColor );
	connect( sel_Cap, &LineCapSelector::changed,
			 settings, &AxisSettings::setLnCap );
}
/**********************************************/
/**********************************************/
/* */
void AxisLineSettingsView::disconnectModifiers() {
	if ( settings != nullptr )
	{
		disconnect( sel_Style, &LineStyleSelector::changed,
				 this, &AxisLineSettingsView::styleChanged );
		disconnect( sel_Width, &IntegerSelector::changed,
				 settings, &AxisSettings::setLnWidth );
		disconnect( sel_Color, &ColorSelector::changed,
				 settings, &AxisSettings::setLnColor );
		disconnect( sel_Cap, &LineCapSelector::changed,
				 settings, &AxisSettings::setLnCap );
	}
}
/**********************************************/
/**********************************************/
/* */
void AxisLineSettingsView::blockChildrenSignals( bool arg ) {
	sel_Style->blockSignals( arg );
	sel_Width->blockSignals( arg );
	sel_Color->blockSignals( arg );
	sel_Cap->blockSignals( arg );
}
/**********************************************/
/**********************************************/
/* */
void AxisLineSettingsView::update() {
	blockChildrenSignals( true );
	sel_Style->setStyle( settings->getLnStyle() );
	disableWidget();
	sel_Width->setValue( settings->getLnWidth() );
	sel_Color->setColor( settings->getLnColor() );
	sel_Cap->setStyle( settings->getLnCap() );
	blockChildrenSignals( false );
}
/**********************************************/
/**********************************************/
/* */
void AxisLineSettingsView::disableWidget() {
	if ( settings->getLnStyle() == Qt::NoPen ) {
		lab_Width->setDisabled( true );
		sel_Width->setDisabled( true );
		lab_Color->setDisabled( true );
		sel_Color->setDisabled( true );
		lab_Cap->setDisabled( true );
		sel_Cap->setDisabled( true );
	}
	else {
		lab_Width->setDisabled( false );
		sel_Width->setDisabled( false );
		lab_Color->setDisabled( false );
		sel_Color->setDisabled( false );
		lab_Cap->setDisabled( false );
		sel_Cap->setDisabled( false );
	}
}
/**********************************************/
/**********************************************/
/* */
void AxisLineSettingsView::styleChanged( const Qt::PenStyle& style ) {
	settings->setLnStyle( style );
	disableWidget();
}
/**********************************************/
/**********************************************/
/* */
void AxisLineSettingsView::updateIcons( Icons* icons ) {
Q_UNUSED(icons)
}
/**********************************************/
/**********************************************/
/* */
void AxisLineSettingsView::updateLang( Lang* lang ) {
	lab_Line->setText( lang->get("axisLineSets") );
	lab_Style->setText( lang->get("style") + " :" );
	sel_Style->updateLang( lang );
	lab_Width->setText( lang->get("width") + " :" );
	lab_Color->setText( lang->get("color") + " :" );
	lab_Cap->setText( lang->get("cap") + " :" );
	sel_Cap->updateLang( lang );
}
/**********************************************/
/**********************************************/
