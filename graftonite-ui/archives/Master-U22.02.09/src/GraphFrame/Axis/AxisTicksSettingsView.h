#ifndef AXISTICKSSETTINGSVIEW_H
#define AXISTICKSSETTINGSVIEW_H

#include "../AxisSettings.h"
#include "Commons/theme.h"
#include "Commons/lang.h"
#include "Commons/TabWidget.h"
#include "Commons/ScrollArea.h"
#include "Commons/FormLayout.h"
#include "Commons/GridLayout.h"
#include "Commons/SettingLabel.h"
#include "Commons/LineEditor.h"
#include "Commons/IntegerSelector.h"
#include "../Misc/AxisTicksPosSelector.h"
#include "../Misc/LineStyleSelector.h"
#include "../Misc/LineCapSelector.h"
#include "../Misc/ColorButton.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class AxisTicksSettingsView
		: public ScrollArea
{
	Q_OBJECT
private:
	AxisSettings* settings = nullptr;
	SettingLabel* lab_Ticks = new SettingLabel;
	SettingLabel* lab_TicksPos = new SettingLabel;
	AxisTicksPosSelector* sel_TicksPos = new AxisTicksPosSelector;
	SettingLabel* lab_MajTicks = new SettingLabel;
	SettingLabel* lab_MinTicks = new SettingLabel;
	SettingLabel* lab_Inc = new SettingLabel;
	SettingLabel* lab_Size = new SettingLabel;
	LineEditor* sel_MajInc = new LineEditor;
	IntegerSelector* sel_MajSize = new IntegerSelector;
	LineEditor* sel_MinInc = new LineEditor;
	IntegerSelector* sel_MinSize = new IntegerSelector;

public:
	AxisTicksSettingsView( QWidget* parent = nullptr );
	void setSettings( AxisSettings* sets );
	void updateIcons( Icons* icons );
	void updateLang( Lang* lang );

public slots:
	void update();
	void connectModifiers();
	void disconnectModifiers();
	void disableWidget();
	void updatePosList();
	void posChanged( const Axis::Pos& pos );
	void setMajInc();
	void setMinInc();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // AXISTICKSSETTINGSVIEW_H
