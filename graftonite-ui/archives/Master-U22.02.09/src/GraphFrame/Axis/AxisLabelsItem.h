#ifndef AXISLABELSITEM_H
#define AXISLABELSITEM_H

#include <QGraphicsItem>

#include "../AxisSettings.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class AxisLabelsItem : public QGraphicsItemGroup
{
	QGraphicsRectItem* boundingItem = nullptr;
public:
	~AxisLabelsItem();
	AxisLabelsItem( QGraphicsItem* parent );

public slots:
	void update( AxisSettings* settings, const QRectF tickRect );
	void mousePressEvent( QGraphicsSceneMouseEvent* event ) override;
	void hoverEnterEvent( QGraphicsSceneHoverEvent* event ) override;
	void hoverLeaveEvent( QGraphicsSceneHoverEvent* event ) override;

};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // AXISLABELSITEM_H
