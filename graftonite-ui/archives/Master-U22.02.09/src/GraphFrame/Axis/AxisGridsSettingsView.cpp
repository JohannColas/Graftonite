#include "AxisGridsSettingsView.h"
/**********************************************/
/**********************************************/
/* */
AxisGridsSettingsView::AxisGridsSettingsView( QWidget* parent )
	: ScrollArea( parent )
{
	GridLayout* lay0 = new GridLayout;

	FormLayout* layPos = new FormLayout;
	layPos->addRow( lab_Grids );
	layPos->addRow( lab_GridsPos, sel_GridsPos );
	lay0->addLayout( layPos, 0, 0, 1, 4 );

	lay0->addWidget( lab_MajGrids, 1, 1 );
	lay0->addWidget( lab_MinGrids, 1, 2 );

	lay0->addWidget( lab_Style, 2, 0 );
	lay0->setAlignment( lab_Style, Qt::AlignRight );
	lay0->addWidget( sel_MajStyle, 2, 1 );
	lay0->addWidget( sel_MinStyle, 2, 2 );
	lay0->addWidget( lab_Width, 3, 0 );
	lay0->setAlignment( lab_Width, Qt::AlignRight );
	lay0->addWidget( sel_MajWidth, 3, 1 );
	lay0->addWidget( sel_MinWidth, 3, 2 );
	lay0->addWidget( lab_Color, 4, 0 );
	lay0->setAlignment( lab_Color, Qt::AlignRight );
	lay0->addWidget( sel_MajColor, 4, 1 );
	lay0->addWidget( sel_MinColor, 4, 2 );
	lay0->addItem( new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum), 1, 3, 5, 1 );
	lay0->addItem( new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding), 5, 0, 1, 2 );
//	lab_Cap->setText( "Cap :" );
//	lay0->addWidget( lab_Cap, 5, 0 );
//	lay0->setAlignment( lab_Cap, Qt::AlignRight );
//	lay0->addWidget( sel_MajCap, 5, 1 );
//	lay0->addWidget( sel_MinCap, 5, 2 );

	QWidget* graphWid = new QWidget;
	graphWid->setMinimumWidth( 220 );
	graphWid->setLayout( lay0 );
	setWidget( graphWid );
//	setLayout( lay0 );
}
/**********************************************/
/**********************************************/
/* */
void AxisGridsSettingsView::setSettings( AxisSettings* sets ) {
	disconnectModifiers();

	settings = sets;

	connectModifiers();

	//update();
}
/**********************************************/
/**********************************************/
/* */
void AxisGridsSettingsView::update() {
	sel_GridsPos->blockSignals( true );
	sel_GridsPos->updateItems( settings->getType() );
	sel_GridsPos->setPos( settings->getGdPos() );
	disableWidget();
	sel_GridsPos->blockSignals( false );
	sel_MajStyle->setStyle( settings->getGdmaStyle() );
	sel_MajWidth->setValue( settings->getGdmaWidth() );
	sel_MajColor->setColor( settings->getGdmaColor() );
//	sel_MajCap->setCurrentCapStyle( settings->getGdmaCap() );
	sel_MinStyle->setStyle( settings->getGdmiStyle() );
	sel_MinWidth->setValue( settings->getGdmiWidth() );
	sel_MinColor->setColor( settings->getGdmiColor() );
//	sel_MinCap->setCurrentCapStyle( settings->getGdmaCap() );
}
/**********************************************/
/**********************************************/
/* */
void AxisGridsSettingsView::connectModifiers() {
	connect( sel_GridsPos, &AxisGridsPosSelector::changed,
			 this, &AxisGridsSettingsView::posChanged );
	connect( sel_MajStyle, &LineStyleSelector::changed,
			 settings, &AxisSettings::setGdmaStyle );
	connect( sel_MajWidth, &IntegerSelector::changed,
			 settings, &AxisSettings::setGdmaWidth );
	connect( sel_MajColor, &ColorSelector::changed,
			 settings, &AxisSettings::setGdmaColor );
//	connect( sel_MajCap, &LineCapSelector::capStyleChanged,
//			 settings, &AxisSettings::set );
	connect( sel_MinStyle, &LineStyleSelector::changed,
			 settings, &AxisSettings::setGdmiStyle );
	connect( sel_MinWidth, &IntegerSelector::changed,
			 settings, &AxisSettings::setGdmiWidth );
	connect( sel_MinColor, &ColorSelector::changed,
			 settings, &AxisSettings::setGdmiColor );
//	connect( sel_MinCap, &LineCapSelector::capStyleChanged,
//			 settings, &AxisSettings::set );
}
/**********************************************/
/**********************************************/
/* */
void AxisGridsSettingsView::disconnectModifiers() {
	if ( settings != nullptr )
	{
		disconnect( sel_GridsPos, &AxisGridsPosSelector::changed,
					this, &AxisGridsSettingsView::posChanged );
		disconnect( sel_MajStyle, &LineStyleSelector::changed,
					settings, &AxisSettings::setGdmaStyle );
		disconnect( sel_MajWidth, &IntegerSelector::changed,
					settings, &AxisSettings::setGdmaWidth );
		disconnect( sel_MajColor, &ColorSelector::changed,
					settings, &AxisSettings::setGdmaColor );
		//	disconnect( sel_MajCap, &LineCapSelector::capStyleChanged,
		//			 settings, &AxisSettings::set );
		disconnect( sel_MinStyle, &LineStyleSelector::changed,
					settings, &AxisSettings::setGdmiStyle );
		disconnect( sel_MinWidth, &IntegerSelector::changed,
					settings, &AxisSettings::setGdmiWidth );
		disconnect( sel_MinColor, &ColorSelector::changed,
					settings, &AxisSettings::setGdmiColor );
		//	disconnect( sel_MinCap, &LineCapSelector::capStyleChanged,
		//			 settings, &AxisSettings::set );
	}
}
/**********************************************/
/**********************************************/
/* */
void AxisGridsSettingsView::disableWidget() {
	if ( settings->getGdPos() == Axis::NONE )
	{
		lab_MajGrids->setDisabled( true );
		lab_MinGrids->setDisabled( true );
		lab_Style->setDisabled( true );
		sel_MajStyle->setDisabled( true );
		sel_MinStyle->setDisabled( true );
		lab_Width->setDisabled( true );
		sel_MajWidth->setDisabled( true );
		sel_MinWidth->setDisabled( true );
		lab_Color->setDisabled( true );
		sel_MajColor->setDisabled( true );
		sel_MinColor->setDisabled( true );
	}
	else
	{
		lab_MajGrids->setDisabled( false );
		lab_MinGrids->setDisabled( false );
		lab_Style->setDisabled( false );
		sel_MajStyle->setDisabled( false );
		sel_MinStyle->setDisabled( false );
		lab_Width->setDisabled( false );
		sel_MajWidth->setDisabled( false );
		sel_MinWidth->setDisabled( false );
		lab_Color->setDisabled( false );
		sel_MajColor->setDisabled( false );
		sel_MinColor->setDisabled( false );
	}
}
/**********************************************/
/**********************************************/
/* */
void AxisGridsSettingsView::updatePosList() {
	sel_GridsPos->updateItems( settings->getType() );
	sel_GridsPos->setPos( settings->getTkPos() );
}
/**********************************************/
/**********************************************/
/* */
void AxisGridsSettingsView::posChanged( const Axis::Pos& pos ) {
	settings->setGdPos( pos );
	disableWidget();
}
/**********************************************/
/**********************************************/
/* */
void AxisGridsSettingsView::updateIcons( Icons* icons ) {
	Q_UNUSED(icons)
}
/**********************************************/
/**********************************************/
/* */
void AxisGridsSettingsView::updateLang( Lang* lang ) {
	lab_Grids->setText( lang->get("axisGridsSets") );
	lab_GridsPos->setText( lang->get("position") + " :" );
	sel_GridsPos->updateLang( lang );
	lab_MajGrids->setText( lang->get("majGrids") );
	lab_MinGrids->setText( lang->get("minGrids") );
	lab_Style->setText( lang->get("style") + " :" );
	sel_MajStyle->updateLang( lang );
	sel_MinStyle->updateLang( lang );
	lab_Width->setText( lang->get("width") + " :" );
	lab_Color->setText( lang->get("color") + " :" );
}
/**********************************************/
/**********************************************/
