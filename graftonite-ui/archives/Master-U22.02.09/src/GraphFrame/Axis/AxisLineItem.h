#ifndef AXISLINEITEM_H
#define AXISLINEITEM_H

#include <QGraphicsLineItem>

#include "../AxisSettings.h"
#include "../Misc/GraphMisc.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class AxisLineItem : public QObject, public QGraphicsLineItem
{
protected:
   double pos = 0;

public:
	~AxisLineItem();
	AxisLineItem( QGraphicsItem* parent );
	void update( AxisSettings* settings, GraphRect& rect );
	//void update( AxisSettings* settings, LayerSettings* layerSettings );
	//double pos() { return _pos; };
//	QRect rect()
//	{
//		return { line().x1(),
//					line().y1(),
//					line().x2() - line().x1(),
//					line().y2() - line().y1() };
//	};
	double getPos() const;
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // AXISLINEITEM_H
