#ifndef AXISLABELSSETTINGSVIEW_H
#define AXISLABELSSETTINGSVIEW_H

#include <QWidget>

#include "../AxisSettings.h"
#include "Commons/TabWidget.h"
#include "Commons/ScrollArea.h"
#include "Commons/FormLayout.h"
#include "Commons/GridLayout.h"
#include "Commons/SettingLabel.h"
#include "Commons/LineEditor.h"
#include "Commons/IntegerSelector.h"
#include "Commons/DoubleSelector.h"
#include "../Misc/AxisLabelsPosSelector.h"
#include "../Misc/AnchorSelector.h"
#include "../Misc/NumberFormatSelector.h"
#include "../Misc/NumberFormatOptionSelector.h"
#include "../Misc/NumberDecimalSepSelector.h"
#include "../Misc/TextCapSelector.h"
#include "Commons/ColorSelector.h"
#include <QFontComboBox>
#include "Commons/theme.h"
#include "Commons/lang.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class AxisLabelsSettingsView
		: public ScrollArea
{
	Q_OBJECT
private:
	AxisSettings* settings = nullptr;
	SettingLabel* lab_Labels = new SettingLabel;
	SettingLabel* lab_Pos = new SettingLabel;
	AxisLabelsPosSelector* sel_Pos = new AxisLabelsPosSelector;
	SettingLabel* lab_Anchor = new SettingLabel;
	AnchorSelector* sel_Anchor = new AnchorSelector;
	SettingLabel* lab_Offset = new SettingLabel;
	DoubleSelector* sel_XOffset = new DoubleSelector;
	DoubleSelector* sel_YOffset = new DoubleSelector;
	SettingLabel* lab_Angle = new SettingLabel;
	DoubleSelector* sel_Angle = new DoubleSelector;
	// Number Formatting
	SettingLabel* lab_NbFrt = new SettingLabel;
	SettingLabel* lab_Format = new SettingLabel;
	NumberFormatSelector* sel_Format = new NumberFormatSelector;
	SettingLabel* lab_FormatOpt = new SettingLabel;
	NumberFormatOptionSelector* sel_FormatOpt = new NumberFormatOptionSelector;
	SettingLabel* lab_NbPrec = new SettingLabel;
	IntegerSelector* sel_NbPrec = new IntegerSelector;
	SettingLabel* lab_NbDecSep = new SettingLabel;
	NumberDecimalSepSelector* sel_NbDecSep = new NumberDecimalSepSelector;
	SettingLabel* lab_Prefix = new SettingLabel;
	LineEditor* sel_Prefix = new LineEditor;
	SettingLabel* lab_Suffix = new SettingLabel;
	LineEditor* sel_Suffix = new LineEditor;
	// Font
	SettingLabel* lab_Font = new SettingLabel;
	QFontComboBox* sel_FtFamily = new QFontComboBox;
	DoubleSelector* sel_FtSize = new DoubleSelector;
	TextCapSelector* sel_FtCap = new TextCapSelector;
	ColorSelector* sel_FtColor = new ColorSelector;
	QPushButton* sel_FtItalic = new QPushButton;
	QPushButton* sel_FtBold = new QPushButton;
	QPushButton* sel_FtUnder = new QPushButton;

public:
	AxisLabelsSettingsView( QWidget* parent = nullptr );
	void setSettings( AxisSettings* sets );
	void updateIcons( Icons* icons );
	void updateLang( Lang* lang );

public slots:
	void update();
	void disableWidget();
	void connectModifiers();
	void disconnectModifiers();
	void updatePosList();
	void posChanged( const Axis::Pos& pos );
	void numberFormatChanged( const Axis::NumberFormat& nbFrmt );
	void updateFormatOption();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // AXISLABELSSETTINGSVIEW_H
