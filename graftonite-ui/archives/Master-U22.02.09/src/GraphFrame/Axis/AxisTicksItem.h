#ifndef AXISTICKSITEM_H
#define AXISTICKSITEM_H

#include <QGraphicsItem>
#include <QGraphicsItemGroup>

#include "../Misc/GraphMisc.h"
#include "../AxisSettings.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class AxisTicksItem : public QGraphicsItemGroup
{
protected:
	bool isMajor = false;

public:
	~AxisTicksItem();
	AxisTicksItem( QGraphicsItem* parent, bool isMajor );

public slots:
	void update( AxisSettings* settings, double linePos );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // AXISTICKSITEM_H
