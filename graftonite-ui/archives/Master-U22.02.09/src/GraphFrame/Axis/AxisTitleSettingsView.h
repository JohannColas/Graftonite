#ifndef AXISTITLESETTINGSVIEW_H
#define AXISTITLESETTINGSVIEW_H

#include "../AxisSettings.h"
#include "Commons/ScrollArea.h"
#include "Commons/FormLayout.h"
#include "Commons/GridLayout.h"
#include "Commons/SettingLabel.h"
#include "Commons/LineEditor.h"
#include "Commons/IntegerSelector.h"
#include "Commons/DoubleSelector.h"
#include "Commons/ColorSelector.h"
#include "../Misc/TextEditor.h"
#include "../Misc/AxisTitlePosSelector.h"
#include "../Misc/AnchorSelector.h"
#include "../Misc/TextAlignSelector.h"
#include "../Misc/TextCapSelector.h"
#include <QFontComboBox>
#include "Commons/theme.h"
#include "Commons/lang.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class AxisTitleSettingsView
		: public ScrollArea
{
	Q_OBJECT
private:
	AxisSettings* settings = nullptr;
	SettingLabel* lab_title = new SettingLabel;
	SettingLabel* lab_Pos = new SettingLabel;
	AxisTitlePosSelector* sel_Pos = new AxisTitlePosSelector;
	SettingLabel* lab_Anchor = new SettingLabel;
	AnchorSelector* sel_Anchor = new AnchorSelector;
	SettingLabel* lab_Offset = new SettingLabel;
	DoubleSelector* sel_XOffset = new DoubleSelector;
	DoubleSelector* sel_YOffset = new DoubleSelector;
	SettingLabel* lab_Angle = new SettingLabel;
	DoubleSelector* sel_Angle = new DoubleSelector;
	// Font
	SettingLabel* lab_Font = new SettingLabel;
	QFontComboBox* sel_FtFamily = new QFontComboBox;
	IntegerSelector* sel_FtSize = new IntegerSelector;
	TextCapSelector* sel_FtCap = new TextCapSelector;
	ColorSelector* sel_FtColor = new ColorSelector;
	QPushButton* sel_FtItalic = new QPushButton;
	QPushButton* sel_FtBold = new QPushButton;
	QPushButton* sel_FtUnder = new QPushButton;
	QPushButton* sel_FtExponant = new QPushButton;
	QPushButton* sel_FtIndice = new QPushButton;
	TextAlignSelector* sel_Align = new TextAlignSelector;
	TextEditor* edt_Title = new TextEditor;

public:
	AxisTitleSettingsView( QWidget* parent = nullptr );
	void setSettings( AxisSettings* sets );

public slots:
	void update();
	void disableWidget();
	void connectModifiers();
	void disconnectModifiers();
	void blockChildrenSignals( QObject* obj, bool checked );
	void updatePosList();
	void posChanged( const Axis::Pos& pos );
	void titleTextChanged();
	void titleText_cursorPosChanged();
	void ftExponantChanged( bool checked );
	void ftIndiceChanged( bool checked );
	void updateIcons( Icons* icons );
	void updateLang( Lang* lang );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // AXISTITLESETTINGSVIEW_H
