#ifndef AXISTITLEITEM_H
#define AXISTITLEITEM_H

#include <QGraphicsItem>

#include "../AxisSettings.h"
#include "../Misc/GraphTextItem.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class AxisTitleItem : public GraphTextItem
{
	Q_OBJECT
public:
	~AxisTitleItem();
	AxisTitleItem( QGraphicsItem* parent );

public slots:
	void update( AxisSettings* settings, const QRectF& lineRect, const QRectF& ticksRect, const QRectF& labelsRect );

};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // AXISTITLEITEM_H
