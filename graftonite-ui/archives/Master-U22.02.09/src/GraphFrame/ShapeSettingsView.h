#ifndef SHAPESETTINGSVIEW_H
#define SHAPESETTINGSVIEW_H

#include <QWidget>
#include "Commons/theme.h"
#include "Commons/lang.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class ShapeSettingsView : public QWidget
{
	Q_OBJECT

public:
	explicit ShapeSettingsView(QWidget *parent = nullptr);
	~ShapeSettingsView();
	void updateIcons( Icons* icons );
	void updateLang( Lang* lang );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SHAPESETTINGSVIEW_H
