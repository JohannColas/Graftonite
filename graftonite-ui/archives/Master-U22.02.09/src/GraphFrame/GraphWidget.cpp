#include "GraphWidget.h"
/**********************************************/
/**********************************************/
/* */
GraphWidget::~GraphWidget() {
    delete settings;
}

GraphWidget::GraphWidget( QWidget *parent )
	: QSplitter( parent )
{
//	lay_main->setMargin( 0 );
	lay_main->addWidget( lab_title );
	lay_main->addWidget( wid_graphView );
	wid_main->setLayout( lay_main );

	addWidget( wid_main );
	addWidget( wid_toolBar );
	setSizes( {this->width()-250, 250} );

	settings = new GraphSettings();
	settings->readSettings( "./aaa.grf" );
	wid_graphView->update( settings );
	wid_toolBar->setSettings( settings );

	connect( settings, &GraphSettings::changed,
			 this, &GraphWidget::updateGraphView );
	connect( wid_toolBar, &GraphSettingsView::layerAdded,
			 settings, &GraphSettings::addLayer );
	connect( wid_toolBar, &GraphSettingsView::layerDeleted,
			 settings, &GraphSettings::deleteLayer );
	connect( wid_toolBar, &GraphSettingsView::axisAdded,
			 settings, &GraphSettings::addAxis );
	connect( wid_toolBar, &GraphSettingsView::axisDeleted,
			 settings, &GraphSettings::deleteAxis );
	connect( wid_toolBar, &GraphSettingsView::curveAdded,
			 settings, &GraphSettings::addCurve );
	connect( wid_toolBar, &GraphSettingsView::curveDeleted,
			 settings, &GraphSettings::deleteCurve );
	connect( wid_toolBar, &GraphSettingsView::shapeAdded,
			 settings, &GraphSettings::addShape );
	connect( wid_toolBar, &GraphSettingsView::shapeDeleted,
			 settings, &GraphSettings::deleteShape );
	connect( wid_graphView, &GraphView::saveGraph,
			 settings, &GraphSettings::saveSettings );
	hide();
}
/**********************************************/
/**********************************************/
/* */
GraphSettings *GraphWidget::getSettings() const {
    return settings;
}
/**********************************************/
/**********************************************/
/* */
void GraphWidget::setSettings(GraphSettings *value) {
    settings = value;
}
/**********************************************/
/**********************************************/
/* */
void GraphWidget::updateGraphView() {
	wid_graphView->update( settings );
	wid_toolBar->update();
}
/**********************************************/
/**********************************************/
/* */
void GraphWidget::updateIcons( Icons* icons ) {
	wid_toolBar->updateIcons( icons );
}
/**********************************************/
/**********************************************/
/* */
void GraphWidget::updateLang( Lang* lang ) {
	wid_toolBar->updateLang( lang );
}
/**********************************************/
/**********************************************/
/* */
//void GraphWidget::addLayer()
//{
//	settings->addLayer();
//}
/**********************************************/
/**********************************************/
/* */
//void GraphWidget::deleteLayer( QString id )
//{
//	settings->deleteLayer( id );
//}
/**********************************************/
/**********************************************/
/* */
//void GraphWidget::addAxis()
//{
//	settings->addAxis();
//}
/**********************************************/
/**********************************************/
/* */
//void GraphWidget::deleteAxis( QString id )
//{
//	settings->deleteAxis( id );
//}
/**********************************************/
/**********************************************/
/* */
//void GraphWidget::addCurve()
//{
//	settings->addCurve();
//}
/**********************************************/
/**********************************************/
/* */
//void GraphWidget::deleteCurve( QString id )
//{
//	settings->deleteCurve( id );
//}
/**********************************************/
/**********************************************/
