#ifndef AXISITEM_H
#define AXISITEM_H

#include <QObject>

#include "GraphItem.h"
#include "LayerItem.h"

#include "GraphSettings.h"
#include "AxisSettings.h"
#include "Axis/AxisLineItem.h"
#include "Axis/AxisTicksItem.h"
#include "Axis/AxisLabelsItem.h"
#include "Axis/AxisTitleItem.h"
#include "Axis/AxisGridsItem.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class AxisItem : public QObject
{
    Q_OBJECT
private:
//	GraphSettings* graphSettings = nullptr;
	AxisSettings* settings = nullptr;
	AxisLineItem* line = nullptr;
	AxisTicksItem* majorTicks = nullptr;
	AxisTicksItem* minorTicks = nullptr;
	AxisLabelsItem* labels = nullptr;
	AxisTitleItem* title = nullptr;
	AxisGridsItem* majorGrids = nullptr;
	AxisGridsItem* minorGrids = nullptr;
public:
    ~AxisItem();
	AxisItem( QGraphicsItem *parent,
			  QGraphicsItem *majGdparent,
			  QGraphicsItem *minGdparent );
	AxisItem( LayerItem *layer);
	void update( AxisSettings* settings, GraphRect& layerRect );


public slots:

};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // AXISITEM_H
