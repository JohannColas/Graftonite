#ifndef AXISSETTINGS_H
#define AXISSETTINGS_H

#include <QSettings>
#include <QColor>
#include <QPen>
#include <QFont>
#include <cmath>

#include "Misc/GraphMisc.h"
#include "Axis.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class AxisSettings : public QObject
{
	Q_OBJECT
protected:
	QString id = "Axis 1";
	QString layerID = "Layer 1";
	// Axis Type
	Axis::Type type = Axis::XAxis;
	Axis::Pos pos = Axis::BOTTOM;
	int posOffset = 0;
	double posAt = 0;
	Axis::Scale scale = Axis::LINEAR;
	double scaleOffset = 0;
	double minBound = 0;
	double maxBound = 1;
	double coef = 0;
	double cons = 0;
	// Title parameters of Axis
	Axis::Pos tlPos = Axis::Pos::BOTTOM;
	Axis::Pos tlAnchor = Axis::Pos::TOPCENTER;
	QPointF tlOffset = {0, 0};
	double tlAngle = 0;
	QString tlText = "";
	// Line parameters of Axis
	Qt::PenStyle lnStyle = Qt::SolidLine;
	QColor lnColor = QColor(0, 0, 0, 255);
	int lnWidth = 2;
	Qt::PenCapStyle lnCap = Qt::RoundCap;
	// Ticks parameters of Axis
	Axis::Pos tkPosition = Axis::Pos::BOTTOM;
	QList<double> tkmaPos;
	QList<double> tkmiPos;
	double tkmaInc = 0.1;
	double tkmiInc = 0.05;
	int tkmaSize = 10;
	int tkmiSize = 5;
	// Labels parameters of Axis
	QStringList labels;
	Axis::Pos lblPos = Axis::Pos::BOTTOM;
	Axis::Pos lblAnchor = Axis::Pos::TOPCENTER;
	QPointF lblOffset = {0, 0};
	double lblAngle = 0;
	Axis::NumberFormat lblNumberFormat = Axis::DEFAULTNF;
	char lblNumberFormatOption = -1;
	char lblNumberDecimalSeparator = 0;
	short lblNumberPrecision = 2;
	QFont lblFont = QFont("Arial", 12);
	QString lblFtFamily = "Arial";
	double lblFtSize = 12;
	QColor lblFtColor = Qt::black;
	QString lblPrefix = "";
	QString lblSuffix = "";
	QStringList customLbl;
	// Grids parameters of Axis
	Axis::Pos gdPos = Axis::BOTH;
	//	Axis::Pos gdmiPos = Axis::Pos::BOTH;
	Qt::PenStyle gdmaStyle = Qt::SolidLine;
	QColor gdmaColor = QColor(50, 50, 50, 255);
	int gdmaWidth = 2;
	//	Qt::PenCapStyle gdmaCap = Qt::RoundCap;
	Qt::PenStyle gdmiStyle = Qt::DashLine;
	QColor gdmiColor = QColor(20, 20, 20, 255);
	int gdmiWidth = 1;
	//	Qt::PenCapStyle gdmiCap = Qt::RoundCap;
	// Arrows parameters of Axis

public:
	AxisSettings();
	QString getID() const {
		return id;
	}
	QString getLayerID() const {
		return layerID;
	}
	Axis::Type getType() const {
		return type;
	}
	void configToNewType();
	Axis::Pos getPos() const {
		return pos;
	}
	int getPosOffset() const {
		return posOffset;
	}
	double getPosAt() const {
		return posAt;
	}
	Axis::Scale getScale() const {
		return scale;
	}
	double getScaleOffset() const {
		return scaleOffset;
	}
	double getMinBound() const {
		return minBound;
	}
	double getMaxBound() const {
		return maxBound;
	}
	// Line Settings
	QPen getLinePen() const {
		QPen pen;
		pen.setStyle( getLnStyle() );
		pen.setColor( getLnColor() );
		pen.setWidth( getLnWidth() );
		pen.setCapStyle( getLnCap() );
		return pen;
	}
	Qt::PenStyle getLnStyle() const {
		return lnStyle;
	}
	int getLnWidth() const {
		return lnWidth;
	}
	QColor getLnColor() const {
		return lnColor;
	}
	Qt::PenCapStyle getLnCap() const {
		return lnCap;
	}
	// Ticks Settings
	Axis::Pos getTkPos() const {
		return tkPosition;
	}
	void calculateTkPos( GraphRect& layerRect );
	double getTkmaInc() const {
		return tkmaInc;
	}
	QList<double> getTkmaPos() const {
		return tkmaPos;
	}
	void setTkmaPos( QList<double> value ) {
		tkmaPos = value;
		emit changed();
	}
	int getTkmaSize() const {
		return tkmaSize;
	}
	double getTkmiInc() const {
		return tkmiInc;
	}
	QList<double> getTkmiPos() const {
		return tkmiPos;
	}
	void setTkmiPos( QList<double> value ) {
		tkmiPos = value;
		emit changed();
	}
	int getTkmiSize() const {
		return tkmiSize;
	}
	// Labels Settings
	Axis::Pos getLblPos() const {
		return lblPos;
	}
	QStringList getLabels() const {
		return labels;
	}
	Axis::Pos getLblAnchor() const {
		return lblAnchor;
	}
	QPointF getLblOffset() const {
		return lblOffset;
	}
	double getLblAngle() const {
		return lblAngle;
	}
	Axis::NumberFormat getLblNumberFormat() const {
		return lblNumberFormat;
	}
	char getLblNumberFormatOption() const {
		return lblNumberFormatOption;
	}
	char getLblNumberDecimalSeparator() const {
		return lblNumberDecimalSeparator;
	}
	short getLblNumberPrecision() const {
		return lblNumberPrecision;
	}
	QFont getLblFont() const {
		return lblFont;
	}
	QColor getLblFtColor() const {
		return lblFtColor;
	}
	QString getLblPrefix() const {
		return lblPrefix;
	}
	QString getLblSuffix() const {
		return lblSuffix;
	}
	QStringList getCustomLbl() const {
		return customLbl;
	}
	// Title Settings
	Axis::Pos getTlPos() const {
		return tlPos;
	}
	Axis::Pos getTlAnchor() const {
		return tlAnchor;
	}
	QPointF getTlOffset() const {
		return tlOffset;
	}
	double getTlAngle() const {
		return tlAngle;
	}
	QString getTlText() const {
		return tlText;
	}
	// Grids Settings
	Axis::Pos getGdPos() const {
		return gdPos;
	}
	QPen getMajorGridsPen() {
		QPen pen;
		pen.setStyle( getGdmaStyle() );
		pen.setColor( getGdmaColor() );
		pen.setWidth( getGdmaWidth() );
		pen.setCapStyle( getLnCap() );
		return pen;
	}
	Qt::PenStyle getGdmaStyle() const {
		return gdmaStyle;
	}
	int getGdmaWidth() const {
		return gdmaWidth;
	}
	QColor getGdmaColor() const {
		return gdmaColor;
	}
	QPen getMinorGridsPen() {
		QPen pen;
		pen.setStyle( getGdmiStyle() );
		pen.setColor( getGdmiColor() );
		pen.setWidth( getGdmiWidth() );
		pen.setCapStyle( getLnCap() );
		return pen;
	}
	Qt::PenStyle getGdmiStyle() const {
		return gdmiStyle;
	}
	int getGdmiWidth() const {
		return gdmiWidth;
	}
	QColor getGdmiColor() const {
		return gdmiColor;
	}


public slots:
	// General
	void setID( const QString &value ) {
		id = value;
		emit changed();
	}
	void setLayerID( const QString& value ) {
		layerID = value;
		emit changed();
	}
	void setType( const Axis::Type& value ) {
		type = value;
		configToNewType();
		emit changed();
	}
	void setPos( const Axis::Pos& value ) {
		pos = value;
		emit changed();
	}
	void setPosOffset( int value ) {
		posOffset = value;
		emit changed();
	}
	void setPosAt( double value ) {
		posAt = value;
		emit changed();
	}
	void setScale( const Axis::Scale& value ) {
		scale = value;
		emit changed();
	}
	void setScaleOffset( double value ) {
		scaleOffset = value;
		emit changed();
	}
	void setMinBound( double value ) {
		minBound = value;
		emit changed();
	}
	void setMaxBound( double value ) {
		maxBound = value;
		emit changed();
	}
	// Line
	void setLnStyle( const Qt::PenStyle& value ) {
		lnStyle = value;
		emit changed();
	}
	void setLnColor( const QColor &value ) {
		lnColor = value;
		emit changed();
	}
	void setLnWidth( int value ) {
		lnWidth = value;
		emit changed();
	}
	void setLnCap( const Qt::PenCapStyle& value ) {
		lnCap = value;
		emit changed();
	}
	// Ticks
	void setTkPos( const Axis::Pos value ) {
		tkPosition = value;
		emit changed();
	}
	void setTkmaInc( double value ) {
		tkmaInc = value;
		emit changed();
	}
	void setTkmiInc( double value ) {
		tkmiInc = value;
		emit changed();
	}
	void setTkmaSize( int value ) {
		tkmaSize = value;
		emit changed();
	}
	void setTkmiSize( int value ) {
		tkmiSize = value;
		emit changed();
	}
	//Labels
	void setLblPos( const Axis::Pos value ) {
		lblPos = value;
		emit changed();
	}
	void setLblAnchor( const Axis::Pos value ) {
		lblAnchor = value;
		emit changed();
	}
	void setLblOffset( const QPointF& value ) {
		lblOffset = value;
		emit changed();
	}
	void setLblXOffset( double val ) {
		lblOffset.setX( val );
		emit changed();
	}
	void setLblYOffset( double val ) {
		lblOffset.setY( val );
		emit changed();
	}
	void setLblAngle( double value ) {
		lblAngle = value;
		emit changed();
	}
	void setLblNumberFormat( const Axis::NumberFormat& value ) {
		lblNumberFormat = value;
		emit changed();
	}
	void setLblNumberFormatOption( char value ) {
		lblNumberFormatOption = value;
		emit changed();
	}
	void setLblNumberDecimalSeparator( char value ) {
		lblNumberDecimalSeparator = value;
		emit changed();
	}
	void setLblNumberPrecision( short value ) {
		lblNumberPrecision = value;
		emit changed();
	}
	void setLblNumberPrec( int value ) {
		lblNumberPrecision = (short)value;
		emit changed();
	}
	void setLblFtCap( const QFont::Capitalization& arg1 ) {
		lblFont.setCapitalization( arg1 );
		emit changed();
	}
	void setLblFtItalic( bool checked ) {
		lblFont.setItalic( checked );
		emit changed();
	}
	void setLblFtBold( bool checked ) {
		lblFont.setBold( checked );
		emit changed();
	}
	void setLblFtUnder( bool checked ) {
		lblFont.setUnderline( checked );
		emit changed();
	}
	void setLblFont( const QFont& val ) {
		lblFont = val;
		emit changed();
	}
	void setLblFtFamily( const QString& value ) {
		lblFtFamily = value;
		lblFont.setFamily( value );
		emit changed();
	}
	void setLblFtSize( double value ) {
		lblFtSize = value;
		lblFont.setPointSize( value );
		emit changed();
	}
	void setLblFtColor( const QColor& value ) {
		lblFtColor = value;
		emit changed();
	}
	void setLblPrefix( const QString& value ) {
		lblPrefix = value;
		emit changed();
	}
	void setLblSuffix( const QString& value ) {
		lblSuffix = value;
		emit changed();
	}
	void setCustomLbl( const QStringList& value ) {
		customLbl = value;
		emit changed();
	}
	// Title
	void setTlPos( const Axis::Pos& value ) {
		tlPos = value;
		emit changed();
	}
	void setTlAnchor( const Axis::Pos& value ) {
		tlAnchor = value;
		emit changed();
	}
	void setTlOffset( const QPointF& value ) {
		tlOffset = value;
		emit changed();
	}
	void setTlXOffset( double val )
	{ tlOffset.setX( val );
		emit changed(); }
	void setTlYOffset( double val )
	{ tlOffset.setY( val );
		emit changed(); }
	void setTlAngle( double value ) {
		tlAngle = value;
		emit changed();
	}
	void setTlText( const QString &value ) {
		tlText = value;
		emit changed();
	}
	// Grids
	void setGdPos( const Axis::Pos& value ) {
		gdPos = value;
		emit changed();
	}
	void setGdmaStyle( const Qt::PenStyle& value ) {
		gdmaStyle = value;
		emit changed();
	}
	void setGdmaWidth( int value ) {
		gdmaWidth = value;
		emit changed();
	}
	void setGdmaColor( const QColor &value ) {
		gdmaColor = value;
		emit changed();
	}
	void setGdmiStyle( const Qt::PenStyle& value ) {
		gdmiStyle = value;
		emit changed();
	}
	void setGdmiWidth( int value ) {
		gdmiWidth = value;
		emit changed();
	}
	void setGdmiColor( const QColor &value ) {
		gdmiColor = value;
		emit changed();
	}
	// Read and Save Settings
	void readSettings( QSettings* setsFile );
	void saveSettings( QSettings* setsFile );

signals:
	void changed();
	void idChanged( QString, QString );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // AXISSETTINGS_H
