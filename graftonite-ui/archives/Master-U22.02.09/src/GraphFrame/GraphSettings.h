#ifndef GRAPHSETTINGS_H
#define GRAPHSETTINGS_H

#include <QColor>
#include <QPen>
#include <QSettings>

#include "LayerSettings.h"
#include "AxisSettings.h"
#include "CurveSettings.h"
#include "LegendSettings.h"
#include "ShapeSettings.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class GraphSettings : public QObject
{
    Q_OBJECT
protected:
	//
	QSettings* setsFile = 0;
    // Sizes parameters of Graph
	int width = 800;
    int height = 500;
    // Border parameters of Graph
    Qt::PenStyle bdStyle = Qt::NoPen;
	int bdWidth = 2;
	QColor bdColor = QColor(0, 0, 0, 255);
    Qt::PenJoinStyle bdJoin = Qt::MiterJoin;
    int bdMiter = 8;
    // Background parameters of Graph
    QColor bkColor = QColor(255, 255, 255, 255);
    // Global Font parameters of Graph
    QString ftFamily = "Arial";
    int ftSize = 14;
    QColor ftColor = QColor(0, 0, 0, 255);
    int ftWeight = 0;
    bool ftItalic = false;
    // Layer Settings List
    QList<LayerSettings*> layersSettings;
    // Axis Settings List
    QList<AxisSettings*> axesSettings;
    // Curve Settings List
	QList<CurveSettings*> curvesSettings;
	// Shape Settings List
	QList<LegendSettings*> legendsSettings;
	// Shape Settings List
	QList<ShapeSettings*> shapesSettings;

public:
    ~GraphSettings();
    GraphSettings();
	int getWidth() const {
		return width;
	}
	int getHeight() const {
		return height;
	}
	QPen getBorderPen() const {
		QPen pen;
		pen.setStyle( getBdStyle() );
		pen.setColor( getBdColor() );
		pen.setWidth( getBdWidth() );
		pen.setJoinStyle( getBdJoin() );
		pen.setMiterLimit( getBdMiter() );
		return pen;
	}
	Qt::PenStyle getBdStyle() const {
		return bdStyle;
	}
	int getBdWidth() const  {
		return bdWidth;
	}
	QColor getBdColor() const {
		return bdColor;
	}
	Qt::PenJoinStyle getBdJoin() const {
		return bdJoin;
	}
	int getBdMiter() const {
		return bdMiter;
	}
	QColor getBkColor() const {
		return bkColor;
	}
	QString getFtFamily() const {
		return ftFamily;
	}
	int getFtSize() const {
		return ftSize;
	}
	QColor getFtColor() const {
		return ftColor;
	}
	int getFtWeight() const {
		return ftWeight;
	}
	bool getFtItalic() const {
		return ftItalic;
	}
	QList<LayerSettings*> getLayersSettings() const {
		return layersSettings;
	}
	void setLayersSettings( const QList<LayerSettings*> &value ) {
		layersSettings = value;
	}
	void addLayer();
	void deleteLayer( QString id );
	QList<AxisSettings*> getAxesSettings() const {
		return axesSettings;
	}
	void setAxesSettings( const QList<AxisSettings*> &value ) {
		axesSettings = value;
	}
	void addAxis();
	void deleteAxis( QString id );
	QList<CurveSettings*> getCurvesSettings() const {
		return curvesSettings;
	}
	void setCurvesSettings( const QList<CurveSettings*> &value ) {
		curvesSettings = value;
	}
	void addCurve();
	void deleteCurve( QString id );
	QList<LegendSettings*> getLegendsSettings() const {
		return legendsSettings;
	}
	void setLegendsSettings( const QList<LegendSettings*> &value ) {
		legendsSettings = value;
	}
	void addLegend();
	void deleteLegend( QString id );
	QList<ShapeSettings*> getShapesSettings() const {
		return shapesSettings;
	}
	void setShapesSettings( const QList<ShapeSettings*> &value ) {
		shapesSettings = value;
	}
	void addShape();
	void deleteShape( QString id );

public slots:
	void updateLayersID( QString oldID, QString newID );
	void updateAxesID( QString oldID, QString newID );
	void setWidth( int value ) {
		width = value;
		emit changed();
	}
	void setHeight( int value ) {
		height = value;
		emit changed();
	}
	void setBdStyle( Qt::PenStyle value ) {
		bdStyle = value;
		emit changed();
	}
	void setBdWidth( int value ) {
		bdWidth = value;
		emit changed();
	}
	void setBdColor( const QColor &value ) {
		bdColor = value;
		emit changed();
	}
	void setBdJoin( Qt::PenJoinStyle value ) {
		bdJoin = value;
		emit changed();
	}
	void setBdMiter( int value ) {
		bdMiter = value;
		emit changed();
	}
	void setBkColor( const QColor &value ) {
		bkColor = value;
		emit changed();
	}
	void setFtFamily( const QString &value ) {
		ftFamily = value;
		emit changed();
	}
	void setFtSize( int value ) {
		ftSize = value;
		emit changed();
	}
	void setFtColor( const QColor &value ) {
		ftColor = value;
		emit changed();
	}
	void setFtWeight( int value ) {
		ftWeight = value;
		emit changed();
	}
	void setFtItalic( bool value ) {
		ftItalic = value;
		emit changed();
	}
	void readSettings( const QString& path );
	void saveSettings();

signals:
    void changed();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // GRAPHSETTINGS_H
