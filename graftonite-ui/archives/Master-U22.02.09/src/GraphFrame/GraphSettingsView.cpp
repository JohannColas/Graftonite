#include "GraphSettingsView.h"

#include <QColorDialog>
#include <QAction>
/**********************************************/
/**********************************************/
/* */
GraphSettingsView::~GraphSettingsView()
{
}
/**********************************************/
/**********************************************/
/* */
GraphSettingsView::GraphSettingsView( QWidget *parent ) :
	QWidget(parent)
{

	settings = new GraphSettings();

	FormLayout* lay_graphSets = new FormLayout;
	sel_Width->setDimension();
	lay_graphSets->addRow( lab_Width, sel_Width );
	sel_Height->setDimension();
	lay_graphSets->addRow( lab_Height, sel_Height );
	lay_graphSets->addRow( lab_Background );
	lay_graphSets->addRow( lab_BkColor, sel_BkColor );
	lay_graphSets->addRow( lab_Borders );
	lay_graphSets->addRow( lab_BdStyle, sel_BdStyle );
	sel_BdWidth->setBorderWidth();
	lay_graphSets->addRow( lab_BdWidth, sel_BdWidth );
	lay_graphSets->addRow( lab_BdColor, sel_BdColor );
	lay_graphSets->addRow( lab_BdJoin, sel_BdJoin );
	lay_graphSets->addRow( lab_GlobalFont );
	lay_graphSets->addRow( lab_GbFont, new QWidget );
	lay_graphSets->addRow( lab_GbFtColor, new QWidget );
	QWidget* graphWid = new QWidget;
	graphWid->setMinimumWidth( 250 );
	graphWid->setLayout( lay_graphSets );
	wid_graphSettings->setWidget( graphWid );
//	tab_settings->addTab( wid_graphSettings, "" );
	tab_settings->addWidget( "", wid_graphSettings);


	GridLayout* lay_layerSets = new GridLayout;
	lay_layerSets->addWidget( lab_layerSettings, 0, 0 );
	pub_addLayer->setSVG( "icons/Dark Theme/add.svg" );
	lay_layerSets->addWidget( pub_addLayer, 0, 1 );
	pub_deleteLayer->setSVG( "icons/Dark Theme/delete.svg" );
	lay_layerSets->addWidget( pub_deleteLayer, 0, 2 );
	lst_layerSettings->setMaximumHeight( 200 );
	lst_layerSettings->setMinimumHeight( 150 );
	lay_layerSets->addWidget( lst_layerSettings, 1, 0, 1, 3 );
	wid_layerSettings->setDisabled( true );
	lay_layerSets->addWidget( wid_layerSettings, 2, 0, 1, 3 );
	QWidget* wid_layerSets = new QWidget;
	wid_layerSets->setLayout( lay_layerSets );
//	tab_settings->addTab( wid_layerSets, "" );
	tab_settings->addWidget( "", wid_layerSets);

	GridLayout* lay_axisSets = new GridLayout;
	lay_axisSets->addWidget( lab_axesSettings, 0, 0 );
	pub_addAxis->setSVG( "icons/Dark Theme/add.svg" );
	lay_axisSets->addWidget( pub_addAxis, 0, 1 );
	pub_deleteAxis->setSVG( "icons/Dark Theme/delete.svg" );
	lay_axisSets->addWidget( pub_deleteAxis, 0, 2 );
	lst_axesSettings->setMaximumHeight( 200 );
	lst_axesSettings->setMinimumHeight( 150 );
	lay_axisSets->addWidget( lst_axesSettings, 1, 0, 1, 3 );
	wid_axisSettings->setDisabled( true );
	lay_axisSets->addWidget( wid_axisSettings, 2, 0, 1, 3 );
	QWidget* wid_axisSets = new QWidget;
	wid_axisSets->setLayout( lay_axisSets );
//	tab_settings->addTab( wid_axisSets, "" );
	tab_settings->addWidget( "", wid_axisSets);

	GridLayout* lay_curveSets = new GridLayout;
	lay_curveSets->addWidget( lab_curveSettings, 0, 0 );
	pub_addCurve->setSVG( "icons/Dark Theme/add.svg" );
	lay_curveSets->addWidget( pub_addCurve, 0, 1 );
	pub_deleteCurve->setSVG( "icons/Dark Theme/delete.svg" );
	lay_curveSets->addWidget( pub_deleteCurve, 0, 2 );
	lst_curveSettings->setMaximumHeight( 200 );
	lst_curveSettings->setMinimumHeight( 150 );
	lay_curveSets->addWidget( lst_curveSettings, 1, 0, 1, 3 );
	wid_curveSettings->setDisabled( true );
	lay_curveSets->addWidget( wid_curveSettings, 2, 0, 1, 3 );
	QWidget* wid_curveSets = new QWidget;
	wid_curveSets->setLayout( lay_curveSets );
//	tab_settings->addTab( wid_curveSets, "" );
	tab_settings->addWidget( "", wid_curveSets);

	GridLayout* lay_legendSets = new GridLayout;
	lay_legendSets->addWidget( lab_legendSettings, 0, 0 );
	pub_addLegend->setSVG( "icons/Dark Theme/add.svg" );
	lay_legendSets->addWidget( pub_addLegend, 0, 1 );
	pub_deleteLegend->setSVG( "icons/Dark Theme/delete.svg" );
	lay_legendSets->addWidget( pub_deleteLegend, 0, 2 );
	lst_legendSettings->setMaximumHeight( 200 );
	lst_legendSettings->setMinimumHeight( 150 );
	lay_legendSets->addWidget( lst_legendSettings, 1, 0, 1, 3 );
	wid_legendSettings->setDisabled( true );
	lay_legendSets->addWidget( wid_legendSettings, 2, 0, 1, 3 );
	QWidget* wid_legendSets = new QWidget;
	wid_legendSets->setLayout( lay_legendSets );
//	tab_settings->addTab( wid_legendSets, "" );
	tab_settings->addWidget( "", wid_legendSets);

	GridLayout* lay_shapeSets = new GridLayout;
	lay_shapeSets->addWidget( lab_shapeSettings, 0, 0 );
	pub_addShape->setSVG( "icons/Dark Theme/add.svg" );
	lay_shapeSets->addWidget( pub_addShape, 0, 1 );
	pub_deleteShape->setSVG( "icons/Dark Theme/delete.svg" );
	lay_shapeSets->addWidget( pub_deleteShape, 0, 2 );
	lst_shapeSettings->setMaximumHeight( 200 );
	lst_shapeSettings->setMinimumHeight( 150 );
	lay_shapeSets->addWidget( lst_shapeSettings, 1, 0, 1, 3 );
	wid_shapeSettings->setDisabled( true );
	lay_shapeSets->addWidget( wid_shapeSettings, 2, 0, 1, 3 );
	QWidget* wid_shapesSets = new QWidget;
	wid_shapesSets->setLayout( lay_shapeSets );
//	tab_settings->addTab( wid_shapesSets, "" );
	tab_settings->addWidget( "", wid_shapesSets);

	tab_settings->changeTab(0);

	GridLayout* lay_main = new GridLayout;
//	lay_main->setMargin( 0 );
	lay_main->addWidget( tab_settings );
	this->setLayout( lay_main );
	setMinimumWidth( 250 );
	setMaximumWidth( 500 );

	update();
}
/**********************************************/
/**********************************************/
/* */
GraphSettings *GraphSettingsView::getSettings() const
{
	return settings;
}
/**********************************************/
/**********************************************/
/* */
void GraphSettingsView::setSettings( GraphSettings *sets )
{
	disconnectModifiers();

	settings = sets;

	connectModifiers();

	update();
}
/**********************************************/
/**********************************************/
/* */
void GraphSettingsView::connectModifiers()
{
	// Connecting the Selectors to the Settings
	connect( sel_Width, &IntegerSelector::changed,
			 settings, &GraphSettings::setWidth );
	connect( sel_Height, &IntegerSelector::changed,
			 settings, &GraphSettings::setHeight );
	connect( sel_BkColor, &ColorSelector::changed,
			 settings, &GraphSettings::setBkColor );
	connect( sel_BdStyle, &LineStyleSelector::changed,
			 settings, &GraphSettings::setBdStyle );
	connect( sel_BdWidth, &IntegerSelector::changed,
			 settings, &GraphSettings::setBdWidth );
	connect( sel_BdColor, &ColorSelector::changed,
			 settings, &GraphSettings::setBdColor );
	connect( sel_BdJoin, &LineJoinSelector::changed,
			 settings, &GraphSettings::setBdJoin );
	connect( lst_layerSettings, &QListWidget::currentTextChanged,
			 this, &GraphSettingsView::updateLayerWidget );
	connect( pub_addLayer, &QPushButton::clicked,
			 this, &GraphSettingsView::addLayer );
	connect( pub_deleteLayer, &QPushButton::clicked,
			 this, &GraphSettingsView::deleteLayer );
	connect( lst_axesSettings, &QListWidget::currentTextChanged,
			 this, &GraphSettingsView::updateAxisWidget );
	connect( pub_addAxis, &QPushButton::clicked,
			 this, &GraphSettingsView::addAxis );
	connect( pub_deleteAxis, &QPushButton::clicked,
			 this, &GraphSettingsView::deleteAxis );
	connect( lst_curveSettings, &QListWidget::currentTextChanged,
			 this, &GraphSettingsView::updateCurveWidget);
	connect( pub_addCurve, &QPushButton::clicked,
			 this, &GraphSettingsView::addCurve );
	connect( pub_deleteCurve, &QPushButton::clicked,
			 this, &GraphSettingsView::deleteLayer );
	connect( lst_legendSettings, &QListWidget::currentTextChanged,
			 this, &GraphSettingsView::updateLegendWidget);
	connect( pub_addLegend, &QPushButton::clicked,
			 this, &GraphSettingsView::addLegend );
	connect( pub_deleteLegend, &QPushButton::clicked,
			 this, &GraphSettingsView::deleteLegend );
	connect( lst_shapeSettings, &QListWidget::currentTextChanged,
			 this, &GraphSettingsView::updateShapeWidget);
	connect( pub_addShape, &QPushButton::clicked,
			 this, &GraphSettingsView::addShape );
	connect( pub_deleteShape, &QPushButton::clicked,
			 this, &GraphSettingsView::deleteShape );
}
/**********************************************/
/**********************************************/
/* */
void GraphSettingsView::disconnectModifiers()
{
	if ( settings != nullptr )
	{
		disconnect( sel_Width, &IntegerSelector::changed,
					settings, &GraphSettings::setWidth );
		disconnect( sel_Height, &IntegerSelector::changed,
					settings, &GraphSettings::setHeight );
		disconnect( sel_BkColor, &ColorSelector::changed,
					settings, &GraphSettings::setBkColor );
		disconnect( sel_BdStyle, &LineStyleSelector::changed,
					settings, &GraphSettings::setBdStyle );
		disconnect( sel_BdWidth, &IntegerSelector::changed,
					settings, &GraphSettings::setBdWidth );
		disconnect( sel_BdColor, &ColorSelector::changed,
					settings, &GraphSettings::setBdColor );
		disconnect( sel_BdJoin, &LineJoinSelector::changed,
					settings, &GraphSettings::setBdJoin );
		disconnect( lst_layerSettings, &QListWidget::currentTextChanged,
					this, &GraphSettingsView::updateLayerWidget );
		disconnect( pub_addLayer, &QPushButton::clicked,
					this, &GraphSettingsView::addLayer );
		disconnect( pub_deleteLayer, &QPushButton::clicked,
					this, &GraphSettingsView::deleteLayer );
		disconnect( lst_axesSettings, &QListWidget::currentTextChanged,
					this, &GraphSettingsView::updateAxisWidget );
		disconnect( pub_addAxis, &QPushButton::clicked,
					this, &GraphSettingsView::addAxis );
		disconnect( pub_deleteAxis, &QPushButton::clicked,
					this, &GraphSettingsView::deleteAxis );
		disconnect( lst_curveSettings, &QListWidget::currentTextChanged,
					this, &GraphSettingsView::updateCurveWidget);
		disconnect( pub_addCurve, &QPushButton::clicked,
					this, &GraphSettingsView::addCurve );
		disconnect( pub_deleteCurve, &QPushButton::clicked,
					this, &GraphSettingsView::deleteLayer );
		disconnect( lst_legendSettings, &QListWidget::currentTextChanged,
					this, &GraphSettingsView::updateLegendWidget);
		disconnect( pub_addLegend, &QPushButton::clicked,
					this, &GraphSettingsView::addLegend );
		disconnect( pub_deleteLegend, &QPushButton::clicked,
					this, &GraphSettingsView::deleteLegend );
		disconnect( lst_shapeSettings, &QListWidget::currentTextChanged,
					this, &GraphSettingsView::updateShapeWidget);
		disconnect( pub_addShape, &QPushButton::clicked,
					this, &GraphSettingsView::addShape );
		disconnect( pub_deleteShape, &QPushButton::clicked,
					this, &GraphSettingsView::deleteShape );
	}
}
/**********************************************/
/**********************************************/
/* */
void GraphSettingsView::blockChildrenSignals( QObject* obj, bool block )
{
	for( auto elm : obj->children() )
	{
		elm->blockSignals( block );
		blockChildrenSignals( elm, block );
	}
}
/**********************************************/
/**********************************************/
/* */
void GraphSettingsView::update()
{
	sel_Width->setValue( settings->getWidth() );
	sel_Height->setValue( settings->getHeight() );
	sel_BkColor->setColor( settings->getBkColor()  );
	sel_BdStyle->setStyle( settings->getBdStyle() );
	sel_BdWidth->setValue( settings->getBdWidth() );
	sel_BdColor->setColor( settings->getBdColor() );
	sel_BdJoin->setStyle( settings->getBdJoin() );

	lst_layerSettings->clear();
	for( auto elm : settings->getLayersSettings() )
	{
		lst_layerSettings->addItem( elm->getID() );
	}
	updateLayersList();
	lst_axesSettings->clear();
	for( auto elm : settings->getAxesSettings() )
	{
		lst_axesSettings->addItem( elm->getID() );
	}
	updateAxesList();
	lst_curveSettings->clear();
	for( auto elm : settings->getCurvesSettings() )
	{
		lst_curveSettings->addItem( elm->getID() );
	}
	lst_legendSettings->clear();
	for( auto elm : settings->getLegendsSettings() )
	{
		lst_legendSettings->addItem( elm->getID() );
	}
	lst_shapeSettings->clear();
	for( auto elm : settings->getShapesSettings() )
	{
		lst_shapeSettings->addItem( elm->getID() );
	}
}
/**********************************************/
/**********************************************/
/* */
void GraphSettingsView::addLayer()
{
	emit layerAdded();
	updateLayersList();
}
/**********************************************/
/**********************************************/
/* */
void GraphSettingsView::deleteLayer()
{
	emit layerDeleted( currentLayerID );
	updateLayersList();
	updateAxesList();
}
/**********************************************/
/**********************************************/
/* */
void GraphSettingsView::updateLayersList()
{
	//	ui->lsLayers->clear();
	//	for( auto elm : settings->getLayersSettings() )
	//	{
	//		ui->lsLayers->addItem( elm->getID() );
	//	}
	QStringList layers;
	for( auto elm : settings->getLayersSettings() )
	{
		layers.append( elm->getID() );
	}
	wid_axisSettings->setComboLayers( layers );
}
/**********************************************/
/**********************************************/
/* */
void GraphSettingsView::updateLayerWidget( const QString& layer )
{
	wid_layerSettings->setDisabled( false );
	for( auto elm : settings->getLayersSettings() )
	{
		if( elm->getID() == layer )
		{
			//			wid_layerSettings->blockChildrenSignals( wid_layerSettings, true );
			wid_layerSettings->setSettings( elm );
			currentLayerID = elm->getID();
			break;
		}
	}
}
/**********************************************/
/**********************************************/
/* */
void GraphSettingsView::updateAxisWidget( const QString& axis )
{
	wid_axisSettings->setDisabled( false );
	for( auto elm : settings->getAxesSettings() )
	{
		if( elm->getID() == axis )
		{
			wid_axisSettings->setSettings( elm );
			currentAxisID = elm->getID();
			break;
		}
	}
}
/**********************************************/
/**********************************************/
/* */
void GraphSettingsView::updateCurveWidget( const QString& curve )
{
	wid_curveSettings->setDisabled( false );
	for( auto elm : settings->getCurvesSettings() )
	{
		if( elm->getID() == curve )
		{
			wid_curveSettings->setSettings( elm );
			currentCurveID = elm->getID();
			break;
		}
	}
}
/**********************************************/
/**********************************************/
/* */
void GraphSettingsView::updateLegendWidget( const QString& legend )
{
	wid_legendSettings->setDisabled( false );
	for( auto elm : settings->getLegendsSettings() )
	{
		if( elm->getID() == legend )
		{
			wid_legendSettings->setSettings( elm );
			currentLegendID = elm->getID();
			break;
		}
	}
}
/**********************************************/
/**********************************************/
/* */
void GraphSettingsView::updateShapeWidget( const QString& shape )
{
	wid_shapeSettings->setDisabled( false );
	for( auto elm : settings->getShapesSettings() )
	{
		if( elm->getID() == shape )
		{
			//			wid_shapeSettings->setSettings( elm );
			currentAxisID = elm->getID();
			break;
		}
	}
}
/**********************************************/
/**********************************************/
/* */
void GraphSettingsView::addAxis()
{
	emit axisAdded();
	updateAxesList();
}
/**********************************************/
/**********************************************/
/* */
void GraphSettingsView::deleteAxis()
{
	emit axisDeleted( currentAxisID );
	updateAxesList();
}
/**********************************************/
/**********************************************/
/* */
void GraphSettingsView::updateAxesList()
{
	//	ui->lsAxes->clear();
	//	for( auto elm : settings->getAxesSettings() )
	//	{
	//		ui->lsAxes->addItem( elm->getID() );
	//	}
	//	QStringList axes;
	//	for( auto elm : settings->getLayersSettings() )
	//	{
	//		axes.append( elm->getID() );
	//	}
	//	ui->axisSetsView->setComboLayers( axes );
}
/**********************************************/
/**********************************************/
/* */
void GraphSettingsView::addCurve()
{
	emit curveAdded();
	updateCurvesList();
}
/**********************************************/
/**********************************************/
/* */
void GraphSettingsView::deleteCurve()
{
	emit curveDeleted( currentCurveID );
	updateCurvesList();
}
/**********************************************/
/**********************************************/
/* */
void GraphSettingsView::updateCurvesList()
{

}
/**********************************************/
/**********************************************/
/* */
void GraphSettingsView::addLegend()
{
	emit legendAdded();
	updateLegendsList();
}
/**********************************************/
/**********************************************/
/* */
void GraphSettingsView::deleteLegend()
{
	emit legendDeleted( currentLegendID );
	updateLegendsList();
}
/**********************************************/
/**********************************************/
/* */
void GraphSettingsView::updateLegendsList()
{

}
/**********************************************/
/**********************************************/
/* */
void GraphSettingsView::addShape()
{
	emit shapeAdded();
	//	updateCurvesList();
}
/**********************************************/
/**********************************************/
/* */
void GraphSettingsView::deleteShape()
{
	emit shapeDeleted( currentShapeID );
	//	updateCurvesList();
}
/**********************************************/
/**********************************************/
/* */
void GraphSettingsView::updateShapesList()
{

}
/**********************************************/
/**********************************************/
/* */
void GraphSettingsView::updateIcons( Icons* icons ) {
	wid_axisSettings->updateIcons( icons );
}
/**********************************************/
/**********************************************/
/* */
void GraphSettingsView::updateLang( Lang* lang ) {
//	tab_settings->setTabText( 0, lang->get("graph") );
	tab_settings->setTabName( 0, lang->get("graph") );
	lab_Width->setText( lang->get("width") + " :" );
	lab_Height->setText( lang->get("height") + " :"  );
	lab_Background->setText( lang->get("background") + " :" );
	lab_BkColor->setText( lang->get("color") + " :" );
	lab_Borders->setText( lang->get("borders") );
	lab_BdStyle->setText( lang->get("style") + " :" );
	sel_BdStyle->updateLang( lang );
	lab_BdWidth->setText( lang->get("width") + " :" );
	lab_BdColor->setText( lang->get("color") + " :" );
	lab_BdJoin->setText( lang->get("join") + " :" );
	sel_BdJoin->updateLang( lang );
	lab_GlobalFont->setText( lang->get("font") );
	lab_GbFont->setText( lang->get("family") + " :" );
	lab_GbFtColor->setText( lang->get("color") + " :" );

	// Layers
//	tab_settings->setTabText( 1, lang->get("layers") );
	tab_settings->setTabName( 1, lang->get("layers") );
	lab_layerSettings->setText( lang->get("layersSets") );
	pub_addLayer->setText( lang->get("add") );
	pub_deleteLayer->setText( lang->get("delete") );
//	lst_layerSettings->
	wid_layerSettings->updateLang( lang );

	// Axes
//	tab_settings->setTabText( 2, lang->get("axes") );
	tab_settings->setTabName( 2, lang->get("axes") );
	lab_axesSettings->setText( lang->get("axesSets") );
	pub_addAxis->setText( lang->get("add") );
	pub_deleteAxis->setText( lang->get("delete") );
	wid_axisSettings->updateLang( lang );

	// Curves
//	tab_settings->setTabText( 3, lang->get("curves") );
	tab_settings->setTabName( 3, lang->get("curves") );
	lab_curveSettings->setText( lang->get("curvesSets") );
	pub_addCurve->setText( lang->get("add") );
	pub_deleteCurve->setText( lang->get("delete") );
	wid_curveSettings->updateLang( lang );

	// Legends
//	tab_settings->setTabText( 4, lang->get("legends") );
	tab_settings->setTabName( 4, lang->get("legends") );
	lab_legendSettings->setText( lang->get("legendsSets") );
	pub_addLegend->setText( lang->get("add") );
	pub_deleteLegend->setText( lang->get("delete") );
	wid_legendSettings->updateLang( lang );

	// Shapes
//	tab_settings->setTabText( 5, lang->get("shapes") );
	tab_settings->setTabName( 5, lang->get("shapes") );
	lab_shapeSettings->setText( lang->get("shapesSets") );
	pub_addShape->setText( lang->get("add") );
	pub_deleteShape->setText( lang->get("delete") );
	wid_shapeSettings->updateLang( lang );



//	GridLayout* lay_axisSets = new GridLayout;
//	lab_axesSettings->setText( "Axes Settings" );
//	lay_axisSets->addWidget( lab_axesSettings, 0, 0 );
//	pub_addAxis->setText( "Add" );
//	pub_addAxis->setSVG( "icons/Dark Theme/add.svg" );
//	lay_axisSets->addWidget( pub_addAxis, 0, 1 );
//	pub_deleteAxis->setText( "Delete" );
//	pub_deleteAxis->setSVG( "icons/Dark Theme/delete.svg" );
//	lay_axisSets->addWidget( pub_deleteAxis, 0, 2 );
//	lst_axesSettings->setMaximumHeight( 200 );
//	lay_axisSets->addWidget( lst_axesSettings, 1, 0, 1, 3 );
//	wid_axisSettings->setDisabled( true );
//	lay_axisSets->addWidget( wid_axisSettings, 2, 0, 1, 3 );
//	QWidget* wid_axisSets = new QWidget;
//	wid_axisSets->setLayout( lay_axisSets );
//	tab_settings->addTab( wid_axisSets, "Axes" );

//	GridLayout* lay_curveSets = new GridLayout;
//	lab_curveSettings->setText( "Curves Settings" );
//	lay_curveSets->addWidget( lab_curveSettings, 0, 0 );
//	pub_addCurve->setText( "Add" );
//	pub_addCurve->setSVG( "icons/Dark Theme/add.svg" );
//	lay_curveSets->addWidget( pub_addCurve, 0, 1 );
//	pub_deleteCurve->setText( "Delete" );
//	pub_deleteCurve->setSVG( "icons/Dark Theme/delete.svg" );
//	lay_curveSets->addWidget( pub_deleteCurve, 0, 2 );
//	lst_curveSettings->setMaximumHeight( 200 );
//	lay_curveSets->addWidget( lst_curveSettings, 1, 0, 1, 3 );
//	wid_curveSettings->setDisabled( true );
//	lay_curveSets->addWidget( wid_curveSettings, 2, 0, 1, 3 );
//	QWidget* wid_curveSets = new QWidget;
//	wid_curveSets->setLayout( lay_curveSets );
//	tab_settings->addTab( wid_curveSets, "Curves" );

//	GridLayout* lay_shapeSets = new GridLayout;
//	lab_shapeSettings->setText( "Shapes Settings" );
//	lay_shapeSets->addWidget( lab_shapeSettings, 0, 0 );
//	pub_addShape->setText( "Add" );
//	pub_addShape->setSVG( "icons/Dark Theme/add.svg" );
//	lay_shapeSets->addWidget( pub_addShape, 0, 1 );
//	pub_deleteShape->setText( "Delete" );
//	pub_deleteShape->setSVG( "icons/Dark Theme/delete.svg" );
//	lay_shapeSets->addWidget( pub_deleteShape, 0, 2 );
//	lst_shapeSettings->setMaximumHeight( 200 );
//	lay_shapeSets->addWidget( lst_shapeSettings, 1, 0, 1, 3 );
//	wid_shapeSettings->setDisabled( true );
//	lay_shapeSets->addWidget( wid_shapeSettings, 2, 0, 1, 3 );
//	QWidget* wid_shapesSets = new QWidget;
//	wid_shapesSets->setLayout( lay_shapeSets );
//	tab_settings->addTab( wid_shapesSets, "Shapes" );

//	GridLayout* lay_main = new GridLayout;
//	lay_main->setMargin( 0 );
//	lay_main->addWidget( tab_settings );
//	//	tab_settings->setParent( this );
	//	this->setLayout( lay_main );
}
/**********************************************/
/**********************************************/
/* */
void GraphSettingsView::resizeEvent(QResizeEvent* event)
{
	QWidget::resizeEvent( event );

//	qDebug() << width();
	if ( width() < 350 ) {
		pub_addLayer->hideText();
		pub_deleteLayer->hideText();
		pub_addAxis->hideText();
		pub_deleteAxis->hideText();
		pub_addCurve->hideText();
		pub_deleteCurve->hideText();
		pub_addLegend->hideText();
		pub_deleteLegend->hideText();
		pub_addShape->hideText();
		pub_deleteShape->hideText();
	}
	else  {
		pub_addLayer->showText();
		pub_deleteLayer->showText();
		pub_addAxis->showText();
		pub_deleteAxis->showText();
		pub_addCurve->showText();
		pub_deleteCurve->showText();
		pub_addLegend->showText();
		pub_deleteLegend->showText();
		pub_addShape->showText();
		pub_deleteShape->showText();
	}
}
/**********************************************/
/**********************************************/
