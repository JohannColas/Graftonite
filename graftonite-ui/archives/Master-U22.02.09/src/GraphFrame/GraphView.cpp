#include "GraphView.h"

#include <QSvgGenerator>
/**********************************************/
/**********************************************/
/* */
GraphView::~GraphView()
{
	delete graphItem;
	//	while ( !layerItems.isEmpty() )
	//	{
	//		delete layerItems.takeFirst();
	//	}

	delete contextMenu;
	delete acSaveGraph;
	delete acExportGraph;
}
/**********************************************/
/**********************************************/
/* */
GraphView::GraphView( QWidget *parent ) : QGraphicsView( parent )
{
	init();
	//
	setScene( new QGraphicsScene() );
	scene()->addText("hello");
	// Définifion of graphItem
	graphItem = new GraphItem();
	connect(graphItem, &GraphItem::doubleClicked, this, &GraphView::autoFit );
	connect(graphItem, &GraphItem::scaled, this, &GraphView::resizeView );
	scene()->addItem( graphItem );
	// Initialization of the contextual menu
	contextMenu = new QMenu(this);
	contextMenu->addAction( acSaveGraph );
	connect( acSaveGraph, &QAction::triggered, this, &GraphView::saveGraph );
	contextMenu->addAction( acExportGraph );
	connect( acExportGraph, &QAction::triggered, this, &GraphView::exportGraph );
	// Connexion entre actions et les slots à effectuer
	//connect( acExportGraph, &QAction::triggered, this, &GraphView::exportGraph );
	//
	//    title = new gTitle("Blabla", 50, 20, _graph );
	//
	//    layerItems.append( new LayerItem( graphItem->innerAxis() ) );
	//    layerItems.at(0)->setSettings( new LayerSettings() );
	//    layerItems.at(0)->update();
	//
	//    _axis.append( new GFT_Axis(AXISTYPE::X, _layers.at(0) ) );
	//    _axis.append( new GFT_Axis(AXISTYPE::Y, _layers.at(0) ) );
	//
	//_curves.append( new GFT_Curve( _graph->curvesLayer() ) );
	//updateCurves();
	//
	//    GFT_TextItem *textItem = new GFT_TextItem( "<center>C<p style=\"font-size:36px;color:red;\">This is a paragraph.</p>er me!<br/>dfdfdf</center>",  120, 100, _graph->foreground()  );
	//    textItem->rotate( 0 );
	//
	//    GFT_SymbolItem *symb = new GFT_SymbolItem( 300, 300, 5, _graph->foreground() );
	//    symb->setSymbolType(SYMBOL::SQUARE);
	//    symb->setAngle(45);
	//    symb->setSymbolTypeOpt(3);
	//    symb->setCoverage(0);
	//    symb->update();
	//    GFT_SymbolItem *symb2 = new GFT_SymbolItem( 350, 350, 5, _graph->foreground() );
	//    symb2->setSymbolType(SYMBOL::STAR);
	//    symb2->setAngle(0);
	//    symb2->setSymbolTypeOpt(6);
	//    symb2->setCoverage(50);
	//    symb2->update();
	//    GFT_SymbolItem *symb3 = new GFT_SymbolItem( 350, 300, 5, _graph->foreground() );
	//    symb3->setSymbolType(SYMBOL::CERCLE);
	//    symb3->setAngle(45);
	//    symb3->setSymbolTypeOpt(3);
	//    symb3->setCoverage(50);
	//    symb3->update();
	//
	//GFT_File::readGPHFile( "testGraph.gph" );
	//		GFT_File::updateGraphData( "testGraph.gph", _graph );
	//
	//
	save();
}
/**********************************************/
/**********************************************/
/* */
// Initialisation of the GraphView
void GraphView::init()
{
	// Set alignement of the item
	setAlignment( Qt::AlignHCenter | Qt::AlignTop );
	// Set render
	setRenderHints( QPainter::Antialiasing | QPainter::SmoothPixmapTransform );
	// Remove the ScrollBar
	setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
	setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
	//
	setScene( new QGraphicsScene() );
	//
	connect( this, &GraphView::resized, this, &GraphView::resizeView );
}
/**********************************************/
/**********************************************/
/* */
void GraphView::save()
{

}
/**********************************************/
/**********************************************/
/* */
void GraphView::update( GraphSettings* settings )
{
	// Updating graphItem
	graphItem->update( settings );
	// Updating titleItem
	// Updating layerItems
	for( auto elm : graphItem->backgroundBed()->childItems() )
	{
		delete elm;
	}
	while( !layerItems.isEmpty() )
	{
		delete layerItems.takeFirst();
	}
	for( LayerSettings* elm : settings->getLayersSettings() )
	{
		LayerItem* layerTMP = new LayerItem( graphItem->innerAxisBed(), graphItem->backgroundBed() );
		layerTMP->update( elm );
		layerItems.append( layerTMP );
	}
	// Updating AxisItems
	while( !axisItems.isEmpty() )
	{
		delete axisItems.takeFirst();
	}
	for( AxisSettings* elm : settings->getAxesSettings() )
	{
		AxisItem* axisTMP = new AxisItem( graphItem->axisBed(),
		                                  graphItem->majorGridsBed(),
		                                  graphItem->minorGridsBed() );
		GraphRect rectTMP( 50, 50, 100, 100 );
		for( LayerSettings* layerSTMP : settings->getLayersSettings() )
		{
			if( layerSTMP->getID() == elm->getLayerID() )
			{
				rectTMP = layerSTMP->getRect();
				break;
			}
		}
		axisTMP->update( elm, rectTMP );
		axisItems.append( axisTMP );
	}
	// Updating CurveItems
	while( !curveItems.isEmpty() )
	{
		delete curveItems.takeFirst();
	}
	for( CurveSettings* elm : settings->getCurvesSettings() )
	{
		CurveItem* curveTMP = new CurveItem( graphItem->curvesBed() );
		GraphRect rectTMP( 50, 50, 100, 100 );
		//		for( LayerSettings* layerSTMP : settings->getLayersSettings() )
		//		{
		//			if( layerSTMP->getID() == elm->getLayerID() )
		//			{
		//				rectTMP = layerSTMP->getRect();
		//				break;
		//			}
		//		}
		curveTMP->update( elm );
		curveItems.append( curveTMP );
	}
	// Updating ShapeItems
}
/**********************************************/
/**********************************************/
/* */
void GraphView::clearAllItems()
{
	// Deleting graphItem
	if( graphItem != nullptr )
	{
		delete graphItem;
		graphItem = nullptr;
	}
	// Deleting titleItem
	if( titleItem != nullptr )
	{
		delete titleItem;
		titleItem = nullptr;
	}
	// Deleting layerItems
	while( !layerItems.isEmpty() )
	{
		delete layerItems.takeFirst();
	}
	// Deleting axisItems
	while( !axisItems.isEmpty() )
	{
		delete axisItems.takeFirst();
	}
	// Deleting curveItems
	while( !curveItems.isEmpty() )
	{
		delete curveItems.takeFirst();
	}
	// Deleting curveItems
	while( !shapeItems.isEmpty() )
	{
		delete shapeItems.takeFirst();
	}
}
/**********************************************/
/**********************************************/
/* */
void GraphView::autoFit()
{
	graphItem->setX(0);
	graphItem->setY(0);
	fitInView(graphItem, Qt::KeepAspectRatio);
}
/**********************************************/
/**********************************************/
/* */
void GraphView::resizeView()
{
	int w = graphItem->rect().width();
	int h = graphItem->rect().height();
	fitInView(0, 0, w, h, Qt::KeepAspectRatio);
}
/**********************************************/
/**********************************************/
/* */
void GraphView::resizeEvent( QResizeEvent *event )
{
	Q_UNUSED( event );
	emit resized();
}
/**********************************************/
/**********************************************/
/* */
void GraphView::contextMenuEvent( QContextMenuEvent *event )
{
	contextMenu->exec( event->globalPos() );
}
/**********************************************/
/**********************************************/
/* */
void GraphView::exportGraph()
{
	//  High Quality Image !!!
	short mult = 10;
	int x = graphItem->boundingRect().x();
	int y = graphItem->boundingRect().y();
	int width = graphItem->boundingRect().width();
	int height = graphItem->boundingRect().height();
	QImage image(mult*width, mult*height, QImage::Format_ARGB32);  // Create the image with the exact size of the shrunk scene
	// Selections would also render to the file
	this->scene()->setSceneRect( x, y, width, height );
	scene()->setBackgroundBrush( QBrush( Qt::NoBrush ) );
	//	QBrush local = _graph->brush();
	//	_graph->setBrush( QBrush( Qt::NoBrush ) );
	image.fill( Qt::transparent );
	QPainter painter(&image);
	painter.setRenderHint(QPainter::Antialiasing);
	this->scene()->render(&painter);
	image.save("AAB.png");
	//	_graph->setBrush( local );

	//Save in SVG
	QSvgGenerator svgGen;
	svgGen.setFileName( "AAA.svg" );
	svgGen.setSize(QSize(1000, 1000));
	svgGen.setViewBox(QRect(0, 0, 1000, 1000));
	svgGen.setTitle(tr("SVG Generator Example Drawing"));
	svgGen.setDescription(tr("An SVG drawing created by the SVG Generator "
	                         "Example provided with Qt."));
	QPainter painter2( &svgGen );
	this->scene()->render( &painter2 );
}

void GraphView::mousePressEvent( QMouseEvent* event )
{
	this->setCursor( Qt::CursorShape::ClosedHandCursor );

	_old_pos = this->mapFromGlobal(event->pos());
	if ( event->button() == Qt::MouseButton::LeftButton )
		_movable = true;
	QGraphicsView::mousePressEvent( event );
}

void GraphView::mouseReleaseEvent( QMouseEvent* event )
{
	this->setCursor( Qt::CursorShape::ArrowCursor );
	_movable = false;
	QGraphicsView::mouseReleaseEvent( event );
}

void GraphView::mouseMoveEvent( QMouseEvent* event )
{
	Qt::MouseButtons buttons = event->buttons();
	QPoint pos = this->mapFromGlobal(event->pos());
	QGraphicsItem* item = this->scene()->itemAt(pos, QTransform());

	if ( _movable )
	{
		QRectF rect = this->sceneRect();
		QPointF newp = rect.topLeft() - 0.2*(QPointF(pos) - QPointF(_old_pos));
		this->setSceneRect( QRectF( newp, rect.size()) );
	}


	QGraphicsView::mouseMoveEvent( event );

//	type = EventTypes.MouseLeftMove  if (buttons & Qt.LeftButton)  else\
//              EventTypes.MouseRightMove if (buttons & Qt.RightButton) else\
//              EventTypes.MouseMidMove   if (buttons & Qt.MidButton)   else\
//              EventTypes.MouseMove

//       handled = self.activeTool().handleEvent(type, object, pos)

//       if (not handled):
//           QGraphicsView.mouseMoveEvent(self, event)
}

void GraphView::wheelEvent( QWheelEvent* event )
{
	if ( event->angleDelta().y() > 0 )
		this->scale( 1.25, 1.25 );
	else
		this->scale( 0.8, 0.8 );
}

void GraphView::paintEvent( QPaintEvent* event )
{
	QPainter painter( viewport() );
	if (_gridVisible)
	{
		const int wView = viewport()->width(), hView = viewport()->height();
		QColor white = QColor( 255,255,255 );
		QColor gray = QColor( 235,235,235 );
		QColor brush = gray;
		QColor brush2;
		for ( int x = 0 / 2; x < wView; x += _gridSize )
		{
			if ( brush == gray )
				brush = white;
			else
				brush = gray;
			brush2 = brush;
			for ( int y = 0 / 2; y < hView; y += _gridSize )
			{
				painter.setBrush( brush2 );
				painter.drawRect( x, y, _gridSize, _gridSize );
				if ( brush2 == gray )
					brush2 = white;
				else
					brush2 = gray;
			}
		}

		//		qPainter.setPen(_gridColor);
//		for (int x = _gridSize / 2; x < wView; x += _gridSize)
//		{
//			qPainter.drawLine(x, 0, x, hView - 1);
//		}
//		for (int y = _gridSize / 2; y < hView; y += _gridSize)
//		{
//			qPainter.drawLine(0, y, wView - 1, y);
//		}
	}
	QGraphicsView::paintEvent(event);
}
/**********************************************/
/**********************************************/
