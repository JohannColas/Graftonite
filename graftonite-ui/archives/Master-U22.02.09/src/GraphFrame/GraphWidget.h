#ifndef GRAPHWIDGET_H
#define GRAPHWIDGET_H

#include <QSplitter>
#include <QWidget>
#include <QVBoxLayout>

#include "GraphView.h"
#include "GraphSettingsView.h"
#include "GraphSettings.h"
#include "Commons/theme.h"
#include "Commons/lang.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class GraphWidget
		: public QSplitter
{
    Q_OBJECT
private:
	QWidget* wid_main = new QWidget;
	QVBoxLayout* lay_main = new QVBoxLayout;
	QLabel* lab_title = new QLabel;
	GraphView* wid_graphView = new GraphView;
	GraphSettingsView* wid_toolBar = new GraphSettingsView;
	GraphSettings *settings = nullptr;

public:
    ~GraphWidget();
    GraphWidget(QWidget *parent = nullptr);

    GraphSettings *getSettings() const;
	void setSettings(GraphSettings *value);

public slots:
	void updateGraphView();
//	void addLayer();
//	void deleteLayer( QString id) ;
//	void addAxis();
//	void deleteAxis( QString id );
//	void addCurve();
//	void deleteCurve( QString id );
	void updateIcons( Icons* icons );
	void updateLang( Lang* lang );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // GRAPHWIDGET_H
