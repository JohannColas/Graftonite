#include "CurveSettingsView.h"
/**********************************************/
/**********************************************/
/* */
CurveSettingsView::~CurveSettingsView()
{
}
/**********************************************/
/**********************************************/
/* */
CurveSettingsView::CurveSettingsView( QWidget *parent )
	: QWidget( parent )
{
	FormLayout* lay_layerSets = new FormLayout;
	lab_Type->setText( "type :" );
	lay_layerSets->addRow( lab_Type, sel_Type );
	lab_Axis1->setText( "Axis 1 :" );
	lay_layerSets->addRow( lab_Axis1, sel_Axis1 );
	lab_Axis2->setText( "Axis 2 :" );
	lay_layerSets->addRow( lab_Axis2, sel_Axis2 );
	lab_Axis3->setText( "Axis 3 :" );
	lay_layerSets->addRow( lab_Axis3, sel_Axis3 );
	lab_Data1->setText( "Data 1 :" );
	lay_layerSets->addRow( lab_Data1, sel_Data1 );
	lab_Data2->setText( "Data 2 :" );
	lay_layerSets->addRow( lab_Data2, sel_Data2 );
	lab_Data3->setText( "Data 3 :" );
	lay_layerSets->addRow( lab_Data3, sel_Data3 );

	QWidget* graphWid = new QWidget;
	graphWid->setLayout( lay_layerSets );
	sll_settings->setWidget( graphWid );
	tab_settings->addWidget( "General", sll_settings );

	tab_settings->addWidget( "Symbols", symbolsSettings );
	tab_settings->addWidget( "Line", lineSettings );
	tab_settings->addWidget( "Error Bars", errorBarsSettings );
	tab_settings->addWidget( "Bars", barsSettings );
	tab_settings->addWidget( "Area", areaSettings );
	tab_settings->addWidget( "Labels", labelsSettings );

	tab_settings->changeTab(0);
	tab_settings->setAlignment( Qt::AlignLeft, Qt::AlignBottom );


	GridLayout* lay_main = new GridLayout;
//	lay_main->setMargin( 0 );
	lay_main->addWidget( sel_ID, 0, 0 );
	lay_main->addWidget( tab_settings, 1, 0 );

	setLayout( lay_main );
}
/**********************************************/
/**********************************************/
/* */
CurveSettings* CurveSettingsView::getSettings() const
{
	return settings;
}
/**********************************************/
/**********************************************/
/* */
void CurveSettingsView::setSettings( CurveSettings* sets )
{
	disconnectModifiers();

	settings = sets;

	connectModifiers();
	// set to others widgets
	symbolsSettings->setSettings( settings );
	lineSettings->setSettings( settings );
	errorBarsSettings->setSettings( settings );
	barsSettings->setSettings( settings );
	areaSettings->setSettings( settings );

	update();
}
/**********************************************/
/**********************************************/
/* */
void CurveSettingsView::update()
{
	//
	sel_ID->setText( settings->getID() );
	setUselessWidgets();

//	ui->leCvID->setText( settings->getID() );
	//	ui->coAxType->blockSignals( true );
	//	ui->coAxType->setCurrentType( settings->getType() );
	//	ui->coAxType->blockSignals( false );
	//	setAxisLayer();
	//	//
	//	ui->coAxPosition->blockSignals( true );
	//	ui->coAxPosition->updateItems( settings->getType() );
	//	ui->coAxPosition->setCurrentPos( settings->getPos() );
	//	ui->coAxPosition->blockSignals( false );
	//	ui->dspAxPosAt->setValue( settings->getPosAt() );
	//	ui->spAxOffset->setValue( settings->getPosOffset() );
	//	ui->coAxScale->setCurrentScale( settings->getScale() );
	//	ui->leAxScaleOffset->setText( QString::number( settings->getScaleOffset() ) );
	//	ui->leAxMinBound->setText( QString::number( settings->getMinBound() ) );
	//	ui->leAxMaxBound->setText( QString::number( settings->getMaxBound() ) );
}
/**********************************************/
/**********************************************/
/* */
void CurveSettingsView::connectModifiers()
{
	connect( sel_ID, &LineEditor::editingFinished,
			 this, &CurveSettingsView::idChanged );
	connect( sel_Type, &CurveTypeSelector::changed,
			 this, &CurveSettingsView::typeChanged );
//	connect( sel_Axis1, &QComboBox::currentTextChanged,
//			 settings, &CurveSettings::setAxis1 );
//	connect( sel_Axis2, &QComboBox::currentTextChanged,
//			 settings, &CurveSettings::setAxis2 );
//	connect( sel_Axis3, &QComboBox::currentTextChanged,
//			 settings, &CurveSettings::setAxis3 );
//	connect( sel_Data1, &QComboBox::currentTextChanged,
//			 settings, &CurveSettings::setData1 );
//	connect( sel_Data2, &QComboBox::currentTextChanged,
//			 settings, &CurveSettings::setData2 );
//	connect( sel_Data3, &QComboBox::currentTextChanged,
//			 settings, &CurveSettings::setData3 );
}
/**********************************************/
/**********************************************/
/* */
void CurveSettingsView::disconnectModifiers()
{
	disconnect( sel_ID, &LineEditor::editingFinished,
				this, &CurveSettingsView::idChanged );
	disconnect( sel_Type, &CurveTypeSelector::changed,
				this, &CurveSettingsView::typeChanged );
//	disconnect( sel_Axis1, &QComboBox::currentTextChanged,
//				settings, &CurveSettings::setAxis1 );
//	disconnect( sel_Axis2, &QComboBox::currentTextChanged,
//				settings, &CurveSettings::setAxis2 );
//	disconnect( sel_Axis3, &QComboBox::currentTextChanged,
//				settings, &CurveSettings::setAxis3 );
//	disconnect( sel_Data1, &QComboBox::currentTextChanged,
//				settings, &CurveSettings::setData1 );
//	disconnect( sel_Data2, &QComboBox::currentTextChanged,
//				settings, &CurveSettings::setData2 );
//	disconnect( sel_Data3, &QComboBox::currentTextChanged,
	//				settings, &CurveSettings::setData3 );
}
/**********************************************/
/**********************************************/
/* */
void CurveSettingsView::idChanged()
{
//	QString oldID = settings->getID();
	QString newID = sel_ID->text();
//	emit settings->idChanged( oldID, newID );
	settings->setID( newID );
}
/**********************************************/
/**********************************************/
/* */
void CurveSettingsView::typeChanged( const Curve::Type& type )
{
	settings->setType( type );
	update();
	setUselessWidgets();

//	errorBarsSettings->typeChanged();
}
/**********************************************/
/**********************************************/
/* */
void CurveSettingsView::setUselessWidgets()
{
	Curve::Type type = settings->getType();
	if ( type == Curve::XY )
	{
		lab_Axis3->hide();
		sel_Axis3->hide();
		lab_Data3->hide();
		sel_Data3->hide();
	}
	else if ( type == Curve::XYZ ||
			  type == Curve::XYY )
	{
		lab_Axis3->show();
		sel_Axis3->show();
		lab_Data3->show();
		sel_Data3->show();
	}
}
/**********************************************/
/**********************************************/
/* */
void CurveSettingsView::updateIcons( Icons* icons ) {
Q_UNUSED(icons)
}
/**********************************************/
/**********************************************/
/* */
void CurveSettingsView::updateLang( Lang* lang ) {
Q_UNUSED(lang)
}
/**********************************************/
/**********************************************/
/* */
