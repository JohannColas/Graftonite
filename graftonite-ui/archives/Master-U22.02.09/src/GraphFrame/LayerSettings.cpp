#include "LayerSettings.h"
/**********************************************/
/**********************************************/
/* */
LayerSettings::LayerSettings()
{
    
}
/**********************************************/
/**********************************************/
/* */
QString LayerSettings::getID() const
{
    return id;
}
/**********************************************/
/**********************************************/
/* */
void LayerSettings::setID(const QString &value)
{
	id = value;
	emit changed();
}
/**********************************************/
/**********************************************/
/* */
GraphRect LayerSettings::getRect() const
{
	return GraphRect( getX(), getY(), getWidth(), getHeight() );
}
/**********************************************/
/**********************************************/
/* */
int LayerSettings::getX() const
{
    return x;
}
/**********************************************/
/**********************************************/
/* */
void LayerSettings::setX(int value)
{
    x = value;
	emit changed();
}
/**********************************************/
/**********************************************/
/* */
int LayerSettings::getY() const
{
    return y;
}
/**********************************************/
/**********************************************/
/* */
void LayerSettings::setY(int value)
{
    y = value;
	emit changed();
}
/**********************************************/
/**********************************************/
/* */
int LayerSettings::getWidth() const
{
    return width;
}
/**********************************************/
/**********************************************/
/* */
void LayerSettings::setWidth(int value)
{
    width = value;
	emit changed();
}
/**********************************************/
/**********************************************/
/* */
int LayerSettings::getHeight() const
{
    return height;
}
/**********************************************/
/**********************************************/
/* */
void LayerSettings::setHeight(int value)
{
    height = value;
	emit changed();
}
/**********************************************/
/**********************************************/
/* */
QPen LayerSettings::getBorderPen() const
{
    QPen pen;
    pen.setStyle( getBdStyle() );
    pen.setColor( getBdColor() );
    pen.setWidth( getBdWidth() );
    pen.setJoinStyle( getBdJoin() );
    pen.setMiterLimit( getBdMiter() );
    return pen;
}
/**********************************************/
/**********************************************/
/* */
Qt::PenStyle LayerSettings::getBdStyle() const
{
    return bdStyle;
}
/**********************************************/
/**********************************************/
/* */
void LayerSettings::setBdStyle(Qt::PenStyle value)
{
    bdStyle = value;
	emit changed();
}
/**********************************************/
/**********************************************/
/* */
QColor LayerSettings::getBdColor() const
{
    return bdColor;
}
/**********************************************/
/**********************************************/
/* */
void LayerSettings::setBdColor(const QColor &value)
{
    bdColor = value;
	emit changed();
}
/**********************************************/
/**********************************************/
/* */
int LayerSettings::getBdWidth() const
{
    return bdWidth;
}
/**********************************************/
/**********************************************/
/* */
void LayerSettings::setBdWidth(int value)
{
    bdWidth = value;
	emit changed();
}
/**********************************************/
/**********************************************/
/* */
Qt::PenJoinStyle LayerSettings::getBdJoin() const
{
    return bdJoin;
}
/**********************************************/
/**********************************************/
/* */
void LayerSettings::setBdJoin(const Qt::PenJoinStyle &value)
{
    bdJoin = value;
	emit changed();
}
/**********************************************/
/**********************************************/
/* */
int LayerSettings::getBdMiter() const
{
    return bdMiter;
}
/**********************************************/
/**********************************************/
/* */
void LayerSettings::setBdMiter(int value)
{
    bdMiter = value;
	emit changed();
}
/**********************************************/
/**********************************************/
/* */
QColor LayerSettings::getBkColor() const
{
    return bkColor;
}
/**********************************************/
/**********************************************/
/* */
void LayerSettings::setBkColor(const QColor &value)
{
    bkColor = value;
	emit changed();
}
/**********************************************/
/**********************************************/
/* */
void LayerSettings::readSettings( QSettings* setsFile )
{
	setID( setsFile->value("id").toString() );
	setX( setsFile->value("x").toInt() );
	setY( setsFile->value("y").toInt() );
	setWidth( setsFile->value("width").toInt() );
	setHeight( setsFile->value("height").toInt() );
	setBkColor( QColor( setsFile->value("bkColor").toString() ) );
	setBdStyle( (Qt::PenStyle)setsFile->value("bdStyle").toInt() );
	setBdWidth( setsFile->value("bdWidth").toInt() );
	setBdColor( QColor( setsFile->value("bdColor").toString() ) );
	setBdJoin( (Qt::PenJoinStyle)setsFile->value("bdJoin").toInt() );
	setBdMiter( setsFile->value("bdMiter").toInt() );
}
/**********************************************/
/**********************************************/
/* */
void LayerSettings::saveSettings( QSettings* setsFile )
{
	setsFile->setValue("id", getID() );
	setsFile->setValue("x", getX() );
	setsFile->setValue("y", getY() );
	setsFile->setValue("width", getWidth() );
	setsFile->setValue("height", getHeight() );
	setsFile->setValue("bkColor", QVariant( getBkColor() ).toString() );
	setsFile->setValue("bdStyle", (int)getBdStyle() );
	setsFile->setValue("bdWidth", getBdWidth() );
	setsFile->setValue("bdColor", QVariant( getBdColor() ).toString() );
	setsFile->setValue("bdJoin", (int)getBdJoin() );
	setsFile->setValue("bdMiter", getBdMiter() );
}
/**********************************************/
/**********************************************/
