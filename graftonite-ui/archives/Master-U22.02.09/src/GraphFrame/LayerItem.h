#ifndef LAYERITEM_H
#define LAYERITEM_H

//#include <QObject>
#include <QGraphicsRectItem>

#include "LayerSettings.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class LayerItem : public QObject, public QGraphicsRectItem
{
    Q_OBJECT
private:
	QGraphicsRectItem* background = nullptr;

public:
    ~LayerItem();
	LayerItem( QGraphicsItem *parent, QGraphicsItem *bkparent );
	void init();

public slots:
	void update( LayerSettings* settings );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LAYERITEM_H
