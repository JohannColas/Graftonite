#include "CurveSettings.h"
/**********************************************/
/**********************************************/
/* */
CurveSettings::~CurveSettings()
{
    
}
/**********************************************/
/**********************************************/
/* */
CurveSettings::CurveSettings()
{
    
}
/**********************************************/
/**********************************************/
/* */
void CurveSettings::readSettings( QSettings* setsFile ) {
	blockSignals( true );
	// Reading General Settings
	setID( setsFile->value("id").toString() );
	setAxesID( setsFile->value("axesID").toStringList() );
	setType( (Curve::Type)setsFile->value("type").toInt() );
	// Reading Symbols Settings
	setSymbType( (Curve::Symbol)setsFile->value("symbType").toInt() );
	setSymbTypeOpt( setsFile->value("symbTypeOpt").toDouble() );
	setSymbSize( setsFile->value("symbSize").toDouble() );
	setSymbCoverage( setsFile->value("symbCoverage").toDouble() );
	setSymbAngle( setsFile->value("symbAngle").toDouble() );
	setSymbDAngle( setsFile->value("symbDAngle").toDouble() );
	setSymbColor( QColor(setsFile->value("symbColor").toString()) );
	setSymbBdWidth( setsFile->value("symbBdWidth").toDouble() );
	setSymbBdColor( QColor(setsFile->value("symbBdColor").toString()) );
	setSymbBdJoin( (Qt::PenJoinStyle)setsFile->value("symbBdJoin").toInt() );
	// Reading Line Settings
	setLineType( (Curve::Line)setsFile->value("lineType").toInt() );
	setLineGap( setsFile->value("lineGap").toDouble() );
	setLineStyle( (Qt::PenStyle)setsFile->value("lineStyle").toInt() );
	setLineWidth( setsFile->value("lineWidth").toDouble() );
	setLineColor( QColor( setsFile->value("lineColor").toString() ) );
	setLineCap( (Qt::PenCapStyle)setsFile->value("lineCap").toInt() );
	setLineJoin( (Qt::PenJoinStyle)setsFile->value("lineJoin").toInt() );
	// Reading Error Bars Settings
	setErrBarsType( (Curve::ErrorBar)setsFile->value("errBarsType").toInt() );
	setErrBarsData1( setsFile->value("errBarsData1").toString() );
	setErrBarsData2( setsFile->value("errBarsData2").toString() );
	setErrBarsData3( setsFile->value("errBarsData3").toString() );
	setErrBarsStyle( (Qt::PenStyle)setsFile->value("errBarsStyle").toInt() );
	setErrBarsWidth( setsFile->value("errBarsWidth").toDouble() );
	setErrBarsColor( QColor( setsFile->value("errBarsColor").toString() ) );
	setErrBarsCap( (Qt::PenCapStyle)setsFile->value("errBarsCap").toInt() );
	// Reading Bars Settings
	setBarsDirec( (Curve::Direction)setsFile->value("barsDirec").toInt() );
	setBarsWidth( setsFile->value("barsWidth").toDouble() );
	setBarsPattern( (Qt::BrushStyle)setsFile->value("barsPattern").toInt() );
	setBarsColor( QColor( setsFile->value("barsColor").toString() ) );
	setBarsBdStyle( (Qt::PenStyle)setsFile->value("barsBdStyle").toInt() );
	setBarsBdWidth( setsFile->value("barsBdWidth").toDouble() );
	setBarsBdColor( QColor( setsFile->value("barsBdColor").toString() ) );
	setBarsBdJoin( (Qt::PenJoinStyle)setsFile->value("barsBdColor").toInt() );
	// Reading Area Settings
	setAreaDirec( (Curve::Direction)setsFile->value("areaDirec").toInt() );
	setAreaPattern( (Qt::BrushStyle)setsFile->value("areaPattern").toInt() );
	setAreaColor( QColor( setsFile->value("areaColor").toString() ) );
	setAreaBdStyle( (Qt::PenStyle)setsFile->value("areaBdStyle").toInt() );
	setAreaBdWidth( setsFile->value("areaBdWidth").toDouble() );
	setAreaBdColor( QColor( setsFile->value("areaBdColor").toString() ) );
	setAreaBdJoin( (Qt::PenJoinStyle)setsFile->value("areaBdJoin").toInt() );
	blockSignals( false );
}
/**********************************************/
/**********************************************/
/* */
void CurveSettings::saveSettings( QSettings* setsFile ) {
	// Adding General Settings
	setsFile->setValue("id", getID() );
	setsFile->setValue("axesID", getAxesID() );
	setsFile->setValue("type", (int)getType() );
	// Adding Symbols Settings
	setsFile->setValue("symbType", (int)getSymbType() );
	setsFile->setValue("symbTypeOpt", getSymbTypeOpt() );
	setsFile->setValue("symbSize", getSymbSize() );
	setsFile->setValue("symbCoverage", getSymbCoverage() );
	setsFile->setValue("symbAngle", getSymbAngle() );
	setsFile->setValue("symbDAngle", getSymbDAngle() );
	setsFile->setValue("symbColor", QVariant(getSymbColor()).toString() );
	setsFile->setValue("symbBdColor", QVariant(getSymbBdColor()).toString() );
	setsFile->setValue("symbBdWidth", getSymbBdWidth() );
	setsFile->setValue("symbBdJoin", (int)getSymbBdJoin() );
	// Adding Line Settings
	setsFile->setValue("lineType", (int)getLineType() );
	setsFile->setValue("lineGap", getLineGap() );
	setsFile->setValue("lineStyle", (int)getLineStyle() );
	setsFile->setValue("lineWidth", getLineWidth() );
	setsFile->setValue("lineColor", QVariant(getLineColor()).toString() );
	setsFile->setValue("lineCap", (int)getLineCap() );
	setsFile->setValue("lineJoin", (int)getLineJoin() );
	// Adding Error Bars Settings
	setsFile->setValue("errBarsType", (int)getErrBarsType() );
	setsFile->setValue("errBarsData1", getErrBarsData1() );
	setsFile->setValue("errBarsData2", getErrBarsData2() );
	setsFile->setValue("errBarsData3", getErrBarsData3() );
	setsFile->setValue("errBarsStyle", (int)getErrBarsStyle() );
	setsFile->setValue("errBarsWidth", getErrBarsWidth() );
	setsFile->setValue("errBarsColor", QVariant(getErrBarsColor()).toString() );
	setsFile->setValue("errBarsCap", (int)getErrBarsCap() );
	// Adding Bars Settings
	setsFile->setValue("barsDirec", (int)getBarsDirec() );
	setsFile->setValue("barsWidth", getBarsWidth() );
	setsFile->setValue("barsPattern", (int)getBarsPattern() );
	setsFile->setValue("barsColor", QVariant(getBarsColor()).toString() );
	setsFile->setValue("barsBdStyle", (int)getBarsBdStyle() );
	setsFile->setValue("barsBdWidth", getBarsBdWidth() );
	setsFile->setValue("barsBdColor", QVariant(getBarsBdColor()).toString() );
	setsFile->setValue("barsBdJoin", (int)getBarsBdJoin() );
	// Adding Area Settings
	setsFile->setValue("areaDirec", (int)getAreaDirec() );
	setsFile->setValue("areaPattern", (int)getAreaPattern() );
	setsFile->setValue("areaColor", QVariant(getAreaColor()).toString() );
	setsFile->setValue("areaBdStyle", (int)getAreaBdStyle() );
	setsFile->setValue("areaBdWidth", getAreaBdWidth() );
	setsFile->setValue("areaBdColor", QVariant(getAreaBdColor()).toString() );
	setsFile->setValue("areaBdJoin", (int)getAreaBdJoin() );
}
/**********************************************/
/**********************************************/
