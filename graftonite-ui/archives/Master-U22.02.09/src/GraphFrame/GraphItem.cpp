#include "GraphItem.h"

#include <QPen>
/**********************************************/
/**********************************************/
/* */
GraphItem::~GraphItem()
{
	delete _backgroundBed;
	delete _minorGridsBed;
	delete _majorGridsBed;
	delete _curvesBed;
	delete _innerAxisBed;
	delete _axisBed;
	delete _outerAxisBed;
	delete _foregroundBed;
}
/**********************************************/
/**********************************************/
/* */
GraphItem::GraphItem( QGraphicsItem *parent ) : QGraphicsRectItem( parent )
{
    init();
}
/**********************************************/
/**********************************************/
/* */
void GraphItem::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *mouseevent)
{
    if ( mouseevent->button() == Qt::LeftButton )
    {
        emit doubleClicked();
    }
}
/**********************************************/
/**********************************************/
/* */
void GraphItem::init()
{
	_backgroundBed = new BedItem( this );
	_minorGridsBed = new BedItem( this );
	_majorGridsBed = new BedItem( this );
	_curvesBed = new BedItem( this );
	_innerAxisBed = new BedItem( this );
	_axisBed = new BedItem( this );
	_outerAxisBed = new BedItem( this );
	_foregroundBed = new BedItem( this );
}
/**********************************************/
/**********************************************/
/* */
void GraphItem::update()
{
	setRect( 0, 0, 800, 500 );
	setBrush( Qt::white );
}
/**********************************************/
/**********************************************/
/* */
void GraphItem::update(GraphSettings* settings)
{
	setRect( 0, 0, settings->getWidth(), settings->getHeight() );
	setBrush( settings->getBkColor() );
	setPen( settings->getBorderPen() );
}
/**********************************************/
/**********************************************/
