#ifndef GRAPHVIEW_H
#define GRAPHVIEW_H

#include <QGraphicsView>
#include <QMenu>
#include <QContextMenuEvent>

#include "GraphItem.h"
#include "LayerItem.h"
#include "TitleItem.h"
#include "AxisItem.h"
#include "CurveItem.h"
#include "ShapeItem.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class GraphView : public QGraphicsView
{
    Q_OBJECT
protected:
	GraphItem* graphItem = nullptr;
	QList<LayerItem*> layerItems;
	TitleItem* titleItem = nullptr;
	QList<AxisItem*> axisItems;
	QList<CurveItem*> curveItems;
	QList<ShapeItem*> shapeItems;
	QMenu *contextMenu = nullptr;
	QAction *acSaveGraph = new QAction("Save Graph", this);
	QAction *acExportGraph = new QAction("Export Graph", this);
	bool _gridVisible = true;
	int _gridSize = 10;
	QPoint _old_pos;
	bool _movable = false;
public:
	~GraphView();
	GraphView( QWidget *parent = nullptr );
	void init();

public slots:
	void save();
	void update( GraphSettings* settings );
	void clearAllItems();
    void autoFit();
    void resizeView();
    void resizeEvent( QResizeEvent *event ) override;
	void contextMenuEvent( QContextMenuEvent* event ) override;
	void exportGraph();
	void mousePressEvent( QMouseEvent *event ) override;
	void mouseReleaseEvent( QMouseEvent *event ) override;
	void mouseMoveEvent( QMouseEvent *event ) override;
	void wheelEvent( QWheelEvent *event ) override;
	virtual void paintEvent( QPaintEvent* event ) override;

signals:
    void resized();
	void saveGraph();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // GRAPHVIEW_H
