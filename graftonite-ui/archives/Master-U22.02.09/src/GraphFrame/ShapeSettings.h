#ifndef SHAPESETTINGS_H
#define SHAPESETTINGS_H

#include <QObject>
#include <QString>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class ShapeSettings
		: public QObject
{
	Q_OBJECT
private:
	QString id = "";

public:
    ShapeSettings();
	QString getID() const;

public slots:
	void setID( const QString& id );

signals:
	void changed();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SHAPESETTINGS_H
