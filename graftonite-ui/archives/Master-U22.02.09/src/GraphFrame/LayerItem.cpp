#include "LayerItem.h"
/**********************************************/
/**********************************************/
/* */
LayerItem::~LayerItem()
{
//	if( background != nullptr )
//	{
//		delete background;
//		background = nullptr;
//	}
}
/**********************************************/
/**********************************************/
/* */
LayerItem::LayerItem( QGraphicsItem *parent, QGraphicsItem *bkparent ) : QGraphicsRectItem( parent )
{
	background = new QGraphicsRectItem( bkparent );
}

void LayerItem::update( LayerSettings* settings )
{
    setPos( settings->getX(), settings->getY() );
    setRect( 0, 0, settings->getWidth(), settings->getHeight() );
    setPen( settings->getBorderPen() );
	background->setPos( settings->getX(), settings->getY() );
	background->setRect( 0, 0, settings->getWidth(), settings->getHeight() );
	background->setBrush( settings->getBkColor() );
	background->setPen( QPen(Qt::NoPen) );
}
/**********************************************/
/**********************************************/
