#ifndef CURVEERRORBARSITEM_H
#define CURVEERRORBARSITEM_H

#include <QGraphicsPathItem>

#include "../CurveSettings.h"
#include "../Misc/GraphMisc.h"

/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class CurveErrorBarsItem
		: public QObject, public QGraphicsPathItem
{
	Q_OBJECT
private:
	QPainterPath _area;

public:
	CurveErrorBarsItem( QGraphicsItem *parent = nullptr );

public slots:
	void update( CurveSettings *settings );

};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // CURVEERRORBARSITEM_H
