#ifndef CURVESYMBOLSSETTINGSVIEW_H
#define CURVESYMBOLSSETTINGSVIEW_H

#include <QWidget>

#include "../CurveSettings.h"
#include "Commons/theme.h"
#include "Commons/lang.h"
#include "Commons/TabWidget.h"
#include "Commons/ScrollArea.h"
#include "Commons/FormLayout.h"
#include "Commons/GridLayout.h"
#include "Commons/SettingLabel.h"
#include "Commons/LineEditor.h"
#include "Commons/IntegerSelector.h"
#include "Commons/DoubleSelector.h"
#include "../Misc/SymbolsTypeSelector.h"
#include "../Misc/LineJoinSelector.h"
#include "Commons/ColorSelector.h"

/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class CurveSymbolsSettingsView
		: public ScrollArea
{
	Q_OBJECT
private:
	CurveSettings* settings = nullptr;
	SettingLabel* lab_symbols = new SettingLabel;
	SettingLabel* lab_type = new SettingLabel;
	SymbolsTypeSelector* sel_type = new SymbolsTypeSelector;
	SettingLabel* lab_option = new SettingLabel;
	LineEditor* sel_option = new LineEditor;
	SettingLabel* lab_size = new SettingLabel;
	DoubleSelector* sel_size = new DoubleSelector;
	SettingLabel* lab_coverage = new SettingLabel;
	DoubleSelector* sel_coverage = new DoubleSelector;
	SettingLabel* lab_angle = new SettingLabel;
	DoubleSelector* sel_angle = new DoubleSelector;
	SettingLabel* lab_Dangle = new SettingLabel;
	DoubleSelector* sel_Dangle = new DoubleSelector;
	SettingLabel* lab_FillColor = new SettingLabel;
	ColorSelector* sel_FillColor = new ColorSelector;
	SettingLabel* lab_Borders = new SettingLabel;
	SettingLabel* lab_BorderColor = new SettingLabel;
	ColorSelector* sel_BorderColor = new ColorSelector;
	SettingLabel* lab_BdWidth = new SettingLabel;
	DoubleSelector* sel_BdWidth = new DoubleSelector;
	SettingLabel* lab_BdJoin = new SettingLabel;
	LineJoinSelector* sel_BdJoin = new LineJoinSelector;

public:
	CurveSymbolsSettingsView( QWidget* parent = nullptr );
	void setSettings( CurveSettings* sets );
	void updateIcons( Icons* icons );
	void updateLang( Lang* lang );

public slots:
	void update();
	void disableWidget();
	void connectModifiers();
	void disconnectModifiers();
	void blockChildrenSignals( bool arg );
	void changeTypeOption();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // CURVESYMBOLSSETTINGSVIEW_H
