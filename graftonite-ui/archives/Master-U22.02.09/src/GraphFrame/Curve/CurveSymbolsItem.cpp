#include "CurveSymbolsItem.h"

/**********************************************/
/**********************************************/
/* */
CurveSymbolsItem::~CurveSymbolsItem()
{

}
/**********************************************/
/**********************************************/
/* */
CurveSymbolsItem::CurveSymbolsItem( QGraphicsItem *parent )
	: QGraphicsPathItem( parent )
{
//	_symbols.setFillRule( Qt::WindingFill );

}
/**********************************************/
/**********************************************/
/* */
void CurveSymbolsItem::update( CurveSettings *settings ) {
	QList<QPoint> points = { {0,0}, {20,20}, {40,50}, {60,90}, {80,140}, {170,0} };
	QRect rect = {20, 0, 135, 200}; // FROM AXES -> LAYER

	if ( settings->getSymbType() != Curve::NOSYMBOL ) {
		for ( QPoint elm : points ) {
			updateSymbol( elm.x(), elm.y(), settings );
		}/*
		updateSymbol( 0, 0, settings );
		updateSymbol( 0, 100, settings );
		updateSymbol( 50, 40, settings );
		updateSymbol( 100, 90, settings );*/

		QColor fill = settings->getSymbColor();
		QColor bdColor = settings->getSymbBdColor();
		double bdWidth = settings->getSymbBdWidth();
		Qt::PenJoinStyle bdJoin = settings->getSymbBdJoin();
		setPen( QPen(QBrush(bdColor), bdWidth, Qt::SolidLine, Qt::RoundCap, bdJoin ) );
		setBrush( QBrush( fill ) );

		QPainterPath cropRect;
		cropRect.addRect( rect ); // FROM AXES -> LAYER
		_symbols = _symbols.intersected( cropRect ).simplified();
		setPath( _symbols );
	}

}
/**********************************************/
/**********************************************/
/* */
void  CurveSymbolsItem::updateSymbol( double posX, double posY, CurveSettings* settings ) {

	Curve::Symbol type = settings->getSymbType();
	int typeOpt = settings->getSymbTypeOpt();
	double coverage = 0.01*settings->getSymbCoverage();
	double angle = settings->getSymbAngle();
	double Dangle = settings->getSymbDAngle();
	double size = settings->getSymbSize();
	double x = -0.5*size,
			y = -0.5*size,
			w = size,
			h = size;


	QPainterPath path;
	path.moveTo( posX, posY );
	if ( type == Curve::CERCLE ) {
		if ( (Dangle == 360 || Dangle == 0) && coverage == 0 ) {
			path.addEllipse( posX+x, posY+y, w, h );
		}
		else {
			path.arcTo( posX+x, posY+y, w, h, angle, Dangle );
		}
		if ( coverage > 0 ) {
			QPainterPath subPath;
			subPath.addEllipse( posX+coverage*x, posY+coverage*y,
								coverage*w, coverage*h );
			subPath.closeSubpath();
			path = path.subtracted( subPath );
		}
	}
	else {
		bool authorizeCover = false;
		bool authorizeUnfilled = true;
		double nbIT= 4;
		double diffangle = 0;
		if ( type == Curve::SQUARE ) {
			diffangle = 45;
		}
		else if ( type == Curve::TRIANGLE ) {
			nbIT = 3;
			diffangle = 90;
		}
		else if ( type == Curve::POLYGON ) {
			nbIT = typeOpt;
			diffangle = 90;
			if ( nbIT == 4 )
				diffangle = 45;
		}
		else if ( type == Curve::STAR ) {
			authorizeCover = true;
			authorizeUnfilled = false;
			diffangle = 90;
			nbIT = 2*typeOpt;
		}
		for ( int it = 1; it <= nbIT; ++it ) {
			double ang = ( GraphMath::PI/180) * ( ((double)(360)/nbIT)*(it - 1) + angle + diffangle );
			double xx  =  x*cos( ang );
			double yy  =  y*sin( ang );
			if ( it % 2 == 0 && authorizeCover ) {
				xx  *= coverage;
				yy  *=  coverage;
			}
			if ( it == 1 ) path.moveTo( posX+xx, posY+yy );
			else path.lineTo( posX+xx, posY+yy );
		}
		if ( coverage > 0 && authorizeUnfilled ) {
			QPainterPath subPath;
			for ( int it = 1; it <= nbIT; it++ ) {
				double ang = ( GraphMath::PI/180) * ( ((double)(360)/nbIT)*(it - 1) + angle + diffangle );
				double xx  =  x*coverage*cos( ang );
				double yy  =  y*coverage*sin( ang );
				if ( it == 1 ) subPath.moveTo( posX+xx, posY+yy );
				else subPath.lineTo( posX+xx, posY+yy );
			}
			subPath.closeSubpath();
			path = path.subtracted( subPath );
		}
	}
	path.closeSubpath();
	_symbols.addPath( path );
}
/**********************************************/
/**********************************************/
/* */
