#ifndef CURVEAREAITEM_H
#define CURVEAREAITEM_H

#include <QGraphicsPathItem>

#include "../CurveSettings.h"
#include "../Misc/GraphMisc.h"

/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class CurveAreaItem
		: public QObject, public QGraphicsPathItem
{
	Q_OBJECT
private:
	QPainterPath _area;

public:
	CurveAreaItem( QGraphicsItem *parent = nullptr );

public slots:
	void update( CurveSettings *settings );

};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // CURVEAREAITEM_H
