#include "CurveLineItem.h"
#include <cmath>
#include <QDebug>
#include <QPainter>
/**********************************************/
/**********************************************/
/* */
CurveLineItem::CurveLineItem( QGraphicsItem *parent )
	: QGraphicsPathItem( parent )
{
	_line.setFillRule( Qt::WindingFill );
}
/**********************************************/
/**********************************************/
/* */
void CurveLineItem::update( CurveSettings* settings ) {
	QList<QPoint> points = { {0,0}, {20,20}, {40,50}, {60,90}, {80,140}, {170,0} };
	QRect rect = {20, 0, 135, 200}; // FROM AXES -> LAYER

	if ( settings->getLineType() != Curve::NOLINE &&
		 points.size() > 0 ) {

		double gap = settings->getLineGap();
		Qt::PenStyle bdStyle = settings->getLineStyle();
		double bdWidth = settings->getLineWidth();
		QColor color = settings->getLineColor();
		Qt::PenJoinStyle bdJoin = settings->getLineJoin();
		Qt::PenCapStyle bdCap = settings->getLineCap();
		setPen( QPen( color, bdWidth, bdStyle, bdCap, bdJoin ) );
		setBrush( Qt::NoBrush );

		if ( gap == 0 ) {
//			_line.moveTo( points.first() );
//			for ( int jt = 1; jt < points.size(); ++jt ) {
//				_line.lineTo( points.at(jt) );
//			}

			int it = 0;
			while ( !rect.contains( points.at(it) ) ) {
				++it;
				if ( it >= points.size() ) break;
			}
			if ( it < points.size() ) {
				_line.moveTo( points.at(it) );
			}
			for ( int jt = it; jt < points.size(); ++jt ) {
				if ( rect.contains( points.at(jt) ) )
					_line.lineTo( points.at(jt) );
				else {
					// Add moveToLine
					while ( !rect.contains( points.at(jt) ) ) {
						++jt;
						if ( jt >= points.size() ) break;
					}
					if ( jt < points.size() ) {
						_line.moveTo( points.at(jt) );
					}
					++jt;
				}
			}
		}
		else
		{
			for ( int it = 1; it < points.size(); ++it ) {
				double x0 = points.at(it-1).x();
				double y0 = points.at(it-1).y();
				double x1 = points.at(it).x();
				double y1 = points.at(it).y();
				double angle = 0;
				if ( x1 != x0 )
					angle = atan( (y1-y0)/(x1-x0) );
				double dX = gap*cos( angle );
				double dY = gap*sin( angle );
				double x0g = x0 + dX;
				double y0g = y0 + dY;
				double x1g = x1 - dX;
				double y1g = y1 - dY;
				if ( rect.contains( {(int)x0g, (int)y0g} ) &&
					 rect.contains( {(int)x1g, (int)y1g} ) ) {
					_line.moveTo( x0g, y0g );
					_line.lineTo( x1g, y1g );
				}
			}
		}

		QPainterPath cropRect;
		cropRect.addRect( rect );
//		_line = _line.intersected( cropRect );
//		QPainterPath pat;
//		for ( int it = 0; it < _line.elementCount(); ++it ) {
//			pat.addPath( _line.elementAt(it) );
//		}
		setPath( _line );
	}
}
/**********************************************/
/**********************************************/
