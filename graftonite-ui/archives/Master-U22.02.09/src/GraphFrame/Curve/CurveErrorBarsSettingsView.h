#ifndef CURVEERRORBARSSETTINGSVIEW_H
#define CURVEERRORBARSSETTINGSVIEW_H

#include <QWidget>

#include "../CurveSettings.h"
#include "Commons/theme.h"
#include "Commons/lang.h"
#include "Commons/TabWidget.h"
#include "Commons/ScrollArea.h"
#include "Commons/FormLayout.h"
#include "Commons/GridLayout.h"
#include "Commons/SettingLabel.h"
#include "Commons/LineEditor.h"
#include "Commons/IntegerSelector.h"
#include "../Misc/LineTypeSelector.h"
#include "../Misc/LineStyleSelector.h"
#include "../Misc/LineCapSelector.h"
#include "Commons/ColorSelector.h"

/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class CurveErrorBarsSettingsView
		: public ScrollArea
{
	Q_OBJECT
private:
	CurveSettings* settings = nullptr;
	SettingLabel* lab_errorBars = new SettingLabel;
	SettingLabel* lab_type = new SettingLabel;
	LineTypeSelector* sel_type = new LineTypeSelector;
	SettingLabel* lab_Error1 = new SettingLabel;
	QComboBox* sel_Error1 = new QComboBox;
	SettingLabel* lab_Error2 = new SettingLabel;
	QComboBox* sel_Error2 = new QComboBox;
	SettingLabel* lab_Error3 = new SettingLabel;
	QComboBox* sel_Error3 = new QComboBox;
	SettingLabel* lab_style = new SettingLabel;
	LineStyleSelector* sel_style = new LineStyleSelector;
	SettingLabel* lab_width = new SettingLabel;
	IntegerSelector* sel_width = new IntegerSelector;
	SettingLabel* lab_color = new SettingLabel;
	ColorSelector* sel_color = new ColorSelector;
	SettingLabel* lab_cap = new SettingLabel;
	LineCapSelector* sel_cap = new LineCapSelector;

public:
	CurveErrorBarsSettingsView( QWidget* parent = nullptr );
	void setSettings( CurveSettings* sets );
	void updateIcons( Icons* icons );
	void updateLang( Lang* lang );

public slots:
	void update();
	void disableWidget();
	void connectModifiers();
	void disconnectModifiers();
	void blockChildrenSignals( bool arg );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // CURVEERRORBARSSETTINGSVIEW_H
