#include "CurveAreaSettingsView.h"
/**********************************************/
/**********************************************/
/* */
CurveAreaSettingsView::CurveAreaSettingsView( QWidget* parent )
	: ScrollArea( parent )
{
	FormLayout* lay_sets = new FormLayout;
	lay_sets->addRow( lab_bars );
	lay_sets->addRow( lab_direc, sel_direc );
	lay_sets->addRow( lab_fill );
	lay_sets->addRow( lab_pattern, sel_pattern );
	lay_sets->addRow( lab_color, sel_color );
	lay_sets->addRow( lab_borders );
	lay_sets->addRow( lab_bdStyle, sel_bdStyle );
	sel_bdWidth->setDimension( 0, 1000 );
	lay_sets->addRow( lab_bdWidth, sel_bdWidth );
	lay_sets->addRow( lab_bdColor, sel_bdColor );
	lay_sets->addRow( lab_bdJoin, sel_bdJoin );

	QWidget* graphWid = new QWidget;
	graphWid->setLayout( lay_sets );
	setWidget( graphWid );
}
/**********************************************/
/**********************************************/
/* */
void CurveAreaSettingsView::setSettings( CurveSettings* sets ) {
	disconnectModifiers();

	settings = sets;

	connectModifiers();

	update();
}
/**********************************************/
/**********************************************/
/* */
void CurveAreaSettingsView::updateIcons( Icons* icons ) {
Q_UNUSED(icons)
}
/**********************************************/
/**********************************************/
/* */
void CurveAreaSettingsView::updateLang( Lang* lang ) {
	Q_UNUSED(lang)
	lab_bars->setText( "Bars Settings" );
	lab_direc->setText( "Direction :" );
	lab_fill->setText( "Fill" );
	lab_pattern->setText( "Pattern :" );
	lab_color->setText( "Color :" );
	lab_borders->setText( "Borders" );
	lab_bdStyle->setText( "Style :" );
	lab_bdWidth->setText( "Width :" );
	lab_bdColor->setText( "Color :" );
	lab_bdJoin->setText( "Join :" );
}
/**********************************************/
/**********************************************/
/* */
void CurveAreaSettingsView::update() {
	sel_direc->setDirection( settings->getAreaDirec() );
	sel_pattern->setPattern( settings->getAreaPattern() );
	sel_color->setColor( settings->getAreaColor() );
	sel_bdStyle->setStyle( settings->getAreaBdStyle() );
	sel_bdWidth->setValue( settings->getAreaBdWidth() );
	sel_bdColor->setColor( settings->getAreaBdColor() );
	sel_bdJoin->setStyle( settings->getAreaBdJoin() );
}
/**********************************************/
/**********************************************/
/* */
void CurveAreaSettingsView::disableWidget() {

}
/**********************************************/
/**********************************************/
/* */
void CurveAreaSettingsView::connectModifiers() {
	connect( sel_direc, &AreaDirectionSelector::changed,
				settings, &CurveSettings::setAreaDirec );
	connect( sel_pattern, &BrushPatternSelector::changed,
				settings, &CurveSettings::setAreaPattern );
	connect( sel_color, &ColorSelector::changed,
				settings, &CurveSettings::setAreaColor );
	connect( sel_bdStyle, &LineStyleSelector::changed,
				settings, &CurveSettings::setAreaBdStyle );
	connect( sel_bdWidth, &DoubleSelector::changed,
				settings, &CurveSettings::setAreaBdWidth );
	connect( sel_bdColor, &ColorSelector::changed,
				settings, &CurveSettings::setAreaBdColor );
	connect( sel_bdJoin, &LineJoinSelector::changed,
				settings, &CurveSettings::setAreaBdJoin );
}
/**********************************************/
/**********************************************/
/* */
void CurveAreaSettingsView::disconnectModifiers() {
	if ( settings != nullptr ) {
		disconnect( sel_direc, &AreaDirectionSelector::changed,
					settings, &CurveSettings::setAreaDirec );
		disconnect( sel_pattern, &BrushPatternSelector::changed,
					settings, &CurveSettings::setAreaPattern );
		disconnect( sel_color, &ColorSelector::changed,
					settings, &CurveSettings::setAreaColor );
		disconnect( sel_bdStyle, &LineStyleSelector::changed,
					settings, &CurveSettings::setAreaBdStyle );
		disconnect( sel_bdWidth, &DoubleSelector::changed,
					settings, &CurveSettings::setAreaBdWidth );
		disconnect( sel_bdColor, &ColorSelector::changed,
					settings, &CurveSettings::setAreaBdColor );
		disconnect( sel_bdJoin, &LineJoinSelector::changed,
					settings, &CurveSettings::setAreaBdJoin );
	}
}
/**********************************************/
/**********************************************/
/* */
void CurveAreaSettingsView::blockChildrenSignals( bool arg ) {
	Q_UNUSED( arg );

}
/**********************************************/
/**********************************************/
