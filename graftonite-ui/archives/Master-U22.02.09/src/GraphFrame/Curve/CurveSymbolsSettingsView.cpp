#include "CurveSymbolsSettingsView.h"
/**********************************************/
/**********************************************/
/* */
CurveSymbolsSettingsView::CurveSymbolsSettingsView( QWidget* parent )
	: ScrollArea( parent )
{
	FormLayout* lay_sets = new FormLayout;
	lay_sets->addRow( lab_symbols );
	lay_sets->addRow( lab_type, sel_type );
	lay_sets->addRow( lab_option, sel_option );
	sel_size->setDimension( 0, 1000 );
	lay_sets->addRow( lab_size, sel_size );
	sel_coverage->set( 0, 100, 1, " %", 5 );
	lay_sets->addRow( lab_coverage, sel_coverage );
	sel_angle->setAngle();
	lay_sets->addRow( lab_angle, sel_angle );
	sel_Dangle->setAngleD();
	lay_sets->addRow( lab_Dangle, sel_Dangle );
	lay_sets->addRow( lab_FillColor, sel_FillColor );
	lay_sets->addRow( lab_Borders );
	lay_sets->addRow( lab_BorderColor, sel_BorderColor );
	sel_BdWidth->setDimension( 0, 1000 );
	lay_sets->addRow( lab_BdWidth, sel_BdWidth );
	lay_sets->addRow( lab_BdJoin, sel_BdJoin );

	QWidget* graphWid = new QWidget;
	graphWid->setLayout( lay_sets );
	setWidget( graphWid );

	lab_symbols->setText( "Symbols Settings" );
	lab_type->setText( "Type :" );
	lab_option->setText( "Option :" );
	lab_size->setText( "Size :" );
	lab_coverage->setText( "Coverage :" );
	lab_angle->setText( "Angle :" );
	lab_Dangle->setText( "Angle Delta :" );
	lab_FillColor->setText( "Fill Color :" );
	lab_Borders->setText( "Borders" );
	lab_BorderColor->setText( "Color :" );
	lab_BdWidth->setText( "Width :" );
	lab_BdJoin->setText( "Join :" );
}
/**********************************************/
/**********************************************/
/* */
void CurveSymbolsSettingsView::setSettings( CurveSettings* sets )
{
	disconnectModifiers();

	settings = sets;

	connectModifiers();

	update();
}
/**********************************************/
/**********************************************/
/* */
void CurveSymbolsSettingsView::updateIcons( Icons* icons )
{
Q_UNUSED(icons)
}
/**********************************************/
/**********************************************/
/* */
void CurveSymbolsSettingsView::updateLang( Lang* lang )
{
Q_UNUSED(lang)
}
/**********************************************/
/**********************************************/
/* */
void CurveSymbolsSettingsView::update()
{
	sel_type->setType( settings->getSymbType() );
	sel_option->setText( QString::number( settings->getSymbTypeOpt() ) );
	sel_size->setValue( settings->getSymbSize() );
	sel_coverage->setValue( settings->getSymbCoverage() );
	sel_angle->setValue( settings->getSymbAngle() );
	sel_Dangle->setValue( settings->getSymbDAngle() );
	sel_FillColor->setColor( settings->getSymbColor() );
	sel_BorderColor->setColor( settings->getSymbBdColor() );
	sel_BdWidth->setValue( settings->getSymbBdWidth() );
	sel_BdJoin->setStyle( settings->getSymbBdJoin() );
}
/**********************************************/
/**********************************************/
/* */
void CurveSymbolsSettingsView::disableWidget()
{
}
/**********************************************/
/**********************************************/
/* */
void CurveSymbolsSettingsView::connectModifiers()
{
	connect( sel_type, &SymbolsTypeSelector::changed,
			 settings, &CurveSettings::setSymbType );
	connect( sel_option, &LineEditor::editingFinished,
			 this, &CurveSymbolsSettingsView::changeTypeOption );
	connect( sel_size, &DoubleSelector::changed,
			 settings, &CurveSettings::setSymbSize );
	connect( sel_coverage, &DoubleSelector::changed,
			 settings, &CurveSettings::setSymbCoverage );
	connect( sel_angle, &DoubleSelector::changed,
			 settings, &CurveSettings::setSymbAngle );
	connect( sel_Dangle, &DoubleSelector::changed,
			 settings, &CurveSettings::setSymbDAngle );
	connect( sel_FillColor, &ColorSelector::changed,
			 settings, &CurveSettings::setSymbColor );
	connect( sel_BorderColor, &ColorSelector::changed,
			 settings, &CurveSettings::setSymbBdColor );
	connect( sel_BdWidth, &DoubleSelector::changed,
			 settings, &CurveSettings::setSymbBdWidth );
	connect( sel_BdJoin, &LineJoinSelector::changed,
			 settings, &CurveSettings::setSymbBdJoin );
}
/**********************************************/
/**********************************************/
/* */
void CurveSymbolsSettingsView::disconnectModifiers()
{
	if ( settings != nullptr )
	{
		disconnect( sel_type, &SymbolsTypeSelector::changed,
					settings, &CurveSettings::setSymbType );
		disconnect( sel_option, &LineEditor::editingFinished,
					this, &CurveSymbolsSettingsView::changeTypeOption );
		disconnect( sel_size, &DoubleSelector::changed,
					settings, &CurveSettings::setSymbSize );
		disconnect( sel_coverage, &DoubleSelector::changed,
					settings, &CurveSettings::setSymbCoverage );
		disconnect( sel_angle, &DoubleSelector::changed,
					settings, &CurveSettings::setSymbAngle );
		disconnect( sel_Dangle, &DoubleSelector::changed,
					settings, &CurveSettings::setSymbDAngle );
		disconnect( sel_FillColor, &ColorSelector::changed,
					settings, &CurveSettings::setSymbColor );
		disconnect( sel_BorderColor, &ColorSelector::changed,
					settings, &CurveSettings::setSymbBdColor );
		disconnect( sel_BdWidth, &DoubleSelector::changed,
					settings, &CurveSettings::setSymbBdWidth );
		disconnect( sel_BdJoin, &LineJoinSelector::changed,
					settings, &CurveSettings::setSymbBdJoin );
	}
}
/**********************************************/
/**********************************************/
/* */
void CurveSymbolsSettingsView::blockChildrenSignals( bool arg )
{
	Q_UNUSED( arg );

}
/**********************************************/
/**********************************************/
/* */
void CurveSymbolsSettingsView::changeTypeOption()
{
	int value = sel_option->text().toInt();
	if ( value < 2) {
		value = 2;
		sel_option->setText( "2" );
	}
	settings->setSymbTypeOpt( value );
}
/**********************************************/
/**********************************************/
