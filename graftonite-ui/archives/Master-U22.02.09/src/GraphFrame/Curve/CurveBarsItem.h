#ifndef CURVEBARSITEM_H
#define CURVEBARSITEM_H

#include <QGraphicsPathItem>

#include "../CurveSettings.h"
#include "../Misc/GraphMisc.h"

/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class CurveBarsItem
		: public QObject, public QGraphicsPathItem
{
	Q_OBJECT
private:
	QPainterPath _area;

public:
	CurveBarsItem( QGraphicsItem *parent = nullptr );

public slots:
	void update( CurveSettings *settings );

};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // CURVEBARSITEM_H
