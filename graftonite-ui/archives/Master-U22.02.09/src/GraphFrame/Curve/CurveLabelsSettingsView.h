#ifndef CURVELABELSSETTINGSVIEW_H
#define CURVELABELSSETTINGSVIEW_H

#include "Commons/ScrollArea.h"
#include "../CurveSettings.h"
#include "Commons/theme.h"
#include "Commons/lang.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class CurveLabelsSettingsView
		: public ScrollArea
{
public:
	CurveLabelsSettingsView( QWidget* parent = nullptr );
	void setSettings( CurveSettings* sets );
	void updateIcons( Icons* icons );
	void updateLang( Lang* lang );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // CURVELABELSSETTINGSVIEW_H
