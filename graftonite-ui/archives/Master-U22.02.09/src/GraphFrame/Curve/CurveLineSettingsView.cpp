#include "CurveLineSettingsView.h"
/**********************************************/
/**********************************************/
/* */
CurveLineSettingsView::CurveLineSettingsView( QWidget* parent )
	: ScrollArea( parent )
{
	FormLayout* lay_sets = new FormLayout;
	lay_sets->addRow( lab_line );
	lay_sets->addRow( lab_type, sel_type );
	sel_gap->setDimension( 0, 1000 );
	lay_sets->addRow( lab_gap, sel_gap );
	lay_sets->addRow( lab_lineStyle );
	lay_sets->addRow( lab_style, sel_style );
	sel_width->setDimension( 0, 1000 );
	lay_sets->addRow( lab_width, sel_width );
	lay_sets->addRow( lab_color, sel_color );
	lay_sets->addRow( lab_cap, sel_cap );
	lay_sets->addRow( lab_join, sel_join );

	QWidget* graphWid = new QWidget;
	graphWid->setLayout( lay_sets );
	setWidget( graphWid );
}
/**********************************************/
/**********************************************/
/* */
void CurveLineSettingsView::setSettings( CurveSettings* sets )
{
	disconnectModifiers();

	settings = sets;

	connectModifiers();

	update();
}
/**********************************************/
/**********************************************/
/* */
void CurveLineSettingsView::updateIcons( Icons* icons )
{
Q_UNUSED(icons)
}
/**********************************************/
/**********************************************/
/* */
void CurveLineSettingsView::updateLang( Lang* lang )
{
	Q_UNUSED(lang)
	lab_line->setText( "Line Settings" );
	lab_type->setText( "Type :" );
	lab_gap->setText( "Gap :" );
	lab_lineStyle->setText( "Line Style" );
	lab_style->setText( "Style :" );
	lab_width->setText( "Width :" );
	lab_color->setText( "Color :" );
	lab_cap->setText( "Cap :" );
	lab_join->setText( "Join :" );
}
/**********************************************/
/**********************************************/
/* */
void CurveLineSettingsView::update()
{
	sel_type->setType( settings->getLineType() );
	sel_gap->setValue( settings->getLineGap() );
	sel_style->setStyle( settings->getLineStyle() );
	sel_width->setValue( settings->getLineWidth() );
	sel_color->setColor( settings->getLineColor() );
	sel_cap->setStyle( settings->getLineCap() );
	sel_join->setStyle( settings->getLineJoin() );
}
/**********************************************/
/**********************************************/
/* */
void CurveLineSettingsView::disableWidget()
{

}
/**********************************************/
/**********************************************/
/* */
void CurveLineSettingsView::connectModifiers()
{
	connect( sel_type, &CurveLineTypeSelector::changed,
			 settings, &CurveSettings::setLineType );
	connect( sel_gap, &DoubleSelector::changed,
			 settings, &CurveSettings::setLineGap );
	connect( sel_style, &LineStyleSelector::changed,
			 settings, &CurveSettings::setLineStyle );
	connect( sel_width, &DoubleSelector::changed,
			 settings, &CurveSettings::setLineWidth );
	connect( sel_color, &ColorSelector::changed,
			 settings, &CurveSettings::setLineColor );
	connect( sel_cap, &LineCapSelector::changed,
			 settings, &CurveSettings::setLineCap );
	connect( sel_join, &LineJoinSelector::changed,
			 settings, &CurveSettings::setLineJoin );
}
/**********************************************/
/**********************************************/
/* */
void CurveLineSettingsView::disconnectModifiers()
{
	if ( settings != nullptr ) {
		disconnect( sel_type, &CurveLineTypeSelector::changed,
					settings, &CurveSettings::setLineType );
		disconnect( sel_gap, &DoubleSelector::changed,
					settings, &CurveSettings::setLineGap );
		disconnect( sel_style, &LineStyleSelector::changed,
					settings, &CurveSettings::setLineStyle );
		disconnect( sel_width, &DoubleSelector::changed,
					settings, &CurveSettings::setLineWidth );
		disconnect( sel_color, &ColorSelector::changed,
					settings, &CurveSettings::setLineColor );
		disconnect( sel_cap, &LineCapSelector::changed,
					settings, &CurveSettings::setLineCap );
		disconnect( sel_join, &LineJoinSelector::changed,
					settings, &CurveSettings::setLineJoin );
	}
}
/**********************************************/
/**********************************************/
/* */
void CurveLineSettingsView::blockChildrenSignals( bool arg )
{
	Q_UNUSED( arg );

}
/**********************************************/
/**********************************************/
