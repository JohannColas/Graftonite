#ifndef CURVELINEITEM_H
#define CURVELINEITEM_H

#include <QGraphicsPathItem>

#include "../CurveSettings.h"
#include "../Misc/GraphMisc.h"
#include "../Misc/CurveLineTypeSelector.h"
#include <QPen>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class CurveLineItem
		: public QObject, public QGraphicsPathItem
{
	Q_OBJECT
private:
	QPainterPath _line;

public:
	CurveLineItem( QGraphicsItem *parent = nullptr );

public slots:
	void update( CurveSettings *settings );
//	void updateSymbol( double posX, double posY, CurveSettings* settings );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // CURVELINEITEM_H
