#ifndef CURVELINESETTINGSVIEW_H
#define CURVELINESETTINGSVIEW_H

#include <QWidget>

#include "../CurveSettings.h"
#include "Commons/theme.h"
#include "Commons/lang.h"
#include "Commons/TabWidget.h"
#include "Commons/ScrollArea.h"
#include "Commons/FormLayout.h"
#include "Commons/GridLayout.h"
#include "Commons/SettingLabel.h"
#include "Commons/LineEditor.h"
#include "Commons/IntegerSelector.h"
#include "Commons/DoubleSelector.h"
#include "../Misc/CurveLineTypeSelector.h"
#include "../Misc/LineTypeSelector.h"
#include "../Misc/LineStyleSelector.h"
#include "../Misc/LineCapSelector.h"
#include "../Misc/LineJoinSelector.h"
#include "Commons/ColorSelector.h"

/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class CurveLineSettingsView
		: public ScrollArea
{
	Q_OBJECT
private:
	CurveSettings* settings = nullptr;
	SettingLabel* lab_line = new SettingLabel;
	SettingLabel* lab_type = new SettingLabel;
	CurveLineTypeSelector* sel_type = new CurveLineTypeSelector;
	SettingLabel* lab_gap = new SettingLabel;
	DoubleSelector* sel_gap = new DoubleSelector;
	SettingLabel* lab_lineStyle = new SettingLabel;
	SettingLabel* lab_style = new SettingLabel;
	LineStyleSelector* sel_style = new LineStyleSelector;
	SettingLabel* lab_width = new SettingLabel;
	DoubleSelector* sel_width = new DoubleSelector;
	SettingLabel* lab_color = new SettingLabel;
	ColorSelector* sel_color = new ColorSelector;
	SettingLabel* lab_cap = new SettingLabel;
	LineCapSelector* sel_cap = new LineCapSelector;
	SettingLabel* lab_join = new SettingLabel;
	LineJoinSelector* sel_join = new LineJoinSelector;

public:
	CurveLineSettingsView( QWidget* parent = nullptr );
	void setSettings( CurveSettings* sets );
	void updateIcons( Icons* icons );
	void updateLang( Lang* lang );

public slots:
	void update();
	void disableWidget();
	void connectModifiers();
	void disconnectModifiers();
	void blockChildrenSignals( bool arg );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // CURVELINESETTINGSVIEW_H
