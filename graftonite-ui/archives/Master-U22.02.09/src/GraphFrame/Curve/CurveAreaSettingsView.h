#ifndef CURVEAREASETTINGSVIEW_H
#define CURVEAREASETTINGSVIEW_H

#include <QWidget>

#include "../CurveSettings.h"
#include "Commons/theme.h"
#include "Commons/lang.h"
#include "Commons/TabWidget.h"
#include "Commons/ScrollArea.h"
#include "Commons/FormLayout.h"
#include "Commons/GridLayout.h"
#include "Commons/SettingLabel.h"
#include "Commons/LineEditor.h"
#include "Commons/DoubleSelector.h"
#include "../Misc/AreaDirection.h"
#include "../Misc/BrushPattern.h"
#include "../Misc/LineTypeSelector.h"
#include "../Misc/LineStyleSelector.h"
#include "../Misc/LineJoinSelector.h"
#include "Commons/ColorSelector.h"

/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class CurveAreaSettingsView
		: public ScrollArea
{
	Q_OBJECT
private:
	CurveSettings* settings = nullptr;
	SettingLabel* lab_bars = new SettingLabel;
	SettingLabel* lab_direc = new SettingLabel;
	AreaDirectionSelector* sel_direc = new AreaDirectionSelector;
	SettingLabel* lab_fill = new SettingLabel;
	SettingLabel* lab_pattern = new SettingLabel;
	BrushPatternSelector* sel_pattern = new BrushPatternSelector;
	SettingLabel* lab_color = new SettingLabel;
	ColorSelector* sel_color = new ColorSelector;
	SettingLabel* lab_borders = new SettingLabel;
	SettingLabel* lab_bdStyle = new SettingLabel;
	LineStyleSelector* sel_bdStyle = new LineStyleSelector;
	SettingLabel* lab_bdWidth = new SettingLabel;
	DoubleSelector* sel_bdWidth = new DoubleSelector;
	SettingLabel* lab_bdColor = new SettingLabel;
	ColorSelector* sel_bdColor = new ColorSelector;
	SettingLabel* lab_bdJoin = new SettingLabel;
	LineJoinSelector* sel_bdJoin = new LineJoinSelector;

public:
	CurveAreaSettingsView( QWidget* parent = nullptr );
	void setSettings( CurveSettings* sets );
	void updateIcons( Icons* icons );
	void updateLang( Lang* lang );

public slots:
	void update();
	void disableWidget();
	void connectModifiers();
	void disconnectModifiers();
	void blockChildrenSignals( bool arg );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // CURVEAREASETTINGSVIEW_H
