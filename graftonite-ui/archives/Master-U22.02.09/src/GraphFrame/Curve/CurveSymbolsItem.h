#ifndef CURVESYMBOLSITEM_H
#define CURVESYMBOLSITEM_H

#include <QGraphicsPathItem>

#include "../CurveSettings.h"
#include "../Misc/GraphMisc.h"
#include <QPen>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class CurveSymbolsItem
		: public QObject, public QGraphicsPathItem
{
	Q_OBJECT
private:
//	CurveSettings* settings = nullptr;
	QPainterPath _symbols;

public:
	~CurveSymbolsItem();
	CurveSymbolsItem( QGraphicsItem *parent = nullptr );

public slots:
	void update( CurveSettings *settings );
	void updateSymbol( double posX, double posY, CurveSettings* settings );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // CURVESYMBOLSITEM_H
