#include "CurveLabelsSettingsView.h"
/**********************************************/
/**********************************************/
/* */
CurveLabelsSettingsView::CurveLabelsSettingsView( QWidget* parent )
	: ScrollArea( parent )
{

}
/**********************************************/
/**********************************************/
/* */
void CurveLabelsSettingsView::setSettings( CurveSettings* sets )
{
	Q_UNUSED(sets)
}
/**********************************************/
/**********************************************/
/* */
void CurveLabelsSettingsView::updateIcons( Icons* icons )
{
Q_UNUSED(icons)
}
/**********************************************/
/**********************************************/
/* */
void CurveLabelsSettingsView::updateLang( Lang* lang )
{
Q_UNUSED(lang)
}
/**********************************************/
/**********************************************/
