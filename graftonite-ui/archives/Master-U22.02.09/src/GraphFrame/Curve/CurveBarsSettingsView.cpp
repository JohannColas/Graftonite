#include "CurveBarsSettingsView.h"
/**********************************************/
/**********************************************/
/* */
CurveBarsSettingsView::CurveBarsSettingsView( QWidget* parent )
	: ScrollArea( parent )
{
	FormLayout* lay_sets = new FormLayout;
	lay_sets->addRow( lab_bars );
	lay_sets->addRow( lab_direc, sel_direc );
	sel_width->setDimension( 0, 1000 );
	lay_sets->addRow( lab_width, sel_width );
	lay_sets->addRow( lab_fill );
	lay_sets->addRow( lab_pattern, sel_pattern );
	lay_sets->addRow( lab_color, sel_color );
	lay_sets->addRow( lab_borders );
	lay_sets->addRow( lab_bdStyle, sel_bdStyle );
	sel_bdWidth->setDimension( 0, 1000 );
	lay_sets->addRow( lab_bdWidth, sel_bdWidth );
	lay_sets->addRow( lab_bdColor, sel_bdColor );
	lay_sets->addRow( lab_bdJoin, sel_bdJoin );

	QWidget* graphWid = new QWidget;
	graphWid->setLayout( lay_sets );
	setWidget( graphWid );
}
/**********************************************/
/**********************************************/
/* */
void CurveBarsSettingsView::setSettings( CurveSettings* sets ) {
	disconnectModifiers();

	settings = sets;

	connectModifiers();

	update();
}
/**********************************************/
/**********************************************/
/* */
void CurveBarsSettingsView::updateIcons( Icons* icons ) {
	Q_UNUSED(icons)
}
/**********************************************/
/**********************************************/
/* */
void CurveBarsSettingsView::updateLang( Lang* lang ) {
	Q_UNUSED(lang)
	lab_bars->setText( "Bars Settings" );
	lab_direc->setText( "Direction :" );
	lab_width->setText( "Width :" );
	lab_fill->setText( "Fill" );
	lab_pattern->setText( "Pattern :" );
	lab_color->setText( "Color :" );
	lab_borders->setText( "Borders" );
	lab_bdStyle->setText( "Style :" );
	lab_bdWidth->setText( "Width :" );
	lab_bdColor->setText( "Color :" );
	lab_bdJoin->setText( "Join :" );
}
/**********************************************/
/**********************************************/
/* */
void CurveBarsSettingsView::update() {
	sel_direc->setDirection( settings->getBarsDirec() );
	sel_width->setValue( settings->getBarsWidth() );
	sel_pattern->setPattern( settings->getBarsPattern() );
	sel_color->setColor( settings->getBarsColor() );
	sel_bdStyle->setStyle( settings->getBarsBdStyle() );
	sel_bdWidth->setValue( settings->getBarsBdWidth() );
	sel_bdColor->setColor( settings->getBarsBdColor() );
	sel_bdJoin->setStyle( settings->getBarsBdJoin() );
}
/**********************************************/
/**********************************************/
/* */
void CurveBarsSettingsView::disableWidget() {

}
/**********************************************/
/**********************************************/
/* */
void CurveBarsSettingsView::connectModifiers()
{
	connect( sel_direc, &BarsDirectionSelector::changed,
			 settings, &CurveSettings::setBarsDirec );
	connect( sel_width, &DoubleSelector::changed,
			 settings, &CurveSettings::setBarsWidth );
	connect( sel_pattern, &BrushPatternSelector::changed,
			 settings, &CurveSettings::setBarsPattern );
	connect( sel_color, &ColorSelector::changed,
			 settings, &CurveSettings::setBarsColor );
	connect( sel_bdStyle, &LineStyleSelector::changed,
			 settings, &CurveSettings::setBarsBdStyle );
	connect( sel_bdWidth, &DoubleSelector::changed,
			 settings, &CurveSettings::setBarsBdWidth );
	connect( sel_bdColor, &ColorSelector::changed,
			 settings, &CurveSettings::setBarsBdColor );
	connect( sel_bdJoin, &LineJoinSelector::changed,
			 settings, &CurveSettings::setBarsBdJoin );
}
/**********************************************/
/**********************************************/
/* */
void CurveBarsSettingsView::disconnectModifiers()
{
	if ( settings != nullptr ) {
		disconnect( sel_direc, &BarsDirectionSelector::changed,
					settings, &CurveSettings::setBarsDirec );
		disconnect( sel_width, &DoubleSelector::changed,
					settings, &CurveSettings::setBarsWidth );
		disconnect( sel_pattern, &BrushPatternSelector::changed,
					settings, &CurveSettings::setBarsPattern );
		disconnect( sel_color, &ColorSelector::changed,
					settings, &CurveSettings::setBarsColor );
		disconnect( sel_bdStyle, &LineStyleSelector::changed,
					settings, &CurveSettings::setBarsBdStyle );
		disconnect( sel_bdWidth, &DoubleSelector::changed,
					settings, &CurveSettings::setBarsBdWidth );
		disconnect( sel_bdColor, &ColorSelector::changed,
					settings, &CurveSettings::setBarsBdColor );
		disconnect( sel_bdJoin, &LineJoinSelector::changed,
					settings, &CurveSettings::setBarsBdJoin );
	}
}
/**********************************************/
/**********************************************/
/* */
void CurveBarsSettingsView::blockChildrenSignals( bool arg )
{
	Q_UNUSED( arg );
}
/**********************************************/
/**********************************************/
