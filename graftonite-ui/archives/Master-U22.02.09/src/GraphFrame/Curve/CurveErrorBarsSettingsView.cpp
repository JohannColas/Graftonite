#include "CurveErrorBarsSettingsView.h"
/**********************************************/
/**********************************************/
/* */
CurveErrorBarsSettingsView::CurveErrorBarsSettingsView( QWidget* parent )
	: ScrollArea( parent )
{
	FormLayout* lay_sets = new FormLayout;
	lay_sets->addRow( lab_errorBars );
	lay_sets->addRow( lab_type, sel_type );
	lay_sets->addRow( lab_Error1, sel_Error1 );
	lay_sets->addRow( lab_Error2, sel_Error2 );
	lay_sets->addRow( lab_Error3, sel_Error3 );
	lay_sets->addRow( lab_style, sel_style );
	sel_width->setDimension( 0, 1000 );
	lay_sets->addRow( lab_width, sel_width );
	lay_sets->addRow( lab_color, sel_color );
	lay_sets->addRow( lab_cap, sel_cap );

	QWidget* graphWid = new QWidget;
	graphWid->setLayout( lay_sets );
	setWidget( graphWid );
}
/**********************************************/
/**********************************************/
/* */
void CurveErrorBarsSettingsView::setSettings( CurveSettings* sets )
{
	disconnectModifiers();

	settings = sets;

	connectModifiers();
}
/**********************************************/
/**********************************************/
/* */
void CurveErrorBarsSettingsView::updateIcons( Icons* icons )
{
Q_UNUSED(icons)
}
/**********************************************/
/**********************************************/
/* */
void CurveErrorBarsSettingsView::updateLang( Lang* lang )
{
	Q_UNUSED(lang)
	lab_errorBars->setText( "Error Bars Settings" );
	lab_type->setText( "Type :" );
	lab_Error1->setText( "Error 1 :" );
	lab_Error2->setText( "Error 2 :" );
	lab_Error3->setText( "Error 3 :" );
	lab_style->setText( "Style :" );
	lab_width->setText( "Width :" );
	lab_color->setText( "Color :" );
	lab_cap->setText( "Cap :" );
}
/**********************************************/
/**********************************************/
/* */
void CurveErrorBarsSettingsView::update()
{

}
/**********************************************/
/**********************************************/
/* */
void CurveErrorBarsSettingsView::disableWidget()
{

}
/**********************************************/
/**********************************************/
/* */
void CurveErrorBarsSettingsView::connectModifiers()
{

}
/**********************************************/
/**********************************************/
/* */
void CurveErrorBarsSettingsView::disconnectModifiers()
{

}
/**********************************************/
/**********************************************/
/* */
void CurveErrorBarsSettingsView::blockChildrenSignals( bool arg )
{
	Q_UNUSED( arg );

}
/**********************************************/
/**********************************************/
