#ifndef LAYERSETTINGSVIEW_H
#define LAYERSETTINGSVIEW_H

#include <QWidget>

#include "LayerSettings.h"
#include "Commons/theme.h"
#include "Commons/lang.h"
#include "Commons/TabWidget.h"
#include "Commons/ScrollArea.h"
#include "Commons/FormLayout.h"
#include "Commons/GridLayout.h"
#include "Commons/SettingLabel.h"
#include "Commons/LineEditor.h"
#include "Commons/IntegerSelector.h"
#include "Misc/LineStyleSelector.h"
#include "Misc/LineJoinSelector.h"
#include "Commons/ColorSelector.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class LayerSettingsView : public QWidget
{
    Q_OBJECT
private:
	LayerSettings* settings = nullptr;
	// Layer Settings
	LineEditor* sel_ID = new LineEditor;
	ScrollArea* wid_layerSettings = new ScrollArea;
	SettingLabel* lab_X = new SettingLabel;
	IntegerSelector* sel_X = new IntegerSelector;
	SettingLabel* lab_Y = new SettingLabel;
	IntegerSelector* sel_Y = new IntegerSelector;
	SettingLabel* lab_Width = new SettingLabel;
	IntegerSelector* sel_Width = new IntegerSelector;
	SettingLabel* lab_Height = new SettingLabel;
	IntegerSelector* sel_Height = new IntegerSelector;
	SettingLabel* lab_Background = new SettingLabel;
	SettingLabel* lab_BkColor = new SettingLabel;
	ColorSelector* sel_BkColor = new ColorSelector;
	SettingLabel* lab_Borders = new SettingLabel;
	SettingLabel* lab_BdStyle = new SettingLabel;
	LineStyleSelector* sel_BdStyle = new LineStyleSelector;
	SettingLabel* lab_BdWidth = new SettingLabel;
	IntegerSelector* sel_BdWidth = new IntegerSelector;
	SettingLabel* lab_BdColor = new SettingLabel;
	ColorSelector* sel_BdColor = new ColorSelector;
	SettingLabel* lab_BdJoin = new SettingLabel;
	LineJoinSelector* sel_BdJoin = new LineJoinSelector;

public:
    explicit LayerSettingsView(QWidget *parent = nullptr);
    ~LayerSettingsView();
	LayerSettings *getSettings() const;
	void setSettings( LayerSettings *value );
	void updateIcons( Icons* icons );
	void updateLang( Lang* lang );

public slots:
	void update();
	void connectModifiers();
	void disconnectModifiers();

private slots:
	void editingIDisFinished();

};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LAYERSETTINGSVIEW_H
