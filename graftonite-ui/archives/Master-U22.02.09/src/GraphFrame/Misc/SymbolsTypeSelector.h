#ifndef SYMBOLSTYPESELECTOR_H
#define SYMBOLSTYPESELECTOR_H

#include <QComboBox>

#include "../Curve.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class SymbolsTypeSelector
		: public QComboBox
{
	Q_OBJECT
public:
	~SymbolsTypeSelector()
	{

	}
	SymbolsTypeSelector( QWidget* parent = nullptr )
		: QComboBox( parent )
	{
		addItems( { "No Symbols", "Cercle", "Square", "Triangle", "Arrow", "Polygon", "Star" } );
		setMaximumWidth( 120 );
		setSizeAdjustPolicy( QComboBox::AdjustToContents );
		connect( this, SIGNAL(currentIndexChanged(int)),
				 this, SLOT(setTypeInd(int)) );
	}

	void setType( const Curve::Symbol& type )
	{
		int ind = 0;
		if ( type == Curve::CERCLE )
			ind = 1;
		else if ( type == Curve::SQUARE )
			ind = 2;
		else if ( type == Curve::TRIANGLE )
			ind = 3;
		else if ( type == Curve::ARROW )
			ind = 4;
		else if ( type == Curve::POLYGON )
			ind = 5;
		else if ( type == Curve::STAR )
			ind = 6;

		setCurrentIndex( ind );
	}

public slots:
	void setTypeInd( int index )
	{
		Curve::Symbol sett = Curve::NOSYMBOL;
		switch ( index ) {
			case 1:
				sett = Curve::CERCLE;
			break;
			case 2:
				sett = Curve::SQUARE;
			break;
			case 3:
				sett = Curve::TRIANGLE;
			break;
			case 4:
				sett = Curve::ARROW;
			break;
			case 5:
				sett = Curve::POLYGON;
			break;
			case 6:
				sett = Curve::STAR;
			break;
		}
		emit changed( sett );
	}

signals:
	void changed( const Curve::Symbol& type );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SYMBOLSTYPESELECTOR_H
