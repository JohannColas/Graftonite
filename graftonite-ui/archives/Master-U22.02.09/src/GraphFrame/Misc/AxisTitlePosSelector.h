#ifndef AXISTITLEPOSSELECTOR_H
#define AXISTITLEPOSSELECTOR_H

#include <QComboBox>
#include "Commons/lang.h"
#include "../AxisSettings.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class AxisTitlePosSelector
		: public QComboBox
{
	Q_OBJECT
private:
	Axis::Type type = Axis::XAxis;
	Lang* _lang = nullptr;

public:
	~AxisTitlePosSelector()
	{

	}
	AxisTitlePosSelector( QWidget* parent = nullptr )
		: QComboBox( parent )
	{
		addItems( {"", "", "", "", "", "", ""} );
		setMaximumWidth( 120 );
		setSizeAdjustPolicy( QComboBox::AdjustToContents );
		connect( this, SIGNAL(currentIndexChanged(int)),
				 this, SLOT(setPosInd(int)) );
	}
	void updateLang( Lang* lang ) {
		setItemText( 0, lang->get("noTitle") );
		_lang = lang;
	}
	void updateItems( const Axis::Type& axType )
	{
		type = axType;
		switch( axType )
		{
			case Axis::XAxis:
				setItemText( 1, _lang->get("anchBotLef") );
				setItemText( 2, _lang->get("anchBotCen") );
				setItemText( 3, _lang->get("anchBotRig") );
				setItemText( 4, _lang->get("anchTopLef") );
				setItemText( 5, _lang->get("anchTopCen") );
				setItemText( 6, _lang->get("anchTopRig") );
			break;
			case Axis::YAxis:
				setItemText( 1, _lang->get("leftTop") );
				setItemText( 2, _lang->get("leftMiddle") );
				setItemText( 3, _lang->get("leftBottom") );
				setItemText( 4, _lang->get("rightTop") );
				setItemText( 5, _lang->get("rightMiddle") );
				setItemText( 6, _lang->get("rightBottom")  );
			break;
			case Axis::RadAxis:
				setItemText( 1, "" );
				setItemText( 2, "" );
				setItemText( 3, "" );
				setItemText( 4, "" );
				setItemText( 5, "" );
				setItemText( 6, "" );
			break;
			case Axis::AngAxis:
				setItemText( 1, "" );
				setItemText( 2, "" );
				setItemText( 3, "" );
				setItemText( 4, "" );
				setItemText( 5, "" );
				setItemText( 6, "" );
			break;
			case Axis::RdrAxis:
				setItemText( 1, "" );
				setItemText( 2, "" );
				setItemText( 3, "" );
				setItemText( 4, "" );
				setItemText( 5, "" );
				setItemText( 6, "" );
			break;
		}
	}
	void setPos( const Axis::Pos& pos )
	{
		int ind = 0;
		if ( pos == Axis::BOTTOMLEFT ) {
			ind = 1;
			if( type == Axis::YAxis)
				ind = 3;
		}
		else if ( pos == Axis::BOTTOMCENTER )
			ind = 2;
		else if ( pos == Axis::BOTTOMRIGHT ) {
			ind = 3;
			if( type == Axis::YAxis)
				ind = 6;
		}
		else if ( pos == Axis::TOPLEFT ) {
			ind = 4;
			if( type == Axis::YAxis)
				ind = 1;
		}
		else if ( pos == Axis::TOPCENTER )
			ind = 5;
		else if ( pos == Axis::TOPRIGHT ) {
			ind = 6;
			if( type == Axis::YAxis)
				ind = 4;
		}
		else if ( pos == Axis::MIDDLELEFT )
			ind = 2;
		else if ( pos == Axis::MIDDLERIGHT )
			ind = 5;

		setCurrentIndex( ind );
	}

public slots:
	void setPosInd( int index )
	{
		Axis::Pos sett = Axis::NONE;
		switch ( index )
		{
			case 1:
				sett = Axis::BOTTOMLEFT;
				if( type == Axis::YAxis )
					sett = Axis::TOPLEFT;
			break;
			case 2:
				sett = Axis::BOTTOMCENTER;
				if( type == Axis::YAxis )
					sett = Axis::MIDDLELEFT;
			break;
			case 3:
				sett = Axis::BOTTOMRIGHT;
				if( type == Axis::YAxis )
					sett = Axis::TOPRIGHT;
			break;
			case 4:
				sett = Axis::TOPLEFT;
				if( type == Axis::YAxis )
					sett = Axis::TOPRIGHT;
			break;
			case 5:
				sett = Axis::TOPCENTER;
				if( type == Axis::YAxis )
					sett = Axis::MIDDLERIGHT;
			break;
			case 6:
				sett = Axis::TOPRIGHT;
				if( type == Axis::YAxis )
					sett = Axis::BOTTOMRIGHT;
			break;
		}
		emit changed( sett );
	}

signals:
	void changed( const Axis::Pos& currentAxPos );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // AXISTITLEPOSSELECTOR_H
