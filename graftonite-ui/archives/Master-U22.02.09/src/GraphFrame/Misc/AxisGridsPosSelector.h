#ifndef AXISGRIDSPOSSELECTOR_H
#define AXISGRIDSPOSSELECTOR_H

#include <QComboBox>
#include "Commons/lang.h"

#include "../AxisSettings.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class AxisGridsPosSelector
		: public QComboBox
{
	Q_OBJECT
private:
	Axis::Type type = Axis::XAxis;
	Lang* _lang = nullptr;

public:
	~AxisGridsPosSelector()
	{

	}
	AxisGridsPosSelector( QWidget* parent = nullptr )
		: QComboBox( parent )
	{
		addItems( {"No Grids", "Both Sides", "Top to Axis", "Bottom to Axis"} );
		setMaximumWidth( 120 );
		setSizeAdjustPolicy( QComboBox::AdjustToContents );
		connect( this, SIGNAL(currentIndexChanged(int)),
				 this, SLOT(setPosInd(int)) );
	}
	void updateLang( Lang* lang ) {
		_lang = lang;
		setItemText( 0, lang->get("noGrids") );
		setItemText( 1, lang->get("bothSides") );
	}
	void updateItems( const Axis::Type& axType )
	{
		type = axType;
		switch( axType )
		{
			case Axis::XAxis:
				setItemText( 2, _lang->get("topToAxis") );
				setItemText( 3, _lang->get("bottomToAxis") );
			break;
			case Axis::YAxis:
				setItemText( 2, _lang->get("rightToAxis") );
				setItemText( 3, _lang->get("leftToAxis") );
			break;
			case Axis::RadAxis:
				setItemText( 2, "" );
				setItemText( 3, "" );
			break;
			case Axis::AngAxis:
				setItemText( 2, "" );
				setItemText( 3, "" );
			break;
			case Axis::RdrAxis:
				setItemText( 2, "" );
				setItemText( 3, "" );
			break;
		}
	}
	void setPos( const Axis::Pos& pos )
	{
		int ind = 0;
		if ( pos == Axis::BOTH )
			ind = 1;
		else if ( pos == Axis::TOP )
			ind = 2;
		else if ( pos == Axis::RIGHT )
			ind = 2;
		else if ( pos == Axis::BOTTOM )
			ind = 3;
		else if ( pos == Axis::LEFT )
			ind = 3;

		setCurrentIndex( ind );
	}

public slots:
	void setPosInd( int index )
	{
		Axis::Pos sett = Axis::NONE;
		switch ( index )
		{
			case 1:
				sett = Axis::BOTH;
			break;
			case 2:
				sett = Axis::TOP;
				if( type == Axis::YAxis )
					sett = Axis::RIGHT;
			break;
			case 3:
				sett = Axis::BOTTOM;
				if( type == Axis::YAxis )
					sett = Axis::LEFT;
			break;
		}
		emit changed( sett );
	}

signals:
	void changed( const Axis::Pos& currentPos );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // AXISGRIDSPOSSELECTOR_H
