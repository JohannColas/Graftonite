#ifndef COLORDIALOG_H
#define COLORDIALOG_H

#include <QColorDialog>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class ColorDialog : public QColorDialog
{
	Q_OBJECT
protected:
	QColor color = { 255, 255, 255, 255 };


public:
	~ColorDialog()
	{

	}
	ColorDialog( QColor& initialColor )
		: QColorDialog( initialColor )
	{
		color = initialColor;

	}
	QColor getColor()
	{
		return color;
	}



};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // COLORDIALOG_H
