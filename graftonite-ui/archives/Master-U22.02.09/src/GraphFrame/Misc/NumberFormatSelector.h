#ifndef NUMBERFORMATSELECTION_H
#define NUMBERFORMATSELECTION_H

#include <QComboBox>
#include "Commons/lang.h"

#include "../AxisSettings.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class NumberFormatSelector
		: public QComboBox
{
	Q_OBJECT
//	AxisPos type = AXISPOS::NONE;
public:
	~NumberFormatSelector()
	{

	}
	NumberFormatSelector( QWidget* parent = nullptr )
		: QComboBox( parent )
	{
		addItems( {"Default", "Number", "Scientific", "Power Of", "Multiple", "Time", "Date", "Time & Date"} );
		setMaximumWidth( 120 );
		setSizeAdjustPolicy( QComboBox::AdjustToContents );
		connect( this, SIGNAL(currentIndexChanged(int)),
				 this, SLOT(setFormatInd(int)) );
	}
	void updateLang( Lang* lang ) {
		setItemText( 0, lang->get("default") );
		setItemText( 1, lang->get("nbFmNumber") );
		setItemText( 2, lang->get("nbFmScientific") );
		setItemText( 3, lang->get("nbFmPowerOf") );
		setItemText( 4, lang->get("nbFmMultiple") );
		setItemText( 5, lang->get("nbFmTime") );
		setItemText( 6, lang->get("nbFmDate") );
		setItemText( 7, lang->get("nbFmTimaeDate") );
	}
	void setFormat( const Axis::NumberFormat& nbFrmt )
	{
		int ind = 0;
		switch ( nbFrmt ) {
			case Axis::DEFAULTNF:
				ind = 0;
			break;
			case Axis::DECIMAL:
				ind = 1;
			break;
			case Axis::SCIENTIFIC:
				ind = 2;
			break;
			case Axis::POWEROF:
				ind = 3;
			break;
			case Axis::MULTIPLE:
				ind = 4;
			break;
			case Axis::TIME:
				ind = 5;
			break;
			case Axis::DATE:
				ind = 6;
			break;
			case Axis::TIMEDATE:
				ind = 7;
			break;
		}
		setCurrentIndex( ind );
	}

public slots:
	void setFormatInd( int index )
	{
		Axis::NumberFormat sett = Axis::DEFAULTNF;
		switch ( index )
		{
			case 1:
				sett = Axis::DECIMAL;
			break;
			case 2:
				sett = Axis::SCIENTIFIC;
			break;
			case 3:
				sett = Axis::POWEROF;
			break;
			case 4:
				sett = Axis::MULTIPLE;
			break;
			case 5:
				sett = Axis::TIME;
			break;
			case 6:
				sett = Axis::DATE;
			break;
			case 7:
				sett = Axis::TIMEDATE;
			break;
		}
		emit changed( sett );
	}

signals:
	void changed( const Axis::NumberFormat& currentNumberFormat );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // NUMBERFORMATSELECTION_H
