#ifndef TEXTCAPSELECTOR_H
#define TEXTCAPSELECTOR_H

#include <QComboBox>
#include "Commons/lang.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TextCapSelector
		: public QComboBox
{
	Q_OBJECT
public:
	~TextCapSelector()
	{

	}
	TextCapSelector( QWidget* parent = nullptr )
		: QComboBox( parent )
	{
		addItems( {"No Capitalization", "All Uppercase", "All Lowercase", "Small caps", "Capitalize"} );
		setMaximumWidth( 120 );
		setSizeAdjustPolicy( QComboBox::AdjustToContents );
		connect( this, SIGNAL(currentIndexChanged(int)),
				 this, SLOT(setCapInd(int)) );
	}
	void updateLang( Lang* lang ) {
		setItemText( 0, lang->get("noCapital") );
		setItemText( 1, lang->get("capUppercase") );
		setItemText( 2, lang->get("capLowercase") );
		setItemText( 3, lang->get("capSmallcase") );
		setItemText( 4, lang->get("capCapital") );
	}
	void setCap( QFont::Capitalization cap )
	{
		int ind = 0;
		if ( cap == QFont::AllUppercase )
			ind = 1;
		else if ( cap == QFont::AllLowercase)
			ind = 2;
		else if ( cap == QFont::SmallCaps )
			ind = 3;
		else if ( cap == QFont::Capitalize )
			ind = 4;
		setCurrentIndex( ind );
	}

public slots:
	void setCapInd( int index )
	{
		QFont::Capitalization sett = QFont::MixedCase;
		switch ( index )
		{
			case 1:
				sett = QFont::AllUppercase;
			break;
			case 2:
				sett = QFont::AllLowercase;
			break;
			case 3:
				sett = QFont::SmallCaps;
			break;
			case 4:
				sett = QFont::Capitalize;
			break;
		}
		emit changed( sett );
	}

signals:
	void changed( QFont::Capitalization cap );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TEXTCAPSELECTOR_H
