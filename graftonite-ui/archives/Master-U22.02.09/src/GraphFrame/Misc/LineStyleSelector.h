#ifndef LINESTYLESELECTOR_H
#define LINESTYLESELECTOR_H

#include <QComboBox>
#include "Commons/lang.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class LineStyleSelector
		: public QComboBox
{
	Q_OBJECT
public:
	~LineStyleSelector()
	{

	}
	LineStyleSelector( QWidget* parent = nullptr )
		: QComboBox( parent )
	{
		addItems( {"No Line", "Solid", "Dash", "Dot", "Dash-Dot", "Dash-Dot-Dot", "Custom"} );
		setMaximumWidth( 120 );
		setSizeAdjustPolicy( QComboBox::AdjustToContents );
		connect( this, SIGNAL(currentIndexChanged(int)),
				 this, SLOT(setStyleInd(int)) );
	}
	void updateLang( Lang* lang ) {
		setItemText( 0, lang->get("noLine") );
		setItemText( 1, lang->get("solidLine") );
		setItemText( 2, lang->get("dashLine") );
		setItemText( 3, lang->get("dotLine") );
		setItemText( 4, lang->get("dashDotLine") );
		setItemText( 5, lang->get("dashDotDotLine") );
		setItemText( 6, lang->get("customLine") );
	}
	void setStyle( Qt::PenStyle style )
	{
		int ind = 0;
		if ( style == Qt::SolidLine )
			ind = 1;
		else if ( style == Qt::DashLine )
			ind = 2;
		else if ( style == Qt::DotLine )
			ind = 3;
		else if ( style == Qt::DashDotLine )
			ind = 4;
		else if ( style == Qt::DashDotDotLine )
			ind = 5;
		else if ( style == Qt::CustomDashLine )
			ind = 6;

		setCurrentIndex( ind );
	}

public slots:
	void setStyleInd( int index )
	{
		Qt::PenStyle sett = Qt::NoPen;
		switch ( index )
		{
			case 1:
				sett = Qt::SolidLine;
			break;
			case 2:
				sett = Qt::DashLine;
			break;
			case 3:
				sett = Qt::DotLine;
			break;
			case 4:
				sett = Qt::DashDotLine;
			break;
			case 5:
				sett = Qt::DashDotDotLine;
			break;
			case 6:
				sett = Qt::CustomDashLine;
			break;
		}
		emit changed( sett );
	}

signals:
	void changed( Qt::PenStyle currentStyle );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LINESTYLESELECTOR_H
