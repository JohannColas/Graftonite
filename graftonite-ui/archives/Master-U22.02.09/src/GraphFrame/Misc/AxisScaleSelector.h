#ifndef AXISSCALESELECTOR_H
#define AXISSCALESELECTOR_H

#include <QComboBox>
#include "Commons/lang.h"

#include "../AxisSettings.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class AxisScaleSelector
		: public QComboBox
{
	Q_OBJECT
public:
	~AxisScaleSelector()
	{

	}
	AxisScaleSelector( QWidget* parent = nullptr )
		: QComboBox( parent )
	{
		addItems( { "Linear", "Neperian Logarithm", "Decadic Logarithm", "Log2", "Reciprocal" } );
		setMaximumWidth( 120 );
		setSizeAdjustPolicy( QComboBox::AdjustToContents );
		connect( this, SIGNAL(currentIndexChanged(int)),
				 this, SLOT(setScaleInd(int)) );
	}
	void updateLang( Lang* lang ) {
		setItemText( 0, lang->get("linearScale") );
		setItemText( 1, lang->get("logScale") );
		setItemText( 2, lang->get("log10Scale") );
		setItemText( 3, lang->get("log2Scale") );
		setItemText( 4, lang->get("reciprScale") );
	}
	void setScale( const Axis::Scale& scale )
	{
		int ind = 0;
		if ( scale == Axis::LN )
			ind = 1;
		else if ( scale == Axis::LOG10 )
			ind = 2;
		else if ( scale == Axis::LOG2 )
			ind = 3;
		else if ( scale == Axis::RECIPROCAL )
			ind = 4;

		setCurrentIndex( ind );
	}

public slots:
	void setScaleInd( int index )
	{
		Axis::Scale sett = Axis::LINEAR;
		switch ( index ) {
			case 1:
				sett = Axis::LN;
			break;
			case 2:
				sett = Axis::LOG10;
			break;
			case 3:
				sett = Axis::LOG2;
			break;
			case 4:
				sett = Axis::RECIPROCAL;
			break;
		}
		emit changed( sett );
	}

signals:
	void changed( const Axis::Scale& currentAxScale );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // AXISSCALESELECTOR_H
