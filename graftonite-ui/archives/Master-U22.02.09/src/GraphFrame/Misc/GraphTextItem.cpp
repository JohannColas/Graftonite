#include "GraphTextItem.h"

#include <QTextDocument>
/**********************************************/
/**********************************************/
/* */
GraphTextItem::~GraphTextItem()
{

}
/**********************************************/
/**********************************************/
/* */
GraphTextItem::GraphTextItem()
	: QGraphicsTextItem()
{

}
/**********************************************/
/**********************************************/
/* */
GraphTextItem::GraphTextItem( QGraphicsItem* parent )
	: QGraphicsTextItem( parent)
{

}
/**********************************************/
/**********************************************/
/* */
void GraphTextItem::setPosition( double x, double y, const Axis::Pos& anchor, double angle )
{
	setPos( x, y );
	rotate( angle );
	setAnchor( anchor );
}
/**********************************************/
/**********************************************/
/* */
void GraphTextItem::rotate( double angle )
{
	setRotation( - angle );
	update(x(), y());
}
/**********************************************/
/**********************************************/
/* */
void GraphTextItem::setAnchor( const Axis::Pos& anchor )
{
	_anchor = anchor;
	updateOriginTransformation();
}
/**********************************************/
/**********************************************/
/* */
void GraphTextItem::updateOriginTransformation()
{
	moveBy( transformOriginPoint().x(), transformOriginPoint().y() );
	double orgTransX = 0;
	if ( Axis::isLocated(_anchor, Axis::CENTER) )
	{
		orgTransX = 0.5*boundingRect().width();
	}
	else if ( Axis::isLocated(_anchor, Axis::RIGHT) )
	{
		orgTransX = boundingRect().width();
	}
	double orgTransY = 0;
	if ( Axis::isLocated(_anchor, Axis::MIDDLE) )
	{
		orgTransY = 0.5*boundingRect().height();
	}
	else if ( Axis::isLocated(_anchor, Axis::BOTTOM) )
	{
		orgTransY = boundingRect().height();
	}
	// Moving the origin to the Anchor position
	moveBy( -orgTransX, -orgTransY );
	setTransformOriginPoint( orgTransX, orgTransY );
}
/**********************************************/
/**********************************************/
/* */
void GraphTextItem::update( double x, double y )
{
	//document()->adjustSize();
	setPos( x, y );
	//updateOriginTransformation();
	fitSize();
	//setRotation( -angle );


//	GraphRect rec = effRect();
//	fdf->setRect( 0, 0, rec.w(), rec.h() );
//	fdf->setPos( rec.x(), rec.y() );
//	fdf->setPen( QPen( Qt::NoPen ) );
//	fdf->setBrush( QBrush( Qt::transparent) );
}
/**********************************************/
/**********************************************/
/* */
GraphRect GraphTextItem::effRect()
{
	return GraphRect::getFullRect( pos(), boundingRect(), transformOriginPoint(), -rotation() );

}
/**********************************************/
/**********************************************/
/* */
void GraphTextItem::fitSize()
{
	setTextWidth( 1000000 );
	setTextWidth( document()->idealWidth() );

	updateOriginTransformation();

//	GraphRect rec = effRect();
//	fdf->setRect( 0, 0, rec.w(), rec.h() );
//	fdf->setPos( rec.x(), rec.y() );
}
/**********************************************/
/**********************************************/
