#ifndef LEGENDPOSSELECTOR_H
#define LEGENDPOSSELECTOR_H

#include <QComboBox>
#include "Commons/lang.h"

#include "../Axis.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class LegendPosSelector
		: public QComboBox
{
	Q_OBJECT
private:
	Axis::Type type = Axis::XAxis;
	Lang* _lang = nullptr;

public:
	~LegendPosSelector()
	{

	}
	LegendPosSelector( QWidget* parent = nullptr )
		: QComboBox( parent )
	{
		addItems( {"No Legend", "Left", "Top", "Right", "Bottom"} );
		setMaximumWidth( 120 );
		setSizeAdjustPolicy( QComboBox::AdjustToContents );
		connect( this, SIGNAL(currentIndexChanged(int)),
				 this, SLOT(setPosInd(int)) );
	}
	void updateLang( Lang* lang ) {
		_lang = lang;
		setItemText( 0, lang->get("noAxis") );
		setItemText( 2, lang->get("centerAxis") );
		setItemText( 4, lang->get("speciAxis") );
	}
	void setPos( const Axis::Pos& pos )
	{
		int ind = 0;
		if ( pos == Axis::NONE )
			ind = 0;
		else if ( pos == Axis::BOTTOM )
			ind = 1;
		else if ( pos == Axis::LEFT )
			ind = 1;
		else if ( pos == Axis::CENTER )
			ind = 2;
		else if ( pos == Axis::TOP )
			ind = 3;
		else if ( pos == Axis::RIGHT )
			ind = 3;
		else if ( pos == Axis::SPECIFIED )
			ind = 4;

		setCurrentIndex( ind );
	}

public slots:
	void setPosInd( int index )
	{
		Axis::Pos sett = Axis::NONE;
		switch ( index )
		{
			case 1:
				sett = Axis::BOTTOM;
				if( type == Axis::YAxis )
					sett = Axis::LEFT;
			break;
			case 2:
				sett = Axis::CENTER;
			break;
			case 3:
				sett = Axis::TOP;
				if( type == Axis::YAxis )
					sett = Axis::RIGHT;
			break;
			case 4:
				sett = Axis::SPECIFIED;
			break;
		}
		emit changed( sett );
	}

signals:
	void changed( const Axis::Pos& currentAxPos );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LEGENDPOSSELECTOR_H
