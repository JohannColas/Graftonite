#include "DeleteDialog.h"
/**********************************************/
/**********************************************/
/* */
DeleteDialog::DeleteDialog(QWidget *parent) :
	QMessageBox(parent)
{
	setText( "Are you sure to delete these elements ?" );
	setStandardButtons(QMessageBox::Yes | QMessageBox::Cancel);
}
/**********************************************/
/**********************************************/
/* */
DeleteDialog::~DeleteDialog()
{
}
/**********************************************/
/**********************************************/
/* */
void DeleteDialog::setIDs(QStringList layers, QStringList axes, QStringList curves, QStringList shapes)
{
	QString content = "";
	if( layers.size() > 0 )
	{
		content += "Layers : \n";
		for( QString elm : layers )
		{
			content += "   " + elm + "\n";
		}
		content += "\n";
	}
	if( axes.size() > 0 )
	{
		content += "Axes : \n";
		for( QString elm : axes )
		{
			content += "   " + elm + "\n";
		}
		content += "\n";
	}
	if( curves.size() > 0 )
	{
		content += "Curves : \n";
		for( QString elm : curves )
		{
			content += "   " + elm + "\n";
		}
		content += "\n";
	}
	if( shapes.size() > 0 )
	{
		content += "Shapes : \n";
		for( QString elm : shapes )
		{
			content += "   " + elm + "\n";
		}
		content += "\n";
	}
	setInformativeText( content );
}
/**********************************************/
/**********************************************/
