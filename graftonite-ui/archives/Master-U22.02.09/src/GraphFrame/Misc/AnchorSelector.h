#ifndef ANCHORSELECTOR_H
#define ANCHORSELECTOR_H

#include <QComboBox>
#include "Commons/lang.h"

#include "../AxisSettings.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class AnchorSelector : public QComboBox
{
	Q_OBJECT
public:
	~AnchorSelector()
	{

	}
	AnchorSelector( QWidget* parent = nullptr )
		: QComboBox( parent )
	{
		addItems( {"Top-Left", "Top-Center", "Top-Right", "Middle-Left", "Middle-Center", "Middle-Right", "Bottom-Left", "Bottom-Center", "Bottom-Right"} );
		setMaximumWidth( 120 );
		setSizeAdjustPolicy( QComboBox::AdjustToContents );
		connect( this, SIGNAL(currentIndexChanged(int)),
				 this, SLOT(setAnchorInd(int)) );
	}
	void updateLang( Lang* lang ) {
		setItemText( 0, lang->get("anchTopLef") );
		setItemText( 1, lang->get("anchTopCen") );
		setItemText( 2, lang->get("anchTopRig") );
		setItemText( 3, lang->get("anchMidLef") );
		setItemText( 4, lang->get("anchMidCen") );
		setItemText( 5, lang->get("anchMidRig") );
		setItemText( 6, lang->get("anchBotLef") );
		setItemText( 7, lang->get("anchBotCen") );
		setItemText( 8, lang->get("anchBotRig") );
	}
	void setAnchor( const Axis::Pos& anch )
	{
		int ind = 0;
		if ( anch == Axis::TOPCENTER )
			ind = 1;
		else if ( anch == Axis::TOPRIGHT )
			ind = 2;
		else if ( anch == Axis::MIDDLELEFT )
			ind = 3;
		else if ( anch == Axis::MIDDLECENTER )
			ind = 4;
		else if ( anch == Axis::MIDDLERIGHT )
			ind = 5;
		else if ( anch == Axis::BOTTOMLEFT )
			ind = 6;
		else if ( anch == Axis::BOTTOMCENTER )
			ind = 7;
		else if ( anch == Axis::BOTTOMRIGHT)
			ind = 8;

		setCurrentIndex( ind );
	}

public slots:
	void setAnchorInd( int index )
	{
		Axis::Pos sett = Axis::TOPLEFT;
		switch ( index )
		{
			case 1:
				sett = Axis::TOPCENTER;
			break;
			case 2:
				sett = Axis::TOPRIGHT;
			break;
			case 3:
				sett = Axis::MIDDLELEFT;
			break;
			case 4:
				sett = Axis::MIDDLECENTER;
			break;
			case 5:
				sett = Axis::MIDDLERIGHT;
			break;
			case 6:
				sett = Axis::BOTTOMLEFT;
			break;
			case 7:
				sett = Axis::BOTTOMCENTER;
			break;
			case 8:
				sett = Axis::BOTTOMRIGHT;
			break;
		}
		emit changed( sett );
	}

signals:
	void changed( const Axis::Pos& anch );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ANCHORSELECTOR_H
