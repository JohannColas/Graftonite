#ifndef LINECAPSELECTOR_H
#define LINECAPSELECTOR_H

#include <QComboBox>
#include "Commons/lang.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class LineCapSelector
		: public QComboBox
{
	Q_OBJECT
public:
	~LineCapSelector()
	{

	}
	LineCapSelector( QWidget* parent = nullptr )
		: QComboBox( parent )
	{
		addItems( {"Square", "Round", "Flat"} );
		setMaximumWidth( 120 );
		setSizeAdjustPolicy( QComboBox::AdjustToContents );
		connect( this, SIGNAL(currentIndexChanged(int)),
				 this, SLOT(setStyleInd(int)) );
	}
	void updateLang( Lang* lang ) {
		setItemText( 0, lang->get("capSquare") );
		setItemText( 1, lang->get("capRound") );
		setItemText( 2, lang->get("capFlat") );
	}
	void setStyle( Qt::PenCapStyle style )
	{
		int ind = 0;
		if ( style == Qt::RoundCap )
			ind = 1;
		else if ( style == Qt::FlatCap )
			ind = 2;
		setCurrentIndex( ind );
	}

public slots:
	void setStyleInd( int index )
	{
		Qt::PenCapStyle sett = Qt::SquareCap;
		switch ( index )
		{
			case 1:
				sett = Qt::RoundCap;
			break;
			case 2:
				sett = Qt::FlatCap;
			break;
		}
		emit changed( sett );
	}

signals:
	void changed( Qt::PenCapStyle currentStyle );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LINECAPSELECTOR_H
