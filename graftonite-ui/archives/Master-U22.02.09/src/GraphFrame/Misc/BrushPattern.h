#ifndef BRUSHPATTERN_H
#define BRUSHPATTERN_H

#include <QComboBox>

/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class BrushPatternSelector
		: public QComboBox
{
	Q_OBJECT
public:
	~BrushPatternSelector()
	{

	}
	BrushPatternSelector( QWidget* parent = nullptr )
		: QComboBox( parent )
	{
		addItems( {"No Pattern",
				   "Solid",
				   "Dense 1",
				   "Dense 2",
				   "Dense 3",
				   "Dense 4",
				   "Dense 5",
				   "Dense 6",
				   "Dense 7",
				   "Hor. Pattern",
				   "Ver. Pattern",
				   "Cross",
				   "B. Diag. Pattern",
				   "F. Diag. Pattern",
				   "Diag. Cross",
				   "Linear Grad.",
				   "Radial Grad.",
				   "Conical Grad."} );
		setMaximumWidth( 120 );
		setSizeAdjustPolicy( QComboBox::AdjustToContents );
		connect( this, SIGNAL(currentIndexChanged(int)),
				 this, SLOT(setPatternInd(int)) );
	}
	void setPattern( Qt::BrushStyle style )
	{
		int ind = 0;
		if ( style == Qt::SolidPattern )
			ind = 1;
		else if ( style == Qt::Dense1Pattern )
			ind = 2;
		else if ( style == Qt::Dense2Pattern )
			ind = 3;
		else if ( style == Qt::Dense3Pattern )
			ind = 4;
		else if ( style == Qt::Dense4Pattern )
			ind = 5;
		else if ( style == Qt::Dense5Pattern )
			ind = 6;
		else if ( style == Qt::Dense6Pattern )
			ind = 7;
		else if ( style == Qt::Dense7Pattern )
			ind = 8;
		else if ( style == Qt::HorPattern )
			ind = 9;
		else if ( style == Qt::VerPattern )
			ind = 10;
		else if ( style == Qt::CrossPattern )
			ind = 11;
		else if ( style == Qt::BDiagPattern )
			ind = 12;
		else if ( style == Qt::FDiagPattern )
			ind = 13;
		else if ( style == Qt::DiagCrossPattern )
			ind = 14;
		else if ( style == Qt::LinearGradientPattern )
			ind = 15;
		else if ( style == Qt::RadialGradientPattern )
			ind = 16;
		else if ( style == Qt::ConicalGradientPattern )
			ind = 17;

		setCurrentIndex( ind );
	}

public slots:
	void setPatternInd( int index )
	{
		Qt::BrushStyle sett = Qt::NoBrush;
		switch ( index )
		{
			case 1:
				sett = Qt::SolidPattern;
			break;
			case 2:
				sett = Qt::Dense1Pattern;
			break;
			case 3:
				sett = Qt::Dense2Pattern;
			break;
			case 4:
				sett = Qt::Dense3Pattern;
			break;
			case 5:
				sett = Qt::Dense4Pattern;
			break;
			case 6:
				sett = Qt::Dense5Pattern;
			break;
			case 7:
				sett = Qt::Dense6Pattern;
			break;
			case 8:
				sett = Qt::Dense7Pattern;
			break;
			case 9:
				sett = Qt::HorPattern;
			break;
			case 10:
				sett = Qt::VerPattern;
			break;
			case 11:
				sett = Qt::CrossPattern;
			break;
			case 12:
				sett = Qt::BDiagPattern;
			break;
			case 13:
				sett = Qt::FDiagPattern;
			break;
			case 14:
				sett = Qt::DiagCrossPattern;
			break;
			case 15:
				sett = Qt::LinearGradientPattern;
			break;
			case 16:
				sett = Qt::RadialGradientPattern;
			break;
			case 17:
				sett = Qt::ConicalGradientPattern;
			break;
		}
		emit changed( sett );
	}

signals:
	void changed( Qt::BrushStyle currentStyle );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // BRUSHPATTERN_H
