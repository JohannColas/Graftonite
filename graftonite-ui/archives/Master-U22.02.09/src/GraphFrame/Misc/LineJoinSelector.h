#ifndef LINEJOINSELECTOR_H
#define LINEJOINSELECTOR_H

#include <QComboBox>
#include "Commons/lang.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class LineJoinSelector
		: public QComboBox
{
	Q_OBJECT
public:
	~LineJoinSelector()
	{

	}
	LineJoinSelector( QWidget* parent = nullptr )
		: QComboBox( parent )
	{
		addItems( {"Miter", "Round", "Bevel"} );
		setMaximumWidth( 120 );
		setSizeAdjustPolicy( QComboBox::AdjustToContents );
		connect( this, SIGNAL(currentIndexChanged(int)),
				 this, SLOT(setStyleInd(int)) );
	}
	void updateLang( Lang* lang ) {
		setItemText( 0, lang->get("joinMiter") );
		setItemText( 1, lang->get("joinRound") );
		setItemText( 2, lang->get("joinBevel") );
	}
	void setStyle( Qt::PenJoinStyle style )
	{
		int ind = 0;
		if ( style == Qt::RoundJoin )
			ind = 1;
		else if ( style == Qt::BevelJoin )
			ind = 2;

		setCurrentIndex( ind );
	}

public slots:
	void setStyleInd( int index )
	{
		Qt::PenJoinStyle sett = Qt::MiterJoin;
		switch ( index )
		{
			case 1:
				sett = Qt::RoundJoin;
			break;
			case 2:
				sett = Qt::BevelJoin;
			break;
		}
		emit changed( sett );
	}

signals:
	void changed( Qt::PenJoinStyle currentStyle );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LINEJOINSELECTOR_H
