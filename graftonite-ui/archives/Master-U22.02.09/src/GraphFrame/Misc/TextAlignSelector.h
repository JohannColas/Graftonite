#ifndef TEXTALIGNSELECTOR_H
#define TEXTALIGNSELECTOR_H

#include <QComboBox>
#include "Commons/theme.h"
#include "Commons/lang.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class TextAlignSelector
		: public QComboBox
{
	Q_OBJECT
public:
	~TextAlignSelector()
	{

	}
	TextAlignSelector( QWidget* parent = nullptr )
		: QComboBox( parent )
	{
		addItems( {"Left", "Center", "Right", "Justify"} );
		setMaximumWidth( 120 );
		setSizeAdjustPolicy( QComboBox::AdjustToContents );
		connect( this, SIGNAL(currentIndexChanged(int)),
				 this, SLOT(setAlignInd(int)) );
	}
	void updateLang( Lang* lang ) {
		setItemText( 0, lang->get("left") );
		setItemText( 1, lang->get("center") );
		setItemText( 2, lang->get("right") );
		setItemText( 3, lang->get("justify") );
	}
	void setAlign( Qt::Alignment align )
	{
		int ind = 0;
		switch ( align ) {
			case Qt::AlignHCenter:
				ind = 1;
			break;
			case Qt::AlignRight:
				ind = 2;
			break;
			case Qt::AlignJustify:
				ind = 3;
			break;
		}
		setCurrentIndex( ind );
	}

public slots:
	void setAlignInd( int index )
	{
		Qt::Alignment sett = Qt::AlignLeft | Qt::AlignAbsolute;
		switch ( index )
		{
			case 1:
				sett = Qt::AlignHCenter;
			break;
			case 2:
				sett = Qt::AlignRight | Qt::AlignAbsolute;
			break;
			case 3:
				sett = Qt::AlignJustify;
			break;
		}
		emit changed( sett );
	}
	void updateIcons( Icons* icons ) {
		setItemIcon( 0, icons->alignLeft() );
		setItemIcon( 1, icons->alignCenter() );
		setItemIcon( 2, icons->alignRight() );
		setItemIcon( 3, icons->alignJustify() );
	}

signals:
	void changed( Qt::Alignment align );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TEXTALIGNSELECTOR_H
