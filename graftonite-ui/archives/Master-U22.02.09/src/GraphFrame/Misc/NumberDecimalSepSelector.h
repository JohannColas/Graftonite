#ifndef NUMBERDECIMALSEPSELECTOR_H
#define NUMBERDECIMALSEPSELECTOR_H

#include <QComboBox>
#include "Commons/lang.h"

/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class NumberDecimalSepSelector : public QComboBox
{
	Q_OBJECT
public:
	~NumberDecimalSepSelector()
	{

	}
	NumberDecimalSepSelector( QWidget* parent = nullptr )
		: QComboBox( parent )
	{
		addItems( {"Default", ".", ","} );
		setMaximumWidth( 120 );
		setSizeAdjustPolicy( QComboBox::AdjustToContents );
		connect( this, SIGNAL(currentIndexChanged(int)),
				 this, SLOT(setDecimalSeparatorInd(int)) );
	}
	void updateLang( Lang* lang ) {
		setItemText( 0, lang->get("default") );
	}
	void setDecimalSeparator( const char& currentDecimalSeparator )
	{
		int index = 0;
		if ( currentDecimalSeparator == '.' )
			index = 1;
		else if ( currentDecimalSeparator == ',' )
			index = 2;
		setCurrentIndex( index );
	}

public slots:
	void setDecimalSeparatorInd( int index )
	{
		char currentChar = 0;
		if ( index == 1 )
			currentChar = '.';
		else if ( index == 2 )
			currentChar = ',';
		emit changed( currentChar );
	}

signals:
	void changed( char currentDecimalSeparator );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // NUMBERDECIMALSEPSELECTOR_H
