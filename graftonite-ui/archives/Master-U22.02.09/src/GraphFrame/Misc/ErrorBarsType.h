#ifndef ERRORBARSTYPESELECTOR_H
#define ERRORBARSTYPESELECTOR_H

#include <QComboBox>

#include "../Curve.h"

/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class ErrorBarsTypeSelector
		: public QComboBox
{
	Q_OBJECT

public:
	ErrorBarsTypeSelector( QWidget* parent = nullptr )
		: QComboBox( parent )
	{

		setMaximumWidth( 120 );
		setSizeAdjustPolicy( QComboBox::AdjustToContents );
	}

signals:
	void changed();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ERRORBARSTYPESELECTOR_H
