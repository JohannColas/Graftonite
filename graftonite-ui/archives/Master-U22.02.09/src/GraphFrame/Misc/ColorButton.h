#ifndef COLORBUTTON_H
#define COLORBUTTON_H

#include <QPushButton>
#include <QColor>
#include <QColorDialog>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class ColorButton
		: public QPushButton
{
	Q_OBJECT
private:
	QColor color;
	bool colored = true;

public:
	~ColorButton()
	{

	}
	ColorButton( QWidget* parent = nullptr )
		: QPushButton( parent )
	{
		connect( this, &ColorButton::clicked, this, &ColorButton::changeColor );

	}
	QColor getColor() const
	{
		return color;
	}
	void uncolored() {
		colored = false;
	}
	void setColor( const QColor& value )
	{
		color = value;
		if ( colored ) {
			QPixmap pix( 40, 16 );
			pix.fill( color );
			QIcon icon( pix );
			setIcon( icon );
			setIconSize( {40, 16} );
		}
	}
	void changeColor()
	{
		//		if ( color.isValid() )
		{
			QColor newColor = QColorDialog::getColor(
								  color,
								  nullptr,
								  "Choose a new Color",
								  QColorDialog::ShowAlphaChannel
								  );
			if( newColor.isValid() )
			{
				setColor( newColor );
				emit changed( newColor );
			}
		}
	}

signals:
	void changed( const QColor& color );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // COLORBUTTON_H




