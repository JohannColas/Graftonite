#ifndef AXISTYPESELECTOR_H
#define AXISTYPESELECTOR_H

#include <QComboBox>
#include "Commons/lang.h"

#include "../AxisSettings.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class AxisTypeSelector
		: public QComboBox
{
	Q_OBJECT
public:
	~AxisTypeSelector()
	{

	}
	AxisTypeSelector( QWidget* parent = nullptr )
		: QComboBox( parent )
	{
		addItems( { "X Axis", "Y Axis", "Radial Axis", "Angular Axis" } );
		setMaximumWidth( 120 );
		setSizeAdjustPolicy( QComboBox::AdjustToContents );
		connect( this, SIGNAL(currentIndexChanged(int)),
				 this, SLOT(setTypeInd(int)) );
	}
	void updateLang( Lang* lang ) {
		setItemText( 0, lang->get("xAxis") );
		setItemText( 1, lang->get("yAxis") );
		setItemText( 2, lang->get("radAxis") );
		setItemText( 3, lang->get("angAxis") );
	}
	void setType( const Axis::Type& type )
	{
		int ind = 0;
		if ( type == Axis::YAxis )
			ind = 1;
		else if ( type == Axis::RadAxis )
			ind = 2;
		else if ( type == Axis::AngAxis )
			ind = 3;
		else if ( type == Axis::RdrAxis )
			ind = 4;

		setCurrentIndex( ind );
	}

public slots:
	void setTypeInd( int index )
	{
		Axis::Type sett = Axis::XAxis;
		switch ( index ) {
			case 1:
				sett = Axis::YAxis;
			break;
			case 2:
				sett = Axis::RadAxis;
			break;
			case 3:
				sett = Axis::AngAxis;
			break;
			case 4:
				sett = Axis::RdrAxis;
			break;
		}
		emit changed( sett );
	}

signals:
	void changed( const Axis::Type& currentAxType );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // AXISTYPESELECTOR_H
