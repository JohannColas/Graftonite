#ifndef GRAPHTEXTITEM_H
#define GRAPHTEXTITEM_H

#include <QGraphicsTextItem>

#include "../AxisSettings.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class GraphTextItem : public QGraphicsTextItem
{
	Q_OBJECT
protected:
	Axis::Pos _anchor = Axis::TOPLEFT;

public:
	~GraphTextItem();
	GraphTextItem();
	GraphTextItem( QGraphicsItem *parent );

	void setPosition ( double x, double y, const Axis::Pos& anchor, double angle );
	void rotate(double angle);
	void setAnchor( const Axis::Pos& anchor);
	void updateOriginTransformation();
	void update( double x, double y );
	GraphRect effRect();

public slots:
	void fitSize();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // GRAPHTEXTITEM_H
