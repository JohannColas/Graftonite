#ifndef DELETEDIALOG_H
#define DELETEDIALOG_H

#include <QMessageBox>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class DeleteDialog : public QMessageBox
{
	Q_OBJECT
private:
	QStringList layersID = {};
	QStringList axesID = {};
	QStringList curvesID = {};
	QStringList shapesID = {};

public:
	explicit DeleteDialog(QWidget *parent = nullptr);
	~DeleteDialog();
	void setIDs( QStringList layers,QStringList axes = {},QStringList curves = {},QStringList shapes = {} );

};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // DELETEDIALOG_H
