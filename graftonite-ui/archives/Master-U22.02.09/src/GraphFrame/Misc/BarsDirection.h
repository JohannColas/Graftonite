#ifndef BARSDIRECTION_H
#define BARSDIRECTION_H

#include <QComboBox>

#include "../Curve.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class BarsDirectionSelector
		: public QComboBox
{
	Q_OBJECT

public:
	BarsDirectionSelector( QWidget* parent = nullptr )
		: QComboBox( parent )
	{
		addItems( {"No Bars", "To X Axis", "To Y Axis", "To Bottom", "To Top", "To Left", "To Right"} );
		setMaximumWidth( 120 );
		setSizeAdjustPolicy( QComboBox::AdjustToContents );
		connect( this, SIGNAL(currentIndexChanged(int)),
				 this, SLOT(setDirectionInd(int)) );
	}
	void setDirection( const Curve::Direction& style )
	{
		int ind = 0;
		if ( style == Curve::TOXAXIS )
			ind = 1;
		else if ( style == Curve::TOYAXIS )
			ind = 2;
		else if ( style == Curve::TOBOTTOM )
			ind = 3;
		else if ( style == Curve::TOTOP )
			ind = 4;
		else if ( style == Curve::TOLEFT )
			ind = 5;
		else if ( style == Curve::TORIGHT )
			ind = 6;
		setCurrentIndex( ind );
	}

public slots:
	void setDirectionInd( int index )
	{
		Curve::Direction sett = Curve::NOBAR;
		switch ( index )
		{
			case 1:
				sett = Curve::TOXAXIS;
			break;
			case 2:
				sett = Curve::TOYAXIS;
			break;
			case 3:
				sett = Curve::TOBOTTOM;
			break;
			case 4:
				sett = Curve::TOTOP;
			break;
			case 5:
				sett = Curve::TOLEFT;
			break;
			case 6:
				sett = Curve::TORIGHT;
			break;
		}
		emit changed( sett );
	}

signals:
	void changed( const Curve::Direction& direc );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // BARSDIRECTION_H
