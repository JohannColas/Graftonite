#ifndef GRAPHMISC_H
#define GRAPHMISC_H

//#include <QPoint>
#include <QString>
#include <QList>
#include <QRect>
#include <QLine>
#include <cmath>

typedef double GraphReal;
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class GraphPoint
{
private:
	GraphReal _x = 0;
	GraphReal _y = 0;
public:
	// Destructor
	~GraphPoint() {};
	// Constructors
	GraphPoint() {};
	GraphPoint( GraphReal x, GraphReal y )
	{
		setPoint(  x, y );
	};
	//    GFT_Point( QPointF p)
	//    {
	//        setPoint(  p.x(), p.y() );
	//    };
	// Accessors & Modifiers
	void setPoint( GraphReal x, GraphReal y )
	{
		setX( x );
		setY( y );
	};
	void setX( GraphReal x )
	{
		_x = x;
	};
	GraphReal x() const
	{
		return _x;
	};
	void setY( GraphReal y )
	{
		_y = y;
	};
	GraphReal y() const
	{
		return _y;
	};
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class GraphSize
{
private:
	GraphReal _w = 0;
	GraphReal _h = 0;
public:
	// Destructor
	~GraphSize() { };
	// Constructors
	GraphSize() { };
	GraphSize( GraphReal w, GraphReal h )
	{
		setSize( w, h );
	};
	// Accessors & Modifiers
	void setSize( GraphReal w, GraphReal h )
	{
		setW( w );
		setH( h );
	};
	void setW( GraphReal w )
	{
		_w = w;
	};
	double w()
	{
		return _w;
	};
	void setH( GraphReal h )
	{
		_h = h;
	};
	double h()
	{
		return _h;
	};
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class GraphRect : public GraphPoint, public GraphSize
{
public:
	// Destructor
	~GraphRect() { };
	// Constructors
	GraphRect() { };
	GraphRect( GraphReal x, GraphReal y, GraphReal w, GraphReal h )
	{
		setRect( x, y, w, h );
	};
	//    GFT_Rect( const QRectF &rect )
	//    {
	//        setRect( rect );
	//    };
	// Accessors & Modifiers
	void setRect( GraphReal x, GraphReal y, GraphReal w, GraphReal h )
	{
		setPoint( x, y );
		setSize( w, h );
	};
	//    void setRect( const QRectF &rect )
	//    {
	//        setRect( rect.x(), rect.y(), rect.width(), rect.height() );
	//    };
	static GraphRect getFullRect( const QPointF &pos, const QRectF &rec, const QPointF &trans, const GraphReal &angle )
	{
		GraphReal x = pos.x();
		GraphReal y =  pos.y();
		GraphReal width = rec.width();
		GraphReal height =  rec.height();
		GraphReal orgX = trans.x();
		GraphReal orgY =  trans.y();
		GraphReal theta = 0.001 * ( (int)(angle*1000.0) % 360000 );
		if ( theta < 0 ) theta += 360;
		GraphReal pi = 3.141592653589793;
		GraphReal cost = abs( cos( (pi/180) * theta ) );
		GraphReal sint = abs( sin( (pi/180) * theta ) );

		GraphReal nw = width * cost + height * sint;
		GraphReal nh = width * sint + height * cost;

		GraphReal dx, dy;
		if ( theta <= 90 )
		{
			dx = orgX * cost + orgY * sint;
			dy = ( width - orgX ) * sint + orgY * cost;
		}
		else if ( theta <= 180 )
		{
			dx = ( width - orgX ) * cost + orgY * sint;
			dy = ( width - orgX ) * sint + ( height - orgY ) * cost;
		}
		else if ( theta <= 270 )
		{
			dx = ( width - orgX ) * cost + ( height - orgY ) * sint;
			dy = orgX * sint + ( height - orgY ) * cost;
		}
		else
		{
			dx = orgX * cost + ( height - orgY ) * sint;
			dy = orgX * sint + orgY * cost;
		}
		x += orgX - dx;
		y += orgY - dy;

		return GraphRect( x, y, nw, nh );
	};
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
struct GraphMath
{
	static constexpr double PI = 3.141592653589793;
	static constexpr double e = 2.71828182845904523536;
	static inline int biggestNBDecimal(QList<double> list)
	{
		int local = 0, iTMP = 0; QString nbTMP;
		for (int i = 0; i < list.size(); i++)
		{
			nbTMP = QString::number(list.at(i));
			iTMP = nbTMP.length() - nbTMP.lastIndexOf('.') - 1;
			if (iTMP > local && nbTMP.contains('.')) local = iTMP;
		}
		return local;
	};
	static inline double max(QList<double> list)
	{
		double local = list.first();
		for (int i = 1; i < list.size(); i++)
		{
			if ( list.at(i) > local ) local = list.at(i);
		}
		return local;
	};
	static inline double min(QList<double> list)
	{
		double local = list.first();
		for (int i = 1; i < list.size(); i++)
		{
			if ( list.at(i) < local ) local = list.at(i);
		}
		return local;
	};
	static inline long long getFirstTick(long long start, long long end, long long step)
	{
		long long first = 0;
		long long mod = start % step;
		if ( mod == 0 )  first = start;
		else
		{
			first = start - mod;
			if ( start < end && start > 0 ) first += step;
			else if ( start > end && start < 0 ) first -= step;
		}
		return first;
	};
	static inline bool stopCondition(long long tick, long long end, char direc)
	{
		if (direc == 1) { return tick <= end;}
		else if (direc == -1) { return tick >= end;}
		return false;
	};
	static inline long long getPowerMultiplier(QList<double> list)
	{
		int tmp = 0, iTMP = 0; QString nbTMP;
		for (int i = 0; i < list.size(); i++)
		{
			nbTMP = QString::number(list.at(i));
			iTMP = nbTMP.length() - nbTMP.lastIndexOf('.') - 1;
			if (iTMP > tmp && nbTMP.contains('.')) tmp = iTMP;
		}
		long long local = 1;
		for (int ml = 1; ml <= tmp; ml++) local *= 10;
		return local;
	};
	static inline GraphRect getContaininGraphRect(QLineF line, QRectF ticks)
	{
		double x1 = line.x1();
		if ( x1 >  ticks.x() ) x1 = ticks.x();
		double x2 = line.x2();
		if ( x2 <  ticks.x() + ticks.width() ) x2 = ticks.x() + ticks.width();
		double y1 = line.y1();
		if ( y1 >  ticks.y() ) y1 = ticks.y();
		double y2 = line.y2();
		if ( y2 < ticks.y() + ticks.height() ) y2 = ticks.y() + ticks.height();
		return GraphRect( x1, y1, x2-x1, y2-y1 );
	};
	static inline GraphRect getContaininGraphRect( QRectF rec1, QRectF rec2 )
	{
		double x1 = rec1.x();
		if ( x1 >  rec2.x() ) x1 = rec2.x();
		double x2 = rec1.x() + rec1.width();
		if ( x2 <  rec2.x() + rec2.width() ) x2 = rec2.x() + rec2.width();
		double y1 = rec1.y();
		if ( y1 >  rec2.y() ) y1 = rec2.y();
		double y2 = rec1.y() + rec1.height();
		if ( y2 < rec2.y() + rec2.height() ) y2 = rec2.y() + rec2.height();
		return GraphRect( x1, y1, x2-x1, y2-y1 );
	};
	static inline GraphRect getContaininGraphRect( GraphRect rec1, QRectF rec2 )
	{
		double x1 = rec1.x();
		if ( x1 >  rec2.x() ) x1 = rec2.x();
		double x2 = rec1.x() + rec1.w();
		if ( x2 <  rec2.x() + rec2.width() ) x2 = rec2.x() + rec2.width();
		double y1 = rec1.y();
		if ( y1 >  rec2.y() ) y1 = rec2.y();
		double y2 = rec1.y() + rec1.h();
		if ( y2 < rec2.y() + rec2.height() ) y2 = rec2.y() + rec2.height();
		return GraphRect( x1, y1, x2-x1, y2-y1 );
	};
	static inline QRectF getContainingGraphRect( QRectF rec1, QRectF rec2 )
	{
		double x1 = rec1.x();
		if ( x1 >  rec2.x() ) x1 = rec2.x();
		double x2 = rec1.x() + rec1.width();
		if ( x2 <  rec2.x() + rec2.width() ) x2 = rec2.x() + rec2.width();
		double y1 = rec1.y();
		if ( y1 >  rec2.y() ) y1 = rec2.y();
		double y2 = rec1.y() + rec1.height();
		if ( y2 < rec2.y() + rec2.height() ) y2 = rec2.y() + rec2.height();
		return QRectF( x1, y1, x2-x1, y2-y1 );
	};
};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class GraphMisc
{
public:
	GraphMisc();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // GRAPHMISC_H
