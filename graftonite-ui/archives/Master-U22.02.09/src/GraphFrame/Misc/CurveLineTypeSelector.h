#ifndef CURVELINETYPESELECTOR_H
#define CURVELINETYPESELECTOR_H

#include <QComboBox>

#include "../Curve.h"

/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class CurveLineTypeSelector
		: public QComboBox
{
	Q_OBJECT
public:
	~CurveLineTypeSelector()
	{

	}
	CurveLineTypeSelector( QWidget* parent = nullptr )
		: QComboBox( parent )
	{
		addItems( { "No Line", "Straight", "Hor. Start", "Ver. Start", "Hor. Midpoint", "Ver. Midpoint", "Cubic Spline", "Akima Spline" } );
		setMaximumWidth( 120 );
		setSizeAdjustPolicy( QComboBox::AdjustToContents );
		connect( this, SIGNAL(currentIndexChanged(int)),
				 this, SLOT(setTypeInd(int)) );
	}
	void setType( const Curve::Line& type )
	{
		int ind = 0;
		if ( type == Curve::STRAIGHT )
			ind = 1;
		else if ( type == Curve::HORSTART )
			ind = 2;
		else if ( type == Curve::VERSTART )
			ind = 3;
		else if ( type == Curve::HORMIDPOINT )
			ind = 4;
		else if ( type == Curve::VERMIDPOINT )
			ind = 5;
		else if ( type == Curve::CUBICSPLINE )
			ind = 6;
		else if ( type == Curve::AKIMASPLINE )
			ind = 7;

		setCurrentIndex( ind );
	}

public slots:
	void setTypeInd( int index )
	{
		Curve::Line sett = Curve::NOLINE;
		switch ( index ) {
			case 1:
				sett = Curve::STRAIGHT;
			break;
			case 2:
				sett = Curve::HORSTART;
			break;
			case 3:
				sett = Curve::VERSTART;
			break;
			case 4:
				sett = Curve::HORMIDPOINT;
			break;
			case 5:
				sett = Curve::VERMIDPOINT;
			break;
			case 6:
				sett = Curve::CUBICSPLINE;
			break;
			case 7:
				sett = Curve::AKIMASPLINE;
			break;
		}
		emit changed( sett );
	}

signals:
	void changed( const Curve::Line& type );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // CURVELINETYPESELECTOR_H
