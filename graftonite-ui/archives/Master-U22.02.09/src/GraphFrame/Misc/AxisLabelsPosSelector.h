#ifndef AXISLABELSPOSSELECTOR_H
#define AXISLABELSPOSSELECTOR_H

#include <QComboBox>

#include "Commons/lang.h"
#include "../AxisSettings.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class AxisLabelsPosSelector
		: public QComboBox
{
	Q_OBJECT
private:
	Axis::Type type = Axis::XAxis;
	Lang* _lang = nullptr;

public:
	~AxisLabelsPosSelector()
	{

	}
	AxisLabelsPosSelector( QWidget* parent = nullptr )
		: QComboBox( parent )
	{
		addItems( {"No Labels", "Bottom", "Top"} );
		setMaximumWidth( 120 );
		setSizeAdjustPolicy( QComboBox::AdjustToContents );
		connect( this, SIGNAL(currentIndexChanged(int)),
				 this, SLOT(setPosInd(int)) );
	}
	void updateLang( Lang* lang ) {
		_lang = lang;
		setItemText( 0, lang->get("noLabels") );
	}
	void updateItems( const Axis::Type& axType )
	{
		type = axType;
		switch( axType )
		{
			case Axis::XAxis:
				setItemText( 1, _lang->get("bottomAxis") );
				setItemText( 2, _lang->get("topAxis") );
			break;
			case Axis::YAxis:
				setItemText( 1, _lang->get("leftAxis") );
				setItemText( 2, _lang->get("rightAxis") );
			break;
			case Axis::RadAxis:
				setItemText( 1, "" );
				setItemText( 2, "" );
			break;
			case Axis::AngAxis:
				setItemText( 1, "" );
				setItemText( 2, "" );
			break;
			case Axis::RdrAxis:
				setItemText( 1, "" );
				setItemText( 2, "" );
			break;
		}
	}
	void setCurrentPos( const Axis::Pos& pos )
	{
		int ind = 0;
		if ( pos == Axis::BOTTOM )
			ind = 1;
		else if ( pos == Axis::LEFT )
			ind = 1;
		else if ( pos == Axis::TOP )
			ind = 2;
		else if ( pos == Axis::RIGHT )
			ind = 2;

		setCurrentIndex( ind );
	}

public slots:
	void setPosInd( int index )
	{
		Axis::Pos sett = Axis::NONE;
		switch ( index )
		{
			case 1:
				sett = Axis::BOTTOM;
				if( type == Axis::YAxis )
					sett = Axis::LEFT;
			break;
			case 2:
				sett = Axis::TOP;
				if( type == Axis::YAxis )
					sett = Axis::RIGHT;
			break;
		}
		emit changed( sett );
	}

signals:
	void changed( const Axis::Pos& currentAxPos );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // AXISLABELSPOSSELECTOR_H
