#ifndef NUMBERFORMATOPTIONSELECTOR_H
#define NUMBERFORMATOPTIONSELECTOR_H

#include <QComboBox>

#include "../AxisSettings.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class NumberFormatOptionSelector
		: public QComboBox
{
	Q_OBJECT
	Axis::NumberFormat format = Axis::DEFAULTNF;
public:
	~NumberFormatOptionSelector()
	{

	}
	NumberFormatOptionSelector( QWidget* parent = nullptr )
		: QComboBox( parent )
	{
		setDisabled( true );
		setMaximumWidth( 120 );
		setSizeAdjustPolicy( QComboBox::AdjustToContents );
		connect( this, SIGNAL(currentIndexChanged(int)),
				 this, SLOT(setOptionInd(int)) );
	}
	void updateOptions( const Axis::NumberFormat& axNbFrmt )
	{
		clear();
		setDisabled( false );
		switch( axNbFrmt )
		{
			case Axis::SCIENTIFIC:
				addItems( {"e", "E", "×10", "D"} );
			break;
			case Axis::POWEROF:
				addItems( {"10", "2", "e"} );
			break;
			case Axis::MULTIPLE:
				addItems( {"π", "e"} );
			break;
			case Axis::TIME:
				addItems( {"HH:MM", "HHhMM", "HH:MM:SS"} );
			break;
			case Axis::DATE:
				addItems( {"DD/MM/YYYY", "", ""} );
			break;
			case Axis::TIMEDATE:
				addItems( {"", "", ""} );
			break;
			default:
				setDisabled( true );
				emit changed( -1 );
			break;
		}
	}
	void setOption( char numberFormat )
	{
		setCurrentIndex( (int)numberFormat );
	}

public slots:
	void setOptionInd( int index )
	{
		emit changed( (char)index );
	}

signals:
	void changed( char numberFormat );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // NUMBERFORMATOPTIONSELECTOR_H
