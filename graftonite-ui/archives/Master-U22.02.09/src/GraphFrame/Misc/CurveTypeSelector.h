#ifndef CURVETYPESELECTOR_H
#define CURVETYPESELECTOR_H

#include <QComboBox>

#include "../Curve.h"

/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class CurveTypeSelector
		: public QComboBox
{
	Q_OBJECT
public:
	~CurveTypeSelector()
	{

	}
	CurveTypeSelector( QWidget* parent = nullptr )
		: QComboBox( parent )
	{
		addItems( { "XY Curve", "XYZ Curve", "XYY Curve" } );
		setMaximumWidth( 120 );
		setSizeAdjustPolicy( QComboBox::AdjustToContents );
		connect( this, SIGNAL(currentIndexChanged(int)),
				 this, SLOT(setTypeInd(int)) );
	}
	void setType( const Curve::Type& type )
	{
		int ind = 0;
		if ( type == Curve::XYZ )
			ind = 1;
		else if ( type == Curve::XYY )
			ind = 2;

		setCurrentIndex( ind );
	}

public slots:
	void setTypeInd( int index )
	{
		Curve::Type sett = Curve::XY;
		switch ( index ) {
			case 1:
				sett = Curve::XYZ;
			break;
			case 2:
				sett = Curve::XYY;
			break;
		}
		emit changed( sett );
	}

signals:
	void changed( const Curve::Type& type );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // CURVETYPESELECTOR_H
