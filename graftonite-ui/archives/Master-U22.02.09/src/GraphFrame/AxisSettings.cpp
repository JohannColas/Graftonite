#include "AxisSettings.h"

#include <QFont>

#include <QDebug>
/**********************************************/
/**********************************************/
/* */
AxisSettings::AxisSettings()
{

}
/**********************************************/
/**********************************************/
/* */
void AxisSettings::configToNewType()
{
	if ( type == Axis::XAxis )
	{
		setPos( Axis::BOTTOM );
		setLblPos( Axis::Pos::BOTTOM );
		setLblAnchor( Axis::Pos::TOPCENTER );
		setTlPos( Axis::Pos::BOTTOMCENTER );
		setTlAnchor( Axis::Pos::TOPCENTER );
		setTlAngle( 0 );
		setTlText( "<p style=\"font-size:14pt;color:Navy;font-weight:bold;\">X Axis</p>" );
	}
	else if ( type == Axis::YAxis )
	{
		setPos( Axis::LEFT );
		setLblPos( Axis::Pos::LEFT );
		setLblAnchor( Axis::Pos::MIDDLERIGHT );
		setTlPos( Axis::Pos::MIDDLELEFT );
		setTlAnchor( Axis::Pos::BOTTOMCENTER );
		setTlAngle( 90 );
		setTlText( "<p style=\"font-size:14pt;color:DarkRed;font-weight:bold;\">Y Axis</p>" );
	}
	else if ( type == Axis::RadAxis )
	{

	}
}
/**********************************************/
/**********************************************/
/* */
void AxisSettings::calculateTkPos( GraphRect& layerRect )
{
	tkmaPos.clear();
	tkmiPos.clear();
	labels.clear();

//	// Get the coefficient and the constant of the coef*y+cons equation
//	updateConvParam();

	double coef1 = layerRect.w();
	double const1 = layerRect.x();
	if  ( type == Axis::YAxis )
	{
		coef1 = -layerRect.h();
		const1 = layerRect.h() + layerRect.y();
	}
	else if  ( type == Axis::RadAxis )
	{
	}
	double maxBound = this->maxBound;
	double minBound = this->minBound;
	Axis::Scale scale = getScale();
	double scaleOffset = getScaleOffset();
	double coef2 = maxBound - minBound;
	double const2 = minBound + scaleOffset;
	if ( scale != Axis::LINEAR )
	{
		if ( minBound+scaleOffset != 0 )
		{

		}
		if ( maxBound+scaleOffset != 0 )
		{

		}
	}
	if ( scale == Axis::LN )
	{
		coef2 = log( ( maxBound+scaleOffset ) / ( minBound+scaleOffset ) );
		const2 = log( minBound+scaleOffset );
	}
	else if ( scale == Axis::LOG10 )
	{
		coef2 = log10( ( maxBound+scaleOffset ) / ( minBound+scaleOffset ) );
		const2 = log10( minBound+scaleOffset );
	}
	else if ( scale == Axis::LOG2 )
	{
		coef2 = log2( ( maxBound+scaleOffset ) / ( minBound+scaleOffset ) );
		const2 = log2( minBound+scaleOffset );
	}
	else if ( scale == Axis::RECIPROCAL )
	{
		coef2 = 1/( maxBound+scaleOffset ) - 1/( minBound+scaleOffset );
		const2 = 1/( minBound+scaleOffset );
	}
	coef = coef1 / coef2;
	cons = const1 - coef * const2 ;

	if ( scale == Axis::LINEAR )
	{
	// Check which Variables have the Biggest Number of  Decimal
		int power = GraphMath::biggestNBDecimal( {minBound, tkmiInc,
																					   scaleOffset} );
	// Multiply All the Variables to Obtain Only long long
		int mulp = pow(10, power);
		long long startLG = minBound * mulp;
		long long endLG = maxBound * mulp;
		long long majorSLG = tkmaInc * mulp;
		long long minorSLG = tkmiInc * mulp;
		long long offsetLG = scaleOffset * mulp;

		char direc = 1;
		if ( startLG > endLG ) direc = -1;

		if ( majorSLG != 0 )
		{
			long long tickLG = GraphMath::getFirstTick(startLG, endLG, majorSLG);
			while ( GraphMath::stopCondition(tickLG, endLG, direc) )
			{
				tkmaPos.append( coef * (tickLG - offsetLG)/double(mulp) + cons );
				// Adding Labels text
				labels.append( QString::number(tickLG/double(mulp)) );
				tickLG += direc * majorSLG;
			}
		}
		if ( minorSLG != 0 )
		{
			 long long  tickLG = GraphMath::getFirstTick(startLG, endLG, minorSLG);
			while ( GraphMath::stopCondition(tickLG, endLG, direc) )
			{
				if  ( tickLG % majorSLG != 0 )
					tkmiPos.append( coef * (tickLG - offsetLG)/double(mulp) + cons );
				tickLG += direc * minorSLG;
			}
		}
	}
	else if ( scale == Axis::LN )
	{
	}
	else if ( scale == Axis::LOG10 )
	{
	}
	else if ( scale == Axis::LOG2 )
	{
	}
	else if ( scale == Axis::RECIPROCAL )
	{
	}
}
/**********************************************/
/**********************************************/
/* */
void AxisSettings::readSettings( QSettings* setsFile )
{
	blockSignals( true );
	setID( setsFile->value("id").toString() );
	setLayerID( setsFile->value("layerID").toString() );
	setType( (Axis::Type)setsFile->value("type").toInt() );
	setPos( (Axis::Pos)setsFile->value("pos").toInt() );
	setPosOffset( setsFile->value("posOffset").toInt() );
	setPosAt( setsFile->value("posAt").toDouble() );
	setScale( (Axis::Scale)setsFile->value("scale").toInt() );
	setScaleOffset( setsFile->value("scaleOffset").toDouble() );
	setMinBound( setsFile->value("minBound").toDouble() );
	setMaxBound( setsFile->value("maxBound").toDouble() );
	// Reading Title Settings
	setTlPos( (Axis::Pos)setsFile->value("titlePos").toInt() );
	setTlAnchor( (Axis::Pos)setsFile->value("titleAnchor").toInt() );
	setTlOffset( setsFile->value("titleOffset").toPointF() );
	setTlAngle( setsFile->value("titleAngle").toDouble() );
	setTlText( setsFile->value("titleText").toString() );
	// Reading Line Settings
	setLnStyle( (Qt::PenStyle)setsFile->value("lineStyle").toInt() );
	setLnWidth( setsFile->value("lineWidth").toInt() );
	setLnColor( QColor( setsFile->value("lineColor").toString() ) );
	setLnCap( (Qt::PenCapStyle)setsFile->value("lineCap").toInt() );
	// Reading Ticks Settings
	setTkPos( (Axis::Pos)setsFile->value("ticksPos").toInt() );
	setTkmaInc( setsFile->value("ticksMajInc").toDouble() );
	setTkmaSize( setsFile->value("ticksMajSize").toInt() );
	setTkmiInc( setsFile->value("ticksMinInc").toDouble() );
	setTkmiSize( setsFile->value("ticksMinSize").toInt() );
	// Reading Labels Settings
	setLblPos( (Axis::Pos)setsFile->value("labelsPos").toInt() );
	setLblAnchor( (Axis::Pos)setsFile->value("labelsAnchor").toInt() );
	setLblOffset( setsFile->value("labelsOffset").toPointF() );
	setLblAngle( setsFile->value("labelsAngle").toDouble() );
	setLblNumberFormat( (Axis::NumberFormat)setsFile->value("labelsNbFrmt").toInt() );
	setLblNumberFormatOption( setsFile->value("labelsNbFrmtOpt").toInt() );
	setLblNumberDecimalSeparator( setsFile->value("labelsNbDecSep").toInt() );
	setLblNumberPrec( setsFile->value("labelsNbPrec").toInt() );
	QFont font;
	font.setFamily( setsFile->value("labelsFtFamily").toString() );
	font.setPointSizeF( setsFile->value("labelsFtSize").toDouble() );
	font.setCapitalization( (QFont::Capitalization)setsFile->value("labelsFtCap").toInt() );
	font.setItalic( setsFile->value("labelsFtItalic").toBool() );
	font.setBold( setsFile->value("labelsFtBold").toBool() );
	font.setUnderline( setsFile->value("labelsFtUnder").toBool() );
	setLblFont( font );
	setLblFtColor( QColor( setsFile->value("labelsFtColor").toString() ) );
	setLblPrefix( setsFile->value("labelsPrefix").toString() );
	setLblSuffix( setsFile->value("labelsSuffix").toString() );
	// Reading Grids Settings
	setGdPos( (Axis::Pos)setsFile->value("gridsPos").toInt() );
	setGdmaStyle( (Qt::PenStyle)setsFile->value("gridsMajStyle").toInt() );
	setGdmaWidth( setsFile->value("gridsMajWidth").toInt() );
	setGdmaColor( QColor( setsFile->value("gridsMajColor").toString() ) );
	setGdmiStyle( (Qt::PenStyle)setsFile->value("gridsMinStyle").toInt() );
	setGdmiWidth( setsFile->value("gridsMinWidth").toInt() );
	setGdmiColor( QColor( setsFile->value("gridsMinColor").toString() ) );
	blockSignals( false );
}
/**********************************************/
/**********************************************/
/* */
void AxisSettings::saveSettings( QSettings* setsFile ) {
	// Adding General Settings
	setsFile->setValue("id", getID() );
	setsFile->setValue("layerID", getLayerID() );
	setsFile->setValue("type", (int)getType() );
	setsFile->setValue("pos", (int)getPos() );
	setsFile->setValue("posOffset", getPosOffset() );
	setsFile->setValue("posAt", getPosAt() );
	setsFile->setValue("scale", (int)getScale() );
	setsFile->setValue("scaleOffset", getScaleOffset() );
	setsFile->setValue("minBound", getMinBound() );
	setsFile->setValue("maxBound", getMaxBound() );
	// Adding Title Settings
	setsFile->setValue("titlePos", (int)getTlPos() );
	setsFile->setValue("titleAnchor", (int)getTlAnchor() );
	setsFile->setValue("titleOffset", QVariant(getTlOffset()) );
	setsFile->setValue("titleAngle", getTlAngle() );
	setsFile->setValue("titleText", getTlText() );
	// Adding Line Settings
	setsFile->setValue("lineStyle", (int)getLnStyle() );
	setsFile->setValue("lineWidth", getLnWidth() );
	setsFile->setValue("lineColor", QVariant( getLnColor() ).toString() );
	setsFile->setValue("lineJoin", (int)getLnCap() );
	// Adding Ticks Settings
	setsFile->setValue("ticksPos", (int)getTkPos() );
	setsFile->setValue("ticksMajInc", getTkmaInc() );
	setsFile->setValue("ticksMajSize", getTkmaSize() );
	setsFile->setValue("ticksMinInc", getTkmiInc() );
	setsFile->setValue("ticksMinSize", getTkmiSize() );
	// Adding Labels Settings
	setsFile->setValue("labelsPos", (int)getLblPos() );
	setsFile->setValue("labelsAnchor", (int)getLblAnchor() );
	setsFile->setValue("labelsOffset", QVariant(getLblOffset()) );
	setsFile->setValue("labelsAngle", getLblAngle() );
	setsFile->setValue("labelsNbFrmt", (int)getLblNumberFormat() );
	setsFile->setValue("labelsNbFrmtOpt", getLblNumberFormatOption() );
	setsFile->setValue("labelsNbDecSep", getLblNumberDecimalSeparator() );
	setsFile->setValue("labelsNbPrec", getLblNumberPrecision() );
	QFont font = getLblFont();
	setsFile->setValue("labelsFtFamily", font.family() );
	setsFile->setValue("labelsFtSize", font.pointSizeF() );
	setsFile->setValue("labelsFtCap", font.capitalization() );
	setsFile->setValue("labelsFtItalic", font.italic() );
	setsFile->setValue("labelsFtBold", font.bold() );
	setsFile->setValue("labelsFtUnder", font.underline() );
	setsFile->setValue("labelsFtColor", QVariant( getLblFtColor() ).toString() );
	setsFile->setValue("labelsPrefix", getLblPrefix() );
	setsFile->setValue("labelsSuffix", getLblSuffix() );
	// Adding Grids Settings
	setsFile->setValue("gridsPos", (int)getGdPos() );
	setsFile->setValue("gridsMajStyle", (int)getGdmaStyle() );
	setsFile->setValue("gridsMajWidth", getGdmaWidth() );
	setsFile->setValue("gridsMajColor", QVariant( getGdmaColor() ).toString() );
	setsFile->setValue("gridsMinStyle", (int)getGdmiStyle() );
	setsFile->setValue("gridsMinWidth", getGdmiWidth() );
	setsFile->setValue("gridsMinColor", QVariant( getGdmiColor() ).toString() );
}
/**********************************************/
/**********************************************/
