#include "LayerSettingsView.h"

#include <QColorDialog>
/**********************************************/
/**********************************************/
/* */
LayerSettingsView::~LayerSettingsView()
{
}
/**********************************************/
/**********************************************/
/* */
LayerSettingsView::LayerSettingsView(QWidget *parent)
	: QWidget(parent)
{
	FormLayout* lay_layerSets = new FormLayout;
	sel_X->setDimension();
	lay_layerSets->addRow( lab_X, sel_X );
	sel_Y->setDimension();
	lay_layerSets->addRow( lab_Y, sel_Y );
	sel_Width->setDimension();
	lay_layerSets->addRow( lab_Width, sel_Width );
	sel_Height->setDimension();
	lay_layerSets->addRow( lab_Height, sel_Height );
	lay_layerSets->addRow( lab_Background );
	lay_layerSets->addRow( lab_BkColor, sel_BkColor );
	lay_layerSets->addRow( lab_Borders );
	lay_layerSets->addRow( lab_BdStyle, sel_BdStyle );
	sel_BdWidth->setBorderWidth();
	lay_layerSets->addRow( lab_BdWidth, sel_BdWidth );
	lay_layerSets->addRow( lab_BdColor, sel_BdColor );
	lay_layerSets->addRow( lab_BdJoin, sel_BdJoin );
	QWidget* graphWid = new QWidget;
	graphWid->setMinimumWidth( 220 );
	graphWid->setLayout( lay_layerSets );
	wid_layerSettings->setWidget( graphWid );
//	wid_layerSettings->setLayout( lay_layerSets );

	GridLayout* lay_main = new GridLayout;
//	lay_main->setMargin( 0 );
	lay_main->addWidget( sel_ID, 0, 0 );
	lay_main->addWidget( wid_layerSettings, 1, 0 );
	this->setLayout( lay_main );
}
/**********************************************/
/**********************************************/
/* */
LayerSettings *LayerSettingsView::getSettings() const
{
	return settings;
}
/**********************************************/
/**********************************************/
/* */
void LayerSettingsView::setSettings(LayerSettings *value)
{
	disconnectModifiers();

	settings = value;

	connectModifiers();

	update();
}
/**********************************************/
/**********************************************/
/* */
void LayerSettingsView::connectModifiers()
{
	connect( sel_ID, &LineEditor::editingFinished,
			 this, &LayerSettingsView::editingIDisFinished );
	connect( sel_X, &IntegerSelector::changed,
			 settings, &LayerSettings::setX );
	connect( sel_Y, &IntegerSelector::changed,
			 settings, &LayerSettings::setY );
	connect( sel_Width, &IntegerSelector::changed,
			 settings, &LayerSettings::setWidth );
	connect( sel_Height, &IntegerSelector::changed,
			 settings, &LayerSettings::setHeight );
	connect( sel_BkColor, &ColorSelector::changed,
			 settings, &LayerSettings::setBkColor );
	connect( sel_BdStyle, &LineStyleSelector::changed,
			 settings, &LayerSettings::setBdStyle );
	connect( sel_BdWidth, &IntegerSelector::changed,
			 settings, &LayerSettings::setBdWidth );
	connect( sel_BdColor, &ColorSelector::changed,
			 settings, &LayerSettings::setBdColor );
	connect( sel_BdJoin, &LineJoinSelector::changed,
			 settings, &LayerSettings::setBdJoin );
}
/**********************************************/
/**********************************************/
/* */
void LayerSettingsView::disconnectModifiers()
{
	if ( settings != nullptr )
	{
		disconnect( sel_ID, &LineEditor::editingFinished,
				 this, &LayerSettingsView::editingIDisFinished );
		disconnect( sel_X, &IntegerSelector::changed,
				 settings, &LayerSettings::setX );
		disconnect( sel_Y, &IntegerSelector::changed,
				 settings, &LayerSettings::setY );
		disconnect( sel_Width, &IntegerSelector::changed,
				 settings, &LayerSettings::setWidth );
		disconnect( sel_Height, &IntegerSelector::changed,
				 settings, &LayerSettings::setHeight );
		disconnect( sel_BkColor, &ColorSelector::changed,
				 settings, &LayerSettings::setBkColor );
		disconnect( sel_BdStyle, &LineStyleSelector::changed,
				 settings, &LayerSettings::setBdStyle );
		disconnect( sel_BdWidth, &IntegerSelector::changed,
				 settings, &LayerSettings::setBdWidth );
		disconnect( sel_BdColor, &ColorSelector::changed,
				 settings, &LayerSettings::setBdColor );
		disconnect( sel_BdJoin, &LineJoinSelector::changed,
				 settings, &LayerSettings::setBdJoin );
	}
}
/**********************************************/
/**********************************************/
/* */
void LayerSettingsView::update()
{
	sel_ID->setText( settings->getID() );
	sel_X->setValue( settings->getX() );
	sel_Y->setValue( settings->getY() );
	sel_Width->setValue( settings->getWidth() );
	sel_Height->setValue( settings->getHeight() );
	sel_BkColor->setColor( settings->getBkColor() );
	sel_BdStyle->setStyle( settings->getBdStyle() );
	sel_BdWidth->setValue( settings->getBdWidth() );
	sel_BdColor->setColor( settings->getBdColor() );
	sel_BdJoin->setStyle( settings->getBdJoin() );
}
/**********************************************/
/**********************************************/
/* */
void LayerSettingsView::editingIDisFinished()
{
	QString oldID = settings->getID();
	QString newID = sel_ID->text();
	emit settings->idChanged( oldID, newID );
	settings->setID( newID );
}
/**********************************************/
/**********************************************/
/* */
void LayerSettingsView::updateIcons( Icons* icons ) {
Q_UNUSED(icons)
}
/**********************************************/
/**********************************************/
/* */
void LayerSettingsView::updateLang( Lang* lang ) {
	lab_X->setText( "X :" );
	lab_Y->setText( "Y :" );
	lab_Width->setText( lang->get("width") + " :" );
	lab_Height->setText( lang->get("height") + " :" );
	lab_Background->setText( lang->get("background") );
	lab_BkColor->setText( lang->get("color") + " :" );
	lab_Borders->setText( lang->get("borders") );
	lab_BdStyle->setText( lang->get("style") + " :" );
	sel_BdStyle->updateLang( lang );
	lab_BdWidth->setText( lang->get("width") + " :" );
	lab_BdColor->setText( lang->get("color") + " :" );
	lab_BdJoin->setText( lang->get("join") + " :" );
	sel_BdJoin->updateLang( lang );
}
/**********************************************/
/**********************************************/
