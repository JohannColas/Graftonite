#ifndef LAYERSETTINGS_H
#define LAYERSETTINGS_H

#include <QColor>
#include <QPen>
#include <QSettings>

#include "Misc/GraphMisc.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class LayerSettings : public QObject
{
    Q_OBJECT
protected:
    QString id = "Layer 1";
    // Position & Sizes parameters of Layer
    int x = 100;
    int y = 100;
    int width = 500;
    int height = 300;
    // Border parameters of Layer
    Qt::PenStyle bdStyle = Qt::SolidLine;
    QColor bdColor = QColor(186, 30, 40, 255);
    int bdWidth = 2;
    Qt::PenJoinStyle bdJoin = Qt::RoundJoin;
    int bdMiter = 8;
    // Background parameters of Layer
    QColor bkColor = QColor(25, 55, 165, 255);

public:
    LayerSettings();
	QString getID() const;
	void setID(const QString &value);
	GraphRect getRect() const;
	int getX() const;
	int getY() const;
	int getWidth() const;
	int getHeight() const;
    QPen getBorderPen() const;
	Qt::PenStyle getBdStyle() const;
	QColor getBdColor() const;
	int getBdWidth() const;
	Qt::PenJoinStyle getBdJoin() const;
	int getBdMiter() const;
    QColor getBkColor() const;

public slots:
	void setY(int value);
	void setX(int value);
	void setWidth(int value);
	void setHeight(int value);
	void setBkColor(const QColor &value);
	void setBdStyle(Qt::PenStyle value);
	void setBdColor(const QColor &value);
	void setBdWidth(int value);
	void setBdJoin(const Qt::PenJoinStyle &value);
	void setBdMiter(int value);
	void readSettings( QSettings* setsFile );
	void saveSettings( QSettings* setsFile );

signals:
	void changed();
	void idChanged(QString, QString);
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LAYERSETTINGS_H
