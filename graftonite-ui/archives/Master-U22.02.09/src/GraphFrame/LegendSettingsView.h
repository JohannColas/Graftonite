#ifndef LEGENDSETTINGSVIEW_H
#define LEGENDSETTINGSVIEW_H

#include <QWidget>

#include "LegendSettings.h"
#include "Commons/theme.h"
#include "Commons/lang.h"
#include "../GraphFrame/Axis.h"
#include "Commons/ScrollArea.h"
#include "Commons/FormLayout.h"
#include "Commons/GridLayout.h"
#include "Commons/SettingLabel.h"
#include "../GraphFrame/Misc/LegendPosSelector.h"
#include "../GraphFrame/Misc/AnchorSelector.h"
#include "../GraphFrame/Misc/TextAlignSelector.h"
#include "../GraphFrame/Misc/TextCapSelector.h"
#include "Commons/LineEditor.h"
#include "Commons/IntegerSelector.h"
#include "Commons/DoubleSelector.h"
#include "Commons/ColorSelector.h"
#include "../GraphFrame/Misc/TextEditor.h"
#include <QFontComboBox>

/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class LegendSettingsView
		: public QWidget
{
	Q_OBJECT
private:
	LegendSettings* settings = nullptr;
	LineEditor* sel_ID = new LineEditor;
	ScrollArea* wid_legendSettings = new ScrollArea;
	SettingLabel* lab_legend = new SettingLabel;
	SettingLabel* lab_Pos = new SettingLabel;
	LegendPosSelector* sel_Pos = new LegendPosSelector;
	SettingLabel* lab_Anchor = new SettingLabel;
	AnchorSelector* sel_Anchor = new AnchorSelector;
	SettingLabel* lab_Offset = new SettingLabel;
	DoubleSelector* sel_XOffset = new DoubleSelector;
	DoubleSelector* sel_YOffset = new DoubleSelector;
	// Font
	SettingLabel* lab_Font = new SettingLabel;
	QFontComboBox* sel_FtFamily = new QFontComboBox;
	IntegerSelector* sel_FtSize = new IntegerSelector;
	TextCapSelector* sel_FtCap = new TextCapSelector;
	ColorSelector* sel_FtColor = new ColorSelector;
	QPushButton* sel_FtItalic = new QPushButton;
	QPushButton* sel_FtBold = new QPushButton;
	QPushButton* sel_FtUnder = new QPushButton;
	QPushButton* sel_FtExponant = new QPushButton;
	QPushButton* sel_FtIndice = new QPushButton;
	TextAlignSelector* sel_Align = new TextAlignSelector;
	TextEditor* edt_Title = new TextEditor;

public:
	LegendSettingsView( QWidget* parent = nullptr );
	void setSettings( LegendSettings* settings );
	void updateIcons( Icons* icons );
	void updateLang( Lang* lang );

public slots:
	void update();
	void disableWidget();
	void connectModifiers();
	void disconnectModifiers();
	void blockChildrenSignals( QObject* obj, bool checked );
	void posChanged( const Axis::Pos& pos );
	void titleTextChanged();
	void titleText_cursorPosChanged();
	void ftExponantChanged( bool checked );
	void ftIndiceChanged( bool checked );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LEGENDSETTINGSVIEW_H
