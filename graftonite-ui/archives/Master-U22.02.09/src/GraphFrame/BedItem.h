#ifndef BEDITEM_H
#define BEDITEM_H

#include <QGraphicsObject>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class BedItem : public QGraphicsObject
{
    Q_OBJECT
public:
    virtual ~BedItem();
    BedItem( QGraphicsItem *parent = nullptr );
    virtual QRectF boundingRect() const;
    virtual void paint( QPainter *, const QStyleOptionGraphicsItem *, QWidget *);;
    virtual void update();;
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // BEDITEM_H
