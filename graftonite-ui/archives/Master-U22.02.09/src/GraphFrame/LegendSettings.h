#ifndef LEGENDSETTINGS_H
#define LEGENDSETTINGS_H

#include <QObject>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class LegendSettings
		: public QObject
{
	Q_OBJECT

private:
	QString id = "";

public:
	LegendSettings();

	QString getID() {
		return id;
	}


public slots:
	void setID( const QString& id ) {
		this->id = id;
		emit changed();
	}


signals:
	void changed();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LEGENDSETTINGS_H
