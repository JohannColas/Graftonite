#ifndef AXISSETTINGSVIEW_H
#define AXISSETTINGSVIEW_H

#include <QWidget>
#include <QAction>

#include "AxisSettings.h"
#include "Axis/AxisLineSettingsView.h"
#include "Axis/AxisTicksSettingsView.h"
#include "Axis/AxisLabelsSettingsView.h"
#include "Axis/AxisTitleSettingsView.h"
#include "Axis/AxisGridsSettingsView.h"
#include "Commons/TabWidget.h"
#include "Commons/ScrollArea.h"
#include "Commons/FormLayout.h"
#include "Commons/GridLayout.h"
#include "Commons/SettingLabel.h"
#include "Commons/LineEditor.h"
#include "Commons/IntegerSelector.h"
#include "Commons/DoubleSelector.h"
#include "Misc/AxisTypeSelector.h"
#include "Misc/AxisPosSelector.h"
#include "Misc/AxisScaleSelector.h"
#include "Misc/ColorButton.h"
#include "Commons/theme.h"
#include "Commons/lang.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class AxisSettingsView
		: public QWidget
{
    Q_OBJECT
private:
	AxisSettings* settings = nullptr;
	TabWidget* tab_settings = new TabWidget;
	AxisLineSettingsView* lineSettings = new AxisLineSettingsView;
	AxisTicksSettingsView* ticksSettings = new AxisTicksSettingsView;
	AxisLabelsSettingsView* labelsSettings = new AxisLabelsSettingsView;
	AxisTitleSettingsView* titleSettings = new AxisTitleSettingsView;
	AxisGridsSettingsView* GridsSettings = new AxisGridsSettingsView;
	// General Axis Settings
	LineEditor* sel_ID = new LineEditor;
	SettingLabel* lab_General = new SettingLabel;
	ScrollArea* sll_settings = new ScrollArea;
	SettingLabel* lab_Type = new SettingLabel;
	AxisTypeSelector* sel_Type = new AxisTypeSelector;
	SettingLabel* lab_Layers = new SettingLabel;
	QComboBox* sel_Layers = new QComboBox;
	SettingLabel* lab_Pos = new SettingLabel;
	AxisPosSelector* sel_Pos = new AxisPosSelector;
	SettingLabel* lab_PosAt = new SettingLabel;
	DoubleSelector* sel_PosAt = new DoubleSelector;
	SettingLabel* lab_Offset = new SettingLabel;
	IntegerSelector* sel_Offset = new IntegerSelector;
	SettingLabel* lab_Scale = new SettingLabel;
	AxisScaleSelector* sel_Scale = new AxisScaleSelector;
	SettingLabel* lab_ScaleOffset = new SettingLabel;
	LineEditor* sel_ScaleOffset = new LineEditor;
	SettingLabel* lab_Bounds = new SettingLabel;
	LineEditor* sel_minBound = new LineEditor;
	LineEditor* sel_maxBound = new LineEditor;

public:
	explicit AxisSettingsView( QWidget *parent = nullptr );
    ~AxisSettingsView();
	void setSettings( AxisSettings* settings );
	void setComboLayers( QStringList layers );
	void setAxisLayer();
	void blockChildrenSignals(QObject* obj, bool block);

public slots:
	void update();
	void connectModifiers();
	void disconnectModifiers();
	void updateIcons( Icons* icons );
	void updateLang( Lang* lang );

private slots:
	void idChanged();
	//
	void typeChanged( const Axis::Type& type );
	void posChanged( const Axis::Pos& pos );
	void disableAxPosAt();
	void scaleOffsetChanged();
	void minBoundChanged();
	void maxBoundChanged();

};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // AXISSETTINGSVIEW_H
