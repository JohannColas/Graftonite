#ifndef AXIS_H
#define AXIS_H
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Axis
{
public:
	enum Type {
		XAxis = 0,
		YAxis = 1,
		RadAxis = 2,
		AngAxis = 3,
		RdrAxis = 4
	};
	enum Pos {
		NONE = 1,
		TOP = 2,
		MIDDLE = 3,
		BOTTOM = 5,
		LEFT = 7,
		CENTER = 11,
		RIGHT = 13,
		TOPLEFT = 14,
		TOPCENTER = 22,
		TOPRIGHT = 26,
		MIDDLELEFT = 21,
		MIDDLECENTER = 33,
		MIDDLERIGHT = 39,
		BOTTOMLEFT = 35,
		BOTTOMCENTER = 55,
		BOTTOMRIGHT = 65,
		SPECIFIED = 17,
		BOTH = 19
	};
	static inline bool isLocated( Axis::Pos pos, Axis::Pos checker )
	{
		if ( pos % checker == 0) { return true; }
		return false;
	};
	enum Scale {
		LINEAR = 1,
		LN = 2,
		LOG10 = 3,
		LOG2 = 4,
		RECIPROCAL = 5
	};
//	enum Anchor {
//		NONEANCH = 1,
//		TOPANCH = 2,
//		MIDDLEANCH = 3,
//		BOTTOMANCH = 5,
//		LEFTANCH = 7,
//		CENTERANCH = 11,
//		RIGHTANCH = 13,
//		TOPLEFTANCH = 14,
//		TOPCENTERANCH = 22,
//		TOPRIGHTANCH = 26,
//		MIDDLELEFTANCH = 21,
//		MIDDLECENTERANCH = 33,
//		MIDDLERIGHTANCH = 39,
//		BOTTOMLEFTANCH = 35,
//		BOTTOMCENTERANCH = 55,
//		BOTTOMRIGHTANCH = 65
//	};
	enum NumberFormat {
		DEFAULTNF = 0,
		DECIMAL = 1,
		SCIENTIFIC = 2,
		POWEROF = 3,
		MULTIPLE = 4,
		TIME = 5,
		DATE = 6,
		TIMEDATE = 7
	};
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // AXIS_H
