#include "CurveItem.h"
/**********************************************/
/**********************************************/
/* */
CurveItem::~CurveItem()
{
	delete symbols;
	delete labels;
	delete line;
	delete errorBars;
	delete bars;
	delete area;
}
/**********************************************/
/**********************************************/
/* */
CurveItem::CurveItem( QGraphicsItem *parent )
{
	area = new CurveAreaItem( parent );
	area->setPos( 150, 140 );
	bars = new CurveBarsItem( parent );
	bars->setPos( 150, 140 );
	errorBars = new CurveErrorBarsItem( parent );
	errorBars->setPos( 150, 140 );
	line = new CurveLineItem( parent );
	line->setPos( 150, 140 );
	labels = new CurveLabelsItem( /*parent*/ );
//	line->setPos( 150, 140 );
	symbols = new CurveSymbolsItem( parent );
	symbols->setPos( 150, 140 );
}
/**********************************************/
/**********************************************/
/* */
void CurveItem::update( CurveSettings* settings )
{
	//
	symbols->update( settings );
//	labels->update( settings );
	line->update( settings );
	errorBars->update( settings );
	bars->update( settings );
	area->update( settings );
}
/**********************************************/
/**********************************************/
