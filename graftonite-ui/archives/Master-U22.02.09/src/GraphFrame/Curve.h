#ifndef CURVE_H
#define CURVE_H
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class Curve
{
public:
	enum Type {
		XY,
		XYY,
		XYZ
	};
	enum Symbol {
		NOSYMBOL,
		CERCLE,
		SQUARE,
		TRIANGLE,
		POLYGON,
		STAR,
		ARROW
	};
	enum Line {
		NOLINE,
		STRAIGHT,
		HORSTART,
		VERSTART,
		HORMIDPOINT,
		VERMIDPOINT,
		CUBICSPLINE,
		AKIMASPLINE
	};
	enum ErrorBar {
		NOERRORBAR,
		NOENDED,
		CAPENDED,
		ARROWENDED
	};
	enum Bar {
	};
	enum Direction {
		NOBAR,
		NOAREA,
		TOXAXIS,
		TOYAXIS,
		TOBOTTOM,
		TOTOP,
		TOLEFT,
		TORIGHT
	};
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // CURVE_H
