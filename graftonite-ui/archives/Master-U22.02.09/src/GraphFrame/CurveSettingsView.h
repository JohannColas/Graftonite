#ifndef CURVESETTINGSVIEW_H
#define CURVESETTINGSVIEW_H

#include <QWidget>

#include "CurveSettings.h"
#include "Commons/theme.h"
#include "Commons/lang.h"
#include "../GraphFrame/Curve/CurveSymbolsSettingsView.h"
#include "../GraphFrame/Curve/CurveLineSettingsView.h"
#include "../GraphFrame/Curve/CurveErrorBarsSettingsView.h"
#include "../GraphFrame/Curve/CurveBarsSettingsView.h"
#include "../GraphFrame/Curve/CurveAreaSettingsView.h"
#include "../GraphFrame/Curve/CurveLabelsSettingsView.h"
#include "Commons/TabWidget.h"
#include "Commons/ScrollArea.h"
#include "Commons/FormLayout.h"
#include "Commons/GridLayout.h"
#include "Commons/SettingLabel.h"
#include "Commons/LineEditor.h"
#include "Commons/IntegerSelector.h"
#include "Commons/DoubleSelector.h"
#include "Misc/CurveTypeSelector.h"
#include "Misc/AxisScaleSelector.h"
#include "Misc/ColorButton.h"

/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class CurveSettingsView
		: public QWidget
{
    Q_OBJECT
private:
	CurveSettings* settings = nullptr;
	TabWidget* tab_settings = new TabWidget;
	CurveSymbolsSettingsView* symbolsSettings = new CurveSymbolsSettingsView;
	CurveLineSettingsView* lineSettings = new CurveLineSettingsView;
	CurveErrorBarsSettingsView* errorBarsSettings = new CurveErrorBarsSettingsView;
	CurveBarsSettingsView* barsSettings = new CurveBarsSettingsView;
	CurveAreaSettingsView* areaSettings = new CurveAreaSettingsView;
	CurveLabelsSettingsView* labelsSettings = new CurveLabelsSettingsView;
	// General Axis Settings
	LineEditor* sel_ID = new LineEditor;
	ScrollArea* sll_settings = new ScrollArea;
	SettingLabel* lab_Type = new SettingLabel;
	CurveTypeSelector* sel_Type = new CurveTypeSelector;
	SettingLabel* lab_Axis1 = new SettingLabel;
	QComboBox* sel_Axis1 = new QComboBox;
	SettingLabel* lab_Axis2 = new SettingLabel;
	QComboBox* sel_Axis2 = new QComboBox;
	SettingLabel* lab_Axis3 = new SettingLabel;
	QComboBox* sel_Axis3 = new QComboBox;
	SettingLabel* lab_Data1 = new SettingLabel;
	QComboBox* sel_Data1 = new QComboBox;
	SettingLabel* lab_Data2 = new SettingLabel;
	QComboBox* sel_Data2 = new QComboBox;
	SettingLabel* lab_Data3 = new SettingLabel;
	QComboBox* sel_Data3 = new QComboBox;

public:
	~CurveSettingsView();
	explicit CurveSettingsView( QWidget *parent = nullptr );
	//
	CurveSettings* getSettings() const;
	void setSettings( CurveSettings* sets );
	void updateIcons( Icons* icons );
	void updateLang( Lang* lang );

public slots:
	void update();
	void connectModifiers();
	void disconnectModifiers();
	void idChanged();
	void typeChanged( const Curve::Type& type );
	void setUselessWidgets();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // CURVESETTINGSVIEW_H
