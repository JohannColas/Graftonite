#ifndef CURVEITEM_H
#define CURVEITEM_H

#include <QObject>
#include <QGraphicsItem>

#include "CurveSettings.h"
#include "Curve/CurveSymbolsItem.h"
#include "Curve/CurveLabelsItem.h"
#include "Curve/CurveLineItem.h"
#include "Curve/CurveErrorBarsItem.h"
#include "Curve/CurveBarsItem.h"
#include "Curve/CurveAreaItem.h"

#include "GraphSettings.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class CurveItem : public QObject
{
	Q_OBJECT
private:
	CurveSettings* settings = nullptr;
//	CurveData data;
	CurveSymbolsItem* symbols = nullptr;
	CurveLabelsItem* labels = nullptr;
	CurveLineItem* line = nullptr;
	CurveErrorBarsItem* errorBars = nullptr;
	CurveBarsItem* bars = nullptr;
	CurveAreaItem* area = nullptr;

public:
	~CurveItem();
	CurveItem( QGraphicsItem *parent = nullptr );
	void update( CurveSettings* settings );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // CURVEITEM_H
