#include "AxisItem.h"

#include <iostream>
using namespace std;
/**********************************************/
/**********************************************/
/* */
AxisItem::~AxisItem()
{
	delete line;
	delete majorTicks;
	delete minorTicks;
	delete labels;
	delete title;
	delete majorGrids;
	delete minorGrids;
}
/**********************************************/
/**********************************************/
/* */
AxisItem::AxisItem( QGraphicsItem *parent,
					QGraphicsItem *majGdparent,
					QGraphicsItem *minGdparent )
{
	line = new AxisLineItem( parent );
	majorTicks = new AxisTicksItem( parent, true );
	minorTicks = new AxisTicksItem( parent, false );
	labels = new AxisLabelsItem( parent );
	title = new AxisTitleItem( parent );
	majorGrids = new AxisGridsItem( majGdparent, true );
	minorGrids = new AxisGridsItem( minGdparent, false );
}
/**********************************************/
/**********************************************/
/* */
AxisItem::AxisItem( LayerItem* layer )
{
	Q_UNUSED(layer)
//	setLayerRect( layer );
//	setType( type );
	//	updateDefaultSettings();
}
/**********************************************/
/**********************************************/
/* */
void AxisItem::update( AxisSettings* settings, GraphRect& layerRect )
{
	// Updating Axis Line
	line->update( settings, layerRect );
	// Updating Axis Ticks
	settings->calculateTkPos( layerRect );
	minorTicks->update( settings, line->getPos() );
	majorTicks->update( settings, line->getPos() );
	// Updating Axis Labels
	//minorTicks->boundingRect()
//	line->boundingRect()
	QRectF boundingRect = line->boundingRect();
	if( settings->getTkPos() != Axis::NONE )
		boundingRect = majorTicks->boundingRect();

	labels->update( settings, boundingRect );

	QRectF boundingRect2 = boundingRect;
	if( settings->getLblPos() != Axis::NONE )
		boundingRect2 = labels->boundingRect();

	// Updating Axis Title
	title->update( settings, line->boundingRect(), boundingRect, boundingRect2 );
	// Updating Axis Grids
	majorGrids->update( settings, layerRect, line->getPos() );
	minorGrids->update( settings, layerRect, line->getPos() );
}

/**********************************************/
/**********************************************/
