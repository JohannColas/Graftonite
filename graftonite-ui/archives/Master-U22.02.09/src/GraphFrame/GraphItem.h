#ifndef GRAPHITEM_H
#define GRAPHITEM_H

#include <QObject>
#include <QGraphicsRectItem>
#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>

#include "GraphSettings.h"
#include "BedItem.h"

/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class GraphItem : public QObject, public QGraphicsRectItem
{
    Q_OBJECT
private:
	BedItem* _backgroundBed = nullptr;
	BedItem* _minorGridsBed = nullptr;
	BedItem* _majorGridsBed = nullptr;
	BedItem* _curvesBed = nullptr;
	BedItem* _innerAxisBed = nullptr;
	BedItem* _axisBed = nullptr;
	BedItem* _outerAxisBed = nullptr;
	BedItem* _foregroundBed = nullptr;

public:
    ~GraphItem();
	GraphItem( QGraphicsItem *parent = nullptr );
    // Accessors
	BedItem* backgroundBed()
	{ return _backgroundBed; };
	BedItem* minorGridsBed()
	{ return _minorGridsBed; };
	BedItem* majorGridsBed()
	{ return _majorGridsBed; };
	BedItem* curvesBed()
	{ return _curvesBed; };
	BedItem* innerAxisBed()
	{ return _innerAxisBed; };
	BedItem* axisBed()
	{ return _axisBed; };
	BedItem* outerAxisBed()
	{ return _outerAxisBed; };
	BedItem* foregroundBed()
	{ return _foregroundBed; };

private:
    void mouseDoubleClickEvent( QGraphicsSceneMouseEvent *mouseevent ) override;

public slots:
	void init();
	void update();
	void update( GraphSettings* settings );

    // SIGNALS
signals:
    void doubleClicked();
    void scaled();
    void moved(QRect);
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // GRAPHITEM_H
