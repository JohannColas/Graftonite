#ifndef GRAPHSETTINGSVIEW_H
#define GRAPHSETTINGSVIEW_H

#include <QWidget>
#include <QPushButton>
#include <QListWidget>

#include "GraphSettings.h"
#include "LayerSettingsView.h"
#include "AxisSettingsView.h"
#include "CurveSettingsView.h"
#include "LegendSettingsView.h"
#include "ShapeSettingsView.h"
#include "Commons/TabWidget.h"
#include "Commons/ScrollArea.h"
#include "Commons/FormLayout.h"
#include "Commons/GridLayout.h"
#include "Commons/SettingLabel.h"
#include "Commons/PushButton.h"
#include "Commons/IntegerSelector.h"
#include "Misc/LineStyleSelector.h"
#include "Misc/LineJoinSelector.h"
#include "Commons/ColorSelector.h"
#include "Commons/theme.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class GraphSettingsView : public QWidget
{
    Q_OBJECT

protected:
	GraphSettings *settings = nullptr;
	QString currentLayerID = "";
	QString currentAxisID = "";
	QString currentCurveID = "";
	QString currentLegendID = "";
	QString currentShapeID = "";
//	TabWidget* tab_settings = new TabWidget;
	TabWidget* tab_settings = new TabWidget;
	// General Graph Settings
	ScrollArea* wid_graphSettings = new ScrollArea;
	SettingLabel* lab_Width = new SettingLabel;
	IntegerSelector* sel_Width = new IntegerSelector;
	SettingLabel* lab_Height = new SettingLabel;
	IntegerSelector* sel_Height = new IntegerSelector;
	SettingLabel* lab_Background = new SettingLabel;
	SettingLabel* lab_BkColor = new SettingLabel;
	ColorSelector* sel_BkColor = new ColorSelector;
	SettingLabel* lab_Borders = new SettingLabel;
	SettingLabel* lab_BdStyle = new SettingLabel;
	LineStyleSelector* sel_BdStyle = new LineStyleSelector;
	SettingLabel* lab_BdWidth = new SettingLabel;
	IntegerSelector* sel_BdWidth = new IntegerSelector;
	SettingLabel* lab_BdColor = new SettingLabel;
	ColorSelector* sel_BdColor = new ColorSelector;
	SettingLabel* lab_BdJoin = new SettingLabel;
	LineJoinSelector* sel_BdJoin = new LineJoinSelector;
	SettingLabel* lab_GlobalFont = new SettingLabel;
	SettingLabel* lab_GbFont = new SettingLabel;
	SettingLabel* lab_GbFtColor = new SettingLabel;
	// Layer Settings Tab
	SettingLabel* lab_layerSettings = new SettingLabel;
	PushButton* pub_addLayer = new PushButton;
	PushButton* pub_deleteLayer = new PushButton;
	QListWidget* lst_layerSettings = new QListWidget;
	LayerSettingsView* wid_layerSettings = new LayerSettingsView;
	// Axis Settings Tab
	SettingLabel* lab_axesSettings = new SettingLabel;
	PushButton* pub_addAxis = new PushButton;
	PushButton* pub_deleteAxis = new PushButton;
	QListWidget* lst_axesSettings = new QListWidget;
	AxisSettingsView* wid_axisSettings = new AxisSettingsView;
	// Curve Settings Tab
	SettingLabel* lab_curveSettings = new SettingLabel;
	PushButton* pub_addCurve = new PushButton;
	PushButton* pub_deleteCurve = new PushButton;
	QListWidget* lst_curveSettings = new QListWidget;
	CurveSettingsView* wid_curveSettings = new CurveSettingsView;
	// Legend Settings Tab
	SettingLabel* lab_legendSettings = new SettingLabel;
	PushButton* pub_addLegend = new PushButton;
	PushButton* pub_deleteLegend = new PushButton;
	QListWidget* lst_legendSettings = new QListWidget;
	LegendSettingsView* wid_legendSettings = new LegendSettingsView;
	// Shape Settings Tab
	SettingLabel* lab_shapeSettings = new SettingLabel;
	PushButton* pub_addShape = new PushButton;
	PushButton* pub_deleteShape = new PushButton;
	QListWidget* lst_shapeSettings = new QListWidget;
	ShapeSettingsView* wid_shapeSettings = new ShapeSettingsView;


public:
    explicit GraphSettingsView(QWidget *parent = nullptr);
    ~GraphSettingsView();
    GraphSettings *getSettings() const;
	void setSettings( GraphSettings *sets );
	void blockChildrenSignals(QObject* obj, bool block);

public slots:
    void update();
	void updateLayersList();
	void updateAxesList();
	void updateCurvesList();
	void updateLegendsList();
	void updateShapesList();
	void updateLayerWidget( const QString& layer );
	void updateAxisWidget( const QString& axis );
	void updateCurveWidget( const QString& curve );
	void updateLegendWidget( const QString& legend );
	void updateShapeWidget( const QString& shape );
	void connectModifiers();
	void disconnectModifiers();
	void updateIcons( Icons* icons );
	void updateLang( Lang* lang );
	void resizeEvent( QResizeEvent *event ) override;

private slots:
	void addLayer();
	void deleteLayer();
	void addAxis();
	void deleteAxis();
	void addCurve();
	void deleteCurve();
	void addLegend();
	void deleteLegend();
	void addShape();
	void deleteShape();

signals:
	void layerAdded();
	void layerDeleted( QString );
	void axisAdded();
	void axisDeleted( QString );
	void curveAdded();
	void curveDeleted( QString );
	void legendAdded();
	void legendDeleted( QString );
	void shapeAdded();
	void shapeDeleted( QString );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // GRAPHSETTINGSVIEW_H
