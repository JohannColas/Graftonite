#include "AxisSettingsView.h"

#include <iostream>
using namespace std;
/**********************************************/
/**********************************************/
/* */
AxisSettingsView::AxisSettingsView(QWidget *parent) :
	QWidget(parent)
{
	FormLayout* lay_axisSets = new FormLayout;
	lay_axisSets->addRow( lab_General );
	lay_axisSets->addRow( lab_Type, sel_Type );
	lay_axisSets->addRow( lab_Layers, sel_Layers );
	sel_PosAt->setPerCent();
	lay_axisSets->addFullLine( lab_Pos, {sel_Pos, lab_PosAt, sel_PosAt} );
	sel_Offset->setOffset();
	lay_axisSets->addRow( lab_Offset, sel_Offset );
	lay_axisSets->addFullLine( lab_Scale, {sel_Scale, lab_ScaleOffset, sel_ScaleOffset} );
	lay_axisSets->addFullLine( lab_Bounds, {sel_minBound, sel_maxBound} );
	lay_axisSets->addItem( new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding) );

	QWidget* graphWid = new QWidget;
	graphWid->setMinimumWidth( 220 );
	graphWid->setLayout( lay_axisSets );
	sll_settings->setWidget( graphWid );
//	sll_settings->setLayout( lay_axisSets );
	tab_settings->addWidget( "General", sll_settings );

	tab_settings->addWidget( "Line", lineSettings );
	tab_settings->addWidget( "Ticks", ticksSettings );
	tab_settings->addWidget( "Labels", labelsSettings );
	tab_settings->addWidget( "Title", titleSettings );
	tab_settings->addWidget( "Grids", GridsSettings );
	tab_settings->setMinimumHeight( 350 );

	tab_settings->changeTab(0);
	tab_settings->setAlignment( Qt::AlignLeft, Qt::AlignBottom );

	GridLayout* lay_main = new GridLayout;
//	lay_main->setMargin( 0 );
	lay_main->addWidget( sel_ID, 0, 0 );
	lay_main->addWidget( tab_settings, 1, 0 );

	setLayout( lay_main );
}
/**********************************************/
/**********************************************/
/* */
AxisSettingsView::~AxisSettingsView()
{
}
/**********************************************/
/**********************************************/
/* */
void AxisSettingsView::setSettings( AxisSettings* sets )
{
	disconnectModifiers();

	settings = sets;

	connectModifiers();

	// set to others widgets
	lineSettings->setSettings( settings );
	ticksSettings->setSettings( settings );
	labelsSettings->setSettings( settings );
	titleSettings->setSettings( settings );
	GridsSettings->setSettings( settings );
	//
	update();
}
/**********************************************/
/**********************************************/
/* */
void AxisSettingsView::connectModifiers()
{
	// connections
	connect( sel_ID, &LineEditor::editingFinished,
			 this, &AxisSettingsView::idChanged );
	connect( sel_Type, &AxisTypeSelector::changed,
			 this, &AxisSettingsView::typeChanged );
	connect( sel_Layers, &QComboBox::currentTextChanged,
			 settings, &AxisSettings::setLayerID );
	connect( sel_Pos, &AxisPosSelector::changed,
			 this, &AxisSettingsView::posChanged );
	connect( sel_PosAt, &DoubleSelector::changed,
			 settings, &AxisSettings::setPosAt );
	connect( sel_Offset, &IntegerSelector::changed,
			 settings, &AxisSettings::setPosOffset );
	connect( sel_Scale, &AxisScaleSelector::changed,
			 settings, &AxisSettings::setScale );
	connect( sel_ScaleOffset, &LineEditor::editingFinished,
			 this, &AxisSettingsView::scaleOffsetChanged );
	connect( sel_minBound, &LineEditor::editingFinished,
			 this, &AxisSettingsView::minBoundChanged );
	connect( sel_maxBound, &LineEditor::editingFinished,
			 this, &AxisSettingsView::maxBoundChanged );
}
/**********************************************/
/**********************************************/
/* */
void AxisSettingsView::disconnectModifiers()
{
	if ( settings != nullptr )
	{
		// disconnections
		disconnect( sel_ID, &LineEditor::editingFinished,
					this, &AxisSettingsView::idChanged );
		disconnect( sel_Type, &AxisTypeSelector::changed,
					this, &AxisSettingsView::typeChanged );
		disconnect( sel_Layers, &QComboBox::currentTextChanged,
					settings, &AxisSettings::setLayerID );
		disconnect( sel_Pos, &AxisPosSelector::changed,
					this, &AxisSettingsView::posChanged );
		disconnect( sel_PosAt, &DoubleSelector::changed,
					settings, &AxisSettings::setPosAt );
		disconnect( sel_Offset, &IntegerSelector::changed,
					settings, &AxisSettings::setPosOffset );
		disconnect( sel_Scale, &AxisScaleSelector::changed,
					settings, &AxisSettings::setScale );
		disconnect( sel_ScaleOffset, &LineEditor::editingFinished,
					this, &AxisSettingsView::scaleOffsetChanged );
		disconnect( sel_minBound, &LineEditor::editingFinished,
					this, &AxisSettingsView::minBoundChanged );
		disconnect( sel_maxBound, &LineEditor::editingFinished,
					this, &AxisSettingsView::maxBoundChanged );
	}
}
/**********************************************/
/**********************************************/
/* */
void AxisSettingsView::setComboLayers(QStringList layers)
{
	sel_Layers->blockSignals( true );
	sel_Layers->clear();
	sel_Layers->addItems( layers );
	setAxisLayer();
	sel_Layers->blockSignals( false );
}
/**********************************************/
/**********************************************/
/* */
void AxisSettingsView::setAxisLayer()
{
	if ( settings != nullptr)
	{
		for( int it = 0; it < sel_Layers->count(); ++it )
		{
			if( sel_Layers->itemText(it) == settings->getLayerID() )
			{
				sel_Layers->setCurrentIndex( it );
				break;
			}
		}
	}
}
/**********************************************/
/**********************************************/
/* */
void AxisSettingsView::blockChildrenSignals( QObject* obj, bool block )
{
	for( auto elm : obj->children() )
	{
		elm->blockSignals( block );
		blockChildrenSignals( elm, block );
	}
}
/**********************************************/
/**********************************************/
/* */
void AxisSettingsView::update()
{
	// General
	sel_ID->setText( settings->getID() );
	sel_Type->blockSignals( true );
	sel_Type->setType( settings->getType() );
	sel_Type->blockSignals( false );
	setAxisLayer();
	sel_Pos->blockSignals( true );
	sel_Pos->updateItems( settings->getType() );
	sel_Pos->setPos( settings->getPos() );
	disableAxPosAt();
	sel_Pos->blockSignals( false );
	sel_PosAt->setValue( 100*settings->getPosAt() );
	sel_Offset->setValue( settings->getPosOffset() );
	sel_Scale->setScale( settings->getScale() );
	sel_ScaleOffset->setText( QString::number( settings->getScaleOffset() ) );
	sel_minBound->setText( QString::number( settings->getMinBound() ) );
	sel_maxBound->setText( QString::number( settings->getMaxBound() ) );
	//
	lineSettings->update();
	ticksSettings->update();
	labelsSettings->update();
	titleSettings->update();
	GridsSettings->update();
}
/**********************************************/
/**********************************************/
/* */
void AxisSettingsView::idChanged()
{
	QString oldID = settings->getID();
	QString newID = sel_ID->text();
	emit settings->idChanged( oldID, newID );
	settings->setID( newID );
}
/**********************************************/
/**********************************************/
/* */
void AxisSettingsView::typeChanged( const Axis::Type& type )
{
	settings->setType( type );
	update();
	ticksSettings->updatePosList();
	labelsSettings->updatePosList();
	titleSettings->updatePosList();
	GridsSettings->updatePosList();
}
/**********************************************/
/**********************************************/
/* */
void AxisSettingsView::posChanged( const Axis::Pos& pos)
{
	settings->setPos( pos );
	disableAxPosAt();
}
/**********************************************/
/**********************************************/
/* */
void AxisSettingsView::disableAxPosAt()
{
	if ( settings->getPos() == Axis::SPECIFIED )
	{
		lab_PosAt->setDisabled( false );
		sel_PosAt->setDisabled( false );
	}
	else
	{
		lab_PosAt->setDisabled( true );
		sel_PosAt->setDisabled( true );
	}
}
/**********************************************/
/**********************************************/
/* */
void AxisSettingsView::scaleOffsetChanged()
{
	QString offset = sel_ScaleOffset->text();
	bool ok;
	double ret = offset.toDouble(&ok);
	if ( ok )
	{
		settings->setScaleOffset( ret );
	}
	else
	{
		sel_ScaleOffset->setText( QString::number( settings->getScaleOffset() ) );
	}
}
/**********************************************/
/**********************************************/
/* */
void AxisSettingsView::minBoundChanged()
{
	QString bound = sel_minBound->text();
	bool ok;
	double ret = bound.toDouble(&ok);
	if ( ok )
	{
		settings->setMinBound( ret );
	}
	else
	{
		sel_minBound->setText( QString::number( settings->getMinBound() ) );
	}
}
/**********************************************/
/**********************************************/
/* */
void AxisSettingsView::maxBoundChanged()
{
	QString bound = sel_maxBound->text();
	bool ok;
	double ret = bound.toDouble(&ok);
	if ( ok )
	{
		settings->setMaxBound( ret );
	}
	else
	{
		sel_maxBound->setText( QString::number( settings->getMaxBound() ) );
	}
}
/**********************************************/
/**********************************************/
/* */
void AxisSettingsView::updateIcons( Icons* icons ) {
	labelsSettings->updateIcons( icons );
	titleSettings->updateIcons( icons );
}
/**********************************************/
/**********************************************/
/* */
void AxisSettingsView::updateLang( Lang* lang ) {
	tab_settings->setTabName( 0, lang->get("axisGeneral") );
	lab_General->setText( lang->get("axisGeneralSets") );
	lab_Type->setText( lang->get("axisType") + " :" );
	sel_Type->updateLang( lang );
	lab_Layers->setText( lang->get("axisLayer") + " :" );
	lab_Pos->setText( lang->get("axisPos") + " :" );
	sel_Pos->updateLang( lang );
	lab_PosAt->setText( lang->get("axisPosAt") + " :" );
	lab_Offset->setText( lang->get("axisOffset") + " :" );
	lab_Scale->setText( lang->get("axisScale") + " :" );
	sel_Scale->updateLang( lang );
	lab_ScaleOffset->setText( lang->get("axisScaleOffsets") + " :" );
	lab_Bounds->setText( lang->get("axisBounds") + " :" );
	// Line
	tab_settings->setTabName( 1, lang->get("axisLine") );
	lineSettings->updateLang( lang );
	// Ticks
	tab_settings->setTabName( 2, lang->get("axisTicks") );
	ticksSettings->updateLang( lang );
	// Labels
	tab_settings->setTabName( 3, lang->get("axisLabels") );
	labelsSettings->updateLang( lang );
	// Title
	tab_settings->setTabName( 4, lang->get("axisTitle") );
	titleSettings->updateLang( lang );
	// Grids
	tab_settings->setTabName( 5, lang->get("axisGrids") );
	GridsSettings->updateLang( lang );
}
/**********************************************/
/**********************************************/
