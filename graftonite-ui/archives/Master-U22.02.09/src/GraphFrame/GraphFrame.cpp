#include "GraphFrame.h"
/**********************************************/
/**********************************************/
/* */
GraphFrame::~GraphFrame() {
	delete settings;
}

GraphFrame::GraphFrame( QWidget *parent )
	: QSplitter( parent )
{
//	lay_main->setMargin( 0 );
	lay_main->addWidget( lab_title );
	lay_main->addWidget( wid_graphView );
	wid_main->setLayout( lay_main );

	addWidget( wid_main );
	addWidget( wid_toolBar );
	setSizes( {this->width()-250, 250} );

	settings = new GraphSettings();
	settings->readSettings( "./aaa.grf" );
	wid_graphView->update( settings );
	wid_toolBar->setSettings( settings );

	connect( settings, &GraphSettings::changed,
			 this, &GraphFrame::updateGraphView );
	connect( wid_toolBar, &GraphSettingsView::layerAdded,
			 settings, &GraphSettings::addLayer );
	connect( wid_toolBar, &GraphSettingsView::layerDeleted,
			 settings, &GraphSettings::deleteLayer );
	connect( wid_toolBar, &GraphSettingsView::axisAdded,
			 settings, &GraphSettings::addAxis );
	connect( wid_toolBar, &GraphSettingsView::axisDeleted,
			 settings, &GraphSettings::deleteAxis );
	connect( wid_toolBar, &GraphSettingsView::curveAdded,
			 settings, &GraphSettings::addCurve );
	connect( wid_toolBar, &GraphSettingsView::curveDeleted,
			 settings, &GraphSettings::deleteCurve );
	connect( wid_toolBar, &GraphSettingsView::shapeAdded,
			 settings, &GraphSettings::addShape );
	connect( wid_toolBar, &GraphSettingsView::shapeDeleted,
			 settings, &GraphSettings::deleteShape );
	connect( wid_graphView, &GraphView::saveGraph,
			 settings, &GraphSettings::saveSettings );
	hide();
}
/**********************************************/
/**********************************************/
/* */
GraphSettings *GraphFrame::getSettings() const {
	return settings;
}
/**********************************************/
/**********************************************/
/* */
void GraphFrame::setSettings(GraphSettings *value) {
	settings = value;
}
/**********************************************/
/**********************************************/
/* */
void GraphFrame::updateGraphView() {
	wid_graphView->update( settings );
	wid_toolBar->update();
}
/**********************************************/
/**********************************************/
/* */
void GraphFrame::updateIcons( Icons* icons ) {
	wid_toolBar->updateIcons( icons );
}
/**********************************************/
/**********************************************/
/* */
void GraphFrame::updateLang( Lang* lang ) {
	wid_toolBar->updateLang( lang );
}
/**********************************************/
/**********************************************/
/* */
//void GraphFrame::addLayer()
//{
//	settings->addLayer();
//}
/**********************************************/
/**********************************************/
/* */
//void GraphFrame::deleteLayer( QString id )
//{
//	settings->deleteLayer( id );
//}
/**********************************************/
/**********************************************/
/* */
//void GraphFrame::addAxis()
//{
//	settings->addAxis();
//}
/**********************************************/
/**********************************************/
/* */
//void GraphFrame::deleteAxis( QString id )
//{
//	settings->deleteAxis( id );
//}
/**********************************************/
/**********************************************/
/* */
//void GraphFrame::addCurve()
//{
//	settings->addCurve();
//}
/**********************************************/
/**********************************************/
/* */
//void GraphFrame::deleteCurve( QString id )
//{
//	settings->deleteCurve( id );
//}
/**********************************************/
/**********************************************/
