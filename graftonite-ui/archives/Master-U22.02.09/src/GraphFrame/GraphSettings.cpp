#include "GraphSettings.h"
#include "Misc/DeleteDialog.h"

#include <QFile>

#include <iostream>
using namespace std;
/**********************************************/
/**********************************************/
/* */
GraphSettings::~GraphSettings()
{
	if ( setsFile != 0 ) delete setsFile;
	while ( !layersSettings.isEmpty() )
	{
		delete layersSettings.takeFirst();
	}
	while ( !axesSettings.isEmpty() )
	{
		delete axesSettings.takeFirst();
	}
	while ( !curvesSettings.isEmpty() )
	{
		delete curvesSettings.takeFirst();
	}
	while ( !legendsSettings.isEmpty() )
	{
		delete legendsSettings.takeFirst();
	}
}
/**********************************************/
/**********************************************/
/* */
GraphSettings::GraphSettings()
{
	LegendSettings *newLegend = new LegendSettings;
	newLegend->setID("Legend 0");
	connect( newLegend, &LegendSettings::changed, this, &GraphSettings::changed );
	legendsSettings.append( newLegend );
}
/**********************************************/
/**********************************************/
/* */
void GraphSettings::addLayer()
{
	LayerSettings *newLayer = new LayerSettings();
	int idLy = 0;
	for( LayerSettings* elm : layersSettings )
	{
		if ( elm->getID().contains("Layer ") )
		{
			int lyID = elm->getID().remove("Layer ").toInt();
			if ( lyID == idLy )
				++idLy;
			else if ( lyID > idLy )
				idLy = lyID + 1;
		}
	}
	newLayer->setID("Layer "+QString::number(idLy));
	connect( newLayer, &LayerSettings::changed, this, &GraphSettings::changed );
	layersSettings.append( newLayer );
	emit changed();
}
/**********************************************/
/**********************************************/
/* */
void GraphSettings::deleteLayer( QString id )
{
	QList<int> lsAxes;
	QStringList lsAxesID;
	for( int it = 0; it < axesSettings.size(); ++it  )
	{
		AxisSettings* elm = axesSettings.at( it );
		if( elm->getLayerID() == id )
		{
			lsAxes.append( it );
			lsAxesID.append( elm->getID() );
		}
	}
	DeleteDialog confirm;
	confirm.setIDs( {id}, lsAxesID );
	int ret = confirm.exec();
	if( ret == QMessageBox::Yes )
	{
		for( int it = lsAxes.size()-1; it >= 0; --it )
		{
			AxisSettings* elm = axesSettings.at( lsAxes[it] );
			axesSettings.removeAt( lsAxes[it] );
			disconnect( elm, &AxisSettings::changed,
						this, &GraphSettings::changed );
			disconnect( elm, &AxisSettings::idChanged,
						this, &GraphSettings::updateLayersID );
			delete elm;
		}
		for( int it = 0; it < layersSettings.size(); ++it )
		{
			LayerSettings* elm = layersSettings.at( it );
			if ( elm->getID() == id )
			{
				layersSettings.removeAt( it );
				disconnect( elm, &LayerSettings::changed,
							this, &GraphSettings::changed );
				disconnect( elm, &LayerSettings::idChanged,
							this, &GraphSettings::updateLayersID );
				delete elm;
				break;
			}
		}
		emit changed();
	}
}
/**********************************************/
/**********************************************/
/* */
void GraphSettings::addAxis()
{
	if( layersSettings.size() != 0 )
	{
		AxisSettings *newAxis = new AxisSettings();
		int idLy = 0;
		for( AxisSettings* elm : axesSettings )
		{
			if ( elm->getID().contains("Axis ") )
			{
				int lyID = elm->getID().remove("Axis ").toInt();
				if ( lyID == idLy )
					++idLy;
				else if ( lyID > idLy )
					idLy = lyID + 1;
			}
		}
		newAxis->setID("Axis "+QString::number(idLy));
		newAxis->setLayerID( layersSettings.at(0)->getID() );
		newAxis->setType( Axis::XAxis );
		connect( newAxis, &AxisSettings::changed, this, &GraphSettings::changed );
		axesSettings.append( newAxis );
		emit changed();
	}
	else
	{
		QMessageBox msg;
		msg.setText( "You must have at least 1 layer !!");
		msg.setStandardButtons( QMessageBox::Ok );
		msg.exec();
	}
}
/**********************************************/
/**********************************************/
/* */
void GraphSettings::deleteAxis( QString id )
{
	QList<int> lsCurves;
	QStringList lsCurvesID;
	for( int it = 0; it < curvesSettings.size(); ++it  )
	{
		CurveSettings* elm = curvesSettings.at( it );
//		Q_UNUSED( elm )
//		if( elm->getAID() == id )
//		{
//			lsAxes.append( it );
//			lsAxesID.append( elm->getID() );
//		}
	}
	DeleteDialog confirm;
	confirm.setIDs( {}, {id}, lsCurvesID );
	int ret = confirm.exec();
	if( ret == QMessageBox::Yes )
	{
		for( int it = 0; it < axesSettings.size(); ++it )
		{
			AxisSettings* elm = axesSettings.at( it );
			if ( elm->getID() == id )
			{
				axesSettings.removeAt( it );
				disconnect( elm, &AxisSettings::changed,
							this, &GraphSettings::changed );
				disconnect( elm, &AxisSettings::idChanged,
							this, &GraphSettings::updateLayersID );
				delete elm;
				emit changed();
				break;
			}
		}
	}
}
/**********************************************/
/**********************************************/
/* */
void GraphSettings::addCurve()
{
	if( axesSettings.size() > 1 )
	{
		CurveSettings *newCurve = new CurveSettings();
		int idLy = 0;
		for( CurveSettings* elm : curvesSettings )
		{
			if ( elm->getID().contains("Curve ") )
			{
				int lyID = elm->getID().remove("Curve ").toInt();
				if ( lyID == idLy )
					++idLy;
				else if ( lyID > idLy )
					idLy = lyID + 1;
			}
		}
		newCurve->setID("Curve "+QString::number(idLy));
		connect( newCurve, &CurveSettings::changed,
				 this, &GraphSettings::changed );
		curvesSettings.append( newCurve );
		emit changed();
	}
	else
	{
		QMessageBox msg;
		msg.setText( "You must have at least 2 Axis !!");
		msg.setStandardButtons( QMessageBox::Ok );
		msg.exec();
	}
}
/**********************************************/
/**********************************************/
/* */
void GraphSettings::deleteCurve( QString id )
{
	DeleteDialog confirm;
	confirm.setIDs( {}, {}, {id} );
	int ret = confirm.exec();
	if( ret == QMessageBox::Yes )
	{
		for( int it = 0; it < curvesSettings.size(); ++it )
		{
			CurveSettings* elm = curvesSettings.at( it );
			if ( elm->getID() == id )
			{
				curvesSettings.removeAt( it );
				delete elm;
				emit changed();
				break;
			}
		}
	}
}
/**********************************************/
/**********************************************/
/* */
void GraphSettings::addLegend()
{
	//	if( axesSettings.size() > 1 )
		{
			LegendSettings *newLegend = new LegendSettings();
			int idLy = 0;
			for( LegendSettings* elm : legendsSettings )
			{
				if ( elm->getID().contains("Legend ") )
				{
					int lyID = elm->getID().remove("Legend ").toInt();
					if ( lyID == idLy )
						++idLy;
					else if ( lyID > idLy )
						idLy = lyID + 1;
				}
			}
			newLegend->setID("Legend "+QString::number(idLy));
			connect( newLegend, &LegendSettings::changed,
					 this, &GraphSettings::changed );
			legendsSettings.append( newLegend );
			emit changed();
		}
	//	else
	//	{
	//		QMessageBox msg;
	//		msg.setText( "You must have at least 2 Axis !!");
	//		msg.setStandardButtons( QMessageBox::Ok );
	//		msg.exec();
	//	}
}
/**********************************************/
/**********************************************/
/* */
void GraphSettings::deleteLegend( QString id )
{
	DeleteDialog confirm;
	confirm.setIDs( {}, {}, {id} );
	int ret = confirm.exec();
	if( ret == QMessageBox::Yes )
	{
		for( int it = 0; it < legendsSettings.size(); ++it )
		{
			LegendSettings* elm = legendsSettings.at( it );
			if ( elm->getID() == id )
			{
				legendsSettings.removeAt( it );
				delete elm;
				emit changed();
				break;
			}
		}
	}
}
/**********************************************/
/**********************************************/
/* */
void GraphSettings::addShape()
{
//	if( axesSettings.size() > 1 )
	{
		ShapeSettings *newShape = new ShapeSettings();
		int idLy = 0;
		for( ShapeSettings* elm : shapesSettings )
		{
			if ( elm->getID().contains("Shape ") )
			{
				int lyID = elm->getID().remove("Shape ").toInt();
				if ( lyID == idLy )
					++idLy;
				else if ( lyID > idLy )
					idLy = lyID + 1;
			}
		}
		newShape->setID("Shape "+QString::number(idLy));
		connect( newShape, &ShapeSettings::changed,
				 this, &GraphSettings::changed );
		shapesSettings.append( newShape );
		emit changed();
	}
//	else
//	{
//		QMessageBox msg;
//		msg.setText( "You must have at least 2 Axis !!");
//		msg.setStandardButtons( QMessageBox::Ok );
//		msg.exec();
//	}
}
/**********************************************/
/**********************************************/
/* */
void GraphSettings::deleteShape( QString id )
{
	DeleteDialog confirm;
	confirm.setIDs( {}, {}, {}, {id} );
	int ret = confirm.exec();
	if( ret == QMessageBox::Yes )
	{
		for( int it = 0; it < shapesSettings.size(); ++it )
		{
			ShapeSettings* elm = shapesSettings.at( it );
			if ( elm->getID() == id )
			{
				shapesSettings.removeAt( it );
				delete elm;
				emit changed();
				break;
			}
		}
	}
}
/**********************************************/
/**********************************************/
/* */
void GraphSettings::updateLayersID(QString oldID, QString newID)
{
	for( int it = 0; it < axesSettings.size(); ++it )
	{
		AxisSettings* elm = axesSettings.at( it );
		if ( elm->getLayerID() == oldID )
		{
			elm->setLayerID( newID );
		}
	}
	emit changed();
}
/**********************************************/
/**********************************************/
/* */
void GraphSettings::updateAxesID(QString oldID, QString newID)
{
	for( int it = 0; it < curvesSettings.size(); ++it )
	{
		CurveSettings* elm = curvesSettings.at( it );
//		if ( elm->getAxisID() == oldID )
//		{
//			elm->setLayerID( newID );
//		}
	}
	emit changed();
}
/**********************************************/
/**********************************************/
/* */
#include <QDebug>

void GraphSettings::readSettings( const QString& path )
{
	if ( setsFile != 0 ) delete setsFile;
	setsFile = new QSettings( path, QSettings::IniFormat );
	setWidth( setsFile->value("width").toInt() );
	setHeight( setsFile->value("height").toInt() );
	setBkColor( QColor( setsFile->value("bkColor").toString() ) );
	setBdStyle( (Qt::PenStyle)setsFile->value("bdStyle").toInt() );
	setBdWidth( setsFile->value("bdWidth").toInt() );
	setBdColor( QColor( setsFile->value("bdColor").toString() ) );
	setBdJoin( (Qt::PenJoinStyle)setsFile->value("bdJoin").toInt() );
	setBdMiter( setsFile->value("bdMiter").toInt() );
	// Reading Layers Settings
	setsFile->beginGroup("layers");
	int size = setsFile->beginReadArray("layer");
	for ( int it = 0; it < size; ++it ) {
		setsFile->setArrayIndex(it);
		LayerSettings* newLayer = new LayerSettings;
		newLayer->readSettings( setsFile );
		layersSettings.append( newLayer );
		connect( newLayer, &LayerSettings::changed,
				 this, &GraphSettings::changed );
		connect( newLayer, &LayerSettings::idChanged,
				 this, &GraphSettings::updateLayersID );
	}
	setsFile->endArray();
	setsFile->endGroup();
	// Reading Axes Settings
	setsFile->beginGroup("axes");
	size = setsFile->beginReadArray("axis");
	for ( int it = 0; it < size; ++it ) {
		setsFile->setArrayIndex(it);
		AxisSettings* newAxis = new AxisSettings;
		newAxis->readSettings( setsFile );
		axesSettings.append( newAxis );
		connect( newAxis, &AxisSettings::changed,
				 this, &GraphSettings::changed );
		connect( newAxis, &AxisSettings::idChanged,
				 this, &GraphSettings::updateLayersID );
	}
	setsFile->endArray();
	setsFile->endGroup();
	// Reading Curves Settings
	setsFile->beginGroup("curves");
	size = setsFile->beginReadArray("curve");
	for ( int it = 0; it < size; ++it ) {
		setsFile->setArrayIndex(it);
		CurveSettings* newCurve = new CurveSettings;
		newCurve->readSettings( setsFile );
		curvesSettings.append( newCurve );
		connect( newCurve, &CurveSettings::changed,
				 this, &GraphSettings::changed );
	}
	setsFile->endArray();
	setsFile->endGroup();
	// Reading Curves Settings
	setsFile->beginGroup("shapes");
	size = setsFile->beginReadArray("shape");
	for ( int it = 0; it < size; ++it ) {
		setsFile->setArrayIndex(it);
//		ShapeSettings* newShape = new ShapeSettings;
//		newShape->readSettings( setsFile );
//		shapesSettings.append( newShape );
//		connect( newShape, &ShapeSettings::changed,
//				 this, &GraphSettings::changed );
	}
	setsFile->endArray();
	setsFile->endGroup();

	emit changed();
}
/**********************************************/
/**********************************************/
/* */
void GraphSettings::saveSettings()
{
	QString path = setsFile->fileName();
	QFile file ( path );
	file.remove();

	if ( setsFile != 0 ) delete setsFile;
	setsFile = new QSettings( path, QSettings::IniFormat );
	setsFile->setValue("width", getWidth() );
	setsFile->setValue("height", getHeight() );
	setsFile->setValue("bkColor", QVariant( getBkColor() ).toString() );
	setsFile->setValue("bdStyle", (int)getBdStyle() );
	setsFile->setValue("bdWidth", getBdWidth() );
	setsFile->setValue("bdColor", QVariant( getBdColor() ).toString() );
	setsFile->setValue("bdJoin", (int)getBdJoin() );
	setsFile->setValue("bdMiter", getBdMiter() );
	// Adding Layers Settings
	setsFile->beginGroup("layers");
	setsFile->beginWriteArray("layer");
	for ( int it = 0; it < layersSettings.size(); ++it ) {
		setsFile->setArrayIndex(it);
		layersSettings.at(it)->saveSettings( setsFile );
	}
	setsFile->endArray();
	setsFile->endGroup();
	// Adding Axis Settings
	setsFile->beginGroup("axes");
	setsFile->beginWriteArray("axis");
	for ( int it = 0; it < axesSettings.size(); ++it ) {
		setsFile->setArrayIndex(it);
		axesSettings.at(it)->saveSettings( setsFile );
	}
	setsFile->endArray();
	setsFile->endGroup();
	// Adding Curves Settings
	setsFile->beginGroup("curves");
	setsFile->beginWriteArray("curve");
	for ( int it = 0; it < curvesSettings.size(); ++it ) {
		setsFile->setArrayIndex(it);
		curvesSettings.at(it)->saveSettings( setsFile );
	}
	setsFile->endArray();
	setsFile->endGroup();
	// Adding Shapes Settings
	setsFile->beginGroup("shapes");
	setsFile->beginWriteArray("shapee");
	for ( int it = 0; it < shapesSettings.size(); ++it ) {
//		setsFile->setArrayIndex(it);
//		shapesSettings.at(it)->saveSettings( setsFile );
	}
	setsFile->endArray();
	setsFile->endGroup();

}
/**********************************************/
/**********************************************/
