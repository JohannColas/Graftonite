#ifndef CURVESETTINGS_H
#define CURVESETTINGS_H

#include <QObject>
#include <QSettings>
#include <QColor>
#include <QBrush>

#include "Curve.h"
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class CurveSettings : public QObject
{
	Q_OBJECT
private:
	// General Settings
	QString id = "Curve";
	QStringList axesID = {};
	Curve::Type type = Curve::XY;
	// Symbols Settings
	Curve::Symbol symbType = Curve::CERCLE;
	double symbTypeOpt = 4;
	double symbSize = 10;
	double symbCoverage = 0;
	double symbAngle = 0;
	double symbDAngle = 360;
	QColor symbColor = QColor(0, 0, 0 );
	QColor symbBdColor = QColor(0, 0, 0 );
	double symbBdWidth = 2;
	Qt::PenJoinStyle symbBdJoin = Qt::RoundJoin;
	// Line Settings
	Curve::Line lineType = Curve::STRAIGHT;
	double lineGap = 0;
	Qt::PenStyle lineStyle = Qt::SolidLine;
	double lineWidth = 3;
	QColor lineColor = QColor(0, 0, 0 );
	Qt::PenCapStyle lineCap = Qt::RoundCap;
	Qt::PenJoinStyle lineJoin = Qt::RoundJoin;
	// Error Bars Settings
	Curve::ErrorBar errBarsType = Curve::NOERRORBAR;
	QString errBarsData1 = "";
	QString errBarsData2 = "";
	QString errBarsData3 = "";
	Qt::PenStyle errBarsStyle = Qt::SolidLine;
	double errBarsWidth = 3;
	QColor errBarsColor = QColor(0, 0, 0 );
	Qt::PenCapStyle errBarsCap = Qt::RoundCap;
	// Bars Settings
	Curve::Direction barsDirec = Curve::NOBAR;
	double barsWidth = 8;
	Qt::BrushStyle barsPattern = Qt::NoBrush;
	QColor barsColor = QColor( 0, 0, 0 );
	Qt::PenStyle barsBdStyle = Qt::NoPen;
	double barsBdWidth = 0;
	QColor barsBdColor = QColor( 0, 0, 0 );
	Qt::PenJoinStyle barsBdJoin = Qt::RoundJoin;
	// Area Settings
	Curve::Direction areaDirec = Curve::NOAREA;
	Qt::BrushStyle areaPattern = Qt::NoBrush;
	QColor areaColor = QColor( 0, 0, 0 );
	Qt::PenStyle areaBdStyle = Qt::NoPen;
	double areaBdWidth = 0;
	QColor areaBdColor = QColor( 0, 0, 0 );
	Qt::PenJoinStyle areaBdJoin = Qt::RoundJoin;


public:
	~CurveSettings();
	CurveSettings();
	// General Settings
	QString getID() const {
		return id;
	}
	Curve::Type getType() const {
		return type;
	}
	QStringList getAxesID() const {
		return axesID;
	}
	// Symbols Settings
	Curve::Symbol getSymbType() const {
		return symbType;
	}
	double getSymbTypeOpt() const {
		return symbTypeOpt;
	}
	double getSymbSize() const {
		return symbSize;
	}
	double getSymbCoverage() const {
		return symbCoverage;
	}
	double getSymbAngle() const {
		return symbAngle;
	}
	double getSymbDAngle() const {
		return symbDAngle;
	}
	QColor getSymbColor() const {
		return symbColor;
	}
	QColor getSymbBdColor() const {
		return symbBdColor;
	}
	double getSymbBdWidth() const {
		return symbBdWidth;
	}
	Qt::PenJoinStyle getSymbBdJoin() const {
		return symbBdJoin;
	}
	// Line Settings
	Curve::Line getLineType() const {
		return lineType;
	}
	double getLineGap() const {
		return lineGap;
	}
	Qt::PenStyle getLineStyle() const {
		return lineStyle;
	}
	QColor getLineColor() const {
		return lineColor;
	}
	double getLineWidth() const {
		return lineWidth;
	}
	Qt::PenCapStyle getLineCap() const {
		return lineCap;
	}
	Qt::PenJoinStyle getLineJoin() const {
		return lineJoin;
	}
	// Error Bars Settings
	Curve::ErrorBar getErrBarsType() const {
		return errBarsType;
	}
	QString getErrBarsData1() const {
		return errBarsData1;
	}
	QString getErrBarsData2() const {
		return errBarsData2;
	}
	QString getErrBarsData3() const {
		return errBarsData3;
	}
	Qt::PenStyle getErrBarsStyle() const {
		return errBarsStyle;
	}
	double getErrBarsWidth() const {
		return errBarsWidth;
	}
	QColor getErrBarsColor() const {
		return errBarsColor;
	}
	Qt::PenCapStyle getErrBarsCap() const {
		return errBarsCap;
	}
	// Bars Settings
	Curve::Direction getBarsDirec() const {
		return barsDirec;
	}
	double getBarsWidth() const {
		return barsWidth;
	}
	Qt::BrushStyle getBarsPattern() const {
		return barsPattern;
	}
	QColor getBarsColor() const {
		return barsColor;
	}
	Qt::PenStyle getBarsBdStyle() const {
		return barsBdStyle;
	}
	double getBarsBdWidth() const {
		return barsBdWidth;
	}
	QColor getBarsBdColor() const {
		return barsBdColor;
	}
	Qt::PenJoinStyle getBarsBdJoin() const {
		return barsBdJoin;
	}
	// Area Settings
	Curve::Direction getAreaDirec() const {
		return areaDirec;
	}
	Qt::BrushStyle getAreaPattern() const {
		return areaPattern;
	}
	QColor getAreaColor() const {
		return areaColor;
	}
	Qt::PenStyle getAreaBdStyle() const {
		return areaBdStyle;
	}
	double getAreaBdWidth() const {
		return areaBdWidth;
	}
	QColor getAreaBdColor() const {
		return areaBdColor;
	}
	Qt::PenJoinStyle getAreaBdJoin() const {
		return areaBdJoin;
	}

public slots:
	// General Settings
	void setID( const QString& value ) {
		id = value;
		emit changed();
	}
	void setType( const Curve::Type& value ) {
		type = value;
		emit changed();
	}
	void setAxesID( const QStringList& value ) {
		axesID = value;
		emit changed();
	}
	// Symbols Settings
	void setSymbType( const Curve::Symbol& value ) {
		symbType = value;
		emit changed();
	}
	void setSymbTypeOpt( double value ) {
		symbTypeOpt = value;
		emit changed();
	}
	void setSymbSize( double value ) {
		symbSize = value;
		emit changed();
	}
	void setSymbCoverage( double value ) {
		symbCoverage = value;
		emit changed();
	}
	void setSymbAngle( double value ) {
		symbAngle = value;
		emit changed();
	}
	void setSymbDAngle( double value ) {
		symbDAngle = value;
		emit changed();
	}
	void setSymbColor( const QColor& value ) {
		symbColor = value;
		emit changed();
	}
	void setSymbBdColor( const QColor& value ) {
		symbBdColor = value;
		emit changed();
	}
	void setSymbBdWidth( double value ) {
		symbBdWidth = value;
		emit changed();
	}
	void setSymbBdJoin( const Qt::PenJoinStyle& value ) {
		symbBdJoin = value;
		emit changed();
	}
	// Line Settings
	void setLineType( const Curve::Line& value ) {
		lineType = value;
		emit changed();
	}
	void setLineGap( double value ) {
		lineGap = value;
		emit changed();
	}
	void setLineStyle( const Qt::PenStyle& value ) {
		lineStyle = value;
		emit changed();
	}
	void setLineColor( const QColor& value ) {
		lineColor = value;
		emit changed();
	}
	void setLineWidth( double value ) {
		lineWidth = value;
		emit changed();
	}
	void setLineCap( const Qt::PenCapStyle& value ) {
		lineCap = value;
		emit changed();
	}
	void setLineJoin( const Qt::PenJoinStyle& value ) {
		lineJoin = value;
		emit changed();
	}
	// Error Bars Settings
	void setErrBarsType( const Curve::ErrorBar& type) {
		errBarsType = type;
		emit changed();
	}
	void setErrBarsData1( const QString& data ) {
		errBarsData1 = data;
		emit changed();
	}
	void setErrBarsData2( const QString& data ) {
		errBarsData2 = data;
		emit changed();
	}
	void setErrBarsData3( const QString& data ) {
		errBarsData3 = data;
		emit changed();
	}
	void setErrBarsStyle( const Qt::PenStyle& style ) {
		errBarsStyle = style;
		emit changed();
	}
	void setErrBarsWidth( double width ) {
		errBarsWidth = width;
		emit changed();
	}
	void setErrBarsColor( const QColor& color ) {
		errBarsColor = color;
		emit changed();
	}
	void setErrBarsCap( const Qt::PenCapStyle& cap ) {
		errBarsCap = cap;
		emit changed();
	}
	// Bars Settings
	void setBarsDirec( const Curve::Direction& direc ) {
		barsDirec = direc;
		emit changed();
	}
	void setBarsWidth( double width ) {
		barsWidth = width;
		emit changed();
	}
	void setBarsPattern( const Qt::BrushStyle& style ) {
		barsPattern = style;
		emit changed();
	}
	void setBarsColor( const QColor& color ) {
		barsColor = color;
		emit changed();
	}
	void setBarsBdStyle( const Qt::PenStyle& style ) {
		barsBdStyle = style;
		emit changed();
	}
	void setBarsBdWidth( double width ) {
		barsBdWidth = width;
		emit changed();
	}
	void setBarsBdColor( const QColor& color ) {
		barsBdColor = color;
		emit changed();
	}
	void setBarsBdJoin( const Qt::PenJoinStyle& join ) {
		barsBdJoin = join;
		emit changed();
	}
	// Area Settings
	void setAreaDirec( const Curve::Direction& direc ) {
		areaDirec = direc;
		emit changed();
	}
	void setAreaPattern( const Qt::BrushStyle& style ) {
		areaPattern = style;
		emit changed();
	}
	void setAreaColor( const QColor& color ) {
		areaColor = color;
		emit changed();
	}
	 void setAreaBdStyle( const Qt::PenStyle& style ) {
		areaBdStyle = style;
		emit changed();
	}
	void setAreaBdWidth( double width ) {
		areaBdWidth = width;
		emit changed();
	}
	void setAreaBdColor( const QColor& color ) {
		areaBdColor = color;
		emit changed();
	}
	void setAreaBdJoin( const Qt::PenJoinStyle& join ) {
		areaBdJoin = join;
		emit changed();
	}
	void readSettings( QSettings* setsFile );
	void saveSettings( QSettings* setsFile );

signals:
	void changed();
//	void idChanged( QString, QString );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // CURVESETTINGS_H
