#ifndef COMBOBOX_H
#define COMBOBOX_H
/**********************************************/
#include "Popup.h"
#include "PopupButton.h"
/**********************************************/
#include <QListWidgetItem>
/**********************************************/
/**********************************************/
/**********************************************/
class ComboBoxPopup
		: public Popup
{
	Q_OBJECT
	QListWidget* wid_list = 0;
	QStringList _items;
	QString _currentitem;
public:
	ComboBoxPopup( const QStringList& items, QWidget* parent = nullptr );
	/******************************************/
	void updateItems();
	void setCurrentItem( const QString& current_item );
	/******************************************/
protected slots:
	void on_itemPressed( QListWidgetItem* item );
	/******************************************/
signals:
	void currentLabelChanged( const QString& label );
	/******************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
class ComboBox
		: public PopupButton
{
	Q_OBJECT
	QStringList _items;
public:
	ComboBox( QWidget* parent = nullptr );
	ComboBox( const QStringList& items, QWidget* parent = nullptr );
	/**********************************************/
	virtual bool childIsInative() override;
	/**********************************************/
	void addItems( const QStringList& items );
	void setCurrentItem( const QString& item );
	QString currentItem();
	/**********************************************/
protected slots:
	void setItem( const QString& item );
	/**********************************************/
signals:
	void popupLeaved();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // COMBOBOX_H
