#ifndef POPUP_H
#define POPUP_H
/**********************************************/
#include <QWidget>
/**********************************************/
/**********************************************/
/**********************************************/
/**********************************************/
class Popup
		: public QWidget
{
	Q_OBJECT
	bool _mouseHovered = false;
public:
	explicit Popup( QWidget* parent = nullptr );
	/**********************************************/
	virtual bool childIsInactive();
	/**********************************************/
public slots:
	void leaveEvent( QEvent* event ) override;
//	void enterEvent( QEvent* event );
	void enterEvent( QEnterEvent* event ) override;
//	void timerEvent( QTimerEvent *event ) override;
	/**********************************************/
signals:
	void closed();
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // POPUP_H
