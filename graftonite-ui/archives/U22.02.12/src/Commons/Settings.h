#ifndef SETTINGS_H
#define SETTINGS_H
/**********************************************/
#include <QObject>
#include <QPair>
#include <QVariant>
/**********************************************/
#include "Key.h"
/**********************************************/
/**********************************************/
/**********************************************/
class Settings
		: public QObject
{
	Q_OBJECT
private:
	QList<QPair<Key,QVariant>> _settings;
	/******************************************/
public:
	Settings( QObject* parent = nullptr );
	Settings( const Settings& settings );
	/******************************************/
	void addSetting( const Key& key, const QVariant& value );
	void set( const QVariant& value, const Key& key );
	/******************************************/
	QList<Key> keys() const;
	bool hasKey( const Key& key ) const;
	/******************************************/
	QList<QPair<Key,QVariant>> settings() const;
	QVariant value( const Key& key ) const;
	QString getString( const Key& key ) const;
	void getString( QString& value, const Key& key ) const;
	int getInt( const Key& key ) const;
	void getInt( int& value, const Key& key ) const;
	/******************************************/
	static int DecomposeLine( const QString& line, Settings& settings );
	/******************************************/
signals:
	/******************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SETTINGS_H
