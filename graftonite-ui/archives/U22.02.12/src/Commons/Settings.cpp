#include "Settings.h"
/**********************************************/
/**********************************************/
/**********************************************/
/**********************************************/
Settings::Settings( QObject* parent )
	: QObject{parent}
{

}
Settings::Settings( const Settings& settings )
{
	_settings = settings.settings();
}
/**********************************************/
/**********************************************/
void Settings::addSetting( const Key& key, const QVariant& value )
{
	if ( !hasKey(key) )
		_settings.append( {key, value} );
	else
		for ( QPair<Key,QVariant>& pair : _settings )
			if ( pair.first == key )
				pair.second = value;
}
void Settings::set( const QVariant& value, const Key& key )
{
	if ( !hasKey(key) )
		_settings.append( {key, value} );
	else
		for ( QPair<Key,QVariant>& pair : _settings )
			if ( pair.first == key )
				pair.second = value;
}
/**********************************************/
/**********************************************/
QList<Key> Settings::keys() const
{
	QList<Key> keys;
	for ( const QPair<Key,QVariant>& pair : _settings )
		keys.append( pair.first );
	return keys;
}
/**********************************************/
bool Settings::hasKey(const Key& key) const
{
	for ( const QPair<Key,QVariant>& pair : _settings )
		if ( pair.first == key )
			return true;
	return false;
}
/**********************************************/
/**********************************************/
QList<QPair<Key,QVariant>> Settings::settings() const
{
	return _settings;
}
/**********************************************/
QVariant Settings::value( const Key& key ) const
{
	for ( const QPair<Key,QVariant>& pair : _settings )
		if ( pair.first == key )
			return pair.second;
	return QVariant();
}
QString Settings::getString( const Key& key ) const
{
	return value( key ).toString();
}
void Settings::getString( QString& value, const Key& key ) const
{
	value = getString( key );
}
int Settings::getInt( const Key& key ) const
{
	return value( key ).toInt();
}
void Settings::getInt( int& value, const Key& key ) const
{
	bool ok;
	int val = this->value( key ).toInt(&ok);
	if ( ok )
		value = val;
}
/**********************************************/
/**********************************************/
int Settings::DecomposeLine( const QString& line, Settings& settings )
{
	if ( line.startsWith("#") )
	{
		int indent = 0;
		int it;
		int beg_ind = 0;
		for ( it = 0; it < line.length(); ++it )
		{
			if ( line.at(it) == '#' )
				++indent;
			else if ( line.at(it) == ' ' )
				continue;
			else
			{
				beg_ind = it;
				break;
			}
		}
		int end_ind = line.indexOf( ' ', it );
		QString type = line.mid( beg_ind, end_ind-beg_ind ).toLower();
		settings.addSetting( Key::Type, Key::toKeys(type) );
		Settings::DecomposeLine( line.mid(end_ind), settings );
		return indent;
	}
	else
	{
		if ( line.isEmpty() || line.startsWith("\\") )
			return -1;
		QString tmp = line;
		QString key;
		QString value;
		bool search_first_space = false;
		bool search_first_quote_space = false;
		bool search_equal = true;
		for ( int it = 0; it < tmp.size(); ++it )
		{
			QChar c = tmp.at(it);
			if ( search_equal && c == '=' )
			{
				search_equal = false;
				if ( it+1 < tmp.size() && tmp.at(it+1) == '\"' )
				{
					search_first_quote_space = true;
					++it;
				}
				else
					search_first_space = true;
				continue;
			}
			else if ( (search_first_quote_space && c == '\"' && ((it+1 < tmp.size() && tmp.at(it+1) == ' ') || it+1 == tmp.size())) || ( search_first_space && c == ' ' ) )
			{
				search_first_space = false;
				search_first_quote_space = false;
				search_equal = true;
//				qDebug() << key << value;
				Key key2 = Key::toKeys(key);
				if ( key2 != Key::Unknown )
					settings.addSetting( key2, value );
				key.clear();
				value.clear();
				continue;
			}

			if ( search_equal && c != ' ' )
				key += c;
			else if ( search_first_quote_space || search_first_space )
				value += c;
		}
		return 1;
	}
	return -1;
}
/**********************************************/
/**********************************************/
