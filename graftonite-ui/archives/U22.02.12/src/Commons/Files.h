#ifndef FILES_H
#define FILES_H
/*------------------------------*/
class QString;
class QDomDocument;
/*------------------------------*/
/*------------------------------*/
/*------------------------------*/
class Files
{
public:
//	static QString readFile( const QString& path );
	static void readFile( const QString& path, QString& content );
	static QDomDocument readXML( const QString& path );
	static void saveXML( const QString& path, const QDomDocument& document );
	/*------------------------------*/
};
/*------------------------------*/
/*------------------------------*/
/*------------------------------*/
#endif // FILES_H
