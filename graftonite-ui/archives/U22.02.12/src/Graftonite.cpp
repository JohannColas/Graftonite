#include "Graftonite.h"
/*------------------------------*/
#include <QApplication>
#include <QPushButton>
#include <QSplitter>
#include <QStackedWidget>
#include <QVBoxLayout>
/**********************************************/
//#include <QDir>
//#include <QFileSystemModel>
//
#include "Project/ProjectItem.h"
#include "Project/ProjectTree.h"
#include "Project/ProjectModel.h"
#include "Project/ProjectTreeWidget.h"
#include "Project/ProjectTreeItem.h"
//
#include "Welcome/WelcomeFrame.h"
#include "Settings/SettingsFrame.h"
//
#include "Data/DataEditor.h"
#include "Graph/GraphEditor.h"
#include "BasicWidgets/ImageViewer.h"
/**********************************************/
#include "Commons/Key.h"
#include "Commons/App.h"
#include "Commons/Files.h"
#include "Project/Project.h"
/**********************************************/
#include <QDebug>

/**********************************************/
/**********************************************/
/**********************************************/
Graftonite::Graftonite( QWidget* parent ) :
    QWidget(parent)
{

	setWindowTitle( "Graftonite" );
	setWindowIcon( QIcon("icons/Dark Theme/graftonite.svg") );

	qApp->setStyleSheet( qApp->styleSheet()+".SidebarButton{text-align:left;font-weight:bold;icon-size:30px;}" );

	_layout = new QVBoxLayout( this );

	pb_welcome = new QPushButton( "Welcome", this );
	pb_welcome->setProperty( "class", "SidebarButton" );
	pb_welcome->setObjectName( "pb_welcome" );
	pb_welcome->setFlat( true );
	pb_welcome->setIcon( QIcon::fromTheme( "welcome", QIcon("icons/Dark Theme/welcome.svg")) );
	connect( pb_welcome, &QPushButton::released,
	         this, &Graftonite::on_pb_welcome_released );
	_layout->addWidget( pb_welcome );

	pb_project_explorer = new QPushButton( "Project Explorer", this );
	pb_project_explorer->setProperty( "class", "SidebarButton" );
	pb_project_explorer->setObjectName( "pb_project_explorer" );
	pb_project_explorer->setFlat( true );
	pb_project_explorer->setIcon( QIcon::fromTheme( "project_explorer", QIcon("icons/Dark Theme/project_explorer.svg")) );
	_layout->addWidget( pb_project_explorer );

	tree_project = new ProjectTree( this );
	tree_project->setProperty( "class", "ProjectTree" );
	tree_project->setObjectName( "tree_project" );

//	model_project = new ProjectTreeModel( "djfhsfds\ndufsdfd\nfhsdgfujsdf\ndfdsfd", this );

	model_project = new ProjectModel( "#project-exemple/project.gpj", this );

	tree_project->setModel( model_project );

	connect( tree_project, &ProjectTree::pressed,
			 this, &Graftonite::itemChanged );

	tree_project2 = new ProjectTreeWidget( this );
	tree_project2->setProperty( "class", "ProjectTree" );
	tree_project2->setObjectName( "tree_project2" );
	tree_project2->openProject( "#project-exemple/project.gpj" );

	connect( tree_project2, &ProjectTreeWidget::itemPressed,
			 this, &Graftonite::onProjectItemChanged );


	_layout->addWidget( tree_project );
	_layout->addWidget( tree_project2 );

	pb_settings = new QPushButton( "Settings", this );
	pb_settings->setProperty( "class", "SidebarButton" );
	pb_settings->setObjectName( "pb_settings" );
	pb_settings->setFlat( true );
	pb_settings->setIcon( QIcon::fromTheme( "settings", QIcon("icons/Dark Theme/settings.svg")) );
	connect( pb_settings, &QPushButton::released,
	         this, &Graftonite::on_pb_settings_released );
	_layout->addWidget( pb_settings );

	cont_main = new QStackedWidget(this);
	cont_main->show();

	wid_welcome = new WelcomeFrame(this);
	cont_main->addWidget(wid_welcome);
	wid_data = new DataEditor(this);
	cont_main->addWidget(wid_data);
	wid_graph = new GraphEditor(this);
	cont_main->addWidget(wid_graph);
	wid_settings = new SettingsFrame(this);
	cont_main->addWidget(wid_settings);
	wid_image = new ImageViewer(this);
	cont_main->addWidget(wid_image);
	wid_empty = new QWidget(this);
	cont_main->addWidget( wid_empty );


	QWidget* wid_sidebar = new QWidget(this);
	wid_sidebar->setLayout(_layout);



	QVBoxLayout* lay_main = new QVBoxLayout(this);
	splitter = new QSplitter(this);
	splitter->addWidget(wid_sidebar);
	splitter->addWidget(cont_main);
	splitter->setStretchFactor(0, 2);
	splitter->setStretchFactor(1, 4);
	lay_main->addWidget( splitter );

	this->setLayout( lay_main );

	update_projectExplorer();

	showMaximized();

}
/**********************************************/
/**********************************************/
Graftonite::~Graftonite()
{
	delete _layout;
	delete splitter;
	delete pb_welcome;
	delete pb_project_explorer;
	delete pb_settings;
	delete tree_project;
//	delete model_project;
	delete cont_main;
	delete wid_welcome;
	delete wid_settings;
	delete wid_data;
	delete wid_graph;
	delete wid_empty;
}
/**********************************************/
/**********************************************/
void Graftonite::update_projectExplorer()
{
//	tree_project->clear();
//	for ( const QString& path : App::openedProjects() )
//		tree_project->addTopLevelItem( path );
//	tree_project->expandAll();
}
/**********************************************/
/**********************************************/
void Graftonite::on_tree_projectExplorer_itemPressed( ProjectItem* item )
{
	if ( !item ) return;

	Key type = item->type();
	if ( type == Key::Project )
	{
		cont_main->setCurrentWidget( wid_empty );
	}
	else if ( type == Key::Folder )
	{
		cont_main->setCurrentWidget( wid_empty );
	}
	else if ( type == Key::Data )
	{
		cont_main->setCurrentWidget( wid_data );
//		wid_data->setDataPath( tree_project->filePath( item ) );
	}
	else if ( type == Key::Graph )
	{
		cont_main->setCurrentWidget( wid_graph );
//		wid_graph->setGraphPath( tree_project->filePath( item ) );
//		QString prj_path = tree_project->currentProject( item )->path();
//		wid_graph->setAvailableData( Project( prj_path ).availableData() );
	}
	else
		cont_main->setCurrentWidget( wid_empty );
}
/**********************************************/
/**********************************************/
void Graftonite::on_pb_welcome_released()
{
	cont_main->setCurrentWidget( wid_welcome );
}
/**********************************************/
/**********************************************/
void Graftonite::on_pb_settings_released()
{
	cont_main->setCurrentWidget( wid_settings );
}
/**********************************************/
/**********************************************/
void Graftonite::itemChanged( const QModelIndex& index )
{
	ProjectItem* item = model_project->item( index );
	if ( item == nullptr )
		return;
	ProjectItem* project = item;
	while ( project != nullptr && project->type() != Key::Project )
		project = project->parent();

	Key type  = item->type();
	QString path  = item->setting(Key::Path).toString();
	if ( !path.startsWith("/") )
	{
		QString prj_path;
		if ( project != nullptr )
			prj_path = project->setting(Key::Path).toString();
		path = prj_path + (prj_path.endsWith("/")?"":"/") + path;
	}

	if ( type == Key::Project )
	{
		cont_main->setCurrentWidget( wid_empty );
	}
	else if ( type == Key::Folder )
	{
		cont_main->setCurrentWidget( wid_empty );
	}
	else if ( type == Key::Data )
	{
		cont_main->setCurrentWidget( wid_data );
		wid_data->setDataPath( path );
	}
	else if ( type == Key::Graph )
	{
		cont_main->setCurrentWidget( wid_graph );
		wid_graph->setGraphPath( path );
//		QString prj_path = tree_project->currentProject( item )->path();
//		wid_graph->setAvailableData( Project( prj_path ).availableData() );
	}
	else if ( type == Key::Image )
	{
		cont_main->setCurrentWidget( wid_image );
		wid_image->loadFile( path );
	}
	else
		cont_main->setCurrentWidget( wid_empty );
}

void Graftonite::onProjectItemChanged( ProjectTreeItem* item )
{
	if ( item == nullptr )
		return;

	ProjectTreeItem* project_item = tree_project2->projectItem( item );


	Key type  = item->projectType();
	QString path  = item->setting(Key::Path).toString();
	if ( !path.startsWith("/") )
	{
		QString prj_path;
		if ( project_item != nullptr )
			prj_path = project_item->setting(Key::Path).toString();
		path = prj_path + (prj_path.endsWith("/")?"":"/") + path;
	}

	if ( type == Key::Project )
	{
		cont_main->setCurrentWidget( wid_empty );
	}
	else if ( type == Key::Folder )
	{
		cont_main->setCurrentWidget( wid_empty );
	}
	else if ( type == Key::Data )
	{
		cont_main->setCurrentWidget( wid_data );
		wid_data->setDataPath( path );
	}
	else if ( type == Key::Graph )
	{
		cont_main->setCurrentWidget( wid_graph );
		wid_graph->setGraphPath( path );
//		QString prj_path = tree_project->currentProject( item )->path();
//		wid_graph->setAvailableData( Project( prj_path ).availableData() );
	}
	else if ( type == Key::Image )
	{
		cont_main->setCurrentWidget( wid_image );
		wid_image->loadFile( path );
	}
	else
		cont_main->setCurrentWidget( wid_empty );
}
/**********************************************/
/**********************************************/
