#ifndef GRAFTONITE_H
#define GRAFTONITE_H
/**********************************************/
#include <QWidget>
/**********************************************/
class QVBoxLayout;
class QSplitter;
class QStackedWidget;
class QPushButton;
class QTreeWidgetItem;
class ProjectElement;
//
class ProjectItem;
class ProjectTree;
class ProjectModel;
class ProjectTreeWidget;
class ProjectTreeItem;
//
/**********************************************/
class WelcomeFrame;
class SettingsFrame;
class DataEditor;
class GraphEditor;
//
class ImageViewer;
/**********************************************/
/**********************************************/
/**********************************************/
class Graftonite
        : public QWidget
{
	Q_OBJECT
public:
	explicit Graftonite( QWidget* parent = nullptr );
	~Graftonite();
	/**********************************************/
	void update_projectExplorer();
	/**********************************************/
private slots:
	void on_tree_projectExplorer_itemPressed( ProjectItem* item );
	/**********************************************/
	void on_pb_welcome_released();

	void on_pb_settings_released();
	void itemChanged( const QModelIndex& index );
	void onProjectItemChanged( ProjectTreeItem* item );

private:
	QList<ProjectElement*> _projects;
	QStringList _projectPaths;

	QVBoxLayout* _layout = nullptr;
	QSplitter* splitter = nullptr;
	// SideBar button
	QPushButton* pb_welcome = nullptr;
	QPushButton* pb_project_explorer = nullptr;
	QPushButton* pb_settings = nullptr;
	//
	ProjectTree* tree_project = nullptr;
	ProjectTreeWidget* tree_project2 = nullptr;
	ProjectModel* model_project = nullptr;
	//
	QStackedWidget* cont_main = nullptr;
	WelcomeFrame* wid_welcome = nullptr;
	SettingsFrame* wid_settings = nullptr;
	DataEditor* wid_data = nullptr;
	GraphEditor* wid_graph = nullptr;
	ImageViewer* wid_image = nullptr;
	QWidget* wid_empty = nullptr;


	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // GRAFTONITE_H
