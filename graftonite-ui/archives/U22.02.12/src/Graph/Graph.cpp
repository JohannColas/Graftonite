#include "Graph.h"
/**********************************************/
#include "Layer.h"
#include "Axis.h"
#include "Curve.h"
#include "Commons/XML.h"
#include "Commons/Key.h"
#include "Commons/Files.h"

#include <QDir>
#include <QList>
/**********************************************/
/**********************************************/
Graph::Graph( const QString& name ) : Element(0)
{
	setID( name );
	init();
}

void Graph::clear()
{
	_sets.clear();
	while ( !_elements.isEmpty() )
		delete _elements.takeLast();
}
/**********************************************/
/**********************************************/
void Graph::init()
{
//	setPath( QDir::currentPath() + "/graph2.gph"  );
//	_sets.clear();
	//
	set( XML::GraphElement(_path) );
}
/**********************************************/
/**********************************************/
void Graph::set( const QDomElement& sets )
{
//	Element::set( sets );
//	XML::setInt( sets, Keys::Width, _width );
//	XML::setInt( sets, Keys::Height, _height );
//	_svgPath = _path;
//	setSVGPath( _svgPath.remove(".gph")+".svg" );
//	for ( QDomElement el : XML::ChildElements(sets) )
//	{
//		QString elType = el.tagName();
//		Element* gelm = 0;
//		if ( elType == Keys::Layer )
//			gelm = new Layer(this);
//		else if ( elType == Keys::Axis )
//			gelm = new Axis( this );
//		else if ( elType == Keys::Curve )
//			gelm = new Curve(this);
//		if ( !gelm )
//			continue;
//		gelm->set( el );
//		gelm->setGraph( this );
//		addElement( gelm );
//	}
//	emit changed();
}
/**********************************************/
/**********************************************/
void Graph::saveXML()
{
//	QDomDocument doc;
//	QDomElement el = doc.createElement( Keys::Graph );
//	el.setAttribute( Keys::ID, str_id );
//	el.setAttribute( Keys::Width, _width );
//	el.setAttribute( Keys::Height, _height );
//	for ( Element* gelm : elements() )
//		el.appendChild( gelm->save() );
//	doc.insertBefore( el, doc.doctype() );
//	Files::saveXML( _path, doc );
}
/**********************************************/
/**********************************************/
QSize Graph::size() const
{
	return { _width, _height};
}
/**********************************************/
/**********************************************/
void Graph::setSize( const QSize& size )
{
	_width = size.width();
	_height = size.height();
	emit changed();
}
/**********************************************/
/**********************************************/
QList<Element*> Graph::elements() const
{
	return _elements;
}
/**********************************************/
/**********************************************/
Element* Graph::element( const QString& id ) const
{
	for ( Element* el : elements() )
		if ( el->ID() == id )
			return el;
	return 0;
}
/**********************************************/
/**********************************************/
void Graph::addElement( Element* element )
{
	if ( element )
		_elements.append( element );
	emit changed();
}
/**********************************************/
/**********************************************/
Element* Graph::elementAt( int index ) const
{
	if ( index < 0 || index >= _elements.size() )
		return 0;
	return _elements.at( index );
}
/**********************************************/
/**********************************************/
Axis* Graph::axisAt( int index ) const
{
//	Element* el = elementAt( index );
//	if ( el )
//		if ( el->elementType() == Keys::Axis )
//			return static_cast<Axis*>(el);
	return 0;
}
/**********************************************/
/**********************************************/
Axis* Graph::axis( const QString& id ) const
{
//	for ( Element* el : elements() )
//		if ( el->elementType() == Keys::Axis )
//			if ( el->ID() == id )
//				return static_cast<Axis*>(el);
	return 0;
}
/**********************************************/
/**********************************************/
QList<Axis*> Graph::axes() const
{
	QList<Axis*> allaxes;
//	for ( Element* el : elements() )
//		if ( el->elementType() == Keys::Axis )
//			allaxes.append( static_cast<Axis*>(el) );
	return allaxes;
}
/**********************************************/
/**********************************************/
Curve* Graph::curveAt( int index ) const
{
//	Element* el = elementAt( index );
//	if ( el )
//		if ( el->elementType() == Keys::Curve )
//			return static_cast<Curve*>(el);
	return 0;
}
/**********************************************/
/**********************************************/
Curve* Graph::curve( const QString& id ) const
{
//	for ( Element* el : elements() )
//		if ( el->elementType() == Keys::Curve )
//			if ( el->ID() == id )
//				return static_cast<Curve*>(el);
	return 0;
}
/**********************************************/
/**********************************************/
QList<Curve*> Graph::curves() const
{
	QList<Curve*> allcurves;
//	for ( Element* el : elements() )
//		if ( el->elementType() == Keys::Curve )
//			allcurves.append( static_cast<Curve*>(el) );
	return allcurves;
}
/**********************************************/
/**********************************************/
Layer* Graph::layerAt( int index ) const
{
//	Element* el = elementAt( index );
//	if ( el )
//		if ( el->elementType() == Keys::Layer )
//			return static_cast<Layer*>(el);
	return 0;
}
/**********************************************/
/**********************************************/
Layer* Graph::layer( const QString& id ) const
{
//	for ( Element* el : elements() )
//		if ( el->elementType() == Keys::Layer )
//			if ( el->ID() == id )
//				return static_cast<Layer*>(el);
	return 0;
}
/**********************************************/
/**********************************************/
QList<Layer*> Graph::layers() const
{
	QList<Layer*> alllayers;
//	for ( Element* el : elements() )
//		if ( el->elementType() == Keys::Layer )
//			alllayers.append( static_cast<Layer*>(el) );
	return alllayers;
}
/**********************************************/
/**********************************************/
void Graph::removeElement( const QString& id )
{
	Element* el = this->element( id );
	if ( el )
	{
		_elements.removeOne( el );
		emit changed();
	}
}
/**********************************************/
/**********************************************/
QString Graph::draw()
{
	QString content;
	content += "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
	content += "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.0\" width=\""+QString::number(_width)+"\" height=\""+QString::number(_height)+"\" font-family=\"Roboto-Regular,Roboto,Arial, Helvetica, sans-serif\">\n";

	for ( Element* el : _elements )
	{
		content += el->draw();
	}

	content += "</svg>";
	return content;
}
/**********************************************/
/**********************************************/
