#ifndef CURVE_H
#define CURVE_H
/**********************************************/
#include <QVector>
/**********************************************/
#include "Element.h"
/**********************************************/
class Graph;
class Axis;
/**********************************************/
namespace CurveType {
	static inline QString Unknown = "Unknown";
	static inline QString XY = "XY curve";
	static inline QString XYY = "XYY curve";
	static inline QString XYZ = "XYZ curve";
}
namespace CurveSimplify {
	static inline QString None = "None";
}
/**********************************************/
/**********************************************/
/**********************************************/
class Curve
		: public Element
{
private:
	static inline int _nbCurves = 0;
	int _id = 0;
	QString _type = CurveType::XY;
	QStringList _axes;
	QString _axis1;
	QString _axis2;
	QString _axis3;
	// Data
	QString _dataPath;
	QStringList _dataIndexes;
	QString _index1;
	QString _index2;
	QString _index3;
	QVector<QVector<double>> _data;
	// Styles
	QString _lineStyle;
	QString _scatterStyle;
	QString _areaStyle;
	// simplify
	QString _simplify = CurveSimplify::None;
	double _simplifyOpt = 0;
	/**********************************************/
public:
	//
	QString type() const { return _type; }
	void setType( const QString& type );
	QStringList axes() { return _axes; }
	void setAxes( const QStringList axes );
	QString axis1() const { return _axis1; }
	void setAxis1( const QString& axis1 );
	QString axis2() const { return _axis2; }
	void setAxis2( const QString& axis2 );
	QString axis3() const { return _axis3; }
	void setAxis3( const QString& axis3 );
	// Data
	QString dataPath() const { return _dataPath; }
	void setDataPath( const QString& path );
	QStringList dataIndexes() const { return _dataIndexes; }
	void setDataIndexes( const QStringList& indexes );
	QString index1() { return _index1; }
	void setIndex1( const QString& index1 );
	QString index2() { return _index2; }
	void setIndex2( const QString& index2 );
	QString index3() { return _index3; }
	void setIndex3( const QString& index3 );
	QVector<QVector<double>> data() const { return _data; }
	void setData( const QVector<QVector<double>>& data );
	// Styles
	QString lineStyle() const { return _lineStyle; }
	void setLineStyle( const QString& style );
	QString scatterStyle() const { return _scatterStyle; }
	void setScatterStyle( const QString& style );
	QString areaStyle() const { return _areaStyle; }
	void setAreaStyle( const QString& style );
	// simplify
	QString simplify() const { return _simplify; }
	void setSimplify( const QString& simplify );
	double simplifyOption() const { return _simplifyOpt; }
	void setSimplifyOption( double option );
	/**********************************************/
public:
	Curve( Graph* graph );
	void init();
	void set( const QDomElement& sets ) override;
	QDomElement save() override;
	QString draw() override;
	/**********************************************/
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // CURVE_H
