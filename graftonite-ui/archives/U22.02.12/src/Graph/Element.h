#ifndef ELEMENT_H
#define ELEMENT_H
/**********************************************/
#include <QObject>
#include <QList>
/**********************************************/
#include <QDomElement>
class Graph;
/**********************************************/
/**********************************************/
class Element
		: public QObject
{
	Q_OBJECT
protected:
	QString str_id;
	QString _elType = "unknown";
	Graph* _graph = nullptr;
	QDomElement _sets;
	/**********************************************/
public:
	QString ID() const { return str_id; }
	void setID( const QString& id );
	QString elementType() const { return _elType; }
	void setElementType( const QString& elType );
	/**********************************************/
public:
	Element( Graph* graph );
	/**********************************************/
	virtual void set( const QDomElement& sets );
	virtual void setGraph( Graph* graph );
	virtual QDomElement settings();
	virtual QDomElement save();
	virtual QString draw();
	/**********************************************/
signals:
	void changed();
};
/**********************************************/
/**********************************************/
#endif // ELEMENT_H
