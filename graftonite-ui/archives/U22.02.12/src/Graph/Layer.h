#ifndef LAYER_H
#define LAYER_H
/**********************************************/
#include "Element.h"
/**********************************************/
/**********************************************/
/**********************************************/
class Layer
		: public Element
{
	static inline int _nbLayers = 0;
	double _x = 150;
	double _y = 150;
	double _width = 1300;
	double _height = 700;
	/**********************************************/
public:
	double x() { return _x; };
	void setX( double x );
	double y() { return _y; };
	void setY( double y );
	double width() { return _width; };
	void setWidth( double width );
	double height() { return _height; };
	void setHeight( double height );
	/**********************************************/
public:
	Layer( Graph* graph );
	void set( const QDomElement& sets ) override;
	QDomElement save() override;
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LAYER_H
