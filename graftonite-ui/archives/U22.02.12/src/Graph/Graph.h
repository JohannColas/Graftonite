#ifndef GRAPH_H
#define GRAPH_H
/**********************************************/
#include "Element.h"
/**********************************************/
class Axis;
class Curve;
class Layer;
class QSize;
/**********************************************/
/**********************************************/
/**********************************************/
class Graph
		: public Element
{
	Q_OBJECT
private:
	QString _path;
	QDomDocument _content;
	QString _svgPath;
	QDomDocument _svgContent;
//	XML _settings;
	int _width = 1600;
	int _height = 1000;
	QList<Element*> _elements;
public:
	QString path() const { return _path; }
	void setPath( const QString& path ) { _path = path; }
	QString svgPath() const { return _svgPath; }
	void setSVGPath( const QString& path ) { _svgPath = path; }
	int width() const { return _width; }
	int height() const { return _height; }
	QSize size() const;
	void setWidth( int width ) { _width = width; emit changed(); }
	void setHeight( int height ) { _height = height; emit changed(); }
	void setSize( int width, int height ) { _width = width; _height = height; emit changed(); }
	void setSize( const QSize& size );
	/**********************************************/
public:
	Graph( const QString& name );
	/**********************************************/
	void clear();
	void init();
	void set( const QDomElement& sets ) override;
	void saveXML();
	QString draw() override;
	/**********************************************/
	QList<Element*> elements() const;
	Element* element( const QString& id ) const;
	Element* elementAt( int index ) const;
	void addElement( Element* element = nullptr );
	Axis* axisAt( int index ) const;
	Axis* axis( const QString& id ) const;
	QList<Axis*> axes() const;
	Curve* curveAt( int index ) const;
	Curve* curve( const QString& id ) const;
	QList<Curve*> curves() const;
	Layer* layerAt( int index ) const;
	Layer* layer( const QString& id ) const;
	QList<Layer*> layers() const;
	void removeElement( const QString& id );
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // GRAPH_H
