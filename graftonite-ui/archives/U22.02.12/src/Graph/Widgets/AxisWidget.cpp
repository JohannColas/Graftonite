#include "AxisWidget.h"
#include "ui_AxisWidget.h"
/**********************************************/
#include "Graph/Graph.h"
#include "Graph/Layer.h"
#include "Graph/Axis.h"
/**********************************************/
/**********************************************/
AxisWidget::AxisWidget( QWidget* parent )
	: QWidget( parent ),
	  ui( new Ui::AxisWidget )
{
	ui->setupUi( this );
	ui->cb_type->blockSignals(true);
	ui->cb_type->addItems( {AxisType::Xaxis, AxisType::Yaxis, AxisType::Zaxis} );
	ui->cb_type->blockSignals(false);
	ui->cb_scale->blockSignals(true);
	ui->cb_scale->addItems( {AxisScale::Linear, AxisScale::Log10, AxisScale::Log, AxisScale::LogP, AxisScale::Reciprocal, AxisScale::OffsetReciprocal} );
	ui->cb_scale->blockSignals(false);
}
/**********************************************/
/**********************************************/
AxisWidget::~AxisWidget()
{
	delete ui;
}
/**********************************************/
/**********************************************/
void AxisWidget::setGraph( Graph* graph )
{
	_graph = graph;
	if ( !_graph ) return;
}
/**********************************************/
/**********************************************/
void AxisWidget::setAxis( Axis* axis )
{
	_axis = axis;
	if ( !_axis ) return;
	ui->le_name->setText( _axis->ID() );
	ui->cb_type->setCurrentText( _axis->type() );
	ui->cb_layer->blockSignals( true );
	ui->cb_layer->clear();
	ui->cb_layer->addItem( QString() );
	if ( _graph )
		for ( Layer* layer : _graph->layers() )
			ui->cb_layer->addItem( layer->ID() );
	ui->cb_layer->blockSignals( false );
	ui->cb_pos->setCurrentText( _axis->position() );
	ui->le_min->setText( _axis->toString( _axis->min() ) );
	ui->le_max->setText( _axis->toString( _axis->max() ) );
	ui->cb_scale->setCurrentText( _axis->scale() );
	ui->le_scaleOpt->setText( _axis->toString( _axis->scaleOption() ) );
	ui->cb_ticksPos->setCurrentText( _axis->ticksPosition() );
	ui->le_ticksSize->setText( _axis->toString( _axis->ticksSize() ) );
	ui->le_ticksInterval->setText( _axis->toString( _axis->ticksInterval() ) );
	ui->le_minoticksSize->setText( _axis->toString( _axis->minorTicksSize() ) );
	ui->le_minorticksInterval->setText( _axis->toString( _axis->minorTicksInterval() ) );
	ui->cb_labelsPos->setCurrentText( _axis->labelsPosition() );
	ui->cb_titlePos->setCurrentText( _axis->titlePosition() );
}
/**********************************************/
/**********************************************/
void AxisWidget::on_le_name_editingFinished()
{
	if ( !_axis ) return;
	_axis->setID( ui->le_name->text() );
	emit changed();
}
/**********************************************/
/**********************************************/
void AxisWidget::on_cb_type_currentIndexChanged( const QString& type )
{
	if ( !_axis ) return;
	if ( _axis->type() != type )
	{
		_axis->setType( type );
		emit changed();
	}
	ui->cb_pos->blockSignals(true);
	ui->cb_pos->clear();
	ui->cb_ticksPos->blockSignals(true);
	ui->cb_ticksPos->clear();
	ui->cb_labelsPos->blockSignals(true);
	ui->cb_labelsPos->clear();
	ui->cb_titlePos->blockSignals(true);
	ui->cb_titlePos->clear();
	if ( type == AxisType::Xaxis )
	{
		ui->cb_pos->addItems( {AxisPosition::None, AxisPosition::Top, AxisPosition::Bottom, AxisPosition::Center} );
		ui->cb_ticksPos->addItems( {AxisPosition::None, AxisPosition::Outside, AxisPosition::Inside, AxisPosition::Center, AxisPosition::Top, AxisPosition::Bottom} );
		ui->cb_labelsPos->addItems( {AxisPosition::None, AxisPosition::Outside, AxisPosition::Inside, AxisPosition::Top, AxisPosition::Bottom} );
		ui->cb_titlePos->addItems( {AxisPosition::None, AxisPosition::Outside, AxisPosition::Inside, AxisPosition::Top, AxisPosition::Bottom} );
	}
	else if ( type == AxisType::Yaxis )
	{
		ui->cb_pos->addItems( {AxisPosition::None, AxisPosition::Left, AxisPosition::Right, AxisPosition::Center} );
		ui->cb_ticksPos->addItems( {AxisPosition::None, AxisPosition::Outside, AxisPosition::Inside, AxisPosition::Center, AxisPosition::Left, AxisPosition::Right} );
		ui->cb_labelsPos->addItems( {AxisPosition::None, AxisPosition::Outside, AxisPosition::Inside, AxisPosition::Left, AxisPosition::Right} );
		ui->cb_titlePos->addItems( {AxisPosition::None, AxisPosition::Outside, AxisPosition::Inside, AxisPosition::Left, AxisPosition::Right} );
	}
	else if ( type == AxisType::Zaxis )
	{
		//		ui->cb_pos->addItems( {AxisPosition::None, AxisPosition::Top, AxisPosition::Bottom, AxisPosition::Center} );
	}
	ui->cb_pos->blockSignals(false);
	ui->cb_ticksPos->blockSignals(false);
	ui->cb_labelsPos->blockSignals(false);
	ui->cb_titlePos->blockSignals(false);
	ui->cb_pos->setCurrentText( _axis->position() );
	ui->cb_ticksPos->setCurrentText( _axis->ticksPosition() );
	ui->cb_labelsPos->setCurrentText( _axis->labelsPosition() );
	ui->cb_titlePos->setCurrentText( _axis->titlePosition() );
}
/**********************************************/
/**********************************************/
void AxisWidget::on_cb_layer_currentIndexChanged( const QString& layer )
{
	if ( !_axis ) return;
	_axis->setLayer( layer );
	emit changed();
}
/**********************************************/
/**********************************************/
void AxisWidget::on_cb_pos_currentIndexChanged( const QString& pos )
{
	if ( !_axis ) return;
	if ( _axis->position() != pos )
	{
		_axis->setPosition( pos );
		emit changed();
	}
}
/**********************************************/
/**********************************************/
void AxisWidget::on_le_min_editingFinished()
{
	if ( !_axis ) return;
	double min = ui->le_min->text().toDouble();
	if ( _axis->min() != min )
	{
		_axis->setMin( min );
		emit changed();
	}
}
/**********************************************/
/**********************************************/
void AxisWidget::on_le_max_editingFinished()
{
	if ( !_axis ) return;
	double max = ui->le_max->text().toDouble();
	if ( _axis->max() != max )
	{
		_axis->setMax( max );
		emit changed();
	}
}
/**********************************************/
/**********************************************/
void AxisWidget::on_cb_scale_currentIndexChanged( const QString& scale )
{
	if ( !_axis ) return;
	if ( _axis->scale() != scale )
	{
		_axis->setScale( scale );
		emit changed();
	}
	if ( scale == AxisScale::LogP || scale == AxisScale::OffsetReciprocal )
		ui->wid_scaleOpt->show();
	else
		ui->wid_scaleOpt->hide();
}
/**********************************************/
/**********************************************/
void AxisWidget::on_le_scaleOpt_editingFinished()
{
	if ( !_axis ) return;
	double scaleOpt = ui->le_scaleOpt->text().toDouble();
	if ( _axis->scaleOption() != scaleOpt )
	{
		_axis->setScaleOption( scaleOpt );
		emit changed();
	}
}
/**********************************************/
/**********************************************/
void AxisWidget::on_cb_ticksPos_currentIndexChanged( const QString& ticksPos )
{
	if ( !_axis ) return;
	if ( _axis->ticksPosition() != ticksPos )
	{
		_axis->setTicksPosition( ticksPos );
		emit changed();
	}
}
/**********************************************/
/**********************************************/
void AxisWidget::on_le_ticksSize_editingFinished()
{
	if ( !_axis ) return;
	double ticksSize = ui->le_ticksSize->text().toDouble();
	if ( _axis->ticksSize() != ticksSize )
	{
		_axis->setTickSize( ticksSize );
		emit changed();
	}
}
/**********************************************/
/**********************************************/
void AxisWidget::on_le_ticksInterval_editingFinished()
{
	if ( !_axis ) return;
	double ticksInterval = ui->le_ticksInterval->text().toDouble();
	if ( _axis->ticksInterval() != ticksInterval )
	{
		_axis->setTickInterval( ticksInterval );
		emit changed();
	}
}
/**********************************************/
/**********************************************/
void AxisWidget::on_le_minoticksSize_editingFinished()
{
	if ( !_axis ) return;
	double minorticksSize = ui->le_minoticksSize->text().toDouble();
	if ( _axis->minorTicksSize() != minorticksSize )
	{
		_axis->setMinorTickSize( minorticksSize );
		emit changed();
	}
}
/**********************************************/
/**********************************************/
void AxisWidget::on_le_minorticksInterval_editingFinished()
{
	if ( !_axis ) return;
	double minorticksInterval = ui->le_minorticksInterval->text().toDouble();
	if ( _axis->minorTicksInterval() != minorticksInterval )
	{
		_axis->setMinorTickInterval( minorticksInterval );
		emit changed();
	}
}
/**********************************************/
/**********************************************/
void AxisWidget::on_cb_labelsPos_currentIndexChanged( const QString& labelsPos )
{
	if ( !_axis ) return;
	if ( _axis->labelsPosition() != labelsPos )
	{
		_axis->setLabelsPosition( labelsPos );
		emit changed();
	}
}
/**********************************************/
/**********************************************/
void AxisWidget::on_cb_titlePos_currentIndexChanged( const QString& titlePos )
{
	if ( !_axis ) return;
	if ( _axis->titlePosition() != titlePos )
	{
		_axis->setTitlePosition( titlePos );
		emit changed();
	}
}
/**********************************************/
/**********************************************/
