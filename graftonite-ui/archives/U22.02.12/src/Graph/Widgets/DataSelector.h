#ifndef DATASELECTOR_H
#define DATASELECTOR_H
/**********************************************/
#include "BasicWidgets/Popup.h"
/**********************************************/
class ProjectTree;
class ComboBox;
class Curve;
class QTreeWidgetItem;
/**********************************************/
/**********************************************/
/**********************************************/
class DataSelector
		: public Popup
{
	Q_OBJECT
	ProjectTree* tree = 0;
	ComboBox* cb = 0;
	Curve* _curve = 0;
	/**********************************************/
public:
	explicit DataSelector( QWidget* parent = nullptr );
	/**********************************************/
	void setCurve( Curve* curve );
	/**********************************************/
	bool childIsInactive() override;
	/**********************************************/
protected slots:
	void update_available_data();
	void setDataPath( QTreeWidgetItem* item, int column );
	/**********************************************/
	void checkHiddable();
	/**********************************************/
	void enterEvent( QEnterEvent* event ) override;
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // DATASELECTOR_H
