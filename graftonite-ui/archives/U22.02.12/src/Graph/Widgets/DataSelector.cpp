#include "DataSelector.h"
/**********************************************/
#include <QGridLayout>
#include <QGridLayout>
#include <QFormLayout>
#include <QLabel>
#include "Commons/App.h"
#include "Project/ProjectTree.h"
#include "Project/ProjectItem.h"
#include "BasicWidgets/ComboBox.h"
/**********************************************/
#include <QDebug>
#include <QLineEdit>
#include <QHeaderView>

#include "Graph/Curve.h"
/**********************************************/
/**********************************************/
DataSelector::DataSelector( QWidget* parent )
	: Popup( parent )
{
	QGridLayout* layout = new QGridLayout;
	QLabel* label = new QLabel( "Data Selector" );
	layout->addWidget( label, 0, 0 );
	tree = new ProjectTree;
	tree->header()->hide();
	tree->hideColumn(1);
	tree->hideColumn(2);
//	tree->showOnlyData();
//	tree->clear();
//	for ( QString path : App::openedProjects() )
//	{
//		tree->addTopLevelItem( path );
//	}
	tree->expandAll();
	layout->addWidget( tree, 1, 0 );

//	connect( tree, &QTreeWidget::itemPressed,
//			 this, &DataSelector::setDataPath );

	//
	QFormLayout* formLayout = new QFormLayout;
	formLayout->setLabelAlignment( Qt::AlignRight | Qt::AlignTop );
	formLayout->setFieldGrowthPolicy( QFormLayout::FieldsStayAtSizeHint );
	cb = new ComboBox( {"Column", "Row"} );

	connect( cb, &ComboBox::popupLeaved,
			 this, &DataSelector::checkHiddable );

//	cb2 = new QComboBox;
//	cb2->addItems( {"Column", "Row"} );
	formLayout->addRow( "Data selection mode:", cb );
	formLayout->addRow( "Row selection:", new QLineEdit );
//	data1 = new QComboBox;
//	data1->addItems( {"1:Contrainte,...", "2:Déformation,...", "3:Température,..."} );
//	formLayout->addRow( "Data 1:", data1 );
//	formLayout->addRow( "Error Data 1:", new QComboBox );
//	formLayout->addRow( "Data 2:", new QComboBox );
//	formLayout->addRow( "Error Data 2:", new QComboBox );

	layout->addLayout( formLayout, 1, 1 );
	//

	setLayout( layout );
}
/**********************************************/
/**********************************************/
void DataSelector::setCurve( Curve* curve )
{
	_curve = curve;
}
/**********************************************/
/**********************************************/
bool DataSelector::childIsInactive()
{
	return (!cb->popupActivated());
}
/**********************************************/
/**********************************************/
void DataSelector::update_available_data()
{
	//	tree->addTopLevelItem(  );
}
/**********************************************/
/**********************************************/
void DataSelector::setDataPath( QTreeWidgetItem* item, int /*column*/ )
{
	if ( !_curve ) return;
//	ProjectItem* prj_item = static_cast<ProjectItem*>(item);
//	_curve->setDataPath( prj_item->path() );
}
/**********************************************/
/**********************************************/
void DataSelector::checkHiddable()
{
	QPoint pos = QCursor::pos();
	if ( !geometry().contains(pos) )
		setVisible(false);
	if ( !isVisible() )
		emit closed();
}
/**********************************************/
/**********************************************/
void DataSelector::enterEvent( QEnterEvent* event )
{
	Popup::enterEvent( event );
	update_available_data();
}
/**********************************************/
/**********************************************/
