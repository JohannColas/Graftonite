#ifndef LAYERWIDGET_H
#define LAYERWIDGET_H
/**********************************************/
#include <QWidget>
/**********************************************/
/**********************************************/
class Layer;
/**********************************************/
/**********************************************/
namespace Ui {
	class LayerWidget;
}
/**********************************************/
/**********************************************/
/**********************************************/
class LayerWidget : public QWidget
{
	Q_OBJECT
public:
	explicit LayerWidget(QWidget *parent = nullptr);
	~LayerWidget();
	/**********************************************/
	void setLayer( Layer* layer = nullptr );
	/**********************************************/
private slots:
	void on_le_name_editingFinished();
	void on_le_x_editingFinished();
	void on_le_y_editingFinished();
	void on_le_width_editingFinished();
	void on_le_height_editingFinished();
	/**********************************************/
private:
	Ui::LayerWidget *ui;
	Layer* _layer = 0;
	/**********************************************/
signals:
	void changed();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LAYERWIDGET_H
