#ifndef GRAPHWIDGET_H
#define GRAPHWIDGET_H
/**********************************************/
#include <QWidget>
/**********************************************/
class Graph;
/**********************************************/
/**********************************************/
namespace Ui {
	class GraphWidget;
}
/**********************************************/
/**********************************************/
/**********************************************/
class GraphWidget
		: public QWidget
{
	Q_OBJECT
public:
	explicit GraphWidget( QWidget* parent = nullptr );
	~GraphWidget();
	/**********************************************/
	void setGraph( Graph* graph = nullptr );
	/**********************************************/
private slots:
	void on_sp_width_valueChanged( int width );
	void on_sp_height_valueChanged( int height );
private:
	Ui::GraphWidget* ui;
	Graph* _graph;
	/**********************************************/
signals:
	void changed();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // GRAPHWIDGET_H
