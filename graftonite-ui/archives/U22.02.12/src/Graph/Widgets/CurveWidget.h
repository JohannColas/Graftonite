#ifndef CURVEWIDGET_H
#define CURVEWIDGET_H
/**********************************************/
#include <QWidget>
/**********************************************/
class Curve;
class Graph;
class DataSelector;
/**********************************************/
/**********************************************/
namespace Ui {
	class CurveWidget;
}
/**********************************************/
/**********************************************/
/**********************************************/
class CurveWidget
		: public QWidget
{
	Q_OBJECT
public:
	explicit CurveWidget( QWidget* parent = nullptr );
	~CurveWidget();
	/**********************************************/
	void setGraph( Graph* graph = nullptr );
	void setAvailableData( const QStringList& availableData );
	void setCurve( Curve* curve = nullptr );
	/**********************************************/
private slots:
	void on_cb_type_currentIndexChanged( const QString& type );
	void update_cb_axes();
	/**********************************************/
	void on_cb_axis1_currentIndexChanged( const QString& axis1 );
	void on_cb_axis2_currentIndexChanged( const QString& axis2 );
	void on_cb_axis3_currentIndexChanged( const QString& axis3 );
	/**********************************************/
	void on_cb_dataPath_currentIndexChanged( const QString& arg1 );
	void on_le_data1_editingFinished();
	void on_le_data2_editingFinished();
	void on_le_data3_editingFinished();
	/**********************************************/
private:
	Ui::CurveWidget* ui;
	Graph* _graph = 0;
	Curve* _curve = 0;
	DataSelector* _dataSelector = 0;
	/**********************************************/
signals:
	void changed();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // CURVEWIDGET_H
