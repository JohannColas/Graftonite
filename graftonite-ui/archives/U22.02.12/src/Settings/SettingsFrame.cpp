#include "SettingsFrame.h"

#include <QDebug>
#include "SettingsWidget.h"
/**********************************************/
/**********************************************/
/* */
SettingsFrame::~SettingsFrame() {
}
/**********************************************/
/**********************************************/
/* */
SettingsFrame::SettingsFrame(QWidget *parent)
	: QWidget( parent )
{

	ThemesColorsSettings* wid_theme_colors = new ThemesColorsSettings;
	QWidget* wid1 = new QWidget;
	wid1->setStyleSheet( "background:#BB0044;" );
	wid1->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Expanding );
	wid1->setVisible( false );
	QWidget* wid2 = new QWidget;
	wid2->setStyleSheet( "background:#BB8844;" );
	wid2->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Expanding );
	wid2->setVisible( false );

	wid_container->addSection("Apparence");
	wid_container->addItem("Theme & Colors", wid_theme_colors);
	wid_container->addItem("Settings 0", wid2);
	wid_container->addSection("Section 2");
	wid_container->addItem("Settings 1", wid1);
	wid_container->addItem("Settings 2");
	wid_container->addItem("Settings 3");
	wid_container->addItem("Settings 4");
	SettingsWidget* wid_sets = new SettingsWidget;
	connect( wid_sets, &SettingsWidget::settingChanged,
			 this, &SettingsFrame::onSettingChanged );
	wid_sets->addSetting( Key::General, "General", "", SettingsItem::Section );
	wid_sets->addSetting( Key::Width, "Width", 12, SettingsItem::SpinBox );
	wid_sets->addSetting( Key::Ticks, "Ticks", "", SettingsItem::Section );
	wid_sets->addSetting( Key::Min, "Min", "22", SettingsItem::LineEdit );
	wid_sets->addSetting( Key::Hide, "Hide", false, SettingsItem::CheckBox);
	wid_container->addItem("Settings 5", wid_sets);
	wid_container->addSection("Others");
	wid_container->addItem("About");
	wid_container->changeCurrentWidget( 0 );


	lay_main->addWidget( wid_container );
	setLayout( lay_main );
	hide();
}
/**********************************************/
/**********************************************/
/* */
void SettingsFrame::showThemeColors()
{
//	removeCurrentSettings();
//	themesColors = new ThemesColorsSettings;
//	ui->widget->layout()->addWidget( themesColors );
//	currentWidget = SettingsWidget::THEMESCOLORS;
}
/**********************************************/
/**********************************************/
/* */
void SettingsFrame::removeCurrentSettings()
{
//	if ( currentWidget == SettingsWidget::NONE )
//	{
//	}
//	else if ( currentWidget == SettingsWidget::THEMESCOLORS )
//	{
//		ui->widget->layout()->removeWidget( themesColors );
//		delete themesColors;
//		themesColors = nullptr;
	//	}
}

void SettingsFrame::onSettingChanged( const Key& setting, const QVariant& value )
{
	qDebug() << setting.toString() << value;
}
/**********************************************/
/**********************************************/
/* */
//void SettingsFrame::updateIcons( Icons* icons ) {
//Q_UNUSED(icons)
//}
/**********************************************/
/**********************************************/
/* */
//void SettingsFrame::updateLang( Lang* lang ) {
//Q_UNUSED(lang)
//}
/**********************************************/
/**********************************************/
