#include "SettingsWidget.h"
/**********************************************/
#include <QFormLayout>
#include <QLabel>
#include <QCheckBox>
#include <QComboBox>
#include <QDoubleSpinBox>
#include <QLineEdit>
#include <QPushButton>
#include "../Commons/Settings.h"
/**********************************************/
/**********************************************/
/**********************************************/
/**********************************************/
SettingsWidget::~SettingsWidget()
{
	qDeleteAll( _items );
}
SettingsWidget::SettingsWidget( QWidget* parent )
	: QWidget(parent)
{
	this->setProperty( "class", "SettingsWidget" );
	lay_main = new QFormLayout;
	lay_main->setLabelAlignment( Qt::AlignRight );
	lay_main->setFieldGrowthPolicy( QFormLayout::FieldsStayAtSizeHint );
	setLayout( lay_main );
}
/**********************************************/
/**********************************************/
void SettingsWidget::addSetting( const Key& setting, const QString& title, const QVariant& value, const SettingsItem::Type& type )
{
	SettingsItem* item = new SettingsItem( setting, title, value, type );
	connect( item, &SettingsItem::settingChanged,
			 this, &SettingsWidget::settingChanged );
	_items.append( item );
	if ( type == SettingsItem::Section )
		lay_main->addRow( item->title() );
	else
	{
		lay_main->addRow( item->title(), item->modifier() );
	}
}
void SettingsWidget::addSettings( const Settings& settings )
{
//	SettingsItem* item = new SettingsItem( setting, title, value, type );
//	connect( item, &SettingsItem::settingChanged,
//			 this, &SettingsWidget::settingChanged );
//	_items.append( item );
//	if ( type == SettingsItem::Section )
//		lay_main->addRow( item->title() );
//	else
//	{
//		lay_main->addRow( item->title(), item->modifier() );
//	}
}
/**********************************************/
/**********************************************/
void SettingsWidget::updateLayout()
{
//	for ( SettingsItem item : _items )
//	{

//	}
}
/**********************************************/
/**********************************************/
SettingsItem::~SettingsItem()
{
	delete lb_title;
	if ( wid_modifier != nullptr )
		delete wid_modifier;
}
SettingsItem::SettingsItem( const Key& setting, const QString& title, const QVariant& value, const Type& type )
	: QObject()
{
	_setting = setting;
	_type = type;
	lb_title = new QLabel;
	lb_title->setText( title );

	switch (_type)
	{
		case SettingsItem::Section:
		{

		}
		break;
		case SettingsItem::PushButton:
		{
			QPushButton* pb = new QPushButton;
			pb->setText( value.toString() );
			wid_modifier = pb;
		}
		break;
		case SettingsItem::CheckBox:
		{
			QCheckBox* cb = new QCheckBox;
			cb->setChecked( value.toBool() );
			connect( cb, &QCheckBox::toggled,
					 this, &SettingsItem::onBoolChanged );
			wid_modifier = cb;
		}
		break;
		case SettingsItem::LineEdit:
		{
			QLineEdit* le = new QLineEdit;
			le->setText( value.toString() );
			connect( le, &QLineEdit::editingFinished,
					 this, &SettingsItem::onEditingFinished );
			wid_modifier = le;
		}
		break;
		case SettingsItem::SpinBox:
		{
			QSpinBox* sp = new QSpinBox;
			sp->setValue( value.toInt() );
			connect( sp, &QSpinBox::valueChanged,
					 this, &SettingsItem::onIntChanged );
			wid_modifier = sp;
		}
		break;
		case SettingsItem::DoubleSpinBox:
		{
			QDoubleSpinBox* dsp = new QDoubleSpinBox;
			dsp->setValue( value.toDouble() );
			connect( dsp, &QDoubleSpinBox::valueChanged,
					 this, &SettingsItem::onDoubleChanged );
			wid_modifier = dsp;
		}
		break;
		case SettingsItem::ComboBox:
		{
			QComboBox* cb = new QComboBox;
			cb->setCurrentText( value.toString() );
			connect( cb, &QComboBox::currentTextChanged,
					 this, &SettingsItem::onTextChanged );
			wid_modifier = cb;
		}
		break;
//		case default:
//		return QVariant();
	}
}
/**********************************************/
/**********************************************/
Key SettingsItem::setting() const
{
	return _setting;
}
/**********************************************/
/**********************************************/
SettingsItem::Type SettingsItem::type()
{
	return _type;
}
/**********************************************/
/**********************************************/
void SettingsItem::setTitle( const QString& title )
{
	lb_title->setText( title );
}
/**********************************************/
/**********************************************/
QVariant SettingsItem::value() const
{
	switch (_type)
	{
		case SettingsItem::Section:
		return lb_title->text();
		case SettingsItem::PushButton:
		return static_cast<QPushButton*>(wid_modifier)->text();
		case SettingsItem::CheckBox:
		return static_cast<QCheckBox*>(wid_modifier)->text();
		case SettingsItem::LineEdit:
		return static_cast<QLineEdit*>(wid_modifier)->text();
		case SettingsItem::SpinBox:
		return static_cast<QSpinBox*>(wid_modifier)->text();
		case SettingsItem::DoubleSpinBox:
		return static_cast<QDoubleSpinBox*>(wid_modifier)->text();
		case SettingsItem::ComboBox:
		return static_cast<QComboBox*>(wid_modifier)->currentText();
		default:
		return QVariant();
	}
}
/**********************************************/
/**********************************************/
QLabel* SettingsItem::title() const
{
	return lb_title;
}
/**********************************************/
/**********************************************/
QWidget* SettingsItem::modifier() const
{
	return wid_modifier;
}
/**********************************************/
/**********************************************/
void SettingsItem::onEditingFinished()
{
	if ( _type == SettingsItem::LineEdit )
	{
		QLineEdit* le = static_cast<QLineEdit*>(wid_modifier);
		emit settingChanged( _setting, le->text() );
	}
}
/**********************************************/
/**********************************************/
void SettingsItem::onTextChanged( const QString& text )
{
	emit settingChanged( _setting, text );
}
/**********************************************/
/**********************************************/
void SettingsItem::onBoolChanged( bool value )
{
	emit settingChanged( _setting, value );
}
/**********************************************/
/**********************************************/
void SettingsItem::onIntChanged( int value )
{
	emit settingChanged( _setting, value );
}
/**********************************************/
/**********************************************/
void SettingsItem::onDoubleChanged( double value )
{
	emit settingChanged( _setting, value );
}
/**********************************************/
/**********************************************/
