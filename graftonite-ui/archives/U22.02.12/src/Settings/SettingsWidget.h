#ifndef SETTINGSWIDGET_H
#define SETTINGSWIDGET_H
/**********************************************/
#include <QWidget>
#include <QVariant>
/**********************************************/
#include "../Commons/Key.h"
/**********************************************/
class QLabel;
class QFormLayout;
class Settings;
/**********************************************/
/**********************************************/
/**********************************************/
class SettingsItem
		: public QObject
{
	Q_OBJECT
public:
	enum Type {
		Section,
		PushButton,
		CheckBox,
		LineEdit,
		SpinBox,
		DoubleSpinBox,
		ComboBox,
	};
private:
	Key _setting;
	SettingsItem::Type _type;
	QLabel* lb_title = nullptr;
	QWidget* wid_modifier = nullptr;
public:
	~SettingsItem();
	SettingsItem( const Key& setting, const QString& title, const QVariant& value, const SettingsItem::Type& type = SettingsItem::LineEdit );
	Key setting() const;
	SettingsItem::Type type();
	void setTitle( const QString& title );
	QVariant value() const;
	QLabel* title() const;
	QWidget* modifier() const;
private slots:
	void onEditingFinished();
	void onTextChanged( const QString& text );
	void onBoolChanged( bool value );
	void onIntChanged( int value );
	void onDoubleChanged( double value );
signals:
	void settingChanged( const Key& setting, const QVariant& value );
};
/**********************************************/
/**********************************************/
/**********************************************/
class SettingsWidget
		: public QWidget
{
	Q_OBJECT
public:
	/******************************************/
	QList<SettingsItem*> _items;
	QFormLayout* lay_main = nullptr;
public:
	~SettingsWidget();
	SettingsWidget( QWidget* parent = nullptr );
	/******************************************/
	void addSetting( const Key& setting, const QString& title, const QVariant& value, const SettingsItem::Type& type = SettingsItem::LineEdit );
	void addSettings( const Settings& settings );
	void updateLayout();
	/******************************************/
signals:
	void settingChanged( const Key& setting, const QVariant& value );
	/******************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SETTINGSWIDGET_H
