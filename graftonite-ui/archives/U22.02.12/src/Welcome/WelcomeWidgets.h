#ifndef WELCOMEWIDGETS_H
#define WELCOMEWIDGETS_H

#include <QListView>
#include <QPushButton>
#include <QLabel>
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class WelcomeList
		: public QListView
{
	Q_OBJECT
public:
	virtual ~WelcomeList () {

	}
	WelcomeList ( QWidget *parent = nullptr )
		: QListView( parent )
	{

	}

};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class WelcomeButton
		: public QPushButton
{
	Q_OBJECT
public:
	virtual ~WelcomeButton () {

	}
	WelcomeButton ( QWidget *parent = nullptr )
		: QPushButton( parent )
	{
		setIconSize( {24,24} );
	}

};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class WelcomeTitle
		: public QLabel
{
	Q_OBJECT
public:
	virtual ~WelcomeTitle () {

	}
	WelcomeTitle ( QWidget *parent = nullptr )
		: QLabel( parent )
	{

	}

};
/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class WelcomeSubTitle
		: public QLabel
{
	Q_OBJECT
public:
	virtual ~WelcomeSubTitle () {

	}
	WelcomeSubTitle ( QWidget *parent = nullptr )
		: QLabel( parent )
	{

	}

};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // WELCOMEWIDGETS_H
