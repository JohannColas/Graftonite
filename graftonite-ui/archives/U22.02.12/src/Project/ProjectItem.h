#ifndef PROJECTITEM_H
#define PROJECTITEM_H
/**********************************************/
#include <QTreeWidgetItem>
#include <QItemDelegate>
/**********************************************/
#include "../Commons/Settings.h"
/**********************************************/
/**********************************************/
/**********************************************/
class ProjectItem
		: public QTreeWidgetItem
{
	Settings _settings;
public:
	ProjectItem( QTreeWidgetItem* parent = nullptr );
	void setData( int column, int role, const QVariant& value ) override;
	QVariant data( int column, int role ) const override;
	Key projectType() const;

	QVariant setting( const Key& key ) const;
	void addSetting( const Key& key, const QVariant& value );
	Settings& settings();
	ProjectItem* parent() const;
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PROJECTITEM_H
