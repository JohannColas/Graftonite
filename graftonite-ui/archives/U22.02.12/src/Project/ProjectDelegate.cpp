#include "ProjectDelegate.h"
/**********************************************/
/**********************************************/
/**********************************************/
/**********************************************/
ProjectDelegate::ProjectDelegate( QObject* parent )
	: QItemDelegate(parent)
{

}
/**********************************************/
/**********************************************/
QWidget* ProjectDelegate::createEditor( QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index ) const
{
	if ( index.column() != 1 )
	{
		return QItemDelegate::createEditor(parent, option, index);
	}
	return nullptr;
}
/**********************************************/
/**********************************************/
