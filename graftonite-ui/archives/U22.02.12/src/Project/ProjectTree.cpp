#include "ProjectTree.h"
/**********************************************/
#include "ProjectDelegate.h"
#include "ProjectItem.h"
/**********************************************/
#include <QTreeWidget>
#include <QVBoxLayout>
/**********************************************/
#include <QFile>
#include <QFileInfo>
/**********************************************/
#include <QMenu>
#include <QContextMenuEvent>
/**********************************************/
#include "../Commons/Settings.h"
/**********************************************/
/**********************************************/
/**********************************************/
ProjectTreeView::ProjectTreeView( QWidget* parent)
	: QWidget(parent)
{
	this->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Expanding );
	_tree = new QTreeWidget;
	_tree->setEditTriggers( QAbstractItemView::DoubleClicked | QAbstractItemView::SelectedClicked );
	_tree->setSelectionMode( QAbstractItemView::ExtendedSelection );
	_tree->setVerticalScrollMode( QAbstractItemView::ScrollPerPixel );
	_tree->setHorizontalScrollMode( QAbstractItemView::ScrollPerPixel );
	connect( _tree, &QTreeWidget::itemPressed,
			 this, &ProjectTreeView::onItemPressed );
	connect( _tree, &QTreeWidget::itemDoubleClicked,
			 this, &ProjectTreeView::onItemDoubleClicked );

	ProjectDelegate* delegate = new ProjectDelegate(this);
	_tree->setItemDelegate(delegate);


	QVBoxLayout* layout = new QVBoxLayout;
	layout->addWidget( _tree );
	setLayout( layout );

	_tree->setHeaderLabels( {"Name", "Type", "Path"});
	_tree->setColumnCount( 3 );
	_tree->hideColumn(3);


	// Menu
	contextMenu = new QMenu;
	ac_newproject = new QAction("New project", this);
	ac_openproject = new QAction("Open project", this);
	ac_saveproject = new QAction("Save project", this);
	ac_closeproject = new QAction("Close project", this);
	contextMenu->addAction( ac_newproject );
	contextMenu->addAction( ac_openproject );
	contextMenu->addAction( ac_saveproject );
	contextMenu->addAction( ac_closeproject );
	connect( ac_newproject, &QAction::triggered,
			 this, &ProjectTreeView::newProject );
	connect( ac_openproject, &QAction::triggered,
			 this, &ProjectTreeView::newProject );
	connect( ac_saveproject, &QAction::triggered,
			 this, &ProjectTreeView::newProject );
	connect( ac_closeproject, &QAction::triggered,
			 this, &ProjectTreeView::newProject );

	contextMenu->addSeparator();

	ac_newfolder = new QAction("New folder", this);
	ac_deletefolder = new QAction("Delete folder", this);
	contextMenu->addAction( ac_newfolder );
	contextMenu->addAction( ac_deletefolder );
	connect( ac_newfolder, &QAction::triggered,
			 this, &ProjectTreeView::newFolder );
	connect( ac_deletefolder, &QAction::triggered,
			 this, &ProjectTreeView::newFolder );

	contextMenu->addSeparator();

	ac_newdata = new QAction("New data", this);
	ac_deletedata = new QAction("Delete data", this);
	contextMenu->addAction( ac_newdata );
	contextMenu->addAction( ac_deletedata );
	connect( ac_newdata, &QAction::triggered,
			 this, &ProjectTreeView::newData );
	connect( ac_deletedata, &QAction::triggered,
			 this, &ProjectTreeView::newData );

	contextMenu->addSeparator();

	ac_newgraph = new QAction("New graph", this);
	ac_deletegraph = new QAction("Delete graph", this);
	contextMenu->addAction( ac_newgraph );
	contextMenu->addAction( ac_deletegraph );
	connect( ac_newgraph, &QAction::triggered,
			 this, &ProjectTreeView::newGraph );
	connect( ac_deletegraph, &QAction::triggered,
			 this, &ProjectTreeView::newGraph );

}
/**********************************************/
/**********************************************/
void ProjectTreeView::newProject()
{
	ProjectItem* project = new ProjectItem;
	project->addSetting( Key::Name, "New project" );
	project->addSetting( Key::Type, Key::Project );
	_tree->addTopLevelItem( project );
}
/**********************************************/
/**********************************************/
void ProjectTreeView::newFolder()
{
//	QModelIndex current = this->currentIndex();
//	_tree->currentIndex()

//	ProjectModel* prj_model = static_cast<ProjectModel*>(model());
//	prj_model->addFolder( "New folder", current );
//	QModelIndex index = prj_model->index(0, 0, QModelIndex() );
//	qDebug() << index.row();
//	for ( int it = 0; it < index.row(); ++it )
//	{
//		QModelIndex ind = prj_model->index(it, 0, index );
//		ProjectItem* item = prj_model->item( ind );
//		if ( item ){
//			qDebug().noquote() << item->type().toString() << item->setting(Key::Name);

//		}
//	}
}
/**********************************************/
/**********************************************/
void ProjectTreeView::newData()
{

}
/**********************************************/
/**********************************************/
void ProjectTreeView::newGraph()
{

}
/**********************************************/
/**********************************************/
void ProjectTreeView::openProject( const QString& path )
{

	QFile file( path );
	if ( file.open(QIODevice::ReadOnly) )
	{
		ProjectItem* project = new ProjectItem;
		project->addSetting( Key::Type, Key::Project );
		QList<ProjectItem*> parent = {project};
		QTextStream stream( &file );
		while ( !stream.atEnd() )
		{
			QString line = stream.readLine();
			if ( line.isEmpty() || line.startsWith("//") )
				continue;
			else if ( line.startsWith("#") )
			{
				int indent = line.indexOf('#');
				for ( ; indent < line.size(); ++indent )
					if ( line.at(indent) != '#' )
						break;
				ProjectItem* item;
				if ( indent < parent.size() )
				{
					item = new ProjectItem(parent.at(indent-1));
					parent.at(indent-1)->addChild( item );
					if ( indent < parent.size() )
					{
						parent.replace( indent, item );
						for ( int jt = indent+1; jt < parent.size(); ++jt )
							parent.remove( jt );
					}
					else
						parent.append( item );
				}
				else
				{
					item = new ProjectItem(parent.last());
					parent.last()->addChild(item);
					parent.append( item );
				}
				Settings::DecomposeLine( line, item->settings() );
			}
			else if ( line.contains("=") )
				Settings::DecomposeLine( line, project->settings() );
		}
		file.close();
		if ( !project->settings().hasKey(Key::Path) )
			project->addSetting( Key::Path, QFileInfo(path).absolutePath() );
		_tree->addTopLevelItem(project);
	}
}
/**********************************************/
/**********************************************/
ProjectItem* ProjectTreeView::projectItem( ProjectItem* item )
{
	ProjectItem* project = item;//tree_project2->projectItem( item );
	while ( project != nullptr && project->projectType() != Key::Project )
		project = project->parent();
	return project;
//	if ( item != nullptr && item->projectType() != Key::Project )
//		return projectItem( static_cast<ProjectItem*>(_tree->itemAbove(item)));
//	else
//		return item;
}
/**********************************************/
/**********************************************/
void ProjectTreeView::onItemPressed( QTreeWidgetItem* item, int column )
{
	emit itemPressed( static_cast<ProjectItem*>(item) );
}
/**********************************************/
/**********************************************/
void ProjectTreeView::onItemDoubleClicked( QTreeWidgetItem* item, int column )
{
	Qt::ItemFlags flags = item->flags();
	if ( column == 1 )
	{
		item->setFlags(flags & (~Qt::ItemIsEditable));
	}
	else
	{
		item->setFlags(flags | Qt::ItemIsEditable);
		_tree->editItem( item, column );
	}
}
/**********************************************/
/**********************************************/
void ProjectTreeView::contextMenuEvent( QContextMenuEvent* event )
{
	contextMenu->exec( event->globalPos() );
}
/**********************************************/
/**********************************************/
void ProjectTreeView::enterEvent( QEnterEvent* event )
{
	if ( _tree_to_hide )
	{
		_tree->show();
		_tree->move( this->mapToGlobal(this->pos()) );
	}

	QWidget::enterEvent( event );
}
/**********************************************/
/**********************************************/
void ProjectTreeView::leaveEvent( QEvent* event )
{
	if ( _tree_to_hide )
	{
		_tree->hide();
	}

	QWidget::leaveEvent( event );
}
/**********************************************/
/**********************************************/
void ProjectTreeView::resizeEvent( QResizeEvent* event )
{
	if ( this->width() < 200 )
	{
		_tree_to_hide = true;
		layout()->removeWidget( _tree );
		_tree->hide();
	}
	else
	{
		_tree_to_hide = false;
		layout()->addWidget( _tree );
		_tree->show();
	}
	QWidget::resizeEvent( event );
}
/**********************************************/
/**********************************************/
