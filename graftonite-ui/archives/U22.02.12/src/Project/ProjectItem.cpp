#include "ProjectItem.h"
/**********************************************/
/**********************************************/
/**********************************************/
/**********************************************/
ProjectItem::ProjectItem( QTreeWidgetItem* parent )
	: QTreeWidgetItem(parent)
{
	addSetting( Key::Name, "[Unknown" );
	addSetting( Key::Type, Key::Unknown );
}

void ProjectItem::setData( int column, int role, const QVariant& value )
{
	if ( role == Qt::EditRole )
	{
		if ( column == 0 )
			addSetting(Key::Name, value);
		else if ( column == 2 )
			addSetting(Key::Path, value);
	}
}
/**********************************************/
/**********************************************/
QVariant ProjectItem::data( int column, int role ) const
{
	if ( role == Qt::DisplayRole || role == Qt::EditRole )
	{
		if ( column == 0 )
			return setting(Key::Name);
		else if ( column == 1 )
			return projectType().toString();
		else if ( column == 2 )
			return setting(Key::Path);
		else
			return QVariant();
	}
	else if ( role == Qt::DecorationRole )
	{
		if ( column == 0 )
		{
			Key type = projectType();
			if ( type == Key::Project )
				return QIcon("icons/Dark Theme/graftonite.svg");
			else if ( type == Key::Folder )
				return QIcon("icons/Dark Theme/folder.svg");
			else if ( type == Key::Data )
				return QIcon("icons/Dark Theme/data.svg");
			else if ( type == Key::Graph )
				return QIcon("icons/Dark Theme/graph.svg");
			else if ( type == Key::Image )
				return QIcon("icons/Dark Theme/image.svg");
		}
	}
//	else if ( role == Qt::EditRole )
//	{
//		return QVariant();
//	}
	return QTreeWidgetItem::data( column, role );
}
/**********************************************/
/**********************************************/
Key ProjectItem::projectType() const
{
	return Key::Keys(setting(Key::Type).toInt());
}
/**********************************************/
/**********************************************/
QVariant ProjectItem::setting( const Key& key ) const
{
	return _settings.value(key);
}
/**********************************************/
/**********************************************/
void ProjectItem::addSetting( const Key& key, const QVariant& value )
{
	_settings.addSetting( key, value );
	if ( key == Key::Type )
	{
		Key type = projectType();
		if ( type == Key::Project )
			setIcon( 0, QIcon("icons/Dark Theme/graftonite.svg") );
		else if ( type == Key::Folder )
			setIcon( 0, QIcon("icons/Dark Theme/folder.svg") );
		else if ( type == Key::Data )
			setIcon( 0, QIcon("icons/Dark Theme/data.svg") );
		else if ( type == Key::Graph )
			setIcon( 0, QIcon("icons/Dark Theme/graph.svg") );
		else if ( type == Key::Image )
			setIcon( 0, QIcon("icons/Dark Theme/image.svg") );
	}
}
/**********************************************/
/**********************************************/
Settings& ProjectItem::settings()
{
	return _settings;
}
/**********************************************/
/**********************************************/
ProjectItem* ProjectItem::parent() const
{
	return static_cast<ProjectItem*>(QTreeWidgetItem::parent());
}
/**********************************************/
/**********************************************/
