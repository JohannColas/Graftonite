#include "DataWidget.h"
#include "ui_DataWidget.h"
/**********************************************/
#include "Data.h"
#include <QDebug>
/**********************************************/
/**********************************************/
DataWidget::DataWidget( QWidget *parent ) :
	QWidget( parent ),
	ui( new Ui::DataWidget )
{
	ui->setupUi(this);

	ui->cb_separator->addItems( {"SPACE", "TAB", ";", ","} );
	ui->cb_nbDecimal->addItems( {".", ","} );
}
/**********************************************/
/**********************************************/
DataWidget::~DataWidget()
{
	delete ui;
}
/**********************************************/
/**********************************************/
void DataWidget::setData( Data* data )
{
	disconnect( _data, &Data::changed,
			 this, &DataWidget::updateWidget );
	_data = data;
	connect( _data, &Data::changed,
			 this, &DataWidget::updateWidget );
	updateWidget();
}
/**********************************************/
/**********************************************/
void DataWidget::updateWidget()
{
	if ( !_data ) return;

	ui->pb_dataPath->setText( _data->dataPath() );
	QString sep = _data->separator();
	if ( sep == "\t" )
		sep = "TAB";
	else if ( sep == " " )
		sep = "SPACE";
	ui->cb_separator->setCurrentText( sep );
	ui->ck_skipEmptyParts->setChecked( _data->skipEmptyParts() );
	ui->le_rowToTreat->setText( _data->rowToTreat() );
	ui->cb_nbDecimal->setCurrentText( _data->decimal() );
}
/**********************************************/
/**********************************************/
void DataWidget::on_pb_dataPath_released()
{
	if ( !_data ) return;
//	_data->setDataPath( ui->pb_dataPath->text() );
	emit changed();
}
/**********************************************/
/**********************************************/
void DataWidget::on_cb_separator_currentTextChanged( const QString& sep )
{
	if ( !_data ) return;
	QString newsep = sep;
	if ( newsep == "SPACE" )
		newsep = " ";
	else if ( newsep == "TAB" )
		newsep = "\t";
	_data->setSeparator( newsep );
	emit changed();
}
/**********************************************/
/**********************************************/
void DataWidget::on_ck_skipEmptyParts_stateChanged( int arg1 )
{
	if ( !_data ) return;
	_data->setSkipEmptyParts( arg1 );
	emit changed();
}
/**********************************************/
/**********************************************/
void DataWidget::on_le_rowToTreat_editingFinished()
{
	if ( !_data ) return;
	_data->setRowToTreat( ui->le_rowToTreat->text() );
	emit changed();
}
/**********************************************/
/**********************************************/
void DataWidget::on_cb_nbDecimal_currentTextChanged( const QString& decimal )
{
	if ( !_data ) return;
	_data->setDecimal( decimal );
	emit changed();
}
/**********************************************/
/**********************************************/
