#include "Data.h"
/**********************************************/
#include "Commons/Key.h"
#include "Commons/Files.h"
#include "Commons/XML.h"
/**********************************************/
/**********************************************/
Data::Data()
{

}
/**********************************************/
/**********************************************/
void Data::setPath( const QString& path )
{
//	_path = path;

//	QDomDocument doc = Files::readXML( _path );
//	QDomElement doc_el = doc.documentElement();
//	QString str = XML::getString( doc_el, Keys::ID );
//	if ( !str.isEmpty() )
//		setID(str);
//	for ( QDomElement el : XML::ChildElements(doc_el) )
//	{
//		QString elType = el.tagName();
//		if ( elType == Keys::File )
//		{
//			str = XML::getString( el, Keys::Path );
//			if ( !str.isEmpty() )
//				setDataPath(str);
//		}
//		else if ( elType == Keys::Number )
//		{
//			str = XML::getString( el, Keys::Decimal );
//			if ( !str.isEmpty() )
//				setDecimal(str);
//		}
//		else if ( elType == Keys::Treatment )
//		{
//			str = XML::getString( el, Keys::Separator );
//			if ( !str.isEmpty() )
//				setSeparator(str);
//			str = XML::getString( el, Keys::SkipEmptyParts );
//			if ( !str.isEmpty() )
//				setSkipEmptyParts(str.toInt());
//			str = XML::getString( el, Keys::RowToTreat );
//			if ( !str.isEmpty() )
//				setRowToTreat(str);
//		}
//	}
//	treat();
}
/**********************************************/
/**********************************************/
void Data::setDataPath( const QString& dataPath )
{
	_dataPath = dataPath;
	emit changed();
}
/**********************************************/
/**********************************************/
void Data::clear()
{

}
/**********************************************/
/**********************************************/
void Data::init()
{

}
/**********************************************/
/**********************************************/
//void Data::set( const QDomElement& /*sets*/ )
//{
//	//	XML::setString( sets, Keys::DataPath, _dataPath );
//}
/**********************************************/
/**********************************************/
//QDomElement Data::save()
//{
//	XML::ResetDocument();
//	//
//	//	QDomElement el = XML::CreateElement( Keys::Data );
//	//	el.setAttribute( Keys::DataPath, _dataPath );
//	//	return el;
//}
/**********************************************/
/**********************************************/
void Data::saveXML()
{
//	QDomDocument doc;
//	QDomElement el = doc.createElement( Keys::Data );
//	el.setAttribute( Keys::ID, _id );

//	QDomElement cel = doc.createElement( Keys::File );
//	cel.setAttribute( Keys::Path, _dataPath );
//	el.appendChild( cel );

//	cel = doc.createElement( Keys::Number );
//	cel.setAttribute( Keys::Decimal, _decimal );
//	el.appendChild( cel );

//	cel = doc.createElement( Keys::Treatment );
//	cel.setAttribute( Keys::Separator, _separator );
//	cel.setAttribute( Keys::SkipEmptyParts, _skipEmptyParts );
//	cel.setAttribute( Keys::RowToTreat, _rowToTreat );
//	el.appendChild( cel );

//	cel = doc.createElement( Keys::PostTreatment );
//	/*------------*/
//	el.appendChild( cel );

//	doc.insertBefore( el, doc.doctype() );
//	Files::saveXML( _path, doc );
}
/**********************************************/
/**********************************************/
void Data::treat()
{
	_data.clear();
	_rowCount = 0;
	_columnCount = 0;
	QString content;
	Files::readFile( _dataPath, content );
	QStringList lines = content.split( '\n', Qt::SkipEmptyParts );
	_rowCount = lines.size();
	QStringList row_to_treat = _rowToTreat.split( ";", Qt::KeepEmptyParts );
	for ( QString to_treat : row_to_treat )
	{
		QStringList indexes = to_treat.split( "-", Qt::KeepEmptyParts );
		int begin_row = indexes.first().toInt()-1;
		if ( begin_row < 0 || indexes.first().isEmpty() || to_treat.isEmpty() )
			begin_row = 0;
		int end_row = indexes.last().toInt();
		if ( end_row < 0 || end_row > _rowCount || indexes.last().isEmpty() || to_treat.isEmpty() )
			end_row = _rowCount;
		for ( int it = begin_row; it < end_row; ++it )
		{
			QString line = lines.at(it);
			QVector<QString> dataline;
			for ( QString col : line.split( _separator, _skipEmptyParts ? Qt::SkipEmptyParts : Qt::KeepEmptyParts ) )
			{
				dataline.append( col );
			}
			_data.append( dataline );
			if ( _columnCount < dataline.size() )
				_columnCount = dataline.size();
		}
	}
}
/**********************************************/
/**********************************************/
void Data::post_treat()
{

}
/**********************************************/
/**********************************************/
