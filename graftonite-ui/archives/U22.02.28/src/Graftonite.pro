QT       += core gui xml svg

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

qtHaveModule(opengl): QT += opengl

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
	BasicWidgets/ComboBox.cpp \
	BasicWidgets/Popup.cpp \
	BasicWidgets/PopupButton.cpp \
	BasicWidgets/TextEdit.cpp \
	Commons/App.cpp \
	Commons/Keys.cpp \
	Data/Data.cpp \
	Data/DataEditor.cpp \
	Data/DataWidget.cpp \
	Commons/Files.cpp \
	Graftonite.cpp \
	Graph/Axis.cpp \
	BasicWidgets/SvgViewer.cpp \
	Graph/Curve.cpp \
	Graph/Element.cpp \
	Graph/Graph.cpp \
	Graph/GraphEditor.cpp \
	Graph/Layer.cpp \
	Graph/SVG.cpp \
	Graph/Widgets/AxisWidget.cpp \
	Graph/Widgets/CurveWidget.cpp \
	Graph/Widgets/DataSelector.cpp \
	Graph/Widgets/GraphWidget.cpp \
	Graph/Widgets/LayerWidget.cpp \
	Commons/XML.cpp \
	Project/Project.cpp \
	Project/ProjectItem.cpp \
	Project/ProjectTree.cpp \
	main.cpp

HEADERS += \
	BasicWidgets/ComboBox.h \
	BasicWidgets/Popup.h \
	BasicWidgets/PopupButton.h \
	BasicWidgets/TextEdit.h \
	Commons/App.h \
	Commons/Keys.h \
	Data/Data.h \
	Data/DataEditor.h \
	Data/DataWidget.h \
	Commons/Files.h \
	Graftonite.h \
	Graph/Axis.h \
	BasicWidgets/ImageViewer.h \
	BasicWidgets/SvgViewer.h \
	Graph/Curve.h \
	Graph/Element.h \
	Graph/Graph.h \
	Graph/GraphEditor.h \
	Graph/Layer.h \
	Graph/SVG.h \
	Graph/Widgets/AxisWidget.h \
	Graph/Widgets/CurveWidget.h \
	Graph/Widgets/DataSelector.h \
	Graph/Widgets/GraphWidget.h \
	Graph/Widgets/LayerWidget.h \
	Commons/XML.h \
	Project/Project.h \
	Project/ProjectItem.h \
	Project/ProjectTree.h

FORMS += \
	Data/DataEditor.ui \
	Data/DataWidget.ui \
	Graftonite.ui \
	Graph/GraphEditor.ui \
	Graph/Widgets/AxisWidget.ui \
	Graph/Widgets/CurveWidget.ui \
	Graph/Widgets/GraphWidget.ui \
	Graph/Widgets/LayerWidget.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
