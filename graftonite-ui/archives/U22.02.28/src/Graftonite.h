#ifndef GRAFTONITE_H
#define GRAFTONITE_H
/**********************************************/
#include <QWidget>
/**********************************************/
class QVBoxLayout;
class QSplitter;
class QStackedWidget;
class QPushButton;
//
class ProjectTreeView;
class ProjectItem;
//
/**********************************************/
class WelcomeFrame;
class SettingsFrame;
class DataEditor;
class GraphEditor;
//
class ImageViewer;
/**********************************************/
/**********************************************/
/**********************************************/
class Graftonite
        : public QWidget
{
	Q_OBJECT
public:
	explicit Graftonite( QWidget* parent = nullptr );
	~Graftonite();
	/**********************************************/
	void update_projectExplorer();
	/**********************************************/
private slots:
	// Sidebar slots
	void on_pb_welcome_released();
	void on_pb_settings_released();
	void onSplitterMoved( int pos, int index );
	//
	// ProjectTree slots
	void onProjectItemChanged( ProjectItem* item );
	/**********************************************/
private:
	QVBoxLayout* _layout = nullptr;
	QSplitter* splitter = nullptr;
	// SideBar button
	QWidget* wid_sidebar = nullptr;
	bool _sidebar_is_collapsed = false;
	int _save_sidebar_width = 0;
	QPushButton* pb_welcome = nullptr;
	QPushButton* pb_project_explorer = nullptr;
	QPushButton* pb_settings = nullptr;
	//
	ProjectTreeView* tree_project = nullptr;
	//
	QStackedWidget* cont_main = nullptr;
	WelcomeFrame* wid_welcome = nullptr;
	SettingsFrame* wid_settings = nullptr;
	DataEditor* wid_data = nullptr;
	GraphEditor* wid_graph = nullptr;
	ImageViewer* wid_image = nullptr;
	QWidget* wid_empty = nullptr;
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // GRAFTONITE_H
