#include "ProjectTreeWidget.h"
/**********************************************/
#include <QFile>
#include <QFileInfo>
#include <QTreeWidget>
#include <QVBoxLayout>
/**********************************************/
#include <QMenu>
#include <QContextMenuEvent>
/**********************************************/
#include "../Commons/Settings.h"
/**********************************************/
#include "ProjectTreeItem.h"
/**********************************************/
/**********************************************/
/**********************************************/
ProjectTreeWidget::ProjectTreeWidget( QWidget* parent)
	: QWidget(parent)
{
	this->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Expanding );
	_tree = new QTreeWidget;
	_tree->setEditTriggers( QAbstractItemView::DoubleClicked | QAbstractItemView::SelectedClicked );
	_tree->setSelectionMode( QAbstractItemView::ExtendedSelection );
	_tree->setVerticalScrollMode( QAbstractItemView::ScrollPerPixel );
	_tree->setHorizontalScrollMode( QAbstractItemView::ScrollPerPixel );
	connect( _tree, &QTreeWidget::itemPressed,
			 this, &ProjectTreeWidget::onItemPressed );
	connect( _tree, &QTreeWidget::itemDoubleClicked,
			 this, &ProjectTreeWidget::onItemDoubleClicked );

	ProjectItemDelegate* delegate = new ProjectItemDelegate(this);
	_tree->setItemDelegate(delegate);


	QVBoxLayout* layout = new QVBoxLayout;
	layout->addWidget( _tree );
	setLayout( layout );

	_tree->setHeaderLabels( {"Name", "Type", "Path"});
	_tree->setColumnCount( 3 );
	_tree->hideColumn(3);


	// Menu
	contextMenu = new QMenu;
	ac_newproject = new QAction("New project", this);
	ac_openproject = new QAction("Open project", this);
	ac_saveproject = new QAction("Save project", this);
	ac_closeproject = new QAction("Close project", this);
	contextMenu->addAction( ac_newproject );
	contextMenu->addAction( ac_openproject );
	contextMenu->addAction( ac_saveproject );
	contextMenu->addAction( ac_closeproject );
	connect( ac_newproject, &QAction::triggered,
			 this, &ProjectTreeWidget::newProject );
	connect( ac_openproject, &QAction::triggered,
			 this, &ProjectTreeWidget::newProject );
	connect( ac_saveproject, &QAction::triggered,
			 this, &ProjectTreeWidget::newProject );
	connect( ac_closeproject, &QAction::triggered,
			 this, &ProjectTreeWidget::newProject );

	contextMenu->addSeparator();

	ac_newfolder = new QAction("New folder", this);
	ac_deletefolder = new QAction("Delete folder", this);
	contextMenu->addAction( ac_newfolder );
	contextMenu->addAction( ac_deletefolder );
	connect( ac_newfolder, &QAction::triggered,
			 this, &ProjectTreeWidget::newFolder );
	connect( ac_deletefolder, &QAction::triggered,
			 this, &ProjectTreeWidget::newFolder );

	contextMenu->addSeparator();

	ac_newdata = new QAction("New data", this);
	ac_deletedata = new QAction("Delete data", this);
	contextMenu->addAction( ac_newdata );
	contextMenu->addAction( ac_deletedata );
	connect( ac_newdata, &QAction::triggered,
			 this, &ProjectTreeWidget::newData );
	connect( ac_deletedata, &QAction::triggered,
			 this, &ProjectTreeWidget::newData );

	contextMenu->addSeparator();

	ac_newgraph = new QAction("New graph", this);
	ac_deletegraph = new QAction("Delete graph", this);
	contextMenu->addAction( ac_newgraph );
	contextMenu->addAction( ac_deletegraph );
	connect( ac_newgraph, &QAction::triggered,
			 this, &ProjectTreeWidget::newGraph );
	connect( ac_deletegraph, &QAction::triggered,
			 this, &ProjectTreeWidget::newGraph );

}
/**********************************************/
/**********************************************/
void ProjectTreeWidget::newProject()
{
	ProjectTreeItem* project = new ProjectTreeItem;
	project->addSetting( Key::Name, "New project" );
	project->addSetting( Key::Type, Key::Project );
	_tree->addTopLevelItem( project );
}
/**********************************************/
/**********************************************/
void ProjectTreeWidget::newFolder()
{
//	QModelIndex current = this->currentIndex();
//	_tree->currentIndex()

//	ProjectModel* prj_model = static_cast<ProjectModel*>(model());
//	prj_model->addFolder( "New folder", current );
//	QModelIndex index = prj_model->index(0, 0, QModelIndex() );
//	qDebug() << index.row();
//	for ( int it = 0; it < index.row(); ++it )
//	{
//		QModelIndex ind = prj_model->index(it, 0, index );
//		ProjectItem* item = prj_model->item( ind );
//		if ( item ){
//			qDebug().noquote() << item->type().toString() << item->setting(Key::Name);

//		}
//	}
}
/**********************************************/
/**********************************************/
void ProjectTreeWidget::newData()
{

}
/**********************************************/
/**********************************************/
void ProjectTreeWidget::newGraph()
{

}
/**********************************************/
/**********************************************/
void ProjectTreeWidget::openProject( const QString& path )
{

	QFile file( path );
	if ( file.open(QIODevice::ReadOnly) )
	{
		ProjectTreeItem* project = new ProjectTreeItem;
		project->addSetting( Key::Type, Key::Project );
		QList<ProjectTreeItem*> parent = {project};
		QTextStream stream( &file );
		while ( !stream.atEnd() )
		{
			QString line = stream.readLine();
			if ( line.isEmpty() || line.startsWith("//") )
				continue;
			else if ( line.startsWith("#") )
			{
				int indent = line.indexOf('#');
				for ( ; indent < line.size(); ++indent )
					if ( line.at(indent) != '#' )
						break;
				ProjectTreeItem* item;
				if ( indent < parent.size() )
				{
					item = new ProjectTreeItem(parent.at(indent-1));
					parent.at(indent-1)->addChild( item );
					if ( indent < parent.size() )
					{
						parent.replace( indent, item );
						for ( int jt = indent+1; jt < parent.size(); ++jt )
							parent.remove( jt );
					}
					else
						parent.append( item );
				}
				else
				{
					item = new ProjectTreeItem(parent.last());
					parent.last()->addChild(item);
					parent.append( item );
				}
				Settings::DecomposeLine( line, item->settings() );
			}
			else if ( line.contains("=") )
				Settings::DecomposeLine( line, project->settings() );
		}
		file.close();
		if ( !project->settings().hasKey(Key::Path) )
			project->addSetting( Key::Path, QFileInfo(path).absolutePath() );
		_tree->addTopLevelItem(project);
	}
}
/**********************************************/
/**********************************************/
ProjectTreeItem* ProjectTreeWidget::projectItem( ProjectTreeItem* item )
{
	ProjectTreeItem* project = item;//tree_project2->projectItem( item );
	while ( project != nullptr && project->projectType() != Key::Project )
		project = project->parent();
	return project;
//	if ( item != nullptr && item->projectType() != Key::Project )
//		return projectItem( static_cast<ProjectTreeItem*>(_tree->itemAbove(item)));
//	else
//		return item;
}
/**********************************************/
/**********************************************/
void ProjectTreeWidget::onItemPressed( QTreeWidgetItem* item, int column )
{
	emit itemPressed( static_cast<ProjectTreeItem*>(item) );
}
/**********************************************/
/**********************************************/
void ProjectTreeWidget::onItemDoubleClicked( QTreeWidgetItem* item, int column )
{
	Qt::ItemFlags flags = item->flags();
	if ( column == 1 )
	{
		item->setFlags(flags & (~Qt::ItemIsEditable));
	}
	else
	{
		item->setFlags(flags | Qt::ItemIsEditable);
		_tree->editItem( item, column );
	}
}
/**********************************************/
/**********************************************/
void ProjectTreeWidget::contextMenuEvent( QContextMenuEvent* event )
{
	contextMenu->exec( event->globalPos() );
}
/**********************************************/
/**********************************************/
void ProjectTreeWidget::enterEvent( QEnterEvent* event )
{
	if ( _tree_to_hide )
	{
		_tree->show();
		_tree->move( this->mapToGlobal(this->pos()) );
	}

	QWidget::enterEvent( event );
}
/**********************************************/
/**********************************************/
void ProjectTreeWidget::leaveEvent( QEvent* event )
{
	if ( _tree_to_hide )
	{
		_tree->hide();
	}

	QWidget::leaveEvent( event );
}
/**********************************************/
/**********************************************/
void ProjectTreeWidget::resizeEvent( QResizeEvent* event )
{
	if ( this->width() < 200 )
	{
		_tree_to_hide = true;
		layout()->removeWidget( _tree );
		_tree->hide();
	}
	else
	{
		_tree_to_hide = false;
		layout()->addWidget( _tree );
		_tree->show();
	}
	QWidget::resizeEvent( event );
}
/**********************************************/
/**********************************************/
