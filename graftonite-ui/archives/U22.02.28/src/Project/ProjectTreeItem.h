#ifndef PROJECTTREEITEM_H
#define PROJECTTREEITEM_H
/**********************************************/
#include <QTreeWidgetItem>
#include <QItemDelegate>
/**********************************************/
#include "../Commons/Settings.h"
/**********************************************/
/**********************************************/
/**********************************************/
class ProjectItemDelegate
		: public QItemDelegate
{
//	Q_OBJECT
public:
	ProjectItemDelegate( QObject* parent );;
	QWidget* createEditor( QWidget*parent, const QStyleOptionViewItem& option, const QModelIndex& index ) const override;
};
/**********************************************/
/**********************************************/
/**********************************************/
class ProjectTreeItem
		: public QTreeWidgetItem
{
	Settings _settings;
public:
	ProjectTreeItem( QTreeWidgetItem* parent = nullptr );
	void setData( int column, int role, const QVariant& value ) override;
	QVariant data( int column, int role ) const override;
	Key projectType() const;

	QVariant setting( const Key& key ) const;
	void addSetting( const Key& key, const QVariant& value );
	Settings& settings();
	ProjectTreeItem* parent() const
	{
		return static_cast<ProjectTreeItem*>(QTreeWidgetItem::parent());
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PROJECTTREEITEM_H
