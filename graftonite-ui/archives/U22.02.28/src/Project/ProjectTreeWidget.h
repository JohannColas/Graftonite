#ifndef PROJECTTREEWIDGET_H
#define PROJECTTREEWIDGET_H
/**********************************************/
#include <QWidget>
/**********************************************/
class QTreeWidget;
class QTreeWidgetItem;
class ProjectTreeItem;
/**********************************************/
class QMenu;
class QAction;
/**********************************************/
/**********************************************/
/**********************************************/
class ProjectTreeWidget
		: public QWidget
{
	Q_OBJECT
private:
	QTreeWidget* _tree = nullptr;
	bool _tree_to_hide = false;
	// Menu
	QMenu* contextMenu = nullptr;
	QAction* ac_newproject = nullptr;
	QAction* ac_openproject = nullptr;
	QAction* ac_saveproject = nullptr;
	QAction* ac_closeproject = nullptr;
	QAction* ac_newfolder = nullptr;
	QAction* ac_deletefolder = nullptr;
	QAction* ac_newdata = nullptr;
	QAction* ac_deletedata = nullptr;
	QAction* ac_newgraph = nullptr;
	QAction* ac_deletegraph = nullptr;
	/******************************************/
public:
	ProjectTreeWidget( QWidget *parent = nullptr );
	/******************************************/
	void openProject( const QString& path );
	/******************************************/
	ProjectTreeItem* projectItem( ProjectTreeItem* item );
	/******************************************/
public slots:
	void newProject();
	void newFolder();
	void newData();
	void newGraph();
	/******************************************/
private slots:
	void onItemPressed( QTreeWidgetItem* item, int column );
	void onItemDoubleClicked( QTreeWidgetItem *item, int column );
	/******************************************/
	void contextMenuEvent( QContextMenuEvent* event ) override;
	void enterEvent( QEnterEvent* event ) override;
	void leaveEvent( QEvent* event ) override;
	void resizeEvent( QResizeEvent* event ) override;
	/******************************************/
signals:
	void itemPressed( ProjectTreeItem* item );
	/******************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PROJECTTREEWIDGET_H
