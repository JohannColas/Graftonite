#ifndef PROJECTTREEVIEW_H
#define PROJECTTREEVIEW_H
/**********************************************/
#include <QWidget>
#include <QTreeWidget>
/**********************************************/
class QVBoxLayout;
/**********************************************/
class QTreeWidgetItem;
class ProjectItem;
/**********************************************/
class QMenu;
class QAction;
/**********************************************/
class ProjectTree
		: public QTreeWidget
{
	Q_OBJECT
public:
	ProjectTree( QWidget* parent = nullptr );
private slots:
	void leaveEvent( QEvent* event ) override;
signals:
	void leaved();
};
class ProjectWidget
		: public QWidget
{
	Q_OBJECT
public:
	ProjectWidget( QWidget* parent = nullptr );
private slots:
	void leaveEvent( QEvent* event ) override;
signals:
	void leaved();

};
/**********************************************/
/**********************************************/
class ProjectTreeView
		: public QWidget
{
	Q_OBJECT
private:
	QVBoxLayout* lay_main = nullptr;
	ProjectWidget* wid_cont = nullptr;
	QVBoxLayout* lay_cont = nullptr;
	ProjectTree* _tree = nullptr;
	bool _tree_to_hide = false;
	bool _abletohide = false;
	// Menu
	QMenu* contextMenu = nullptr;
	QAction* ac_newproject = nullptr;
	QAction* ac_newfolder = nullptr;
	QAction* ac_newdata = nullptr;
	QAction* ac_newgraph = nullptr;
	QAction* ac_newimage = nullptr;
	QAction* ac_open = nullptr;
	QAction* ac_load = nullptr;
	QAction* ac_save = nullptr;
	QAction* ac_export = nullptr;
	QAction* ac_delete = nullptr;
	QAction* ac_close = nullptr;
	/******************************************/
public:
	~ProjectTreeView();
	ProjectTreeView( QWidget *parent = nullptr );
	/******************************************/
	void openProject( const QString& path );
	/******************************************/
	ProjectItem* projectItem( ProjectItem* item );
	/******************************************/
	void setAbleToHide( bool abletohide );
	/******************************************/
public slots:
	void newProject();
	void newFolder();
	void newData();
	void newGraph();
	void newImage();
	void open();
	void load();
	void deleteItem();
	void save();
	void exportItem();
	void closeItem();
	/******************************************/
private slots:
	void onItemPressed( QTreeWidgetItem* item, int column );
	void onItemDoubleClicked( QTreeWidgetItem *item, int column );
	/******************************************/
	void contextMenuEvent( QContextMenuEvent* event ) override;
	void enterEvent( QEnterEvent* event ) override;
	void leaveEvent( QEvent* event ) override;
	void resizeEvent( QResizeEvent* event ) override;
	/******************************************/
signals:
	void itemPressed( ProjectItem* item );
	/******************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PROJECTTREEVIEW_H
