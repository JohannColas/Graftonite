#ifndef PROJECT_H
#define PROJECT_H
/**********************************************/
#include <QObject>
/**********************************************/
class Data;
class Graph;
class QTreeWidgetItem;
class QDomElement;
/**********************************************/
/**********************************************/
/**********************************************/
class Project
{
private:
	QString _name;
	QString _type;
	QString _path;
	QStringList _availableData;
	/**********************************************/
public:
	Project();
	Project( const QString& path );
	QString name() { return _name; }
	void setName( const QString& name );
	QString type() { return _type; }
	void setType( const QString& type );
	QString path() { return _path; }
	void setPath( const QString& path );
	/**********************************************/
	QStringList availableData();
	/**********************************************/
	void readXML();
	void readXML( const QDomElement& el, const QString& relative_path = QString() );
	void saveXML();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PROJECT_H
