#ifndef TITLESETTINGSWIDGET_H
#define TITLESETTINGSWIDGET_H
/**********************************************/
#include "../Settings/SettingsWidget.h"
/**********************************************/
/**********************************************/
/**********************************************/
class TitleSettingsWidget
        : public SettingsWidget
{
	Q_OBJECT
public:
	TitleSettingsWidget( QWidget* parent = nullptr );
	/******************************************/
	void connectSettings( Settings* settings ) override;
	/******************************************/
signals:
	/******************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TITLESETTINGSWIDGET_H
