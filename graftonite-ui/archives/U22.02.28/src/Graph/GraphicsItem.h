#ifndef GRAPHICSITEM_H
#define GRAPHICSITEM_H
/**********************************************/
#include <QObject>
#include <QTreeWidgetItem>
#include <QGraphicsItemGroup>
/**********************************************/
#include "../Commons/Settings.h"
/**********************************************/
#include "../Commons/GeometricObjects.h"
/**********************************************/
class GraphItem;
/**********************************************/
/**********************************************/
/**********************************************/
class GraphicsItem
		: public QObject,
		  public QGraphicsItemGroup,
		  public QTreeWidgetItem
{
	Q_OBJECT
protected:
	Settings _settings;
	Key _item_type = Key::Unknown;
	QGraphicsRectItem* item_bounding_rect = nullptr;
	/******************************************/
public:
	~GraphicsItem();
	GraphicsItem( GraphItem* parent );
	GraphicsItem( QGraphicsItem* parent = nullptr );
	GraphicsItem( const QString& name, QGraphicsItem* parent = nullptr );
	Key itemType() const;
	QString name() const;
	/******************************************/
	// Settings Management
	Settings& settings();
	Settings settings() const;
	virtual void addSetting( const Key& key, const QVariant& value );
	bool setting( QVariant& boolean, const Key& key );
	bool setting( QString& boolean, const Key& key );
	bool setting( bool& boolean, const Key& key );
	bool setting( int& integer, const Key& key );
	bool setting( double& doubl, const Key& key );
	bool setting( QBrush& integer, const Key& key );
	bool setting( QPen& integer, const Key& key );
	/******************************************/
	QVariant data( int column, int role ) const override;
	void setData( int column, int role, const QVariant& value ) override;
	/******************************************/
public slots:
	void onSettingsChanged();
	/******************************************/
	virtual void updateGraphItem();
	/******************************************/
	void mouseReleaseEvent( QGraphicsSceneMouseEvent* event ) override;
signals:
	void changed();
	void treeItemSelected( QTreeWidgetItem* item );
	/******************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // GRAPHICSITEM_H
