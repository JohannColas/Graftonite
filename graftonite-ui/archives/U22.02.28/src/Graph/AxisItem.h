#ifndef AXISITEM_H
#define AXISITEM_H
/**********************************************/
#include "GraphicsItem.h"
/**********************************************/
/**********************************************/
/**********************************************/
class AxisItem
		: public GraphicsItem
{
	GraphItem* _graph = nullptr;
	QGraphicsPathItem* _line_item = nullptr;
	QList<QGraphicsPathItem*> _tick_items;
	QList<QGraphicsPathItem*> _minor_tick_items;
	QList<QGraphicsTextItem*> _label_items;
	QGraphicsTextItem* _title_item = nullptr;
	QList<QGraphicsPathItem*> _grid_items;
	QList<QGraphicsPathItem*> _minor_grid_items;
public:
	~AxisItem();
	AxisItem( QGraphicsItem* parent = nullptr );
	AxisItem( const QString& name, QGraphicsItem* parent = nullptr );
	void init();
	/******************************************/
	double scale( double real_value );
	QPointF deviceCoordinates( double real_value );
	void CalculateScaleCoefficients();
	void CalculateTicksPosition( QList<QPointF>& tickspos, QList<QPointF>& minortickspos, QList<QString>& labels );
	/******************************************/
	QRectF frameRect() const;
	/******************************************/
public slots:
	void updateGraphItem() override;
	/******************************************/
private:
	Rect _frameRect = {100, 100, 1200, 800};
	Rect _boundingRect = {0,0,0,0};
	Rect _tightBoundingRect = {0,0,0,0};
	Key _type = Key::X;
	double _min = 0;
	double _max = 1;
	Key _scale = Key::Linear;
	double _scale_option = 0;
	double a_x, b_x;
	double a_y, b_y;
	Line _line;
	QList<Line> _ticks;
	QList<Line> _minor_ticks;
//	LineStyle _line_style;
	//	UList<StyledText> _labels;
	QList<QString> _labels;
//	TextStyle _Labels_style;
//	StyledText _title;
//	UList<StyledText> _titles;
//	TextStyle _title_style;
	QList<QLineF> _grids;
//	LineStyle _grids_style;
	QList<QLineF> _minor_grids;
//	LineStyle _minor_grids_style;
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // AXISITEM_H
