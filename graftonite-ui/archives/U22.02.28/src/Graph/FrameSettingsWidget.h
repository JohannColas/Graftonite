#ifndef FRAMESETTINGSWIDGET_H
#define FRAMESETTINGSWIDGET_H
/**********************************************/
#include "../Settings/SettingsWidget.h"
/**********************************************/
/**********************************************/
/**********************************************/
class FrameSettingsWidget
        : public SettingsWidget
{
	Q_OBJECT
public:
	FrameSettingsWidget( QWidget* parent = nullptr );
	/******************************************/
	void connectSettings( Settings* settings ) override;
	/******************************************/
signals:
	/******************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // FRAMESETTINGSWIDGET_H
