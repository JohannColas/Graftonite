#ifndef SHAPEITEM_H
#define SHAPEITEM_H
/**********************************************/
#include "GraphicsItem.h"
/**********************************************/
/**********************************************/
/**********************************************/
class ShapeItem
		: public GraphicsItem
{
public:
	ShapeItem( QGraphicsItem* parent = nullptr );
	ShapeItem( const QString& name, QGraphicsItem* parent = nullptr );
	void init();
	/******************************************/
	void addSetting( const Key& key, const QVariant& value ) override;
	/******************************************/
public slots:
	void updateGraphItem() override;
	/******************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SHAPEITEM_H
