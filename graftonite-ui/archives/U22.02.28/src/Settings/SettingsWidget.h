#ifndef SETTINGSWIDGET_H
#define SETTINGSWIDGET_H
/**********************************************/
#include <QScrollArea>
#include <QVariant>
/**********************************************/
#include "SettingItem.h"
#include "../Commons/Key.h"
/**********************************************/
class QFormLayout;
class QComboBox;
class ComboBox2;
class PenModifier;
class Settings;
/**********************************************/
/**********************************************/
/**********************************************/
class SettingsWidget
        : public QScrollArea
{
	Q_OBJECT
protected:
	/******************************************/
	QList<SettingItem*> _items;
	QWidget* wid_cont = nullptr;
	QFormLayout* lay_main = nullptr;
	Settings* _settings = nullptr;
public:
	~SettingsWidget();
	SettingsWidget( QWidget* parent = nullptr );
	/******************************************/
	SettingItem* getItem( const Key& setting );
	/******************************************/
	SettingItem* addSetting( const Key& setting, const QString& title, const QVariant& value, const SettingItem::Type& type = SettingItem::LineEdit );
	void addSeparator();
	void addSection( const QString& title );
	SettingItem* addWidget( const Key& setting, const QString& title, QWidget* widget );
	SettingItem* addWidgetRow( const Key& setting, QWidget* widget );
	void addPushButton( const Key& setting, const QString& title, const QVariant& value = QVariant() );
	void addCheckBox( const Key& setting, const QString& title, const QVariant& value = QVariant() );
	void addLineEdit( const Key& setting, const QString& title, const QVariant& value = QVariant() );
	void addIntegerEdit( const Key& setting, const QString& title, const QVariant& value = QVariant() );
	void addPositiveIntegerEdit( const Key& setting, const QString& title, const QVariant& value = QVariant() );
	void addDoubleEdit( const Key& setting, const QString& title, const QVariant& value = QVariant() );
	void addPositiveDoubleEdit( const Key& setting, const QString& title, const QVariant& value = QVariant() );
	QComboBox* addComboBox( const Key& setting, const QString& title, const QVariant& value = QVariant() );
	void addColorSelector( const Key& setting, const QString& title, const QVariant& value = QVariant() );
	ComboBox2* addAnchorSelector( const Key& setting, const QString& title, const QVariant& value = QVariant() );
	ComboBox2* addKeySelector( const Key& setting, const QString& title, const QVariant& value = QVariant() );
	PenModifier* addPenEditor( const Key& setting, const QString& title, const QVariant& value = QVariant() );
	//void addAxisType
	//void addAxisPosition
	//void addAxisFrame
	//void addLineDash
	//void addLineJoin
	//void addLineCap
	//void addAnchor
	/******************************************/
	virtual void connectSettings( Settings* settings );
	virtual void updateSettings( Settings* settings );
	/******************************************/
signals:
	void settingChanged( const Key& setting, const QVariant& value );
	/******************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SETTINGSWIDGET_H
