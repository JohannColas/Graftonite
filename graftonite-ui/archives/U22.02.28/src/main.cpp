#include <QApplication>
/**********************************************/
#include "Graftonite.h"
#include "Commons/App.h"
/**********************************************/
/**********************************************/
/**********************************************/
int main( int argc, char* argv[] )
{
	QApplication a(argc, argv);

	App::addProjects( "#project-exemple/project.gprj" );

	Graftonite w;
	w.show();

	return a.exec();
}
/**********************************************/
/**********************************************/
/**********************************************/
