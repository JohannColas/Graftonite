#ifndef POPUPBUTTON_H
#define POPUPBUTTON_H
/**********************************************/
#include <QPushButton>
/**********************************************/
class Popup;
/**********************************************/
/**********************************************/
/**********************************************/
class PopupButton : public QPushButton
{
	Q_OBJECT
protected:
	Popup* _popup = 0;
	bool _showPopupOnEnterEvent = false;
public:
	explicit PopupButton( QWidget* parent = nullptr );
	explicit PopupButton( const QString& text, QWidget* parent = nullptr );
	/**********************************************/
	virtual bool childIsInative();
	/**********************************************/
	Popup* popup() { return _popup; }
	void setPopup( Popup* popup ) { _popup = popup; }
	/**********************************************/
	virtual bool popupActivated();
	/**********************************************/
protected slots:
	void adjustPopup();
	/**********************************************/
	void enterEvent( QEnterEvent* event ) override;
	void leaveEvent( QEvent* event ) override;
	void mouseReleaseEvent( QMouseEvent* event ) override;
	void moveEvent( QMoveEvent* event ) override;
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // POPUPBUTTON_H
