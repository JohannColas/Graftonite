#include "TextEditor.h"

#include <QGridLayout>
#include <QComboBox>
#include <QFontComboBox>
#include <QPushButton>
#include <QTextEdit>
#include <QTextCursor>
#include <QTextDocumentWriter>
#include <QTextList>
#include <QColorDialog>
#include <QApplication>

void TextEditor::mergeFormatOnWordOrSelection(const QTextCharFormat& format)
{
	QTextCursor cursor = textEdit->textCursor();
	if (!cursor.hasSelection())
		cursor.select(QTextCursor::WordUnderCursor);
	cursor.mergeCharFormat(format);
	textEdit->mergeCurrentCharFormat(format);
}

void TextEditor::fontChanged(const QFont& font)
{
	comboFont->setCurrentIndex(comboFont->findText(QFontInfo(font).family()));
	comboSize->setCurrentIndex(comboSize->findText(QString::number(font.pointSize())));
//    actionTextBold->setChecked(f.bold());
//    actionTextItalic->setChecked(f.italic());
//    actionTextUnderline->setChecked(f.underline());
}

void TextEditor::colorChanged(const QColor& color)
{
	QPixmap pix(16, 16);
	pix.fill(color);
	pb_TextColor->setIcon(pix);
}

TextEditor::TextEditor( QWidget* parent )
	: QWidget(parent)
{

	comboStyle = new QComboBox;
	comboFont = new QFontComboBox;
	comboSize = new QComboBox;

	textEdit = new QTextEdit;
	connect( textEdit, &QTextEdit::textChanged,
			 this, &TextEditor::onTextChanged );

	pb_TextBold = new QPushButton;
	pb_TextBold->setCheckable(true);
	pb_TextUnderline = new QPushButton;
	pb_TextUnderline->setCheckable(true);
	pb_TextItalic = new QPushButton;
	pb_TextItalic->setCheckable(true);
	pb_TextColor = new QPushButton;
//	pb_UnderlineColor = new QPushButton;


//	textEdit = new QTextEdit(this);
	connect(textEdit, &QTextEdit::currentCharFormatChanged,
			this, &TextEditor::currentCharFormatChanged);
	connect(textEdit, &QTextEdit::cursorPositionChanged,
			this, &TextEditor::cursorPositionChanged);
//	setCentralWidget(textEdit);

//	setToolButtonStyle(Qt::ToolButtonFollowStyle);
//	setupFileActions();
//	setupEditActions();
//	setupTextActions();

//	{
//		QMenu *helpMenu = menuBar()->addMenu(tr("Help"));
//		helpMenu->addAction(tr("About"), this, &TextEdit::about);
//		helpMenu->addAction(tr("About &Qt"), qApp, &QApplication::aboutQt);
//	}

	QFont textFont("Helvetica");
	textFont.setStyleHint(QFont::SansSerif);
	textEdit->setFont(textFont);
	fontChanged(textEdit->font());
	colorChanged(textEdit->textColor());
//	alignmentChanged(textEdit->alignment());

//	auto *document = textEdit->document();
//	connect(document, &QTextDocument::modificationChanged,
//			actionSave, &QAction::setEnabled);
//	connect(document, &QTextDocument::modificationChanged,
//			this, &QWidget::setWindowModified);
//	connect(document, &QTextDocument::undoAvailable,
//			actionUndo, &QAction::setEnabled);
//	connect(document, &QTextDocument::redoAvailable,
//			actionRedo, &QAction::setEnabled);

//	setWindowModified(document->isModified());
//	actionSave->setEnabled(document->isModified());
//	actionUndo->setEnabled(document->isUndoAvailable());
//	actionRedo->setEnabled(document->isRedoAvailable());


	textEdit->setFocus();
//	setCurrentFileName(QString());

	const QIcon boldIcon = QIcon::fromTheme("format-text-bold"/*, QIcon(rsrcPath + "/textbold.png")*/);
//	actionTextBold = menu->addAction(boldIcon, tr("&Bold"), this, &TextEdit::textBold);
	pb_TextBold->setShortcut(Qt::CTRL | Qt::Key_B);
//	pb_TextBold->setPriority(QAction::LowPriority);
	QFont bold;
	bold.setBold(true);
	pb_TextBold->setFont(bold);
//	tb->addAction(actionTextBold);
//	actionTextBold->setCheckable(true);
	connect( pb_TextBold, &QPushButton::released,
			 this, &TextEditor::textBold );

	const QIcon italicIcon = QIcon::fromTheme("format-text-italic"/*, QIcon(rsrcPath + "/textitalic.png")*/);
//	pb_TextItalic = menu->addAction(italicIcon, tr("&Italic"), this, &TextEdit::textItalic);
//	actionTextItalic->setPriority(QAction::LowPriority);
	pb_TextItalic->setShortcut(Qt::CTRL | Qt::Key_I);
	QFont italic;
	italic.setItalic(true);
	pb_TextItalic->setFont(italic);
//	tb->addAction(actionTextItalic);
//	actionTextItalic->setCheckable(true);
	connect( pb_TextItalic, &QPushButton::released,
			 this, &TextEditor::textItalic );

	const QIcon underlineIcon = QIcon::fromTheme("format-text-underline"/*, QIcon(rsrcPath + "/textunder.png")*/);
//	actionTextUnderline = menu->addAction(underlineIcon, tr("&Underline"), this, &TextEdit::textUnderline);
	pb_TextUnderline->setShortcut(Qt::CTRL | Qt::Key_U);
//	actionTextUnderline->setPriority(QAction::LowPriority);
	QFont underline;
	underline.setUnderline(true);
	pb_TextUnderline->setFont(underline);
//	tb->addAction(actionTextUnderline);
	pb_TextUnderline->setCheckable(true);
	connect( pb_TextUnderline, &QPushButton::released,
			 this, &TextEditor::textUnderline );


	connect( pb_TextColor, &QPushButton::released,
			 this, &TextEditor::textColor );

//	menu->addSeparator();

//	const QIcon leftIcon = QIcon::fromTheme("format-justify-left", QIcon(rsrcPath + "/textleft.png"));
//	actionAlignLeft = new QAction(leftIcon, tr("&Left"), this);
//	actionAlignLeft->setShortcut(Qt::CTRL | Qt::Key_L);
//	actionAlignLeft->setCheckable(true);
//	actionAlignLeft->setPriority(QAction::LowPriority);
//	const QIcon centerIcon = QIcon::fromTheme("format-justify-center", QIcon(rsrcPath + "/textcenter.png"));
//	actionAlignCenter = new QAction(centerIcon, tr("C&enter"), this);
//	actionAlignCenter->setShortcut(Qt::CTRL | Qt::Key_E);
//	actionAlignCenter->setCheckable(true);
//	actionAlignCenter->setPriority(QAction::LowPriority);
//	const QIcon rightIcon = QIcon::fromTheme("format-justify-right", QIcon(rsrcPath + "/textright.png"));
//	actionAlignRight = new QAction(rightIcon, tr("&Right"), this);
//	actionAlignRight->setShortcut(Qt::CTRL | Qt::Key_R);
//	actionAlignRight->setCheckable(true);
//	actionAlignRight->setPriority(QAction::LowPriority);
//	const QIcon fillIcon = QIcon::fromTheme("format-justify-fill", QIcon(rsrcPath + "/textjustify.png"));
//	actionAlignJustify = new QAction(fillIcon, tr("&Justify"), this);
//	actionAlignJustify->setShortcut(Qt::CTRL | Qt::Key_J);
//	actionAlignJustify->setCheckable(true);
//	actionAlignJustify->setPriority(QAction::LowPriority);
//	const QIcon indentMoreIcon = QIcon::fromTheme("format-indent-more", QIcon(rsrcPath + "/format-indent-more.png"));
//	actionIndentMore = menu->addAction(indentMoreIcon, tr("&Indent"), this, &TextEdit::indent);
//	actionIndentMore->setShortcut(Qt::CTRL | Qt::Key_BracketRight);
//	actionIndentMore->setPriority(QAction::LowPriority);
//	const QIcon indentLessIcon = QIcon::fromTheme("format-indent-less", QIcon(rsrcPath + "/format-indent-less.png"));
//	actionIndentLess = menu->addAction(indentLessIcon, tr("&Unindent"), this, &TextEdit::unindent);
//	actionIndentLess->setShortcut(Qt::CTRL | Qt::Key_BracketLeft);
//	actionIndentLess->setPriority(QAction::LowPriority);

//	// Make sure the alignLeft is always left of the alignRight
//	QActionGroup *alignGroup = new QActionGroup(this);
//	connect(alignGroup, &QActionGroup::triggered, this, &TextEdit::textAlign);

//	if (QGuiApplication::isLeftToRight()) {
//		alignGroup->addAction(actionAlignLeft);
//		alignGroup->addAction(actionAlignCenter);
//		alignGroup->addAction(actionAlignRight);
//	} else {
//		alignGroup->addAction(actionAlignRight);
//		alignGroup->addAction(actionAlignCenter);
//		alignGroup->addAction(actionAlignLeft);
//	}
//	alignGroup->addAction(actionAlignJustify);

//	tb->addActions(alignGroup->actions());
//	menu->addActions(alignGroup->actions());
//	tb->addAction(actionIndentMore);
//	tb->addAction(actionIndentLess);
//	menu->addAction(actionIndentMore);
//	menu->addAction(actionIndentLess);

//	menu->addSeparator();

	QPixmap pix(16, 16);
	pix.fill(Qt::black);
	pb_TextColor->setIcon( QIcon(pix) );
//	actionTextColor = menu->addAction(pix, tr("&Color..."), this, &TextEdit::textColor);
//	tb->addAction(actionTextColor);

//	const QIcon underlineColorIcon(rsrcPath + "/textundercolor.png");
//	actionUnderlineColor = menu->addAction(underlineColorIcon, tr("Underline color..."), this, &TextEdit::underlineColor);
//	tb->addAction(actionUnderlineColor);

//	menu->addSeparator();

//	const QIcon checkboxIcon = QIcon::fromTheme("status-checkbox-checked", QIcon(rsrcPath + "/checkbox-checked.png"));
//	actionToggleCheckState = menu->addAction(checkboxIcon, tr("Chec&ked"), this, &TextEdit::setChecked);
//	actionToggleCheckState->setShortcut(Qt::CTRL | Qt::Key_K);
//	actionToggleCheckState->setCheckable(true);
//	actionToggleCheckState->setPriority(QAction::LowPriority);
//	tb->addAction(actionToggleCheckState);

//	tb = addToolBar(tr("Format Actions"));
//	tb->setAllowedAreas(Qt::TopToolBarArea | Qt::BottomToolBarArea);
//	addToolBarBreak(Qt::TopToolBarArea);
//	addToolBar(tb);

//	comboStyle = new QComboBox(tb);
//	tb->addWidget(comboStyle);
//	comboStyle->addItems({"Standard",
//						  "Bullet List (Disc)",
//						  "Bullet List (Circle)",
//						  "Bullet List (Square)",
//						  "Task List (Unchecked)",
//						  "Task List (Checked)",
//						  "Ordered List (Decimal)",
//						  "Ordered List (Alpha lower)",
//						  "Ordered List (Alpha upper)",
//						  "Ordered List (Roman lower)",
//						  "Ordered List (Roman upper)",
//						  "Heading 1",
//						  "Heading 2",
//						  "Heading 3",
//						  "Heading 4",
//						  "Heading 5",
//						  "Heading 6"}),

//	connect(comboStyle, &QComboBox::activated, this, &TextEdit::textStyle);

//	comboFont = new QFontComboBox(tb);
//	tb->addWidget(comboFont);
	connect(comboFont, &QComboBox::textActivated, this, &TextEditor::textFamily);

//	comboSize = new QComboBox(tb);
//	comboSize->setObjectName("comboSize");
//	tb->addWidget(comboSize);
	comboSize->setEditable(true);

	const QList<int> standardSizes = QFontDatabase::standardSizes();
	for (int size : standardSizes)
		comboSize->addItem(QString::number(size));
	comboSize->setCurrentIndex(standardSizes.indexOf(QApplication::font().pointSize()));

	connect(comboSize, &QComboBox::textActivated, this, &TextEditor::textSize);



	QGridLayout* lay = new QGridLayout;
	lay->addWidget( comboFont, 0, 0 );
	lay->addWidget( comboSize, 0, 1 );
	lay->addWidget( pb_TextItalic, 0, 2 );
	lay->addWidget( pb_TextBold, 0, 3 );
	lay->addWidget( pb_TextUnderline, 0, 4 );
	lay->addWidget( pb_TextColor, 0, 5 );
//	lay->addWidget( pb_TextUnderline, 0, 4 );
	lay->addWidget( textEdit, 1, 0, 1, 6 );
	setLayout( lay );

}

void TextEditor::setValue( const QVariant& value )
{

}

void TextEditor::setText( const QString& text )
{
	textEdit->setHtml( text );
}

QString TextEditor::text() const
{
	return textEdit->toHtml();
}

void TextEditor::textBold()
{
	QTextCharFormat fmt;
	fmt.setFontWeight(pb_TextBold->isChecked() ? QFont::Bold : QFont::Normal);
	mergeFormatOnWordOrSelection(fmt);
}

void TextEditor::textUnderline()
{
	QTextCharFormat fmt;
	fmt.setFontUnderline(pb_TextUnderline->isChecked());
	mergeFormatOnWordOrSelection(fmt);
}

void TextEditor::textItalic()
{
	QTextCharFormat fmt;
	fmt.setFontItalic(pb_TextItalic->isChecked());
	mergeFormatOnWordOrSelection(fmt);
}

void TextEditor::textFamily(const QString& family)
{
	QTextCharFormat fmt;
	fmt.setFontFamilies({family});
	mergeFormatOnWordOrSelection(fmt);
}

void TextEditor::textSize(const QString& size)
{
	qreal pointSize = size.toFloat();
	if (pointSize > 0) {
		QTextCharFormat fmt;
		fmt.setFontPointSize(pointSize);
		mergeFormatOnWordOrSelection(fmt);
	}
}

void TextEditor::textStyle(int styleIndex)
{
	QTextCursor cursor = textEdit->textCursor();
	QTextListFormat::Style style = QTextListFormat::ListStyleUndefined;
	QTextBlockFormat::MarkerType marker = QTextBlockFormat::MarkerType::NoMarker;

	switch (styleIndex) {
	case 1:
		style = QTextListFormat::ListDisc;
		break;
	case 2:
		style = QTextListFormat::ListCircle;
		break;
	case 3:
		style = QTextListFormat::ListSquare;
		break;
	case 4:
		if (cursor.currentList())
			style = cursor.currentList()->format().style();
		else
			style = QTextListFormat::ListDisc;
		marker = QTextBlockFormat::MarkerType::Unchecked;
		break;
	case 5:
		if (cursor.currentList())
			style = cursor.currentList()->format().style();
		else
			style = QTextListFormat::ListDisc;
		marker = QTextBlockFormat::MarkerType::Checked;
		break;
	case 6:
		style = QTextListFormat::ListDecimal;
		break;
	case 7:
		style = QTextListFormat::ListLowerAlpha;
		break;
	case 8:
		style = QTextListFormat::ListUpperAlpha;
		break;
	case 9:
		style = QTextListFormat::ListLowerRoman;
		break;
	case 10:
		style = QTextListFormat::ListUpperRoman;
		break;
	default:
		break;
	}

	cursor.beginEditBlock();

	QTextBlockFormat blockFmt = cursor.blockFormat();

	if (style == QTextListFormat::ListStyleUndefined) {
		blockFmt.setObjectIndex(-1);
		int headingLevel = styleIndex >= 11 ? styleIndex - 11 + 1 : 0; // H1 to H6, or Standard
		blockFmt.setHeadingLevel(headingLevel);
		cursor.setBlockFormat(blockFmt);

		int sizeAdjustment = headingLevel ? 4 - headingLevel : 0; // H1 to H6: +3 to -2
		QTextCharFormat fmt;
		fmt.setFontWeight(headingLevel ? QFont::Bold : QFont::Normal);
		fmt.setProperty(QTextFormat::FontSizeAdjustment, sizeAdjustment);
		cursor.select(QTextCursor::LineUnderCursor);
		cursor.mergeCharFormat(fmt);
		textEdit->mergeCurrentCharFormat(fmt);
	} else {
		blockFmt.setMarker(marker);
		cursor.setBlockFormat(blockFmt);
		QTextListFormat listFmt;
		if (cursor.currentList()) {
			listFmt = cursor.currentList()->format();
		} else {
			listFmt.setIndent(blockFmt.indent() + 1);
			blockFmt.setIndent(0);
			cursor.setBlockFormat(blockFmt);
		}
		listFmt.setStyle(style);
		cursor.createList(listFmt);
	}

	cursor.endEditBlock();
}

void TextEditor::textColor()
{
	QColor col = QColorDialog::getColor(textEdit->textColor(), this);
	if (!col.isValid())
		return;
	QTextCharFormat fmt;
	fmt.setForeground(col);
	mergeFormatOnWordOrSelection(fmt);
	colorChanged(col);
}

void TextEditor::underlineColor()
{
	QColor col = QColorDialog::getColor(Qt::black, this);
	if (!col.isValid())
		return;
	QTextCharFormat fmt;
	fmt.setUnderlineColor(col);
	mergeFormatOnWordOrSelection(fmt);
	colorChanged(col);
}

void TextEditor::textAlign(QAction* a)
{
//	if (a == actionAlignLeft)
//        textEdit->setAlignment(Qt::AlignLeft | Qt::AlignAbsolute);
//    else if (a == actionAlignCenter)
//        textEdit->setAlignment(Qt::AlignHCenter);
//    else if (a == actionAlignRight)
//        textEdit->setAlignment(Qt::AlignRight | Qt::AlignAbsolute);
//    else if (a == actionAlignJustify)
//        textEdit->setAlignment(Qt::AlignJustify);
}

void TextEditor::setChecked(bool checked)
{
	textStyle(checked ? 5 : 4);
}

void TextEditor::indent()
{
	modifyIndentation(1);
}

void TextEditor::unindent()
{
	modifyIndentation(-1);
}

void TextEditor::modifyIndentation(int amount)
{
	QTextCursor cursor = textEdit->textCursor();
	cursor.beginEditBlock();
	if (cursor.currentList()) {
		QTextListFormat listFmt = cursor.currentList()->format();
		// See whether the line above is the list we want to move this item into,
		// or whether we need a new list.
		QTextCursor above(cursor);
		above.movePosition(QTextCursor::Up);
		if (above.currentList() && listFmt.indent() + amount == above.currentList()->format().indent()) {
			above.currentList()->add(cursor.block());
		} else {
			listFmt.setIndent(listFmt.indent() + amount);
			cursor.createList(listFmt);
		}
	} else {
		QTextBlockFormat blockFmt = cursor.blockFormat();
		blockFmt.setIndent(blockFmt.indent() + amount);
		cursor.setBlockFormat(blockFmt);
	}
	cursor.endEditBlock();
}

void TextEditor::currentCharFormatChanged(const QTextCharFormat& format)
{
	fontChanged(format.font());
	colorChanged(format.foreground().color());
}

void TextEditor::cursorPositionChanged()
{
//	alignmentChanged(textEdit->alignment());
	QTextList *list = textEdit->textCursor().currentList();
	if (list) {
		switch (list->format().style()) {
		case QTextListFormat::ListDisc:
			comboStyle->setCurrentIndex(1);
			break;
		case QTextListFormat::ListCircle:
			comboStyle->setCurrentIndex(2);
			break;
		case QTextListFormat::ListSquare:
			comboStyle->setCurrentIndex(3);
			break;
		case QTextListFormat::ListDecimal:
			comboStyle->setCurrentIndex(6);
			break;
		case QTextListFormat::ListLowerAlpha:
			comboStyle->setCurrentIndex(7);
			break;
		case QTextListFormat::ListUpperAlpha:
			comboStyle->setCurrentIndex(8);
			break;
		case QTextListFormat::ListLowerRoman:
			comboStyle->setCurrentIndex(9);
			break;
		case QTextListFormat::ListUpperRoman:
			comboStyle->setCurrentIndex(10);
			break;
		default:
			comboStyle->setCurrentIndex(-1);
			break;
		}
		switch (textEdit->textCursor().block().blockFormat().marker()) {
		case QTextBlockFormat::MarkerType::NoMarker:
//            actionToggleCheckState->setChecked(false);
			break;
		case QTextBlockFormat::MarkerType::Unchecked:
			comboStyle->setCurrentIndex(4);
//            actionToggleCheckState->setChecked(false);
			break;
		case QTextBlockFormat::MarkerType::Checked:
			comboStyle->setCurrentIndex(5);
//            actionToggleCheckState->setChecked(true);
			break;
		}
	} else {
		int headingLevel = textEdit->textCursor().blockFormat().headingLevel();
		comboStyle->setCurrentIndex(headingLevel ? headingLevel + 10 : 0);
	}
}

void TextEditor::clipboardDataChanged()
{
//#ifndef QT_NO_CLIPBOARD
//    if (const QMimeData *md = QGuiApplication::clipboard()->mimeData())
//        actionPaste->setEnabled(md->hasText());
	//#endif
}

void TextEditor::onTextChanged()
{
	emit textChanged( text() );
}
