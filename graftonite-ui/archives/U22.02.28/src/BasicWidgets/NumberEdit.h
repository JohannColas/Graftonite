#ifndef NUMBEREDIT_H
#define NUMBEREDIT_H
/**********************************************/
#include <QLineEdit>
/**********************************************/
/**********************************************/
/**********************************************/
class NumberEdit
        : public QLineEdit
{
	Q_OBJECT
public:
	enum Mode {
		Integer,
		PositiveInteger,
		Double,
		PositiveDouble,
		Date,
		Time
	};
protected:
	NumberEdit::Mode _mode = NumberEdit::Integer;
	/******************************************/
public:
	NumberEdit( QWidget* parent = nullptr );
	NumberEdit( const NumberEdit::Mode& mode, QWidget* parent = nullptr );
	/******************************************/
	void setMode( const NumberEdit::Mode& mode );
	void setValue( int value );
	void setValue( double value );
//	void setValue( int value );
//	void setValue( int value );
	/******************************************/
protected slots:
	void onEditingFinished();
	/******************************************/
signals:
	void intChanged( int integer );
	void doubleChanged( double integer );
//	void timeChanged( int integer );
//	void dateChanged( int integer );
	/******************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // NUMBEREDIT_H
