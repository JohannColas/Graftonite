#include "PenWidgets.h"
/**********************************************/
#include <QHBoxLayout>
#include <QComboBox>
#include <QLineEdit>
#include <QPainter>
#include <QFormLayout>
#include <QLabel>
/**********************************************/
#include "ColorSelector.h"
#include "NumberEdit.h"
/**********************************************/
/**********************************************/
/**********************************************/
PenModifier::~PenModifier()
{
	delete lb_dash;
	delete lay_dash;
	delete cb_dashStyle;
	delete le_dashPattern;
	delete lb_dashOffset;
	delete ne_dashOffset;
	// Pen Width
	delete lb_width;
	delete ne_width;
	// Pen Color
	delete lb_color;
	delete cs_color;
	// Pen Join
	delete lb_join;
	delete lay_join;
	delete cb_join;
	delete lb_miterlimit;
	delete ne_miterlimit;
	// Pen Cap
	delete lb_cap;
	delete cb_cap;
}
/**********************************************/
/**********************************************/
PenModifier::PenModifier( QWidget* parent )
	: QWidget( parent )
{
	// Pen Dash
	lb_dash = new QLabel("Dash :");
	cb_dashStyle = new QComboBox;
	cb_dashStyle->setIconSize(QSize(30,14));
	cb_dashStyle->setMinimumWidth(80);
	for (int style = Qt::NoPen; style <= Qt::CustomDashLine; ++style)
	{
		QPixmap pix(30,14);
		pix.fill(Qt::white);

		QBrush brush(Qt::black);
		QPen pen(brush,2.5,(Qt::PenStyle)style);

		QPainter painter(&pix);
		painter.setPen(pen);
		painter.drawLine(2,7,28,7);

		QString text;
		switch ((Qt::PenStyle)style)
		{
			case Qt::NoPen:
			{
				text = "None";
				break;
			}
			case Qt::SolidLine:
			{
				text = "Solid";
				break;
			}
			case Qt::DashLine:
			{
				text = "Dash";
				break;
			}
			case Qt::DotLine:
			{
				text = "Dot";
				break;
			}
			case Qt::DashDotLine:
			{
				text = "Dash-Dot";
				break;
			}
			case Qt::DashDotDotLine:
			{
				text = "Dash-Dot-Dot";
				break;
			}
			case Qt::CustomDashLine:
			{
				text = "Custom";
				break;
			}
			default:
			break;
		}
		cb_dashStyle->addItem(QIcon(pix),text);
	}
	cb_dashStyle->setCurrentIndex( 0 );
	le_dashPattern = new QLineEdit;
	le_dashPattern->hide();
	lay_dash = new QHBoxLayout;
	lay_dash->setContentsMargins(0,0,0,0);
	lay_dash->addWidget( cb_dashStyle );
	lay_dash->addWidget( le_dashPattern );
	lb_dashOffset = new QLabel("Dash Offset :");
	lb_dashOffset->hide();
	ne_dashOffset = new NumberEdit(NumberEdit::Double);
	ne_dashOffset->hide();
	lay_dashOffset = new QFormLayout;
	lay_dashOffset->setContentsMargins(0,0,0,0);
	lay_dashOffset->setFieldGrowthPolicy( QFormLayout::FieldsStayAtSizeHint );
	lay_dashOffset->setLabelAlignment( Qt::AlignRight | Qt::AlignCenter );
	lay_dashOffset->addRow( lb_dashOffset, ne_dashOffset );
	connect( cb_dashStyle, &QComboBox::currentIndexChanged,
			 this, &PenModifier::onDashStyleIndexChanged );
	connect( cb_dashStyle, &QComboBox::currentIndexChanged,
			 this, &PenModifier::onDashPatternChanged );
	connect( le_dashPattern, &QLineEdit::editingFinished,
			 this, &PenModifier::onDashPatternChanged );
	connect( ne_dashOffset, &NumberEdit::doubleChanged,
			 this, &PenModifier::onDashOffsetChanged );
	// Pen Width
	lb_width = new QLabel("Width :");
	ne_width = new NumberEdit(NumberEdit::Double);
	connect( ne_width, &NumberEdit::doubleChanged,
			 this, &PenModifier::onWidthChanged );
	// Pen Color
	lb_color = new QLabel( "Color :");
	cs_color = new ColorSelector;
	connect( cs_color, &ColorSelector::colorChanged,
			 this, &PenModifier::onColorChanged );
	// Pen Join
	lb_join = new QLabel("Join :");
	cb_join = new QComboBox;
	cb_join->addItems( {"Bevel", "Miter", "Round"} );
	connect( cb_join, &QComboBox::currentIndexChanged,
			 this, &PenModifier::onJoinIndexChanged );
	lb_miterlimit = new QLabel("Miter limit :");
	ne_miterlimit = new NumberEdit(NumberEdit::Double);
	lb_miterlimit->hide();
	ne_miterlimit->hide();
	connect( ne_miterlimit, &NumberEdit::doubleChanged,
			 this, &PenModifier::onMiterLimitChanged );
	// Pen Cap
	lb_cap = new QLabel("Cap :");
	cb_cap = new QComboBox;
	cb_cap->addItems( {"Flat", "Square", "Round"} );
	connect( cb_cap, &QComboBox::currentIndexChanged,
			 this, &PenModifier::onCapIndexChanged );
	//
	lay_main = new QFormLayout;
	lay_main->setFieldGrowthPolicy( QFormLayout::FieldsStayAtSizeHint );
	lay_main->setLabelAlignment( Qt::AlignRight | Qt::AlignCenter );

	updateLayout();

	setLayout( lay_main );

}
/**********************************************/
/**********************************************/
PenModifier::PenModifier( const QPen& pen, QWidget* parent )
	: PenModifier(parent)
{

}
/**********************************************/
/**********************************************/
QPen PenModifier::pen() const
{
	return _pen;
}
/**********************************************/
/**********************************************/
void PenModifier::setPen( const QPen& pen )
{
	_pen = pen;

	switch (_pen.style())
	{
		case Qt::SolidLine:
		{
			cb_dashStyle->setCurrentIndex( 1 );
			break;
		}
		case Qt::DashLine:
		{
			cb_dashStyle->setCurrentIndex( 2 );
			break;
		}
		case Qt::DotLine:
		{
			cb_dashStyle->setCurrentIndex( 3 );
			break;
		}
		case Qt::DashDotLine:
		{
			cb_dashStyle->setCurrentIndex( 4 );
			break;
		}
		case Qt::DashDotDotLine:
		{
			cb_dashStyle->setCurrentIndex( 5 );
			break;
		}
		case Qt::CustomDashLine:
		{
			cb_dashStyle->setCurrentIndex( 6 );
			break;
		}
		default:
			cb_dashStyle->setCurrentIndex( 0 );
		break;
	}
	if ( _pen.style() == Qt::CustomDashLine )
	{
		le_dashPattern->show();
		QString pattern;
		for ( const qreal& dash : _pen.dashPattern() )
			pattern += QString::number(dash) + ";";
		le_dashPattern->blockSignals(true);
		le_dashPattern->setText( pattern );
		le_dashPattern->blockSignals(false);
	}
	else
		le_dashPattern->hide();
	if ( _pen.style() == Qt::NoPen ||
		 _pen.style() == Qt::SolidLine )
		_isDashed = false;
	else
		_isDashed = true;


	ne_dashOffset->blockSignals(true);
	ne_dashOffset->setValue( _pen.dashOffset() );
	ne_dashOffset->blockSignals(false);

	ne_width->blockSignals(true);
	ne_width->setValue( _pen.widthF() );
	ne_width->blockSignals(false);

	cs_color->blockSignals(true);
	cs_color->setColor( _pen.color() );
	cs_color->blockSignals(false);

	cb_join->blockSignals(true);
	if ( _pen.joinStyle() == Qt::BevelJoin )
		cb_join->setCurrentIndex( 0 );
	else if ( _pen.joinStyle() == Qt::MiterJoin )
		cb_join->setCurrentIndex( 1 );
	else if ( _pen.joinStyle() == Qt::RoundJoin )
		cb_join->setCurrentIndex( 2 );
	cb_join->blockSignals(false);
	ne_miterlimit->blockSignals(true);
	ne_miterlimit->setValue( _pen.miterLimit() );
	ne_miterlimit->blockSignals(false);

	cb_cap->blockSignals(true);
	if ( _pen.capStyle() == Qt::FlatCap )
		cb_cap->setCurrentIndex( 0 );
	else if ( _pen.capStyle() == Qt::SquareCap )
		cb_cap->setCurrentIndex( 1 );
	else if ( _pen.capStyle() == Qt::RoundCap )
		cb_cap->setCurrentIndex( 2 );
	cb_cap->blockSignals(false);
}
/**********************************************/
/**********************************************/
void PenModifier::activateDashOffset()
{
	_hasDashOffset = true;
	updateLayout();
}
void PenModifier::desactivateDashOffset()
{
	_hasDashOffset = false;
	updateLayout();
}
void PenModifier::activateJoin()
{
	_hasJoin = true;
	updateLayout();
}
void PenModifier::desactivateJoin()
{
	_hasJoin = false;
	updateLayout();
}
void PenModifier::activateCap()
{
	_hasCap = true;
	updateLayout();
}
void PenModifier::desactivateCap()
{
	_hasCap = false;
	updateLayout();
}
/**********************************************/
/**********************************************/
//#include "Popup.h"
//#include "PopupButton.h"
void PenModifier::activatePopupMode()
{

}
void PenModifier::desactivatePopupMode()
{
//	lay_ = new QHBoxLayout;
//	pb_popup = new PopupButton;
//	wid_popup = new Popup;


}
/**********************************************/
/**********************************************/
void PenModifier::updateLayout()
{
	for ( int it = lay_main->count()-1; it > -1 ; --it )
		lay_main->takeAt( it );

	lay_main->addRow( lb_dash, lay_dash );
	if ( _isDashed && _hasDashOffset )
	{
		lay_main->addRow( "", lay_dashOffset );
		lb_dashOffset->show();
		ne_dashOffset->show();
	}
	else
	{
		lb_dashOffset->hide();
		ne_dashOffset->hide();
	}
	lay_main->addRow( lb_width, ne_width );
	lay_main->addRow( lb_color, cs_color );
	if ( _hasJoin )
	{
		lay_main->addRow( lb_join, cb_join );
		lb_join->show();
		cb_join->show();
	}
	else
	{
		lb_join->hide();
		cb_join->hide();
	}
	if ( _hasJoin && cb_join->currentIndex() == 1 )
	{
		lay_main->addRow( lb_miterlimit, ne_miterlimit );
		lb_miterlimit->show();
		ne_miterlimit->show();
	}
	else
	{
		lb_miterlimit->hide();
		ne_miterlimit->hide();
	}
	if ( _hasCap )
	{
		lay_main->addRow( lb_cap, cb_cap );
		lb_cap->show();
		cb_cap->show();
	}
	else
	{
		lb_cap->hide();
		cb_cap->hide();
	}
}
/**********************************************/
/**********************************************/
void PenModifier::onDashStyleIndexChanged( int index )
{
	switch ( index )
	{
		case 1:
		{
			_pen.setStyle( Qt::SolidLine );
			break;
		}
		case 2:
		{
			_pen.setStyle( Qt::DashLine );
			break;
		}
		case 3:
		{
			_pen.setStyle( Qt::DotLine );
			break;
		}
		case 4:
		{
			_pen.setStyle( Qt::DashDotLine );
			break;
		}
		case 5:
		{
			_pen.setStyle( Qt::DashDotDotLine );
			break;
		}
		case 6:
		{
			_pen.setStyle( Qt::CustomDashLine );
			break;
		}
		default:
		{
			_pen.setStyle( Qt::NoPen );
			break;
		}
	}
	if ( _pen.style() == Qt::CustomDashLine )
	{
		le_dashPattern->show();
		QString pattern;
		for ( const qreal& dash : _pen.dashPattern() )
			pattern += QString::number(dash) + ";";
		le_dashPattern->blockSignals(true);
		le_dashPattern->setText( pattern );
		le_dashPattern->blockSignals(false);
	}
	else
		le_dashPattern->hide();

	if ( _pen.style() == Qt::NoPen ||
		 _pen.style() == Qt::SolidLine )
		_isDashed = false;
	else
		_isDashed = true;

	updateLayout();

	emit penChanged( _pen );
}
/**********************************************/
/**********************************************/
void PenModifier::onDashPatternChanged()
{
	if ( _pen.style() != Qt::CustomDashLine )
		return;

	QStringList lst = le_dashPattern->text().split(';');
	if ( lst.size() % 2 != 0 )
		return;

	QList<qreal> dashPattern;
	double width = 0;
	for ( int it = 0; it < lst.size(); ++it )
	{
		double dash =  lst.at(it).toDouble();
		width += dash;
		dashPattern << dash;
	}
	if ( width < 26 )
		width = 26;
	int marg = width/13;
	QPixmap pix(width+2*marg,14);
	pix.fill(Qt::white);
	QBrush brush(Qt::black);
	QPen pen(brush,2.5,Qt::CustomDashLine);
	pen.setDashPattern( dashPattern );

	QPainter painter(&pix);
	painter.setPen(pen);
	painter.drawLine(marg,7,width,7);
	cb_dashStyle->setItemIcon(6,QIcon(pix.scaled(30,14, Qt::IgnoreAspectRatio,Qt::SmoothTransformation)));

	_pen.setDashPattern(dashPattern);

	emit penChanged( _pen );
}
/**********************************************/
/**********************************************/
void PenModifier::onDashOffsetChanged( double offset )
{
	_pen.setDashOffset( offset );

	emit penChanged( _pen );
}
/**********************************************/
/**********************************************/
void PenModifier::onWidthChanged( double width )
{
	_pen.setWidthF( width );

	emit penChanged( _pen );
}
/**********************************************/
/**********************************************/
void PenModifier::onColorChanged( const QColor& color )
{
	_pen.setColor( color );

	emit penChanged( _pen );
}
/**********************************************/
/**********************************************/
void PenModifier::onJoinIndexChanged( int index )
{
	if ( index == 0 )
		_pen.setJoinStyle( Qt::BevelJoin );
	else if ( index == 1 )
		_pen.setJoinStyle( Qt::MiterJoin );
	else if ( index == 2 )
		_pen.setJoinStyle( Qt::RoundJoin );

	updateLayout();

	emit penChanged( _pen );
}
/**********************************************/
/**********************************************/
void PenModifier::onMiterLimitChanged( double miterlimit )
{
	_pen.setMiterLimit( miterlimit );

	emit penChanged( _pen );
}
/**********************************************/
/**********************************************/
void PenModifier::onCapIndexChanged( int index )
{
	if ( index == 0 )
		_pen.setCapStyle( Qt::FlatCap );
	else if ( index == 1 )
		_pen.setCapStyle( Qt::SquareCap );
	else if ( index == 2 )
		_pen.setCapStyle( Qt::RoundCap );

	emit penChanged( _pen );
}
/**********************************************/
/**********************************************/
