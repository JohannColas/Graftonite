#include "Modifier.h"
/**********************************************/
#include <QLabel>
#include <QPushButton>
#include <QCheckBox>
#include <QLineEdit>
#include <QComboBox>
/**********************************************/
#include "NumberEdit.h"
#include "ComboBox2.h"
#include "ColorSelector.h"
#include "PenWidgets.h"
#include "TextEditor.h"
/**********************************************/
/**********************************************/
/**********************************************/
Modifier::~Modifier()
{
	if ( wid_label )
		delete wid_label;
	if ( wid_modifier )
		delete wid_modifier;
}
/**********************************************/
Modifier::Modifier( QObject* parent )
	: QObject(parent)
{

}
/**********************************************/
Modifier::Modifier( const Key& key, const Type& type, QObject* parent )
	: Modifier(parent)
{
	_type = type;
	switch (_type)
	{
		case Modifier::Section:
		{
			break;
		}
		case Modifier::PushButton:
		{
			QPushButton* pb = new QPushButton;
			wid_modifier = pb;
			break;
		}
		case Modifier::CheckBox:
		{
			QCheckBox* cb = new QCheckBox;
			connect( cb, &QCheckBox::toggled,
					 this, &Modifier::onBoolChanged );
			wid_modifier = cb;
			break;
		}
		case Modifier::LineEdit:
		{
			QLineEdit* le = new QLineEdit;
			connect( le, &QLineEdit::editingFinished,
					 this, &Modifier::onEditingFinished );
			wid_modifier = le;
			break;
		}
		case Modifier::Integer:
		{
			NumberEdit* sp = new NumberEdit(NumberEdit::Integer);
			connect( sp, &NumberEdit::intChanged,
					 this, &Modifier::onIntChanged );
			wid_modifier = sp;
			break;
		}
		case Modifier::PositiveInteger:
		{
			NumberEdit* sp = new NumberEdit(NumberEdit::PositiveInteger);
			connect( sp, &NumberEdit::intChanged,
					 this, &Modifier::onIntChanged );
			wid_modifier = sp;
			break;
		}
		case Modifier::Double:
		{
			NumberEdit* sp = new NumberEdit(NumberEdit::Double);
			connect( sp, &NumberEdit::doubleChanged,
					 this, &Modifier::onDoubleChanged );
			wid_modifier = sp;
			break;
		}
		case Modifier::PositiveDouble:
		{
			NumberEdit* sp = new NumberEdit(NumberEdit::PositiveDouble);
			connect( sp, &NumberEdit::doubleChanged,
					 this, &Modifier::onDoubleChanged );
			wid_modifier = sp;
			break;
		}
		case Modifier::ComboBox:
		{
			QComboBox* cb = new QComboBox;
			connect( cb, &QComboBox::currentTextChanged,
					 this, &Modifier::onTextChanged );
			wid_modifier = cb;
			break;
		}
		case Modifier::Anchor:
		{
			ComboBox2* ks = new class ComboBox2;
			ks->setData( {{"Top-Left", Key::TopLeft},
						  {"Top", Key::Top},
						  {"Top-Right", Key::TopRight},
						  {"Left", Key::Left},
						  {"Center", Key::Center},
						  {"Right", Key::Right},
						  {"Bottom-Left", Key::BottomLeft},
						  {"Bottom", Key::Bottom},
						  {"Bottom-Right", Key::BottomRight}} );
			connect( ks, &ComboBox2::valueChanged,
					 this, &Modifier::onValueChanged );
			wid_modifier = ks;
			break;
		}
		case Modifier::KeySelector:
		{
			ComboBox2* ks = new ComboBox2;
			connect( ks, &ComboBox2::valueChanged,
					 this, &Modifier::onValueChanged );
			wid_modifier = ks;
			break;
		}
		case Modifier::Color:
		{
			ColorSelector* cs = new ColorSelector;
			connect( cs, &ColorSelector::colorChanged,
					 this, &Modifier::onColorChanged );
			wid_modifier = cs;
			break;
		}
		case Modifier::PenEditor:
		{
			PenModifier* pen_mod = new PenModifier;
			connect( pen_mod, &PenModifier::penChanged,
					 this, &Modifier::onPenChanged );
			wid_modifier = pen_mod;
			break;
		}
		case Modifier::TextEdit:
		{
			TextEditor* te = new TextEditor;
			connect( te, &TextEditor::textChanged,
					 this, &Modifier::onTextChanged );
			wid_modifier = te;
			break;
		}
		default:
		{
			break;
		}
	}
}
/**********************************************/
Modifier::Modifier( const QString& label, const Key& key, const Type& type, QObject* parent )
	: Modifier(key,type,parent)
{
	wid_label = new QLabel( label );
}
/**********************************************/
/**********************************************/
Key Modifier::setting() const
{
	return _setting;
}
/**********************************************/
Modifier::Type Modifier::type()
{
	return _type;
}
/**********************************************/
void Modifier::setTitle( const QString& title )
{
	wid_label->setText( title );
}
/**********************************************/
QVariant Modifier::value() const
{
	switch (_type)
	{
		case Modifier::Section:
		{
			return wid_label->text();
		}
		case Modifier::PushButton:
		{
			return static_cast<QPushButton*>(wid_modifier)->text();
		}
		case Modifier::CheckBox:
		{
			return static_cast<QCheckBox*>(wid_modifier)->text();
		}
		case Modifier::LineEdit:
		{
			return static_cast<QLineEdit*>(wid_modifier)->text();
		}
		case Modifier::Integer:
		{
			return static_cast<NumberEdit*>(wid_modifier)->text();
		}
		case Modifier::PositiveInteger:
		{
			return static_cast<NumberEdit*>(wid_modifier)->text();
		}
		case Modifier::Double:
		{
			return static_cast<NumberEdit*>(wid_modifier)->text();
		}
		case Modifier::PositiveDouble:
		{
			return static_cast<NumberEdit*>(wid_modifier)->text();
		}
		case Modifier::ComboBox:
		{
			return static_cast<QComboBox*>(wid_modifier)->currentText();
		}
		case Modifier::Anchor:
		{
			return static_cast<ComboBox2*>(wid_modifier)->currentValue();
		}
		case Modifier::KeySelector:
		{
			return static_cast<ComboBox2*>(wid_modifier)->currentValue();
		}
		case Modifier::Color:
		{
			return static_cast<ColorSelector*>(wid_modifier)->color();
		}
		case Modifier::PenEditor:
		{
			return static_cast<PenModifier*>(wid_modifier)->pen();
		}
		case Modifier::TextEdit:
		{
			return static_cast<TextEditor*>(wid_modifier)->text();
		}
		default:
		{
			return QVariant();
		}
	}
}
/**********************************************/
/**********************************************/
void Modifier::setValue( const QVariant& value )
{
	switch (_type)
	{
		case Modifier::PushButton:
		{
			static_cast<QPushButton*>(wid_modifier)->setText( value.toString() );
			break;
		}
		case Modifier::CheckBox:
		{
			static_cast<QCheckBox*>(wid_modifier)->setChecked( value.toBool() );
			break;
		}
		case Modifier::LineEdit:
		{
			static_cast<QLineEdit*>(wid_modifier)->setText( value.toString() );
			break;
		}
		case Modifier::Integer:
		{
			static_cast<NumberEdit*>(wid_modifier)->setValue( value.toInt() );
			break;
		}
		case Modifier::PositiveInteger:
		{
			static_cast<NumberEdit*>(wid_modifier)->setValue( value.toInt() );
			break;
		}
		case Modifier::Double:
		{
			static_cast<NumberEdit*>(wid_modifier)->setValue( value.toDouble() );
			break;
		}
		case Modifier::PositiveDouble:
		{
			static_cast<NumberEdit*>(wid_modifier)->setValue( value.toDouble() );
			break;
		}
		case Modifier::ComboBox:
		{
			static_cast<QComboBox*>(wid_modifier)->setCurrentText( value.toString() );
			break;
		}
		case Modifier::Anchor:
		{
			static_cast<ComboBox2*>(wid_modifier)->setCurrentValue( value );
			break;
		}
		case Modifier::KeySelector:
		{
			static_cast<ComboBox2*>(wid_modifier)->setCurrentValue( value );
			break;
		}
		case Modifier::Color:
		{
			static_cast<ColorSelector*>(wid_modifier)->setColor( value.toString() );
			break;
		}
		case Modifier::PenEditor:
		{
			static_cast<PenModifier*>(wid_modifier)->setPen( value.value<QPen>() );
			break;
		}
		case Modifier::TextEdit:
		{
			static_cast<TextEditor*>(wid_modifier)->setText( value.toString() );
			break;
		}
		default:
		break;
	}
}
/**********************************************/
/**********************************************/
QLabel* Modifier::title() const
{
	return wid_label;
}
/**********************************************/
QWidget* Modifier::modifier() const
{
	return wid_modifier;
}
/**********************************************/
/**********************************************/
void Modifier::onEditingFinished()
{
	if ( _type == Modifier::LineEdit )
	{
		QLineEdit* le = static_cast<QLineEdit*>(wid_modifier);
		emit settingChanged( _setting, le->text() );
	}
}
void Modifier::onValueChanged( const QVariant& value )
{
	emit settingChanged( _setting, value );
}

void Modifier::onKeyChanged( const Key& value )
{
	emit settingChanged( _setting, value.toString() );
}
/**********************************************/
void Modifier::onTextChanged( const QString& text )
{
	emit settingChanged( _setting, text );
}
/**********************************************/
void Modifier::onBoolChanged( bool value )
{
	emit settingChanged( _setting, value );
}
/**********************************************/
void Modifier::onIntChanged( int value )
{
	emit settingChanged( _setting, value );
}
/**********************************************/
void Modifier::onDoubleChanged( double value )
{
	emit settingChanged( _setting, value );
}
/**********************************************/
void Modifier::onColorChanged( const QColor& color )
{
	emit settingChanged( _setting, color );
}
/**********************************************/
void Modifier::onPenChanged( const QPen& pen )
{
	emit settingChanged( _setting, pen );
}
/**********************************************/
/**********************************************/
void Modifier::hide()
{

}
void Modifier::show()
{

}
/**********************************************/
/**********************************************/
