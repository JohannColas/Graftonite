#ifndef GRAPHPAINTER_H
#define GRAPHPAINTER_H
/**********************************************/
class QPainter;
class Rect;
class Line;
/**********************************************/
/**********************************************/
/**********************************************/
/**********************************************/
class GraphPainter
{
	static inline QPainter* _painter = nullptr;
public:
	GraphPainter();
	static void init( QPainter* painter );
	static Rect boundingRect( const Line& line );

//	static Rect drawLine( const Line& line, const LineStyle& style );
};
/**********************************************/
/**********************************************/
/**********************************************/
/**********************************************/
#endif // GRAPHPAINTER_H
