#include "ComboBox.h"
/**********************************************/
#include <QBoxLayout>
#include <QListWidget>
//#include <QListWidgetItem>
/**********************************************/
/**********************************************/
ComboBoxPopup::ComboBoxPopup( const QStringList& items, QWidget* parent )
	: Popup( parent )
{
	QHBoxLayout* lay_main = new QHBoxLayout;
//	lay_main->setMargin( 0 );
	lay_main->setContentsMargins(0,0,0,0);
	wid_list = new QListWidget;
	wid_list->setFrameShape( QFrame::StyledPanel );
	wid_list->setItemAlignment( Qt::AlignCenter );
	wid_list->setSpacing( 3 );
	lay_main->addWidget( wid_list );
	setLayout( lay_main );

	QColor col = palette().highlight().color();
	QString str_col = QString::number(col.red()) + "," + QString::number(col.green()) + "," + QString::number(col.blue());
	QColor col2 = palette().highlightedText().color();
	QString str_col2 = QString::number(col2.red()) + "," + QString::number(col2.green()) + "," + QString::number(col2.blue());
	wid_list->setStyleSheet("QListWidget::item\n{padding:5px;}\n"
"QListWidget::item:hover\n{background-color:rgb("+str_col+");color:rgb("+str_col2+");}");//"QListWidget::item:selected\n{background-color:rgb(255,0,50)}

	_items = items;
	updateItems();

	connect( wid_list, &QListWidget::itemPressed,
			 this, &ComboBoxPopup::on_itemPressed );
}
/**********************************************/
/**********************************************/
void ComboBoxPopup::updateItems()
{
	if ( !wid_list ) return;
	wid_list->clear();
	for ( QString item : _items )
	{
		QListWidgetItem* lst_item = new QListWidgetItem( item, wid_list );
		lst_item->setTextAlignment( Qt::AlignCenter );
		wid_list->addItem( lst_item );
	}
	int fixedH = wid_list->count()*wid_list->sizeHintForRow(0) +
				 (wid_list->count()+2)*wid_list->spacing() + 2*wid_list->frameWidth();
	if ( fixedH > 450 ) fixedH = 450;
	setFixedSize( wid_list->sizeHintForColumn(0) + 2*(wid_list->frameWidth()+wid_list->spacing())+25,
				  fixedH );
	setCurrentItem( _currentitem );
}
/**********************************************/
/**********************************************/
void ComboBoxPopup::setCurrentItem( const QString& current_item )
{
	_currentitem = current_item;
	for ( int it = 0; it < wid_list->count(); ++it )
	{
		QListWidgetItem* lst_item = wid_list->item(it);
		if ( lst_item->text() == current_item )
			wid_list->setCurrentItem( lst_item );
	}
}
/**********************************************/
/**********************************************/
void ComboBoxPopup::on_itemPressed( QListWidgetItem* item )
{
	if ( !item ) return;
	wid_list->setCurrentItem( item );
	emit currentLabelChanged( wid_list->currentItem()->text() );
	setVisible( false );
}
/**********************************************/
/**********************************************/
ComboBox::ComboBox( QWidget* parent )
	: PopupButton( parent )
{

	_showPopupOnEnterEvent = true;
	_popup = new ComboBoxPopup( _items, this );
	connect( _popup, &ComboBoxPopup::closed,
			 this, &ComboBox::popupLeaved );
	connect( static_cast<ComboBoxPopup*>(_popup), &ComboBoxPopup::currentLabelChanged,
			 this, &ComboBox::setItem );
}
/**********************************************/
/**********************************************/
ComboBox::ComboBox( const QStringList& items, QWidget* parent )
	: PopupButton( parent )
{
	_items = items;
	_showPopupOnEnterEvent = true;

	_popup = new ComboBoxPopup( _items, this );
	connect( _popup, &ComboBoxPopup::closed,
			 this, &ComboBox::popupLeaved );
	connect( static_cast<ComboBoxPopup*>(_popup), &ComboBoxPopup::currentLabelChanged,
			 this, &ComboBox::setItem );

	if ( _items.size() > 0 )
		setCurrentItem(_items.first());
}
/**********************************************/
/**********************************************/
bool ComboBox::childIsInative()
{
	return true;
}
/**********************************************/
/**********************************************/
void ComboBox::addItems( const QStringList& items )
{
	_items.append( items );
	static_cast<ComboBoxPopup*>(_popup)->updateItems();
}
/**********************************************/
/**********************************************/
void ComboBox::setCurrentItem( const QString& item )
{
	for ( QString av_item : _items )
	{
		if ( av_item == item )
		{
			setText( item );
			static_cast<ComboBoxPopup*>(_popup)->setCurrentItem( item );
		}
	}
}
/**********************************************/
/**********************************************/
QString ComboBox::currentItem()
{
	return text();
}
/**********************************************/
/**********************************************/
void ComboBox::setItem( const QString& item )
{
	setText( item );
	static_cast<ComboBoxPopup*>(_popup)->setCurrentItem( item );
}
/**********************************************/
/**********************************************/
