#ifndef COLORSELECTOR_H
#define COLORSELECTOR_H
/**********************************************/
#include <QPushButton>
/**********************************************/
/**********************************************/
/**********************************************/
class ColorSelector
        : public QPushButton
{
	Q_OBJECT
private:
	QColor _cur_color = {0,0,0,0};
public:
	ColorSelector( QWidget* parent = nullptr );
	/******************************************/
	QColor color() const;
	void setColor( const QColor& color );
	void setColor( const QString& color );
	/******************************************/
private slots:
	void onReleasedMouse();
	void paintEvent( QPaintEvent* event ) override;
	/******************************************/
signals:
	void colorChanged( const QColor& color );
	void colorStringChanged( const QString& color );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // COLORSELECTOR_H
