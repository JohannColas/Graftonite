#include "ComboBox2.h"
/**********************************************/
/**********************************************/
ComboBox2::ComboBox2( QWidget* parent )
	: QComboBox(parent)
{
	init();
}
/**********************************************/
/**********************************************/
ComboBox2::ComboBox2( const QList<QPair<QString,QVariant>>& data, QWidget* parent )
	: QComboBox(parent)
{
	setData( data );
	init();
}
/**********************************************/
/**********************************************/
void ComboBox2::init()
{
	connect( this, &ComboBox2::currentIndexChanged,
			 this, &ComboBox2::onIndexChanged );
}
/**********************************************/
/**********************************************/
void ComboBox2::setData( const QList<QPair<QString,QVariant>>& data )
{
	this->clear();
	_values.clear();
	for ( const QPair<QString,QVariant>& pair : data )
	{
		this->addItem( pair.first );
		_values.append( pair.second );
	}
	onIndexChanged( 0 );
}
/**********************************************/
/**********************************************/
void ComboBox2::setCurrentValue( const QVariant& value )
{
//	this->blockSignals(true);
	for ( int it = 0; it < _values.size(); ++it )
	{
		if ( _values.at(it) == value )
		{
			setCurrentIndex( it );
			break;
		}
	}
//	this->blockSignals(false);
}
/**********************************************/
/**********************************************/
QVariant ComboBox2::currentValue() const
{
	if ( -1 < currentIndex() && currentIndex() < _values.size() )
		return _values.at(currentIndex());
	return QVariant();
}
/**********************************************/
/**********************************************/
void ComboBox2::onIndexChanged( int index )
{
	if ( -1 < index && index < _values.size() )
		emit valueChanged( _values.at(index) );
}
/**********************************************/
/**********************************************/
KeySelector::KeySelector( QWidget* parent )
	: QComboBox(parent)
{
	init();
}
/**********************************************/
/**********************************************/
KeySelector::KeySelector( const QList<QPair<QString,Key>>& data, QWidget* parent )
	: QComboBox(parent)
{
	setData( data );
	init();
}
/**********************************************/
/**********************************************/
void KeySelector::init()
{
	connect( this, &KeySelector::currentIndexChanged,
			 this, &KeySelector::onIndexChanged );
}
/**********************************************/
/**********************************************/
void KeySelector::setData( const QList<QPair<QString,Key>>& data )
{
	this->clear();
	_keys.clear();
	for ( const QPair<QString,Key>& pair : data )
	{
		this->addItem( pair.first );
		_keys.append( pair.second );
	}
//	if ( count() > 0 )
//		onIndexChanged( 0 );
}
/**********************************************/
/**********************************************/
void KeySelector::setCurrentKey( const Key& key )
{
//	this->blockSignals(true);
	for ( int it = 0; it < _keys.size(); ++it )
	{
		if ( _keys.at(it) == key )
		{
			setCurrentIndex( it );
			break;
		}
	}
//	this->blockSignals(false);
}
/**********************************************/
/**********************************************/
Key KeySelector::currentKey() const
{
	if ( -1 < currentIndex() && currentIndex() < _keys.size() )
		return _keys.at(currentIndex());
	return Key::Unknown;
}
/**********************************************/
/**********************************************/
void KeySelector::onIndexChanged( int index )
{
	if ( -1 < index && index < _keys.size() )
		emit keyChanged( _keys.at(index) );
}
/**********************************************/
/**********************************************/
