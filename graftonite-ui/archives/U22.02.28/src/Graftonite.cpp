#include "Graftonite.h"
/*------------------------------*/
#include <QApplication>
#include <QPushButton>
#include <QSplitter>
#include <QStackedWidget>
#include <QVBoxLayout>
/**********************************************/
//
#include "Project/ProjectItem.h"
#include "Project/ProjectTree.h"
//
#include "Welcome/WelcomeFrame.h"
#include "Settings/SettingsFrame.h"
//
#include "Data/DataEditor.h"
#include "Graph/GraphEditor.h"
#include "BasicWidgets/ImageViewer.h"
/**********************************************/
#include "Commons/Key.h"
#include "Commons/App.h"
#include "Commons/Files.h"
#include "Project/Project.h"
/**********************************************/
#include <QDebug>
/**********************************************/
/**********************************************/
/**********************************************/
Graftonite::Graftonite( QWidget* parent ) :
    QWidget(parent)
{

	setWindowTitle( "Graftonite" );
	setWindowIcon( QIcon("icons/Dark Theme/graftonite.svg") );

	qApp->setStyleSheet( qApp->styleSheet()+
						 ".SidebarButton{text-align:left;font-weight:bold;icon-size:30px;}"+
						 ".SettingsSection{font: bold 12px;}" );

	_layout = new QVBoxLayout( this );

	pb_welcome = new QPushButton( "Welcome", this );
	pb_welcome->setProperty( "class", "SidebarButton" );
	pb_welcome->setObjectName( "pb_welcome" );
	pb_welcome->setFlat( true );
	pb_welcome->setIcon( QIcon::fromTheme( "welcome", QIcon("icons/Dark Theme/welcome.svg")) );
	connect( pb_welcome, &QPushButton::released,
	         this, &Graftonite::on_pb_welcome_released );
	_layout->addWidget( pb_welcome );

	pb_project_explorer = new QPushButton( "Project Explorer", this );
	pb_project_explorer->setProperty( "class", "SidebarButton" );
	pb_project_explorer->setObjectName( "pb_project_explorer" );
	pb_project_explorer->setFlat( true );
	pb_project_explorer->setIcon( QIcon::fromTheme( "project_explorer", QIcon("icons/Dark Theme/project_explorer.svg")) );
	_layout->addWidget( pb_project_explorer );

	tree_project = new ProjectTreeView( this );
	tree_project->setProperty( "class", "ProjectTree" );
	tree_project->setObjectName( "tree_project" );
	tree_project->openProject( "#project-exemple/project.gpj" );
	connect( tree_project, &ProjectTreeView::itemPressed,
			 this, &Graftonite::onProjectItemChanged );


	_layout->addWidget( tree_project );

	pb_settings = new QPushButton( "Settings", this );
	pb_settings->setProperty( "class", "SidebarButton" );
	pb_settings->setObjectName( "pb_settings" );
	pb_settings->setFlat( true );
	pb_settings->setIcon( QIcon::fromTheme( "settings", QIcon("icons/Dark Theme/settings.svg")) );
	connect( pb_settings, &QPushButton::released,
	         this, &Graftonite::on_pb_settings_released );
	_layout->addWidget( pb_settings );

	cont_main = new QStackedWidget(this);
	cont_main->show();

	wid_welcome = new WelcomeFrame(this);
	cont_main->addWidget(wid_welcome);
	wid_data = new DataEditor(this);
	cont_main->addWidget(wid_data);
	wid_graph = new GraphEditor(this);
	cont_main->addWidget(wid_graph);
	wid_settings = new SettingsFrame(this);
	cont_main->addWidget(wid_settings);
	wid_image = new ImageViewer(this);
	cont_main->addWidget(wid_image);
	wid_empty = new QWidget(this);
	cont_main->addWidget( wid_empty );


	wid_sidebar = new QWidget(this);
	wid_sidebar->setLayout(_layout);



	QVBoxLayout* lay_main = new QVBoxLayout(this);
	splitter = new QSplitter(this);
	splitter->setChildrenCollapsible( false );
	splitter->addWidget(wid_sidebar);
	splitter->addWidget(cont_main);
	splitter->setStretchFactor(0, 2);
	splitter->setStretchFactor(1, 4);
	lay_main->addWidget( splitter );
	connect( splitter, &QSplitter::splitterMoved,
			 this, &Graftonite::onSplitterMoved );

	this->setLayout( lay_main );

	update_projectExplorer();


	showMaximized();
	_save_sidebar_width = wid_sidebar->sizeHint().width();

}
/**********************************************/
/**********************************************/
Graftonite::~Graftonite()
{
	delete splitter;
//	delete _layout;
//	delete cont_main;
//	delete wid_welcome;
//	delete wid_settings;
//	delete wid_data;
//	delete wid_graph;
//	delete wid_empty;
}
/**********************************************/
/**********************************************/
void Graftonite::update_projectExplorer()
{
//	tree_project->clear();
//	for ( const QString& path : App::openedProjects() )
//		tree_project->addTopLevelItem( path );
//	tree_project->expandAll();
}
/**********************************************/
/**********************************************/
void Graftonite::on_pb_welcome_released()
{
	cont_main->setCurrentWidget( wid_welcome );
}
/**********************************************/
void Graftonite::on_pb_settings_released()
{
	cont_main->setCurrentWidget( wid_settings );
}
/**********************************************/
void Graftonite::onSplitterMoved( int pos, int index )
{
	if ( pos < _save_sidebar_width+2 )
	{
		_sidebar_is_collapsed = true;
		pb_welcome->setText( "" );
		pb_project_explorer->setText( "" );
		pb_settings->setText( "" );
		tree_project->setAbleToHide( true );
	}
	else if ( _sidebar_is_collapsed && pos > _save_sidebar_width+2 )
	{
		_sidebar_is_collapsed = false;
		pb_welcome->setText( "Welcome" );
		pb_project_explorer->setText( "Project Explorer" );
		pb_settings->setText( "Settings" );
		tree_project->setAbleToHide( false );
	}

}
/**********************************************/
/**********************************************/
void Graftonite::onProjectItemChanged( ProjectItem* item )
{
	if ( item == nullptr )
		return;

	ProjectItem* project_item = tree_project->projectItem( item );


	Key type  = item->projectType();
	QString path  = item->setting(Key::Path).toString();
	if ( !path.startsWith("/") )
	{
		QString prj_path;
		if ( project_item != nullptr )
			prj_path = project_item->setting(Key::Path).toString();
		path = prj_path + (prj_path.endsWith("/")?"":"/") + path;
	}

	if ( type == Key::Project )
	{
		cont_main->setCurrentWidget( wid_empty );
	}
	else if ( type == Key::Folder )
	{
		cont_main->setCurrentWidget( wid_empty );
	}
	else if ( type == Key::Data )
	{
		cont_main->setCurrentWidget( wid_data );
		wid_data->setDataPath( path );
	}
	else if ( type == Key::Graph )
	{
		cont_main->setCurrentWidget( wid_graph );
		wid_graph->setGraphPath( path );
//		QString prj_path = tree_project->currentProject( item )->path();
//		wid_graph->setAvailableData( Project( prj_path ).availableData() );
	}
	else if ( type == Key::Image )
	{
		cont_main->setCurrentWidget( wid_image );
		wid_image->loadFile( path );
	}
	else
		cont_main->setCurrentWidget( wid_empty );
}
/**********************************************/
/**********************************************/
