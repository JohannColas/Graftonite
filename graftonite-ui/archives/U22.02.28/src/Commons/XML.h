#ifndef XML_H
#define XML_H
/**********************************************/
#include <QDomDocument>
/**********************************************/
class QPointF;
/**********************************************/
/**********************************************/
/**********************************************/
/**********************************************/
class XML
{
	QString _path;
	static inline QDomDocument _doc;
	 QDomDocument _file;
	/**********************************************/
public:
	XML();
	bool exists();
	void setPath( const QString& path );
	/**********************************************/
	QDomElement getGraph() const;
	static inline void ResetDocument() { _doc = QDomDocument(); }
	static inline QDomDocument Document() { return _doc; }
	static inline QDomElement CreateElement( const QString& tagName ) { return _doc.createElement(tagName); }
	static QDomElement GraphElement( const QString& path );
	static QList<QDomElement> ChildElements( const QDomElement& parent );
	QList<QDomElement> getElements();
	static QStringList AttributeKeys( const QDomElement& el );
	/**********************************************/
	static double GetDouble( const QDomElement& el, const QString& atr );
	/**********************************************/
	static void setInt( const QDomElement& el, const QString& atr, int& value );
	static void setDouble( const QDomElement& el, const QString& atr, double& value );
	static void setString( const QDomElement& el, const QString& atr, QString& value );
	static void setStringList( const QDomElement& el, const QString& atr, QStringList& value );
	static void setPoint( const QDomElement& el, const QString& atr, QPointF& value );
	static int getInt( const QDomElement& el, const QString& atr );
	static double getDouble( const QDomElement& el, const QString& atr );
	static QString getString( const QDomElement& el, const QString& atr );
	static QStringList getStringList( const QDomElement& el, const QString& atr );
	static QPointF getPoint( const QDomElement& el, const QString& atr );
	static int getInt( const QDomElement& el, const QString& child, const QString& atr );
	static double getDouble( const QDomElement& el, const QString& child, const QString& atr );
	static QString getString( const QDomElement& el, const QString& child, const QString& atr );
	static QStringList getStringList( const QDomElement& el, const QString& child, const QString& atr );
	static QPointF getPoint( const QDomElement& el, const QString& child, const QString& atr );
	static void save( QDomElement& el, const QString& atr, int value );
	static void save( QDomElement& el, const QString& atr, double value );
	static void save( QDomElement& el, const QString& atr, const QString& value );
	static void save( QDomElement& el, const QString& atr, const QStringList& value );
	static void save( QDomElement& el, const QString& atr, const QPointF& value );
	static void save( QDomElement& el, const QString& child, const QString& atr, int value );
	static void save( QDomElement& el, const QString& child, const QString& atr, double value );
	static void save( QDomElement& el, const QString& child, const QString& atr, const QString& value );
	static void save( QDomElement& el, const QString& child, const QString& atr, const QStringList& value );
	static void save( QDomElement& el, const QString& child, const QString& atr, const QPointF& value );
	/**********************************************/
	static QString Attribute( const QDomElement& el, const QString& atr );
	/**********************************************/
	static bool saveXML( const QString& path, QDomDocument& xmlDoc );
	static QDomDocument readXML( const QString& path );
	static bool readXML( const QString& path, QDomDocument& contents );
	/**********************************************/
};
/**********************************************/
/**********************************************/
#endif // XML_H
