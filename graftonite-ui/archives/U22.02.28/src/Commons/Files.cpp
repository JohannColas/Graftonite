#include "Files.h"
/**********************************************/
/**********************************************/
#include <QDomDocument>
#include <QFile>
#include <QTextStream>
/**********************************************/
/**********************************************/
void Files::readFile( const QString& path, QString& content )
{
	// Creating file
	QFile file( path );
	// Opening file
	if ( file.open( QIODevice::ReadOnly | QIODevice::Text ) )
	{
		QTextStream in(&file);
		// Reading file
		content = in.readAll();
		// Closing file du fichier
		file.close();
	}
}
/**********************************************/
/**********************************************/
QDomDocument Files::readXML( const QString& path )
{
	QDomDocument document;
	// Creating file
	QFile file( path );
	// Opening file
	if ( file.open( QIODevice::ReadOnly | QIODevice::Text ) )
	{
		// Reading file
		document.setContent( &file );
		// Closing file du fichier
		file.close();
	}
	return document;
}
/**********************************************/
/**********************************************/
void Files::saveXML( const QString& path, const QDomDocument& document )
{
	// Creating file
	QFile file( path );
	// Opening file
	if( !file.open( QFile::WriteOnly ) ) return ;
	// Creating QTextStream
	QTextStream stream( &file );
	// Writing document in the stream
	document.save( stream, 4, QDomDocument::EncodingFromTextStream );
	// Closing file du fichier
	file.close();
}
/**********************************************/
/**********************************************/
