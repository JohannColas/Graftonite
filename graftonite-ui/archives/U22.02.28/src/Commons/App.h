#ifndef APP_H
#define APP_H
/**********************************************/
#include <QStringList>
/**********************************************/
/**********************************************/
/**********************************************/
class App
{
	static inline QStringList _openedProjects = {};//"/home/colas/Documents/QtProjects/Graftonite-Debug/#project-exemple/project.gprj"};
	static inline QString _currentProject = "";
public:
	static inline QStringList openedProjects() { return _openedProjects; }
	static inline void addProjects( const QString& prj_path ) { _openedProjects.append(prj_path); }
	static inline QString currentProject() { return _currentProject; }
	static inline void setCurrentProject( const QString& prj_path ) { _currentProject = prj_path; }
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // APP_H
