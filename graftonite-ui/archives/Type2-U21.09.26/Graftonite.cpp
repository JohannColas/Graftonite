#include "Graftonite.h"
#include "ui_Graftonite.h"
/*------------------------------*/
#include <QDir>
#include <QFileSystemModel>
#include <QTreeWidgetItem>
/*------------------------------*/
#include "Project/Project.h"
/*------------------------------*/
#include <QDebug>
/*------------------------------*/
/*------------------------------*/
Graftonite::Graftonite( const QString& project, QWidget *parent ) :
	QWidget(parent),
	ui(new Ui::Graftonite)
{
	ui->setupUi(this);

	ProjectElement* prj = new ProjectElement("Proj", "Project", project);
	prj->readXML();
	_projects.append( prj );
//	_projects.append( new ProjectElement("Proj1", "Project", project) );

	ui->splitter->setStretchFactor(0, 2);
	ui->splitter->setStretchFactor(1, 4);
	ui->stack_main->hide();

	update_projectExplorer();
}
/*------------------------------*/
/*------------------------------*/
Graftonite::~Graftonite()
{
	delete ui;
}
/*------------------------------*/
/*------------------------------*/
void Graftonite::update_projectExplorer()
{
	QString oldID;
	if ( ui->tree_projectExplorer->currentIndex().row() != -1 )
		oldID = ui->tree_projectExplorer->currentItem()->text(2);
	QTreeWidgetItem* currentItem = 0;
	ui->tree_projectExplorer->clear();
	for ( ProjectElement* project : _projects )
	{
		QTreeWidgetItem* projectItem = new QTreeWidgetItem( 0 );
		projectItem->setText( 0, project->name() );
		projectItem->setText( 1, "Project" );
		projectItem->setText( 2, project->path() );
		ui->tree_projectExplorer->insertTopLevelItem( 0, projectItem );
		if ( !oldID.isEmpty() && project->path() == oldID )
			currentItem = projectItem;
		project->buildTree( projectItem );
	}
	ui->tree_projectExplorer->expandAll();
	if ( currentItem )
		ui->tree_projectExplorer->setCurrentItem( currentItem );
}
/*------------------------------*/
/*------------------------------*/
void Graftonite::on_tree_projectExplorer_itemPressed( QTreeWidgetItem *item, int /*column*/ )
{
	QString type = item->text(1);
	ui->stack_main->show();
	QTreeWidgetItem* par_item = item;
	while ( par_item->parent() )
		par_item = par_item->parent();
	if ( !par_item ) return;
	if ( type == Keys::Project )
	{

	}
	else if ( type == Keys::Folder )
	{

	}
	else if ( type == Keys::Data )
	{
		ui->stack_main->setCurrentWidget( ui->editor_data );
		ui->editor_data->setDataPath( par_item->text(2) + "/" + item->text(2) );
	}
	else if ( type == Keys::Graph )
	{
		ui->stack_main->setCurrentWidget( ui->editor_graph );
		ui->editor_graph->setGraphPath( par_item->text(2) + "/" + item->text(2) );
	}
	else
	{
		ui->stack_main->hide();
	}
}
/*------------------------------*/
/*------------------------------*/
