#include "Data.h"
/*------------------------------*/
#include "Graph/XML.h"
#include "Files.h"

#include <QDebug>
/*------------------------------*/
/*------------------------------*/
Data::Data()
{

}
/*------------------------------*/
/*------------------------------*/
void Data::setPath( const QString& path )
{
	_path = path;

	QDomDocument doc = Files::readXML( _path );
	QDomElement doc_el = doc.documentElement();
	QString str = XML::getString( doc_el, Keys::ID );
	if ( !str.isEmpty() )
		setID(str);
	for ( QDomElement el : XML::ChildElements(doc_el) )
	{
		QString elType = el.tagName();
		if ( elType == Keys::File )
		{
			str = XML::getString( el, Keys::Path );
			if ( !str.isEmpty() )
				setDataPath(str);
		}
		else if ( elType == Keys::Number )
		{
			str = XML::getString( el, Keys::Decimal );
			if ( !str.isEmpty() )
				setDecimal(str);
		}
		else if ( elType == Keys::Treatment )
		{
			str = XML::getString( el, Keys::Separator );
			if ( !str.isEmpty() )
				setSeparator(str);
			str = XML::getString( el, Keys::SkipEmptyParts );
			if ( !str.isEmpty() )
				setSkipEmptyParts(str.toInt());
			str = XML::getString( el, Keys::BeginRow );
			if ( !str.isEmpty() )
				setBeginRow(str.toInt());
			str = XML::getString( el, Keys::EndRow );
			if ( !str.isEmpty() )
				setEndRow(str.toInt());
		}
	}
	treat();
}
/*------------------------------*/
/*------------------------------*/
void Data::clear()
{

}
/*------------------------------*/
/*------------------------------*/
void Data::init()
{

}
/*------------------------------*/
/*------------------------------*/
void Data::set( const QDomElement& sets )
{
	//	XML::setString( sets, Keys::DataPath, _dataPath );
}
/*------------------------------*/
/*------------------------------*/
QDomElement Data::save()
{
	XML::ResetDocument();
	//
	//	QDomElement el = XML::CreateElement( Keys::Data );
	//	el.setAttribute( Keys::DataPath, _dataPath );
	//	return el;
}
/*------------------------------*/
/*------------------------------*/
void Data::saveXML()
{
	QDomDocument doc;
	QDomElement el = doc.createElement( Keys::Data );
	el.setAttribute( Keys::ID, _id );

	QDomElement cel = doc.createElement( Keys::File );
	cel.setAttribute( Keys::Path, _dataPath );
	el.appendChild( cel );

	cel = doc.createElement( Keys::Number );
	cel.setAttribute( Keys::Decimal, _decimal );
	el.appendChild( cel );

	cel = doc.createElement( Keys::Treatment );
	cel.setAttribute( Keys::Separator, _separator );
	cel.setAttribute( Keys::SkipEmptyParts, _skipEmptyParts );
	cel.setAttribute( Keys::BeginRow, _beginRow );
	cel.setAttribute( Keys::EndRow, _endRow );
	el.appendChild( cel );

	cel = doc.createElement( Keys::PostTreatment );
	/*------------*/
	el.appendChild( cel );

	doc.insertBefore( el, doc.doctype() );
	Files::saveXML( _path, doc );
}
/*------------------------------*/
/*------------------------------*/
void Data::treat()
{
	_data.clear();
	_rowCount = 0;
	_columnCount = 0;
	QString content;
	Files::readFile( _dataPath, content );
	QStringList lines = content.split( '\n', Qt::SkipEmptyParts );
	_rowCount = lines.size();
	for ( QString line : lines )
	{
		QVector<QString> dataline;
		for ( QString col : line.split( _separator, _skipEmptyParts ? Qt::SkipEmptyParts : Qt::KeepEmptyParts ) )
		{
			dataline.append( col );
		}
		_data.append( dataline );
		if ( _columnCount < dataline.size() )
			_columnCount = dataline.size();
	}
}
/*------------------------------*/
/*------------------------------*/
void Data::post_treat()
{

}
/*------------------------------*/
/*------------------------------*/
