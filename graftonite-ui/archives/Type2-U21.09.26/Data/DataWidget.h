#ifndef DATAWIDGET_H
#define DATAWIDGET_H
/*------------------------------*/
#include <QWidget>
/*------------------------------*/
class Data;
/*------------------------------*/
/*------------------------------*/
namespace Ui {
	class DataWidget;
}
/*------------------------------*/
/*------------------------------*/
/*------------------------------*/
class DataWidget : public QWidget
{
	Q_OBJECT
public:
	explicit DataWidget(QWidget *parent = nullptr);
	~DataWidget();
	/*------------------------------*/
	void setData( Data* data );
	/*------------------------------*/
private slots:
	void on_pb_dataPath_released();
	void on_cb_separator_currentTextChanged( const QString& sep );
	void on_ck_skipEmptyParts_stateChanged( int arg1 );
	void on_sp_beginRow_valueChanged( int row );
	void on_sp_endRow_valueChanged( int row );
	void on_cb_nbDecimal_currentTextChanged( const QString& decimal );
	/*------------------------------*/
private:
	Ui::DataWidget *ui;
	/*------------------------------*/
	Data* _data = 0;
	/*------------------------------*/
signals:
	void changed();
};
/*------------------------------*/
/*------------------------------*/
/*------------------------------*/
#endif // DATAWIDGET_H
