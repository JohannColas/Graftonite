#include "DataWidget.h"
#include "ui_DataWidget.h"
/*------------------------------*/
#include "Data.h"
/*------------------------------*/
/*------------------------------*/
DataWidget::DataWidget(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::DataWidget)
{
	ui->setupUi(this);

	ui->cb_separator->addItems( {"SPACE", "TAB", ";", ","} );
	ui->cb_nbDecimal->addItems( {".", ","} );

}
/*------------------------------*/
/*------------------------------*/
DataWidget::~DataWidget()
{
	delete ui;
}
/*------------------------------*/
/*------------------------------*/
void DataWidget::setData( Data* data )
{
	_data = data;
	if ( !_data ) return;

	ui->pb_dataPath->setText( _data->dataPath() );
	QString sep = _data->separator();
	if ( sep == "\t" )
		sep = "TAB";
	else if ( sep == " " )
		sep = "SPACE";
	ui->cb_separator->setCurrentText( sep );
	ui->ck_skipEmptyParts->setChecked( _data->skipEmptyParts() );
	ui->sp_beginRow->setValue( _data->beginRow() );
	ui->sp_endRow->setValue( _data->endRow() );
	ui->cb_nbDecimal->setCurrentText( _data->decimal() );
}
/*------------------------------*/
/*------------------------------*/
void DataWidget::on_pb_dataPath_released()
{
	if ( !_data ) return;
//	_data->setDataPath( ui->pb_dataPath->text() );
	emit changed();
}
/*------------------------------*/
/*------------------------------*/
void DataWidget::on_cb_separator_currentTextChanged( const QString& sep )
{
	if ( !_data ) return;
	QString newsep = sep;
	if ( newsep == "SPACE" )
		newsep = " ";
	else if ( newsep == "TAB" )
		newsep = "\t";
	_data->setSeparator( newsep );
	emit changed();
}
/*------------------------------*/
/*------------------------------*/
void DataWidget::on_ck_skipEmptyParts_stateChanged( int arg1 )
{
	if ( !_data ) return;
	_data->setSkipEmptyParts( arg1 );
	emit changed();
}
/*------------------------------*/
/*------------------------------*/
void DataWidget::on_sp_beginRow_valueChanged( int row )
{
	if ( !_data ) return;
	_data->setBeginRow( row );
	emit changed();
}
/*------------------------------*/
/*------------------------------*/
void DataWidget::on_sp_endRow_valueChanged( int row )
{
	if ( !_data ) return;
	_data->setEndRow( row );
	emit changed();
}
/*------------------------------*/
/*------------------------------*/
void DataWidget::on_cb_nbDecimal_currentTextChanged( const QString& decimal )
{
	if ( !_data ) return;
	_data->setDecimal( decimal );
	emit changed();
}
/*------------------------------*/
/*------------------------------*/

