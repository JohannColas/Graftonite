#ifndef DATAEDITOR_H
#define DATAEDITOR_H
/*------------------------------*/
#include <QWidget>
#include "Data.h"
/*------------------------------*/
/*------------------------------*/
namespace Ui {
	class DataEditor;
}
/*------------------------------*/
/*------------------------------*/
/*------------------------------*/
class DataEditor : public QWidget
{
	Q_OBJECT
public:
	explicit DataEditor( QWidget *parent = nullptr );
	~DataEditor();
	/*------------------------------*/
	void setDataPath( const QString& path );
	/*------------------------------*/
private slots:
	void updateEditor();
	void on_pb_Save_released();
	void on_pb_SaveAsTemplate_released();
	void on_pb_oriData_released();
	/*------------------------------*/
private:
	Ui::DataEditor *ui;
	Data _data = Data();
	/*------------------------------*/
};
/*------------------------------*/
/*------------------------------*/
/*------------------------------*/
#endif // DATAEDITOR_H
