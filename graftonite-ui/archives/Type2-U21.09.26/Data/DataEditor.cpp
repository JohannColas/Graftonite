#include "DataEditor.h"
#include "ui_DataEditor.h"
/*------------------------------*/
#include "Files.h"
#include <QTableWidget>
#include <QDebug>
/*------------------------------*/
/*------------------------------*/
DataEditor::DataEditor( QWidget *parent ) :
	QWidget(parent),
	ui(new Ui::DataEditor)
{
	ui->setupUi(this);

	ui->splitter->setStretchFactor( 0, 3 );
	ui->splitter->setStretchFactor( 1, 1 );

	ui->wid_data->setData( &_data );

	connect( &_data, &Data::changed,
			 this, &DataEditor::updateEditor );
	connect( ui->wid_data, &DataWidget::changed,
			 this, &DataEditor::updateEditor );
}
/*------------------------------*/
/*------------------------------*/
DataEditor::~DataEditor()
{
	delete ui;
}
/*------------------------------*/
/*------------------------------*/
void DataEditor::setDataPath( const QString& path )
{
	_data.setPath( path );
	updateEditor();
}
/*------------------------------*/
/*------------------------------*/
void DataEditor::updateEditor()
{
	_data.treat();
	QString content;
	Files::readFile( _data.dataPath(), content );
	ui->te_data->setPlainText( content );
	ui->table_data->clear();
	ui->table_data->setRowCount( _data.rowCount() );
	ui->table_data->setColumnCount( _data.columnCount() );
	for ( int it = 0; it < _data.data().size(); ++it )
	{
		QVector<QString> dataline = _data.data().at(it);
		for ( int jt = 0; jt < dataline.size(); ++jt )
		{
			QTableWidgetItem* newItem = new QTableWidgetItem( dataline.at(jt) );
			newItem->setTextAlignment( Qt::AlignCenter );
			ui->table_data->setItem( it, jt, newItem );
		}
	}
}
/*------------------------------*/
/*------------------------------*/
void DataEditor::on_pb_Save_released()
{
	_data.saveXML();
}
/*------------------------------*/
/*------------------------------*/
void DataEditor::on_pb_SaveAsTemplate_released()
{

}
/*------------------------------*/
/*------------------------------*/
void DataEditor::on_pb_oriData_released()
{
	if ( ui->stack_dataView->currentIndex() == 0 )
	{
		ui->pb_oriData->setText( "Hide Original Data" );
		ui->stack_dataView->setCurrentIndex(1);
	}
	else
	{
		ui->pb_oriData->setText( "Show Original Data" );
		ui->stack_dataView->setCurrentIndex(0);
	}
}
/*------------------------------*/
/*------------------------------*/
