#include "Layer.h"
/*------------------------------*/
#include "XML.h"
/*------------------------------*/
/*------------------------------*/
Layer::Layer( Graph* graph ) : Element(graph)
{
	setElementType( Keys::Layer );
	// Initialize Settings
	_sets = XML::CreateElement(_elType);
	//
	setID( "Layer"+QString::number(_nbLayers) );
	++_nbLayers;
}
/*------------------------------*/
/*------------------------------*/
void Layer::set( const QDomElement& sets )
{
	Element::set( sets );
	QString str = XML::getString( sets, Keys::X );
	if ( !str.isEmpty() )
		setX(str.toDouble());
	str = XML::getString( sets, Keys::Y );
	if ( !str.isEmpty() )
		setY(str.toDouble());
	str = XML::getString( sets, Keys::Width );
	if ( !str.isEmpty() )
		setWidth(str.toDouble());
	str = XML::getString( sets, Keys::Height );
	if ( !str.isEmpty() )
		setHeight(str.toDouble());
}
/*------------------------------*/
/*------------------------------*/
QDomElement Layer::save()
{
	return settings();
}
/*------------------------------*/
/*------------------------------*/
void Layer::setX( double x )
{
	_x = x;
	XML::save( _sets, Keys::X, _x );
}
/*------------------------------*/
/*------------------------------*/
void Layer::setY( double y )
{
	_y = y;
	XML::save( _sets, Keys::Y, _y );
}
/*------------------------------*/
/*------------------------------*/
void Layer::setWidth( double width )
{
	_width = width;
	XML::save( _sets, Keys::Width, _width );
}
/*------------------------------*/
/*------------------------------*/
void Layer::setHeight( double height )
{
	_height = height;
	XML::save( _sets, Keys::Height, _height );
}
/*------------------------------*/
/*------------------------------*/
