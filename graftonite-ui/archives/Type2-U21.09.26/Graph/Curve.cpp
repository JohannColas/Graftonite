#include "Curve.h"
/*------------------------------*/
#include "Graph.h"
#include "Axis.h"
#include "XML.h"
/*------------------------------*/
/*------------------------------*/
Curve::Curve( Graph* graph ) : Element(graph)
{
	init();
}
/*------------------------------*/
/*------------------------------*/
void Curve::init()
{
	setElementType( Keys::Curve );
	// Initialize Settings
	_sets = XML::CreateElement(_elType);
	QDomElement el = XML::CreateElement( Keys::Data );
	_sets.appendChild( QDomNode(el) );
	el = XML::CreateElement( Keys::Axes );
	_sets.appendChild( QDomNode(el) );
	el = XML::CreateElement( Keys::Line );
	_sets.appendChild( QDomNode(el) );
	//
	_id = _nbCurves;
	setID( "Curve"+QString::number(_id) );
	++_nbCurves;
	setData( {{0,0},{0.1,0.2},{0.3,0.5},{0.7,0.8},{0.9,0.9}} );
}
/*------------------------------*/
/*------------------------------*/
void Curve::set( const QDomElement& sets )
{
	Element::set( sets );
	QString str = XML::getString( sets, Keys::Type );
	if ( !str.isEmpty() )
		setType(str);
	str = XML::getString( sets, Keys::Axes, Keys::Axis1 );
	if ( !str.isEmpty() )
		setAxis1(str);
	str = XML::getString( sets, Keys::Axes, Keys::Axis2 );
	if ( !str.isEmpty() )
		setAxis2(str);
	str = XML::getString( sets, Keys::Axes, Keys::Axis3 );
	if ( !str.isEmpty() )
		setAxis3(str);
	str = XML::getString( sets, Keys::Data, Keys::Path );
	if ( !str.isEmpty() )
		setDataPath(str);
	str = XML::getString( sets, Keys::Data, Keys::Index1 );
	if ( !str.isEmpty() )
		setIndex1(str);
	str = XML::getString( sets, Keys::Data, Keys::Index2 );
	if ( !str.isEmpty() )
		setIndex2(str);
	str = XML::getString( sets, Keys::Data, Keys::Index3 );
	if ( !str.isEmpty() )
		setIndex3(str);
}
/*------------------------------*/
/*------------------------------*/
QDomElement Curve::save()
{
	return settings();
}
/*------------------------------*/
/*------------------------------*/
QString Curve::draw()
{
	if ( !_graph ) return "";
	Axis* axis1 = 0;
	Axis* axis2 = 0;
	if ( _axes.size() < 2 )
	{
		_axes.clear();
		QList<Axis*> lst = _graph->axes();
		if ( lst.size() > 1 )
		{
		axis1 = lst.at(0);
		axis2 = lst.at(1);
		}
	}
	else
	{
		axis1 = _graph->axis(_axes.at(0) );
		axis2 = _graph->axis(_axes.at(1) );
	}
	if ( !axis1 || !axis2 ) return "";

	QVector<QVector<double>> graphedData;
	for ( QVector<double> point : _data )
	{
		double x = axis1->GetGraphCoord(point.at(0));
		double y = axis2->GetGraphCoord(point.at(1));
		graphedData.append( {x,y} );
	}
	QString content;
	// Open Curve Group
	content += "\t<g id=\""+str_id+"-curve\" fill=\"none\" stroke=\"red\" stroke-linecap=\"round\" stroke-width=\"5px\">\n";
	// Draw Curve Line
	content += "\t\t<path d=\"M ";
	bool originDefined = false;
	for ( int it = 0; it < graphedData.size(); ++it  )
	{
		if ( originDefined )
			content += " L ";
		QVector<double> point = graphedData.at( it );
		double x = point.at(0);
		double y = point.at(1);
		if ( x >= axis1->graphRect().left() &&
			 x <= axis1->graphRect().right() &&
			 y >= axis2->graphRect().top() &&
			 y <= axis2->graphRect().bottom() )
		{
			originDefined = true;
			content += QString::number(x) + "," + QString::number(y);
		}
	}
	content += "\"/>\n";
	// Draw Curve Symbols
	// Draw Curve Area
	// Close Curve Group
	content += "\t</g>\n";
	return content;
}
/*------------------------------*/
/*------------------------------*/
void Curve::setType( const QString& type )
{
	_type = type;
	XML::save( _sets, Keys::Type, _type );
}
/*------------------------------*/
/*------------------------------*/
void Curve::setAxes( const QStringList axisNames )
{
	_axes = axisNames;
	XML::save( _sets, Keys::Axes, Keys::List, _axes );
}
/*------------------------------*/
/*------------------------------*/
void Curve::setAxis1( const QString& axis1 )
{
	_axis1 = axis1;
	XML::save( _sets, Keys::Axes, Keys::Axis1, _axis1 );
}
/*------------------------------*/
/*------------------------------*/
void Curve::setAxis2( const QString& axis2 )
{
	_axis2 = axis2;
	XML::save( _sets, Keys::Axes, Keys::Axis2, _axis2 );
}
/*------------------------------*/
/*------------------------------*/
void Curve::setAxis3( const QString& axis3 )
{
	_axis3 = axis3;
	XML::save( _sets, Keys::Axes, Keys::Axis3, _axis3 );
}
/*------------------------------*/
/*------------------------------*/
void Curve::setDataPath( const QString& path )
{
	_dataPath = path;
	XML::save( _sets, Keys::Data, Keys::Path, _dataPath );
}
/*------------------------------*/
/*------------------------------*/
void Curve::setDataIndexes( const QStringList& indexes )
{
	_dataIndexes = indexes;
	XML::save( _sets, Keys::Data, Keys::Indexes, _dataIndexes );
}
/*------------------------------*/
/*------------------------------*/
void Curve::setIndex1( const QString& index1 )
{
	_index1 = index1;
	XML::save( _sets, Keys::Data, Keys::Index1, _index1 );
}
/*------------------------------*/
/*------------------------------*/
void Curve::setIndex2( const QString& index2 )
{
	_index2 = index2;
	XML::save( _sets, Keys::Data, Keys::Index2, _index2 );
}
/*------------------------------*/
/*------------------------------*/
void Curve::setIndex3( const QString& index3 )
{
	_index3 = index3;
	XML::save( _sets, Keys::Data, Keys::Index3, _index3 );
}
/*------------------------------*/
/*------------------------------*/
void Curve::setData( const QVector<QVector<double> >& data )
{
	_data = data;
//	XML::save( _sets, Keys::Data, Keys::Raw, _data );
}
/*------------------------------*/
/*------------------------------*/
void Curve::setLineStyle( const QString& style)
{
	_lineStyle = style;
	XML::save( _sets, Keys::Line, Keys::Style, _lineStyle );
}
/*------------------------------*/
/*------------------------------*/
void Curve::setScatterStyle( const QString& style )
{
	_scatterStyle = style;
	XML::save( _sets, Keys::Scatter, Keys::Style, _scatterStyle );
}
/*------------------------------*/
/*------------------------------*/
void Curve::setAreaStyle( const QString& style )
{
	_areaStyle = style;
	XML::save( _sets, Keys::Area, Keys::Style, _scatterStyle );
}
/*------------------------------*/
/*------------------------------*/
void Curve::setSimplify( const QString& simplify )
{
	_simplify = simplify;
}
/*------------------------------*/
/*------------------------------*/
void Curve::setSimplifyOption( double option )
{
	_simplifyOpt = option;
}
/*------------------------------*/
/*------------------------------*/
