#include "Element.h"
#include "XML.h"
#include "Graph.h"
/*------------------------------*/
/*------------------------------*/
Element::Element( Graph* graph )
{
	_graph = graph;
}
/*------------------------------*/
/*------------------------------*/
QString Element::draw()
{
	return QString();
}
/*------------------------------*/
/*------------------------------*/
void Element::set( const QDomElement& sets )
{
	QString str = XML::getString( sets, Keys::ID );
	if ( !str.isEmpty() )
		setID(str);
	setElementType( sets.tagName() );
}
/*------------------------------*/
/*------------------------------*/
void Element::setGraph( Graph* graph )
{
	_graph = graph;
}
/*------------------------------*/
/*------------------------------*/
QDomElement Element::settings()
{
	return _sets;
}
/*------------------------------*/
/*------------------------------*/
QDomElement Element::save()
{
	return QDomElement();
}
/*------------------------------*/
/*------------------------------*/
void Element::setID( const QString& id )
{
	str_id = id;
	XML::save( _sets, Keys::ID, str_id );
}
/*------------------------------*/
/*------------------------------*/
void Element::setElementType( const QString& elType )
{
	_elType = elType;
}
/*------------------------------*/
/*------------------------------*/
