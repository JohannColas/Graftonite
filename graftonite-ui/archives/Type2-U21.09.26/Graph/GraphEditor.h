#ifndef GRAPHEDITOR_H
#define GRAPHEDITOR_H
/*------------------------------*/
#include <QWidget>
/*------------------------------*/
#include "Graph.h"
/*------------------------------*/
class QTreeWidgetItem;
/*------------------------------*/
/*------------------------------*/
QT_BEGIN_NAMESPACE
namespace Ui { class GraphEditor; }
QT_END_NAMESPACE
/*------------------------------*/
/*------------------------------*/
/*------------------------------*/
class GraphEditor : public QWidget
{
	Q_OBJECT
public:
	GraphEditor( QWidget *parent = nullptr );
	~GraphEditor();
	/*------------------------------*/
	void setGraphPath( const QString& path );
	/*------------------------------*/
private slots:
	void graphChanged();
	void on_tree_elements_itemPressed( QTreeWidgetItem* item, int column );
	void closeEvent( QCloseEvent* event ) override;
	/*------------------------------*/
	void on_pb_newLayer_released();
	void on_pb_newAxis_released();
	void on_pb_newCurve_released();
	void on_pb_remove_released();
	/*------------------------------*/
	void saveGraph();
	/*------------------------------*/
	void on_pb_save_released();
	/*------------------------------*/
private:
	Ui::GraphEditor *ui;
	/*------------------------------*/
	Graph graph = Graph( "Graph Name" );
	/*------------------------------*/
};
/*------------------------------*/
/*------------------------------*/
/*------------------------------*/
#endif // GRAPHEDITOR_H
