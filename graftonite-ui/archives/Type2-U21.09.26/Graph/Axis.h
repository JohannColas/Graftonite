#ifndef AXIS_H
#define AXIS_H
/*------------------------------*/
#include "Element.h"
/*------------------------------*/
#include <QList>
#include <QRect>
#include <cmath>
#include <QDebug>
class QDomElement;
class Graph;
/*------------------------------*/
namespace AxisType {
	static inline QString Xaxis = "X axis";
	static inline QString Yaxis = "Y axis";
	static inline QString Zaxis = "Z axis";
};
namespace AxisScale {
	static inline QString Linear = "Linear";
	static inline QString Log10 = "Log10";
	static inline QString Log = "Log";
	static inline QString LogP = "Personalized Log";
	static inline QString Reciprocal = "Reciprocal";
	static inline QString OffsetReciprocal = "Offset Reciprocal";
};
namespace AxisPosition {
	static inline QString None = "None";
	static inline QString Outside = "Outside";
	static inline QString Inside = "Inside";
	static inline QString Top = "Top";
	static inline QString Bottom = "Bottom";
	static inline QString Left = "Left";
	static inline QString Right = "Right";
	static inline QString Center = "Center";
};
/*------------------------------*/
/*------------------------------*/
/*------------------------------*/
class Axis
		: public Element
{
private:
	static inline int _nbXaxes = 0;
	static inline int _nbYaxes = 0;
	int _id = 0;
	/*------------------------------*/
	QString _type = AxisType::Xaxis;
	QString _layer;
	double _min = 0;
	double _max = 1;
	QRectF _boundingRect = QRect(150,150,1300,700);
	// Scale
	QString _scale = AxisScale::Linear;
	double _scaleOpt = 0;
	double _scaleA = 0;
	double _scaleB = 0;
	// Axis Line
	QString _axispos = AxisPosition::Bottom;
	QString _linestyle = "";
	// Ticks
	QString _tickPos = AxisPosition::Outside;
	double _tickSize = 10;
	double _minortickSize = 5;
	double _tickinterval = 0.1;
	double _minortickinterval = 0.025;
	// Labels
	QString _labelsPos = AxisPosition::Outside;
	QString _labelsstyle = "";
	QPointF _labelsshift = {0,0};
	// Title
	QString _title = "X Axis";
	QString _titlePos = AxisPosition::Outside;
	QString _titlestyle = "";
	QPointF _titleshift = {0,0};
	/*------------------------------*/
public:
	QString type() const { return _type; }
	void setType( const QString& type );
	QString layer() const { return _layer; }
	void setLayer( const QString& layer );
	double min() { return _min; }
	double max() { return _max; }
	void setMin( double min );
	void setMax( double max );
	void setBounds( double min, double max ) { _min = min; _max = max; }
	QRectF graphRect() const { return _boundingRect; }
	void setGraphRect( const QRectF& rect ) { _boundingRect = rect; }
	void setGraphRect( double x, double y, double w, double h ) { _boundingRect = {x, y, w, h}; }
	// Scale
	QString scale() const { return _scale; }
	void setScale( const QString& scale );
	double scaleOption() const { return _scaleOpt; }
	void setScaleOption( double scaleopt );
	// Axis Line
	QString position() const { return _axispos; }
	void setPosition( const QString& pos );
	QString lineStyle() const { return _linestyle; }
	void setLineStyle( const QString& style );
	// Ticks
	QString ticksPosition() const { return _tickPos; }
	void setTicksPosition( const QString& pos );
	double ticksSize() const { return _tickSize; }
	void setTickSize( double size );
	double ticksInterval() const { return _tickinterval; }
	void setTickInterval( double interval );
	double minorTicksSize() const { return _minortickSize; }
	void setMinorTickSize( double size );
	double minorTicksInterval() const { return _minortickinterval; }
	void setMinorTickInterval( double interval );
	// Labels
	QString labelsPosition() const { return _labelsPos; }
	void setLabelsPosition( const QString& pos );
	QString labelsStyle() const { return _labelsstyle; }
	void setLabelsStyle( const QString& style );
	QPointF labelsShift() const { return _labelsshift; }
	void setLabelsShift( const QPointF& shift );
	// Title
	QString title() const { return _title; }
	void setTitle( const QString& title );
	QString titlePosition() const { return _titlePos; }
	void setTitlePosition( const QString& pos );
	QString titleStyle() const { return _titlestyle; }
	void setTitleStyle( const QString& style );
	QPointF titleShift() const { return _titleshift; }
	void setTitleShift( const QPointF& shift );
	/*------------------------------*/
public:
	Axis( Graph* graph );
	Axis( const QString& type, Graph* graph );
	void init();
	void set( const QDomElement& sets ) override;
	QDomElement save() override;
	QString draw() override;
	/*------------------------------*/
	void CalculateTicksPosition( QList<double>& tickspos,  QList<double>& minortickspos,  QStringList& labels, bool isYaxis = false  );
	double round( double value );
	QString toString( double value );
	void CalculateScaleCoef();
	double GetGraphCoord( double coord );
	/*------------------------------*/
};
/*------------------------------*/
/*------------------------------*/
/*------------------------------*/
#endif // AXIS_H
