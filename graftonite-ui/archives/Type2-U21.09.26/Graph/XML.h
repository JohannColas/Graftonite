#ifndef XML_H
#define XML_H
/*------------------------------*/
#include <QDomDocument>
/*------------------------------*/
class QPointF;
/*------------------------------*/
/*------------------------------*/
namespace Keys {
	static inline QString Graph = "graph";
	static inline QString Width = "width", Height = "height";
	static inline QString X = "x", Y = "y";
	static inline QString Layers = "layers";
	static inline QString Layer = "layer";
	static inline QString Axis = "axis";
	static inline QString Type = "type";
	static inline QString Option = "option";
	static inline QString Position = "pos";
	static inline QString Style = "style";
	static inline QString General = "general";
	static inline QString Min = "min";
	static inline QString Max = "max";
	static inline QString Scale = "scale";
	static inline QString Line = "line";
	static inline QString Scatter = "scatter";
	static inline QString Area = "area";
	static inline QString Ticks = "ticks";
	static inline QString MinorTicks = "minorticks";
	static inline QString Labels = "labels";
	static inline QString Title = "title";
	static inline QString Curve = "curve";
	static inline QString Size = "size";
	static inline QString Interval = "interval";
	static inline QString Shift = "shift";
	static inline QString Name = "name";
	static inline QString ID = "id";
	static inline QString Data = "data";
	static inline QString Axes = "axes";
	static inline QString Axis1 = "axis1", Axis2 = "axis2", Axis3 = "axis3";
	static inline QString List = "list";
	static inline QString Gap = "gap";
	static inline QString Path = "path";
	static inline QString DataPath = "datapath";
	static inline QString Indexes = "indexes";
	static inline QString Index1 = "index1", Index2 = "index2", Index3 = "index3";
	static inline QString Raw = "raw";
	static inline QString File = "file";
	static inline QString Number = "number";
	static inline QString PostTreatment = "posttreatment";
	static inline QString Treatment = "treatment";
	static inline QString Decimal = "decimal";
	static inline QString Separator = "separator";
	static inline QString SkipEmptyParts = "skip_empty_parts";
	static inline QString BeginRow = "begin_row";
	static inline QString EndRow = "end_row";
}
/*------------------------------*/
/*------------------------------*/
class XML
{
	QString _path;
	static inline QDomDocument _doc;
	 QDomDocument _file;
	/*------------------------------*/
public:
	XML();
	bool exists();
	void setPath( const QString& path );
	/*------------------------------*/
	QDomElement getGraph() const;
	static inline void ResetDocument() { _doc = QDomDocument(); }
	static inline QDomDocument Document() { return _doc; }
	static inline QDomElement CreateElement( const QString& tagName ) { return _doc.createElement(tagName); }
	static QDomElement GraphElement( const QString& path );
	static QList<QDomElement> ChildElements( const QDomElement& parent );
	QList<QDomElement> getElements();
	static QStringList AttributeKeys( const QDomElement& el );
	/*------------------------------*/
	static double GetDouble( const QDomElement& el, const QString& atr );
	/*------------------------------*/
	static void setInt( const QDomElement& el, const QString& atr, int& value );
	static void setDouble( const QDomElement& el, const QString& atr, double& value );
	static void setString( const QDomElement& el, const QString& atr, QString& value );
	static void setStringList( const QDomElement& el, const QString& atr, QStringList& value );
	static void setPoint( const QDomElement& el, const QString& atr, QPointF& value );
	static int getInt( const QDomElement& el, const QString& atr );
	static double getDouble( const QDomElement& el, const QString& atr );
	static QString getString( const QDomElement& el, const QString& atr );
	static QStringList getStringList( const QDomElement& el, const QString& atr );
	static QPointF getPoint( const QDomElement& el, const QString& atr );
	static int getInt( const QDomElement& el, const QString& child, const QString& atr );
	static double getDouble( const QDomElement& el, const QString& child, const QString& atr );
	static QString getString( const QDomElement& el, const QString& child, const QString& atr );
	static QStringList getStringList( const QDomElement& el, const QString& child, const QString& atr );
	static QPointF getPoint( const QDomElement& el, const QString& child, const QString& atr );
	static void save( QDomElement& el, const QString& atr, int value );
	static void save( QDomElement& el, const QString& atr, double value );
	static void save( QDomElement& el, const QString& atr, const QString& value );
	static void save( QDomElement& el, const QString& atr, const QStringList& value );
	static void save( QDomElement& el, const QString& atr, const QPointF& value );
	static void save( QDomElement& el, const QString& child, const QString& atr, int value );
	static void save( QDomElement& el, const QString& child, const QString& atr, double value );
	static void save( QDomElement& el, const QString& child, const QString& atr, const QString& value );
	static void save( QDomElement& el, const QString& child, const QString& atr, const QStringList& value );
	static void save( QDomElement& el, const QString& child, const QString& atr, const QPointF& value );
	/*------------------------------*/
	static QString Attribute( const QDomElement& el, const QString& atr );
	/*------------------------------*/
	static bool saveXML( const QString& path, QDomDocument& xmlDoc );
	static QDomDocument readXML( const QString& path );
	static bool readXML( const QString& path, QDomDocument& contents );
	/*------------------------------*/
};
/*------------------------------*/
/*------------------------------*/
#endif // XML_H
