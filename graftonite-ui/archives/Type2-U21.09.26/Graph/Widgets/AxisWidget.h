#ifndef AXISWIDGET_H
#define AXISWIDGET_H
/*------------------------------*/
#include <QWidget>
/*------------------------------*/
class Graph;
class Axis;
/*------------------------------*/
/*------------------------------*/
namespace Ui {
	class AxisWidget;
}
/*------------------------------*/
/*------------------------------*/
/*------------------------------*/
class AxisWidget
		: public QWidget
{
	Q_OBJECT
public:
	explicit AxisWidget( QWidget* parent = nullptr );
	~AxisWidget();
	/*------------------------------*/
	void setGraph( Graph* graph = 0 );
	void setAxis( Axis* axis = 0 );
	/*------------------------------*/
private slots:
	//
	void on_le_name_editingFinished();
	void on_cb_type_currentIndexChanged( const QString& type );
	void on_cb_layer_currentIndexChanged( const QString& layer );
	//
	void on_cb_pos_currentIndexChanged( const QString& pos );
	void on_le_min_editingFinished();
	void on_le_max_editingFinished();
	//
	void on_cb_scale_currentIndexChanged( const QString& scale );
	void on_le_scaleOpt_editingFinished();
	//
	void on_cb_ticksPos_currentIndexChanged( const QString& ticksPos );
	void on_le_ticksSize_editingFinished();
	void on_le_ticksInterval_editingFinished();
	void on_le_minoticksSize_editingFinished();
	void on_le_minorticksInterval_editingFinished();
	//
	void on_cb_labelsPos_currentIndexChanged( const QString& labelsPos);
	//
	void on_cb_titlePos_currentIndexChanged( const QString& titlePos );
	/*------------------------------*/

private:
	Ui::AxisWidget* ui;
	Graph* _graph;
	Axis* _axis;
	/*------------------------------*/
signals:
	void changed();
};
/*------------------------------*/
/*------------------------------*/
/*------------------------------*/
#endif // AXISWIDGET_H
