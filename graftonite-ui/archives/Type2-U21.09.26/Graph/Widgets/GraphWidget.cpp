#include "GraphWidget.h"
#include "ui_GraphWidget.h"
/*------------------------------*/
#include "Graph/Graph.h"
/*------------------------------*/
/*------------------------------*/
GraphWidget::GraphWidget( QWidget* parent )
	: QWidget( parent ),
	  ui( new Ui::GraphWidget )
{
	ui->setupUi( this );
}
/*------------------------------*/
/*------------------------------*/
GraphWidget::~GraphWidget()
{
	delete ui;
}
/*------------------------------*/
/*------------------------------*/
void GraphWidget::setGraph( Graph* graph)
{
	_graph = graph;
	if ( !_graph ) return;

	ui->le_name->setText( _graph->ID() );
	ui->sp_width->setValue( _graph->width() );
	ui->sp_height->setValue( _graph->height() );
}
/*------------------------------*/
/*------------------------------*/

void GraphWidget::on_sp_width_valueChanged( int width )
{
	if ( !_graph ) return;
	_graph->setWidth( width );
	emit changed();
}

void GraphWidget::on_sp_height_valueChanged( int height )
{
	if ( !_graph ) return;
	_graph->setHeight( height );
	emit changed();
}
