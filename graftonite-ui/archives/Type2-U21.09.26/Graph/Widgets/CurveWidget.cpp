#include "CurveWidget.h"
#include "ui_CurveWidget.h"
/*------------------------------*/
#include "Graph/Graph.h"
#include "Graph/Axis.h"
#include "Graph/Curve.h"
/*------------------------------*/
/*------------------------------*/
CurveWidget::CurveWidget( QWidget* parent )
	: QWidget( parent ),
	  ui( new Ui::CurveWidget )
{
	ui->setupUi( this );

	ui->cb_type->addItems( {CurveType::XY,CurveType::XYY,CurveType::XYZ} );
}
/*------------------------------*/
/*------------------------------*/
CurveWidget::~CurveWidget()
{
	delete ui;
}
/*------------------------------*/
/*------------------------------*/
void CurveWidget::setGraph( Graph* graph )
{
	_graph = graph;
}
/*------------------------------*/
/*------------------------------*/
void CurveWidget::setCurve( Curve* curve )
{
	_curve = curve;
	if ( !_curve ) return;
	ui->le_name->setText( _curve->ID() );
	ui->cb_type->setCurrentText( _curve->type() );

	update_cb_axes();
}
/*------------------------------*/
/*------------------------------*/
void CurveWidget::on_cb_type_currentIndexChanged( const QString& type )
{
	if ( !_curve ) return;
	_curve->setType( type );
	update_cb_axes();
	emit changed();
}
/*------------------------------*/
/*------------------------------*/
void CurveWidget::update_cb_axes()
{
	ui->cb_axis1->blockSignals(true);
	ui->cb_axis2->blockSignals(true);
	ui->cb_axis3->blockSignals(true);
	ui->cb_dataPath->blockSignals(true);
	ui->cb_axis1->clear();
	ui->cb_axis2->clear();
	ui->cb_axis3->clear();
	ui->cb_dataPath->clear();
	if ( !_graph ) return;
	ui->cb_axis1->addItem( QString() );
	ui->cb_axis2->addItem( QString() );
	ui->cb_axis3->addItem( QString() );
	for ( Axis* axis : _graph->axes() )
	{
		ui->cb_axis1->addItem( axis->ID() );
		ui->cb_axis2->addItem( axis->ID() );
		if ( _curve->type() != CurveType::XY )
		{
			ui->cb_axis3->addItem( axis->ID() );
		}
	}
	if ( _curve->type() == CurveType::XY )
	{
		ui->cb_axis3->hide();
		ui->le_data3->hide();
//		if ( _curve->axes().size() < 2 ) return;
		ui->cb_axis1->setCurrentText( _curve->axis1() );
		ui->cb_axis2->setCurrentText( _curve->axis2() );
		ui->le_data1->setText( _curve->index1() );
		ui->le_data2->setText( _curve->index2() );
	}
	else
	{
		ui->cb_axis3->show();
		ui->le_data3->show();
	}
	ui->cb_axis1->blockSignals(false);
	ui->cb_axis2->blockSignals(false);
	ui->cb_axis3->blockSignals(false);
	ui->cb_dataPath->blockSignals(false);
}
/*------------------------------*/
/*------------------------------*/
void CurveWidget::on_cb_axis1_currentIndexChanged( const QString& axis1 )
{
	if ( !_curve ) return;
	_curve->setAxis1( axis1 );
}
/*------------------------------*/
/*------------------------------*/
void CurveWidget::on_cb_axis2_currentIndexChanged( const QString& axis2 )
{
	if ( !_curve ) return;
	_curve->setAxis2( axis2 );
}
/*------------------------------*/
/*------------------------------*/
void CurveWidget::on_cb_axis3_currentIndexChanged( const QString& axis3 )
{
	if ( !_curve ) return;
	_curve->setAxis3( axis3 );
}
/*------------------------------*/
/*------------------------------*/
void CurveWidget::on_cb_dataPath_currentIndexChanged( const QString& path )
{
	_curve->setDataPath( path );
}
/*------------------------------*/
/*------------------------------*/
void CurveWidget::on_le_data1_editingFinished()
{
	_curve->setIndex1( ui->le_data1->text() );
}
/*------------------------------*/
/*------------------------------*/
void CurveWidget::on_le_data2_editingFinished()
{
	_curve->setIndex2( ui->le_data2->text() );
}
/*------------------------------*/
/*------------------------------*/
void CurveWidget::on_le_data3_editingFinished()
{
	_curve->setIndex3( ui->le_data3->text() );
}
/*------------------------------*/
/*------------------------------*/
