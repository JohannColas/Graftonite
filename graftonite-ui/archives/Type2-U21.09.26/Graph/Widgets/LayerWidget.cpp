#include "LayerWidget.h"
#include "ui_LayerWidget.h"
/*------------------------------*/
/*------------------------------*/
#include "Graph/Layer.h"
/*------------------------------*/
/*------------------------------*/
LayerWidget::LayerWidget(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::LayerWidget)
{
	ui->setupUi(this);
}
/*------------------------------*/
/*------------------------------*/
LayerWidget::~LayerWidget()
{
	delete ui;
}
/*------------------------------*/
/*------------------------------*/
void LayerWidget::setLayer( Layer* layer )
{
	_layer = layer;
	if ( !_layer ) return;
	ui->le_name->setText( _layer->ID() );
	ui->le_x->setText( QString::number(_layer->x()) );
	ui->le_y->setText( QString::number(_layer->y()) );
	ui->le_width->setText( QString::number(_layer->width()) );
	ui->le_height->setText( QString::number(_layer->height()) );
}
/*------------------------------*/
/*------------------------------*/
void LayerWidget::on_le_name_editingFinished()
{
	if ( !_layer ) return;
	_layer->setID( ui->le_name->text() );
	emit changed();
}
/*------------------------------*/
/*------------------------------*/
void LayerWidget::on_le_x_editingFinished()
{
	if ( !_layer ) return;
	_layer->setX( ui->le_x->text().toDouble() );
	emit changed();
}
/*------------------------------*/
/*------------------------------*/
void LayerWidget::on_le_y_editingFinished()
{
	if ( !_layer ) return;
	_layer->setY( ui->le_y->text().toDouble() );
	emit changed();
}
/*------------------------------*/
/*------------------------------*/
void LayerWidget::on_le_width_editingFinished()
{
	if ( !_layer ) return;
	_layer->setWidth( ui->le_width->text().toDouble() );
	emit changed();
}
/*------------------------------*/
/*------------------------------*/
void LayerWidget::on_le_height_editingFinished()
{
	if ( !_layer ) return;
	_layer->setHeight( ui->le_height->text().toDouble() );
	emit changed();
}
/*------------------------------*/
/*------------------------------*/
