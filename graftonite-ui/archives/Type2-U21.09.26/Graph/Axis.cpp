#include "Axis.h"
/*------------------------------*/
#include "SVG.h"
#include "XML.h"
#include "Graph.h"
#include "Layer.h"
/*------------------------------*/
/*------------------------------*/
Axis::Axis( Graph* graph ) : Element(graph)
{
	init();
}
/*------------------------------*/
/*------------------------------*/
Axis::Axis( const QString& type, Graph* graph ) : Element(graph)
{
	setType( type );
	init();
}
/*------------------------------*/
/*------------------------------*/
void Axis::init()
{
	setElementType( Keys::Axis );
	// Initialize Settings
	_sets = XML::CreateElement(_elType);
	QDomElement el = XML::CreateElement( Keys::General );
	_sets.appendChild( QDomNode(el) );
	el = XML::CreateElement( Keys::Scale );
	_sets.appendChild( QDomNode(el) );
	el = XML::CreateElement( Keys::Line );
	_sets.appendChild( QDomNode(el) );
	el = XML::CreateElement(  Keys::Ticks );
	_sets.appendChild( QDomNode(el) );
	el = XML::CreateElement( Keys::MinorTicks );
	_sets.appendChild( QDomNode(el) );
	el = XML::CreateElement( Keys::Labels );
	_sets.appendChild( QDomNode(el) );
	el = XML::CreateElement( Keys::Title );
	_sets.appendChild( QDomNode(el) );
	//
	if ( _type == AxisType::Xaxis )
	{
		_id = _nbXaxes;
		setID( "x"+QString::number(_id) );
		++_nbXaxes;
	}
	else if ( _type == AxisType::Yaxis )
	{
		_id = _nbYaxes;
		setID( "y"+QString::number(_id) );
		++_nbYaxes;
		setPosition( AxisPosition::Left );
		setTitle( "Y Axis" );
	}
}
/*------------------------------*/
/*------------------------------*/
void Axis::set( const QDomElement& sets )
{
	Element::set( sets );
	QString str = XML::getString( sets, Keys::Type );
	if ( !str.isEmpty() )
		setType(str);
	str = XML::getString( sets, Keys::Position );
	if ( !str.isEmpty() )
		setPosition( str );
	str = XML::getString( sets, Keys::Layer );
	if ( !str.isEmpty() )
		setLayer( str );
	str = XML::getString( sets, Keys::General, Keys::Min );
	if ( !str.isEmpty() )
		setMin( str.toDouble() );
	str = XML::getString( sets, Keys::General, Keys::Max );
	if ( !str.isEmpty() )
		setMax( str.toDouble() );
	//
	str = XML::getString( sets, Keys::Scale, Keys::Type );
	if ( !str.isEmpty() )
		setScale( str );
	str = XML::getString( sets, Keys::Scale, Keys::Option );
	if ( !str.isEmpty() )
		setScaleOption( str.toDouble() );
	//
	str = XML::getString( sets, Keys::Line, Keys::Style );
	if ( !str.isEmpty() )
		setLineStyle( str );
	//
	str = XML::getString( sets, Keys::Ticks, Keys::Position );
	if ( !str.isEmpty() )
		setTicksPosition( str );
	str = XML::getString( sets, Keys::Ticks, Keys::Size );
	if ( !str.isEmpty() )
		setTickSize( str.toDouble() );
	str = XML::getString( sets, Keys::Ticks, Keys::Interval );
	if ( !str.isEmpty() )
		setTickInterval( str.toDouble() );
	str = XML::getString( sets, Keys::MinorTicks, Keys::Size );
	if ( !str.isEmpty() )
		setMinorTickSize( str.toDouble() );
	str = XML::getString( sets, Keys::MinorTicks, Keys::Interval );
	if ( !str.isEmpty() )
		setMinorTickInterval( str.toDouble() );
	//
	str = XML::getString( sets, Keys::Labels, Keys::Position );
	if ( !str.isEmpty() )
		setLabelsPosition( str );
	str = XML::getString( sets, Keys::Labels, Keys::Style );
	if ( !str.isEmpty() )
		setLabelsStyle( str );
	str = XML::getString( sets, Keys::Labels, Keys::Shift );
	if ( !str.isEmpty() )
	{
		QStringList lst = str.split(',');
		if ( lst.size() == 2 )
			setLabelsShift( {lst.first().toDouble(),lst.last().toDouble()} );
	}
	//
	str = XML::getString( sets, Keys::Title, Keys::Name );
	if ( !str.isEmpty() )
		setTitle( str );
	str = XML::getString( sets, Keys::Title, Keys::Position );
	if ( !str.isEmpty() )
		setTitlePosition( str );
	str = XML::getString( sets, Keys::Title, Keys::Style );
	if ( !str.isEmpty() )
		setTitleStyle( str );
	str = XML::getString( sets, Keys::Title, Keys::Shift );
	if ( !str.isEmpty() )
	{
		QStringList lst = str.split(',');
		if ( lst.size() == 2 )
			setTitleShift( {lst.first().toDouble(),lst.last().toDouble()} );
	}
}
/*------------------------------*/
/*------------------------------*/
QDomElement Axis::save()
{
	return settings();
}
/*------------------------------*/
/*------------------------------*/
QString Axis::draw()
{
	QString content;
	Layer* layer = _graph->layer( _layer );
	if ( layer )
	{
		_boundingRect = {layer->x(), layer->y(), layer->width(), layer->height()};
	}
	CalculateScaleCoef();
	// Open Axis Group
	content += "\t<g id=\""+str_id+"-axis\">\n";
	//
	double line_pos = 0;
	QString line_dir = "";
	QList<double> line_limits = {0,0};
	//
	double ticks_pos = 0;
	QString ticks_dir = "";
	QList<double> ticks_limits;
	double minorticks_pos = 0;
	QList<double> minorticks_limits;
	//
	QStringList labels;
	double labels_pos = 0;
	QStringList labels_anchor;
	//
	QList<double> title_pos = {0,0};
	QStringList title_anchor;
	QString title_transfo = "";
	// -----------------------
	// -----------------------
	// Draw X Axis
	// -----------------------
	if ( _type == AxisType::Xaxis )
	{
		// Open Axis Line Group
		content += "\t\t<g id=\"axis-line\" stroke=\"black\" stroke-linecap=\"round\" stroke-width=\"3px\">\n";
		// ---------------
		// Draw X Axis Line
		line_pos = _boundingRect.top();
		if ( _axispos == AxisPosition::Center )
			line_pos = _boundingRect.center().y();
		else if ( _axispos == AxisPosition::Bottom )
			line_pos = _boundingRect.bottom();
		content += "\t\t\t" + SVG::drawLine( GetGraphCoord(_min), line_pos, "H", GetGraphCoord(_max) ) + "\n";
		// ---------------
		// Draw X Axis Ticks (major & minor)
		ticks_pos = line_pos;
		minorticks_pos = line_pos;
		if ( _tickPos == AxisPosition::Top ||
			 (_tickPos == AxisPosition::Outside && _axispos == AxisPosition::Top) ||
			 (_tickPos == AxisPosition::Inside && _axispos == AxisPosition::Bottom) )
		{
			ticks_pos = line_pos - _tickSize;
			minorticks_pos = line_pos - _minortickSize;
		}
		else if ( _tickPos == AxisPosition::Center )
		{
			ticks_pos = line_pos - 0.5*_tickSize;
			minorticks_pos = line_pos - 0.5*_minortickSize;
		}
		// calculate ticks_limits
		CalculateTicksPosition( ticks_limits, minorticks_limits, labels );
		for ( double xpos : ticks_limits )
		{
			content += "\t\t\t" + SVG::drawLine( xpos, ticks_pos, "v", _tickSize) + "\n";
		}
		for ( double xpos : minorticks_limits )
		{
			content += "\t\t\t" + SVG::drawLine( xpos, minorticks_pos, "v", _minortickSize) + "\n";
		}
		// Close X Axis Line Group
		content += "\t\t</g>\n";
		// ---------------
		// Draw X Axis Labels
		labels_pos = ticks_pos + _tickSize + _labelsshift.y();
		labels_anchor.append( {"middle", "text-before-edge"} );
		if ( _labelsPos == AxisPosition::Top ||
			 (_labelsPos == AxisPosition::Outside && _axispos == AxisPosition::Top) ||
			 (_labelsPos == AxisPosition::Inside && _axispos == AxisPosition::Bottom) )
		{
			labels_pos = ticks_pos - _labelsshift.y();
			labels_anchor.replace( 1, "text-after-edge");
		}
		else if ( _labelsPos == AxisPosition::Center )
		{
			labels_pos = line_pos + _labelsshift.y();
			labels_anchor.replace( 1, "middle");
		}
		// ---------------
		// Open X Axis Labels Group
		content += "\t\t<g id=\"axis-ticklabels\" text-anchor=\""+labels_anchor.at(0)+"\" dominant-baseline=\""+labels_anchor.at(1)+"\" font-size=\"20pt\" transform=\"translate(0,"+QString::number(labels_pos)+")\">\n";
		// ---------------
		// Draw each label
		for ( int it = 0; it < labels.size(); ++it )
		{
			content += "\t\t\t"+SVG::drawText( "x", ticks_limits.at(it), labels.at(it) )+"\n";
		}
		// Close X Axis Labels Group
		content += "\t\t</g>\n";
		// ---------------
		// Draw X Axis Title
		title_pos = { _boundingRect.center().x() + _titleshift.x(), labels_pos + _titleshift.y() };
		if ( ticks_pos + _tickSize > labels_pos )
			title_pos.replace( 1, ticks_pos + _tickSize + _titleshift.y() );
		title_anchor.append( {"middle", "text-before-edge"} );
		if ( _titlePos == AxisPosition::Top ||
			 (_titlePos == AxisPosition::Outside && _axispos == AxisPosition::Top) ||
			 (_titlePos == AxisPosition::Inside && _axispos == AxisPosition::Bottom) )
		{
			title_pos.replace( 1, labels_pos - _titleshift.y() );
			if ( ticks_pos < labels_pos )
				title_pos.replace( 1, ticks_pos - _titleshift.y() );
			title_anchor.replace( 1, "text-after-edge");
		}
		content += "\t\t<g id=\"axis-ticklabels\" text-anchor=\""+title_anchor.at(0)+"\" dominant-baseline=\""+title_anchor.at(1)+"\" font-size=\"24pt\">\n";
		content += "\t\t\t"+SVG::drawText( title_pos.at(0),title_pos.at(1), title(), title_transfo )+"\n";
		content += "\t\t</g>\n";
		// Draw X Axis END
	}
	// -----------------------
	// -----------------------
	// Draw Y Axis
	// -----------------------
	else if ( _type == AxisType::Yaxis )
	{
		// Open Axis Line Group
		content += "\t\t<g id=\"axis-line\" stroke=\"black\" stroke-linecap=\"round\" stroke-width=\"3px\">\n";
		// ---------------
		// Draw Y Axis Line
		line_pos = _boundingRect.left();
		if ( _axispos == AxisPosition::Center )
			line_pos = _boundingRect.center().x();
		else if ( _axispos == AxisPosition::Right )
			line_pos = _boundingRect.right();
		content += "\t\t\t" + SVG::drawLine( line_pos, GetGraphCoord(_min), "V", GetGraphCoord(_max)) + "\n";
		// ---------------
		// Draw Y Axis Ticks (major & minor)
		ticks_dir = "h";
		ticks_pos = line_pos - _tickSize;
		minorticks_pos = line_pos - _minortickSize;
		if ( _tickPos == AxisPosition::Right ||
			 (_tickPos == AxisPosition::Outside && _axispos == AxisPosition::Right) ||
			 (_tickPos == AxisPosition::Inside && _axispos == AxisPosition::Left) )
		{
			ticks_pos = line_pos;
			minorticks_pos = line_pos;
		}
		else if ( _tickPos == AxisPosition::Center )
		{
			ticks_pos = line_pos - 0.5*_tickSize;
			ticks_pos = line_pos - 0.5*_minortickSize;
		}
		// calculate ticks_limits
		CalculateTicksPosition( ticks_limits, minorticks_limits, labels	, true );
		for ( double ypos : ticks_limits )
		{
			content += "\t\t\t" + SVG::drawLine( ticks_pos, ypos, "h", _tickSize) + "\n";
		}
		for ( double ypos : minorticks_limits )
		{
			content += "\t\t\t" + SVG::drawLine( minorticks_pos, ypos, "h", _minortickSize) + "\n";
		}
		// Close Y Axis Line Group
		content += "\t\t</g>\n";
		// ---------------
		// Draw Y Axis Labels
		labels_pos = ticks_pos - _labelsshift.x();
		labels_anchor.append( {"end", "middle"} );
		if ( _labelsPos == AxisPosition::Right ||
			 (_labelsPos == AxisPosition::Outside && _axispos == AxisPosition::Right) ||
			 (_labelsPos == AxisPosition::Inside && _axispos == AxisPosition::Left) )
		{
			labels_pos = ticks_pos + _tickSize + _labelsshift.x();
			labels_anchor.replace( 0, "start");
		}
		else if ( _labelsPos == AxisPosition::Center )
		{
			labels_pos = line_pos + _labelsshift.y();
			labels_anchor.replace( 0, "middle");
		}
		// ---------------
		// Open Y Axis Labels Group
		content += "\t\t<g id=\"axis-ticklabels\" text-anchor=\""+labels_anchor.at(0)+"\" dominant-baseline=\""+labels_anchor.at(1)+"\" font-size=\"20pt\" transform=\"translate("+QString::number(labels_pos)+",0)\">\n";
		// ---------------
		// Draw each label
		for ( int it = 0; it < labels.size(); ++it )
		{
			content += "\t\t\t"+SVG::drawText( "y", ticks_limits.at(it), labels.at(it) )+"\n";
		}
		// Close Y Axis Labels Group
		content += "\t\t</g>\n";
		// ---------------
		// Draw Y Axis Title
		title_pos = {labels_pos - _titleshift.x(), _boundingRect.center().y() + _titleshift.y()};
		if ( ticks_pos < labels_pos )
			title_pos.replace( 0, ticks_pos - _titleshift.x() );
		title_anchor.append( {"middle", "text-after-edge"} );
		if ( _titlePos == AxisPosition::Right ||
			 (_titlePos == AxisPosition::Outside && _axispos == AxisPosition::Right) ||
			 (_titlePos == AxisPosition::Inside && _axispos == AxisPosition::Left) )
		{
			title_pos.replace( 0, labels_pos + _titleshift.x() );
			if ( ticks_pos + _tickSize > labels_pos )
				title_pos.replace( 0, ticks_pos + _tickSize + _titleshift.x() );
			title_anchor.replace( 1, "text-before-edge");
		}
		title_transfo = "transform=\"rotate(-90 "+QString::number(title_pos.at(0))+" "+QString::number(title_pos.at(1))+")\"";
		content += "\t\t<g id=\"axis-ticklabels\" text-anchor=\""+title_anchor.at(0)+"\" dominant-baseline=\""+title_anchor.at(1)+"\" font-size=\"24pt\">\n";
		content += "\t\t\t"+SVG::drawText( title_pos.at(0),title_pos.at(1), title(), title_transfo )+"\n";
		content += "\t\t</g>\n";
		// Draw Y Axis END
	}
	// -----------------------
	// Close Axis Group
	content += "\t</g>\n";
	//
	return content;
}
/*------------------------------*/
/*------------------------------*/
void Axis::CalculateTicksPosition( QList<double>& tickspos, QList<double>& minortickspos, QStringList& labels, bool isYaxis )
{
	char axis_dir = 1; if ( _min > _max ) axis_dir = -1;

	int quot = (int)(_min/_tickinterval);
	if ( _min > 0 && _max > _min ) ++quot;
	else if ( _min < 0 && _max < _min ) --quot;

	int minorquot = (int)(_min/_minortickinterval);
	if ( _min > 0 && _max > _min ) ++minorquot;
	else if ( _min < 0 && _max < _min ) --minorquot;

	double posp = quot*_tickinterval;
	double pos = GetGraphCoord( posp );

	int it = 0;
	double minorposp = minorquot*_minortickinterval;
	double minorpos = GetGraphCoord( minorposp );
	while (( (minorpos+1 < pos && !isYaxis) ||
			(minorpos-1 > pos && isYaxis) )&&it<100 )
	{
		minortickspos.append( minorpos );
		minorposp += axis_dir*_minortickinterval;
		minorpos = GetGraphCoord( minorposp );
		++it;
	}
	it = 0;
	while (( (pos-1 < _boundingRect.right() && !isYaxis) ||
			(pos+1 > _boundingRect.top() && isYaxis) )&&it<100)
	{
		tickspos.append( pos );
		// -----------
		// Get Labels
		labels.append( toString(posp) );
		// -----------
		int jt = 0;
		double minorposp = _minortickinterval;
		double minorpos = GetGraphCoord( posp + axis_dir*minorposp );
		while (( minorposp < _tickinterval &&
				((minorpos-1 < _boundingRect.right() && !isYaxis) ||
				 (minorpos+1 > _boundingRect.top() && isYaxis) ) )&&jt<100)
		{
			minortickspos.append( minorpos );
			minorposp += _minortickinterval;
			minorpos = GetGraphCoord( posp + axis_dir*minorposp );
			++jt;
		}
		posp += axis_dir*_tickinterval;
		pos = GetGraphCoord( posp );
		++it;
	}
}
/*------------------------------*/
/*------------------------------*/
double Axis::round( double value )
{
	double val = value;
	return val;
}
/*------------------------------*/
/*------------------------------*/
QString Axis::toString( double value )
{
	QString num = QString::number(_tickinterval);
	int it = num.length() - num.lastIndexOf(".") - 1;
	QString temp = QString::number(value,'f',it);
	if ( temp.contains(".") )
		while ( temp.right(1) == "0" )
			temp = temp.remove( temp.length()-1, 1);
	if ( temp.right(1) == "." )
		temp = temp.remove( temp.length()-1, 1);
	if ( temp == "-0" )
		temp = "0";
	return temp;
}
/*------------------------------*/
/*------------------------------*/
void Axis::CalculateScaleCoef()
{
	double po = _boundingRect.left();
	double pm = _boundingRect.right();
	if ( _type == AxisType::Yaxis )
	{
		po = _boundingRect.bottom();
		pm = _boundingRect.top();
	}
	if ( _scale == AxisScale::Linear )
	{
		_scaleA = ( pm - po ) / ( _max - _min );
		_scaleB = po - _scaleA*_min;
	}
	else if ( _scale == AxisScale::Log10 )
	{
		if ( _min < 0 )
			_min = 0.1;
		if ( _max < 0 )
			_max = 1;
		_scaleA = ( pm - po ) / ( log10(_max) - log10(_min) );
		_scaleB = po - _scaleA*log10(_min);
	}
	else if ( _scale == AxisScale::Log )
	{
		if ( _min < 0 )
			_min = 0.1;
		if ( _max < 0 )
			_max = 1;
		_scaleA = ( pm - po ) / ( log(_max) - log(_min) );
		_scaleB = po - _scaleA*log(_min);
	}
	else if ( _scale == AxisScale::LogP )
	{
		if ( _min < 0 )
			_min = 0.1;
		if ( _max < 0 )
			_max = 1;
		_scaleA = log(_scaleOpt) * ( pm - po ) / ( log(_max) - log(_min) );
		_scaleB = po - _scaleA*log(_min)/log(_scaleOpt);
	}
	else if ( _scale == AxisScale::Reciprocal )
	{
		if ( _min == 0 )
			_min = 1;
		if ( _max == 0 )
			_max = 1;
		_scaleA = ( pm - po ) / ( 1/_max - 1/_min );
		_scaleB = po - _scaleA/_min;
	}
	else if ( _scale == AxisScale::OffsetReciprocal )
	{
		if ( _min+_scaleOpt == 0 )
			_min = 1-_scaleOpt;
		if ( _max+_scaleOpt == 0 )
			_max = 1-_scaleOpt;
		_scaleA = ( pm - po ) / ( 1/(_max+_scaleOpt) - 1/(_min+_scaleOpt) );
		_scaleB = po - _scaleA/(_min+_scaleOpt);
	}
}
/*------------------------------*/
/*------------------------------*/
double Axis::GetGraphCoord(double coord)
{
	if ( _scale == AxisScale::Linear )
		return _scaleA*coord+_scaleB;
	else if ( _scale == AxisScale::Log10 )
		return _scaleA*log10(coord)+_scaleB;
	else if ( _scale == AxisScale::Log )
		return _scaleA*log(coord)+_scaleB;
	else if ( _scale == AxisScale::LogP )
		return _scaleA*log(coord)/log(_scaleOpt)+_scaleB;
	else if ( _scale == AxisScale::Reciprocal )
		return _scaleA*(1/coord)+_scaleB;
	else if ( _scale == AxisScale::OffsetReciprocal )
		return _scaleA*(1/(coord+_scaleOpt))+_scaleB;
	return 0;
}
/*------------------------------*/
/*------------------------------*/
void Axis::setType( const QString& type )
{
	_type = type;
	XML::save( _sets, Keys::Type, _type );
}
/*------------------------------*/
/*------------------------------*/
void Axis::setLayer( const QString& layer )
{
	_layer = layer;
	if ( _graph )
	{
		Layer* layer = _graph->layer( _layer );
		if ( layer )
		{
			_boundingRect = { layer->x(), layer->y(), layer->width(), layer->height() };	
			XML::save( _sets, Keys::Layer, _layer );
		}
	}
}
/*------------------------------*/
/*------------------------------*/
void Axis::setMin( double min )
{
	_min = min;
	XML::save( _sets, Keys::General, Keys::Min, _min );
}
/*------------------------------*/
/*------------------------------*/
void Axis::setMax( double max )
{
	_max = max;
	XML::save( _sets, Keys::General, Keys::Max, _max );
}
/*------------------------------*/
/*------------------------------*/
void Axis::setScale( const QString& scale )
{
	_scale = scale;
	XML::save( _sets, Keys::Scale, Keys::Type, _scale );
}
/*------------------------------*/
/*------------------------------*/
void Axis::setScaleOption( double scaleopt )
{
	_scaleOpt = scaleopt;
	XML::save( _sets, Keys::Scale, Keys::Option, _scaleOpt );
}
/*------------------------------*/
/*------------------------------*/
void Axis::setPosition( const QString& pos )
{
	_axispos = pos;
	XML::save( _sets, Keys::Position, _axispos );
}
/*------------------------------*/
/*------------------------------*/
void Axis::setLineStyle( const QString& style )
{
	_linestyle = style;
	XML::save( _sets, Keys::Line, Keys::Style, _linestyle );
}
/*------------------------------*/
/*------------------------------*/
void Axis::setTicksPosition( const QString& pos )
{
	_tickPos = pos;
	XML::save( _sets, Keys::Ticks, Keys::Position, _tickPos );
}
/*------------------------------*/
/*------------------------------*/
void Axis::setTickSize( double size )
{
	_tickSize = size;
	XML::save( _sets, Keys::Ticks, Keys::Size, _tickSize );
}
/*------------------------------*/
/*------------------------------*/
void Axis::setTickInterval( double interval )
{
	_tickinterval = interval;
	XML::save( _sets, Keys::Ticks, Keys::Interval, _tickinterval );
}
/*------------------------------*/
/*------------------------------*/
void Axis::setMinorTickSize( double size )
{
	_minortickSize = size;
	XML::save( _sets, Keys::MinorTicks, Keys::Size, _minortickSize );
}
/*------------------------------*/
/*------------------------------*/
void Axis::setMinorTickInterval( double interval )
{
	_minortickinterval = interval;
	XML::save( _sets, Keys::MinorTicks, Keys::Interval, _minortickinterval );
}
/*------------------------------*/
/*------------------------------*/
void Axis::setLabelsPosition( const QString& pos )
{
	_labelsPos = pos;
	XML::save( _sets, Keys::Labels, Keys::Position, _labelsPos );
}
/*------------------------------*/
/*------------------------------*/
void Axis::setLabelsStyle( const QString& style )
{
	_labelsstyle = style;
	XML::save( _sets, Keys::Labels, Keys::Style, _linestyle );
}
/*------------------------------*/
/*------------------------------*/
void Axis::setLabelsShift( const QPointF& shift )
{
	_labelsshift = shift;
	XML::save( _sets, Keys::Labels, Keys::Shift, _labelsshift );
}
/*------------------------------*/
/*------------------------------*/
void Axis::setTitle( const QString& title )
{
	_title = title;
	_sets.firstChildElement(Keys::Title).setAttribute( Keys::Name, _title );
}
/*------------------------------*/
/*------------------------------*/
void Axis::setTitlePosition( const QString& pos )
{
	_titlePos = pos;
	_sets.firstChildElement(Keys::Title).setAttribute( Keys::Position, _titlePos );
}
/*------------------------------*/
/*------------------------------*/
void Axis::setTitleStyle( const QString& style )
{
	_titlestyle = style;
	XML::save( _sets, Keys::Title, Keys::Style, _titlestyle );
}
/*------------------------------*/
/*------------------------------*/
void Axis::setTitleShift( const QPointF& shift )
{
	_titleshift = shift;
	XML::save( _sets, Keys::Title, Keys::Shift, _titleshift );
}
/*------------------------------*/
/*------------------------------*/
