#include "GraphEditor.h"
#include "ui_GraphEditor.h"
/*------------------------------*/
#include <QTreeWidgetItem>
#include "XML.h"
#include "Layer.h"
#include "Axis.h"
#include "Curve.h"
/*------------------------------*/
#include <QDir>
#include <QElapsedTimer>
#include <QFile>
#include <QTextStream>
/*------------------------------*/
/*------------------------------*/
GraphEditor::GraphEditor( QWidget *parent )
    : QWidget(parent)
    , ui(new Ui::GraphEditor)
{
	ui->setupUi(this);
	ui->splitter->setSizes({(int)(0.7*width()),(int)(0.3*width())});

	ui->wid_graph->setGraph( &graph );
	ui->wid_axis->setGraph( &graph );
	ui->wid_curve->setGraph( &graph );

	graphChanged();

	connect( &graph, &Graph::changed,
			 this, &GraphEditor::graphChanged );
	connect( ui->wid_graph, &GraphWidget::changed,
			 this, &GraphEditor::graphChanged );
	connect( ui->wid_layer, &LayerWidget::changed,
			 this, &GraphEditor::graphChanged );
	connect( ui->wid_axis, &AxisWidget::changed,
			 this, &GraphEditor::graphChanged );
	connect( ui->wid_curve, &CurveWidget::changed,
			 this, &GraphEditor::graphChanged );

	connect( &graph, &Graph::changed,
			 this, &GraphEditor::saveGraph );
}
/*------------------------------*/
/*------------------------------*/
GraphEditor::~GraphEditor()
{
	delete ui;
}
/*------------------------------*/
/*------------------------------*/
void GraphEditor::on_tree_elements_itemPressed( QTreeWidgetItem* item, int column )
{
	Q_UNUSED( column )
	QString _type = item->text( 1 );
	if ( _type == Keys::Graph )
		ui->stackedWidget->setCurrentIndex(0);
	else if ( _type == Keys::Layer )
	{
		ui->stackedWidget->setCurrentIndex(1);
		ui->wid_layer->setLayer( graph.layer( item->text(0) ) );
	}
	else if ( _type == Keys::Axis )
	{
		ui->stackedWidget->setCurrentIndex(2);
		ui->wid_axis->setAxis( graph.axis( item->text(0) ) );
	}
	else if ( _type == Keys::Curve )
	{
		ui->stackedWidget->setCurrentIndex(3);
		ui->wid_curve->setCurve( graph.curve( item->text(0) ) );
	}
}
/*------------------------------*/
/*------------------------------*/
void GraphEditor::closeEvent( QCloseEvent* event )
{
	QDomDocument doc;
	QDomElement el = doc.createElement( Keys::Graph  );
	el.setAttribute( Keys::ID, graph.ID() );
	el.setAttribute( Keys::Width, graph.width() );
	el.setAttribute( Keys::Height, graph.height() );
	for ( Element* gelm : graph.elements() )
		el.appendChild( gelm->save() );
	doc.insertBefore( el, doc.documentElement() );
	QFile xmlFile( graph.path() );
	if( !xmlFile.open( QFile::WriteOnly ) )
		return ;
	// Create QTextStream
	QTextStream stream( &xmlFile );
	// Writing the
	doc.save( stream, 4, QDomDocument::EncodingFromTextStream );
	//Fermeture du fichier
	xmlFile.close();
	QWidget::closeEvent( event );
}
/*------------------------------*/
/*------------------------------*/
void GraphEditor::graphChanged()
{
	QString oldID;
	if ( ui->tree_elements->currentIndex().row() != -1 )
		oldID = ui->tree_elements->currentItem()->text(0);
	QTreeWidgetItem* currentItem = 0;
	ui->tree_elements->clear();
	QTreeWidgetItem* _graphItem = new QTreeWidgetItem( 0 );
	_graphItem->setText( 0, graph.ID() );
	_graphItem->setText( 1, graph.elementType() );
//	QList<QTreeWidgetItem *> items;
	for ( Element* el : graph.elements() )
	{
		QTreeWidgetItem* item = new QTreeWidgetItem(_graphItem, {el->ID(),el->elementType()});
		if ( !oldID.isEmpty() && el->ID() == oldID )
			currentItem = item;
	}
	ui->tree_elements->insertTopLevelItem( 0, _graphItem );
	ui->tree_elements->expandAll();
	if ( currentItem )
		ui->tree_elements->setCurrentItem( currentItem );

	QFile saveFile( graph.svgPath() );
	if ( saveFile.open( QIODevice::WriteOnly | QIODevice::Text ) )
	{
		QTextStream out( &saveFile );
//		out.setEncoding( QStringConverter::Utf8 );
		out.setCodec( "UTF-8" );
		out << graph.draw();
		saveFile.close();
	}


	ui->view_svg->openFile( graph.svgPath() );
	ui->view_svg->zoomOut();
}
/*------------------------------*/
/*------------------------------*/
void GraphEditor::on_pb_newLayer_released()
{
	graph.addElement( new Layer(&graph) );
}
/*------------------------------*/
/*------------------------------*/
void GraphEditor::on_pb_newAxis_released()
{
	graph.addElement( new Axis(&graph) );
}
/*------------------------------*/
/*------------------------------*/
void GraphEditor::on_pb_newCurve_released()
{

	graph.addElement( new Curve(&graph) );
}
/*------------------------------*/
/*------------------------------*/
void GraphEditor::on_pb_remove_released()
{
	graph.removeElement( ui->tree_elements->currentItem()->text(0) );
}
/*------------------------------*/
/*------------------------------*/
void GraphEditor::setGraphPath( const QString& path )
{
	graph.clear();
	graph.setPath( path );
	graph.init();
}
/*------------------------------*/
/*------------------------------*/
void GraphEditor::saveGraph()
{
	graph.saveXML();
}
/*------------------------------*/
/*------------------------------*/
void GraphEditor::on_pb_save_released()
{
	saveGraph();
}
/*------------------------------*/
/*------------------------------*/
