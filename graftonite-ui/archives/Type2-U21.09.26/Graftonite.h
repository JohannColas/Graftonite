#ifndef GRAFTONITE_H
#define GRAFTONITE_H
/*------------------------------*/
#include <QWidget>
/*------------------------------*/
class QTreeWidgetItem;
class ProjectElement;
/*------------------------------*/
/*------------------------------*/
namespace Ui {
	class Graftonite;
}
/*------------------------------*/
/*------------------------------*/
/*------------------------------*/
class Graftonite : public QWidget
{
	Q_OBJECT
public:
	explicit Graftonite( const QString& project, QWidget *parent = nullptr );
	~Graftonite();
	/*------------------------------*/
	void update_projectExplorer();
	/*------------------------------*/
private slots:
	void on_tree_projectExplorer_itemPressed( QTreeWidgetItem *item, int column );
	/*------------------------------*/
private:
	Ui::Graftonite *ui;
	QList<ProjectElement*> _projects;
	/*------------------------------*/
};
/*------------------------------*/
/*------------------------------*/
/*------------------------------*/
#endif // GRAFTONITE_H
