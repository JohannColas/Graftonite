#include "Project.h"
/*------------------------------*/
#include "Files.h"
#include <QDomDocument>
#include <QTreeWidgetItem>
#include <QDebug>
#include "Graph/XML.h"
/*------------------------------*/
/*------------------------------*/
ProjectElement::ProjectElement()
{
}
/*------------------------------*/
/*------------------------------*/
ProjectElement::ProjectElement(const QString& name, const QString& type, const QString& path)
{
	setName( name );
	setType( type );
	setPath( path );
}
/*------------------------------*/
/*------------------------------*/
void ProjectElement::setName(const QString& name)
{
	_name = name;
}
/*------------------------------*/
/*------------------------------*/
void ProjectElement::setType(const QString& type)
{
	_type = type;
}
/*------------------------------*/
/*------------------------------*/
void ProjectElement::setPath(const QString& path)
{
	_path = path;
}
/*------------------------------*/
/*------------------------------*/
void ProjectElement::setProject( ProjectElement* project )
{
	_project = project;
}
/*------------------------------*/
/*------------------------------*/
QList<ProjectElement*> ProjectElement::children()
{
	return _children;
}
/*------------------------------*/
/*------------------------------*/
bool ProjectElement::hasChildren()
{
	return !_children.isEmpty();
}
/*------------------------------*/
/*------------------------------*/
void ProjectElement::addElement(ProjectElement* prj_el)
{
	_children.append( prj_el);
}
/*------------------------------*/
/*------------------------------*/
void ProjectElement::buildTree( QTreeWidgetItem* item, ProjectElement* el )
{
	if ( !el ) el = this;
	for ( ProjectElement* prj_el : el->children() )
	{
		QTreeWidgetItem* child_item = new QTreeWidgetItem( item );
		child_item->setText( 0, prj_el->name() );
		child_item->setText( 1, prj_el->type() );
		child_item->setText( 2, prj_el->path() );
		if ( prj_el->hasChildren() )
			buildTree( child_item, prj_el );
	}
}
/*------------------------------*/
/*------------------------------*/
void ProjectElement::readXML()
{
	_children.clear();
	QDomDocument doc = Files::readXML( _path );
	QDomElement doc_el = doc.documentElement();
	QString str = XML::getString( doc_el, Keys::Name );
	if ( !str.isEmpty() )
		setName(str);
	setType( doc_el.tagName() );
	str = XML::getString( doc_el, Keys::Path );
	if ( !str.isEmpty() )
		setPath(str);
	readXML( doc_el, this );
}
/*------------------------------*/
/*------------------------------*/
void ProjectElement::readXML( const QDomElement& el, ProjectElement* prj_el )
{
	for ( QDomElement del : XML::ChildElements(el) )
	{
		ProjectElement* new_prh_el = new ProjectElement;
		new_prh_el->setName( del.attribute(Keys::Name) );
		new_prh_el->setType( del.tagName() );
		new_prh_el->setPath( del.attribute(Keys::Path) );
		new_prh_el->setProject(this);
		prj_el->addElement( new_prh_el );
		if ( del.hasChildNodes() )
			readXML( del, new_prh_el );
	}
}
/*------------------------------*/
/*------------------------------*/
void ProjectElement::saveXML()
{

}
/*------------------------------*/
/*------------------------------*/
