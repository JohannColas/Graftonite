#ifndef PROJECT_H
#define PROJECT_H
/*------------------------------*/
#include <QObject>
/*------------------------------*/
class Data;
class Graph;
class QTreeWidgetItem;
class QDomElement;
/*------------------------------*/
namespace Keys {
	static inline QString Project = "project";
	static inline QString Folder = "folder";
}
/*------------------------------*/
class ProjectElement
{
private:
	ProjectElement* _project = 0;
	QList<ProjectElement*> _children;
	QString _name;
	QString _type;
	QString _path;
	/*------------------------------*/
public:
	ProjectElement();
	ProjectElement( const QString& name, const QString& type, const QString& path );
	QString name() { return _name; }
	void setName( const QString& name );
	QString type() { return _type; }
	void setType( const QString& type );
	QString path() { return _path; }
	void setPath( const QString& path );
	void setProject( ProjectElement* project );
	/*------------------------------*/
	QList<ProjectElement*> children();
	bool hasChildren();
	void addElement( ProjectElement* prj_el );;
	void buildTree( QTreeWidgetItem* item, ProjectElement* el = 0 );
	/*------------------------------*/
	void readXML();
	void readXML( const QDomElement& el, ProjectElement* prj_el = 0 );
	void saveXML();
};
/*------------------------------*/
/*------------------------------*/
/*------------------------------*/
#endif // PROJECT_H
