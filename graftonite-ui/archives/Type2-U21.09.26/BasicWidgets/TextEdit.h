#ifndef TEXTEDIT_H
#define TEXTEDIT_H
/**********************************************/
#include <QPlainTextEdit>
/**********************************************/
/**********************************************/
/**********************************************/
QT_BEGIN_NAMESPACE
class QPaintEvent;
class QResizeEvent;
class QSize;
class QWidget;
QT_END_NAMESPACE
/**********************************************/
/**********************************************/
/**********************************************/
class LineNumberArea;
/**********************************************/
/**********************************************/
/**********************************************/
class TextEdit : public QPlainTextEdit
{
	Q_OBJECT

public:
	TextEdit(QWidget *parent = nullptr);
	void lineNumberAreaPaintEvent(QPaintEvent *event);
	int lineNumberAreaWidth();
protected:
	void resizeEvent(QResizeEvent *event) override;
private slots:
	void updateLineNumberAreaWidth(int newBlockCount);
	void highlightCurrentLine();
	void updateLineNumberArea(const QRect &rect, int dy);
protected slots:
	void focusOutEvent( QFocusEvent* event ) override
	{
		emit editingFinished();
		QPlainTextEdit::focusOutEvent( event );
	}
private:
	QWidget *lineNumberArea;
	int _padding = 8;
signals:
	void editingFinished();
};
/**********************************************/
/**********************************************/
/**********************************************/
class LineNumberArea : public QWidget
{
public:
	LineNumberArea(TextEdit *editor) : QWidget(editor), textEdit(editor)
	{}
	QSize sizeHint() const override
	{
		return QSize(textEdit->lineNumberAreaWidth(), 0);
	}
protected:
	void paintEvent(QPaintEvent *event) override
	{
		textEdit->lineNumberAreaPaintEvent(event);
	}
private:
	TextEdit *textEdit;
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TEXTEDIT_H
