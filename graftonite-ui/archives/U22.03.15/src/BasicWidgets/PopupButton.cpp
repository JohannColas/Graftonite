#include "PopupButton.h"
/**********************************************/
#include <QApplication>
#include <QScreen>
#include <QDebug>
/**********************************************/
#include "Popup.h"
/**********************************************/
/**********************************************/
PopupButton::PopupButton( QWidget* parent )
	: QPushButton( parent )
{
//	setFlat( true );
}
/**********************************************/
/**********************************************/
PopupButton::PopupButton( const QString& text, QWidget* parent )
	: QPushButton( text, parent )
{
//	setFlat( true );
}

bool PopupButton::childIsInative()
{
	return true;
}
/**********************************************/
/**********************************************/
bool PopupButton::popupActivated()
{
	if ( !_popup )
		return false;
	return _popup->isVisible();
}
/**********************************************/
/**********************************************/
void PopupButton::adjustPopup() {
	if ( !_popup ) return;
	int popup_w = _popup->sizeHint().width(),
			popup_h = _popup->sizeHint().height();
	/**********************************************/
	QPoint lf_bt = mapToGlobal( QPoint(0,height()) );
	int x = lf_bt.x(),
			y = lf_bt.y();
	QPoint rg_tp = mapToGlobal( QPoint(width(),0) );
	/**********************************************/
	QScreen* qscreen = QApplication::screenAt(lf_bt);
	if ( !qscreen ) return;
	QRect screen = qscreen->geometry();
	/**********************************************/
	if ( x + popup_w > screen.x() + screen.width() )
	{
		x =  rg_tp.x() - popup_w;
	}
	if ( y + popup_h > screen.y() + screen.height() )
	{
		y =  rg_tp.y() - popup_h;
	}
	/**********************************************/
	_popup->move( x, y );
	/**********************************************/
}
/**********************************************/
/**********************************************/
void PopupButton::enterEvent( QEnterEvent* event )
{
	QPushButton::enterEvent( event );
	if ( !_popup ) return;
	if ( !_popup->isVisible() )
		adjustPopup();
	if ( _showPopupOnEnterEvent && !_popup->isVisible() )
	{
		_popup->setVisible(true);
	}
}
/**********************************************/
/**********************************************/
void PopupButton::leaveEvent( QEvent* event )
{
	QPushButton::leaveEvent( event );
	if ( !_popup ) return;
	QPoint pos = QCursor::pos();
	if ( !_popup->geometry().contains(pos) )
		_popup->setVisible(false);
}
/**********************************************/
/**********************************************/
void PopupButton::mouseReleaseEvent( QMouseEvent* event )
{
	QPushButton::mouseReleaseEvent( event );
	if ( !_popup ) return;
	if ( !_popup->isVisible() )
	{
//		_popup->adjustSize();
		adjustPopup();
		_popup->setVisible(true);
	}
	else
		_popup->setVisible(false);
}
/**********************************************/
/**********************************************/
void PopupButton::moveEvent( QMoveEvent* event )
{
	QPushButton::moveEvent( event );
	if ( !_popup ) return;
//	_popup->adjustSize();
	adjustPopup();
}
/**********************************************/
/**********************************************/
