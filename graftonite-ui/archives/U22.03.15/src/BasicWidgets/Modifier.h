#ifndef MODIFIER_H
#define MODIFIER_H
/**********************************************/
#include <QObject>
/**********************************************/
#include "../Commons/Key.h"
/**********************************************/
class QWidget;
class QLabel;
/**********************************************/
/**********************************************/
/**********************************************/
class Modifier
		: public QObject
{
	Q_OBJECT
public:
	enum Type
	{
		NoType,
		Section,
		Widget,
		PushButton,
		CheckBox,
		LineEdit,
		Integer,
		PositiveInteger,
		Double,
		PositiveDouble,
		Date,
		ComboBox,
		Color,
		PenEditor,
		Position,
		Anchor,
		KeySelector,
		TextEdit,
	};
	/******************************************/
private:
	Key _setting = Key::Unknown;
	Modifier::Type _type = Modifier::NoType;
	QWidget* wid_modifier = nullptr;
	QLabel* wid_label = nullptr;
	/******************************************/
public:
	~Modifier();
	Modifier( QObject* parent = nullptr );
	Modifier( const Key& key, const Modifier::Type& type, QObject* parent = nullptr );
	Modifier( const QString& label, const Key& key, const Modifier::Type& type, QObject* parent = nullptr );
	/******************************************/
	Key setting() const;
	Modifier::Type type();
	void setTitle( const QString& title );
	QVariant value() const;
	void setValue( const QVariant& value );
	QLabel* title() const;
	QWidget* modifier() const;
	/******************************************/
public slots:
	void onEditingFinished();
	void onValueChanged( const QVariant& value );
	void onKeyChanged( const Key& value );
	void onTextChanged( const QString& text );
	void onBoolChanged( bool value );
	void onIntChanged( int value );
	void onDoubleChanged( double value );
	void onColorChanged( const QColor& color );
	void onPenChanged( const QPen& pen );
	void hide();
	void show();
	/******************************************/
signals:
	void settingChanged( const Key& setting, const QVariant& value );
	/******************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // MODIFIER_H
