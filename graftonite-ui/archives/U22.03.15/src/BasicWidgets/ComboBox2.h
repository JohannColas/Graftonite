#ifndef COMBOBOX2_H
#define COMBOBOX2_H
/**********************************************/
#include <QComboBox>
#include "../Commons/Key.h"
/**********************************************/
/**********************************************/
/**********************************************/
class ComboBox2
		: public QComboBox
{
	Q_OBJECT
	QList<QVariant> _values;
public:
	ComboBox2( QWidget* parent = nullptr );
	ComboBox2( const QList<QPair<QString,QVariant>>& data, QWidget* parent = nullptr );
	void init();
	void setData( const QList<QPair<QString,QVariant>>& data );
	void setCurrentValue( const QVariant& value );
	QVariant currentValue() const;
protected slots:
	void onIndexChanged( int index );
signals:
	void valueChanged( const QVariant& value );
};
/**********************************************/
/**********************************************/
/**********************************************/
class KeySelector
		: public QComboBox
{
	Q_OBJECT
	QList<Key> _keys;
public:
	KeySelector( QWidget* parent = nullptr );
	KeySelector( const QList<QPair<QString,Key>>& data, QWidget* parent = nullptr );
	void init();
	void setData( const QList<QPair<QString,Key>>& data );
	void setCurrentKey( const Key& value );
	Key currentKey() const;
protected slots:
	void onIndexChanged( int index );
signals:
	void keyChanged( const Key& value );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // COMBOBOX2_H
