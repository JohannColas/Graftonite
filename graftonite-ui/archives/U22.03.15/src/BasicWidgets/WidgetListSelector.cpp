#include "WidgetListSelector.h"
/**********************************************/
/**********************************************/
/**********************************************/
/**********************************************/
LabelItemList::LabelItemList( const QString& text, QWidget* parent )
	: QLabel( parent )
{
	setText( text );
	setSizePolicy( QSizePolicy::Preferred,
				   QSizePolicy::Preferred );
}
/**********************************************/
/**********************************************/
/**********************************************/
ButtonItemList::ButtonItemList( const QString& text, QWidget* parent )
	: QPushButton( parent )
{
	setText( text );
	setCheckable( true );
	setChecked( false );
	setSizePolicy( QSizePolicy::Preferred,
				   QSizePolicy::Preferred );
	setIconSize( {16, 16} );
	connect( this, &ButtonItemList::clicked,
			 this, &ButtonItemList::onClicked );
}
/**********************************************/
/**********************************************/
void ButtonItemList::setSVG( const QString& path )
{
	setIcon( QIcon( path ) );
}
/**********************************************/
/**********************************************/
int ButtonItemList::getIndex() const
{
	return index;
}
/**********************************************/
/**********************************************/
void ButtonItemList::setIndex( int ind )
{
	index = ind;
}
/**********************************************/
/**********************************************/
void ButtonItemList::onClicked()
{
	emit sendIndex( getIndex() );
}
/**********************************************/
/**********************************************/
/**********************************************/
WidgetListSelector::WidgetListSelector( QWidget* parent )
	: QWidget(parent)
{

	list->setWidgetResizable( true );
	list->setFrameShape( QFrame::NoFrame );
	setMinimumWidth( 250 );
	setLayout( layMain );
	layMain->setSpacing(0);
	//		layMain->setMargin(0);
	QWidget* container = new QWidget;
	QVBoxLayout* layout2 = new QVBoxLayout;
	layout2->addLayout( layout );
	layout2->addItem( new QSpacerItem(0,0,QSizePolicy::Preferred,QSizePolicy::Expanding));
	container->setLayout( layout2 );
	list->setWidget( container );
	list->setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
	list->setMaximumWidth( 250 );
	list->setFrameShape( QFrame::StyledPanel );
	layMain->addWidget( list, 0, 0 );
}
/**********************************************/
/**********************************************/
void WidgetListSelector::addItem( const QString& name, QWidget* widget )
{
	ButtonItemList* newItem = new ButtonItemList( name );
	newItem->setIndex( _nbItems );
	++_nbItems;
	connect( newItem, &ButtonItemList::sendIndex,
			 this, &WidgetListSelector::changeCurrentWidget );
	layout->addWidget( newItem );
	_items.append( newItem );
	_widgets.append( widget );
	widget->hide();

	int col = 1;
	layMain->addWidget( widget, 0, col );
}
/**********************************************/
/**********************************************/
void WidgetListSelector::addSection( const QString& section )
{
	LabelItemList* newSection = new LabelItemList( section );
	layout->addWidget( newSection );
	_sections.append( newSection );
}
/**********************************************/
/**********************************************/
void WidgetListSelector::setSectionText( int index, const QString& text )
{
	if ( index > -1 && index < _nbSections ) {
		_sections.at( index )->setText( text );
	}
}
/**********************************************/
/**********************************************/
void WidgetListSelector::setItemText( int index, const QString& text )
{
	if ( index > -1 && index < _nbItems ) {
		_items.at( index )->setText( text );
	}
}
/**********************************************/
/**********************************************/
void WidgetListSelector::setItemIcon( int index, const QIcon& icon )
{
	if ( index > -1 && index < _nbItems ) {
		_items.at( index )->setIcon( icon );
	}
}
/**********************************************/
/**********************************************/
void WidgetListSelector::setWidget( int index, QWidget* widget )
{
	if ( index < _nbItems ) {
		_widgets.replace( index, widget );
	}
}
/**********************************************/
/**********************************************/
void WidgetListSelector::changeCurrentWidget( int index )
{
	if ( index > -1 && index < _nbItems )
	{
		if ( _selectedTab > -1 && _selectedTab < _nbItems ) {
			_items.at( _selectedTab )->setChecked( false );
			_widgets.at( _selectedTab )->hide();
		}
		if ( index > -1 && index < _nbItems ) {
			_selectedTab = index;
			_items.at( index )->setChecked( true );
			_widgets.at( index )->show();
		}
	}
}
/**********************************************/
/**********************************************/
/**********************************************/
