#include "Popup.h"
/**********************************************/
#include <QEnterEvent>
/**********************************************/
/**********************************************/
Popup::Popup( QWidget* parent )
	: QWidget( parent )
{
	setWindowFlags( Qt::Drawer | Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint );
	setSizePolicy( QSizePolicy::Fixed, QSizePolicy::Fixed );
//	setFrameShape( QFrame::StyledPanel );
//	setWindowModality( Qt::WindowModal );
//	resize( 300,200);
//	setStyleSheet( "background:red;" );

//	QVBoxLayout* layout = new QVBoxLayout;
//	QLabel* label = new QLabel( "Curve Data Settings" );
//	QTreeWidget* tree = new QTreeWidget;
//	layout->addWidget( label );
//	layout->addWidget( tree );
	//	setLayout( layout );
}
/**********************************************/
/**********************************************/
bool Popup::childIsInactive()
{
	return true;
}
/**********************************************/
/**********************************************/
void Popup::leaveEvent( QEvent* event )
{
	QWidget::leaveEvent( event );
	if ( childIsInactive() )
	{
		QWidget* parent = parentWidget();
		if ( parent )
		{
			QPoint pos = QCursor::pos();
			if ( !parent->geometry().contains(pos) )
				setVisible(false);
		}
		else
			setVisible(false);
		if ( !isVisible() )
			emit closed();
	}
}
/**********************************************/
/**********************************************/
void Popup::enterEvent( QEnterEvent* event )
{
	QWidget::enterEvent( event );
}
/**********************************************/
/**********************************************/
