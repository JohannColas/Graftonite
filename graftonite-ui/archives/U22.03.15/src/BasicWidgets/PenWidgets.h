#ifndef PENWIDGETS_H
#define PENWIDGETS_H
/**********************************************/
#include <QWidget>
/**********************************************/
class PopupButton;
class Popup;
class QFormLayout;
class QHBoxLayout;
class QLabel;
class QComboBox;
class NumberEdit;
class ColorSelector;
class QLineEdit;
/**********************************************/
#include <QPen>
/**********************************************/
/**********************************************/
/**********************************************/
class PenModifier
		: public QWidget
{
	Q_OBJECT
private:
	QPen _pen = QPen(Qt::black, 4, Qt::NoPen );
	/******************************************/
	QHBoxLayout* lay_ = nullptr;
	PopupButton* pb_popup = nullptr;
	Popup* wid_popup = nullptr;
	/******************************************/
	QFormLayout* lay_main = nullptr;
	// Pen Dash
	bool _isDashed = false;
	QLabel* lb_dash = nullptr;
	QHBoxLayout* lay_dash = nullptr;
	QComboBox* cb_dashStyle = nullptr;
	QLineEdit* le_dashPattern = nullptr;
	bool _hasDashOffset = true;
	QFormLayout* lay_dashOffset = nullptr;
	QLabel* lb_dashOffset = nullptr;
	NumberEdit* ne_dashOffset = nullptr;
	// Pen Width
	QLabel* lb_width = nullptr;
	NumberEdit* ne_width = nullptr;
	// Pen Color
	QLabel* lb_color = nullptr;
	ColorSelector* cs_color = nullptr;
	// Pen Join
	bool _hasJoin = true;
	QLabel* lb_join = nullptr;
	QHBoxLayout* lay_join = nullptr;
	QComboBox* cb_join = nullptr;
	QLabel* lb_miterlimit = nullptr;
	NumberEdit* ne_miterlimit = nullptr;
	// Pen Cap
	bool _hasCap = true;
	QLabel* lb_cap = nullptr;
	QComboBox* cb_cap = nullptr;
	/******************************************/
public:
	~PenModifier();
	PenModifier( QWidget* parent = nullptr );
	PenModifier( const QPen& pen, QWidget* parent = nullptr );
	/******************************************/
	QPen pen() const;
	void setPen( const QPen& pen );
	/******************************************/
	void activateDashOffset();
	void desactivateDashOffset();
	void activateJoin();
	void desactivateJoin();
	void activateCap();
	void desactivateCap();
	/******************************************/
	void activatePopupMode();
	void desactivatePopupMode();
	/******************************************/
	void updateLayout();
	/******************************************/
private slots:
	void onDashStyleIndexChanged( int index );
	void onDashPatternChanged();
	void onDashOffsetChanged( double offset );
	void onWidthChanged( double width );
	void onColorChanged( const QColor& color );
	void onJoinIndexChanged( int index );
	void onMiterLimitChanged( double miterlimit );
	void onCapIndexChanged( int index );
	/******************************************/
signals:
	void penChanged( const QPen& pen );
	/******************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PENWIDGETS_H
