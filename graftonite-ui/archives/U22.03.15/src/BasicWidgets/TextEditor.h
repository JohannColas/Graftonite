#ifndef TEXTEDITOR_H
#define TEXTEDITOR_H

#include <QWidget>

class QComboBox;
class QFontComboBox;
class QTextEdit;
class QTextCharFormat;
class QPushButton;

class TextEditor : public QWidget
{
	Q_OBJECT

	QComboBox* comboStyle;
	QFontComboBox* comboFont;
	QComboBox* comboSize;

	QTextEdit *textEdit;

	QPushButton* pb_TextBold;
	QPushButton* pb_TextUnderline;
	QPushButton* pb_TextItalic;
	QPushButton* pb_TextColor;
	QPushButton* pb_UnderlineColor;

	void mergeFormatOnWordOrSelection( const QTextCharFormat& format );
	void fontChanged( const QFont& font );
	void colorChanged( const QColor& color );

public:
	TextEditor( QWidget* parent = nullptr );

	void setValue( const QVariant& value );
	void setText( const QString& text );
	QString text() const;


private slots:
	void textBold();
	void textUnderline();
	void textItalic();
	void textFamily( const QString& family );
	void textSize( const QString& size );
	void textStyle( int styleIndex );
	void textColor();
	void underlineColor();
	void textAlign( QAction* a );
	void setChecked( bool checked );
	void indent();
	void unindent();
	void modifyIndentation(int amount);

	void currentCharFormatChanged( const QTextCharFormat& format );
	void cursorPositionChanged();

	void clipboardDataChanged();
	void onTextChanged();

signals:
	void textChanged( const QString& text );
};

#endif // TEXTEDITOR_H
