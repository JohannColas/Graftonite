#ifndef IMAGEVIEWER_H
#define IMAGEVIEWER_H
/**********************************************/
#include <QMenu>
#include <QAction>
#include <QImageReader>
#include <QImage>
#include <QPixmap>
#include <QColorSpace>
#include <QHBoxLayout>
#include <QImageReader>
#include <QImageWriter>
#include <QKeyEvent>
#include <QLabel>
#include <QWheelEvent>
#include <QShortcut>
/**********************************************/
/**********************************************/
/**********************************************/
class ImageViewer
		: public QWidget
{
	Q_OBJECT
private:
	QImage image;
	QLabel *imageLabel;
	double scaleFactor = 1;
	QAction *zoomInAct;
	QAction *zoomOutAct;
	QAction *normalSizeAct;
	QAction *fitToWindowAct;
	QMenu *viewMenu;
	int _x_inc = 20;
	int _y_inc = 20;
	/******************************************/
public:
	ImageViewer( QWidget* parent = nullptr );
	/******************************************/
	bool loadFile( const QString& fileName );
	/******************************************/
private:
	void setImage( const QImage& newImage );
	/******************************************/
public slots:
	void showContextMenu( const QPoint& pos );
	void zoomIn();
	void zoomOut();
	void goLeft();
	void goRight();
	void goDown();
	void goUp();
	void normalSize();
	void fitToWindow();
	void createActions();
	void updateActions();
	void scaleImage( double factor );
	bool eventFilter( QObject* /*obj*/, QEvent* evt );
	/******************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // IMAGEVIEWER_H
