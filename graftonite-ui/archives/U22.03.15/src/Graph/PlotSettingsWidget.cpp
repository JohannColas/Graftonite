#include "PlotSettingsWidget.h"
/**********************************************/
#include <QFormLayout>
#include <QTabWidget>
#include <QLabel>
#include <QTextEdit>
/**********************************************/
#include "../BasicWidgets/ComboBox2.h"
#include "../BasicWidgets/TextEditor.h"
#include "../BasicWidgets/PenWidgets.h"
#include "GraphItem.h"
#include "FrameItem.h"
/**********************************************/
#include "../Commons/Settings.h"
#include "../Settings/SettingItem.h"
/**********************************************/
/**********************************************/
/**********************************************/
PlotSettingsWidget::PlotSettingsWidget( QWidget* parent )
    : SettingsWidget(parent)
{
	addLineEdit( Key::Name, "Plot :" );

	wid_general = new SettingsWidget(this);
	cb_type = wid_general->addKeySelector( Key::Type, "Type :" );
	cb_type->setData( {{"XY", Key::XY},{"XYY", Key::XYY}} );
	connect( cb_type, &ComboBox2::valueChanged,
			 this, &PlotSettingsWidget::onTypeChanged );
	wid_general->addSeparator();
	wid_general->addSection( "Axes" );
	cb_axis1 = wid_general->addComboBox( Key::Axis1, "Axis 1 :" );
	cb_axis2 = wid_general->addComboBox( Key::Axis2, "Axis 2 :" );
	cb_axis3 = wid_general->addComboBox( Key::Axis3, "Axis 3 :" );
	wid_general->addSeparator();
	wid_general->addSection( "Data" );
	cb_data1 = wid_general->addComboBox( Key::Data1, "Data 1 :" );
	cb_data2 = wid_general->addComboBox( Key::Data2, "Data 2 :" );
	cb_data3 = wid_general->addComboBox( Key::Data3, "Data 3 :" );
//	cb_pos = wid_general->addKeySelector( Key(Key::Position), "Position :" );
//	cb_pos->setData( x_data );
//	cb_frames = wid_general->addComboBox( Key(Key::Frame), "Parent Frame :" );
//	wid_general->addDoubleEdit( Key(Key::Min), "Min :" );
//	wid_general->addDoubleEdit( Key(Key::Max), "Max :" );
//	cb_scale = wid_general->addKeySelector( Key(Key::Scale), "Scale :" );
//	cb_scale->setData( {{"Linear",Key::Linear},{"Log",Key::Log},{"Log10",Key::Log10},{"LogX",Key::LogX},{"Reciprocal",Key::Reciprocal},{"Reciprocal Offset",Key::OffsetReciprocal}} );
//	wid_general->addDoubleEdit( Key(Key::Scale, Key::Offset), "Scale Offset :" );
//	wid_general->addDoubleEdit( Key(Key::Shift), "Shift :" );
//	wid_general->addSection( "Line Style" );
//	PenModifier* mod = wid_general->addPenEditor( Key(Key::Line,Key::Style), "Line Style" );
//	mod->desactivateJoin();

	wid_line = new SettingsWidget(this);
	wid_line->addSection( "Style" );
	wid_line->addPenEditor( Key(Key::Line,Key::Style), "Style" );
	wid_line->addDoubleEdit( Key(Key::Line,Key::Gap), "Gap :" );

	wid_symbols = new SettingsWidget(this);
	wid_symbols->addSection("Borders Style");
	/*mod = */wid_symbols->addPenEditor( Key(Key::Symbols,Key::Borders,Key::Style), "Symbols Style" );
//	cb_ticks_pos = wid_symbols->addKeySelector( Key(Key::Ticks, Key::Position), "Position :" );
//	QList<QPair<QString,QVariant>> lst = x_data;
//	lst.append( o_data );
//	cb_ticks_pos->setData( lst );
//	wid_symbols->addSection( "Major" );
//	wid_symbols->addIntegerEdit( Key(Key::Ticks, Key::Size), "Size :" );
//	wid_symbols->addDoubleEdit( Key(Key::Ticks, Key::Increment), "Increment :" );
//	wid_symbols->addIntegerEdit( Key(Key::Ticks, Key::Numbers), "Numbers :" );
//	wid_symbols->addSection( "Minor" );
//	wid_symbols->addIntegerEdit( Key(Key::Minor, Key::Ticks, Key::Size), "Size :" );
//	wid_symbols->addDoubleEdit( Key(Key::Minor, Key::Ticks, Key::Increment), "Increment :" );
//	wid_symbols->addIntegerEdit( Key(Key::Minor, Key::Ticks, Key::Numbers), "Numbers :" );

	wid_area = new SettingsWidget(this);
//	cb_title_pos = wid_area->addKeySelector( Key(Key::Title, Key::Position), "Position :" );
//	cb_title_pos->setData( lst );
//	wid_area->addAnchorSelector( Key(Key::Title,Key::Anchor), "Anchor :" );
//	wid_area->addDoubleEdit( Key(Key::Title,Key::Rotation), "Rotation :" );
//	wid_area->addSection( "Title Text :");
//	wid_area->addSetting( Key(Key::Title,Key::Text), "Title", "", SettingItem::TextEdit  );
	wid_area->addSection("Borders Style");
	/*mod = */wid_area->addPenEditor( Key(Key::Area,Key::Borders,Key::Style), "Borders Style" );

	wid_bars = new SettingsWidget(this);
//	cb_grids_pos = wid_bars->addKeySelector( Key(Key::Grids, Key::Position), "Position :" );
//	cb_grids_pos->setData( lst );
//	wid_bars->addSection( "Major Grids Style" );
//	mod = wid_bars->addPenEditor( Key(Key::Grids,Key::Style), "Grids Style" );
//	mod->desactivateJoin();
//	wid_bars->addSection( "Minor Grids Style" );
	wid_bars->addSection("Borders Style");
	/*mod = */wid_bars->addPenEditor( Key(Key::Bars,Key::Borders,Key::Style), "Borders Style" );
//	mod->desactivateJoin();
//	mod->desactivateCap();

	wid_errorBars = new SettingsWidget(this);
	//	cb_axis1 = wid_general->addComboBox( Key::Data1, "Data 1 :" );
	//	cb_axis2 = wid_general->addComboBox( Key::Data2, "Data 2 :" );
	//	cb_axis3 = wid_general->addComboBox( Key::Data3, "Data 3 :" );
	wid_errorBars->addSection("Line Style");
	/*mod = */wid_errorBars->addPenEditor( Key(Key::ErrorBars,Key::Line,Key::Style), "Borders Style" );

	wid_labels = new SettingsWidget(this);
	cb_labels_pos = wid_labels->addKeySelector( Key(Key::Labels,Key::Position), "Position :" );
//	cb_labels_pos->setData( lst );
	wid_labels->addAnchorSelector( Key(Key::Labels,Key::Anchor), "Anchor :" );
	wid_labels->addComboBox( Key(Key::Labels,Key::Rotation), "Rotation :" );

	tab_elements = new QTabWidget(this);
	tab_elements->addTab( wid_general, "General" );
	tab_elements->addTab( wid_line, "Line" );
	tab_elements->addTab( wid_symbols, "Symbols" );
	tab_elements->addTab( wid_area, "Area" );
	tab_elements->addTab( wid_bars, "Bars" );
	tab_elements->addTab( wid_errorBars, "Error Bars" );
	tab_elements->addTab( wid_labels, "Labels" );

	lay_main->addRow( tab_elements );
}
/**********************************************/
/**********************************************/
void PlotSettingsWidget::connectSettings( Settings* settings )
{
	SettingsWidget::connectSettings( settings );
	wid_general->connectSettings( settings );
	wid_line->connectSettings( settings );
	wid_symbols->connectSettings( settings );
	wid_area->connectSettings( settings );
	wid_bars->connectSettings( settings );
	wid_errorBars->connectSettings( settings );
	wid_labels->connectSettings( settings );
	this->updateSettings( settings );
}

void PlotSettingsWidget::updateSettings(Settings* settings)
{
	SettingsWidget::updateSettings( settings );
	wid_general->updateSettings( settings );
	wid_line->updateSettings( settings );
	wid_symbols->updateSettings( settings );
	wid_area->updateSettings( settings );
	wid_bars->updateSettings( settings );
	wid_errorBars->updateSettings( settings );
	wid_labels->updateSettings( settings );
}
/**********************************************/
/**********************************************/
void PlotSettingsWidget::onTypeChanged( const QVariant& value )
{
	Key key = value;
//	QList<QPair<QString,QVariant>> ax_lst;
//	QList<QPair<QString,QVariant>> tk_lst;
//	QList<QPair<QString,QVariant>> lb_lst;
//	QList<QPair<QString,QVariant>> tl_lst;
//	QList<QPair<QString,QVariant>> gd_lst;
	if ( key == Key::XYY )
	{
		cb_axis3->setEnabled(true);
		cb_data3->setEnabled(true);
//		ax_lst.append( x_data );
//		ax_lst.append( {"Center", Key::Center} );
//		tk_lst.append( x_data );
//		tk_lst.append( {"Center", Key::Center} );
//		tk_lst.append( o_data );
//		lb_lst.append( x_data );
//		lb_lst.append( o_data );
//		tl_lst.append( {{"Top-Left", Key::TopLeft},{"Top", Key::Top},{"Top-Right", Key::TopRight},{"Bottom-Left", Key::BottomLeft},{"Bottom", Key::Bottom},{"Bottom-Right", Key::BottomRight}} );
//		_settings->addSetting( Key(Key::Labels,Key::Anchor), Key::Top );
//		_settings->addSetting( Key(Key::Title,Key::Anchor), Key::Top );
	}
	else //if ( key == Key::Y )
	{
		cb_axis3->setEnabled(false);
		cb_data3->setEnabled(false);
//		ax_lst.append( y_data );
//		ax_lst.append( {"Center", Key::Center} );
//		tk_lst.append( y_data );
//		tk_lst.append( {"Center", Key::Center} );
//		tk_lst.append( o_data );
//		lb_lst.append( y_data );
//		lb_lst.append( o_data );
//		tl_lst.append( {{"Top-Left", Key::TopLeft},{"Left", Key::Left},{"Bottom-Left", Key::BottomLeft},{"Top-Right", Key::TopRight},{"Right", Key::Right},{"Bottom-Right", Key::BottomRight}} );
//		_settings->addSetting( Key(Key::Labels,Key::Anchor), Key::Right );
//		_settings->addSetting( Key(Key::Title,Key::Anchor), Key::Bottom );
	}
//	else if ( key == Key::Polar )
//	{
//		ax_lst = polar_data;
//		tk_lst.append( o_data );
//	}
//	cb_pos->setData( ax_lst );
//	cb_ticks_pos->setData( tk_lst );
//	cb_labels_pos->setData( lb_lst );
//	cb_title_pos->setData( tl_lst );
//	cb_grids_pos->setData( gd_lst );
}
/**********************************************/
/**********************************************/
void PlotSettingsWidget::onFramesChanged()
{
//	cb_frames->clear();
//	for ( FrameItem* frame : _graph->frames() )
//		cb_frames->addItem(frame->name());
}
/**********************************************/
/**********************************************/
