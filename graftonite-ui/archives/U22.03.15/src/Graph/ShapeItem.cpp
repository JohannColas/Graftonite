#include "ShapeItem.h"
/**********************************************/
#include "GraphView.h"
/**********************************************/
/**********************************************/
/**********************************************/
ShapeItem::ShapeItem( QGraphicsItem* parent )
	: GraphicsItem(parent)
{
	init();
}
ShapeItem::ShapeItem( const QString& name, QGraphicsItem* parent )
	: GraphicsItem(name, parent)
{
	init();
}
void ShapeItem::init()
{
	_item_type = Key::Shape;
}
/**********************************************/
/**********************************************/
void ShapeItem::addSetting( const Key& key, const QVariant& value )
{
//	if ( key == Key::Name ||
//	     key == Key::Type )
//		return Element::set( key, value );
//	else
//		Graph::warningLog( "Unknown key \""+key.fullKeys()+"\" for shpae element !" );
	//	return false;
}
/**********************************************/
/**********************************************/
void ShapeItem::updateGraphItem()
{

	emit changed();
}
/**********************************************/
/**********************************************/
