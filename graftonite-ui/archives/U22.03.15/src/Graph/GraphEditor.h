#ifndef GRAPHEDITOR_H
#define GRAPHEDITOR_H
/**********************************************/
#include <QWidget>
/**********************************************/
//class QTreeWidget;
class QTreeWidgetItem;
class QStackedWidget;
class QSplitter;
class QVBoxLayout;
/**********************************************/
class GraphSettingsWidget;
class FrameSettingsWidget;
class AxisSettingsWidget;
class PlotSettingsWidget;
class LegendSettingsWidget;
class TitleSettingsWidget;
class ShapeSettingsWidget;
/**********************************************/
class GraphTree;
class GraphView;
class GraphItem;
/**********************************************/
class ProjectItem2;
/**********************************************/
/**********************************************/
/**********************************************/
class GraphEditor
        : public QWidget
{
	Q_OBJECT
public:
	~GraphEditor();
	GraphEditor( QWidget *parent = nullptr );
	/******************************************/
	void setGraphPath( const QString& path );
	void setGraphFile( ProjectItem2* graph_item );
	void setAvailableData( const QStringList& availableData );
	/******************************************/
private slots:
	void graphChanged();
	void onItemPressed( QTreeWidgetItem* item, int column );
	void closeEvent( QCloseEvent* event ) override;
	/******************************************/
	void on_pb_newLayer_released();
	void on_pb_newAxis_released();
	void on_pb_newCurve_released();
	void on_pb_remove_released();
	/******************************************/
	void newFrame();
	void newAxis();
	void newPlot();
	void newLegend();
	void newTitle();
	void newShape();
	void resetGraph();
	void deleteItem();
	/******************************************/
	void saveGraph();
	/******************************************/
	void on_pb_save_released();
	/******************************************/
	void hideEvent( QHideEvent* event ) override;
private:
	GraphItem* _graph_item = nullptr;
	QVBoxLayout* lay_main = nullptr;
	GraphView* view_graph = nullptr;
	GraphTree* tree_graph = nullptr;
	QStackedWidget* cont_settings = nullptr;
	QSplitter* splitter = nullptr;
	QSplitter* sp_sidebar = nullptr;
//	QVBoxLayout* lay_sidebar = nullptr;
	/******************************************/
	GraphSettingsWidget* wid_graph = nullptr;
	FrameSettingsWidget* wid_frame = nullptr;
	AxisSettingsWidget* wid_axis = nullptr;
	PlotSettingsWidget* wid_plot = nullptr;
	LegendSettingsWidget* wid_legend = nullptr;
	TitleSettingsWidget* wid_title = nullptr;
	ShapeSettingsWidget* wid_shape = nullptr;
	/******************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // GRAPHEDITOR_H
