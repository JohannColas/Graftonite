#include "GraphSettingsWidget.h"
/**********************************************/
#include "../Commons/Settings.h"
#include "../BasicWidgets/PenWidgets.h"
/**********************************************/
/**********************************************/
/**********************************************/
GraphSettingsWidget::GraphSettingsWidget( QWidget *parent )
    : SettingsWidget(parent)
{
	addLineEdit( Key::Name, "Graph :" );

	addSeparator();

	addSection( "Frame" );
	addCheckBox( Key::Hide, "Hide :" );

//	addSeparator();

	addSection( "Background" );
	addColorSelector( Key(Key::Background,Key::Color), "Color :" );

//	addSeparator();

	addSection( "Borders" );
	PenModifier* mod_bd = addPenEditor( Key::Borders, "Borders Style" );
	mod_bd->desactivateCap();
}
/**********************************************/
/**********************************************/
void GraphSettingsWidget::connectSettings( Settings* settings )
{
	SettingsWidget::connectSettings( settings );
}
/**********************************************/
/**********************************************/
