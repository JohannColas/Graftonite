#include "ShapeSettingsWidget.h"
/**********************************************/
/**********************************************/
/**********************************************/
/**********************************************/
ShapeSettingsWidget::ShapeSettingsWidget( QWidget* parent )
    : SettingsWidget(parent)
{
	addLineEdit( Key::Name, "Shape :" );
}
/**********************************************/
/**********************************************/
void ShapeSettingsWidget::connectSettings( Settings* settings )
{
	SettingsWidget::connectSettings(settings);

}
/**********************************************/
/**********************************************/
