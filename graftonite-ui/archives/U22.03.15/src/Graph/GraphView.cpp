#include "GraphView.h"
/**********************************************/
#include <QScrollBar>
/**********************************************/
#include <QSvgGenerator>
#include <QPdfWriter>
/**********************************************/
#include <QGraphicsRectItem>
#include <QGraphicsTextItem>
/**********************************************/
/**********************************************/
GraphView::~GraphView()
{
	//	delete graphItem;
	//	while ( !layerItems.isEmpty() )
	//	{
	//		delete layerItems.takeFirst();
	//	}

	delete contextMenu;
	delete acSaveGraph;
	delete acExportGraph;
}
/**********************************************/
GraphView::GraphView( QWidget *parent )
    : QGraphicsView( parent )
{
	init();
	//

//	QGraphicsRectItem* item = new QGraphicsRectItem;
//	QPen pen;
//	pen.setColor( QColor(255,0,0) );
//	pen.setWidth( 20 );
//	item->setPen( pen );
//	item->setBrush( QColor(239,239,239) );
//	item->setRect( 0,0,1600,1200 );
//	this->scene()->addItem( item );

//	QGraphicsTextItem* text_item = new QGraphicsTextItem;
//	QFont font;
	//	font.setPointSize( 24 );
//	text_item->setFont( font );
//	text_item->setDefaultTextColor(QColor(51,51,51));
//	text_item->setPlainText( "From QGraphicsTextItem" );
//	this->scene()->addItem( text_item );


	// Définifion of graphItem
//	graphItem = new GraphItem();
//	connect(graphItem, &GraphItem::doubleClicked, this, &GraphView::autoFit );
//	connect(graphItem, &GraphItem::scaled, this, &GraphView::resizeView );
//	scene()->addItem( graphItem );

	// Initialization of the contextual menu
	contextMenu = new QMenu(this);
	contextMenu->addAction( acFitToScene );
	connect( acFitToScene, &QAction::triggered, this, &GraphView::fitToViewport );
	contextMenu->addSeparator();
	contextMenu->addAction( acSaveGraph );
	connect( acSaveGraph, &QAction::triggered, this, &GraphView::saveGraph );
	contextMenu->addAction( acSaveGraph );
	connect( acSaveGraph, &QAction::triggered, this, &GraphView::save );
	contextMenu->addAction( acExportGraph );
	connect( acExportGraph, &QAction::triggered, this, &GraphView::exportGraph );
	/******************************************/
}
/**********************************************/
/**********************************************/
/* */
// Initialisation of the GraphView
void GraphView::init()
{
	// Set alignement of the item
	setAlignment( Qt::AlignHCenter | Qt::AlignTop );
	// Set render
	setRenderHints( QPainter::Antialiasing | QPainter::SmoothPixmapTransform );
	// Remove the ScrollBar
	//this->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	//this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	//
	//setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
	//this->setTransformationAnchor( QGraphicsView::NoAnchor );
	//this->setResizeAnchor(QGraphicsView::AnchorUnderMouse);
	//this->setCacheMode(QGraphicsView::CacheBackground);
	//this->setDragMode(QGraphicsView::RubberBandDrag);
	//this->setDragMode(QGraphicsView::ScrollHandDrag);
	this->setViewportUpdateMode( QGraphicsView::NoViewportUpdate );
	// Adding a scene
	setScene( new QGraphicsScene() );
	//
	//connect( this, &GraphView::resized, this, &GraphView::resizeView );
}
/**********************************************/
/**********************************************/
/* PUBLIC SLOTS */
/**********************************************/
void GraphView::save()
{
//	scene()->clear();

//	QGraphicsTextItem* text_item = new QGraphicsTextItem;
//	QFont font;
//	font.setPointSize( 48 );
//	text_item->setFont( font );
//	text_item->setDefaultTextColor(QColor(51,51,51));
//	text_item->setPlainText( "A text 2" );
//	text_item->setPos( -200, -544 );
//	this->scene()->addItem( text_item );
//	this->repaint();
//	this->fitToViewport();
}
/**********************************************/
/**********************************************/
/* */
//void GraphView::update( GraphSettings* settings )
//{
//	// Updating graphItem
//	graphItem->update( settings );
//	// Updating titleItem
//	// Updating layerItems
//	for( auto elm : graphItem->backgroundBed()->childItems() )
//	{
//		delete elm;
//	}
//	while( !layerItems.isEmpty() )
//	{
//		delete layerItems.takeFirst();
//	}
//	for( LayerSettings* elm : settings->getLayersSettings() )
//	{
//		LayerItem* layerTMP = new LayerItem( graphItem->innerAxisBed(), graphItem->backgroundBed() );
//		layerTMP->update( elm );
//		layerItems.append( layerTMP );
//	}
//	// Updating AxisItems
//	while( !axisItems.isEmpty() )
//	{
//		delete axisItems.takeFirst();
//	}
//	for( AxisSettings* elm : settings->getAxesSettings() )
//	{
//		AxisItem* axisTMP = new AxisItem( graphItem->axisBed(),
//										  graphItem->majorGridsBed(),
//										  graphItem->minorGridsBed() );
//		GraphRect rectTMP( 50, 50, 100, 100 );
//		for( LayerSettings* layerSTMP : settings->getLayersSettings() )
//		{
//			if( layerSTMP->getID() == elm->getLayerID() )
//			{
//				rectTMP = layerSTMP->getRect();
//				break;
//			}
//		}
//		axisTMP->update( elm, rectTMP );
//		axisItems.append( axisTMP );
//	}
//	// Updating CurveItems
//	while( !curveItems.isEmpty() )
//	{
//		delete curveItems.takeFirst();
//	}
//	for( CurveSettings* elm : settings->getCurvesSettings() )
//	{
//		CurveItem* curveTMP = new CurveItem( graphItem->curvesBed() );
//		GraphRect rectTMP( 50, 50, 100, 100 );
//		//		for( LayerSettings* layerSTMP : settings->getLayersSettings() )
//		//		{
//		//			if( layerSTMP->getID() == elm->getLayerID() )
//		//			{
//		//				rectTMP = layerSTMP->getRect();
//		//				break;
//		//			}
//		//		}
//		curveTMP->update( elm );
//		curveItems.append( curveTMP );
//	}
//	// Updating ShapeItems
//}
/**********************************************/
/**********************************************/
/* */
void GraphView::clearAllItems()
{
//	// Deleting graphItem
//	if( graphItem != nullptr )
//	{
//		delete graphItem;
//		graphItem = nullptr;
//	}
//	// Deleting titleItem
//	if( titleItem != nullptr )
//	{
//		delete titleItem;
//		titleItem = nullptr;
//	}
//	// Deleting layerItems
//	while( !layerItems.isEmpty() )
//	{
//		delete layerItems.takeFirst();
//	}
//	// Deleting axisItems
//	while( !axisItems.isEmpty() )
//	{
//		delete axisItems.takeFirst();
//	}
//	// Deleting curveItems
//	while( !curveItems.isEmpty() )
//	{
//		delete curveItems.takeFirst();
//	}
//	// Deleting curveItems
//	while( !shapeItems.isEmpty() )
//	{
//		delete shapeItems.takeFirst();
	//	}
}

/**********************************************/
void GraphView::translateScene( const QPointF& translation, const QPoint& cursor_pos )
{

	//
	QPointF botrig = mapToScene( viewport()->rect().bottomRight() );
	QPointF toplef = mapToScene( viewport()->rect().topLeft() );
	if ( scene()->width() < botrig.x() - toplef.x() &&
	     scene()->height() < botrig.x() - toplef.x() )
	{
		if ( cursor_pos.x() < 0 || cursor_pos.y() < 0 )
		{
			Qt::Alignment align = alignment();
			if ( cursor_pos.y() < 0 )
			{
				if ( (align & Qt::AlignTop) && cursor_pos.y() == -2 )
				{
					align = (align & ~Qt::AlignTop) | Qt::AlignVCenter;
				}
				else if ( (align & Qt::AlignBottom) && cursor_pos.y() == -1 )
				{
					align = (align & ~Qt::AlignBottom) | Qt::AlignVCenter;
				}
				else if ( align & Qt::AlignVCenter )
				{
					align = (align & ~Qt::AlignVCenter) | (cursor_pos.y() == -1 ? Qt::AlignTop : Qt::AlignBottom);
				}
			}
			if ( cursor_pos.x() < 0 )
			{
				if ( ((align & Qt::AlignLeft) || (align & Qt::AlignLeading)) && cursor_pos.x() == -2 )
				{
					align = (align & ~Qt::AlignLeft) | Qt::AlignHCenter;
				}
				else if ( ((align & Qt::AlignRight) || (align & Qt::AlignTrailing)) && cursor_pos.x() == -1 )
				{
					align = (align & ~Qt::AlignRight) | Qt::AlignHCenter;
				}
				else if ( align & Qt::AlignHCenter )
				{
					align = (align & ~Qt::AlignHCenter) | (cursor_pos.x() == -1 ? Qt::AlignLeft : Qt::AlignRight);
				}
			}
			this->setAlignment( align );
			return;
		}
		int vp_w = 0.33*viewport()->width();
		int vp_h = 0.33*viewport()->height();
		Qt::Alignment align;
		if ( cursor_pos.x() < vp_w )
			align = Qt::AlignLeft;
		else if ( cursor_pos.x() < 2*vp_w )
			align = Qt::AlignHCenter;
		else
			align = Qt::AlignRight;
		if ( cursor_pos.y() < vp_h )
			align = align | Qt::AlignTop;
		else if ( cursor_pos.y() < 2*vp_h )
			align = align | Qt::AlignVCenter;
		else
			align = align | Qt::AlignBottom;
		this->setAlignment( align );
	}
	else
	{
		horizontalScrollBar()->setValue( horizontalScrollBar()->value() - translation.x() );
		verticalScrollBar()->setValue( verticalScrollBar()->value() - translation.y() );
		translate( translation.x(), translation.y() );
	}

}
/**********************************************/
/**********************************************/
void GraphView::fitToViewport()
{
	_scale = 1;
	this->fitInView( scene()->sceneRect(), Qt::KeepAspectRatio );
}
/**********************************************/
/* */
void GraphView::resizeView()
{
	_scale = 1;
	this->fitInView( scene()->sceneRect(), Qt::KeepAspectRatio );
}
/**********************************************/
void GraphView::exportGraph()
{
	//  High Quality Image !!!
	short mult = 10;
	int x = 0;//graphItem->boundingRect().x();
	int y = 0;//graphItem->boundingRect().y();
	int width = 1000;//graphItem->boundingRect().width();
	int height = 1000;//graphItem->boundingRect().height();
	QImage image(mult*width, mult*height, QImage::Format_ARGB32);  // Create the image with the exact size of the shrunk scene
	// Selections would also render to the file
//	this->scene()->setSceneRect( x, y, width, height );
	scene()->setBackgroundBrush( QBrush( Qt::NoBrush ) );
	//	QBrush local = _graph->brush();
	//	_graph->setBrush( QBrush( Qt::NoBrush ) );
	image.fill( Qt::transparent );
	QPainter painter(&image);
	painter.setRenderHint(QPainter::Antialiasing);
	this->scene()->render(&painter);
	image.save("AAB.png");
	//	_graph->setBrush( local );

	//Save in SVG
	QSvgGenerator svgGen;
	svgGen.setFileName( "AAA.svg" );
//	svgGen.setSize(this->sceneRect().size());
	svgGen.setViewBox(this->sceneRect());//QRect(0, 0, 1000, 1000));
	svgGen.setTitle(tr("SVG Generator Example Drawing"));
	svgGen.setDescription(tr("An SVG drawing created by the SVG Generator "
	                         "Example provided with Qt."));
	QPainter painter2( &svgGen );
	this->scene()->render( &painter2 );

	QPdfWriter pdfRend( "sfdf.pdf" );
	pdfRend.setPageSize( QPageSize::Custom );
	pdfRend.setPageOrientation( QPageLayout::Portrait );
	QPainter painter3(&pdfRend);
	this->scene()->render( &painter3);
}
/**********************************************/
/**********************************************/
/* PRIVATE METHODS */
/**********************************************/
void GraphView::zoomAt( const QPoint& centerPos, double factor )
{
	if ( _scale*factor < 0.1 )
		factor = 0.1/_scale;
	else if ( _scale*factor > 100 )
		factor = 100/_scale;
	_scale *= factor;

	QPointF targetScenePos = mapToScene(centerPos);
	ViewportAnchor oldAnchor = this->transformationAnchor();
	setTransformationAnchor(QGraphicsView::AnchorUnderMouse);

	QTransform matrix = transform();
	matrix.translate( targetScenePos.x(), targetScenePos.y())
	      .scale(factor, factor)
	      .translate(-targetScenePos.x(), -targetScenePos.y());
	setTransform(matrix);

	setTransformationAnchor(oldAnchor);
}
/**********************************************/
/**********************************************/
/* PRIVATE SLOTS */
/**********************************************/
void GraphView::keyPressEvent( QKeyEvent* event )
{
	if ( event->modifiers() == Qt::ControlModifier )
	{
		QPointF botrig = mapToScene( viewport()->rect().bottomRight() );
		QPointF toplef = mapToScene( viewport()->rect().topLeft() );
		if ( scene()->width() < botrig.x() - toplef.x() &&
		     scene()->height() < botrig.x() - toplef.x() )
		{
			if ( event->key() == Qt::Key_Left )
			{
				translateScene( {100,0}, {-1,0} );
			}
			else if ( event->key() == Qt::Key_Right )
			{
				translateScene( {-100,0}, {-2,0} );
			}
			else if ( event->key() == Qt::Key_Up)
			{
				translateScene( {0,100}, {0,-1} );
			}
			else if ( event->key() == Qt::Key_Down )
			{
				translateScene( {0,-100}, {0,-2} );
			}
		}
	}
	QGraphicsView::keyReleaseEvent( event );
}
/**********************************************/
void GraphView::mousePressEvent( QMouseEvent* event )
{
//	scene()->setFocus( Qt::OtherFocusReason );
//	scene()->setStickyFocus(true);
//viewport()->setFocusProxy(0);
//setFocus();

	if ( event->button() == Qt::MouseButton::LeftButton )
	{
		_movable = true;
		_old_pos = event->pos();
		this->setCursor(Qt::ClosedHandCursor);
	}
	QGraphicsView::mousePressEvent( event );
}
/**********************************************/
void GraphView::mouseReleaseEvent( QMouseEvent* event )
{
	this->setCursor( Qt::CursorShape::ArrowCursor );
	_movable = false;
	QGraphicsView::mouseReleaseEvent( event );
}
/**********************************************/
void GraphView::mouseMoveEvent( QMouseEvent* event )
{
	if ( _movable )
	{
		QPoint pos = event->pos();
		QPointF translation = mapToScene( pos ) - mapToScene( _old_pos );
		_old_pos = pos;
		//
		translateScene( translation, pos );
	}
	QGraphicsView::mouseMoveEvent( event );
}
/**********************************************/
void GraphView::wheelEvent( QWheelEvent* event )
{
	double angle = event->angleDelta().y();
	double factor = pow(1.0015, angle); // smoother zoom
	zoomAt( QCursor::pos(), factor );
}
/**********************************************/
void GraphView::resizeEvent( QResizeEvent *event )
{
	Q_UNUSED( event );
	emit resized();
}
/**********************************************/
void GraphView::contextMenuEvent( QContextMenuEvent *event )
{
	contextMenu->exec( event->globalPos() );
}
/**********************************************/
void GraphView::paintEvent( QPaintEvent* event )
{
	QPainter painter( viewport() );
	if ( _gridVisible )
	{
		const int wView = viewport()->width(), hView = viewport()->height();
		QColor white = QColor( 255,255,255 );
		QColor gray = QColor( 235,235,235 );
		QColor brush = gray;
		QColor brush2;
		for ( int x = 0 / 2; x < wView; x += _gridSize )
		{
			if ( brush == gray )
				brush = white;
			else
				brush = gray;
			brush2 = brush;
			for ( int y = 0 / 2; y < hView; y += _gridSize )
			{
				painter.setBrush( brush2 );
				painter.drawRect( x, y, _gridSize, _gridSize );
				if ( brush2 == gray )
					brush2 = white;
				else
					brush2 = gray;
			}
		}

		//		qPainter.setPen(_gridColor);
//		for (int x = _gridSize / 2; x < wView; x += _gridSize)
//		{
//			qPainter.drawLine(x, 0, x, hView - 1);
//		}
//		for (int y = _gridSize / 2; y < hView; y += _gridSize)
//		{
//			qPainter.drawLine(0, y, wView - 1, y);
//		}
	}
	QGraphicsView::paintEvent(event);
}
/**********************************************/
/**********************************************/
