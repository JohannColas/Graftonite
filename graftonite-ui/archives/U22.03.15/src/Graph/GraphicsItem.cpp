#include "GraphicsItem.h"
/**********************************************/
#include "GraphView.h"
#include <QGraphicsScene>
/**********************************************/
/**********************************************/
/**********************************************/
GraphicsItem::~GraphicsItem()
{
//	QGraphicsItemGroup::removeFromGroup(item_bounding_rect);
//	delete item_bounding_rect;
}
GraphicsItem::GraphicsItem( GraphItem* parent )
	: QObject(), QGraphicsItemGroup(), QTreeWidgetItem(QTreeWidgetItem::UserType)
{
	QTreeWidgetItem::setFlags( QTreeWidgetItem::flags() | Qt::ItemIsEditable );
}
/**********************************************/
GraphicsItem::GraphicsItem( QGraphicsItem* parent )
	: QObject(), QGraphicsItemGroup(parent), QTreeWidgetItem(QTreeWidgetItem::UserType)
{
	QTreeWidgetItem::setFlags( QTreeWidgetItem::flags() | Qt::ItemIsEditable );
}
/**********************************************/
/**********************************************/
GraphicsItem::GraphicsItem( const QString& name, QGraphicsItem* parent )
	: QObject(), QGraphicsItemGroup(parent), QTreeWidgetItem(QTreeWidgetItem::UserType)
{
	QTreeWidgetItem::setData( 0, Qt::DisplayRole, name );
	QTreeWidgetItem::setFlags( QTreeWidgetItem::flags() | Qt::ItemIsEditable );
	_settings.addSetting(Key::Name,name);
}
Key GraphicsItem::itemType() const
{
	return _item_type;
}
QString GraphicsItem::name() const
{
	QString name = "[No name]";
	_settings.getString(name,Key::Name);
	return name;
}
/**********************************************/
/**********************************************/
/* Settings Management */
/**********************************************/
Settings& GraphicsItem::settings()
{
	return _settings;
}
/**********************************************/
Settings GraphicsItem::settings() const
{
	return _settings;
}
/**********************************************/
void GraphicsItem::addSetting( const Key& key, const QVariant& value )
{
	_settings.addSetting( key, value );
}
/**********************************************/
/**********************************************/
bool GraphicsItem::setting( QVariant& boolean, const Key& key )
{
	return true;
}
bool GraphicsItem::setting( QString& boolean, const Key& key )
{
	return true;
}
bool GraphicsItem::setting( bool& boolean, const Key& key )
{
	return true;
}
bool GraphicsItem::setting( int& integer, const Key& key )
{
	return true;
}
bool GraphicsItem::setting( double& doubl, const Key& key )
{
	return true;
}
bool GraphicsItem::setting( QBrush& integer, const Key& key )
{
	return true;
}
bool GraphicsItem::setting( QPen& integer, const Key& key )
{
	return true;
}
/**********************************************/
/**********************************************/
QVariant GraphicsItem::data( int column, int role ) const
{
	if ( column == 0 && role == Qt::DisplayRole )
		return _settings.getString(Key::Name);
	return QTreeWidgetItem::data( column, role );
}
void GraphicsItem::setData( int column, int role, const QVariant& value )
{
	if ( column == 0 && role == Qt::DisplayRole )
		_settings.addSetting(Key::Name, value);

	QTreeWidgetItem::setData( column, role, value );
}
/**********************************************/
/**********************************************/
void GraphicsItem::onSettingsChanged()
{
	bool hide = false; _settings.getBool( hide, Key::Hide );
	// Draw Rect
	if ( !hide )
	{
		QPen pen = _settings.getPen(Key::Borders);
		QBrush brush = _settings.getBrush(Key::Background);
		if ( item_bounding_rect == nullptr )
			item_bounding_rect = new QGraphicsRectItem;
		item_bounding_rect->setBrush( brush );
		item_bounding_rect->setPen( pen );
		item_bounding_rect->setRect( this->boundingRect() );
	}
}
/**********************************************/
/**********************************************/
void GraphicsItem::updateGraphItem()
{

	emit changed();
}
/**********************************************/
/**********************************************/
void GraphicsItem::mouseReleaseEvent( QGraphicsSceneMouseEvent* event )
{
//	QPen pen = QPen(QColor(244,10,10), 3);
//	if ( item_bounding_rect == nullptr )
//		item_bounding_rect = new QGraphicsRectItem;
//	item_bounding_rect->setPen( pen );
//	item_bounding_rect->setRect( this->boundingRect() );
	emit treeItemSelected( this );
	QGraphicsItem::mouseReleaseEvent( event );
}
/**********************************************/
/**********************************************/
