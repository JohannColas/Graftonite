#include "AxisItem.h"
/**********************************************/
#include <QPainterPath>
#include <QPen>
/**********************************************/
#include "GraphView.h"
#include "GraphItem.h"
#include "FrameItem.h"
/**********************************************/
/**********************************************/
/**********************************************/
AxisItem::~AxisItem()
{
	while ( !childItems().isEmpty() )
	{
		QGraphicsItem* tick = childItems().first();
		this->removeFromGroup(tick);
		delete tick;
	}
}
AxisItem::AxisItem( QGraphicsItem* parent )
	: GraphicsItem(parent)
{
	init();
}
AxisItem::AxisItem( const QString& name, QGraphicsItem* parent )
	: GraphicsItem(name, parent)
{
	init();
}
/**********************************************/
void AxisItem::init()
{
	_item_type = Key::Axis;
	// Default settings
	_settings.addSetting( Key::Min, "0" );
	_settings.addSetting( Key::Max, "1" );
	_settings.addSetting( Key::Type, Key::X );
	_settings.addSetting( Key::Scale, Key::Linear );
	_settings.addSetting( Key::Position, Key::Bottom );
	_settings.addSetting( Key(Key::Line,Key::Style), QPen(QColor(Qt::black),4,Qt::SolidLine, Qt::RoundCap) );
	_settings.addSetting( Key(Key::Line, Key::Width), "3" );
	_settings.addSetting( Key(Key::Line, Key::Cap), Key::Square );
	_settings.addSetting( Key(Key::Ticks, Key::Position), Key::Outside );
	_settings.addSetting( Key(Key::Ticks, Key::Increment), "0.1" );
	_settings.addSetting( Key(Key::Ticks, Key::Size), "10" );
	_settings.addSetting( Key(Key::Minor, Key::Ticks, Key::Increment), "0.05" );
	_settings.addSetting( Key(Key::Minor, Key::Ticks, Key::Size), "5" );
	_settings.addSetting( Key(Key::Labels, Key::Position), Key::Outside );
	_settings.addSetting( Key(Key::Labels, Key::Anchor), Key::Top );
	_settings.addSetting( Key(Key::Labels, Key::Font, Key::Size), "24" );
	_settings.addSetting( Key(Key::Title, Key::Position), Key::Outside );
	_settings.addSetting( Key(Key::Title, Key::Text), "Title" );
	_settings.addSetting( Key(Key::Title, Key::Font, Key::Size), "30" );
	_settings.addSetting( Key(Key::Title, Key::Anchor), Key::Top );
	_settings.addSetting( Key(Key::Title, Key::Rotation), 0 );
	_settings.addSetting( Key(Key::Grids,Key::Style), Key::BothSides );
	_settings.addSetting( Key(Key::Grids,Key::Style), QPen(QColor(Qt::darkGray),2,Qt::NoPen, Qt::RoundCap) );
	_settings.addSetting( Key(Key::Minor,Key::Grids,Key::Style), QPen(QColor(Qt::gray),2,Qt::NoPen, Qt::RoundCap) );
	//
	connect( &_settings, &Settings::changed,
			 this, &AxisItem::updateGraphItem );
}
/**********************************************/
/**********************************************/
double AxisItem::scale( double real_value )
{
	if ( _scale == Key::Log10 )
		return log10(real_value);
	else if ( _scale == Key::Log )
		return log(real_value);
	else if ( _scale == Key::LogX )
		return log(real_value)/log(_scale_option);
	else if ( _scale == Key::Reciprocal )
		return (1/real_value);
	else if ( _scale == Key::OffsetReciprocal )
		return (1/(real_value+_scale_option));
	return real_value;
}
/**********************************************/
QPointF AxisItem::deviceCoordinates( double real_value )
{
	double sc_rv = AxisItem::scale(real_value);
	double x = a_x*sc_rv + b_x;
	double y = a_y*sc_rv + b_y;
	return QPointF(x,y);
}
/**********************************************/
void AxisItem::CalculateScaleCoefficients()
{
	double scaleOpt = 0; _settings.getDouble( scaleOpt, Key(Key::Scale, Key::Option) );
	// _min & _max checkers
	if ( _scale == Key::Log ||
		 _scale == Key::Log10 ||
		 _scale == Key::LogX )
	{
		if ( _min <= 0 )
			_min = 0.1;
		if ( _max <= 0 )
			_max = 1;
	}
	else if ( _scale == Key::Reciprocal )
	{
		if ( _min == 0 )
			_min = 1;
		if ( _max == 0 )
			_max = 10;
	}
	else if ( _scale == Key::OffsetReciprocal )
	{
		if ( _min+scaleOpt == 0 )
			_min = 1-scaleOpt;
		if ( _max+scaleOpt == 0 )
			_max = 10-scaleOpt;
	}
	if ( _min == _max )
		_max = _min+1;
	//
	double sc_min = AxisItem::scale(_min);
	double sc_max = AxisItem::scale(_max);
	double Dx = _line.x2() - _line.x1();
	double Dy = _line.y1() - _line.y2();
	double Drv = sc_max - sc_min;
	a_x = Dx/Drv;
	b_x = _line.x1() - a_x*sc_min;
	a_y = Dy/Drv;
	b_y = _line.y2() - a_y*sc_min;
	//
}
/**********************************************/
void AxisItem::CalculateTicksPosition( QList<QPointF>& ticks_pos, QList<QPointF>& minor_ticks_pos, QList<QString>& labels )
{
	// Get Scale Coefficients
	AxisItem::CalculateScaleCoefficients();
	//
	double Drv = std::abs(_max-_min);
	// Get Ticks number and increment
	int tick_number = 10;
	double tick_increment = Drv/tick_number;
	if ( !_settings.getDouble(tick_increment, Key(Key::Ticks, Key::Increment)) )
		if ( _settings.getInt(tick_number, Key(Key::Ticks, Key::Numbers)) )
			tick_increment = Drv/tick_number;
	// Get Minor Ticks number and increment
	int minor_tick_number = 20;
	double minor_tick_increment = Drv/minor_tick_number;
	if ( !_settings.getDouble(minor_tick_increment, Key(Key::Minor, Key::Ticks, Key::Increment)) )
		if ( _settings.getInt(minor_tick_number, Key(Key::Minor, Key::Ticks, Key::Numbers)) )
			minor_tick_increment = tick_increment/minor_tick_number;
	if ( minor_tick_increment > tick_increment )
		minor_tick_increment = 0.5*tick_increment;
	//
	int tick_factor = tick_increment/minor_tick_increment;
	double mi_tk_int = tick_increment/tick_factor;
	double min_rv;
	double max_rv;
	if ( _min > _max )
		min_rv = _max, max_rv = _min;
	else
		min_rv = _min, max_rv = _max;
	//
	int first_rv_mult = min_rv/mi_tk_int;
	int last_rv_mult = max_rv/mi_tk_int;
	for ( int it = first_rv_mult; it <= last_rv_mult; ++it )
	{
		double rv = it*mi_tk_int;
		if ( rv < min_rv || max_rv < rv )
			continue;
		if ( (it % tick_factor) == 0 )
		{
			ticks_pos.append( deviceCoordinates( rv ) );
			labels.append( QString::number(rv) );
		}
		else
			minor_ticks_pos.append( deviceCoordinates( rv ) );
	}
	//
}
/**********************************************/
/**********************************************/
QRectF AxisItem::frameRect() const
{
	return _frameRect;
}
/**********************************************/
/**********************************************/
void AxisItem::updateGraphItem()
{
	//	Element::getTemplate();
	FrameItem* frame = _graph ? _graph->frame( _settings.getString(Key::Frame) ) : nullptr;
	if ( frame )
		_frameRect = frame->rect();
	// Building Entry Geometry
	// Except for entry part position
	// -----------------------
	_settings.getKey( _type, Key::Type );
	_settings.getKey( _scale, Key::Scale );
	_min = 0;
	_settings.getDouble( _min, Key::Min );
	_max = 1;
	_settings.getDouble( _max, Key::Max );
	//
	// Axis Settings
//	QString type = "X";
	//	get( type, Key::Type );
	Key axis_pos = Key::Bottom;
	_settings.getKey( axis_pos, Key::Position );
	_line = _frameRect.line( axis_pos );
	_boundingRect = _line;

	QPen line_style;// = _settings.getPen(Key::Line);
	_settings.getPen( line_style, Key(Key::Line,Key::Style) );
	if ( _line_item == nullptr )
	{
		_line_item = new QGraphicsPathItem;
		_line_item->setZValue(1);
		addToGroup(_line_item);
	}
	_line_item->setPen( line_style );
	QPainterPath path;
	path.moveTo(_line.p1());
	path.lineTo(_line.p2());
	_line_item->setPath(path);
	// -----------------------
	//
	// Ticks Settings
	Key tick_pos = Key::Outside;
	_settings.getKey( tick_pos, Key(Key::Ticks, Key::Position) );
	double tick_size = 10;
	_settings.getDouble( tick_size, Key(Key::Ticks, Key::Size) );
	double minor_tick_size = 5;
	_settings.getDouble( minor_tick_size, Key(Key::Minor, Key::Ticks, Key::Size) );
	//
	Line::Orientation tick_orientation = Line::Vertical;
	if ( _type == Key::Y )
		tick_orientation = Line::Horizontal;
	//
	double tick_shift = 0;
	if ( tick_pos == Key::Top ||
		 (tick_pos == Key::Outside && axis_pos == Key::Top) ||
		 (tick_pos == Key::Inside && axis_pos == Key::Bottom) ||
		 tick_pos == Key::Left ||
		 (tick_pos == Key::Outside && axis_pos == Key::Left) ||
		 (tick_pos == Key::Inside && axis_pos == Key::Right) )
	{
		tick_shift = -1;
	}
	else if ( tick_pos == Key::Center )
	{
		tick_shift = -0.5;
	}
	//
	QList<QPointF> ticks_pos, minor_ticks_pos; QList<QString> labels;
	AxisItem::CalculateTicksPosition( ticks_pos, minor_ticks_pos, labels );
	_ticks.clear(), _minor_ticks.clear(), _labels.clear();
	//
	for ( int it = 0; it < ticks_pos.size(); ++it )
	{
		QPointF pos = ticks_pos.at(it);

		Line tick = Line( pos, tick_shift*tick_size, tick_size, tick_orientation );

		QGraphicsPathItem* tick_item;
		if ( _tick_items.size() < it+1 )
		{
			tick_item = new QGraphicsPathItem;
			tick_item->setZValue(1);
			_tick_items.append(tick_item);
			addToGroup(tick_item);
		}
		else
			tick_item = _tick_items.at(it);

		tick_item->setPen( line_style );
		QPainterPath path;
		path.moveTo(tick.p1());
		path.lineTo(tick.p2());
		tick_item->setPath(path);

		_ticks.append( tick );
		_boundingRect.expandTo( tick );
	}
	while ( _ticks.size() < _tick_items.size() )
	{
		QGraphicsPathItem* tick_item = _tick_items.takeLast();
		removeFromGroup( tick_item );
		delete tick_item;
	}
	for ( int it = 0; it < minor_ticks_pos.size(); ++it )
	{
		QPointF pos = minor_ticks_pos.at(it);

		Line minor_tick = Line( pos, tick_shift*minor_tick_size, minor_tick_size, tick_orientation );

		QGraphicsPathItem* tick_item;
		if ( _minor_tick_items.size() < it+1 )
		{
			tick_item = new QGraphicsPathItem;
			tick_item->setZValue(1);
			_minor_tick_items.append(tick_item);
			addToGroup(tick_item);
		}
		else
			tick_item = _minor_tick_items.at(it);

		tick_item->setPen( line_style );
		QPainterPath path;
		path.moveTo(minor_tick.p1());
		path.lineTo(minor_tick.p2());
		tick_item->setPath(path);

		_minor_ticks.append( minor_tick );
		_boundingRect.expandTo( minor_tick );
	}
	while ( _minor_ticks.size() < _minor_tick_items.size() )
	{
		QGraphicsPathItem* tick_item = _minor_tick_items.takeLast();
		removeFromGroup( tick_item );
		delete tick_item;
	}
	_tightBoundingRect = _boundingRect;
	// -----------------------
	//
	// Labels Settings
	Key labels_pos = Key::Outside;
	_settings.getKey( labels_pos, Key(Key::Labels, Key::Position) );
	Point labels_shift = {0,0};
	//	get( labels_shift, Key::Labels, Key::Shift );
	Key labels_anchor = Key::Top;
	_settings.getKey( labels_anchor, Key(Key::Labels, Key::Anchor) );
	double labels_rotation = 0;
	_settings.getDouble( labels_rotation, Key(Key::Labels, Key::Rotation) );
	//	TextStyle labels_style = getTextStyle( Key::Labels );
	//
	if ( (labels_pos == Key::Outside && axis_pos == Key::Bottom) ||
		 (labels_pos == Key::Inside && axis_pos == Key::Top) )
		labels_pos = Key::Bottom;
	else if ( (labels_pos == Key::Outside && axis_pos == Key::Top) ||
			  (labels_pos == Key::Inside && axis_pos == Key::Bottom) )
		labels_pos = Key::Top;
	else if ( (labels_pos == Key::Outside && axis_pos == Key::Left) ||
			  (labels_pos == Key::Inside && axis_pos == Key::Right) )
		labels_pos = Key::Left;
	else if ( (labels_pos == Key::Outside && axis_pos == Key::Right) ||
			  (labels_pos == Key::Inside && axis_pos == Key::Left) )
		labels_pos = Key::Right;
	//
	for ( int it = 0; it < labels.size(); ++it )
	{
		Rect tick_rect = Line(_ticks.at(it));
		Point label_pos = tick_rect.point(labels_pos)+labels_shift;
		//		StyledText label( labels.at(it), labels_style );
		QFont font;
		font.setPointSize( 24 );
		font.setBold( true );
		QGraphicsTextItem* item;
		if ( _label_items.size() < it+1 )
		{
			item = new QGraphicsTextItem;
			item->setZValue(1);
			addToGroup( item );
			_label_items.append( item );
		}
		else
			item = _label_items.at(it);

		item->setFont( font );
		item->setHtml( labels.at(it) );
		Rect label_rect =  item->boundingRect();
		Point pos = label_pos - label_rect.point(labels_anchor);
		item->setPos( pos );
		item->setTransformOriginPoint(label_rect.point(labels_anchor));
		item->setRotation( -labels_rotation );
		item->setDefaultTextColor(QColor(51,51,51));
		Rect rect = Rect(item->mapRectToScene(item->boundingRect()));
		_boundingRect.expandTo( rect );
		if ( _type == Key::X )
		{
			_tightBoundingRect.expandTo( rect.line(Key::Center, true) );
		}
		else if ( _type == Key::Y )
		{
			_tightBoundingRect.expandTo( rect.line(Key::Center, false) );
		}
	}
	while ( labels.size() < _label_items.size() )
	{
		QGraphicsTextItem* item = _label_items.takeLast();
		removeFromGroup( item );
		delete item;
	}
	//	LineStyle line_style;
	//	line_style.setColor( Color("black") );
	//	ShapeStyle shape_style;
	//	shape_style.setLine( line_style );
	//	drawing->drawRect( _boundingRect, shape_style );
	// -----------------------
	//
	// Title Settings
	Key title_pos = Key::Outside;
	_settings.getKey( title_pos, Key(Key::Title, Key::Position) );
	QString title = "Title";
	_settings.getString( title, Key(Key::Title, Key::Text) );
	QPointF title_shift = {0,0};
	//	get( title_shift, Key::Title, Key::Shift );
	Key title_anchor = Key::Top;
	_settings.getKey( title_anchor, Key(Key::Title, Key::Anchor) );
	double title_rotation = 0;
	_settings.getDouble( title_rotation, Key(Key::Title, Key::Rotation) );
	//
	if ( (title_pos == Key::Outside && axis_pos == Key::Bottom) ||
		 (title_pos == Key::Inside && axis_pos == Key::Top) )
		title_pos = Key::Bottom;
	else if ( (title_pos == Key::Outside && axis_pos == Key::Top) ||
			  (title_pos == Key::Inside && axis_pos == Key::Bottom) )
		title_pos = Key::Top;
	else if ( (title_pos == Key::Outside && axis_pos == Key::Left) ||
			  (title_pos == Key::Inside && axis_pos == Key::Right) )
		title_pos = Key::Left;
	else if ( (title_pos == Key::Outside && axis_pos == Key::Right) ||
			  (title_pos == Key::Inside && axis_pos == Key::Left) )
		title_pos = Key::Right;
	//
	Point title_position = _tightBoundingRect.point(title_pos);//_boundingRect.point(title_pos);// - title_shift;
	if ( _title_item == nullptr )
	{
		_title_item = new QGraphicsTextItem;
		_title_item->setZValue(1);
		addToGroup(_title_item);
	}
	QFont font;
	font.setPointSize( 24 );
	font.setBold( true );
	_title_item->setFont( font );
	_title_item->setHtml( title );
	Rect title_rect =  _title_item->boundingRect();
	Point pos = title_position - title_rect.point(title_anchor);
	_title_item->setPos( pos );
	_title_item->setTransformOriginPoint(title_rect.point(title_anchor));
	_title_item->setRotation( -title_rotation );
	_title_item->setDefaultTextColor(QColor(51,51,51));
	addToGroup( _title_item );
	Rect rect = Rect(_title_item->mapRectToScene(_title_item->boundingRect()));
	_boundingRect.expandTo( rect );
//	Rect rect = Rect(_title_item->mapRectToScene(_title_item->boundingRect()));
//	QGraphicsRectItem* ite = new QGraphicsRectItem;
//	ite->setRect(rect);
//	ite->setPen(QColor(0,89,78));
//	addToGroup(ite);
	// -----------------------
	//
	// Grids Settings
	// Grids Position
	Key gd_pos = Key::BothSides;
	double gd_beg = _frameRect.point(Key::Top).y();
	double gd_end = _frameRect.point(Key::Bottom).y();
	if ( _type == Key::Y )
	{
		gd_beg = _frameRect.point(Key::Left).x();
		gd_end = _frameRect.point(Key::Right).x();
	}
	QPen grid_style = QPen(Qt::NoPen);
	_settings.getPen(grid_style,Key(Key::Grids,Key::Style));
	QPen minor_grid_style = QPen(Qt::NoPen);
	_settings.getPen(minor_grid_style,Key(Key::Minor,Key::Grids,Key::Style));
	minor_grid_style.setCapStyle( grid_style.capStyle() );
	_grids.clear(), _minor_grids.clear();
	for ( int it = 0; it < ticks_pos.size(); ++it )
	{
		QPointF pos = ticks_pos.at(it);
		if ( _type == Key::X )
			pos.setY(0);
		else if ( _type == Key::Y )
			pos.setX(0);
		Line grid = Line( pos, gd_beg, gd_end-gd_beg, tick_orientation );

		QGraphicsPathItem* grid_item;
		if ( _grid_items.size() < it+1 )
		{
			grid_item = new QGraphicsPathItem;
			_grid_items.append(grid_item);
			addToGroup(grid_item);
		}
		else
			grid_item = _grid_items.at(it);

		grid_item->setPen( grid_style );
		QPainterPath path;
		path.moveTo(grid.p1());
		path.lineTo(grid.p2());
		grid_item->setPath(path);

		_grids.append( grid );
		_boundingRect.expandTo( grid );
	}
	while ( _grids.size() < _grid_items.size() )
	{
		QGraphicsPathItem* grid_item = _grid_items.takeLast();
		removeFromGroup( grid_item );
		delete grid_item;
	}
	for ( int it = 0; it < minor_ticks_pos.size(); ++it )
	{
		QPointF pos = minor_ticks_pos.at(it);
		if ( _type == Key::X )
			pos.setY(0);
		else if ( _type == Key::Y )
			pos.setX(0);

		Line minor_grid = Line(  pos, gd_beg, gd_end-gd_beg, tick_orientation );

		QGraphicsPathItem* grid_item;
		if ( _minor_grid_items.size() < it+1 )
		{
			grid_item = new QGraphicsPathItem;
			_minor_grid_items.append(grid_item);
			addToGroup(grid_item);
		}
		else
			grid_item = _minor_grid_items.at(it);

		grid_item->setPen( minor_grid_style );
		QPainterPath path;
		path.moveTo(minor_grid.p1());
		path.lineTo(minor_grid.p2());
		grid_item->setPath(path);

		_minor_grids.append( minor_grid );
		_boundingRect.expandTo( minor_grid );
	}
	while ( _minor_grids.size() < _minor_grid_items.size() )
	{
		QGraphicsPathItem* tick_grid = _minor_grid_items.takeLast();
		removeFromGroup( tick_grid );
		delete tick_grid;
	}




	//	for ( Line grid : _minor_grids )
	//		painter->drawLine( grid, _minor_grids_style );
	//	for ( Line grid : _grids )
	//		painter->drawLine( grid, _grids_style );
//	QPen pen;
//	pen.setColor( QColor(0,0,0) );
//	pen.setWidth( 4 );
//	painter->setPen( pen );
//	painter->drawLine( _line );

//	for ( QLineF tick : _minor_ticks )
//		painter->drawLine( tick );
//	for ( QLineF tick : _ticks )
//		painter->drawLine( tick );

	//	painter->drawLine( boundingRect().topLeft(), boundingRect().bottomRight() );

	//	for ( StyledText label : _labels )
	//		painter->drawText( label.text(), label.pos(), label.style() );

	//	painter->drawText( _title.text(), _title.pos(), _title.style() );


//	// TEST
//	LineStyle styl;
//	styl.setColor( Color("red6") );
//	styl.setWidth( 0.5 );
//	ShapeStyle style;
//	style.setLine( styl );
//	drawing->drawRect( _boundingRect, style );
//	// TEST END
	emit changed();
}
/**********************************************/
/**********************************************/
