#include "GraphItem.h"
/**********************************************/
#include "GraphView.h"
/**********************************************/
#include "FrameItem.h"
#include "AxisItem.h"
#include "PlotItem.h"
#include "LegendItem.h"
#include "TitleItem.h"
#include "ShapeItem.h"
/**********************************************/
/**********************************************/
/**********************************************/
GraphItem::~GraphItem()
{
	while ( !_frames.isEmpty() )
		delete _frames.takeFirst();
	while ( !_axes.isEmpty() )
		delete _axes.takeFirst();
	while ( !_plots.isEmpty() )
		delete _plots.takeFirst();
	while ( !_legends.isEmpty() )
		delete _legends.takeFirst();
	while ( !_titles.isEmpty() )
		delete _titles.takeFirst();
	while ( !_shapes.isEmpty() )
		delete _shapes.takeFirst();
}
GraphItem::GraphItem( QGraphicsItem* parent )
	: GraphicsItem(parent)
{
	init();
}
/**********************************************/
/**********************************************/
GraphItem::GraphItem( const QString& name, QGraphicsItem* parent )
	: GraphicsItem(name, parent)
{
	init();
}
void GraphItem::init()
{
	_item_type = Key::Graph;
	setIcon( 0, QIcon("icons/Dark Theme/grapha.svg") );
	_frames_treeitem = new QTreeWidgetItem({"Frames"});
	_frames_treeitem->setIcon( 0, QIcon("icons/Dark Theme/frame.svg") );
	_axes_treeitem = new QTreeWidgetItem({"Axes"});
	_axes_treeitem->setIcon( 0, QIcon("icons/Dark Theme/axis.svg") );
	_plots_treeitem = new QTreeWidgetItem({"Plots"});
	_plots_treeitem->setIcon( 0, QIcon("icons/Dark Theme/plot.svg") );
	_legends_treeitem = new QTreeWidgetItem({"Legends"});
	_legends_treeitem->setIcon( 0, QIcon("icons/Dark Theme/legend.svg") );
	_titles_treeitem = new QTreeWidgetItem({"Titles"});
	_titles_treeitem->setIcon( 0, QIcon("icons/Dark Theme/title.svg") );
	_shapes_treeitem = new QTreeWidgetItem({"Shapes"});
	_shapes_treeitem->setIcon( 0, QIcon("icons/Dark Theme/shapes.svg") );
	this->addChild( _frames_treeitem );
	this->addChild( _axes_treeitem );
	this->addChild( _plots_treeitem );
	this->addChild( _legends_treeitem );
	this->addChild( _titles_treeitem );
	this->addChild( _shapes_treeitem );

	// Defaut Settings
	GraphicsItem::addSetting( Key(Key::Background, Key::Color), QColor(0,0,0,0));
	GraphicsItem::addSetting( Key(Key::Borders, Key::Color), QColor(0,0,0,0));

	connect( &_settings, &Settings::changed,
			 this, &GraphItem::updateGraphItem );
}
/**********************************************/
/**********************************************/
void GraphItem::addSetting( const Key& key, const QVariant& value )
{
	_settings.addSetting( key, value );
}
/**********************************************/
/**********************************************/
/* Graph Element Management*/
void GraphItem::graphitem()
{

}
void GraphItem::clearAllGraphItem()
{

}
void GraphItem::newFrame( const QString& name )
{
	QString tmp = name;
	while ( frame(tmp) )
		tmp = "New " + tmp;
	FrameItem* frame = new FrameItem(tmp);
	frame->setZValue( 1 );
	_frames.append(frame);
	this->addToGroup(frame);
	_frames_treeitem->addChild(frame);
	frame->updateGraphItem();
	connect( frame, SIGNAL(changed()),
			 this, SIGNAL(changed()) );
	emit framesChanged();
}
FrameItem* GraphItem::frame( const QString& name )
{
	for ( FrameItem* frame : _frames )
		if ( frame->name() == name )
			return frame;
	return nullptr;
}
QList<FrameItem*> GraphItem::frames() const
{
	return _frames;
}
void GraphItem::deleteFrame()
{

	emit framesChanged();
}
void GraphItem::clearFrames()
{

	emit framesChanged();
}
void GraphItem::newAxis( const QString& name )
{
	QString tmp = name;
	while ( frame(tmp) )
		tmp = "New " + tmp;
	AxisItem* axis = new AxisItem(tmp);
	axis->setZValue( 1 );
	_axes.append(axis);
	this->addToGroup(axis);
	_axes_treeitem->addChild(axis);
	axis->updateGraphItem();
	connect( axis, SIGNAL(changed()),
			 this, SIGNAL(changed()) );
}
AxisItem* GraphItem::axis( const QString& name )
{
	for ( AxisItem* axis : _axes )
		if ( axis->name() == name )
			return axis;
	return nullptr;
}
void GraphItem::deleteAxis()
{

}
void GraphItem::clearAxes()
{

}
void GraphItem::newPlot( const QString& name )
{
	QString tmp = name;
	while ( frame(tmp) )
		tmp = "New " + tmp;
	PlotItem* plot = new PlotItem(tmp);
	plot->setZValue( 1 );
	_plots.append(plot);
	this->addToGroup(plot);
	_plots_treeitem->addChild(plot);
	plot->updateGraphItem();
	connect( plot, SIGNAL(changed()),
			 this, SIGNAL(changed()) );
}
PlotItem* GraphItem::plot( const QString& name )
{
	for ( PlotItem* plot : _plots )
		if ( plot->name() == name )
			return plot;
	return nullptr;
}
void GraphItem::deletePlot()
{

}
void GraphItem::clearPlots()
{

}
void GraphItem::newLegend( const QString& name )
{
	QString tmp = name;
	while ( frame(tmp) )
		tmp = "New " + tmp;
	LegendItem* legend = new LegendItem(tmp);
	legend->setZValue( 1 );
	_legends.append(legend);
	this->addToGroup(legend);
	_legends_treeitem->addChild(legend);
	legend->updateGraphItem();
	connect( legend, SIGNAL(changed()),
			 this, SIGNAL(changed()) );
}
LegendItem* GraphItem::legend( const QString& name )
{
	for ( LegendItem* legend : _legends )
		if ( legend->name() == name )
			return legend;
	return nullptr;
}
void GraphItem::deleteLegend()
{

}
void GraphItem::clearLegends()
{

}
void GraphItem::newTitle( const QString& name )
{
	QString tmp = name;
	while ( frame(tmp) )
		tmp = "New " + tmp;
	TitleItem* title = new TitleItem(tmp);
	title->setZValue( 1 );
	_titles.append(title);
	this->addToGroup(title);
	_titles_treeitem->addChild(title);
	title->updateGraphItem();
	connect( title, SIGNAL(changed()),
			 this, SIGNAL(changed()) );
}
TitleItem* GraphItem::title( const QString& name )
{
	for ( TitleItem* title : _titles )
		if ( title->name() == name )
			return title;
	return nullptr;
}
void GraphItem::deleteTitle()
{

}
void GraphItem::clearTitles()
{

}
void GraphItem::newShape( const QString& name )
{
	QString tmp = name;
	while ( frame(tmp) )
		tmp = "New " + tmp;
	ShapeItem* shape = new ShapeItem(tmp);
	shape->setZValue( 1 );
	_shapes.append(shape);
	this->addToGroup(shape);
	_shapes_treeitem->addChild(shape);
	shape->updateGraphItem();
	connect( shape, SIGNAL(changed()),
			 this, SIGNAL(changed()) );
}
ShapeItem* GraphItem::shape( const QString& name )
{
	for ( ShapeItem* shape : _shapes )
		if ( shape->name() == name )
			return shape;
	return nullptr;
}
void GraphItem::deleteShape()
{

}
void GraphItem::clearShapes()
{

}
/**********************************************/
/**********************************************/
void GraphItem::updateGraphItem()
{
	if ( item_bounding_rect == nullptr )
	{
		item_bounding_rect = new QGraphicsRectItem;
		addToGroup( item_bounding_rect );
	}
//	QPen pen;
//	pen.setWidth(2);
//	pen.setColor( QColor(0,0,0) );
//	pen.setJoinStyle( Qt::MiterJoin );
//	item_bounding_rect->setPen( pen );
//	item_bounding_rect->setBrush( QColor(0,0,0,0) );
//	item_bounding_rect->setRect( this->boundingRect() );
//	item_bounding_rect->setZValue( 0 );


	bool hide = false; _settings.getBool( hide, Key::Hide );
	if ( !hide )
	{
		QPen  pen = QPen(Qt::NoPen);
		_settings.getPen( pen, Key::Borders );
		QBrush brush = _settings.getBrush(Key::Background);
		if ( graph_rect == nullptr )
		{
			graph_rect = new QGraphicsRectItem;
			addToGroup( graph_rect );
		}
		graph_rect->setPen( pen );
		graph_rect->setBrush( brush );
		graph_rect->setRect( this->boundingRect() );
		graph_rect->setZValue( 0 );
	}
	else
	{
		if ( graph_rect != nullptr )
		{
			removeFromGroup( graph_rect );
			delete graph_rect;
			graph_rect = nullptr;
		}
	}
	emit changed();
}
/**********************************************/
/**********************************************/
