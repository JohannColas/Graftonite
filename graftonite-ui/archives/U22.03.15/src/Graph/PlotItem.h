#ifndef PLOTITEM_H
#define PLOTITEM_H
/**********************************************/
#include "GraphicsItem.h"
/**********************************************/
/**********************************************/
/**********************************************/
class PlotItem
		: public GraphicsItem
{
private:
	QGraphicsPathItem* line_item = nullptr;
public:
	~PlotItem();
	PlotItem( QGraphicsItem* parent = nullptr );
	PlotItem( const QString& name, QGraphicsItem* parent = nullptr );
	void init();
	/**********************************************/
	void addSetting( const Key& key, const QVariant& value ) override;
	/******************************************/
public slots:
	void updateGraphItem() override;
	/******************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PLOTITEM_H
