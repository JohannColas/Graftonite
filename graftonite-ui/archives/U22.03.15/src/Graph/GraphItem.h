#ifndef GRAPHITEM_H
#define GRAPHITEM_H
/**********************************************/
#include "GraphicsItem.h"
/**********************************************/
class FrameItem;
class AxisItem;
class PlotItem;
class LegendItem;
class TitleItem;
class ShapeItem;
/**********************************************/
/**********************************************/
/**********************************************/
class GraphItem
		: public GraphicsItem
{
	Q_OBJECT
protected:
	QGraphicsRectItem* graph_rect = nullptr;
	Rect _rect = {0,0,1600,1200};
	/******************************************/
	QTreeWidgetItem* _frames_treeitem = nullptr;
	QTreeWidgetItem* _axes_treeitem = nullptr;
	QTreeWidgetItem* _plots_treeitem = nullptr;
	QTreeWidgetItem* _legends_treeitem = nullptr;
	QTreeWidgetItem* _titles_treeitem = nullptr;
	QTreeWidgetItem* _shapes_treeitem = nullptr;
	QList<FrameItem*> _frames;
	QList<AxisItem*> _axes;
	QList<PlotItem*> _plots;
	QList<LegendItem*> _legends;
	QList<TitleItem*> _titles;
	QList<ShapeItem*> _shapes;
	/******************************************/
public:
	~GraphItem();
	GraphItem( QGraphicsItem* parent = nullptr );
	GraphItem( const QString& name, QGraphicsItem* parent = nullptr );
	/******************************************/
	void init();
	/******************************************/
	//
	void addSetting( const Key& key, const QVariant& value ) override;
	/******************************************/
	// Graph Element Management
	void graphitem();
	void clearAllGraphItem();
	void newFrame( const QString& name );
	FrameItem* frame( const QString& name );
	QList<FrameItem*> frames() const;
	void deleteFrame( );
	void clearFrames();
	void newAxis( const QString& name );
	AxisItem* axis( const QString& name );
	void deleteAxis( );
	void clearAxes();
	void newPlot( const QString& name );
	PlotItem* plot( const QString& name );
	void deletePlot( );
	void clearPlots();
	void newLegend( const QString& name );
	LegendItem* legend( const QString& name );
	void deleteLegend( );
	void clearLegends();
	void newTitle( const QString& name );
	TitleItem* title( const QString& name );
	void deleteTitle( );
	void clearTitles();
	void newShape( const QString& name );
	ShapeItem* shape( const QString& name );
	void deleteShape( );
	void clearShapes();
	/******************************************/
public slots:
	void updateGraphItem() override;
	/******************************************/
signals:
	void framesChanged();
	/******************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // GRAPHITEM_H
