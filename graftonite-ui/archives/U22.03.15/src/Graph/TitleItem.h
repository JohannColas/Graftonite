#ifndef TITLEITEM_H
#define TITLEITEM_H
/**********************************************/
#include "GraphicsItem.h"
/**********************************************/
/**********************************************/
/**********************************************/
class TitleItem
		: public GraphicsItem
{
public:
	TitleItem( QGraphicsItem* parent = nullptr );
	TitleItem( const QString& name, QGraphicsItem* parent = nullptr );
	void init();
	/******************************************/
	void addSetting( const Key& key, const QVariant& value ) override;
	/******************************************/
public slots:
	void updateGraphItem() override;
	/******************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TITLEITEM_H
