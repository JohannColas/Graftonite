#include "ProjectModel.h"
/**********************************************/
#include "ProjectItem.h"
/**********************************************/
#include <QFile>
#include <QFileInfo>
#include <QIcon>
#include <QPixmap>
#include <QStringList>
/**********************************************/
/**********************************************/
ProjectModel::~ProjectModel()
{
	delete _rootItem;
}
/**********************************************/
/**********************************************/
ProjectModel::ProjectModel( const QString& data, QObject* parent )
	: QAbstractItemModel(parent)
{
	_rootItem = new ProjectItem;
	_rootItem->addSetting( Key::Name, "Name" );
	_rootItem->addSetting( Key::Type, Key::Root );
	_rootItem->addSetting( Key::Path, "Path" );
	this->setupModelData(data.split('\n'), _rootItem);
}
/**********************************************/
/**********************************************/
ProjectModel::ProjectModel( const QList<Project*>& data, QObject* parent )
{
	_rootItem = new ProjectItem;
	_rootItem->addSetting( Key::Name, "Name" );
	_rootItem->addSetting( Key::Type, Key::Root );
	_rootItem->addSetting( Key::Path, "Path" );
	for ( Project* project : data )
		this->setupModelData( project, _rootItem );
}
/**********************************************/
/**********************************************/
bool ProjectModel::setData( const QModelIndex& index, const QVariant& value, const int role )
{
	if ( role != Qt::EditRole )
		return false;

	ProjectItem* item = this->item(index);
	bool result = item->setData(index.column(), value);

	if ( result )
		emit dataChanged( index, index, {Qt::DisplayRole, Qt::EditRole} );

	return result;
}
/**********************************************/
/**********************************************/
QVariant ProjectModel::data( const QModelIndex& index, int role ) const
{
	if ( !index.isValid() )
		return QVariant();

	ProjectItem* item = this->item(index);
	if ( role == Qt::DisplayRole )
	{
		return item->data(index.column());
	}
	else if ( role == Qt::DecorationRole )
	{
		if ( index.column() == 0 )
		{
			Key type = item->type();
			if ( type == Key::Project )
				return QIcon("icons/Dark Theme/graftonite.svg");
			else if ( type == Key::Folder )
				return QIcon("icons/Dark Theme/folder.svg");
			else if ( type == Key::Data )
				return QIcon("icons/Dark Theme/data.svg");
			else if ( type == Key::Graph )
				return QIcon("icons/Dark Theme/graph.svg");
			else if ( type == Key::Image )
				return QIcon("icons/Dark Theme/image.svg");
		}
	}
	else if ( role == Qt::EditRole )
	{
		return QVariant();
	}
	return QVariant();
}
/**********************************************/
/**********************************************/
Qt::ItemFlags ProjectModel::flags( const QModelIndex& index ) const
{
	if ( !index.isValid() )
		return Qt::NoItemFlags;
	Qt::ItemFlags flags = QAbstractItemModel::flags(index);

	if ( index.column() == 0 )
		flags = Qt::ItemIsEditable |  flags;

	return flags;
}
/**********************************************/
/**********************************************/
QVariant ProjectModel::headerData( int section, Qt::Orientation orientation, int role ) const
{
	if ( orientation == Qt::Horizontal && role == Qt::DisplayRole )
		return _rootItem->data(section);

	return QVariant();
}
/**********************************************/
/**********************************************/
bool ProjectModel::setHeaderData( int section, Qt::Orientation orientation, const QVariant& value, int role )
{
	if ( role != Qt::EditRole || orientation != Qt::Horizontal )
		return false;

	const bool result = _rootItem->setData(section, value);

	if ( result )
		emit headerDataChanged(orientation, section, section);

	return result;
}
/**********************************************/
/**********************************************/
QModelIndex ProjectModel::index( int row, int column, const QModelIndex& index ) const
{
	if ( index.isValid() && index.column() != 0 )
		return QModelIndex();

	ProjectItem* parentItem = this->item(index);
	if ( !parentItem )
		return QModelIndex();

	ProjectItem* childItem = parentItem->child(row);
	if ( childItem )
		return createIndex(row, column, childItem);
	return QModelIndex();
}
/**********************************************/
/**********************************************/
QModelIndex ProjectModel::parent( const QModelIndex& index ) const
{
	if ( !index.isValid() )
		return QModelIndex();

	ProjectItem* childItem = this->item(index);
	ProjectItem* parentItem = childItem ? childItem->parent() : nullptr;

	if ( parentItem == _rootItem || !parentItem )
		return QModelIndex();

	return createIndex(parentItem->row(), 0, parentItem);
}
/**********************************************/
/**********************************************/
int ProjectModel::rowCount( const QModelIndex& index ) const
{
	if ( index.isValid() && index.column() > 0 )
		return 0;

	const ProjectItem* parentItem = this->item(index);

	return parentItem ? parentItem->childCount() : 0;
}
/**********************************************/
/**********************************************/
int ProjectModel::columnCount( const QModelIndex& /*index*/ ) const
{
//	if (index.isValid())
//		return static_cast<ProjectItem*>(index.internalPointer())->columnCount();

	return _rootItem->columnCount();
}
/**********************************************/
/**********************************************/
bool ProjectModel::insertRows( int position, int rows, const QModelIndex& index )
{
	ProjectItem* parentItem = this->item(index);
	if ( !parentItem )
		return false;

	qDebug() << index << position << rows;
	beginInsertRows( index, position, position + rows - 1 );
	const bool success = parentItem->insertChildren(position,
													rows,
													_rootItem->columnCount());
	endInsertRows();

	return success;
}
/**********************************************/
/**********************************************/
bool ProjectModel::removeRows( int position, int rows, const QModelIndex& index )
{
	ProjectItem* parentItem = this->item(index);
	if ( !parentItem )
		return false;

	beginRemoveRows( index, position, position + rows - 1 );
	const bool success = parentItem->removeChildren(position, rows);
	endRemoveRows();

	return success;
}
/**********************************************/
/**********************************************/
void ProjectModel::addProject( const QString& name )
{
	ProjectItem* project = new ProjectItem;
	project->addSetting( Key::Name, name );
	project->addSetting( Key::Type, Key::Project );
	project->addSetting( Key::Path, "" );
	_rootItem->appendChild(project);

	insertRows( 0, 0, QModelIndex() );
}

void ProjectModel::addFolder( const QString& name, const QModelIndex& index )
{
	ProjectItem* parentItem = this->item(index);
	if ( !parentItem )
		return;
	ProjectItem* project = new ProjectItem;
	project->addSetting( Key::Name, name );
	project->addSetting( Key::Type, Key::Folder );
	project->addSetting( Key::Path, name );
	parentItem->appendChild(project);

	insertRows( 0, 0, index );
}
/**********************************************/
/**********************************************/
void ProjectModel::setupModelData( const QStringList& lines, ProjectItem* root )
{
	for ( const QString& project_path : lines )
	{
		QFile file( project_path );
		if ( file.open(QIODevice::ReadOnly) )
		{
			ProjectItem* project = new ProjectItem;
			project->addSetting( Key::Type, Key::Project );
			QList<ProjectItem*> parent = {project};
			QTextStream stream( &file );
			while ( !stream.atEnd() )
			{
				QString line = stream.readLine();
				if ( line.isEmpty() || line.startsWith("//") )
					continue;
				else if ( line.startsWith("#") )
				{
					ProjectItem* item = new ProjectItem;
					int indent = Settings::DecomposeLine( line, item->settings() );
					if ( indent < parent.size() )
					{
						item->setParent( parent.at(indent-1) );
						parent.at(indent-1)->appendChild(item);
						if ( indent < parent.size() )
						{
							parent.replace( indent, item );
							for ( int jt = indent+1; jt < parent.size(); ++jt )
								parent.remove( jt );
						}
						else
							parent.append( item );
					}
					else
					{
						item->setParent( parent.last() );
						parent.last()->appendChild(item);
						parent.append( item );
					}
				}
				else if ( line.contains("=") )
					Settings::DecomposeLine( line, project->settings() );
			}
			file.close();
			if ( !project->settings().hasKey(Key::Path) )
				project->addSetting( Key::Path, QFileInfo(project_path).absolutePath() );
			root->appendChild(project);
		}
	}
}
/**********************************************/
/**********************************************/
void ProjectModel::setupModelData( Project* project, ProjectItem* parent )
{

}
/**********************************************/
/**********************************************/
ProjectItem* ProjectModel::item( const QModelIndex& index ) const
{
	if ( index.isValid() )
	{
		ProjectItem* item = static_cast<ProjectItem*>(index.internalPointer());
		if ( item )
			return item;
	}
	return _rootItem;
}
/**********************************************/
/**********************************************/
