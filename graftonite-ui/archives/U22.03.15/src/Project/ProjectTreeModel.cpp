#include "ProjectTreeModel.h"
/**********************************************/
#include "ProjectTreeItem.h"
/**********************************************/
#include <QStringList>
/**********************************************/
/**********************************************/
ProjectTreeModel::~ProjectTreeModel()
{
	delete _rootItem;
}
/**********************************************/
/**********************************************/
ProjectTreeModel::ProjectTreeModel( const QString& data, QObject* parent )
	: QAbstractItemModel(parent)
{
	_rootItem = new ProjectTreeItem("Name", Key::Root, "Path");
	this->setupModelData(data.split('\n'), _rootItem);
}
/**********************************************/
/**********************************************/
ProjectTreeModel::ProjectTreeModel( const QList<Project*>& data, QObject* parent )
{
	_rootItem = new ProjectTreeItem("Name", Key::Root, "Path");
	for ( Project* project : data )
		this->setupModelData( project, _rootItem );
}
/**********************************************/
/**********************************************/
bool ProjectTreeModel::setData( const QModelIndex& index, const QVariant& value, const int role )
{
	if ( role != Qt::EditRole )
		return false;

	ProjectTreeItem* item = this->item(index);
	bool result = item->setData(index.column(), value);

	if ( result )
		emit dataChanged( index, index, {Qt::DisplayRole, Qt::EditRole} );

	return result;
}
/**********************************************/
/**********************************************/
QVariant ProjectTreeModel::data( const QModelIndex& index, int role ) const
{
	if ( !index.isValid() )
		return QVariant();

	if ( role != Qt::DisplayRole && role != Qt::EditRole )
		return QVariant();

	ProjectTreeItem* item = this->item(index);

	return item->data(index.column());
}
/**********************************************/
/**********************************************/
Qt::ItemFlags ProjectTreeModel::flags( const QModelIndex& index ) const
{
	if ( !index.isValid() )
		return Qt::NoItemFlags;
	Qt::ItemFlags flags = QAbstractItemModel::flags(index);

	if ( index.column() == 0 )
		flags = Qt::ItemIsEditable |  flags;

	return flags;
}
/**********************************************/
/**********************************************/
QVariant ProjectTreeModel::headerData( int section, Qt::Orientation orientation, int role ) const
{
	if ( orientation == Qt::Horizontal && role == Qt::DisplayRole )
		return _rootItem->data(section);

	return QVariant();
}
/**********************************************/
/**********************************************/
bool ProjectTreeModel::setHeaderData( int section, Qt::Orientation orientation, const QVariant& value, int role )
{
	if ( role != Qt::EditRole || orientation != Qt::Horizontal )
		return false;

	const bool result = _rootItem->setData(section, value);

	if ( result )
		emit headerDataChanged(orientation, section, section);

	return result;
}
/**********************************************/
/**********************************************/
QModelIndex ProjectTreeModel::index( int row, int column, const QModelIndex& index ) const
{
	if ( index.isValid() && index.column() != 0 )
		return QModelIndex();

	ProjectTreeItem* parentItem = this->item(index);
	if ( !parentItem )
		return QModelIndex();

	ProjectTreeItem* childItem = parentItem->child(row);
	if ( childItem )
		return createIndex(row, column, childItem);
	return QModelIndex();
}
/**********************************************/
/**********************************************/
QModelIndex ProjectTreeModel::parent( const QModelIndex& index ) const
{
	if ( !index.isValid() )
		return QModelIndex();

	ProjectTreeItem* childItem = this->item(index);
	ProjectTreeItem* parentItem = childItem ? childItem->parent() : nullptr;

	if ( parentItem == _rootItem || !parentItem )
		return QModelIndex();

	return createIndex(parentItem->row(), 0, parentItem);
}
/**********************************************/
/**********************************************/
int ProjectTreeModel::rowCount( const QModelIndex& index ) const
{
	if ( index.isValid() && index.column() > 0 )
		return 0;

	const ProjectTreeItem* parentItem = this->item(index);

	return parentItem ? parentItem->childCount() : 0;
}
/**********************************************/
/**********************************************/
int ProjectTreeModel::columnCount( const QModelIndex& /*index*/ ) const
{
//	if (index.isValid())
//		return static_cast<ProjectTreeItem*>(index.internalPointer())->columnCount();

	return _rootItem->columnCount();
}
/**********************************************/
/**********************************************/
bool ProjectTreeModel::insertRows( int position, int rows, const QModelIndex& index )
{
	ProjectTreeItem* parentItem = this->item(index);
	if ( !parentItem )
		return false;

	beginInsertRows( index, position, position + rows - 1 );
	const bool success = parentItem->insertChildren(position,
													rows,
													_rootItem->columnCount());
	endInsertRows();

	return success;
}
/**********************************************/
/**********************************************/
bool ProjectTreeModel::removeRows( int position, int rows, const QModelIndex& index )
{
	ProjectTreeItem* parentItem = this->item(index);
	if ( !parentItem )
		return false;

	beginRemoveRows( index, position, position + rows - 1 );
	const bool success = parentItem->removeChildren(position, rows);
	endRemoveRows();

	return success;
}
/**********************************************/
/**********************************************/
void ProjectTreeModel::setupModelData( const QStringList& lines, ProjectTreeItem* parent )
{
	QList<ProjectTreeItem*> parents;
	QList<int> indentations;
	parents << parent;
	indentations << 0;

	int number = 0;

	while ( number < lines.count() )
	{
		int position = 0;
		while ( position < lines[number].length() )
		{
			if ( lines[number].at(position) != ' ' )
				break;
			++position;
		}

		const QString lineData = lines[number].mid(position).trimmed();

		if ( !lineData.isEmpty() )
		{
			// Read the column data from the rest of the line.
			const QStringList columnStrings = lineData.split(QLatin1Char('\t'), Qt::SkipEmptyParts);
			QList<QVariant> columnData;
			columnData.reserve( columnStrings.size() );
			for ( const QString &columnString : columnStrings )
				columnData << columnString;

			if ( position > indentations.last() )
			{
				// The last child of the current parent is now the new parent
				// unless the current parent has no children.

				if ( parents.last()->childCount() > 0 )
				{
					parents << parents.last()->child(parents.last()->childCount()-1);
					indentations << position;
				}
			}
			else
			{
				while ( position < indentations.last() && parents.count() > 0 )
				{
					parents.pop_back();
					indentations.pop_back();
				}
			}

			// Append a new item to the current parent's list of children.
			ProjectTreeItem* parent = parents.last();
			parent->insertChildren( parent->childCount(), 1, _rootItem->columnCount() );
			for ( int column = 0; column < columnData.size(); ++column )
			{
//				parent->child(parent->childCount() - 1)->setData(column, columnData[column]);
				parent->child(parent->childCount() - 1)->setName(columnData[column].toString());
				if ( number == 0 )
					parent->child(parent->childCount() - 1)->setType(Key::Project);
				else if ( number == 1 )
					parent->child(parent->childCount() - 1)->setType(Key::Graph);
				else
					parent->child(parent->childCount() - 1)->setType(Key::Data);
				parent->child(parent->childCount() - 1)->setPath("");
			}
		}
		++number;
	}
}
/**********************************************/
/**********************************************/
void ProjectTreeModel::setupModelData( Project* project, ProjectTreeItem* parent )
{

}
/**********************************************/
/**********************************************/
ProjectTreeItem* ProjectTreeModel::item( const QModelIndex& index ) const
{
	if ( index.isValid() )
	{
		ProjectTreeItem* item = static_cast<ProjectTreeItem*>(index.internalPointer());
		if ( item )
			return item;
	}
	return _rootItem;
}
/**********************************************/
/**********************************************/
