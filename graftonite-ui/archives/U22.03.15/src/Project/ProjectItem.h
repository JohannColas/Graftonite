#ifndef PROJECTITEM2_H
#define PROJECTITEM2_H
/**********************************************/
#include <QTreeWidgetItem>
#include <QItemDelegate>
/**********************************************/
#include "../Commons/Settings.h"
/**********************************************/
/**********************************************/
/**********************************************/
class ProjectItem2
		: public QTreeWidgetItem
{
	Settings _settings;
public:
	ProjectItem2( QTreeWidgetItem* parent = nullptr );
	void setData( int column, int role, const QVariant& value ) override;
	QVariant data( int column, int role ) const override;
	Key projectType() const;
	/******************************************/
	QVariant setting( const Key& key ) const;
	void addSetting( const Key& key, const QVariant& value );
	Settings& settings();
	ProjectItem2* parent() const;
	/******************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PROJECTITEM2_H
