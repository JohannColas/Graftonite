#ifndef PROJECTDELEGATE_H
#define PROJECTDELEGATE_H
/**********************************************/
#include <QItemDelegate>
/**********************************************/
/**********************************************/
/**********************************************/
class ProjectDelegate
		: public QItemDelegate
{
//	Q_OBJECT
public:
	ProjectDelegate( QObject* parent );;
	QWidget* createEditor( QWidget*parent, const QStyleOptionViewItem& option, const QModelIndex& index ) const override;
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PROJECTDELEGATE_H
