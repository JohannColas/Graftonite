#include "ProjectItem.h"
/**********************************************/
#include "../Commons/Files.h"
/**********************************************/
/**********************************************/
/**********************************************/
ProjectItem2::ProjectItem2( QTreeWidgetItem* parent )
	: QTreeWidgetItem(parent)
{
	addSetting( Key::Name, "[Unknown" );
	addSetting( Key::Type, Key::Unknown );
}

void ProjectItem2::setData( int column, int role, const QVariant& value )
{
	if ( role == Qt::EditRole )
	{
		if ( column == 0 )
			addSetting(Key::Name, value);
		else if ( column == 2 )
			addSetting(Key::Path, value);
	}
}
/**********************************************/
/**********************************************/
QVariant ProjectItem2::data( int column, int role ) const
{
	if ( role == Qt::DisplayRole || role == Qt::EditRole )
	{
		if ( column == 0 )
			return setting(Key::Name);
		else if ( column == 1 )
			return projectType().toString();
		else if ( column == 2 )
			return setting(Key::Path);
		else
			return QVariant();
	}
	else if ( role == Qt::DecorationRole )
	{
		if ( column == 0 )
		{
			Key type = projectType();
			if ( type == Key::Project )
				return QIcon("icons/Dark Theme/graftonite.svg");
			else if ( type == Key::Folder )
				return QIcon("icons/Dark Theme/folder.svg");
			else if ( type == Key::Data )
				return QIcon("icons/Dark Theme/data.svg");
			else if ( type == Key::Graph )
				return QIcon("icons/Dark Theme/graph.svg");
			else if ( type == Key::Image )
				return QIcon("icons/Dark Theme/image.svg");
		}
	}
//	else if ( role == Qt::EditRole )
//	{
//		return QVariant();
//	}
	return QTreeWidgetItem::data( column, role );
}
/**********************************************/
/**********************************************/
Key ProjectItem2::projectType() const
{
	return Key::Keys(setting(Key::Type).toInt());
}
/**********************************************/
/**********************************************/
QVariant ProjectItem2::setting( const Key& key ) const
{
	return _settings.value(key);
}
/**********************************************/
/**********************************************/
void ProjectItem2::addSetting( const Key& key, const QVariant& value )
{
	_settings.addSetting( key, value );
	if ( key == Key::Type )
	{
		Key type = projectType();
		if ( type == Key::Project )
			setIcon( 0, QIcon("icons/Dark Theme/graftonite.svg") );
		else if ( type == Key::Folder )
			setIcon( 0, QIcon("icons/Dark Theme/folder.svg") );
		else if ( type == Key::Data )
			setIcon( 0, QIcon("icons/Dark Theme/data.svg") );
		else if ( type == Key::Graph )
			setIcon( 0, QIcon("icons/Dark Theme/graph.svg") );
		else if ( type == Key::Image )
			setIcon( 0, QIcon("icons/Dark Theme/image.svg") );
	}
}
/**********************************************/
/**********************************************/
Settings& ProjectItem2::settings()
{
	return _settings;
}
/**********************************************/
/**********************************************/
ProjectItem2* ProjectItem2::parent() const
{
	return static_cast<ProjectItem2*>(QTreeWidgetItem::parent());
}
/**********************************************/
/**********************************************/
