#include "ProjectTree.h"
/**********************************************/
#include "ProjectDelegate.h"
#include "ProjectItem.h"
/**********************************************/
//#include <QTreeWidget>
#include <QVBoxLayout>
/**********************************************/
#include <QFile>
#include <QFileInfo>
/**********************************************/
#include <QMenu>
#include <QContextMenuEvent>
/**********************************************/
#include "../Commons/Settings.h"
#include "../Commons/Files.h"
/**********************************************/
/**********************************************/
/**********************************************/
ProjectTree::ProjectTree( QWidget* parent )
	: QTreeWidget(parent)
{

}
void ProjectTree::leaveEvent( QEvent* event )
{
	emit leaved();
	QTreeWidget::leaveEvent( event );
}
ProjectWidget::ProjectWidget( QWidget* parent )
{

}
void ProjectWidget::leaveEvent( QEvent* event )
{
	emit leaved();
	QWidget::leaveEvent( event );
}
/**********************************************/
/**********************************************/
/**********************************************/
ProjectTreeView::~ProjectTreeView()
{
	delete lay_cont;
	delete wid_cont;
	delete lay_main;
	delete contextMenu;
	delete ac_newproject;
}
ProjectTreeView::ProjectTreeView( QWidget* parent)
	: QWidget(parent)
{
	this->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Expanding );
	_tree = new ProjectTree;
	_tree->setEditTriggers( QAbstractItemView::DoubleClicked | QAbstractItemView::SelectedClicked );
	_tree->setSelectionMode( QAbstractItemView::ExtendedSelection );
	_tree->setVerticalScrollMode( QAbstractItemView::ScrollPerPixel );
	_tree->setHorizontalScrollMode( QAbstractItemView::ScrollPerPixel );
	connect( _tree, &QTreeWidget::itemPressed,
			 this, &ProjectTreeView::onItemPressed );
	connect( _tree, &QTreeWidget::itemDoubleClicked,
			 this, &ProjectTreeView::onItemDoubleClicked );

	ProjectDelegate* delegate = new ProjectDelegate(this);
	_tree->setItemDelegate(delegate);




	lay_cont = new QVBoxLayout;
	lay_cont->addWidget( _tree );
	wid_cont = new ProjectWidget;
	wid_cont->setLayout( lay_cont );


	lay_main = new QVBoxLayout;
	lay_main->setContentsMargins(0,0,0,0);
	lay_main->addWidget( wid_cont );
	setLayout( lay_main );

	_tree->setHeaderLabels( {"Name", "Type", "Path"});
	_tree->setColumnCount( 3 );
	_tree->hideColumn(3);


	// Menu
	contextMenu = new QMenu;
	QMenu* newMenu = new QMenu("New", this);
	newMenu->setIcon( QIcon("icons/Dark Theme/new.svg") );
	contextMenu->addMenu(newMenu);

	ac_newproject = new QAction("Project", this);
	ac_newproject->setIcon( QIcon("icons/Dark Theme/graftonite.svg") );
	newMenu->addAction( ac_newproject );
	connect( ac_newproject, &QAction::triggered,
			 this, &ProjectTreeView::newProject );
	ac_newfolder = new QAction("Folder", this);
	ac_newfolder->setIcon( QIcon("icons/Dark Theme/folder.svg") );
	newMenu->addAction( ac_newfolder );
	connect( ac_newfolder, &QAction::triggered,
			 this, &ProjectTreeView::newFolder );
	ac_newdata = new QAction("Data", this);
	ac_newdata->setIcon( QIcon("icons/Dark Theme/data.svg") );
	newMenu->addAction( ac_newdata );
	connect( ac_newdata, &QAction::triggered,
			 this, &ProjectTreeView::newData );
	ac_newgraph = new QAction("Graph", this);
	ac_newgraph->setIcon( QIcon("icons/Dark Theme/graph.svg") );
	newMenu->addAction( ac_newgraph );
	connect( ac_newgraph, &QAction::triggered,
			 this, &ProjectTreeView::newGraph );
	ac_newimage = new QAction("Image", this);
	ac_newimage->setIcon( QIcon("icons/Dark Theme/image.svg") );
	newMenu->addAction( ac_newimage );
	connect( ac_newimage, &QAction::triggered,
			 this, &ProjectTreeView::newImage );

	ac_open = new QAction("Open", this);
	ac_open->setIcon( QIcon("icons/Dark Theme/open.svg") );
	contextMenu->addAction( ac_open );
	connect( ac_open, &QAction::triggered,
			 this, &ProjectTreeView::open );

	ac_load = new QAction("Load", this);
	ac_load->setIcon( QIcon("icons/Dark Theme/load.svg") );
	contextMenu->addAction( ac_load );
	connect( ac_load, &QAction::triggered,
			 this, &ProjectTreeView::load );

	contextMenu->addSeparator();

	ac_delete = new QAction("Delete", this);
	ac_delete->setIcon( QIcon("icons/Dark Theme/delete.svg") );
	contextMenu->addAction( ac_delete );
	connect( ac_delete, &QAction::triggered,
			 this, &ProjectTreeView::deleteItem );

	contextMenu->addSeparator();

	ac_save = new QAction("Save", this);
	ac_save->setIcon( QIcon("icons/Dark Theme/save.svg") );
	contextMenu->addAction( ac_save );
	connect( ac_save, &QAction::triggered,
			 this, &ProjectTreeView::save );
	ac_export = new QAction("Export", this);
	ac_export->setIcon( QIcon("icons/Dark Theme/export.svg") );
	contextMenu->addAction( ac_export );
	connect( ac_export, &QAction::triggered,
			 this, &ProjectTreeView::exportItem );
	ac_close = new QAction("Close", this);
	ac_close->setIcon( QIcon("icons/Dark Theme/close.svg") );
	contextMenu->addAction( ac_close );
	connect( ac_close, &QAction::triggered,
			 this, &ProjectTreeView::closeItem );

}
/**********************************************/
/**********************************************/
void ProjectTreeView::newProject()
{
	ProjectItem2* project = new ProjectItem2;
	project->addSetting( Key::Name, "New project" );
	project->addSetting( Key::Type, Key::Project );

	_tree->addTopLevelItem( project );
}
void ProjectTreeView::newFolder()
{
	ProjectItem2* folder = new ProjectItem2;
	folder->addSetting( Key::Name, "New folder" );
	folder->addSetting( Key::Type, Key::Folder );

	QTreeWidgetItem* current_item = _tree->currentItem();
	current_item->addChild( folder );
}
void ProjectTreeView::newData()
{
	ProjectItem2* data = new ProjectItem2;
	data->addSetting( Key::Name, "New data" );
	data->addSetting( Key::Type, Key::Data );

	QTreeWidgetItem* current_item = _tree->currentItem();
	current_item->addChild( data );
}
void ProjectTreeView::newGraph()
{
	ProjectItem2* graph = new ProjectItem2;
	graph->addSetting( Key::Name, "New graph" );
	graph->addSetting( Key::Type, Key::Graph );

	QTreeWidgetItem* current_item = _tree->currentItem();
	current_item->addChild( graph );
}
void ProjectTreeView::newImage()
{
	ProjectItem2* iamge = new ProjectItem2;
	iamge->addSetting( Key::Name, "New image" );
	iamge->addSetting( Key::Type, Key::Image );

	QTreeWidgetItem* current_item = _tree->currentItem();
	current_item->addChild( iamge );
}
void ProjectTreeView::open()
{

}
void ProjectTreeView::load()
{

}
void ProjectTreeView::deleteItem()
{

}
void ProjectTreeView::save()
{

}
void ProjectTreeView::exportItem()
{

}
void ProjectTreeView::closeItem()
{

}
/**********************************************/
/**********************************************/
void ProjectTreeView::openProject( const QString& path )
{

	QFile file( path );
	if ( file.open(QIODevice::ReadOnly) )
	{
		ProjectItem2* project = new ProjectItem2;
		project->addSetting( Key::Type, Key::Project );
		QList<ProjectItem2*> parent = {project};
		QTextStream stream( &file );
		while ( !stream.atEnd() )
		{
			QString line = stream.readLine();
			if ( line.isEmpty() || line.startsWith("//") )
				continue;
			else if ( line.startsWith("#") )
			{
				int indent = line.indexOf('#');
				for ( ; indent < line.size(); ++indent )
					if ( line.at(indent) != '#' )
						break;
				ProjectItem2* item;
				if ( indent < parent.size() )
				{
					item = new ProjectItem2(parent.at(indent-1));
					parent.at(indent-1)->addChild( item );
					if ( indent < parent.size() )
					{
						parent.replace( indent, item );
						for ( int jt = indent+1; jt < parent.size(); ++jt )
							parent.remove( jt );
					}
					else
						parent.append( item );
				}
				else
				{
					item = new ProjectItem2(parent.last());
					parent.last()->addChild(item);
					parent.append( item );
				}
				Settings::DecomposeLine( line, item->settings() );

				item->settings().addSetting( Key::ID, Files::generateUniqueID() );
			}
			else if ( line.contains("=") )
				Settings::DecomposeLine( line, project->settings() );
		}
		file.close();
		if ( !project->settings().hasKey(Key::Path) )
			project->settings().addSetting( Key::Path, QFileInfo(path).absolutePath() );
		project->addSetting( Key::ID, Files::generateUniqueID() );
		_tree->addTopLevelItem(project);
	}
}
/**********************************************/
/**********************************************/
ProjectItem2* ProjectTreeView::projectItem( ProjectItem2* item )
{
	ProjectItem2* project = item;//tree_project2->ProjectItem2( item );
	while ( project != nullptr && project->projectType() != Key::Project )
		project = project->parent();
	return project;
//	if ( item != nullptr && item->projectType() != Key::Project )
//		return ProjectItem2( static_cast<ProjectItem2*>(_tree->itemAbove(item)));
//	else
	//		return item;
}
/**********************************************/
/**********************************************/
void ProjectTreeView::setAbleToHide( bool abletohide )
{
	_abletohide = abletohide;
}
/**********************************************/
/**********************************************/
void ProjectTreeView::onItemPressed( QTreeWidgetItem* item, int column )
{
	emit itemPressed( static_cast<ProjectItem2*>(item) );
}
/**********************************************/
/**********************************************/
void ProjectTreeView::onItemDoubleClicked( QTreeWidgetItem* item, int column )
{
	Qt::ItemFlags flags = item->flags();
	if ( column == 1 )
	{
		item->setFlags(flags & (~Qt::ItemIsEditable));
	}
	else
	{
		item->setFlags(flags | Qt::ItemIsEditable);
		_tree->editItem( item, column );
	}
}
/**********************************************/
/**********************************************/
void ProjectTreeView::contextMenuEvent( QContextMenuEvent* event )
{
	contextMenu->exec( event->globalPos() );
}
/**********************************************/
/**********************************************/
void ProjectTreeView::enterEvent( QEnterEvent* event )
{
	if ( _tree_to_hide )
	{
		wid_cont->show();
		wid_cont->move( this->mapToGlobal( QPoint(0,0) ) );
		wid_cont->setFixedHeight( this->height() );
	}

	QWidget::enterEvent( event );
}
/**********************************************/
/**********************************************/
void ProjectTreeView::leaveEvent( QEvent* event )
{
	if ( _tree_to_hide &&
		 !wid_cont->geometry().contains(QCursor::pos()) )
	{
		wid_cont->hide();
	}
	QWidget::leaveEvent( event );
}
/**********************************************/
/**********************************************/
void ProjectTreeView::resizeEvent( QResizeEvent* event )
{
	if ( _abletohide )//this->width() < this->sizeHint().width()+2 )
	{
		_tree_to_hide = true;
		lay_main->removeWidget( wid_cont );
		wid_cont->setWindowFlag( Qt::ToolTip, true );
		wid_cont->hide();
		connect( wid_cont, &ProjectWidget::leaved,
				 wid_cont, &ProjectWidget::hide );
	}
	else
	{
		_tree_to_hide = false;
		lay_main->addWidget( wid_cont );
		wid_cont->setWindowFlag( Qt::ToolTip, false );
		wid_cont->setFixedHeight(QWIDGETSIZE_MAX);
		wid_cont->show();
		disconnect( wid_cont, &ProjectWidget::leaved,
					wid_cont, &ProjectWidget::hide );

	}
	QWidget::resizeEvent( event );
}

void ProjectTreeView::saveProjectChildren( QTextStream* out, ProjectItem2* item, int level )
{
	for ( int it = 0; it < item->childCount(); ++it )
	{
		ProjectItem2* ch_item = static_cast<ProjectItem2*>(item->child(it));
		for ( int it = 0; it < level; ++it )
			*out << "#";
		*out << Key::toString(Key::Keys(ch_item->setting(Key::Type).toInt())).toUpper();
		for ( const Key& key : ch_item->settings().keys() )
			if ( key != Key::Type )
				*out << " " << key.toString() << "=\"" << ch_item->setting(key).toString() << "\"";
		*out << "\n";
		saveProjectChildren( out, ch_item, level+1 );
	}
}
/**********************************************/
/**********************************************/
void ProjectTreeView::saveProjects()
{
	qDebug() << "Saving Projects....";
	for ( int it = 0; it < _tree->topLevelItemCount(); ++it )
	{
		ProjectItem2* prj_item = static_cast<ProjectItem2*>(_tree->topLevelItem(it));
		QFile prj_file(prj_item->setting(Key::Path).toString()+"/"+prj_item->setting(Key::Name).toString()+".gpj");
		if ( !prj_file.open( QIODevice::WriteOnly | QIODevice::Text ) )
			return;
		QTextStream prj_data( &prj_file );
		for ( const Key& key : prj_item->settings().keys() )
			if ( key != Key::Type )
				prj_data << key.toString() << "=\"" << prj_item->setting(key).toString() << "\"\n";
		saveProjectChildren( &prj_data, prj_item );
		prj_file.close();
	}
}
/**********************************************/
/**********************************************/
