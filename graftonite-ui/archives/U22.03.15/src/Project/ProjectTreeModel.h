#ifndef PROJECTTREEMODEL_H
#define PROJECTTREEMODEL_H
/**********************************************/
#include <QAbstractItemModel>
#include <QModelIndex>
#include <QVariant>
/**********************************************/
class ProjectTreeItem;
class Project;
/**********************************************/
/**********************************************/
/**********************************************/
class ProjectTreeModel
		: public QAbstractItemModel
{
	Q_OBJECT
public:
	~ProjectTreeModel();
	explicit ProjectTreeModel( const QString& data, QObject* parent = nullptr );
	ProjectTreeModel( const QList<Project*>& data, QObject* parent = nullptr );
	/**********************************************/
	// Reimplementation of QAbstractItemModel methods
	bool setData( const QModelIndex& index, const QVariant& value, const int role) override;
	QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
	Qt::ItemFlags flags( const QModelIndex& index ) const override;
	QVariant headerData( int section, Qt::Orientation orientation, int role = Qt::DisplayRole ) const override;
	bool setHeaderData( int section, Qt::Orientation orientation, const QVariant& value, int role = Qt::EditRole ) override;
	QModelIndex index( int row, int column, const QModelIndex& parent = QModelIndex() ) const override;
	QModelIndex parent( const QModelIndex& index ) const override;
	int rowCount( const QModelIndex& index = QModelIndex() ) const override;
	int columnCount( const QModelIndex& index = QModelIndex() ) const override;
	/**********************************************/
	//
	bool insertRows( int position, int rows, const QModelIndex& index = QModelIndex() ) override;
	bool removeRows( int position, int rows, const QModelIndex& index = QModelIndex() ) override;
	/**********************************************/
	ProjectTreeItem* item( const QModelIndex& index ) const;
	/**********************************************/
private:
	void setupModelData( const QStringList& lines, ProjectTreeItem* parent );
	void setupModelData( Project* project, ProjectTreeItem* parent );
	ProjectTreeItem* _rootItem;
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PROJECTTREEMODEL_H
