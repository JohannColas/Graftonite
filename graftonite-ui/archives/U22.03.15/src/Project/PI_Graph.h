//#ifndef PI_GRAPH_H
//#define PI_GRAPH_H

//#include "ProjectItem.h"

//class PI_Graph : public ProjectItem
//{
//	QList<Settings> _frames;
//	QList<Settings> _axes;
//	QList<Settings> _plots;
//	QList<Settings> _legends;
//	QList<Settings> _titles;
//	QList<Settings> _shapes;
//public:
//	PI_Graph();

//	QList<Settings>& frames()
//	{
//		return _frames;
//	}
//	QList<Settings>& axes()
//	{
//		return _axes;
//	}
//	QList<Settings>& plots()
//	{
//		return _plots;
//	}
//	QList<Settings>& legends()
//	{
//		return _legends;
//	}
//	QList<Settings>& titles()
//	{
//		return _titles;
//	}
//	QList<Settings>& shapes()
//	{
//		return _shapes;
//	}
//	void addFrame( const QString& name )
//	{
//		Settings newframe = Settings();
//		newframe.addSetting( Key::Name, name );
//		_frames.append( newframe );
//		emit framesChanged();
//	}
//	void addAxis( const QString& name )
//	{
//		Settings newaxis = Settings();
//		newaxis.addSetting( Key::Name, name );
//		_axes.append( newaxis );
//		emit axesChanged();
//	}
//	void addPlot( const QString& name )
//	{
//		Settings newplot = Settings();
//		newplot.addSetting( Key::Name, name );
//		_plots.append( newplot );
//		emit plotsChanged();
//	}
//	void addLegend( const QString& name )
//	{
//		Settings newlegend = Settings();
//		newlegend.addSetting( Key::Name, name );
//		_legends.append( newlegend );
//		emit legendsChanged();
//	}
//	void addTitle( const QString& name )
//	{
//		Settings newtitle = Settings();
//		newtitle.addSetting( Key::Name, name );
//		_titles.append( newtitle );
//		emit titlesChanged();
//	}
//	void addShape( const QString& name )
//	{
//		Settings newshape = Settings();
//		newshape.addSetting( Key::Name, name );
//		_shapes.append( newshape );
//		emit shapesChanged();
//	}
//	void moveFrame( int oldindex, int newindex )
//	{
//		if ( oldindex < 0 || oldindex >= _frames.size() ||
//			 newindex < 0 || newindex >= _frames.size() )
//			return;
//		_frames.move( oldindex, newindex );
//		emit framesChanged();
//	}
//	void removeFrame( int index )
//	{
//		if ( index < 0 || index >= _frames.size() )
//			return;
//		_frames.removeAt( index );
//		emit framesChanged();
//	}
//	void removeAxis( int index )
//	{
//		if ( index < 0 || index >= _axes.size() )
//			return;
//		_axes.removeAt( index );
//		emit axesChanged();
//	}
//	void removePlot( int index )
//	{
//		if ( index < 0 || index >= _plots.size() )
//			return;
//		_plots.removeAt( index );
//		emit plotsChanged();
//	}
//	void removeLegend( int index )
//	{
//		if ( index < 0 || index >= _legends.size() )
//			return;
//		_legends.removeAt( index );
//		emit legendsChanged();
//	}
//	void removeTitle( int index )
//	{
//		if ( index < 0 || index >= _titles.size() )
//			return;
//		_titles.removeAt( index );
//		emit titlesChanged();
//	}
//	void removeShape( int index )
//	{
//		if ( index < 0 || index >= _shapes.size() )
//			return;
//		_shapes.removeAt( index );
//		emit shapesChanged();
//	}


//signals:
//	void framesChanged();
//	void axesChanged();
//	void plotsChanged();
//	void legendsChanged();
//	void titlesChanged();
//	void shapesChanged();
//	void frameChanged( Settings* settings );
//	void axisChanged( Settings* settings );
//	void plotChanged( Settings* settings );
//	void legendChanged( Settings* settings );
//	void titleChanged( Settings* settings );
//	void shapeChanged( Settings* settings );
//};

//#endif // PI_GRAPH_H
