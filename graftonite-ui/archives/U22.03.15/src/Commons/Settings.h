#ifndef SETTINGS_H
#define SETTINGS_H
/**********************************************/
#include <QObject>
#include <QPair>
#include <QVariant>
/**********************************************/
#include "Key.h"
/**********************************************/
/**********************************************/
/**********************************************/
class Settings
		: public QObject
{
	Q_OBJECT
private:
	QList<QPair<Key,QVariant>> _settings;
	/******************************************/
public:
	Settings( QObject* parent = nullptr );
	Settings( const Settings& settings );
	/******************************************/
	void addSetting( const Key& key, const QVariant& value );
	void set( const QVariant& value, const Key& key );
	/******************************************/
	QList<Key> keys() const;
	bool hasKey( const Key& key ) const;
	/******************************************/
	QList<QPair<Key,QVariant>> settings() const;
	QVariant value( const Key& key ) const;
	bool value( QVariant& value, const Key& key ) const;
	QString getString( const Key& key ) const;
	bool getKey( Key& value, const Key& key ) const;
	Key getKey( const Key& key ) const;
	bool getString( QString& value, const Key& key ) const;
	bool getBool( const Key& key ) const;
	bool getBool( bool& value, const Key& key ) const;
	int getInt( const Key& key ) const;
	bool getInt( int& value, const Key& key ) const;
	double getDouble( const Key& key ) const;
	bool getDouble( double& value, const Key& key ) const;
	QColor getColor( const Key& key ) const;
	bool getColor( QColor& value, const Key& key ) const;
	QList<qreal> getDash( const Key& key ) const;
	bool getDash( QList<qreal>& value, const Key& key ) const;
	QPen getPen( const Key& key ) const;
	bool getPen( QPen& pen, const Key& key ) const;
	QBrush getBrush( const Key& key ) const;
	/******************************************/
	static int DecomposeLine( const QString& line, Settings& settings );
	/******************************************/
public slots:
	void changeSetting( const Key& key, const QVariant& value );
	/******************************************/
signals:
	void changed();
	/******************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SETTINGS_H
