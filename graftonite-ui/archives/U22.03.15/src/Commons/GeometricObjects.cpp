#include "GeometricObjects.h"
/**********************************************/
#include <QString>
/**********************************************/
/**********************************************/
/**********************************************/
Point::Point()
    : QPointF()
{

}
Point::Point( double x, double y )
    : QPointF(x, y)
{
	setX(x);
	setY(y);
}
Point::Point( const Point& point )
    : QPointF(point.x(), point.y())
{
}
Point::Point( const QPointF& point )
    : QPointF(point)
{
}
Point& Point::operator=( const Point& point )
{
	this->setX( point.x() );
	this->setY( point.y() );
	return *this;
}
Point& Point::operator+=( const Point& point )
{
	this->setX( this->x() + point.x() );
	this->setY( this->y() + point.y() );
	return *this;
}
Point Point::operator+( const Point& point )
{
	Point new_point;
	new_point.setX( this->x() + point.x() );
	new_point.setY( this->y() + point.y() );
	return new_point;
}
Point& Point::operator-=( const Point& point )
{
	this->setX( this->x() - point.x() );
	this->setY( this->y() - point.y() );
	return *this;
}
Point Point::operator-( const Point& point )
{
	Point new_point;
	new_point.setX( this->x() - point.x() );
	new_point.setY( this->y() - point.y() );
	return new_point;
}
/**********************************************/
/**********************************************/
/**********************************************/
Anchor::Anchor()
{
}
Anchor::Anchor( const Key::Keys& anchor )
{
	_anchor = anchor;
}
bool Anchor::isLeft() const
{
	return ( _anchor == Key::Left ||
	         _anchor == Key::BaselineLeft ||
	         _anchor == Key::TopLeft ||
	         _anchor == Key::BottomLeft );
}
bool Anchor::isRight() const
{
	return ( _anchor == Key::Right ||
	         _anchor == Key::BaselineRight ||
	         _anchor == Key::TopRight ||
	         _anchor == Key::BottomRight );
}
bool Anchor::isHCenter() const
{
	return ( _anchor == Key::Center ||
	         _anchor == Key::Baseline ||
	         _anchor == Key::Top ||
	         _anchor == Key::Bottom );
}
bool Anchor::isTop() const
{
	return ( _anchor == Key::Top ||
	         _anchor == Key::TopLeft ||
	         _anchor == Key::TopRight );
}
bool Anchor::isBottom() const
{
	return ( _anchor == Key::Bottom ||
	         _anchor == Key::BottomLeft ||
	         _anchor == Key::BottomRight );
}
bool Anchor::isVCenter() const
{
	return ( _anchor == Key::Center ||
	         _anchor == Key::Left ||
	         _anchor == Key::Right );
}
bool Anchor::isBaseline() const
{
	return ( _anchor == Key::Baseline ||
	         _anchor == Key::BaselineLeft ||
	         _anchor == Key::BaselineRight );
}
/**********************************************/
/**********************************************/
/**********************************************/
Line::Line()
    : QLineF()
{

}
Line::Line( const Point& p1, const Point& p2 )
    : QLineF(p1, p2)
{
}
Line::Line( double x1, double y1, double x2, double y2 )
    : QLineF(x1,y1,x2,y2)
{
}
Line::Line( double x, double y, double length, const Orientation& orientation )
    : QLineF()
{
	setP1( {x,y} );
	if ( orientation == Line::Horizontal )
		setP2( {x+length,y} );
	else
		setP2( {x,y+length} );
}
Line::Line( const Point& origin, double shift, double length, const Orientation& orientation )
    : QLineF()
{
	if ( orientation == Line::Horizontal )
	{
		setP1( {origin.x()+shift, origin.y()} );
		setP2( {p1().x()+length, origin.y()} );
	}
	else
	{
		setP1( {origin.x(), origin.y()+shift} );
		setP2( {origin.x(), p1().y()+length} );
	}
}
Line::Line( const QLineF& line )
    : QLineF(line)
{
}
/**********************************************/
/**********************************************/
/**********************************************/
Size::Size()
    : QSizeF()
{

}
Size::Size( double width, double height )
    : QSizeF(width, height)
{
	setWidth(width);
	setHeight(height);
}
Size::Size( const QString& size )
    : QSizeF()
{
	if ( size.contains("x") || size.contains(",") )
	{
		QStringList sls;
		if ( size.contains("x") )
			sls = size.split("x");
		else if ( size.contains(",") )
			sls = size.split(",");
		if ( sls.size() > 1 )
		{
			setWidth( sls.at(0).toDouble() );
			setHeight( sls.at(1).toDouble() );
		}
	}
//	else if ( size.isDouble() || size.isInteger() )
//	{
//		_width = size.toDouble();
//		_height = _width;
//	}
}
Point Size::point(const Key& anchor) const
{
	if ( anchor == Key::Top )
		return Point(0.5*width(),0);
	if ( anchor == Key::TopRight )
		return Point(width(),0);
	if ( anchor == Key::Left )
		return Point(0,0.5*height());
	if ( anchor == Key::Center )
		return Point(0.5*width(),0.5*height());
	if ( anchor == Key::Right )
		return Point(width(),0.5*height());
	if ( anchor == Key::BottomLeft )
		return Point(0,height());
	if ( anchor == Key::Bottom )
		return Point(0.5*width(),height());
	if ( anchor == Key::BottomRight )
		return Point(width(),height());
	return Point(0,0);
}
/**********************************************/
/**********************************************/
/**********************************************/
Rect::Rect()
    : QRectF()
{

}
Rect::Rect( double x, double y, double width, double height )
    : QRectF(x, y, width, height)
{
}
Rect::Rect( const Point& point, const Size& size )
    : QRectF(point,size)
{
}
Rect::Rect( const Line& line )
    : QRectF()
{
	double x = line.x1();
	double width = line.x2()-x;
	if ( line.x1() > line.x2() )
	{
		x = line.x2();
		width = line.x1() - x;
	}
	double y = line.y1();
	double height = line.y2()-y;
	if ( line.y1() > line.y2() )
	{
		y = line.y2();
		height = line.y1() - y;
	}
	setRect( x, y, width, height );
}
Rect::Rect( const QString& rect )
    : QRectF()
{
//	QStringList parts = rect.split(",");
//	if ( parts.size() == 2 )
//	{
//		setPoint( parts.at(0) );
//		setSize( parts.at(1) );
//	}
//	else if ( parts.size() == 4 )
//	{
//		setX( parts.at(0).toDouble() );
//		setY( parts.at(1).toDouble() );
//		setWidth( parts.at(2).toDouble() );
//		setHeight( parts.at(3).toDouble() );
	//	}
}
Rect::Rect( const QRectF& rect )
    : QRectF(rect)
{

}
Point Rect::origin() const
{
	return Point( x(), y() );
}
Point Rect::point( const Key& anchor ) const
{
	if ( anchor == Key::Top )
		return topLine().center();
	if ( anchor == Key::TopRight )
		return topRight();
	if ( anchor == Key::Left )
		return leftLine().center();
	if ( anchor == Key::Center )
		return center();
	if ( anchor == Key::Right )
		return rightLine().center();
	if ( anchor == Key::BottomLeft )
		return bottomLeft();
	if ( anchor == Key::Bottom )
		return bottomLine().center();
	if ( anchor == Key::BottomRight )
		return bottomRight();
	return topLeft();
}
Line Rect::topLine() const
{
	return Line(topLeft(), topRight());
}

Line Rect::hCenterLine() const
{

	return Line(left(), y()+0.5*height(), right(), y()+0.5*height());
}
Line Rect::bottomLine() const
{
	return Line(bottomLeft(), bottomRight());
}

Line Rect::leftLine() const
{
	return Line(topLeft(), bottomLeft());
}

Line Rect::vCenterLine() const
{
	return Line(x()+0.5*width(), top(), x()+0.5*width(), bottom());
}

Line Rect::rightLine() const
{
	return Line(topRight(), bottomRight());
}

Line Rect::line(const Key& pos, bool vcenter) const
{
	if ( pos == Key::Top )
		return topLine();
	if ( pos == Key::Bottom )
		return bottomLine();
	if ( pos == Key::Left )
		return leftLine();
	if ( pos == Key::Right )
		return rightLine();
	if ( pos == Key::Center && vcenter )
		return vCenterLine();
	return hCenterLine();
}
bool Rect::contains( const Point& pos )
{
	if ( left() <= pos.x() &&
	     pos.x() <= right() &&
	     top() <= pos.y() &&
	     pos.y() <= bottom() )
		return true;
	return false;
}
bool Rect::contains( const Line& line )
{
	if ( this->contains(line.p1()) &&
	     this->contains(line.p2()) )
		return true;
	return false;
}
bool Rect::contains( const Rect& rect )
{

	if ( this->contains(rect.topLeft()) &&
	     this->contains(rect.bottomRight()) )
		return true;
	return false;
}
void Rect::expandTo( const Point& pos )
{
	if ( this->contains(pos) )
		return;
	double px = pos.x(), py = pos.y();
	double xl = left(), xr = right(),
	        yt = top(), yb = bottom();
	if ( px < xl )
	{
		setX(px);
		setWidth(xr - x());
	}
	else if ( px > xr )
		setWidth(px - x());
	if ( py < yt )
	{
		setY(py);
		setHeight(yb - y());
	}
	else if ( py > yb )
		setHeight(py - y());
}
void Rect::expandTo( const Line& line )
{
	if ( this->contains(line) )
		return;
	expandTo( line.p1() );
	expandTo( line.p2() );
}
void Rect::expandTo( const Rect& rect )
{
	if ( this->contains(rect) )
		return;
	expandTo( rect.topLeft() );
	expandTo( rect.bottomRight() );
}
/**********************************************/
/**********************************************/
/**********************************************/
