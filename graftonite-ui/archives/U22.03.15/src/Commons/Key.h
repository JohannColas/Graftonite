#ifndef KEYS_H
#define KEYS_H
/**********************************************/
#include <QList>
/**********************************************/
/**********************************************/
/**********************************************/
class Key
{
public:
	enum Keys
	{
		Unknown,
		// App Settings
		Lang,
		Theme,
		IconTheme,
		NoType,
		Type,
		ID,

		// Project Elements
		Root,
		Project,
		Folder,
		Data,
		DataSheet,
		Graph,
		Image,
		TextFile,
		File,

		// File Keys
		Name,
		FileName,
		Path,
		Working,

		// Position
		Position,
		Top,
		Bottom,
		Left,
		Right,
		Center,
		Outside,
		Inside,
		BottomLeft,
		BottomRight,
		TopLeft,
		TopRight,
		BaselineLeft,
		Baseline,
		BaselineRight,
		BothSides,

		// Size
		Size,
		Width,

		// Graph Elements
		// Graph => already defined
		Template,
		Layer,
		Frame,
		Axis,
		Plot,
		Legend,
		Title,
		Shape,
		Text,
		Current,
		// Graph Format
		Format,
		PNG,
		PDF,
		SVG,
		PS,
		TIFF,
		JPEG,
		BMP,

		// Axis Settings
		X,
		Y,
		Polar,
		Min,
		Max,
		Scale,
		Linear,
		Log10,
		Log,
		LogX,
		Reciprocal,
		OffsetReciprocal,
		LinkTo,
		Minor,
		Ticks,
		Increment,
		Numbers,
		Labels,
		// Title => already define
		Grids,

		// Data Settings
		Coordinates,
		Function,

		// Plot Settings
		XY,
		XYY,
		Axis1,
		Axis2,
		Axis3,
		Data1,
		Data2,
		Data3,
		Area,
		Bars,
		ErrorBars,

		// Legend Settings
		Entry,
		AddEntry,

		// Geometry Keys
		Spacing,
		Margins,
		Anchor,
		Shift,
		Transform,

		// Layout Keys
		Layout,
		Vertical,
		Horizontal,
		Table,
		Row,
		Column,

		// Fill Style Settings
		Background,
		Fill,

		// Line Style Settings
		Borders,
		Line,
		Dash,
		Join, Round, Miter, Bevel,
		MiterLimit,
		Cap, Butt, Square,
		Gap,

		// Text Style Settings
		Font,
		Family,
		Weight,
		Slant,
		Underline,
		Capitalize,
		ScriptOpt,

		Symbols,
		Covering,

		// Other Settings
		General,
		Style,
		Hide,
		Offset,
		Option,
		Color,
		Alignment,
		Rotation,
		BeginAngle,
		EndAngle,
		Separator,
	};
	/**********************************************/
	static QString toString( const Key::Keys& key );
	static Key::Keys toKeys( const QString& keys );
	static QList<Key::Keys> toKeysList( const QString& key );
	static void setKeysList( const QString& keys, QList<Key::Keys>& key_list );
	static bool isElementType( const Key::Keys& key );
	/**********************************************/
private:
	QList<Key::Keys> _keys;
	QString _original_key;
	/**********************************************/
public:
	Key();
	Key( const QVariant& key );
	Key( const QString& keys );
	Key( const Key::Keys& key, const Key::Keys& key2 = Key::Unknown, const Key::Keys& key3 = Key::Unknown );
	Key( const Key& key, const Key::Keys& key2 = Key::Unknown, const Key::Keys& key3 = Key::Unknown );
	Key( const QList<Key::Keys>& keys, const Key::Keys& key = Key::Unknown, const Key::Keys& key2 = Key::Unknown );
	/**********************************************/
	QList<Key::Keys> keys() const;
	QString toString() const;
	int toInt() const;
	static QString toPositonString( const Key& key );
	/**********************************************/
	bool isLineStyle( const Key& keys, bool include_cap = true, bool include_join = true ) const;
	bool isFillStyle( const Key& key ) const;
	bool isTextStyle( const Key& key ) const;
	/**********************************************/
	// Operators
	bool operator<( const Key& key ) const;
	bool operator==( const Key& key ) const;
	bool operator!=( const Key& key ) const;
	bool operator==( const Key::Keys& key ) const;
	bool operator!=( const Key::Keys& key ) const;
	bool operator==( const QList<Key::Keys>& keys ) const;
	/**********************************************/
	friend QDataStream& operator<<( QDataStream& os, const Key& key )
	{
		os << key.toString();
		return os;
	}
};
/**********************************************/
/**********************************************/
/**********************************************/


/**********************************************/
//#include <QString>
/**********************************************/
/**********************************************/
/**********************************************/
//namespace Keys {
//	static inline QString Project = "project";
//	static inline QString Folder = "folder";
//	static inline QString Graph = "graph";
//	static inline QString Width = "width", Height = "height";
//	static inline QString X = "x", Y = "y";
//	static inline QString Layers = "layers";
//	static inline QString Layer = "layer";
//	static inline QString Axis = "axis";
//	static inline QString Type = "type";
//	static inline QString Option = "option";
//	static inline QString Position = "pos";
//	static inline QString Style = "style";
//	static inline QString General = "general";
//	static inline QString Min = "min";
//	static inline QString Max = "max";
//	static inline QString Scale = "scale";
//	static inline QString Line = "line";
//	static inline QString Scatter = "scatter";
//	static inline QString Area = "area";
//	static inline QString Ticks = "ticks";
//	static inline QString MinorTicks = "minorticks";
//	static inline QString Labels = "labels";
//	static inline QString Title = "title";
//	static inline QString Curve = "curve";
//	static inline QString Size = "size";
//	static inline QString Interval = "interval";
//	static inline QString Shift = "shift";
//	static inline QString Name = "name";
//	static inline QString ID = "id";
//	static inline QString Data = "data";
//	static inline QString Axes = "axes";
//	static inline QString Axis1 = "axis1", Axis2 = "axis2", Axis3 = "axis3";
//	static inline QString List = "list";
//	static inline QString Gap = "gap";
//	static inline QString Path = "path";
//	static inline QString DataPath = "datapath";
//	static inline QString Indexes = "indexes";
//	static inline QString Index1 = "index1", Index2 = "index2", Index3 = "index3";
//	static inline QString Raw = "raw";
//	static inline QString File = "file";
//	static inline QString Number = "number";
//	static inline QString PostTreatment = "posttreatment";
//	static inline QString Treatment = "treatment";
//	static inline QString Decimal = "decimal";
//	static inline QString Separator = "separator";
//	static inline QString SkipEmptyParts = "skip_empty_parts";
//	static inline QString BeginRow = "begin_row";
//	static inline QString EndRow = "end_row";
//	static inline QString RowToTreat = "row_to_treat";
//}

#endif // KEYS_H
