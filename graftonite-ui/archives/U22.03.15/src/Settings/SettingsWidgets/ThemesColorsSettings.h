#ifndef THEMESCOLORSSETTINGS_H
#define THEMESCOLORSSETTINGS_H

#include <QSplitter>
#include <QWidget>
#include <QLabel>
#include <QTabWidget>
#include <QVBoxLayout>

/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class ThemesColorsSettings
		: public QWidget
{
	Q_OBJECT
private:
	QVBoxLayout* lay_main = new QVBoxLayout;
	QLabel* lab_title = new QLabel;
	QTabWidget* wid_tab = new QTabWidget;

public:
	explicit ThemesColorsSettings( QWidget *parent = nullptr );
	~ThemesColorsSettings();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // THEMESCOLORSSETTINGS_H
