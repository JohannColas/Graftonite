#ifndef PROJECTMODEL_H
#define PROJECTMODEL_H
/**********************************************/
#include <QAbstractItemModel>
/**********************************************/
class QItemSelectionModel;
/**********************************************/
#include "ProjectItem.h"
/**********************************************/
/**********************************************/
/**********************************************/
class ProjectModel
        : public QAbstractItemModel
{
	Q_OBJECT
public:
	~ProjectModel();
	ProjectModel( QObject* parent = nullptr );
	/******************************************/
	QModelIndex index( int row, int column, const QModelIndex& parent = QModelIndex() ) const override;
	QModelIndex parent( const QModelIndex& child ) const override;
	/******************************************/
	int rowCount( const QModelIndex& parent = QModelIndex() ) const override;
	int columnCount( const QModelIndex& parent = QModelIndex() ) const override;
	/******************************************/
	bool insertRows( int row, int count, const QModelIndex& parent = QModelIndex() ) override;
	bool removeRows( int row, int count, const QModelIndex& parent = QModelIndex() ) override;
	bool insertRow( int row, ProjectItem* item = nullptr, const QModelIndex& parent = QModelIndex() );
	bool removeRow( int row, const QModelIndex& parent = QModelIndex() );
	bool deleteRow( int row, const QModelIndex& parent = QModelIndex() );
	bool insertColumns( int column, int count, const QModelIndex& parent = QModelIndex() ) override;
	bool removeColumns( int column, int count, const QModelIndex& parent = QModelIndex() ) override;
	/******************************************/
	QVariant headerData( int section, Qt::Orientation orientation, int role = Qt::DisplayRole ) const override;
	QVariant data( const QModelIndex& index, int role = Qt::DisplayRole ) const override;
	QMap<int,QVariant> itemData( const QModelIndex& index ) const override;
	/******************************************/
	Qt::ItemFlags flags( const QModelIndex& index ) const override;
	/******************************************/
	bool setData( const QModelIndex& index, const QVariant& value, int role = Qt::DisplayRole ) override;
	bool setItemData( const QModelIndex& index, const QMap<int,QVariant>& roles ) override;
	/******************************************/
	QStringList mimeTypes() const override;
	QMimeData* mimeData( const QModelIndexList &indexes ) const override;
	Qt::DropActions supportedDropActions() const override;
	bool canDropMimeData( const QMimeData* data, Qt::DropAction action, int row, int column, const QModelIndex& parent );
	bool dropMimeData( const QMimeData* data, Qt::DropAction action, int row, int column, const QModelIndex& parent ) override;
	/******************************************/
	//void sort(int column, Qt::SortOrder order = Qt::AscendingOrder) override;
	void setItemSelectionModel( QItemSelectionModel* selection );
	/******************************************/
	// Child manager
	bool addChild( const QString& name, const QString& path, const Key::Keys& type );
	bool insertChild( const QString& name, const QString& path, const Key::Keys& type );
	/******************************************/
public slots:
	// Project functions
	void createProject();
	void openProject();
	void saveProject();
	void closeProject();
	// File functions
	void addFolder();
	void addDataSheet();
	void addGraph();
	void addImage();
	void insertFolder();
	void insertDataSheet();
	void insertGraph();
	void insertImage();
	void removeFile();
	void openFile();
	void saveFile();
	void closeFile();
	// Import/Export functions
	void importFile();
	void exportFile();
	/******************************************/
private:
	ProjectItem* _rootItem = nullptr;
	ProjectItem* projectItemAt( const QModelIndex& index ) const;
	/******************************************/
	QItemSelectionModel* _selectionModel = nullptr;
	/******************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PROJECTMODEL_H
