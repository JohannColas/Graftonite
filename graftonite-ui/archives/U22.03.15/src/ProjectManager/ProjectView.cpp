#include "ProjectView.h"

ProjectView::ProjectView(QWidget* parent)
    : QTreeView(parent)
{
	setSelectionMode(QAbstractItemView::ExtendedSelection);
//	viewport()->setAcceptDrops(true);
	setDragEnabled(true);
	setDropIndicatorShown(true);
	setDragDropMode(QAbstractItemView::InternalMove);
}
