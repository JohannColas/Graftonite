#include "ProjectItem.h"

ProjectItem::~ProjectItem()
{
	qDeleteAll(_children);
}

ProjectItem::ProjectItem( ProjectItem* parent )
    : _parent(parent)
{

}

ProjectItem::ProjectItem( const QString& name, const QString& path, const Key::Keys& type, ProjectItem* parent )
    : _name(name), _path(path), _type(type), _parent(parent)
{

}

QString ProjectItem::name() const
{
	return _name;
}

QString ProjectItem::path() const
{
	return _path;
}

QString ProjectItem::id() const
{
	return _id;
}

Key::Keys ProjectItem::type() const
{
	return _type;
}

void ProjectItem::setName( const QString& name )
{
	_name = name;
}

void ProjectItem::setPath( const QString& path )
{
	_path = path;
}

void ProjectItem::setID( const QString& id )
{
	_id = id;
}

void ProjectItem::setType( const Key::Keys& type )
{
	_type = type;
}

int ProjectItem::row() const
{
	if ( _parent == nullptr )
		return 0;//-1;
	return _parent->indexOf(const_cast<ProjectItem*>(this));
}

bool ProjectItem::indexValid( const int& index ) const
{
	return ( -1 < index && index < _children.size() );
}

int ProjectItem::childCount() const
{
	return _children.count();
}

ProjectItem* ProjectItem::childAt( int index )
{
	if ( indexValid(index) )
		return _children.at(index);
	return nullptr;
}

int ProjectItem::indexOf( ProjectItem* item ) const
{
	int it = 0;
	for ( ProjectItem* child : _children )
	{
		if ( child == item )
			return it;
		++it;
	}
	return -1;
}

void ProjectItem::appendChild( ProjectItem* item )
{
	_children.append( item );
}

bool ProjectItem::insertChild( int index, ProjectItem* item )
{
	if ( index+1 > _children.size() )
		_children.append( item );
	else
	{
		if ( indexValid(index) )
			_children.insert( index, item );
		else
			return false;
	}
	return true;
}

bool ProjectItem::removeChild( int index )
{
	if ( indexValid(index) )
		_children.removeAt(index);
//		delete _children.takeAt(index);
	else
		return false;
	return true;
}

bool ProjectItem::deleteChild(int index)
{
	if ( indexValid(index) )
		delete _children.takeAt(index);
	else
		return false;
	return true;
}

ProjectItem* ProjectItem::parent()
{
	return _parent;
}

void ProjectItem::setParent( ProjectItem* parent )
{
	_parent	= parent;
}
