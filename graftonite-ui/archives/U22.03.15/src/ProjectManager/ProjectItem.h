#ifndef PROJECTITEM_H
#define PROJECTITEM_H
/**********************************************/
#include <QList>
#include <QString>
/**********************************************/
#include "../Commons/Key.h"
/**********************************************/
/**********************************************/
/**********************************************/
class ProjectItem
{
public:
	~ProjectItem();
	ProjectItem( ProjectItem* parent = nullptr );
	ProjectItem( const QString& name, const QString& path, const Key::Keys& type = Key::NoType, ProjectItem* parent = nullptr );
	/******************************************/
	QString name() const;
	QString path() const;
	QString id() const;
	Key::Keys type() const;
	void setName( const QString& name );
	void setPath( const QString& path );
	void setID( const QString& id );
	void setType( const Key::Keys& type );
	/******************************************/
	int row() const;
	bool indexValid( const int& index ) const;
	int childCount() const;
	ProjectItem* childAt( int index );
	int indexOf( ProjectItem* item ) const;
	void appendChild( ProjectItem* item );
	bool insertChild( int index, ProjectItem* item );
	bool removeChild( int index );
	bool deleteChild( int index );
	/******************************************/
	ProjectItem* parent();
	void setParent( ProjectItem* parent );
	/******************************************/
private:
	QString _name;
	QString _path;
	QString _id;
	Key::Keys _type = Key::NoType;
	/******************************************/
	ProjectItem* _parent = nullptr;
	QList<ProjectItem*> _children;
	/******************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PROJECTITEM_H
