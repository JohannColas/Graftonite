#include "ProjectModel.h"

#include <QIODevice>
#include <QMimeData>

#include "ProjectItem.h"

#include <QItemSelectionModel>

ProjectModel::~ProjectModel()
{
	delete _rootItem;
}

ProjectModel::ProjectModel( QObject *parent )
    : QAbstractItemModel(parent)
{
	 _rootItem = new ProjectItem;
	ProjectItem* item = new ProjectItem("hdskjf", "hgdkjgf", Key::Project, _rootItem);
	ProjectItem* item2 = new ProjectItem("aaeaz'é", "hgdkjgf", Key::Graph, item);
	item->appendChild(item2);
	_rootItem->appendChild(item);
	_rootItem->appendChild(new ProjectItem("215464", "hgdkjgf", Key::Project, _rootItem));
	_rootItem->appendChild(new ProjectItem("fgdfgdf", "hgdkjgf", Key::Project, _rootItem));
	_rootItem->appendChild(new ProjectItem("lpwkmqsm", "hgdkjgf", Key::Project, _rootItem));
}

QModelIndex ProjectModel::index( int row, int column, const QModelIndex& parent ) const
{
	if ( !hasIndex(row, column, parent) )
		return QModelIndex();

	ProjectItem* parentItem;

	if ( !parent.isValid() )
		parentItem = _rootItem;
	else
		parentItem = this->projectItemAt(parent);

	ProjectItem* childItem = parentItem->childAt(row);
	if ( childItem == nullptr )
		return QModelIndex();

	return createIndex(row, column, childItem);
}

QModelIndex ProjectModel::parent( const QModelIndex& child ) const
{
	if ( !child.isValid() )
		return QModelIndex();

	ProjectItem* childItem = this->projectItemAt(child);
	ProjectItem* parentItem = childItem->parent();

	if ( parentItem == nullptr || parentItem == _rootItem )
		return QModelIndex();

	return createIndex(parentItem->row(), 0, parentItem);
}

int ProjectModel::rowCount( const QModelIndex& parent ) const
{
	if ( parent.column() > 0 )
		return 0;

	ProjectItem* parentItem;
	if ( !parent.isValid() )
		parentItem = _rootItem;
	else
		parentItem = this->projectItemAt(parent);

	return (parentItem == nullptr ? 0 :parentItem->childCount());
}

int ProjectModel::columnCount( const QModelIndex& parent ) const
{
	return 3;
}

bool ProjectModel::insertRows( int row, int count, const QModelIndex& parent )
{
	ProjectItem *parentItem = projectItemAt(parent);
	if ( !parentItem )
		return false;

	beginInsertRows(parent, row, row+count-1);
	const bool success = parentItem->insertChild( row, new ProjectItem );
	endInsertRows();

	return success;
}

bool ProjectModel::removeRows( int row, int count, const QModelIndex& parent )
{
	ProjectItem *parentItem = projectItemAt(parent);
	if ( !parentItem )
		return false;

	beginRemoveRows(parent, row, row+count-1);
	bool result = parentItem->removeChild(row);
	endRemoveRows();

	return result;
}

bool ProjectModel::insertRow( int row, ProjectItem* item, const QModelIndex& parent )
{
	ProjectItem *parentItem = projectItemAt(parent);
	if ( !parentItem )
		return false;

	beginInsertRows(parent, row, row);
	bool success = false;
	if ( item != nullptr )
		success = parentItem->insertChild(row, item);
	endInsertRows();

	return success;
}

bool ProjectModel::removeRow( int row, const QModelIndex& parent )
{
	ProjectItem *parentItem = projectItemAt(parent);
	if ( !parentItem )
		return false;

	beginRemoveRows(parent, row, row);
	bool result = parentItem->removeChild(row);
	endRemoveRows();

	return result;
}

bool ProjectModel::deleteRow( int row, const QModelIndex& parent )
{
	ProjectItem *parentItem = projectItemAt(parent);
	if ( !parentItem )
		return false;

	beginRemoveRows(parent, row, row);
	bool result = parentItem->deleteChild(row);
	endRemoveRows();

	return result;
}

bool ProjectModel::insertColumns( int column, int count, const QModelIndex& parent )
{
	return false;
}

bool ProjectModel::removeColumns( int column, int count, const QModelIndex& parent )
{
	return false;
}

QVariant ProjectModel::headerData( int section, Qt::Orientation orientation, int role ) const
{
	if ( orientation == Qt::Horizontal && role == Qt::DisplayRole )
	{
		switch (section)
		{
			case 0:
			return "Name";
			break;
			case 1:
			    return "Path";
			    break;
			case 2:
			    return "Type";
			    break;
		}
	}

	return QAbstractItemModel::headerData ( section, orientation, role );
}

QVariant ProjectModel::data( const QModelIndex& index, int role ) const
{
	if ( !index.isValid() )
		return QVariant();

	if ( role == Qt::DisplayRole || role == Qt::EditRole )
	{
		ProjectItem* item = this->projectItemAt(index);//static_cast<ProjectItem*>(index.internalPointer());
		if (index.column() == 0)
		{
			return item->name();
		}
		else if (index.column() == 1)
		{
			return item->path();
		}
		else if (index.column() == 2)
		{
			return item->type();
		}
	}
	else if ( role == Qt::DecorationRole )
	{
		if ( index.column() == 0 )
		{
//			Key type = projectType();
//			if ( type == Key::Project )
//				return QIcon("icons/Dark Theme/graftonite.svg");
//			else if ( type == Key::Folder )
//				return QIcon("icons/Dark Theme/folder.svg");
//			else if ( type == Key::Data )
//				return QIcon("icons/Dark Theme/data.svg");
//			else if ( type == Key::Graph )
//				return QIcon("icons/Dark Theme/graph.svg");
//			else if ( type == Key::Image )
//				return QIcon("icons/Dark Theme/image.svg");
		}
	}


	return QVariant();
}

QMap<int,QVariant> ProjectModel::itemData( const QModelIndex& index ) const
{
	QMap<int,QVariant> data;
	return data;
}

Qt::ItemFlags ProjectModel::flags ( const QModelIndex& index ) const
{
	if (!index.isValid())
		return Qt::NoItemFlags;

	Qt::ItemFlags defaultFlags = QAbstractItemModel::flags(index);

	defaultFlags = defaultFlags | Qt::ItemIsEditable;

	if ( !index.isValid()/* || parentItem == _rootItem*/ )
		defaultFlags = defaultFlags | Qt::ItemIsDropEnabled;
	else
		defaultFlags = defaultFlags | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled;

	return defaultFlags;
}

bool ProjectModel::setData ( const QModelIndex& index, const QVariant& value, int role )
{
	if ( !index.isValid() )
		return false;

	switch (role)
	{
	case Qt::DisplayRole:
	case Qt::EditRole:
			ProjectItem* item = projectItemAt(index);
			if ( item == nullptr || item == _rootItem )
				return false;
			if (index.column() == 0)
			{
				item->setName( value.toString() );
			}
			else if (index.column() == 1)
			{
				item->setPath( value.toString() );
			}
			emit dataChanged(index, index);
		return true;
		break;
	}

	return false;
}

bool ProjectModel::setItemData( const QModelIndex& index, const QMap<int,QVariant>& roles )
{
	return false;
}

QStringList ProjectModel::mimeTypes() const
{
	QStringList types;
	types << "application/vnd.text.list";
	return types;
}

QMimeData* ProjectModel::mimeData( const QModelIndexList& indexes ) const
{
	QMimeData* mimeData = new QMimeData;
	QByteArray encodedData;

	QDataStream stream( &encodedData, QIODevice::WriteOnly );

	for ( const QModelIndex &index : indexes )
	{
		if ( index.isValid() )
		{
			ProjectItem* item = projectItemAt(index);
			stream << item->path();
		}
	}

	mimeData->setData( "application/vnd.text.list", encodedData );
	return mimeData;
}

Qt::DropActions ProjectModel::supportedDropActions() const
{
	return (Qt::CopyAction | Qt::MoveAction);
}

bool ProjectModel::canDropMimeData( const QMimeData* data, Qt::DropAction action, int row, int column, const QModelIndex& parent )
{
	Q_UNUSED(action);
	Q_UNUSED(row);
	Q_UNUSED(parent);

	if (!data->hasFormat("application/vnd.text.list"))
		return false;

//	if (column > 0)
//		return false;

	return true;
}

bool ProjectModel::dropMimeData( const QMimeData* data, Qt::DropAction action, int row, int column, const QModelIndex& parent )
{
	QModelIndexList selection = _selectionModel->selectedRows(0);

	int beginRow;
	if ( row != -1 )
		beginRow = row;
	else if ( parent.isValid() )
		beginRow = parent.row();
	else
		beginRow = rowCount(parent);

	ProjectItem* newParentItem = projectItemAt(parent);

	for ( const QModelIndex& index : selection )
	{
		ProjectItem* item = projectItemAt(index);
		ProjectItem* parentItem = item->parent();
		if ( parentItem == _rootItem )
			continue;

		// Remove item
		int oldrow = index.row();
		beginRemoveRows(index.parent(), oldrow, oldrow);
		bool result = parentItem->removeChild(oldrow);
		endRemoveRows();

		if ( !result )
			continue;

		// Insert item
		beginInsertRows(parent, beginRow, beginRow);
		item->setParent(newParentItem);
		result = newParentItem->insertChild( beginRow, item );
		endInsertRows();

		if ( !result )
			continue;

		++beginRow;

	}
	return true;

//	if (!canDropMimeData(data, action, row, column, parent))
//		return false;

//	if (action == Qt::IgnoreAction)
//		return true;
//	int beginRow;

//	if (row != -1)
//		beginRow = row;
//	else if (parent.isValid())
//		beginRow = parent.row();
//	else
//		beginRow = rowCount(parent);

//	QByteArray encodedData = data->data("application/vnd.text.list");
//	QDataStream stream(&encodedData, QIODevice::ReadOnly);
//	QStringList newItems;
//	int rows = 0;

//	while (!stream.atEnd()) {
//		QString text;
//		stream >> text;
//		newItems << text;
//		++rows;
//	}

//	insertRows(beginRow, rows, parent);
//	for (const QString &text : qAsConst(newItems)) {
//		QModelIndex idx = index(beginRow, 0, parent);
//		setData(idx, text);
//		beginRow++;
//	}

//	return true;
}

void ProjectModel::setItemSelectionModel( QItemSelectionModel* selection )
{
	_selectionModel = selection;
}

bool ProjectModel::addChild( const QString& name, const QString& path, const Key::Keys& type )
{
	QModelIndex index = _selectionModel->currentIndex();
	if ( !index.isValid() )
		return false;
	ProjectItem* parentItem = projectItemAt(index);

	beginInsertRows( index, parentItem->childCount(), parentItem->childCount());
	ProjectItem* newchild = new ProjectItem(name, path, type, parentItem);
	parentItem->appendChild( newchild );
	endInsertRows();
	return true;
}

bool ProjectModel::insertChild( const QString& name, const QString& path, const Key::Keys& type )
{
	QModelIndex index = _selectionModel->currentIndex();
	if ( !index.isValid() )
		return false;
	ProjectItem* item = projectItemAt(index);
	ProjectItem* parentItem = item->parent();
	if ( parentItem == _rootItem )
		return false;
	QModelIndex parentIndex = index.parent();

	int row = index.row();
	this->beginInsertRows( parentIndex, row, row );
	ProjectItem* newchild = new ProjectItem(name, path, type, parentItem);
	bool result = parentItem->insertChild( row, newchild );
	this->endInsertRows();

	return result;
}

void ProjectModel::createProject()
{
	int row = _rootItem->childCount();
	this->beginInsertRows( QModelIndex(), row, row );
	ProjectItem* newproject = new ProjectItem("New Project", "", Key::Project, _rootItem);
	_rootItem->appendChild( newproject );
	this->endInsertRows();
	//    return success;
}

void ProjectModel::openProject()
{

}

void ProjectModel::saveProject()
{

}

void ProjectModel::closeProject()
{
	QModelIndex index = _selectionModel->currentIndex();
	if ( !index.isValid() )
		return /*false*/;
	ProjectItem* item = projectItemAt(index);
	ProjectItem* parentItem = item->parent();
	if ( parentItem == _rootItem )
	{
		int oldrow = _rootItem->indexOf( item );
		beginRemoveRows(QModelIndex(), oldrow, oldrow);
		/*bool result = */parentItem->removeChild(oldrow);
		endRemoveRows();
	}
}

void ProjectModel::addFolder()
{
	this->addChild( "New Folder", "", Key::Folder );
}

void ProjectModel::addDataSheet()
{
	this->addChild( "New Data Sheet", "", Key::DataSheet );
}

void ProjectModel::addGraph()
{
	this->addChild( "New Graph", "", Key::DataSheet );
}

void ProjectModel::addImage()
{
	this->addChild( "New Image", "", Key::Image );
}

void ProjectModel::insertFolder()
{
	this->insertChild( "New Folder", "", Key::Folder );
}

void ProjectModel::insertDataSheet()
{
	this->insertChild( "New Data Sheet", "", Key::DataSheet );
}

void ProjectModel::insertGraph()
{
	this->insertChild( "New Graph", "", Key::Graph );
}

void ProjectModel::insertImage()
{
	this->insertChild( "New Image", "", Key::Image );
}

void ProjectModel::removeFile()
{
	QModelIndexList selection = _selectionModel->selectedRows(0);

	for ( const QModelIndex& index : selection )
	{
		ProjectItem* item = projectItemAt(index);
		ProjectItem* parentItem = item->parent();
		if ( parentItem == _rootItem )
			continue;

		// Remove item
		int oldrow = index.row();
		beginRemoveRows(index.parent(), oldrow, oldrow);
		/*bool result = */parentItem->removeChild(oldrow);
		endRemoveRows();
	}
}

void ProjectModel::openFile()
{

}

void ProjectModel::saveFile()
{

}

void ProjectModel::closeFile()
{

}

void ProjectModel::importFile()
{

}

void ProjectModel::exportFile()
{

}

ProjectItem* ProjectModel::projectItemAt( const QModelIndex& index ) const
{
	if ( index.isValid() )
	{
		ProjectItem* item = static_cast<ProjectItem*>(index.internalPointer());
		if ( item != nullptr )
			return item;
	}
	return _rootItem;
}
