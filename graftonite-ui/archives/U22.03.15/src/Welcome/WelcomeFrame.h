#ifndef WELCOMEFRAME_H
#define WELCOMEFRAME_H

#include <QWidget>
#include <QGridLayout>
#include <QHBoxLayout>
#include "WelcomeModels.h"
#include "WelcomeWidgets.h"
// #include "../Commons/theme.h"
// #include "../Commons/lang.h"

/**********************************************/
/**********************************************/
/**********************************************/
/*
 *
 * */
class WelcomeFrame
		: public QWidget
{
	Q_OBJECT

public:
	explicit WelcomeFrame(QWidget *parent = nullptr);
	~WelcomeFrame();

public slots:
// 	void updateIcons( Icons* icons );
// 	void updateLang( Lang* lang );

private:
	QGridLayout* lay_main = new QGridLayout;
	// Title Bar
	QHBoxLayout* lay_title = new QHBoxLayout;
	WelcomeTitle* lab_projects = new WelcomeTitle;
	WelcomeButton* but_new = new WelcomeButton;
	WelcomeButton* but_open = new WelcomeButton;
	// Recent Projects
	WelcomeSubTitle* lab_recentProjects = new WelcomeSubTitle;
	WelcomeList* list_recentProjects = new WelcomeList;
	WelcomeModel *mod_recentProjects = new WelcomeModel;
	// Saved Projects
	WelcomeSubTitle* lab_savedProjects = new WelcomeSubTitle;
	WelcomeList* list_savedProjects = new WelcomeList;
	WelcomeModel *mod_savedProjects = new WelcomeModel;
	// Sessions
	WelcomeSubTitle* lab_sessions = new WelcomeSubTitle;
	WelcomeList* list_sessions = new WelcomeList;
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // WELCOMEFRAME_H
