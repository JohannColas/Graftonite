# Graftonite-UI


## Description

This program is dedicated to data treatment.

Written in C++17.


### Working features

    - None


## Requirements

    - CMake (Verison 3.5+)
    - gcc
    - Qt6 library


## License

CC0 ("No Rights Reserved" )


## Updates

### Update 2022.03.18

    - GraphItem Management improvements (create, remove, ...)
    - Graph implementation (add settings for each graph item, graph item drawing method implementation, ...)

### Update 2022.02.

    - Project Tree improvements (create new project, create new file)
    - SideBar improvements
    - Settings class implementation
    - Graph View improvements (scale graph by scrolling, movable graph, etc.)
    - Graph items creation
    
### Update 2022.02.09

    - Merging of two old branches
    - Change of ProjectTree (now it uses a model, ProjectTreeModel)
    - Add Key class for settings


## ToDo

    - Data Management
    - Settings Management
    - Project import
    - Project save & export
    - ...

### In Progress


