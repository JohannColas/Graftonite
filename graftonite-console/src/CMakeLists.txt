cmake_minimum_required ( VERSION 3.5 )

project ( graftonite-console VERSION 22.02.08 LANGUAGES CXX )

set ( CMAKE_CXX_STANDARD 17 )
set ( CMAKE_CXX_STANDARD_REQUIRED ON )

#configure_file("${PROJECT_SOURCE_DIR}/config.h.in")
add_compile_definitions(GRAFTONITE_VERSION="${PROJECT_VERSION}")

add_executable ( graftonite-console main.cpp
    Graftonite.h Graftonite.cpp
    Graph/Axis.h Graph/Axis.cpp
    Graph/Data.h Graph/Data.cpp
    Graph/Element.h Graph/Element.cpp
    Graph/Frame.h Graph/Frame.cpp
    Graph/Graph.h Graph/Graph.cpp
    Graph/Layer.h Graph/Layer.cpp
    Graph/Legend.h Graph/Legend.cpp
    Graph/Plot.h Graph/Plot.cpp
    Graph/Shape.h Graph/Shape.cpp
    Graph/Title.h Graph/Title.cpp
    Commons/Cairo.h Commons/Cairo.cpp
    Commons/Color.h Commons/Color.cpp
    Commons/File.h Commons/File.cpp
    Commons/Geometry.h Commons/Geometry.cpp
    Commons/Key.h Commons/Key.cpp
    Commons/Styles.h Commons/Styles.cpp
    Commons/System.h Commons/System.cpp
    Commons/UConsole.h Commons/UConsole.cpp
    Commons/UList.h
    Commons/UString.h Commons/UString.cpp
    )


message ( "${PROJECT_SOURCE_DIR}" )

include ( FindCairo )

if ( CAIRO_FOUND )
    include_directories(${CAIRO_INCLUDE_DIR})
    target_link_libraries(
	graftonite-console
	${CAIRO_LIBRARIES}
	)
    message( "Cairo library is found !" )
else ()
    message( "Cairo library is NOT found !" )
endif ()

include ( FindImageMagick )


install ( TARGETS graftonite-console DESTINATION /bin )


#file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/test.txt
#     DESTINATION ${CMAKE_CURRENT_BINARY_DIR})
#add_custom_command(
#    TARGET untitled POST_BUILD
#    COMMAND ${CMAKE_COMMAND} -E copy
#            ${CMAKE_SOURCE_DIR}/test.txt
#            ${CMAKE_CURRENT_BINARY_DIR}/test.txt)

