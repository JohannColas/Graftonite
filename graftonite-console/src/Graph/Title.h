#ifndef TITLE_H
#define TITLE_H
/**********************************************/
#include "Element.h"
/**********************************************/
/**********************************************/
/**********************************************/
class Title : public Element
{
	UList<UList<StyledText>> _text;
	TextStyle _style;
public:
	Title( Graph* parent = nullptr );
	Title( const ustring& name, unsigned index, Graph* parent = nullptr, bool log = false );
	void init();
	/**********************************************/
	bool set( const Key& key, const ustring& value ) override;
	void prepare( Cairo* drawing ) override;
	void draw( Cairo* drawing ) override;
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TITLE_H
