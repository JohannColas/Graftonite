#include "Legend.h"
/**********************************************/
#include "../Commons/Cairo.h"
/**********************************************/
#include "Graph.h"
/**********************************************/
/**********************************************/
Legend::Legend( Graph* parent )
	: Element(parent)
{
	init();
}
Legend::Legend( const ustring& name, unsigned index, Graph* parent, bool log )
	: Element(parent)
{
	init();
	Element::init( name, index, log );
}
void Legend::init()
{
	_type = Key::Legend;
	Element::set( Key::Layout, "vertical" );
	Element::set( Key::Entry, "is-\\#a:1; ttr=\\#a:0;\\@plot1; djsfhsd\\@0;j sd\n jdshfk jf hsf\n\\@0; \\#pl:0;" /*"t=eer - @{pl:0} - entry1\nt=@a:1;p=@0\np=@0;t=@a:1"*/ );
	// entry examples
	// p=@plot1;t=@a:1
	// t=@{An axis} - Stress:1;p=@plot1´
	// examples "p=@plot1" "p=@1"
	// examples "t=@p:plot1" "t=@a:{An axis} - Stress" "t=@axis:1"<=>"t=@a:1"
	// examples "t=A single plot"
	Element::set( Key::Alignment, "left" );
	Element::set( Key::Spacing, "10" );
	Element::set( Key(Key::Symbols, Key::Size), "50,20" );
	Element::set( Key(Key::Symbols, Key::Spacing), "5" );
	Element::set( Key(Key::Position), "\\@f:0;top" );
	Element::set( Key(Key::Anchor), "topleft" );
	Element::set( Key(Key::Font, Key::Size), "24" );
}
/**********************************************/
/**********************************************/
bool Legend::set( const Key& key, const ustring& value )
{
	if ( key == Key::Name ||
	     key == Key::Position ||
		 key == Key::Shift ||
	     key == Key::Anchor ||
		 key == Key::Template ||
	     key == Key::Layout ||
		 key == Key::Entry ||
		 key == Key::Scale ||
		 key == Key::Spacing ||
		 key == Key::Margins ||
		 key == Key(Key::Symbols, Key::Size) ||
		 key == Key(Key::Symbols, Key::Spacing) ||
	     key.isTextStyle() )
	{
		return Element::set( key, value );
	}
	else if ( key == Key::AddEntry )
	{
		ustring entries = get(Key::Entry);
		if ( !entries.empty() && entries.right(1) != "\n" )
			entries += "\n";
		entries += value;
	}
	else
		Graph::warningLog( "Unknown key \""+key.fullKeys()+"\" for legend element !" );
	return false;
}
/**********************************************/
/**********************************************/
void Legend::prepare( Cairo* drawing )
{
	Element::getTemplate();
	_all_entries.clear();
	ustring entries;
	get( entries, Key::Entry );
	if ( entries.empty() )
		return;
	//
	Key layout = Key::Vertical;
	get( layout, Key::Layout );
	double spacing = 5;
	get( spacing, Key::Spacing );
	//
	// Symbol Settings
	Size symbol_size = {50,20};
	get( symbol_size, Key::Symbols, Key::Size );
	double symbol_spacing = 5;
	get( symbol_spacing, Key::Symbols, Key::Spacing );
	//
	// Get Global TextStyle
	TextStyle global_style = getTextStyle(Key::NoKey);
	//
	// Building entries
	// Separate symbol ref & text
	// Acces to text ref
	for ( const ustring& entry : entries.split("\n") )
	{
		UList<StyledText> entry_parts;
		StyledText::buildStyledTextList( entry, global_style, entry_parts );
		for ( int it = 0; it < entry_parts.size(); ++it )
		{
			ustring text = entry_parts.at(0).text();
			if ( !text.contains("\\@") && !text.contains("\\#") )
				continue;
			TextStyle style = entry_parts.at(0).style();
			unsigned long index = text.find("\\#");
			while ( index != std::string::npos )
			{
				unsigned long end_index = text.find(";", index+1);
				ustring ref = text.substr( index, end_index-index+1 );
				Element* text_el = _parent->element( ref );
				if ( text_el != nullptr && text_el->type() == Key::Axis )
					ref = text_el->get(Key::Title, Key::Text);
				else if ( text_el != nullptr && text_el->type() == Key::Plot )
					ref = text_el->get(Key::Name);
				text = text.substr(0, index) +
					   ref +
					   text.substr(end_index+1);
				index = text.find("\\#");
			}
			if ( !text.contains("\\@") )
				entry_parts.replace( it, {text, style} );

			index = text.find("\\@");
			ustring tmp = text;
			while ( index != std::string::npos )
			{
				entry_parts.removeAt(it);
				UList<StyledText> divis;
				int end_index = tmp.find( ";", index+1 );
				ustring str = tmp.substr( 0, index );
				if ( !str.empty() )
					divis.append( {str, style} );
				str = tmp.substr( index, end_index-index+1 );
				if ( !str.empty() )
					divis.append( {str, style} );
				str = tmp.substr( end_index+1 );
				if ( !str.empty() )
					divis.append( {str, style} );
				entry_parts.insert( it, divis );
				it += divis.size()-1;
				tmp = str;
				index = tmp.find("\\@");
			}
		}
		_all_entries.append(entry_parts);
	}
	//
	double nx = 0, ny = 0;
	bool boundingRectInit = false;
	//
	// Building Entry Geometry
	// Except for entry part position
	double w_legend = 0, h_legend = 0;
	for ( int et = 0; et < _all_entries.size(); ++et )
	{
		UList<StyledText> entry = _all_entries.at(et);
		double w_entry = 0, h_entry = 0;
		for ( int it = 0; it < entry.size(); ++it )
		{
			StyledText part = entry.at(it);
			Rect rect;
			if ( part.text().substr(0,2) == "\\@" )
			{
				rect = {{nx, ny}, symbol_size};
				nx += symbol_spacing;
				w_entry += symbol_size.width()+symbol_spacing;
				if ( h_entry < symbol_size.height() )
					h_entry = symbol_size.height();
			}
			else
			{
				part.setPos( Point(nx, ny) );
				part.prepare( drawing );
				rect = part.boundingRect();
				w_entry += part.boundingRect().width();
				if ( h_entry < part.boundingRect().height() )
					h_entry = part.boundingRect().height();
			}
			nx += rect.width();
			if ( boundingRectInit )
				_boundingRect.expand(rect);
			else
			{
				_boundingRect = rect;
				boundingRectInit = true;
			}
			entry.replace( it, part );
		}
		_entries_sizes.append( Size(w_entry, h_entry) );
		_all_entries.replace( et, entry );
		if ( layout == Key::Horizontal )
		{
			nx = _boundingRect.width() + spacing;
			w_legend += w_entry;
			if ( et+1 != _all_entries.size() )
				w_legend += spacing;
			if ( h_legend < h_entry )
				h_legend = h_entry;
		}
		else if ( layout == Key::Vertical )
		{
			ny = _boundingRect.height();
			if ( et+1 != _all_entries.size() )
				ny += spacing;
			h_legend += h_entry;
			if ( et+1 != _all_entries.size() )
				h_legend += spacing;

			if ( w_legend < w_entry )
				w_legend = w_entry;
			nx = 0;
		}
	}
	//
	// Set legend size
	Point margins = {10,10};
	get( margins, Key::Margins );
	_boundingRect = {0, 0, w_legend+2*margins.x(), h_legend+2*margins.y() };
	// Pos
	Point pos = {180,120};
	getPosition( pos );
	// Diff
	Key anchor = Key::TopLeft;
	get( anchor, Key::Anchor );
	Point diff = _boundingRect.point(anchor);
	// Shift
	Point shift = {0,0};
	get( shift, Key::Shift );
	pos -= diff - shift;
	// Set legend position
	_boundingRect.setPoint( pos );
	//
}
/**********************************************/
/**********************************************/
void Legend::draw( Cairo* drawing )
{
	if ( _all_entries.isEmpty() )
		return;
	//
	Key layout = Key::Vertical;
	get( layout, Key::Layout );
	//
	// Geometry Settings

	//
	Key alignment = Key::Left;
	get( alignment, Key::Alignment );
	double spacing = 5;
	get( spacing, Key::Spacing );
	Point margins = {10,10};
	get( margins, Key::Margins );
	double scale = 1;
	get( scale, Key::Scale );
	//
	// Get Final Geometry
//	Rect rect = _legend_rect;
//	rect.setWidth( rect.width() + 2*margins.x() );
//	rect.setHeight( rect.height() + 2*margins.y() );

//	Key anchor = Key::TopLeft;
//	get( anchor, Key::Anchor );
//	Point diff = rect.point(anchor);
//	Point translate = _legend_rect.point();
//	if ( pos.substr(0,2) == "\\@" )
//	{
//		UList<ustring> tmp = pos.split(";");
//		if ( tmp.size() > 1 && _parent != nullptr )
//		{
//			Element* el = _parent->element( tmp.at(0) );
//			if ( el != nullptr )
//				translate = el->boundingRect().point(Key::toKeys(tmp.at(1)));
//		}
//	}
//	else
//		translate.set( pos );
//	Point shift = {0,0};
//	get( shift, Key::Shift );
//	translate.setX( translate.x() - diff.x() + shift.x() );
//	translate.setY( translate.y() - diff.y() + shift.y() );
	//
	// Symbol Settings
	Size symbol_size = {50,20};
	get( symbol_size, Key::Symbols, Key::Size );
	double symbol_spacing = 5;
	get( symbol_spacing, Key::Symbols, Key::Spacing );
	//
	// Begin Drawing
	drawing->saveState();
	drawing->moveTo( _boundingRect.origin() );
	drawing->scale( scale );
	//
	double nx = margins.x(), ny = margins.y();
	for ( int it = 0; it < _all_entries.size(); ++it )
	{
		UList<StyledText> entry = _all_entries.at(it);
		Rect entry_rect = { {nx,ny}, _entries_sizes.at(it) };
		if ( layout == Key::Vertical )
		{
			if ( alignment == Key::Center )
				nx += 0.5*( _boundingRect.width() - entry_rect.width() );
			else if ( alignment == Key::Right )
				nx += _boundingRect.width() - entry_rect.width();
		}
		for ( StyledText part : entry )
		{
			if ( part.text().substr(0,2) == "\\@" )
			{
				Plot* plot = _parent->plot(part.text().remove("\\@").remove(";"));
				if ( plot != nullptr )
				{
					double gap = plot->line_gap;
					LineStyle style = plot->line_style;
					double left = nx,
							xcenter = nx + 0.5*symbol_size.width(),
							right = nx + symbol_size.width();
					if ( gap > 0 )
					{
						drawing->drawLine( {left,entry_rect.yCenter()}, {xcenter-gap, entry_rect.yCenter()}, style );
						drawing->drawLine( {xcenter+gap,  entry_rect.yCenter()}, {right,entry_rect.yCenter()}, style );
					}
					else
						drawing->drawLine( {left,entry_rect.yCenter()}, {right,entry_rect.yCenter()}, style );
					Symbol symbol = plot->symbol;
					ShapeStyle symbol_style = plot->symbol_style;
					drawing->drawSymbols( symbol, {xcenter,entry_rect.yCenter()}, symbol.size(), symbol_style );
				}
				else
				{
					drawing->drawText( part );
				}
				nx += symbol_size.width()+symbol_spacing;
			}
			else
			{
				part.setPos( {nx, entry_rect.yCenter()} );
				part.setAnchor( Key::Left );
				nx += drawing->drawText( part ).width();
			}
		}
		// -------------------------
		if ( layout == Key::Horizontal )
		{
			nx += entry_rect.width();
			if ( it+1 != _all_entries.size() )
				nx += spacing;
		}
		else if ( layout == Key::Vertical )
		{
			ny += entry_rect.height();
			if ( it+1 != _all_entries.size() )
				ny += spacing;
			nx = margins.x();
		}
		else if ( layout == Key::Table )
		{
			ny += entry_rect.height();
			if ( it+1 != _all_entries.size() )
				ny += spacing;
			nx = margins.x();
		}
		// -------------------------
	}
	//
	// End Drawing
	drawing->restoreState();

	_boundingRect = drawing->getTransformRect( _boundingRect );
	//
	// TEST
	LineStyle styl;
	styl.setColor( Color("green8") );
	styl.setWidth( 0.5 );
	ShapeStyle style;
	style.setLine( styl );
	drawing->drawRect( _boundingRect, style );
	// TEST END
}
/**********************************************/
/**********************************************/
