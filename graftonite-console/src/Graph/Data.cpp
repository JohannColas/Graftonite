#include "Data.h"
/**********************************************/
#include "../Commons/File.h"
/**********************************************/
#include "Graph.h"
/**********************************************/
/**********************************************/
Data::~Data()
{

}
/**********************************************/
/**********************************************/
Data::Data( Graph* parent )
	: Element(parent)
{
	init();
}
/**********************************************/
Data::Data( const ustring& name, unsigned index, Graph* parent, bool log )
	: Element(parent)
{
	init();
	Element::init( name, index, log );
}
/**********************************************/
void Data::init()
{
	_type = Key::Data;
//	set( Key::Separator, " " );
}
/**********************************************/
/**********************************************/
bool Data::set( const Key& key, const ustring& value )
{
	if ( key == Key::Name )
		return Element::set( key, value );
	else if ( key == Key::Path ||
			  key == Key::Separator  )
	{
		Element::unset( Key::Coordinates );
		return Element::set( key, value );
		initData();
		return true;
	}
	else if ( key == Key::Coordinates )
	{
		Element::unset( Key::Path );
		Element::unset( Key::Separator );
		return Element::set( key, value );
		initData();
		return true;
	}
	else
		Graph::warningLog( "Unknown key \""+key.fullKeys()+"\" for data element !" );
	return false;
}
/**********************************************/
/**********************************************/
UList<UList<ustring>> Data::data() const
{
	return _data;
}
/**********************************************/
void Data::initData()
{
	if ( hasKey(Key::Path) )
	{
		ustring path = get(Key::Path);
		ustring sep = ";";
		get( sep, Key::Separator );
		UList<ustring> data_str;
		if ( File::read( path, data_str ) )
			for ( const ustring& row : data_str )
				_data.append( row.split(sep) );
	}
	else if ( hasKey(Key::Coordinates) )
	{
		_data.clear();
		ustring coordinates = get(Key(Key::Coordinates));
		for ( const ustring& row : coordinates.split("/") )
			_data.append( row.split(",") );
	}
}
/**********************************************/
UList<ustring> Data::getData( const ustring& index )
{
	UList<ustring> data;
	if ( index.size() > 1 )
	{
		ustring type = index.at(0);
		unsigned long ind = -1;
		if ( index.remove(type).isInteger() )
			ind = index.remove(type).toULong();
		if ( type == "c" )
			data = getColumn( ind );
		else if ( type == "r" )
			data = getRow( ind );
	}
	return data;
}
UList<ustring> Data::getColumn( int index )
{
	UList<ustring> data;
	if ( index >= 0 )
		for ( int it = 0; it < _data.size(); ++it )
			if ( index < _data.at(it).size() )
				data.append( _data.at(it).at(index) );
	return data;
}
UList<ustring> Data::getRow( int index )
{
	if ( 0 <= index && index < _data.size() )
		return _data.at(index);
	UList<ustring> data;
	return data;
}
/**********************************************/
/**********************************************/
