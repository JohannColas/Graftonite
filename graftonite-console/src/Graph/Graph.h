#ifndef GRAPH_H
#define GRAPH_H
/**********************************************/
#include "Element.h"
#include "Layer.h"
#include "Frame.h"
#include "Axis.h"
#include "Data.h"
#include "Plot.h"
#include "Legend.h"
#include "Title.h"
#include "Shape.h"
#include "../Commons/Cairo.h"
#include "../Commons/File.h"
/**********************************************/
/**********************************************/
/**********************************************/
class Graph : public Element
{
	ustring name = "";
	Key _format = Key::PNG;
	double width = 1200;
	double height = 800;
	/**********************************************/
	Element* _currentEl = 0;
	/**********************************************/
	UList<Element*> _templates;
	UList<Layer*> _layers;
	UList<Frame*> _frames;
	UList<Axis*> _axes;
	UList<Data*> _data;
	UList<Plot*> _plots;
	UList<Legend*> _legends;
	UList<Title*> _titles;
	UList<Shape*> _shapes;
	/**********************************************/
	bool _log = true;
	static inline UList<ustring> _log_contents = {};
	/**********************************************/
public:
	~Graph();
	Graph();
	/**********************************************/
	/**********************************************/
	/* Element Management */
	Element* element( const Key::Keys& type, const ustring& name );
	Element* element( const ustring& ref );
	Element* element( UList<ustring>& args );
	UList<Element*> elements( const Key::Keys& type );
	void newElement( const Key::Keys& type = Key::NoType, const ustring& name = ustring(), bool log = false );
	void removeElement( const Key::Keys& type = Key::NoType, const ustring& name = ustring() );
	void setCurrent( UList<ustring>& args );
	Element* templateAt( const ustring& name );
	UList<Element*> templates();
	void removeTemplate( Element* templat );
	void clearTemplates();
	Layer* layer( const ustring& name );
	UList<Layer*> layers();
	void removeLayer( Layer* layer );
	void clearLayers();
	Frame* frame( const ustring& name );
	UList<Frame*> frames();
	void removeFrame( Frame* frame );
	void clearFrames();
	Axis* axis( const ustring& name );
	UList<Axis*> axes();
	void removeAxis( Axis* axis );
	void clearAxes();
	Data* data( const ustring& name );
	UList<Data*> datalst();
	void removeData( Data* data );
	void clearData();
	Plot* plot( const ustring& name );
	UList<Plot*> plots();
	void removePlot( Plot* plot );
	void clearPlots();
	Legend* legend( const ustring& name );
	UList<Legend*> legends();
	void removeLegend( Legend* legend );
	void clearLegends();
	Title* title( const ustring& name );
	UList<Title*> titles();
	void removeTitle( Title* title );
	void clearTitles();
	Shape* shape( const ustring& name );
	UList<Shape*> shapes();
	void removeShape( Shape* shape );
	void clearShapes();
	/**********************************************/
	/* Modifiers */
	bool set( const Key& key, const ustring& value ) override;
	bool set( UList<ustring>& args, bool log = false ) override;
	bool reset( UList<ustring>& args, bool log = false ) override;
	void unset( const UList<ustring>& args, bool log = false ) override;
	/**********************************************/
	/* Displays */
	void showAll( const Key::Keys& type = Key::NoType, const ustring& name = ustring() );
	void showCurrent();
	/**********************************************/
	/* Drawing functions */
	void prepare( Cairo* drawing ) override;
	void prepare( Element* el, Cairo* drawing );
	void draw( Cairo* drawing ) override;
	void exportGraph( UList<ustring>& args );
	/**********************************************/
	/* Log functions */
	static void addLogLine( const ustring& line );
	static void clearLog();
	static void saveLog( const ustring& path );
	static void normalLog( const ustring& message );
	static void errorLog( const ustring& message );
	static void warningLog( const ustring& message );
	/**********************************************/
	ustring _exportpath = "";
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // GRAPH_H
