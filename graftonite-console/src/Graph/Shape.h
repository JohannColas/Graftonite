#ifndef SHAPE_H
#define SHAPE_H
/**********************************************/
#include "Element.h"
/**********************************************/
/**********************************************/
/**********************************************/
class Shape
        : public Element
{
public:
	~Shape();
	Shape( Graph* parent = nullptr );
	Shape( const ustring& name, unsigned index, Graph* parent = nullptr, bool log = false );
	void init();
	/**********************************************/
	bool set( const Key& key, const ustring& value ) override;
	/**********************************************/
	void prepare( Cairo* drawing ) override;
	void draw( Cairo* drawing ) override;
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SHAPE_H
