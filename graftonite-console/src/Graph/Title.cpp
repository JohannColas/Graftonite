#include "Title.h"
/**********************************************/
#include "Graph.h"
/**********************************************/
/**********************************************/
Title::Title( Graph* parent )
	: Element(parent)
{
	init();
}
Title::Title(const ustring& name, unsigned index, Graph* parent, bool log)
	: Element(parent)
{
	init();
	Element::init( name, index, log );
}
void Title::init()
{
	_type = Key::Title;
}
/**********************************************/
/**********************************************/
bool Title::set( const Key& key, const ustring& value )
{
	if ( key == Key::Name ||
		 key == Key::Position ||
	     key == Key::Anchor ||
	     key == Key::Shift ||
	     key == Key::Margins ||
	     key == Key::Template ||
		 key == Key::Text ||
	     key.isTextStyle() ||
		 key.isFillStyle(Key::Background) ||
		 key.isLineStyle({Key::Borders}, false) )
	{
		return Element::set( key, value );
	}
	else
		Graph::warningLog( "Unknown key \""+key.fullKeys()+"\" for title element !" );
	return false;
}
/**********************************************/
/**********************************************/
void Title::prepare( Cairo* drawing )
{
	Element::getTemplate();
	//
	// Building Entry Geometry
	// Except for entry part position

	_style = getTextStyle( Key::NoKey );
	ustring text;
	get( text, Key::Text );
	_text.clear();
	StyledText::buildStyledTextList( text, _style, _text );


	Rect rect = drawing->textBoundingRect( _text, {0,0}, _style );

	//
	// Set Title size
	Point margins = {5,5};
	get( margins, Key::Margins );
	_boundingRect = {0, 0, rect.width()+2*margins.x(), rect.height()+2*margins.y() };
	// Pos
	Point pos = {0,0};
	getPosition( pos );
	// Diff
	Key anchor = Key::TopLeft;
	get( anchor, Key::Anchor );
	Point diff = _boundingRect.point(anchor);
	// Shift
	Point shift = {0,0};
	get( shift, Key::Shift );
	pos -= diff - shift;
	// Set Title position
	_boundingRect.setPoint( pos );
	//
}
/**********************************************/
/**********************************************/
void Title::draw( Cairo* drawing )
{
	Point margins = {5,5};
	get( margins, Key::Margins );
	//
	// styles
	FillStyle background_style = getFillStyle({Key::Background});
	LineStyle borders_style = getLineStyle({Key::Borders});
	ShapeStyle rect_style;
	rect_style.setFill( background_style );
	rect_style.setLine( borders_style );
	//
	// draw
	drawing->drawRect( _boundingRect, rect_style );
	_style.setAnchor( Key::Center );
	drawing->drawText( _text, _boundingRect.center(), _style );
}
/**********************************************/
/**********************************************/
