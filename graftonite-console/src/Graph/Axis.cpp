#include "Axis.h"
/**********************************************/
#include <cmath>
/**********************************************/
#include "../Commons/Cairo.h"
/**********************************************/
#include "Graph.h"
#include "Layer.h"
/**********************************************/
/**********************************************/
Axis::~Axis()
{
}
Axis::Axis( Graph* parent )
    : Element(parent)
{
	init();
}
Axis::Axis( const ustring& name, unsigned index, Graph* parent, bool log )
    : Element(parent)
{
	init();
	Element::init( name, index, log );
}
void Axis::init()
{
	_type = Key::Axis;
	//	_boundingRect = {100, 100, 1200, 800};
	// Default settings
	Element::set( Key::Min, "0" );
	Element::set( Key::Max, "1" );
	Element::set( Key::Type, "X" );
	Element::set( Key::Scale, "linear" );
	Element::set( Key::Position, "bottom" );
	Element::set( Key(Key::Line, Key::Width), "3" );
	Element::set( Key(Key::Line, Key::Cap), "square" );
	Element::set( Key(Key::Ticks, Key::Position), "outside" );
	Element::set( Key(Key::Ticks, Key::Increment), "0.1" );
	Element::set( Key(Key::Ticks, Key::Size), "10" );
	Element::set( Key(Key::Minor, Key::Ticks, Key::Increment), "0.05" );
	Element::set( Key(Key::Minor, Key::Ticks, Key::Size), "5" );
	Element::set( Key(Key::Labels, Key::Position), "outside" );
	Element::set( Key(Key::Labels, Key::Font, Key::Size), "24" );
	Element::set( Key(Key::Title, Key::Position), "outside" );
	Element::set( Key(Key::Title, Key::Text), "Title" );
	Element::set( Key(Key::Title, Key::Font, Key::Size), "30" );
	Element::set( Key(Key::Title, Key::Anchor), "30" );
}
/**********************************************/
/**********************************************/
bool Axis::set( const Key& key, const ustring& value )
{
	if ( key == Key::Name ||
	     key == Key::Template ||
	     key == Key::Layer ||
	     key == Key::Frame ||
	     key == Key::Hide ||
	     key == Key::Min ||
	     key == Key::Max ||
	     key == Key::Scale ||
	     key == Key::LinkTo ||
	     key == Key::Line ||
	     key.isLineStyle({Key::Line}, true, false) ||
	     key == Key(Key::Ticks, Key::Position) ||
	     key == Key(Key::Ticks, Key::Increment) ||
	     key == Key(Key::Ticks, Key::Numbers) ||
	     key == Key(Key::Ticks, Key::Size) ||
	     key == Key(Key::Minor, Key::Ticks, Key::Increment) ||
	     key == Key(Key::Minor, Key::Ticks, Key::Numbers) ||
	     key == Key(Key::Minor, Key::Ticks, Key::Size) ||
	     key == Key(Key::Labels, Key::Position) ||
	     key.isTextStyle(Key::Labels) ||
	     key == Key(Key::Title, Key::Position) ||
	     key == Key(Key::Title, Key::Text) ||
	     key == Key(Key::Title, Key::Rotation) ||
	     key.isTextStyle(Key::Title) )
	{
		return Element::set( key, value );
	}
	else if ( key == Key::Type )
	{
		if ( value == "X" )
		{
			Element::set( Key::Position, Key::toString(Key::Bottom) );
			Element::set( Key(Key::Labels, Key::Font, Key::Anchor), Key::toString(Key::Top) );
			Element::set( Key(Key::Title, Key::Font, Key::Anchor), Key::toString(Key::Top) );
			Element::unset( {Key::Title, Key::Rotation} );
		}
		else if ( value == "Y" )
		{
			Element::set( Key::Position, Key::toString(Key::Left) );
			Element::set( Key(Key::Labels, Key::Font, Key::Anchor), Key::toString(Key::Right) );
			Element::set( Key(Key::Title, Key::Font, Key::Anchor), Key::toString(Key::Bottom) );
			Element::set( Key(Key::Title, Key::Rotation), "90" );
		}
		if ( value != get(Key::Type) )
		{
			Element::set( Key(Key::Ticks, Key::Position), Key::toString(Key::Outside) );
			Element::set( Key(Key::Labels, Key::Position), Key::toString(Key::Outside) );
			Element::set( Key(Key::Title, Key::Position), Key::toString(Key::Outside) );
		}
		return Element::set( key, value );
	}
	else if ( key == Key::Position )
	{
		ustring type;
		get( type, Key::Type );
		Key key = value;
		if ( (type == "X" && key != Key::Left && key != Key::Right) ||
		     (type == "Y" && key != Key::Top && key != Key::Bottom) )
			return Element::set( key, value );
		return false;
	}
	else if ( key == Key::Ticks )
	{

	}
	else if ( key == Key::Labels )
	{

	}
	else if ( key == Key::Title )
	{

	}
	else
		Graph::warningLog( "Unknown key \""+key.fullKeys()+"\" for axis element !" );
	return false;
}
/**********************************************/
/**********************************************/
void Axis::prepare( Cairo* drawing )
{
	Element::getTemplate();
	Frame* frame = _parent->frame( get(Key::Frame) );
	if ( frame )
		_frameRect = frame->boundingRect();
	//
	// Building Entry Geometry
	// Except for entry part position
	// -----------------------
	_min = 0;
	get( _min, Key::Min );
	_max = 0;
	get( _max, Key::Max );
	//
	// Axis Settings
	ustring type = "X";
	get( type, Key::Type );
	Key axis_pos = Key::Bottom;
	get( axis_pos, Key::Position );
	_line = _frameRect.line( axis_pos );
	_boundingRect = _line;
	_line_style = getLineStyle();
	// -----------------------
	//
	// Ticks Settings
	Key tick_pos = Key::Outside;
	get( tick_pos, Key::Ticks, Key::Position );
	double tick_size = 10;
	get( tick_size, Key::Ticks, Key::Size );
	double minor_tick_size = 5;
	get( minor_tick_size, Key::Minor, Key::Ticks, Key::Size );
	//
	Line::Orientation tick_orientation = Line::Vertical;
	if ( type == "Y" )
		tick_orientation = Line::Horizontal;
	//
	double tick_shift = 0;
	if ( tick_pos == Key::Top ||
	     (tick_pos == Key::Outside && axis_pos == Key::Top) ||
	     (tick_pos == Key::Inside && axis_pos == Key::Bottom) ||
	     tick_pos == Key::Left ||
	     (tick_pos == Key::Outside && axis_pos == Key::Left) ||
	     (tick_pos == Key::Inside && axis_pos == Key::Right) )
	{
		tick_shift = -1;
	}
	else if ( tick_pos == Key::Center )
	{
		tick_shift = -0.5;
	}
	//
	CalculateScaleCoef();
	UList<Point> ticks_pos, minor_ticks_pos; UList<ustring> labels;
	CalculateTicksPosition( ticks_pos, minor_ticks_pos, labels );
	_ticks.clear(), _minor_ticks.clear(), _labels.clear();
	//
	for ( const Point& pos : ticks_pos )
	{
		Line tick = Line( pos, tick_shift*tick_size, tick_size, tick_orientation );
		_ticks.append( tick );
		_boundingRect.expand( tick );
	}
	for ( const Point& pos : minor_ticks_pos )
	{
		Line minor_tick = Line( pos, tick_shift*minor_tick_size, minor_tick_size, tick_orientation );
		_minor_ticks.append( minor_tick );
		_boundingRect.expand( minor_tick );
	}
	// -----------------------
	//
	// Labels Settings
	Key labels_pos = Key::Outside;
	get( labels_pos, Key::Labels, Key::Position );
	Point labels_shift = {0,0};
	get( labels_shift, Key::Labels, Key::Shift );
	Key labels_anchor = Key::Top;
	get( labels_anchor, Key::Labels, Key::Anchor );
	TextStyle labels_style = getTextStyle( Key::Labels );
	//
	if ( (labels_pos == Key::Outside && axis_pos == Key::Bottom) ||
	     (labels_pos == Key::Inside && axis_pos == Key::Top) )
		labels_pos = Key::Bottom;
	else if ( (labels_pos == Key::Outside && axis_pos == Key::Top) ||
	          (labels_pos == Key::Inside && axis_pos == Key::Bottom) )
		labels_pos = Key::Top;
	else if ( (labels_pos == Key::Outside && axis_pos == Key::Left) ||
	     (labels_pos == Key::Inside && axis_pos == Key::Right) )
		labels_pos = Key::Left;
	else if ( (labels_pos == Key::Outside && axis_pos == Key::Right) ||
	          (labels_pos == Key::Inside && axis_pos == Key::Left) )
		labels_pos = Key::Right;
	//
	for ( int it = 0; it < labels.size(); ++it )
	{
		Rect tick_rect = _ticks.at(it);
		Point label_pos = tick_rect.point(labels_pos);
		StyledText label( labels.at(it), labels_style );
		label.setPos( label_pos );
		_labels.append( label );
		_boundingRect.expand( drawing->boundingRect(label.text(), label_pos, labels_style ) );
	}
	LineStyle line_style;
	line_style.setColor( Color("black") );
	ShapeStyle shape_style;
	shape_style.setLine( line_style );
	drawing->drawRect( _boundingRect, shape_style );
	// -----------------------
	//
	// Title Settings
	Key title_pos = Key::Outside;
	get( title_pos, Key::Title, Key::Position );
	ustring title = "Title";
	get( title, Key::Title, Key::Text );
	Point title_shift = {0,0};
	get( title_shift, Key::Title, Key::Shift );
	TextStyle title_style = getTextStyle( Key::Title );
	//
	if ( (title_pos == Key::Outside && axis_pos == Key::Bottom) ||
	     (title_pos == Key::Inside && axis_pos == Key::Top) )
		title_pos = Key::Bottom;
	else if ( (title_pos == Key::Outside && axis_pos == Key::Top) ||
	          (title_pos == Key::Inside && axis_pos == Key::Bottom) )
		title_pos = Key::Top;
	else if ( (title_pos == Key::Outside && axis_pos == Key::Left) ||
	     (title_pos == Key::Inside && axis_pos == Key::Right) )
		title_pos = Key::Left;
	else if ( (title_pos == Key::Outside && axis_pos == Key::Right) ||
	          (title_pos == Key::Inside && axis_pos == Key::Left) )
		title_pos = Key::Right;
	//
	Point title_position = _boundingRect.point(title_pos) - title_shift;
//	FillStyle fill;
//	fill.setColor( Color("black") );
//	ShapeStyle sha; sha.setFill( fill );
//	drawing->drawCercle( {title_position}, 5, sha );
	_title = StyledText( title, title_style );
	_title.setPos(title_position);
	_boundingRect.expand( drawing->boundingRect(title, title_position, title_style) );
	drawing->drawCercle( {title_position}, 5 );
	// -----------------------
	//
	// Grids Settings

}
/**********************************************/
/**********************************************/
void Axis::draw( Cairo* drawing )
{

	for ( Line grid : _minor_grids )
		drawing->drawLine( grid, _minor_grids_style );
	for ( Line grid : _grids )
		drawing->drawLine( grid, _grids_style );

	drawing->drawLine( _line, _line_style );

	for ( Line tick : _minor_ticks )
		drawing->drawLine( tick, _line_style );
	for ( Line tick : _ticks )
		drawing->drawLine( tick, _line_style );

	for ( StyledText label : _labels )
		drawing->drawText( label.text(), label.pos(), label.style() );

	drawing->drawText( _title.text(), _title.pos(), _title.style() );


	// TEST
	LineStyle styl;
	styl.setColor( Color("red6") );
	styl.setWidth( 0.5 );
	ShapeStyle style;
	style.setLine( styl );
	drawing->drawRect( _boundingRect, style );
	// TEST END
}
/*------------------------------*/
/*------------------------------*/
void Axis::CalculateTicksPosition( UList<Point>& ticks_pos, UList<Point>& minor_ticks_pos, UList<ustring>& labels )
{
	double Drv = std::abs(_max-_min);
	// Get Ticks number and increment
	int tick_number = 10;
	double tick_increment = Drv/tick_number;
	if ( get(tick_increment, Key::Ticks, Key::Increment) )
		tick_number = Drv/tick_increment;
	else if ( get(tick_number, Key::Ticks, Key::Numbers) )
		tick_increment = Drv/tick_number;
	// Get Minor Ticks number and increment
	int minor_tick_number = 20;
	double minor_tick_increment = Drv/minor_tick_number;
	if ( get(minor_tick_increment, Key::Minor, Key::Ticks, Key::Increment) )
		minor_tick_number = Drv/minor_tick_increment;
	else if ( get(minor_tick_number, Key::Minor, Key::Ticks, Key::Numbers) )
	{
		minor_tick_increment = tick_increment/minor_tick_number;
		minor_tick_number = Drv/minor_tick_increment;
	}

	// TEST of a new code -> Not Working for now
//	int minor_tick_number_between_two_tick = std::round(tick_increment/minor_tick_increment)-1;
//	std::cout << tick_increment << " - " << tick_number << std::endl;
//	std::cout << minor_tick_increment << " - " << minor_tick_number << std::endl;
//	std::cout << "number of minor tick between two ticks : " << minor_tick_number_between_two_tick << std::endl;
//	int tick_multiplicator = _min/tick_increment;
//	int minor_tick_multiplicator = _min/minor_tick_increment;
//	char rv_dir = 1; if ( _min > _max ) rv_dir = -1;
//	for ( int it_mtk = 0; it_mtk <= minor_tick_number; ++it_mtk )
//	{
//		bool isMajorTick = false;
//		if ( tick_multiplicator*(minor_tick_number_between_two_tick+1) == minor_tick_multiplicator )
//		{
//			isMajorTick = true;
//			++tick_multiplicator;
//		}

//		double real_value = rv_dir*minor_tick_multiplicator*minor_tick_increment;
//		Point tick_pos = this->deviceCoordinates( real_value );
//		if ( isMajorTick )
//		{
//			ticks_pos.append( tick_pos );
//			labels.append( ustring(real_value) );
//		}
//		else
//		{
//			minor_ticks_pos.append( tick_pos );
//		}
//		++minor_tick_multiplicator;
//	}
	// END TEST


	char axis_dir = 1; if ( _min > _max ) axis_dir = -1;

	int quot = (int)(_min/tick_increment);
	if ( _min > 0 && _max > _min ) ++quot;
	else if ( _min < 0 && _max < _min ) --quot;

	int minorquot = (int)(_min/minor_tick_increment);
	if ( _min > 0 && _max > _min ) ++minorquot;
	else if ( _min < 0 && _max < _min ) --minorquot;

	double posp = quot*tick_increment;
	Point pos = this->deviceCoordinates( posp );
	Rect first_tick_rect = {pos, {20,20} };

	int it = 0;
	double minorposp = minorquot*minor_tick_increment;
	Point minorpos = this->deviceCoordinates( minorposp );
	while ( !first_tick_rect.contains(minorpos) && it<100 )
	{
		minor_ticks_pos.append( minorpos );
		minorposp += axis_dir*minor_tick_increment;
		minorpos = this->deviceCoordinates( minorposp );
		++it;
	}
	it = 0;
	while ( _frameRect.contains(pos) && it<100 )
	{
		ticks_pos.append( pos );
		// -----------
		// Get Labels
		labels.append( ustring(posp) );
		// -----------
		int jt = 0;
		double minorposp = minor_tick_increment;
		Point minorpos = this->deviceCoordinates( posp + axis_dir*minorposp );
		while ( minorposp < tick_increment && _frameRect.contains(minorpos) && jt<100 )
		{
			minor_ticks_pos.append( minorpos );
			minorposp += minor_tick_increment;
			minorpos = this->deviceCoordinates( posp + axis_dir*minorposp );
			++jt;
		}
		posp += axis_dir*tick_increment;
		pos = this->deviceCoordinates( posp );
		++it;
	}
}
/*------------------------------*/
/*------------------------------*/
double Axis::round( double value )
{
	double val = value;
	return val;
}
/*------------------------------*/
/*------------------------------*/
void Axis::CalculateScaleCoef()
{
	ustring type = get( Key::Type );
	ustring scale = get( Key::Scale );
	double scaleOpt = get( Key(Key::Scale, Key::Option) ).toDouble();

	// New Code
	// _min & _max checkers
	if ( scale == "log10" ||
	     scale == "log" ||
	     scale == "logX" )
	{
		if ( _min <= 0 )
			_min = 0.1;
		if ( _max <= 0 )
			_max = 1;
	}
	else if ( scale == "reciprocal" )
	{
		if ( _min == 0 )
			_min = 1;
		if ( _max == 0 )
			_max = 10;
	}
	else if ( scale == "offsetreciprocal" )
	{
		if ( _min+scaleOpt == 0 )
			_min = 1-scaleOpt;
		if ( _max+scaleOpt == 0 )
			_max = 10-scaleOpt;
	}
	if ( _min == _max )
		_max = _min+1;
	double sc_min = this->scale(_min);
	double sc_max = this->scale(_max);
	double Dx = _line.x2() - _line.x1();
	double Dy = _line.y1() - _line.y2();
	double Drv = sc_max - sc_min;
	a_x = Dx/Drv;
	b_x = _line.x1() - a_x*sc_min;
	a_y = Dy/Drv;
	b_y = _line.y2() - a_y*sc_min;
	//
}
/*------------------------------*/
/*------------------------------*/
double Axis::GetGraphCoord( double coord )
{
	double sc_rv = this->scale(coord);
	double x = a_x*sc_rv + b_x;
	double y = a_y*sc_rv + b_y;
	ustring type = "X";
	get( type, Key::Type );
	if ( type == "Y" )
		return y;
	else
		return x;
}
Point Axis::deviceCoordinates( double real_value )
{
	double sc_rv = this->scale(real_value);
	double x = a_x*sc_rv + b_x;
	double y = a_y*sc_rv + b_y;
	return Point(x,y);
}
/*------------------------------*/
/*------------------------------*/
Rect Axis::frameRect() const
{
	return _frameRect;
}
/**********************************************/
/**********************************************/
