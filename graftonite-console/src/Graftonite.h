#ifndef GRAFTONITE_H
#define GRAFTONITE_H
/**********************************************/
#include "Graph/Graph.h"
/**********************************************/
/**********************************************/
/**********************************************/
class Graftonite
{
	std::map<Key,ustring> _settings;
	Graph _graph = Graph();
	bool _log = false;
public:
	Graftonite( int argc, char *argv[] );
	int exec();
	/**********************************************/
	std::map<Key,ustring> settings() const;
	ustring setting( const Key& key ) const;
	ustring setting( const Key::Keys& key, const Key::Keys& key2 = Key::NoKey, const Key::Keys& key3 = Key::NoKey ) const;
	void addSettings( const ustring& set, const Key& key );
	void loadSettings();
	void saveSettings();
	/**********************************************/
	void decomposeCommandLine( const ustring& cmdline, ustring& cmd, UList<ustring>& args );
	bool runCommand( const ustring& cmdline );
	/**********************************************/
	void newElement( UList<ustring>& args );
	void reorder( UList<ustring>& args );
	void insert( UList<ustring>& args );
	void deleteElement( UList<ustring>& args );
	void setCurrentElement( UList<ustring>& args );
	void set( UList<ustring>& args );
	void unset( UList<ustring>& args );
	//
	void open( UList<ustring>& args );
	void load( const UList<ustring>& args );
	void run( UList<ustring>& args );
	void externalCommand( UList<ustring>& args );
	//
	void save( UList<ustring>& args );
	void exportGraph( UList<ustring>& args );
	//
	void show( UList<ustring>& args );
	void log( UList<ustring>& args );
	//
	void about();
	void version();
	void help( UList<ustring>& args );
	void exit();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // GRAFTONITE_H
