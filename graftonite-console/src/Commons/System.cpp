#include "System.h"
/**********************************************/
#include <stdlib.h>
#include <filesystem>
#include "UString.h"
/**********************************************/
/**********************************************/
void System::openFile( const ustring& path )
{
	ustring cmd = "mimeopen \""+path+"\"";
	cmd = "xdg-open \"" + path + "\"";
	System::exec( cmd.c_str() );
}
/**********************************************/
/**********************************************/
int System::exec( const ustring& cmd )
{
	return std::system( cmd.c_str() );
}
/**********************************************/
/**********************************************/
ustring System::AppPath()
{
	return ustring(std::filesystem::current_path());
}
/**********************************************/
/**********************************************/
ustring System::HomePath()
{
	return ustring(getenv("HOME"));
}
/**********************************************/
/**********************************************/
ustring System::LocalPath()
{
	ustring local_path = System::HomePath();
#if defined(_WIN32)
		local_path += "/AppData/Local/graftonite/console";
#elif defined(_WIN64)
	#define PLATFORM_NAME "windows" // Windows
#elif defined(__CYGWIN__) && !defined(_WIN32)
	#define PLATFORM_NAME "windows" // Windows (Cygwin POSIX under Microsoft Window)
#elif defined(__linux__)
		local_path += "/.local/graftonite/console";
//	#define PLATFORM_NAME "linux" // Debian, Ubuntu, Gentoo, Fedora, openSUSE, RedHat, Centos and other
#elif defined(__unix__) || !defined(__APPLE__) && defined(__MACH__)
	#include <sys/param.h>
	#if defined(BSD)
		#define PLATFORM_NAME "bsd" // FreeBSD, NetBSD, OpenBSD, DragonFly BSD
	#endif
#elif defined(__APPLE__) && defined(__MACH__) // Apple OSX and iOS (Darwin)
	#include <TargetConditionals.h>
	#if TARGET_IPHONE_SIMULATOR == 1
		#define PLATFORM_NAME "ios" // Apple iOS
	#elif TARGET_OS_IPHONE == 1
		#define PLATFORM_NAME "ios" // Apple iOS
	#elif TARGET_OS_MAC == 1
		#define PLATFORM_NAME "osx" // Apple OSX
	#endif
#endif
	return local_path;
}
/**********************************************/
/**********************************************/
void System::CreateDirectories( const ustring& path )
{
	std::filesystem::create_directories( std::string(path) );
}
/**********************************************/
/**********************************************/
bool System::Exists(const ustring& path)
{
	return std::filesystem::exists( std::string(path) );
}
/**********************************************/
/**********************************************/
