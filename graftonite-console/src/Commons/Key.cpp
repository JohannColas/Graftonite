#include "Key.h"
/**********************************************/
/**********************************************/
/**********************************************/
ustring Key::toString( const Key::Keys& key )
{
	switch ( key )
	{
		case Key::Name: return "name";
		case Key::FileName: return "filename";
		case Key::PNG: return "png";
		case Key::PDF: return "pdf";
		case Key::SVG: return "svg";
		case Key::PS: return "ps";
		case Key::Working: return "working";
		case Key::Command: return "command";
		case Key::History: return "history";
		case Key::Lang: return "lang";
		case Key::Log: return "log";
		// Element types
		case Key::NoType: return "unknown type";
		case Key::Current: return "current";
		case Key::Graph: return "graph";
		case Key::Template: return "template";
		case Key::Layer: return "layer";
		case Key::Frame: return "frame";
		case Key::Axis: return "axis";
		case Key::Plot: return "plot";
		case Key::Legend: return "legend";
		case Key::Layout: return "layout";
		case Key::Vertical: return "vertical";
		case Key::Horizontal: return "horizontal";
		case Key::Table: return "table";
		case Key::Column: return "column";
		case Key::Row: return "row";
		case Key::Spacing: return "spacing";
		case Key::Margins: return "margins";
		case Key::Entry: return "entry";
		case Key::AddEntry: return "addentry";
		case Key::Title: return "title";
			// Geometry Settings
		case Key::X: return "x";
		case Key::Y: return "y";
		case Key::Position: return "position";
		case Key::Width: return "width";
		case Key::Height: return "height";
		case Key::Size: return "size";
		case Key::Type: return "type";
		case Key::Format: return "format";
			//  Settings
		case Key::Background: return "background";
		case Key::Fill: return "fill";
		case Key::Borders: return "borders";
			// Positions
		case Key::Center: return "center";
		case Key::Bottom: return "bottom";
		case Key::Top: return "top";
		case Key::Right: return "right";
		case Key::Left: return "left";
		case Key::Outside: return "outside";
		case Key::Inside: return "inside";
			// Anchors
		case Key::BottomLeft: return "bottomleft";
		case Key::BottomRight: return "bottomright";
		case Key::TopLeft: return "topleft";
		case Key::TopRight: return "topright";
		case Key::BaselineLeft: return "baselineleft";
		case Key::Baseline: return "baseline";
		case Key::BaselineRight: return "baselineright";
			// Specific Axis Settings
		case Key::Hide: return "hide";
		case Key::Min: return "min";
		case Key::Max: return "max";
		case Key::Scale: return "scale";
		case Key::Option: return "option";
		case Key::LinkTo: return "linkto";
			// Line Settings
		case Key::Line: return "line";
		case Key::Dash: return "dash";
		case Key::Offset: return "offset";
		case Key::Color: return "color";
		case Key::Join: return "join";
		case Key::Round: return "round";
		case Key::Bevel: return "bevel";
		case Key::Miter: return "miter";
		case Key::MiterLimit: return "miterlimit";
		case Key::Cap: return "cap";
		case Key::Butt: return "butt";
		case Key::Square: return "square";
			// Ticks Settings
		case Key::Ticks: return "ticks";
		case Key::Increment: return "increment";
		case Key::Numbers: return "numbers";
		case Key::Minor: return "minor";
			// Labels Settings
		case Key::Labels: return "labels";
		case Key::Shift: return "shift";
		case Key::Anchor: return "anchors";
		case Key::Transform: return "transform";
			// Font Settings
		case Key::Font: return "font";
		case Key::Family: return "family";
		case Key::Slant: return "slant";
		case Key::Weight: return "weight";
		case Key::Underline: return "underline";
		case Key::Alignment: return "alignment";
		case Key::ScriptOpt: return "scriptopt";
		case Key::Capitalize: return "capitalization";
			// Title Settings
		case Key::Text: return "text";
			// Grids Settings
		case Key::Grids: return "grids";
			// Plot Settings
		case Key::Axis1: return "axis1";
		case Key::Axis2: return "axis2";
		case Key::Axis3: return "axis3";
		case Key::Coordinates: return "coordinates";
		case Key::Function: return "function";
		case Key::Image: return "image";
		case Key::Data: return "data";
		case Key::Data1: return "data1";
		case Key::Data2: return "data2";
		case Key::Data3: return "data3";
		case Key::Gap: return "gap";
		case Key::Symbols: return "symbols";
		case Key::BeginAngle: return "beginangle";
		case Key::EndAngle: return "endangle";
		case Key::Rotation: return "rotation";
		case Key::Covering: return "covering";
		case Key::Area: return "area";
		case Key::Bars: return "bars";
		case Key::ErrorBars: return "errorbars";
			// Settings
		case Key::Path: return "path";
		case Key::Separator: return "separator";
		case Key::Shape: return "shape";
		default: return "";
	}
}
/**********************************************/
Key::Keys Key::toKeys( const ustring& keys )
{
	ustring key = keys.remove("-");
	// Element types
	if ( key == "n"	|| key == "name" )
		return Key::Name;
	else if ( key == "fln" || key == "filename" )
		return Key::FileName;
	else if ( key == "wk" || key == "working" )
		return Key::Working;
	else if ( key == "cmd" || key == "command" )
		return Key::Command;
	else if ( key == "hst" || key == "history" )
		return Key::History;
	else if ( key == "la" || key == "lang" )
		return Key::Lang;
	else if ( key == "png" )
		return Key::PNG;
	else if ( key == "pdf" )
		return Key::PDF;
	else if ( key == "svg" )
		return Key::SVG;
	else if ( key == "ps" )
		return Key::PS;
	else if ( key == "cr"||  key == "current" )
		return Key::Current;
	else if ( key == "g"|| key == "graph" )
		return Key::Graph;
	else if ( key == "tpl"|| key == "template" )
		return Key::Template;
	else if ( key == "ly"|| key == "layer" )
		return Key::Layer;
	else if ( key == "f" || key == "frame" )
		return Key::Frame;
	else if ( key == "a" || key == "axis" )
		return Key::Axis;
	else if ( key == "pl" || key == "plot" )
		return Key::Plot;
	else if ( key == "lg" || key == "legend" )
		return Key::Legend;
	else if ( key == "lyt" || key == "layout" )
		return Key::Layout;
	else if ( key == "ver" || key == "vertical" )
		return Key::Vertical;
	else if ( key == "hor" || key == "horizontal" )
		return Key::Horizontal;
	else if ( key == "tab" || key == "table" )
		return Key::Table;
	else if ( key == "col" || key == "column" )
		return Key::Column;
	else if ( key == "row")
		return Key::Row;
	else if ( key == "spc" || key == "spacing" )
		return Key::Spacing;
	else if ( key == "mgn" || key == "margins" )
		return Key::Margins;
	else if ( key == "en" || key == "entry" )
		return Key::Entry;
	else if ( key == "aen" || key == "addentry" )
		return Key::AddEntry;
	else if ( key == "tl" || key == "title" )
		return Key::Title;
	else if ( key == "layers" )
		return Key::Layer;
	else if ( key == "frames" )
		return Key::Frame;
	else if ( key == "axes" )
		return Key::Axis;
	else if ( key == "plots" )
		return Key::Plot;
	else if ( key == "legends" )
		return Key::Legend;
	else if ( key == "titles" )
		return Key::Title;
	// Geometry Settings
	else if ( key == "x" )
		return Key::X;
	else if ( key == "y" )
		return Key::Y;
	else if ( key == "p" || key == "position" )
		return Key::Position;
	else if ( key == "w" || key == "width" )
		return Key::Width;
	else if ( key == "h" || key == "height" )
		return Key::Height;
	else if ( key == "s" || key == "size" )
		return Key::Size;
	else if ( key == "ty" || key == "type" )
		return Key::Type;
	else if ( key == "fm" || key == "format" )
		return Key::Format;
	//  Settings
	else if ( key == "bg" || key == "background" )
		return Key::Background;
	else if ( key == "fl" || key == "fill" )
		return Key::Fill;
	else if ( key == "bd" || key == "borders" )
		return Key::Borders;
	// Positions
	else if ( key == "c" || key == "center" )
		return Key::Center;
	else if ( key == "b" || key == "bottom" )
		return Key::Bottom;
	else if ( key == "t" || key == "top" )
		return Key::Top;
	else if ( key == "l" || key == "left" )
		return Key::Left;
	else if ( key == "r" || key == "right" )
		return Key::Right;
	else if ( key == "o" || key == "outside" )
		return Key::Outside;
	else if ( key == "i" || key == "inside" )
		return Key::Inside;
	// Anchors
	else if ( key == "bl" || key == "bottomleft" )
		return Key::BottomLeft;
	else if ( key == "br" || key == "bottomright" )
		return Key::BottomRight;
	else if ( key == "tl" || key == "topleft" )
		return Key::TopLeft;
	else if ( key == "tr" || key == "topright" )
		return Key::TopRight;
	else if ( key == "bsf" || key == "baselineleft" )
		return Key::BaselineLeft;
	else if ( key == "bs" || key == "baseline" )
		return Key::Baseline;
	else if ( key == "bsr" || key == "baselineright" )
		return Key::BaselineRight;
	// Specific Axis Settings
	else if ( key == "hd" || key == "hide" )
		return Key::Hide;
	else if ( key == "mn" || key == "min" )
		return Key::Min;
	else if ( key == "mx" || key == "max" )
		return Key::Max;
	else if ( key == "sc" || key == "scale" )
		return Key::Scale;
	else if ( key == "opt" || key == "option" )
		return Key::Option;
	else if ( key == "lin" || key == "linear" )
		return Key::Linear;
	else if ( key == "log10" )
		return Key::Log10;
	else if ( key == "log" )
		return Key::Log;
	else if ( key == "logx" )
		return Key::LogX;
	else if ( key == "rcp" || key == "reciprocal" )
		return Key::Reciprocal;
	else if ( key == "orcp" || key == "offsetreciprocal" )
		return Key::OffsetReciprocal;
	else if ( key == "lk" || key == "linkto" )
		return Key::LinkTo;
	// Line Settings
	else if ( key == "ln" || key == "line" )
		return Key::Line;
	else if ( key == "ds" || key == "dash" )
		return Key::Dash;
	else if ( key == "of" || key == "offset" )
		return Key::Offset;
	else if ( key == "cl" || key == "color" )
		return Key::Color;
	else if ( key == "jn" || key == "join" )
		return Key::Join;
	else if ( key == "rd" || key == "round" )
		return Key::Round;
	else if ( key == "bv" || key == "bevel" )
		return Key::Bevel;
	else if ( key == "mt" || key == "miter" )
		return Key::Miter;
	else if ( key == "ml" || key == "miterlimit" )
		return Key::MiterLimit;
	else if ( key == "cp" || key == "cap" )
		return Key::Cap;
	else if ( key == "bt" || key == "butt" )
		return Key::Butt;
	else if ( key == "sq" || key == "square" )
		return Key::Square;
	// Ticks Settings
	else if ( key == "tk" || key == "ticks" )
		return Key::Ticks;
	else if ( key == "inc" || key == "increment" )
		return Key::Increment;
	else if ( key == "nb" || key == "numbers" )
		return Key::Numbers;
	else if ( key == "mi" || key == "minor" )
		return Key::Minor;
	// Labels Settings
	else if ( key == "lb" || key == "labels" )
		return Key::Labels;
	else if ( key == "sh" || key == "shift" )
		return Key::Shift;
	else if ( key == "anc" || key == "anchor" )
		return Key::Anchor;
	else if ( key == "tr" || key == "transform" )
		return Key::Transform;
	else if ( key == "ft" || key == "font" )
		return Key::Font;
	else if ( key == "fam" || key == "family" )
		return Key::Family;
	else if ( key == "slt" || key == "slant" )
		return Key::Slant;
	else if ( key == "wg" || key == "weight" )
		return Key::Weight;
	else if ( key == "cpt" || key == "capitalization" )
		return Key::Capitalize;
	else if ( key == "cpt" || key == "underline" )
		return Key::Underline;
	else if ( key == "alg" || key == "alignment" )
		return Key::Alignment;
	else if ( key == "sco" || key == "scriptopt" )
		return Key::ScriptOpt;
	// Title Settings
	else if ( key == "tx" || key == "text" )
		return Key::Text;
	// Grids Settings
	else if ( key == "gd" || key == "grids" )
		return Key::Grids;
	// Plot Settings
	else if ( key == "a1" || key == "axis1" )
		return Key::Axis1;
	else if ( key == "a2" || key == "axis2" )
		return Key::Axis2;
	else if ( key == "a3" || key == "axis3" )
		return Key::Axis3;
	else if ( key == "pth" || key == "path" )
		return Key::Path;
	else if ( key == "cd" || key == "coordinates" )
		return Key::Coordinates;
	else if ( key == "fn" || key == "function" )
		return  Key::Function;
	else if ( key == "img" || key == "image" )
		return  Key::Image;
	else if ( key == "dt" || key == "data" )
		return Key::Data;
	else if ( key == "dt1" || key == "data1" )
		return Key::Data1;
	else if ( key == "dt2" || key == "data2" )
		return Key::Data2;
	else if ( key == "dt3" || key == "data3" )
		return Key::Data3;
	else if ( key == "gp" || key == "gap" )
		return Key::Gap;
	else if ( key == "sy" || key == "symbols" )
		return Key::Symbols;
	else if ( key == "bang" || key == "beginangle" )
		return Key::BeginAngle;
	else if ( key == "eang" || key == "endangle" )
		return Key::EndAngle;
	else if ( key == "rot" || key == "rotation" )
		return Key::Rotation;
	else if ( key == "cov" || key == "covering" )
		return Key::Covering;
	else if ( key == "ar" || key == "area" )
		return Key::Area;
	else if ( key == "br" || key == "bars" )
		return Key::Bars;
	else if ( key == "eb" || key == "errorbars" )
		return Key::ErrorBars;
	else if ( key == "sep" || key == "separator" )
		return Key::Separator;
	else if ( key == "sh" || key == "shape" )
		return Key::Shape;
	return Key::NoKey;
}
/**********************************************/
UList<Key::Keys> Key::toKeysList( const ustring& keys )
{
	UList<Key::Keys> keys_lst;
	Key::setKeysList( keys, keys_lst );
	return keys_lst;
}
/**********************************************/
void Key::setKeysList( const ustring& keys, UList<Key::Keys>& key_list )
{
	key_list.clear();
	UList<ustring> keys_lst;
	if ( keys.contains(".") )
		keys_lst = keys.remove("-").split(".");
	else if ( keys.contains("/") )
		keys_lst = keys.remove("-").split("/");
	else
		keys_lst = {keys.remove("-")};

	for ( const ustring& key : keys_lst )
		key_list.append( Key::toKeys(key) );
}
bool Key::isElementType( const Key::Keys& key )
{
	if ( key == Key::Graph ||
	     key == Key::Layer ||
	     key == Key::Frame ||
	     key == Key::Axis ||
	     key == Key::Plot ||
	     key == Key::Legend ||
	     key == Key::Title ||
	     key == Key::Data )
		return true;
	return false;
}
/**********************************************/
/**********************************************/
Key::Key( const ustring& keys )
{
	_fullkeys = keys;
	Key::setKeysList( keys, _keys );

}
Key::Key( const Keys& key, const Key::Keys& key2, const Key::Keys& key3 )
{
	if ( key != Key::NoKey )
		_keys.append( key );
	if ( key2 != Key::NoKey )
		_keys.append( key2 );
	if ( key3 != Key::NoKey )
		_keys.append( key3 );
}
Key::Key( const UList<Keys>& keys )
{
	_keys = keys;
}
Key::Key( const UList<Keys>& keys, const Keys& subkey, const Key::Keys& subkey2 )
{
	_keys = keys;
	if ( subkey != Key::NoKey )
		_keys.append( subkey );
	if ( subkey2 != Key::NoKey )
		_keys.append( subkey2 );
}
/**********************************************/
UList<Key::Keys> Key::keys() const
{
	return _keys;
}
/**********************************************/
ustring Key::fullKeys() const
{
	if ( keys().first() == Key::NoKey )
		return _fullkeys;
	ustring str;
	str += Key::toString( keys().first() );
	for ( int it = 1; it < keys().size(); ++it )
		str += "." + Key::toString( keys().at(it) );
	return str;
}
/**********************************************/
/**********************************************/
bool Key::isLineStyle(const Keys& key, const Keys& key2) const
{
	UList<Key::Keys> keys;
	if ( key != Key::NoKey )
		keys.append(key);
	if ( key2 != Key::NoKey )
		keys.append(key2);
	return ( *this == Key(keys, Key::Dash) ||
	         *this == Key(keys, Key::Dash, Key::Offset) ||
	         *this == Key(keys, Key::Width) ||
	         *this == Key(keys, Key::Color) ||
	         *this == Key(keys, Key::Cap) ||
	         *this == Key(keys, Key::Join) ||
	         *this == Key(keys, Key::MiterLimit)
	         );
}
/**********************************************/
bool Key::isLineStyle(const UList<Keys>& keys, bool include_cap, bool include_join) const
{
	return ( *this == Key(keys, Key::Dash) ||
	         *this == Key(keys, Key::Dash, Key::Offset) ||
	         *this == Key(keys, Key::Width) ||
	         *this == Key(keys, Key::Color) ||
	         (*this == Key(keys, Key::Cap) && include_cap) ||
	         (*this == Key(keys, Key::Join) && include_join) ||
	         (*this == Key(keys, Key::MiterLimit) && include_join)
	         );
}
/**********************************************/
bool Key::isFillStyle( const Keys& key, const Keys& key2 ) const
{
	UList<Key::Keys> keys;
	if ( key != Key::NoKey )
		keys.append(key);
	if ( key2 != Key::NoKey )
		keys.append(key2);
	return ( *this == Key(keys, Key::Color)
	         );
}
/**********************************************/
bool Key::isTextStyle( const Keys& key, const Keys& key2 ) const
{
	UList<Key::Keys> keys;
	if ( key != Key::NoKey )
		keys.append(key);
	if ( key2 != Key::NoKey )
		keys.append(key2);
//	keys.append(Key::Font);
	return ( *this == Key(keys, Key::Font, Key::Family) ||
	         *this == Key(keys, Key::Font, Key::Slant) ||
	         *this == Key(keys, Key::Font, Key::Weight) ||
	         *this == Key(keys, Key::Font, Key::Underline) ||
	         *this == Key(keys, Key::Font, Key::Capitalize) ||
	         *this == Key(keys, Key::Font, Key::Size) ||
	         *this == Key(keys, Key::Font, Key::Color) ||
	         *this == Key(keys, Key::Font, Key::Anchor) ||
	         *this == Key(keys, Key::Alignment) ||
	         *this == Key(keys, Key::Font, Key::ScriptOpt) ||
	         *this == Key(keys, Key::Line, Key::Spacing) ||
	         *this == Key(keys, Key::Rotation)
	         );
}
/**********************************************/
/**********************************************/
bool Key::operator<( const Key& key ) const
{
	return ( this->fullKeys() < key.fullKeys() );
}
/**********************************************/
bool Key::operator==( const Key& key ) const
{
	if ( this->keys().size() != key.keys().size() )
		return false;
	for ( int it = 0; it < this->keys().size(); ++it )
	{
		if ( this->keys().at(it) != key.keys().at(it) )
			return false;
	}
	return true;
}
/**********************************************/
bool Key::operator!=( const Key& key ) const
{
	return !(*this == key);
}
/**********************************************/
bool Key::operator==( const Key::Keys& key ) const
{
	if ( this->keys().size() != 1 )
		return false;
	return (this->keys().at(0) == key);
}
/**********************************************/
bool Key::operator!=( const Key::Keys& key ) const
{
	return !(*this == key);
}
/**********************************************/
bool Key::operator==( const UList<Key::Keys>& keys ) const
{
	return (*this == Key(keys));
}
/**********************************************/
/**********************************************/
