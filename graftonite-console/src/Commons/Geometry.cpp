#include "Geometry.h"
/**********************************************/
#include "UString.h"
/**********************************************/
/**********************************************/
/**********************************************/
Point::Point()
{

}
Point::Point( double x, double y )
{
	setX(x);
	setY(y);
}
Point::Point( const Point& point )
{
	setX(point.x());
	setY(point.y());
}
Point::Point(const ustring& point)
{
	set( point );
}
double Point::x() const
{
	return _x;
}
void Point::setX( double x )
{
	_x = x;
}
double Point::y() const
{
	return _y;
}
void Point::setY( double y )
{
	_y = y;
}
void Point::set( const ustring& point )
{
	if ( point.contains("x") || point.contains(",") )
	{
		UList<ustring> sls;
		if ( point.contains("x") )
			sls = point.split("x");
		else if ( point.contains(",") )
			sls = point.split(",");
		if ( sls.size() > 1 )
		{
			_x = sls.at(0).toDouble();
			_y = sls.at(1).toDouble();
		}
	}
	else if ( point.isDouble() || point.isInteger() )
	{
		_x = point.toDouble();
		_y = _x;
	}
}
Point& Point::operator=(const Point& point)
{
	this->setX( point.x() );
	this->setY( point.y() );
	return *this;
}
Point& Point::operator+=( const Point& point )
{
	this->setX( this->x() + point.x() );
	this->setY( this->y() + point.y() );
	return *this;
}
Point Point::operator+( const Point& point )
{
	Point new_point;
	new_point.setX( this->x() + point.x() );
	new_point.setY( this->y() + point.y() );
	return new_point;
}
Point& Point::operator-=( const Point& point )
{
	this->setX( this->x() - point.x() );
	this->setY( this->y() - point.y() );
	return *this;
}
Point Point::operator-( const Point& point )
{
	Point new_point;
	new_point.setX( this->x() - point.x() );
	new_point.setY( this->y() - point.y() );
	return new_point;
}
/**********************************************/
/**********************************************/
/**********************************************/
Anchor::Anchor()
{
}
Anchor::Anchor( const Key::Keys& anchor )
{
	_anchor = anchor;
}
bool Anchor::isLeft() const
{
	return ( _anchor == Key::Left ||
	         _anchor == Key::BaselineLeft ||
	         _anchor == Key::TopLeft ||
	         _anchor == Key::BottomLeft );
}
bool Anchor::isRight() const
{
	return ( _anchor == Key::Right ||
	         _anchor == Key::BaselineRight ||
	         _anchor == Key::TopRight ||
	         _anchor == Key::BottomRight );
}
bool Anchor::isHCenter() const
{
	return ( _anchor == Key::Center ||
	         _anchor == Key::Baseline ||
	         _anchor == Key::Top ||
	         _anchor == Key::Bottom );
}
bool Anchor::isTop() const
{
	return ( _anchor == Key::Top ||
	         _anchor == Key::TopLeft ||
	         _anchor == Key::TopRight );
}
bool Anchor::isBottom() const
{
	return ( _anchor == Key::Bottom ||
	         _anchor == Key::BottomLeft ||
	         _anchor == Key::BottomRight );
}
bool Anchor::isVCenter() const
{
	return ( _anchor == Key::Center ||
	         _anchor == Key::Left ||
	         _anchor == Key::Right );
}
bool Anchor::isBaseline() const
{
	return ( _anchor == Key::Baseline ||
	         _anchor == Key::BaselineLeft ||
	         _anchor == Key::BaselineRight );
}
/**********************************************/
/**********************************************/
/**********************************************/
Line::Line()
{

}
Line::Line( const Point& p1, const Point& p2 )
{
	_x1 = p1.x();
	_y1 = p1.y();
	_x2 = p2.x();
	_y2 = p2.y();
}
Line::Line( double x1, double y1, double x2, double y2 )
{
	_x1 = x1;
	_y1 = y1;
	_x2 = x2;
	_y2 = y2;
}
Line::Line( double x, double y, double length, const Orientation& orientation )
{
	_x1 = x;
	_y1 = y;
	if ( orientation == Line::Horizontal )
	{
		_x2 = x + length;
		_y2 = y;
	}
	else
	{
		_x2 = x;
		_y2 = y + length;
	}
}
Line::Line( const Point& p1, double shift, double length, const Orientation& orientation )
{
	_x1 = p1.x();
	_y1 = p1.y();
	if ( orientation == Line::Horizontal )
	{
		_x1 += shift;
		_x2 = _x1 + length;
		_y2 = _y1;
	}
	else
	{
		_x2 = _x1;
		_y1 += shift;
		_y2 = _y1 + length;
	}
}
double Line::x1() const
{
	return _x1;
}
void Line::setX1( double x1 )
{
	_x1 = x1;
}
double Line::y1() const
{
	return _y1;
}
void Line::setY1( double y1 )
{
	_y1 = y1;
}
void Line::setP1( const Point& p1 )
{
	_x1 = p1.x();
	_y1 = p1.y();
}
Point Line::p1() const
{
	return Point( _x1, _y1 );
}
double Line::x2() const
{
	return _x2;
}
void Line::setX2( double x2 )
{
	_x2 = x2;
}
double Line::y2() const
{
	return _y2;
}
void Line::setY2( double y2 )
{
	_y2 = y2;
}
void Line::setP2( const Point& p2 )
{
	_x2 = p2.x();
	_y2 = p2.y();
}
Point Line::p2() const
{
	return Point( _x2, _y2 );
}
/**********************************************/
/**********************************************/
/**********************************************/
Size::Size()
{

}
Size::Size( double width, double height )
{
	setWidth(width);
	setHeight(height);
}
Size::Size( const ustring& size )
{
	if ( size.contains("x") || size.contains(",") )
	{
		UList<ustring> sls;
		if ( size.contains("x") )
			sls = size.split("x");
		else if ( size.contains(",") )
			sls = size.split(",");
		if ( sls.size() > 1 )
		{
			_width = sls.at(0).toDouble();
			_height = sls.at(1).toDouble();
		}
	}
	else if ( size.isDouble() || size.isInteger() )
	{
		_width = size.toDouble();
		_height = _width;
	}
}
double Size::width() const
{
	return _width;
}
void Size::setWidth( double width )
{
	_width = width;
}
double Size::height() const
{
	return _height;
}
void Size::setHeight( double height )
{
	_height = height;
}
Point Size::point(const Key& anchor) const
{
	if ( anchor == Key::Top )
		return Point(0.5*_width,0);
	if ( anchor == Key::TopRight )
		return Point(_width,0);
	if ( anchor == Key::Left )
		return Point(0,0.5*_height);
	if ( anchor == Key::Center )
		return Point(0.5*_width,0.5*_height);
	if ( anchor == Key::Right )
		return Point(_width,0.5*_height);
	if ( anchor == Key::BottomLeft )
		return Point(0,_height);
	if ( anchor == Key::Bottom )
		return Point(0.5*_width,_height);
	if ( anchor == Key::BottomRight )
		return Point(_width,_height);
	return Point(0,0);
}
/**********************************************/
/**********************************************/
/**********************************************/
Rect::Rect()
{

}
Rect::Rect( double x, double y, double width, double height )
{
	setX(x);
	setY(y);
	setWidth(width);
	setHeight(height);
}
Rect::Rect( const Point& point, const Size& size )
{
	setX(point.x());
	setY(point.y());
	setWidth(size.width());
	setHeight(size.height());
}
Rect::Rect( const Line& line )
{
	_x = line.x1();
	_width = line.x2()-_x;
	if ( line.x1() > line.x2() )
	{
		_x = line.x2();
		_width = line.x1() - _x;
	}
	_y = line.y1();
	_height = line.y2()-_y;
	if ( line.y1() > line.y2() )
	{
		_y = line.y2();
		_height = line.y1() - _y;
	}
}
Rect::Rect( const ustring& rect )
{
	UList<ustring> parts = rect.split(",");
	if ( parts.size() == 2 )
	{
		setPoint( parts.at(0) );
		setSize( parts.at(1) );
	}
	else if ( parts.size() == 4 )
	{
		setX( parts.at(0).toDouble() );
		setY( parts.at(1).toDouble() );
		setWidth( parts.at(2).toDouble() );
		setHeight( parts.at(3).toDouble() );
	}
}
double Rect::x() const
{
	return _x;
}
void Rect::setX( double x )
{
	_x = x;
}
double Rect::y() const
{
	return _y;
}
void Rect::setY( double y )
{
	_y = y;
}
Point Rect::origin() const
{
	return Point( _x, _y );
}
Point Rect::point( const Key& anchor ) const
{
	if ( anchor == Key::Top )
		return top();
	if ( anchor == Key::TopRight )
		return topRight();
	if ( anchor == Key::Left )
		return centerLeft();
	if ( anchor == Key::Center )
		return center();
	if ( anchor == Key::Right )
		return centerRight();
	if ( anchor == Key::BottomLeft )
		return bottomLeft();
	if ( anchor == Key::Bottom )
		return bottom();
	if ( anchor == Key::BottomRight )
		return bottomRight();
	return topLeft();
}
void Rect::setPoint( double x, double y )
{
	setX(x);
	setY(y);
}
void Rect::setPoint( const Point& point )
{
	setX(point.x());
	setY(point.y());
}
double Rect::width() const
{
	return _width;
}
void Rect::setWidth( double width )
{
	_width = width;
}
double Rect::height() const
{
	return _height;
}
void Rect::setHeight( double height )
{
	_height = height;
}
Size Rect::size() const
{
	return Size( _width, _height );
}
void Rect::setSize( double width, double height )
{
	setWidth(width);
	setHeight(height);
}
void Rect::setSize( const Size& size )
{
	setWidth(size.width());
	setHeight(size.height());
}

double Rect::yTop() const
{
	return _y;
}

double Rect::yCenter() const
{
	return (_y+0.5*_height);
}

double Rect::yBottom() const
{
	return (_y+_height);
}

double Rect::xLeft() const
{
	return _x;
}

double Rect::xCenter() const
{
	return (_x+0.5*_width);
}

double Rect::xRight() const
{
	return (_x+_width);
}

Point Rect::topLeft() const
{
	return Point( xLeft(), yTop() );
}

Point Rect::top() const
{
	return Point( xCenter(), yTop() );
}

Point Rect::topRight() const
{
	return Point( xRight(), yTop() );
}

Point Rect::centerLeft() const
{
	return Point( xLeft(), yCenter() );
}

Point Rect::center() const
{
	return Point( xCenter(), yCenter() );
}

Point Rect::centerRight() const
{
	return Point( xRight(), yCenter() );
}

Point Rect::bottomLeft() const
{
	return Point( xLeft(), yBottom() );
}

Point Rect::bottom() const
{
	return Point( xCenter(), yBottom() );
}

Point Rect::bottomRight() const
{
	return Point( xRight(), yBottom() );
}

Line Rect::topLine() const
{
	return Line(topLeft(), topRight());
}

Line Rect::hCenterLine() const
{
	return Line(centerLeft(), centerRight());
}

Line Rect::bottomLine() const
{
	return Line(bottomLeft(), bottomRight());
}

Line Rect::leftLine() const
{
	return Line(topLeft(), bottomLeft());
}

Line Rect::vCenterLine() const
{
	return Line(top(), bottom());
}

Line Rect::rightLine() const
{
	return Line(topRight(), bottomRight());
}

Line Rect::line(const Key& pos, bool vcenter) const
{
	if ( pos == Key::Top )
		return topLine();
	if ( pos == Key::Bottom )
		return bottomLine();
	if ( pos == Key::Left )
		return leftLine();
	if ( pos == Key::Right )
		return rightLine();
	if ( pos == Key::Center && vcenter )
		return vCenterLine();
	return hCenterLine();
}

bool Rect::contains(const Point& pos)
{
	if ( xLeft() <= pos.x() &&
	     pos.x() <= xRight() &&
	     yTop() <= pos.y() &&
	     pos.y() <= yBottom() )
		return true;
	return false;
}

bool Rect::contains(const Line& line)
{
	if ( this->contains(line.p1()) &&
	     this->contains(line.p2()) )
		return true;
	return false;
}

bool Rect::contains(const Rect& rect)
{

	if ( this->contains(rect.topLeft()) &&
	     this->contains(rect.bottomRight()) )
		return true;
	return false;
}

void Rect::expand(const Point& pos)
{
	if ( this->contains(pos) )
		return;
	double px = pos.x(), py = pos.y();
	double xl = xLeft(), xr = xRight(),
	        yt = yTop(), yb = yBottom();
	if ( px < xl )
	{
		_x = px;
		_width = xr - _x;
	}
	else if ( px > xr )
		_width = px - _x;
	if ( py < yt )
	{
		_y = py;
		_height = yb - _y;
	}
	else if ( py > yb )
		_height = py - _y;
}

void Rect::expand(const Line& line)
{
	if ( this->contains(line) )
		return;
	this->expand( line.p1() );
	this->expand( line.p2() );
}

void Rect::expand(const Rect& rect)
{
	if ( this->contains(rect) )
		return;
	this->expand( rect.topLeft() );
	this->expand( rect.bottomRight() );
}
/**********************************************/
/**********************************************/
/**********************************************/
