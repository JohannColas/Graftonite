#include "UConsole.h"
/**********************************************/
#include <unistd.h>
/**********************************************/
#include "../Graftonite.h"
/**********************************************/
/**********************************************/
/**********************************************/
/* Terminal Core */
/**********************************************/
void UConsole::init()
{
	//	struct termios old_tio, new_tio;
	/* get the terminal settings for stdin */
	tcgetattr( STDIN_FILENO, &old_tio );
	/* we want to keep the old setting to restore them a the end */
	new_tio = old_tio;
	/* disable canonical mode (buffered i/o) and local echo */
	new_tio.c_lflag &=( ~ICANON & ~ECHO );
	/* set the new settings immediately */
	tcsetattr( STDIN_FILENO, TCSANOW, &new_tio );
	/*--------------------*/
}
/**********************************************/
void UConsole::close()
{
	/* restore the former settings */
	tcsetattr( STDIN_FILENO, TCSANOW, &old_tio );
	/*--------------------*/
}
/**********************************************/
void UConsole::exec( Graftonite* app )
{
	_app = app;
	/* --------------------------- */
	// Terminal Initialisation
	UConsole::init();
	/* --------------------------- */
	unsigned char c;
	ustring cur_cmd;
	int history_index = -1;
	int cursor_pos = 0;
	bool exit = false;
	UConsole::startCommandLine();
	do
	{
		if ( !cur_cmd.empty() )
			UConsole::startCommandLine();
		cursor_pos = 0;
		history_index = -1;
		cur_cmd.clear();
		do
		{
			c = getchar();
			if ( c == KEYS::ESC ) // escape key
			{
				char new_c = getchar();
				if ( new_c == 91 )
				{
					new_c = getchar();
					if ( new_c == KEYS::UP_ARROW ) // Up Arrow
					{
						UConsole::clearCommandLine();
						cur_cmd.clear();
						if ( history_index == -1 )
						{
							history_index = cmd_history.size()-1;
						}
						else
							--history_index;
						if ( history_index < 0 )
							history_index = 0;
						if ( history_index != -1 && cmd_history.size() > 0 )
						{
							cur_cmd = cmd_history.at(history_index);
							UConsole::clearCommandLine();
							UConsole::drawCommandLine( cur_cmd );
							cursor_pos = cur_cmd.length();
						}
					}
					else if ( new_c == KEYS::DOWN_ARROW ) // Down Arrow
					{
						UConsole::clearCommandLine();
						cur_cmd.clear();
						cursor_pos = 0;
						if ( history_index > -1 )
							++history_index;
						if ( history_index > (int)cmd_history.size()-1 )
							history_index = -1;
						if ( history_index != -1 && cmd_history.size() > 0 )
						{
							cur_cmd = cmd_history.at(history_index);
							UConsole::clearCommandLine();
							UConsole::drawCommandLine( cur_cmd );
							cursor_pos = cur_cmd.length();
						}
					}
					else if ( new_c == KEYS::RIGHT_ARROW ) // Right Arrow
					{
						if ( !cur_cmd.empty() )
						{
							++cursor_pos;
							if ( cursor_pos < (int)cur_cmd.length()+1 )
								UConsole::moveCursorForward(1);
							if ( cursor_pos > (int)cur_cmd.length() )
								cursor_pos = cur_cmd.length();
						}
					}
					else if ( new_c == KEYS::LEFT_ARROW )// Left Arrow
					{
						if ( !cur_cmd.empty() )
						{
							--cursor_pos;
							if ( cursor_pos >= 0 )
								UConsole::moveCursorBackward(1);
							if ( cursor_pos < 0 )
								cursor_pos = 0;
						}
					}
					else if ( new_c == KEYS::FN_RIGHT )// FN + Right Arrow
					{
						if ( !cur_cmd.empty() && cursor_pos < (int)cur_cmd.length() )
						{
							int diff = cur_cmd.length()-cursor_pos;
							if ( diff > 0 )
							{
								UConsole::moveCursorForward(diff);
								cursor_pos = cur_cmd.length();
							}
						}
					}
					else if ( new_c == KEYS::FN_LEFT )// FN + Left Arrow
					{
						if ( !cur_cmd.empty() && cursor_pos > 0 )
						{
								UConsole::moveCursorBackward(cursor_pos);
								cursor_pos=0;
						}
					}
					else if ( new_c == 51 )
					{
						new_c = getchar();
						if ( new_c == KEYS::DEL ) // delete key
						{
							if ( cursor_pos < (int)cur_cmd.length() )
							{
								int old_dif = cur_cmd.length() - cursor_pos;
								cur_cmd.erase( cursor_pos, 1 );
								UConsole::clearCommandLine();
								UConsole::drawCommandLine( cur_cmd );
								if ( old_dif > 0 )
								{
									UConsole::moveCursorBackward(old_dif);
									UConsole::moveCursorForward(1);
								}
							}
						}
					}
				}
				else // escape key
				{
					exit = true;
					break;
				}
			}
			else if ( c == KEYS::RETURN )
			{
				UConsole::normalColor();
				if ( _app != nullptr && !_app->runCommand( cur_cmd ) )
					exit = true;
				history_index = -1;
				cursor_pos = 0;
			}
			else if ( c == KEYS::BACKSPACE ) // BACKSPACE KEY
			{
				// Reset Current Cmd Index
				history_index = -1;
				//
				int old_dif = cur_cmd.length() - cursor_pos;
				--cursor_pos;
				if ( cur_cmd.length() > 0 && cursor_pos >= 0 )
				{
					cur_cmd.erase( cursor_pos, 1 );
					UConsole::clearCommandLine();
					UConsole::drawCommandLine( cur_cmd );
					UConsole::moveCursorBackward(old_dif);
				}
				if ( cursor_pos < 0 )
					cursor_pos = 0;
			}
			else
			{
				int old_dif = cur_cmd.length() - cursor_pos;
				cur_cmd.insert( cursor_pos, 1, c );
				++cursor_pos;
				UConsole::clearCommandLine();
				UConsole::drawCommandLine( cur_cmd );
				UConsole::moveCursorBackward(old_dif);
			}
		}
		while ( c != KEYS::RETURN );
	}
	while( cur_cmd.substr(0,4) != "exit" && !exit );

	UConsole::close();

}
/**********************************************/
void UConsole::clear()
{
	printf( "\033[2J" );
	printf( "\033[0;0f" );
}
UList<ustring> UConsole::CommandHistory()
{
	return cmd_history;
}
UList<ustring> UConsole::todayHistory()
{
	return today_history;
}
void UConsole::addCommandHistory( const UList<ustring>& cmdHistory )
{
	for ( const ustring& cmd : cmdHistory )
		if ( !cmd.empty() )
			cmd_history.append( cmd );
}
/**********************************************/
/**********************************************/
/**********************************************/
/*  */
/**********************************************/
void UConsole::fill( int width, const ustring& filler )
{
	for ( int it = 0; it < width; ++it )
		std::cout << filler;
}
void UConsole::show( const ustring& str, int width, const ustring& filler )
{
	ustring tmp;
	if ( width == -1 )
		tmp = str;
	else
	{
		for ( int it = 0; it < width; ++it )
		{
			if ( it < str.size() )
				tmp += str.at(it);
			else
				tmp += filler;
		}
	}
	std::cout << tmp;
}
void UConsole::showRight( const ustring& str, int width, const ustring& filler )
{
	ustring tmp;
	if ( width == -1 )
		tmp = str;
	else
	{
		for ( int it = 0; it < width; ++it )
		{
			if ( it < str.size() )
				tmp += str.at(it);
			else
				tmp = filler + tmp;
		}
	}
	std::cout << tmp;
}
void UConsole::print( const ustring& str, int indent, const ustring& filler )
{
	UConsole::fill( indent, filler );
	UConsole::show( str );
}
void UConsole::print2( const ustring& str, int width, int indent, const ustring& filler, int end_width, const ustring& end_filler )
{
	UConsole::fill( indent, filler );
	UConsole::show( str, width );
	UConsole::fill( end_width, end_filler );
}
void UConsole::println( const ustring& str, int indent, const ustring& filler )
{
	UConsole::print( str, indent, filler );
	std::cout << std::endl;
}
void UConsole::print2ln( const ustring& str, int width, int indent, const ustring& filler, int end_width, const ustring& end_filler )
{
	UConsole::print2( str, width, indent, filler, end_width, end_filler );
	std::cout << std::endl;
}
void UConsole::showTableRow( const UList<ustring>& strs, const UList<int>& widths, const UList<ustring>& fillers, const UList<bool>& aligns, const ustring& separator, int indent, const ustring& filler )
{
	UConsole::fill( indent, filler );
	for ( int it = 0; it < strs.size(); ++it )
	{
		ustring str = strs.at(it);
		int width = -1;
		if ( it < widths.size() )
			width = widths.at(it);
		ustring filler2 = " ";
		if ( it < fillers.size() )
			filler2 = fillers.at(it);
		bool align = false;
		if ( it < aligns.size() )
			align = aligns.at(it);
		if ( align )
			showRight( str, width, filler2 );
		else
			show( str, width, filler2 );
		if ( it+1 != strs.size() )
			std::cout << separator;
	}
	std::cout << std::endl;
}
/**********************************************/
/**********************************************/
/**********************************************/
/* Cursor Positionning */
/**********************************************/
void UConsole::changeCursorCol( int delta )
{
	if ( delta < 0 )
		moveCursorBackward( delta );
	else
		moveCursorForward( delta );
}
/**********************************************/
void UConsole::moveCursorBackward( int delta )
{
	if ( delta > 0 )
		printf( "\033[%dD", delta );
}
/**********************************************/
void UConsole::moveCursorForward( int delta )
{
	if ( delta > 0 )
		printf( "\033[%dC", delta );
}
/**********************************************/
void UConsole::saveCursorPos()
{
	printf("\033[s"); // save cursor position
}
/**********************************************/
void UConsole::restoreCursorPos()
{
	printf("\033[u"); // restore cursor position
}
/**********************************************/
/**********************************************/
/**********************************************/
/* Colors */
/**********************************************/
void UConsole::normalColor()
{
	printf( "\033[0m" );
}
/**********************************************/
void UConsole::cmdInputColor()
{
	printf( "\033[1;36m" );
}
/**********************************************/
void UConsole::cmdColor()
{
	printf( "\x1B[93m" );
}
/**********************************************/
void UConsole::warningColor()
{
	printf( "\x1B[33m" );
}
/**********************************************/
void UConsole::errorColor()
{
	printf( "\x1B[31m" );
}
/**********************************************/
void UConsole::testColors()
{
	printf("\n");
	printf("\x1B[31mTexting\033[0m\t\t");//ROUGE
	printf("\x1B[32mTexting\033[0m\t\t");//VERT
	printf("\x1B[33mTexting\033[0m\t\t");//ORANGE
	printf("\x1B[34mTexting\033[0m\t\t");//BLEU
	printf("\x1B[35mTexting\033[0m\n");//VIOLET

	printf("\x1B[36mTexting\033[0m\t\t");//BLEUVERT
	printf("\x1B[36mTexting\033[0m\t\t");//BLEUVERT
	printf("\x1B[36mTexting\033[0m\t\t");//BLEUVERT
	printf("\x1B[37mTexting\033[0m\t\t");//BLANC
	printf("\x1B[93mTexting\033[0m\n");//JAUNE

	printf("\033[3;42;30mTexting\033[0m\t\t");
	printf("\033[3;43;30mTexting\033[0m\t\t");
	printf("\033[3;44;30mTexting\033[0m\t\t");
	printf("\033[3;104;30mTexting\033[0m\t\t");
	printf("\033[3;100;30mTexting\033[0m\n");

	printf("\033[3;47;35mTexting\033[0m\t\t");
	printf("\033[2;47;35mTexting\033[0m\t\t");
	printf("\033[1;47;35mTexting\033[0m\t\t");
	printf("\n");
}
/**********************************************/
/**********************************************/
/**********************************************/
/* Messages */
/**********************************************/
void UConsole::warning_message( const ustring& message )
{
	UConsole::warningColor();
	printf( "Warning: %s\n", message.c_str() );
	UConsole::normalColor();
}
/**********************************************/
void UConsole::error_message( const ustring& message )
{
	UConsole::errorColor();
	printf( "Error: %s\n", message.c_str() );
	UConsole::normalColor();
}
/**********************************************/
/**********************************************/
/**********************************************/
/* Dialogs */
/**********************************************/
bool UConsole::confirmationDialog( const ustring& message )
{
	std::cout << std::endl << message;
	char c = 'n';
	std::cin >> c;
	std::cout << c << std::endl;
	return (c == 'Y' || c == 'y');
}
/**********************************************/
/**********************************************/
/**********************************************/
/* Command Lines Format */
/**********************************************/
void UConsole::startCommandLine()
{
	UConsole::cmdInputColor();
	printf( "%s> ", _progname.c_str() );
	UConsole::normalColor();
	UConsole::saveCursorPos();
}
/**********************************************/
void UConsole::clearCommandLine()
{
	UConsole::restoreCursorPos();
	UConsole::deleteToEndLine();
}
/**********************************************/
void UConsole::drawCommandLine( const ustring& cmdline )
{
	bool first_space_found = false;
	UConsole::cmdColor();
	char last_c = '&';
	for ( char c : cmdline )
	{
		if ( c == ' ' && last_c != '&' && !first_space_found )
		{
			UConsole::normalColor();
			printf( " " );
			first_space_found = true;
		}
		else
		{
			printf( "%c", c );
		}
		if ( c == '&' && last_c == '&' )
		{
			UConsole::cmdColor();
			first_space_found = false;
		}
		last_c = c;
	}
	UConsole::normalColor();
}
/**********************************************/
void UConsole::deleteToEndLine()
{
	printf("\033[K"); // delete char from cursor position to end line
}
/**********************************************/
void UConsole::addCommandToHistory( const ustring& cmdline )
{
	cmd_history.append( cmdline );
	today_history.append( cmdline );
}
/**********************************************/
/**********************************************/
/**********************************************/
