# Graftonite-Console


## Description

This program is dedicated to data visualization. It allows the user to plot data from various sources (manual coordinates, data file, function, or image).Only 2D axes are available (X & Y).

Written in C++17.


### Working features

    - Frame
    - X & Y Axis
    - Data plotting from coordinates
    - Graph styling
    - Frame styling
    - Axis styling
    - Plot styling
    - Data (accept only numerical value)


## Requirements

    - CMake (Verison 3.5+)
    - gcc
    - Cairo library (Version 1.16.0)


## License

CC0 ("No Rights Reserved" )


## Updates

### Update 2022.02.08

    - Text drawing modification (add linebreak, super & sub-script)
    - Application settings implementation
    - Generation of a log file
    - Title implementation
    
### Update 2022.02.05

    - Transition to CMake
    - Minor modification
    - Text drawing optimization


## ToDo

    - Image import to plot -> maybe need of image processing library !
    - Plot : add AREA, BARS, ERRORBARS, LABELS / add smooth line option
    - Axis : add POLAR & RADAR axis
    - Data : not numerical value implementation / avoid n first lines / data from function
    - Layer Management implementation
    - Graph : fit to contents / Margins or padding option

### In Progress

    - Shape implementation
    - help implementation
    - Write install procedure
    - Add more log information



