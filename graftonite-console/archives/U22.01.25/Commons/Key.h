#ifndef KEY_H
#define KEY_H
/**********************************************/
#include "UString.h"
/**********************************************/
/**********************************************/
/**********************************************/
class Key
{
public:
	enum Keys
	{
		NoKey,
		Name,
		FileName,
		Path,
		NoType,
		Current,
		Graph,
		Layer,
		Frame,
		Axis,
		Plot,
		Legend,
		Title,
		X,
		Y,
		Position,
		Width,
		Height,
		Size,
		Type,
		Format,
		Background,
		Fill,
		Borders,
		Center,
		Bottom,
		Top,
		Right,
		Left,
		Outside,
		Inside,
		BottomLeft,
		BottomRight,
		TopLeft,
		TopRight,
		BaselineLeft,
		Baseline,
		BaselineRight,
		Hide,
		Min,
		Max,
		Scale,
		LinkTo,
		Line,
		Dash,
		Offset,
		Color,
		Join, Round, Miter, Bevel,
		MiterLimit,
		Cap, Butt, Square,
		Ticks,
		Labels,
		Family,
		Weight,
		Slant,
		Underline,
		Capitalize,
		Alignment,
		ScriptOpt,
		Grids,
		Axis1,
		Axis2,
		Axis3,
		General,
		Option,
		Increment,
		Numbers,
		Minor,
		Font,
		Anchor,
		Shift,
		Transform,
		Text,
		Separator,
		Coordinates,
		Data,
		Data1,
		Data2,
		Data3,
		Gap,
		Symbols,
		BeginAngle,
		EndAngle,
		Rotation,
		Covering,
		Area,
		Bars,
		ErrorBars,
		Shape
	};
	static ustring toString( const Key::Keys& key );
	static Key::Keys toKeys( const ustring& keys );
	static UList<Key::Keys> toKeysList( const ustring& key );
	static void setKeysList( const ustring& keys, UList<Key::Keys>& key_list );
	static bool isElementType( const Key::Keys& key );
	//
private:
	UList<Key::Keys> _keys;
	//
public:
	Key( const ustring& keys );
	Key( const Key::Keys& key, const Key::Keys& key2 = Key::NoKey, const Key::Keys& key3 = Key::NoKey );
	Key( const UList<Key::Keys>& keys );
	Key( const UList<Key::Keys>& keys, const Key::Keys& subkey, const Key::Keys& subkey2 = Key::NoKey );
	//
	UList<Key::Keys> keys() const;
	ustring fullKeys() const;
	//
	bool isLineStyle( const Key::Keys& key, const Key::Keys& key2 = Key::NoKey ) const
	{
		UList<Key::Keys> keys;
		if ( key != Key::NoKey )
			keys.append(key);
		if ( key2 != Key::NoKey )
			keys.append(key2);
		return ( *this == Key(keys, Key::Dash) ||
				 *this == Key(keys, Key::Dash, Key::Offset) ||
				 *this == Key(keys, Key::Width) ||
				 *this == Key(keys, Key::Color) ||
				 *this == Key(keys, Key::Cap) ||
				 *this == Key(keys, Key::Join) ||
				 *this == Key(keys, Key::MiterLimit)
				 );
	}
	bool isLineStyle( const UList<Key::Keys>& keys, bool include_cap = true, bool include_join = true ) const
	{
		return ( *this == Key(keys, Key::Dash) ||
				 *this == Key(keys, Key::Dash, Key::Offset) ||
				 *this == Key(keys, Key::Width) ||
				 *this == Key(keys, Key::Color) ||
				 (*this == Key(keys, Key::Cap) && include_cap) ||
				 (*this == Key(keys, Key::Join) && include_join) ||
				 (*this == Key(keys, Key::MiterLimit) && include_join)
				 );
	}
	bool isFillStyle( const Key::Keys& key, const Key::Keys& key2 = Key::NoKey ) const
	{
		UList<Key::Keys> keys;
		if ( key != Key::NoKey )
			keys.append(key);
		if ( key2 != Key::NoKey )
			keys.append(key2);
		return ( *this == Key(keys, Key::Color)
				 );
	}
	bool isFontStyle( const Key::Keys& key, const Key::Keys& key2 = Key::NoKey ) const
	{
		UList<Key::Keys> keys;
		if ( key != Key::NoKey )
			keys.append(key);
		if ( key2 != Key::NoKey )
			keys.append(key2);
		keys.append(Key::Font);
		return ( *this == Key(keys, Key::Family) ||
				 *this == Key(keys, Key::Slant) ||
				 *this == Key(keys, Key::Weight) ||
				 *this == Key(keys, Key::Underline) ||
				 *this == Key(keys, Key::Capitalize) ||
				 *this == Key(keys, Key::Size) ||
				 *this == Key(keys, Key::Color) ||
				 *this == Key(keys, Key::Anchor) ||
				 *this == Key(keys, Key::Alignment) ||
				 *this == Key(keys, Key::ScriptOpt)
				 );
	}
	// Operators
	bool operator<( const Key& key ) const;
	bool operator==( const Key& key ) const;
	bool operator!=( const Key& key ) const;
	bool operator==( const Key::Keys& key ) const;
	bool operator!=( const Key::Keys& key ) const;
	bool operator==( const UList<Key::Keys>& keys ) const;
	friend std::ostream& operator<<( std::ostream& os, const Key& key )
	{
		os << key.fullKeys();
		return os;
	}
};
/**********************************************/
/**********************************************/
//ostream& operator<<(ostream& os, const Key& key)
/**********************************************/
/**********************************************/
/**********************************************/
#endif // KEY_H
