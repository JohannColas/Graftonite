#include "Geometry.h"
/**********************************************/
#include "UString.h"
/**********************************************/
/**********************************************/
/**********************************************/
Point::Point()
{

}
Point::Point( double x, double y )
{
	setX(x);
	setY(y);
}
Point::Point( const Point& point )
{
	setX(point.x());
	setY(point.y());
}
double Point::x() const
{
	return _x;
}
void Point::setX( double x )
{
	_x = x;
}
double Point::y() const
{
	return _y;
}
void Point::setY( double y )
{
	_y = y;
}
/**********************************************/
/**********************************************/
/**********************************************/
Size::Size()
{

}
Size::Size( double width, double height )
{
	setWidth(width);
	setHeight(height);
}
Size::Size( const ustring& size )
{
	if ( size.contains("x") || size.contains(",") )
	{
		UList<ustring> sls = size.split("x");
		if ( sls.size() > 1 )
		{
			_width = sls.at(0).toDouble();
			_height = sls.at(1).toDouble();
		}
	}
	else if ( size.isDouble() || size.isInteger() )
	{
		_width = size.toDouble();
		_height = size.toDouble();
	}
}
double Size::width() const
{
	return _width;
}
void Size::setWidth( double width )
{
	_width = width;
}
double Size::height() const
{
	return _height;
}
void Size::setHeight( double height )
{
	_height = height;
}
/**********************************************/
/**********************************************/
/**********************************************/
Rect::Rect()
{

}
Rect::Rect( double x, double y, double width, double height )
{
	setX(x);
	setY(y);
	setWidth(width);
	setHeight(height);
}
Rect::Rect( const Point& point, const Size& size )
{
	setX(point.x());
	setY(point.y());
	setWidth(size.width());
	setHeight(size.height());
}
double Rect::x() const
{
	return _x;
}
void Rect::setX( double x )
{
	_x = x;
}
double Rect::y() const
{
	return _y;
}
void Rect::setY( double y )
{
	_y = y;
}
Point Rect::point() const
{
	return Point( _x, _y );
}
void Rect::setPoint( double x, double y )
{
	setX(x);
	setY(y);
}
void Rect::setPoint( const Point& point )
{
	setX(point.x());
	setY(point.y());
}
double Rect::width() const
{
	return _width;
}
void Rect::setWidth( double width )
{
	_width = width;
}
double Rect::height() const
{
	return _height;
}
void Rect::setHeight( double height )
{
	_height = height;
}
Size Rect::size() const
{
	return Size( _width, _height );
}
void Rect::setSize( double width, double height )
{
	setWidth(width);
	setHeight(height);
}
void Rect::setSize( const Size& size )
{
	setWidth(size.width());
	setHeight(size.height());
}
/**********************************************/
/**********************************************/
/**********************************************/
