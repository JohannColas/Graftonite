#ifndef STYLES_H
#define STYLES_H
/**********************************************/
#include <cairo/cairo.h>
#include "Key.h"
class Cairo;
/**********************************************/
#include "Geometry.h"
#include "Color.h"
/**********************************************/
/**********************************************/
/**********************************************/
class FontMetrics
{
	double _ascent = -1;
	double _descent = -1;
	double _height = -1;
	double _tight_height = -1;
	double _width = -1;
	double _tight_width = -1;
	double _x_bearing = -1;
	double _y_bearing = -1;
public:
	FontMetrics();
	FontMetrics( cairo_t* drawing );
	FontMetrics( const ustring& text, cairo_t* drawing );
	void init( cairo_t* drawing );
	void init( const ustring& text, cairo_t* drawing );
	double ascent() const;
	void setAscent( double ascent );
	double descent() const;
	void setDescent( double descent );
	double height() const;
	void setHeight( double height );
	double tightHeight() const;
	void setTightHeight( double tightHeight );
	double width() const;
	void setWidth( double width );
	double tightWidth() const;
	void setTightWidth( double tightWidth );
	double xBearing() const;
	void setXBearing( double xBearing );
	double yBearing() const;
	void setYBearing( double yBearing );
};
/**********************************************/
/**********************************************/
/**********************************************/
class TextStyle
{
public:
	// ------------------------------------
	// For capitalization
	// Font size of small cap is equal to :
	// smallcaps_font_size = 0.8 * original_font_size + 0.1
	// char value of a = 97 / z = 122 and of A = 65 / Z = 90 -> Thus remove 32
	// ------------------------------------
	enum Capitalization
	{
		NoCap,
		SmallCap,
		Cap
	};
	enum Align
	{
		Center,
		Justify,
		Left,
		Right
	};
	enum ScriptOpt
	{
		NormalScript,
		SuperScript,
		SubScript,
	};
private:
	ustring _family = "Sans Serif";
	cairo_font_slant_t _slant = CAIRO_FONT_SLANT_NORMAL;
	cairo_font_weight_t _weight = CAIRO_FONT_WEIGHT_NORMAL;
	Capitalization _cap = NoCap;
	double _size = 10;
	Color _color;
	Anchor _anchor = Key::Left;
	Align _align = Align::Left;
	ScriptOpt _scriptopt = TextStyle::NormalScript;
	bool _underline = false;
public:
	TextStyle();
	TextStyle( const ustring& style );
	FontMetrics metrics( const ustring& text, cairo_t* drawing ) const;
	FontMetrics metrics( const ustring& text, Cairo& drawing ) const;
	FontMetrics metrics( const ustring& text, Cairo* drawing ) const;
	void set( const ustring& style );
	ustring family() const;
	void setFamily( const ustring& family );
	cairo_font_slant_t slant() const;
	void setSlant( const cairo_font_slant_t& slant );
	void setSlant( const ustring& slant );
	cairo_font_weight_t weight() const;
	void setWeight( const cairo_font_weight_t& weight );
	void setWeight( const ustring& weight );
	bool underline() const;
	void setUnderline( bool underline );
	Capitalization cap() const;
	void setCap( const Capitalization& cap );
	void setCap( const ustring& cap );
	double size() const;
	void setSize( double size );
	Color color() const;
	void setColor( const Color& color );
	Anchor anchor() const;
	void setAnchor( const Anchor& anchor );
	void setAnchor( const Key::Keys& anchor );
	void setAnchor( const ustring& anchor );
	Align align() const;
	void setAlign( const Align& align );
	void setAlign( const ustring& align );
	ScriptOpt scriptOpt() const;
	void setScriptOpt( const ScriptOpt& scriptopt );
	void setScriptOpt( const ustring& scriptopt );
	TextStyle capStyle() const;
};
/**********************************************/
/**********************************************/
/**********************************************/
class Label
{
	ustring _text;
	// Input Position
	Point _pos;
	// Position of the baseline,
	// calculate in prepare(...)
	Point _text_pos;
	// Anchor
	Anchor _anchor;
	FontMetrics _metrics;
	Rect _boundingRect;
	//
public:
	Label();
	Label( const ustring& text, const Point& pos, const Anchor& anchor );
	//
	ustring text() const;
	void setText( const ustring& text );
	Point pos() const;
	void setPos( const Point& pos );
	Point textPos() const;
	Anchor anchor() const;
	void setAnchor( const Anchor& anchor );
	FontMetrics metrics() const;
	Rect boundingRect() const;
	void prepare( cairo_t*  drawing, const TextStyle& style = TextStyle() );
	void prepare( Cairo*  drawing, const TextStyle& style = TextStyle() );
	void draw( Cairo& drawing, const TextStyle& style = TextStyle() );
	//
};
/**********************************************/
/**********************************************/
/**********************************************/
class StyledText
{
	ustring _text;
	TextStyle _style;
	// Input Position
	Point _pos;
	// Position of the baseline,
	// calculate in prepare(...)
	Point _text_pos;
	// Anchor
	Anchor _anchor;
	FontMetrics _metrics;
	Rect _boundingRect;
public:
	StyledText();
	StyledText( const ustring& text, const TextStyle& style );
	ustring text() const;
	void setText( const ustring& text );
	TextStyle style() const;
	void setStyle( const TextStyle& style );
	Point pos() const;
	void setPos( const Point& pos );
	Point textPos() const;
	Anchor anchor() const;
	void setAnchor( const Anchor& anchor );
	FontMetrics metrics() const;
	Rect boundingRect() const;
	void prepare( Cairo* drawing );
};
/**********************************************/
/**********************************************/
/**********************************************/
class LineStyle
{
	// Line cap (CAIRO_LINE_CAP_BUTT, CAIRO_LINE_CAP_ROUND, CAIRO_LINE_CAP_SQUARE)
	UList<double> _dash;
	double _dash_offset = 0.0;
	double _width = 3.0;
	Color _color;
	cairo_line_join_t _join = CAIRO_LINE_JOIN_BEVEL;
	double _miter_limit = 10.0;
	cairo_line_cap_t _cap = CAIRO_LINE_CAP_BUTT;
	double _gap = 0.0;
public:
	LineStyle();
	UList<double> dash() const;
	void setDash( const UList<double>& dash );
	double dashOffset() const;
	void setDashOffset( double dashoffset );
	double width() const;
	void setWidth( double width );
	Color color() const;
	void setColor( const Color& color );
	cairo_line_join_t join() const;
	void setJoin( const cairo_line_join_t& join );
	void setJoin( const ustring& join );
	double miterLimit() const;
	void setMiterLimit( double miter_limit );
	cairo_line_cap_t cap() const;
	void setCap( const cairo_line_cap_t& cap );
	void setCap( const ustring& cap );
	double gap() const;
	void setGap( double gap );
};
/**********************************************/
/**********************************************/
/**********************************************/
class FillStyle
{
public:
	enum Type { Flat, LinearGradient, RadialGradient };
private:
	FillStyle::Type _type = FillStyle::Flat;
	Color _color = { 0, 0, 0, 0 };
	cairo_pattern_t* _linear_gradient;
	cairo_pattern_t* _radial_gradient;
public:
	FillStyle();
	Type type() const;
	void setType( const FillStyle::Type& type );
	Color color() const;
	void setColor( const Color& color );
	cairo_pattern_t* linearPattern() const;
	void setLinearPattern( cairo_pattern_t* linear_gradient );
	cairo_pattern_t* radialPattern() const;
	void setRadialPattern( cairo_pattern_t* radial_gradient );
};
/**********************************************/
/**********************************************/
/**********************************************/
class ShapeStyle
{
public:
	enum Shape
	{
		Cercle,
		Rectangle,
		Star,
		Plus,
		Minus
	};
private:
	FillStyle _fill;
	LineStyle _line;
	Key::Keys _anchor;
public:
	ShapeStyle();
	FillStyle fill() const;
	void setFill( const FillStyle& fill );
	LineStyle line() const;
	void setLine( const LineStyle& line );
	Key::Keys anchor() const;
	void setAnchor( const Key::Keys& anchor );
};
/**********************************************/
/**********************************************/
/**********************************************/
class Symbol
{
public:
	enum Symbols
	{
		Cercle,
		HalfCercle,
		Square,
		Rectangle,
		Triangle,
		Polygon,
		Diamond,
		Hourglass,
		Boomerang,
		Star,
		Plus,
		Minus,
		Line,
		Text
	};
	/**********************************************/
private:
	Symbol::Symbols _type = Symbol::Cercle;
	ustring _option;
	Size _size = {12,12};
	Size _option_size;
	double _begin_angle = 0.0;
	double _end_angle = 360.0;
	double _rotation = 360.0;
	double _covering = 60.0;
	/**********************************************/
public:
	Symbol();
	Symbol( const Symbol::Symbols& type );
	/**********************************************/
	Symbol::Symbols type() const;
	void setType( const Symbol::Symbols& type );
	void setType( const ustring& type );
	ustring option() const;
	void setOption( const ustring& option );
	Size size() const;
	void setSize( const Size& size );
	void setSize( const ustring& size );
	Size optionSize() const;
	void setOptionSize( const Size& size );
	void setOptionSize( const ustring& size );
	double beginAngle() const;
	void setBeginAngle( double angle );
	double endAngle() const;
	void setEndAngle( double angle );
	double rotation() const;
	void setRotation( double rotation );
	double covering() const;
	void setCovering( double covering );
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#include <cmath>
#include <ctime>
#include <iomanip>
#include <sstream>
class Format
{
public:
	enum Type
	{
		Default,
		Number,
		Scientific,
		Product,
//		Date,
		Time,
//		TimeDate,
		Fraction,
		Text
	};
private:
	Format::Type _type = Format::Product;
	// Number Format Settings
	bool _forceSign = false;
	ustring _decimalMarker = ","; // '.'
	int _nbDecimal = 2; // '.'
	bool _forceDecimal = true;
	int _nbIntegerDigits = 8;
	ustring _integerFiller = "0";
	// "fs;dm:.;nd:2;fd;ni:8"
	//
	ustring _productBase = "e"; // e, 2, pi, phi,...
	ustring _productMarker = "times"; // "", ".", "dot", "times",...
	// "pb:10;pm:x"
	ustring _exponentBase = "10"; // e, 2, pi, phi,...
	ustring _exponentMaker = "10"; // E, D, e, 2, pi, phi,..
	double _fixedExponent = 0; //
	bool _forceExponentSign = false;
	bool _tightSpacing = false;
	bool _hideUnityMantissa = true;
	bool _hideZeroExponent = true;
	// "em:10;eb:10;fe:8;fes;ts;sum;sze;"
	double _scale = 1.0;
	// "sc:1.0;"
	// Date Format Settings
	ustring _dateSeparator = "/"; // " ", ":", ...
//	ustring _dateOrder = "%d%m%y";
//	ustring _dateFormat = "%d/%m/%Y";
	ustring _timeFormat = "%d/%m/%Y";
//	ustring _Format = "%d/%m/%Y";
	// "ds:/;do:dmy;"
	// DD/MM/YY
	// DD/MM/YYYY
	// DD MMMM YYYY
	// D MMMM YYYY
	// NN D MM YYYY
	// NN D MM YYYY
	// NNNN D MMMM YYYY
	// Time Format Settings
//	ustring _dateformat = "%d/%m/%Y"; // "%u"
//	ustring _timeSeparator = ":"; // "%u"
//	bool _am_pm = false; //
	// "ts::;"
	// HH:MM:SS AM/PM
	// HH:MM:SS
	// Fraction Format Settings
	// ?/?
	// Text Format Settings
	// @

public:
	Format()
	{
	}
	Format( const ustring& format )
	{
		UList<ustring> args = format.split(";");
		for ( const ustring& arg : args )
			setOption( arg );
	}
	void setOption( const ustring option )
	{
		UList<ustring> values = option.split(":");
		if ( !values.isEmpty() )
		{
			ustring first_val = values.first();
			// ty : _type
			if ( first_val == "ty" && values.size() > 1 )
			{
				setType( values.at(1) );
			}
			// fs : _forceSign
			else if ( first_val == "fs" )
			{
				_forceSign = true;
			}
			// dm :_decimalMarker
			else if ( first_val == "dm" && values.size() > 1 )
			{
				_decimalMarker = values.at(1);
			}
			// nd : _nbDecimal
			else if ( first_val == "nd" && values.size() > 1 )
			{
				_nbDecimal = std::stoi( values.at(1) );
			}
			// fd : _forceDecimal
			else if ( first_val == "fd" )
			{
				_forceDecimal = true;
			}
			// ni : _nbIntegerDigits
			else if ( first_val == "ni" && values.size() > 1 )
			{
				_nbIntegerDigits = std::stoi( values.at(1) );
			}
			// pb : _productBase
			else if ( first_val == "pb" && values.size() > 1 )
			{
				_productBase = values.at(1);
			}
			// pm :_productMarker
			else if ( first_val == "pm" && values.size() > 1 )
			{
				_productMarker = values.at(1);
			}
			// eb : _exponentBase
			else if ( first_val == "eb" && values.size() > 1 )
			{
				_exponentBase = values.at(1);
			}
			// em :_exponentMaker
			else if ( first_val == "em"  && values.size() > 1)
			{
				_exponentMaker = values.at(1);
			}
			// fe : _fixedExponent
			else if ( first_val == "fe" && values.size() > 1 )
			{
				_fixedExponent = std::stoi( values.at(1) );
			}
			// fes : _forceExponentSign
			else if ( first_val == "fes" )
			{
				_forceExponentSign = true;
			}
			// ts : _tightSpacing
			else if ( first_val == "ts" )
			{
				_tightSpacing = true;
			}
			// hum : _hideUnityMantissa
			else if ( first_val == "hum" )
			{
				_hideUnityMantissa = true;
			}
			// hze : _hideZeroExponent
			else if ( first_val == "hze" )
			{
				_hideZeroExponent = true;
			}
			// sc : _scale
			else if ( first_val == "sc" && values.size() > 1 )
			{
				_scale = std::stod( values.at(1) );
			}
			// tf : _timeFormat
			else if ( first_val == "tf" && values.size() > 1 )
			{
				_timeFormat = std::stod( values.at(1) );
			}
		}
	}
	void setType( const Format::Type& type )
	{
		_type = type;
		if ( _type == Format::Scientific || _type == Format::Product )
			_nbIntegerDigits = 0;
	}
	void setType( const ustring& type )
	{
		if ( type == "n" )
			setType( Format::Number );
		else if ( type == "p" )
			setType( Format::Product );
		else if ( type == "s" )
			setType( Format::Scientific );
		else if ( type == "@" )
			setType( Format::Text );
//		else if ( type == "d" )
//			setType( Format::Date );
		else if ( type == "t" )
			setType( Format::Time );
//		else if ( type == "dt" )
//			setType( Format::TimeDate );
		else
			setType( Format::Default );
	}
//	static inline ustring convert( double number, const ustring& format )
//	{
//		return Format::convert( number, Format(format) );
//	}
	static inline ustring convert( double number, const Format& format )
	{
		return (format.convert( number ));
	}
	ustring convert( double number ) const
	{
		number = number/_scale;
		int _exponent = 0;
		if ( _type == Format::Number )
		{

		}
		if ( _type == Format::Scientific )
		{
			double base;
			if ( _exponentBase == "pi" )
				base = M_PI;
			else if ( _exponentBase == "e" )
				base = M_E;
			else
				base = stod(_exponentBase);
			if ( _fixedExponent != 0 )
			{
				number = number/(pow(base, _fixedExponent));
				_exponent = _fixedExponent;
			}
			else
			{
				double tmp_nb = number;
				if ( tmp_nb < 0 )
					tmp_nb *= -1;
				while ( tmp_nb > 10 )
				{
					tmp_nb = tmp_nb/base;
					++_exponent;
				}
				while ( tmp_nb < 1 )
				{
					tmp_nb *= base;
					--_exponent;
				}
				number = tmp_nb;
			}
		}
		else if ( _type == Format::Product )
		{
			double base;
			if ( _productBase == "pi" )
				base = M_PI;
			else if ( _productBase == "e" )
				base = M_E;
			else
				base = stod(_productBase);
			number = number/base;
		}
		ustring tmp = std::to_string( number );
		int ind = tmp.find('.');
		ustring integers = tmp.substr(0,ind);
		ustring decimals = tmp.substr(ind+1);
		if ( _nbIntegerDigits > (int)integers.size() )
		{
			for ( int it = integers.size(); it < _nbIntegerDigits; ++it )
				integers = "0" + integers;
		}
		if ( _forceDecimal )
		{
			for ( int it = decimals.size(); it < _nbDecimal; ++it )
				decimals += "0";
			while ( (int)decimals.size() > _nbDecimal )
				decimals = decimals.replace(decimals.size()-1, 1, "");
		}
		tmp = integers + _decimalMarker + decimals;
		if ( _forceSign && number >= 0)
			tmp = '+' + tmp;
		ustring pm = _productMarker;
		if ( pm == "dot" )
		{
			pm = "·";
		}
		else if ( pm == "times" )
		{
			pm = "×";
		}
		if ( !_tightSpacing )
		{
			pm = " " + pm + " ";
		}
		if ( _type == Format::Scientific )
		{
			bool unityIsHidden = false;
			if ( _hideUnityMantissa && abs(stoi(integers)) == 1 && stoi(decimals) == 0 )
			{
				tmp = "";
				if ( number < 0)
					tmp = '-' + tmp;
				else if ( _forceSign && number >= 0)
					tmp = '+' + tmp;
				unityIsHidden = true;
			}
			if ( !(_hideZeroExponent && _exponent == 0) )
			{
				if ( !unityIsHidden )
					tmp += pm;
				tmp += _exponentBase +"^";
				if ( _forceExponentSign && _exponent >= 0 )
					tmp += "+";
				tmp += std::to_string(_exponent);
			}
		}
		else if ( _type == Format::Product )
		{
			tmp += pm + _productBase;
		}
		else if ( _type == Format::Time )
		{
			auto t = std::time(nullptr);
			auto tm = *std::localtime(&t);
//			tm.tm_mday = 299823;
			std::ostringstream oss;
			oss << std::put_time(&tm, _timeFormat.c_str());
			return oss.str();
		//	std::time_t t = std::mktime(&tm);
//			oss << "UTC:   " << std::put_time(std::gmtime(&t), "%T") << '\n';
//			oss << "local: " << std::put_time(std::localtime(&t), "%T") << '\n';
			////	%a	Abbreviated weekday name *	Thu
			////	%A	Full weekday name * 	Thursday
			////	%b	Abbreviated month name *	Aug
			////	%B	Full month name *	August
			////	%C	Year divided by 100 and truncated to integer (00-99)	20
			////	%d	Day of the month, zero-padded (01-31)	23
			////	%D	Short MM/DD/YY date, equivalent to %m/%d/%y	08/23/01
			////	%e	Day of the month, space-padded ( 1-31)	23
			////	%F	Short YYYY-MM-DD date, equivalent to %Y-%m-%d	2001-08-23
			////	%g	Week-based year, last two digits (00-99)	01
			////	%G	Week-based year	2001
			////	%h	Abbreviated month name * (same as %b)	Aug
			////	%j	Day of the year (001-366)	235
			////	%m	Month as a decimal number (01-12)	08
			////	%u	ISO 8601 weekday as number with Monday as 1 (1-7)	4
			////	%U	Week number with the first Sunday as the first day of week one (00-53)	33
			////	%V	ISO 8601 week number (01-53)	34
			////	%w	Weekday as a decimal number with Sunday as 0 (0-6)	4
			////	%W	Week number with the first Monday as the first day of week one (00-53)	34
			////	%x	Date representation *	08/23/01
			////	%y	Year, last two digits (00-99)	01
			////	%Y	Year	2001
		}
//		else if ( _type == Format::Time )
//		{
//			////	%H	Hour in 24h format (00-23)	14
//			////	%I	Hour in 12h format (01-12)	02
//			////	%M	Minute (00-59)	55
//			////	%p	AM or PM designation	PM
//			////	%r	12-hour clock time *	02:55:02 pm
//			////	%R	24-hour HH:MM time, equivalent to %H:%M	14:55
//			////	%S	Second (00-61)	02
//			////	%T	ISO 8601 time format (HH:MM:SS), equivalent to %H:%M:%S	14:55:02
//			////	%X	Time representation *	14:55:02
//		}
//		else if ( _type == Format::TimeDate )
//		{
//			////	%c	Date and time representation *	Thu Aug 23 14:55:02 2001
//			////	%z	ISO 8601 offset from UTC in timezone (1 minute=1, 1 hour=100)
//			////	If timezone cannot be determined, no characters	+100
//			////	%Z	Timezone name or abbreviation *
//			////	If timezone cannot be determined, no characters	CDT
//			////	%%	A % sign	%
//		}
		return tmp;
	}
	ustring convert( int number )
	{
		ustring tmp = std::to_string( number );
		if ( _nbIntegerDigits > (int)tmp.size() )
		{
			for ( int it = tmp.size(); it < _nbIntegerDigits; ++it )
				tmp = _integerFiller + tmp;
		}
//		if ( _forceDecimal && _nbDecimal > 0 )
//		{
//			tmp += _decimalMaker;
//			for ( int it = 0; it < _nbDecimal; ++it )
//				tmp += "0";
//		}
		if ( _forceSign && number >= 0)
			tmp = '+' + tmp;
		return tmp;
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // STYLES_H
