#include "UString.h"
/**********************************************/
#include "Styles.h"
#include "Key.h"
/**********************************************/
/**********************************************/
ustring::ustring() : std::string()
{

}
/**********************************************/
ustring::ustring( const double d ) : std::string()
{
	ustring strd = std::to_string(d);
	char dec = 0;
	if ( strd.contains(".") ) dec = '.';
	else if ( strd.contains(",") ) dec = ',';
	while ( strd.back() == '0' )
		strd.pop_back();
	if ( strd.back() == dec )
		strd.pop_back();
	if ( strd == "-0" )
		strd = "0";
	(*this) = strd;
}
ustring::ustring( const char c ) : std::string()
{
	(*this) += c;
}
/**********************************************/
ustring::ustring( const int i ) : std::string()
{
	ustring strd = std::to_string(i);
	if ( strd == "-0" )
		strd = "0";
	(*this) = strd;
}
/**********************************************/
ustring::ustring( const unsigned u ) : std::string()
{
	ustring strd = std::to_string(u);
	if ( strd == "-0" )
		strd = "0";
	(*this) = strd;
}
/**********************************************/
ustring::ustring( const long l ) : std::string()
{
	ustring strd = std::to_string(l);
	if ( strd == "-0" )
		strd = "0";
	(*this) = strd;
}
/**********************************************/
ustring::ustring( const char* c_str ) : std::string(c_str)
{

}
/**********************************************/
ustring::ustring( const std::string& str ) : std::string(str)
{

}
/**********************************************/
ustring::ustring( const ustring& str ) : std::string(str)
{

}
/**********************************************/
/**********************************************/
bool ustring::contains( const std::string& str ) const
{
	return (this->find(str) != std::string::npos);
}
/**********************************************/
/**********************************************/
bool ustring::contains( const char* c_str ) const
{
	return (this->find(c_str) != std::string::npos);
}
/**********************************************/
/**********************************************/
ustring ustring::remove( const ustring& str ) const
{
	ustring tmp;
	for ( unsigned long it = 0; it < this->length(); ++it )
	{
		ustring tmp2 = this->substr(it, str.size());
		if ( tmp2 != str )
			tmp += this->at(it);
	}
	return tmp;
}
/**********************************************/
/**********************************************/
Color ustring::toColor() const
{
	return Color(*this);
}
/**********************************************/
/**********************************************/
UList<ustring> ustring::split( const ustring& separator, bool keepEmptyParts ) const
{
	UList<ustring> list;
	unsigned long ind0 = 0,
			ind1,
	        strsz = separator.size();
	do
	{
		ind1 = this->find( separator, ind0 );
		std::string tmp = this->substr(ind0, ind1-ind0);

		if ( !tmp.empty() || (tmp.empty() && keepEmptyParts) )
			list.append(tmp);

		ind0 = ind1+strsz;
	}
	while ( ind1 != std::string::npos );

	return list;
}
UList<ustring> ustring::decomposeCap() const
{
	UList<ustring> list;
	ustring tmp;
	char old_c;
	for ( char c : *this )
	{
		if ( (('a' <= c && c <= 'z') && (old_c < 'a' || 'z' < old_c)) ||
		     (('a' <= old_c && old_c <= 'z') && (c < 'a' || 'z' < c)) )
		{
			if ( !tmp.empty() )
			{
				list.append( tmp );
				tmp.clear();
			}
		}
		tmp += c;
		old_c = c;
	}
	if ( !tmp.empty() )
		list.append( tmp );
	return list;
}
/**********************************************/
/**********************************************/
std::ostream& operator<<( std::ostream& os, const ustring& str )
{
	for ( auto c : str )
		os << c;
	return os;
}
/**********************************************/
/**********************************************/
