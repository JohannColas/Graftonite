#ifndef GRAPH_H
#define GRAPH_H
/**********************************************/
#include "Element.h"
#include "Layer.h"
#include "Frame.h"
#include "Axis.h"
#include "Plot.h"
#include "Legend.h"
#include "Title.h"
#include "Data.h"
#include "Commons/Cairo.h"
/**********************************************/
/**********************************************/
/**********************************************/
class Graph : public Element
{
	ustring name = "";
	Cairo::Format _format = Cairo::PNG;
	double width = 1200;
	double height = 800;
	/**********************************************/
	Element* _currentEl = 0;
	/**********************************************/
	UList<Layer*> _layers;
	UList<Frame*> _frames;
	UList<Axis*> _axes;
	UList<Plot*> _plots;
	UList<Legend*> _legends;
	UList<Title*> _titles;
	UList<Data*> _data;
	/**********************************************/
public:
	~Graph();
	Graph();
	/**********************************************/
	Layer* layer( const ustring& name );
	UList<Layer*> layers();
	void clearLayers()
	{
		while ( !_layers.isEmpty() )
			delete _layers.takeFirst();
	}
	Frame* frame( const ustring& name );
	UList<Frame*> frames();
	void clearFrames()
	{
		while ( !_frames.isEmpty() )
			delete _frames.takeFirst();
	}
	Axis* axis( const ustring& name );
	UList<Axis*> axes();
	void clearAxes()
	{
		while ( !_axes.isEmpty() )
			delete _axes.takeFirst();
	}
	Plot* plot( const ustring& name );
	UList<Plot*> plots();
	void clearPlots()
	{
		while ( !_plots.isEmpty() )
			delete _plots.takeFirst();
	}
	Legend* legend( const ustring& name );
	UList<Legend*> legends();
	void clearLegends()
	{
		while ( !_legends.isEmpty() )
			delete _legends.takeFirst();
	}
	Title* title( const ustring& name );
	UList<Title*> titles();
	void clearTitles()
	{
		while ( !_titles.isEmpty() )
			delete _titles.takeFirst();
	}
	Data* data( const ustring& name );
	UList<Data*> datalst();
	void clearData()
	{
		while ( !_data.isEmpty() )
			delete _data.takeFirst();
	}
	Element* element( const Key::Keys& type, const ustring& name );
	Element* element( UList<ustring>& args );
	UList<Element*> elements( const Key::Keys& type );
	/**********************************************/
	void newElement( const Key::Keys& type = Key::NoType, const ustring& name = ustring(), bool log = false );
	void removeElement( const Key::Keys& type = Key::NoType, const ustring& name = ustring() );
	void setCurrent( UList<ustring>& args );
	bool set( const Key& key, const ustring& value ) override;
	bool set( UList<ustring>& args, bool log = false ) override;
	bool reset( UList<ustring>& args, bool log = false ) override;
	void unset( const UList<ustring>& args, bool log = false ) override;
	void showAll( const Key::Keys& type = Key::NoType, const ustring& name = ustring() );
	void showCurrent();
	void prepare( Cairo* drawing ) override;
	void draw( Cairo* drawing ) override;
	void exportGraph( UList<ustring>& args );
	/**********************************************/
	ustring _exportpath = "";
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // GRAPH_H
