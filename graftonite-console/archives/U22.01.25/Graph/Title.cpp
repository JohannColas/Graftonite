#include "Title.h"
/**********************************************/
/**********************************************/
/**********************************************/
Title::Title( Graph* parent )
	: Element(parent)
{
	init();
}
Title::Title(const ustring& name, unsigned index, Graph* parent, bool log)
	: Element(parent)
{
	init();
	Element::init( name, index, log );
}
void Title::init()
{
	_type = Key::Title;
}
/**********************************************/
/**********************************************/
bool Title::set( const Key& key, const ustring& value )
{
	if ( key == Key::Name )
	{
		return Element::set( key, value );
	}
	return false;
}
/**********************************************/
/**********************************************/
void Title::draw( Cairo* /*drawing*/ )
{

}
/**********************************************/
/**********************************************/
