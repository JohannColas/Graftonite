#ifndef ELEMENT_H
#define ELEMENT_H
/**********************************************/
#include <map>
#include <deque>
#include <string>
/**********************************************/
#include "Commons/Key.h"
#include "Commons/UList.h"
/**********************************************/
//#include "Commons/Geometry.h"
#include "Commons/Styles.h"
class Graph;
class Cairo;
//class LineStyle;
//class FillStyle;
//class ShapeStyle;
//class TextStyle;
/**********************************************/
/**********************************************/
/**********************************************/
class Element
{
public:
	/**********************************************/
	std::map<Key,ustring> _settings;
	Key::Keys _type = Key::NoType;
	unsigned _index = -1;
	Graph* _parent = nullptr;
	Rect _boundingRect;
	/**********************************************/
public:
	virtual ~Element();
	Element( Graph* parent = nullptr );
	void init( const ustring& name, unsigned index, bool log = false );
	/**********************************************/
	Key::Keys type() const;
	ustring typeString() const;
	void setType( const Key::Keys& type );
	unsigned index() const { return _index; }
	void setIndex( unsigned index) { _index = index; }
	static Key::Keys getType( const ustring& cmd );
	LineStyle getLineStyle( const UList<Key::Keys>& firstKeys = {Key::Line} ) const;
	FillStyle getFillStyle( const UList<Key::Keys>& firstKeys = {Key::Background} ) const;
	ShapeStyle getShapeStyle( const UList<Key::Keys>& firstKeys = {Key::Line} ) const;
	TextStyle getTextStyle( const Key::Keys& firstKeys ) const;
	/**********************************************/
	bool hasKey( const Key& key ) const;
	ustring get( const ustring& key ) const;
	ustring get( const Key& key ) const;
	ustring get( const Key::Keys& key, const Key::Keys& key2 = Key::NoKey, const Key::Keys& key3 = Key::NoKey ) const;
	ustring get( const UList<Key::Keys>& key ) const;
	Key getKey( const Key::Keys& key, const Key::Keys& key2 = Key::NoKey, const Key::Keys& key3 = Key::NoKey ) const;
	virtual bool set( const Key& key, const ustring& value );
	bool set( const Key::Keys& key, const ustring& value );
	bool set( const UList<Key::Keys>& key, const ustring& value );
	bool set( const ustring& key, const ustring& value );
	virtual bool set( UList<ustring>& args, bool log = false );
	virtual bool reset( UList<ustring>& args, bool log = false );
	virtual bool unset( const ustring& key );
	virtual void unset( const UList<ustring>& args, bool log = false );
	virtual void remove( const ustring& key );
	void clear();
	/**********************************************/
	void show();
	void shortShow();
	virtual void prepare( Cairo* drawing );
	virtual void draw( Cairo* drawing );
	/**********************************************/
	Rect boundingRect() const;
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ELEMENT_H
