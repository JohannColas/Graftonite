#ifndef FRAME_H
#define FRAME_H
/**********************************************/
#include "Element.h"
/**********************************************/
/**********************************************/
/**********************************************/
class Frame : public Element
{
	ShapeStyle _style;
public:
	Frame( Graph* parent = nullptr );
	Frame( const ustring& name, unsigned index, Graph* parent = nullptr, bool log = false );
	void init();
	/**********************************************/
	bool set( const Key& key, const ustring& value ) override;
	void prepare( Cairo* drawing ) override;
	void draw( Cairo* drawing ) override;
	/**********************************************/
	ShapeStyle style() const
	{
		return _style;
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // FRAME_H
