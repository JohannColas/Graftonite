#ifndef AXIS_H
#define AXIS_H
/**********************************************/
#include "Element.h"
#include <vector>
/**********************************************/
/**********************************************/
/**********************************************/
class Axis : public Element
{
	Rect _frameRect = {100, 100, 1200, 800};
	ustring name = "axis";
	double _min = 0;
	double _max = 1;
	double _scaleA = 0.1;
	double _scaleB = 0;
	double _tickinterval = 0.1;
	double _minortickinterval = 0.05;
	Line _line;
	UList<Line> _ticks;
	UList<Line> _minor_ticks;
	LineStyle _line_style;
	UList<Label> _labels;
	TextStyle _Labels_style;
	Label _title;
	TextStyle _title_style;
	UList<Line> _girds;
	LineStyle _grids_style;
	UList<Line> _minor_grids;
	LineStyle _minor_grids_style;
	//
public:
	~Axis()
	{
//		std::cout << "axis dtr : " << get(Key::Name) << std::endl;
	}
	Axis( Graph* parent = nullptr );
	Axis( const ustring& name, unsigned index, Graph* parent = nullptr, bool log = false );
	void init();
	/**********************************************/
	bool set( const Key& key, const ustring& value ) override;
	void prepare( Cairo* drawing ) override;
	void draw( Cairo* drawing ) override;
	/**********************************************/
	void CalculateTicksPosition( UList<double>& tickspos, UList<double>& minortickspos, UList<ustring>& labels, bool isYaxis = false );
	double round( double value );
	void CalculateScaleCoef();
	double GetGraphCoord( double coord );
	/**********************************************/
	Rect frameRect() const
	{
		return _frameRect;
	}
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // AXIS_H
