#ifndef PLOT_H
#define PLOT_H
/**********************************************/
#include "Element.h"

#include "Commons/Styles.h"
/**********************************************/
/**********************************************/
/**********************************************/
class Plot
		: public Element
{
public:
	Plot( Graph* parent = nullptr );
	Plot( const ustring& name, unsigned index, Graph* parent = nullptr, bool log = false );
	void init();
	/**********************************************/
	bool set( const Key& key, const ustring& value ) override;
	void prepare( Cairo* drawing ) override;
	void draw( Cairo* drawing ) override;
	/**********************************************/
	LineStyle line_style;
	double line_gap = 0;
	Symbol symbol;
	ShapeStyle symbol_style;
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PLOT_H
