#include "Legend.h"
/**********************************************/
#include "Commons/Cairo.h"
/**********************************************/
#include "Graph.h"
/**********************************************/
/**********************************************/
Legend::Legend( Graph* parent )
	: Element(parent)
{
	init();
}
Legend::Legend( const ustring& name, unsigned index, Graph* parent, bool log )
	: Element(parent)
{
	init();
	Element::init( name, index, log );
}
void Legend::init()
{
	_type = Key::Legend;
}
/**********************************************/
/**********************************************/
bool Legend::set( const Key& key, const ustring& value )
{
	if ( key == Key::Name )
	{

		return Element::set( key, value );
	}
	return false;
}
/**********************************************/
/**********************************************/
void Legend::draw( Cairo* drawing )
{
	double scale = 1;
	Point p1 = {100,40};
	Point p2 = {150,40};
	Point c = {75,40};
	Rect rec = {100,100,50,20};

	drawing->saveState();
	drawing->scale( scale );

	Plot* plot = _parent->plot("0");
	if ( plot )
	{
		double gap = plot->line_gap;
		LineStyle style = plot->line_style;
		if ( gap > 0 )
		{
//			drawing->drawPath( {p1,{c.x()-gap, p1.y()}}, style );
//			drawing->drawPath( {{c.x()+gap, p1.y()},p2}, style );
			drawing->drawLine( p1, {c.x()-gap, p1.y()}, style );
			drawing->drawLine( {c.x()+gap, p1.y()}, p2, style );
		}
		else
//			drawing->drawPath( {p1,p2}, style );
		drawing->drawLine( p1, p2, style );

		Symbol symbol = plot->symbol;
		ShapeStyle symbol_style = plot->symbol_style;
		drawing->drawSymbols( symbol, c, symbol.size(), symbol_style );

		TextStyle tsty;
		tsty.setAnchor( Key::Center );
		tsty.setSize( 24 );
		drawing->drawText( "POppppp", p2.x()+5, p2.y(), tsty );
	}
	else
		std::cout << "plot not find !" << std::endl;
	drawing->restoreState();
}
/**********************************************/
/**********************************************/
