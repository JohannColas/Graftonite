#include "Graftonite.h"
/**********************************************/
#include "Commons/UConsole.h"
/**********************************************/
#include "Commons/File.h"
#include "Commons/System.h"
/**********************************************/
/**********************************************/
/**********************************************/
Graftonite::Graftonite( int argc, char *argv[] )
{
	// ---------------------
	// TO FINISH
	std::cout << "\e[1m"; // Bold Text
	std::cout << std::endl << "----------------------------" << std::endl;
	std::cout << std::endl << "\x1B[34m" << " Welcome to Graftonite !" << std::endl;
	UConsole::normalColor();
	std::cout << "\e[1m"; // Bold Text
	std::cout << std::endl << "----------------------------" << std::endl;
	UConsole::normalColor();
	std::cout << "\e[0m";
	// ---------------------

	UList<ustring> sys_args;
	for ( int it = 1; it < argc; ++it )
		sys_args.append( argv[it] );

	bool open_interface = true;
	if ( !sys_args.isEmpty() )
	{
		ustring cmd = sys_args.takeFirst();
		if ( cmd.contains("help") || cmd == "-h" )
		{
			//			help();
			open_interface = false;
		}
		else if ( cmd.contains("load") || cmd == "-l" )
		{
			ustring arg = argv[2];
			load( arg.split("#") );
		}
		else if ( cmd.contains("run") || cmd == "-r t" )
		{
			ustring arg = argv[2];
			runCommand(arg);
			open_interface = false;
		}
	}
	if ( open_interface )
		std::cout << std::endl;
}
/**********************************************/
int Graftonite::exec()
{
	UConsole::exec( this );
	exit();
	return 0;
}
/**********************************************/
/**********************************************/
void Graftonite::decomposeCommandLine( const ustring& cmdline, ustring& cmd, UList<ustring>& args )
{
	args.clear();
	unsigned long len = cmdline.length();
	ustring args_tmp;
	unsigned it;
	for ( it = 0; it < cmdline.size(); ++it  )
	{
		char c = cmdline.at(it);
		if ( c == ' ' )
		{
			if ( cmd.empty() )
				continue;
			break;
		}
		else
			cmd += c;
	}

	bool argMode = true;
	char limChar = 0;
	ustring arg;
	for ( ; it < len; ++it)
	{
		char c = cmdline.at(it);
		if ( c == ' ' && !limChar )
		{
			if ( argMode )
			{
				if ( !arg.empty() )
					args.append( arg );
				arg.clear();
			}
			argMode = false;
			continue;
		}
		else if ( c == '\"' || c == '\'' || c == '<' || c == '>' )
		{
			if ( c == limChar || (limChar == '<' && c == '>') )
			{
				limChar = 0;
				if ( !arg.empty() )
					args.append( arg );
				arg.clear();
				continue;
			}
			else if ( !limChar )
			{
				limChar = c;
				continue;
			}
		}
		else
		{
			argMode = true;
		}
		if ( argMode || limChar )
		{
			arg += c;
		}
	}
	if ( !arg.empty() )
		args.append(arg);
}
/**********************************************/
bool Graftonite::runCommand( const ustring& cmdline )
{
	std::cout << std::endl;
	if ( cmdline.empty() )
		return true;
	else
		UConsole::addCommandToHistory( cmdline );

	UList<ustring> cmds = cmdline.split("&&");
	for ( ustring cmdd : cmds )
	{
		ustring cmd;
		UList<ustring> args;
		decomposeCommandLine( cmdd, cmd, args );

		if ( cmd.contains("new") || cmd == "n" )
			newElement(args);
		else if ( cmd.contains("reorder") || cmd == "=" )
			reorder(args);
		else if ( cmd.contains("insert") || cmd == "i" )
			insert(args);
		else if ( cmd.contains("delete") || cmd == "d" )
			deleteElement(args);
		else if ( cmd.contains("current") || cmd == "c" )
			setCurrentElement(args);
		else if ( cmd == "set" || cmd == "s" )
			set(args);
		else if ( cmd.contains("unset") || cmd == "u" )
			unset(args);
		else if ( cmd.contains("reset") || cmd == "rs" )
			_graph.reset(args);
		else if ( cmd.contains("open") || cmd == "o" )
			open(args);
		else if ( cmd.contains("load") || cmd == "l" )
			load(args);
		else if ( cmd.contains("run") || cmd == "r" )
			run(args);
		else if ( cmd.contains("save") || cmd == "e" )
			save(args);
		else if ( cmd.contains("export") || cmd == "x" )
			exportGraph(args);
		else if ( cmd.contains("show") || cmd == "w" )
			show(args);
		else if ( cmd.contains("log") )
			log(args);
		else if ( cmd.contains("about") || cmd == "a" )
			about();
		else if ( cmd.contains("version") || cmd == "v" )
			version();
		else if ( cmd.contains("help") || cmd == "h" )
			help(args);
		else if ( cmd.contains("clear") || cmd == "k" )
			UConsole::clear();
		else if ( cmd.contains("exit") || cmd == "q" )
			return false;
		else if ( cmd.contains("colors") )
			UConsole::testColors();
		else
			UConsole::warning_message("Unknown command !");
	}
	return true;
}
/**********************************************/
void Graftonite::newElement( UList<ustring>& args )
{
	if ( args.isEmpty() )
		return UConsole::error_message("No arguments !!");
	ustring cmd = args.first();
	ustring name;
	if ( args.size() > 1 )
		name = args.at(1);
	_graph.newElement( Element::getType(cmd), name, _log );
	if ( args.size() > 0 )
		set( args );
}
/**********************************************/
void Graftonite::reorder( UList<ustring>& /*args*/ )
{

}
/**********************************************/
void Graftonite::insert( UList<ustring>& /*args*/ )
{

}
/**********************************************/
void Graftonite::deleteElement( UList<ustring>& args )
{
	if ( args.isEmpty() )
		return UConsole::error_message("No arguments !!\n");
	ustring cmd;
	if ( args.isEmpty() ) return;
	cmd = args.takeFirst();
	ustring name;
	if ( !args.isEmpty() )
	{
		name = args.takeFirst();
	}
	_graph.removeElement( Element::getType(cmd), name );
}
/**********************************************/
void Graftonite::setCurrentElement( UList<ustring>& args )
{
	_graph.setCurrent( args );
}
/**********************************************/
void Graftonite::set( UList<ustring>& args )
{
	if ( args.isEmpty() )
		return UConsole::error_message("No arguments !!\n");
	_graph.set( args, _log ); // TO FINISH !
}
/**********************************************/
void Graftonite::unset( UList<ustring>& args )
{
	if ( args.isEmpty() )
		return UConsole::error_message("No arguments !!\n");
	_graph.unset( args, _log ); // TO CHECK !
}
/**********************************************/
void Graftonite::open( UList<ustring>& args )
{
	ustring filepath;
	if ( args.isEmpty() )
	{
		filepath = _graph._exportpath;
	}
	else
	{
		filepath = args.takeFirst();
	}
//		return UConsole::error_message("Missing file path !!");
//	if ( args.isEmpty() ) return;
	std::cout << std::endl << "Opening : " << filepath << std::endl;
	System::openFile( filepath );

}
/**********************************************/
void Graftonite::load( const UList<ustring>& args )
{
	if ( args.isEmpty() )
		return UConsole::error_message("Missing file path !!\n");
	ustring filepath;
	if ( args.isEmpty() ) return;
	filepath = args.at(0);
	std::cout << "Loading : " << filepath;
	ustring contents = File::read( filepath );
	std::cout << std::endl << contents;
	std::cout << std::endl;
}
/**********************************************/
void Graftonite::run( UList<ustring>& args )
{
	if ( args.isEmpty() )
		return UConsole::error_message("Missing file path !!\n");
	ustring filepath;
	if ( args.isEmpty() ) return;
	filepath = args.takeFirst();
	UList<ustring> contents;
	if ( File::read(filepath, contents) )
	{
		std::cout << "Running : " << filepath << std::endl;
		for ( const ustring& line : contents )
		{
			if ( !line.empty() &&
			     line.substr(0,2) != "//" &&
			     line.substr(0,1) != "#" )
			{
				std::cout << std::endl;
				UConsole::drawCommandLine(line);
				runCommand(line);
			}
		}
	}
	else
	{
		return UConsole::error_message("File can not be opened !!\n");
	}
	std::cout << std::endl;
}
/**********************************************/
void Graftonite::save( UList<ustring>& args )
{
	if ( args.isEmpty() )
		return UConsole::error_message("Missing file path !!\n");
	File::write( args.first(), UConsole::CommandHistory() );
}
/**********************************************/
void Graftonite::exportGraph( UList<ustring>& args )
{
	_graph.exportGraph( args );
}
/**********************************************/
void Graftonite::show( UList<ustring>& args )
{
	if ( args.isEmpty() )
		return _graph.showCurrent();
	ustring arg1 = args.takeFirst();
	if ( arg1.contains("all") )
		_graph.showAll( Key::NoKey );
	else if ( arg1.contains("current") )
		_graph.showCurrent();
	else if ( args.isEmpty() )
		_graph.showAll( Key::toKeys(arg1) );
	else
		_graph.showAll( Key::toKeys(arg1), args.takeFirst() );
}
/**********************************************/
void Graftonite::log( UList<ustring>& args )
{
	if ( args.isEmpty() )
		_log = true;
	else
	{
		ustring tmp = args.first();
		if ( tmp == "on" )
			_log = true;
		else
			_log = false;
	}
}
/**********************************************/
void Graftonite::about()
{
	//	std::cout << "\e[1m"; // Bold Text
	std::cout << "  Graftonite-Console" << std::endl;
	std::cout << "    " << "This program allows you to create scientific" << std::endl;
	std::cout << "    " << "graphics from data." << std::endl;
}
/**********************************************/
void Graftonite::version()
{
	//	std::cout << "\e[1m"; // Bold Text
	std::cout << "\x1B[34m";
	std::cout << "  Graftonite-Console - Version U22.01.18" << std::endl;
}
/**********************************************/
void Graftonite::help( UList<ustring>& /*args*/ )
{
	std::cout << "Help manual : " << std::endl << "\t blabla" << std::endl;

}
/**********************************************/
void Graftonite::exit()
{
	UConsole::normalColor();
	std::cout << "\e[1m"; // Bold Text
	std::cout << "----------------------------" << std::endl;
	std::cout << std::endl << "Thank you for using Graftonite !";
	std::cout << std::endl << "See you later !" << std::endl;
	std::cout << std::endl << "----------------------------" << std::endl;
	std::cout << "\e[0m" << std::endl;
	// ---------------------
}
/**********************************************/
/**********************************************/
/**********************************************/
