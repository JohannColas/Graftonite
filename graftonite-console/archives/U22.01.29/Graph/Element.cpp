#include "Element.h"
/**********************************************/
#include "Commons/UConsole.h"
/**********************************************/
#include "Graph.h"
/**********************************************/
/**********************************************/
Element::~Element()
{

}
Element::Element( Graph* parent )
{
	_parent = parent;
}
void Element::init( const ustring& name, unsigned index, bool log )
{
	set( Key::Name, name );
	setIndex( index );
	if ( log )
		std::cout << "  " << "new " << Key::toString(_type) << " \"" << name << "\"" << std::endl;
}
/**********************************************/
/**********************************************/
Key::Keys Element::type() const
{
	return _type;
}
ustring Element::typeString() const
{
	return Key::toString( _type );
}
void Element::setType( const Key::Keys& type )
{
	_type = type;
}
unsigned Element::index() const
{
	return _index;
}
void Element::setIndex( unsigned index )
{
	_index = index;
}
void Element::getTemplate()
{
	_template = nullptr;
	if ( hasKey(Key::Template) )
		_template = _parent->templateAt( get(Key::Template) );
}
Key::Keys Element::getType( const ustring& cmd )
{
	return Key::toKeys(cmd);
}
/**********************************************/
/**********************************************/
LineStyle Element::getLineStyle( const UList<Key::Keys>& firstKeys ) const
{
	LineStyle style;
	ustring tmp = get( Key(firstKeys, Key::Dash) );
	if ( !tmp.empty() )
		style.setDash( tmp.toDoubleList() );
	tmp = get( Key(firstKeys, Key::Dash, Key::Offset) );
	if ( !tmp.empty() )
		style.setDashOffset( tmp.toDouble() );

	tmp = get( Key(firstKeys, Key::Width) );
	if ( !tmp.empty() )
		style.setWidth( tmp.toDouble() );

	tmp = get( Key(firstKeys, Key::Color) );
	if ( !tmp.empty() )
		style.setColor( tmp );

	tmp = get( Key(firstKeys, Key::Join) );
	if ( !tmp.empty() )
		style.setJoin( tmp );
	tmp = get( Key(firstKeys, Key::MiterLimit) );
	if ( !tmp.empty() )
		style.setMiterLimit( tmp.toDouble() );

	tmp = get( Key(firstKeys, Key::Cap) );
	if ( !tmp.empty() )
		style.setCap( tmp );

	return style;
}
FillStyle Element::getFillStyle( const UList<Key::Keys>& firstKeys ) const
{
	FillStyle style;
	ustring tmp;

	tmp = get( Key(firstKeys, Key::Color) );
	if ( !tmp.empty() )
		style.setColor( tmp );

	return style;
}
ShapeStyle Element::getShapeStyle( const UList<Key::Keys>& firstKeys ) const
{
	ShapeStyle style;
	style.setFill( getFillStyle( firstKeys ) );
	style.setLine( getLineStyle( firstKeys ) );
	return style;
}
TextStyle Element::getTextStyle( const Key::Keys& firstKeys ) const
{
	TextStyle style;
	ustring tmp = get( firstKeys, Key::Font, Key::Family );
	if ( !tmp.empty() )
		style.setFamily( tmp );

	tmp = get( firstKeys, Key::Font, Key::Slant );
	if ( !tmp.empty() )
		style.setSlant( tmp );

	tmp = get( firstKeys, Key::Font, Key::Weight );
	if ( !tmp.empty() )
		style.setWeight( tmp );

	tmp = get( firstKeys, Key::Font, Key::Underline );
	if ( !tmp.empty() )
		style.setUnderline( tmp.toBool() );

	tmp = get( Key(firstKeys, Key::Font, Key::Capitalize) );
	if ( !tmp.empty() )
		style.setCap( tmp );

	tmp = get( Key(firstKeys, Key::Font, Key::Size) );
	if ( !tmp.empty() )
		style.setSize( tmp.toDouble() );

	tmp = get( Key(firstKeys, Key::Font, Key::Color) );
	if ( !tmp.empty() )
		style.setColor( tmp );

	tmp = get( Key(firstKeys, Key::Font, Key::Anchor) );
	if ( !tmp.empty() )
		style.setAnchor( tmp );

	tmp = get( Key(firstKeys, Key::Font, Key::Alignment) );
	if ( !tmp.empty() )
		style.setAlign( tmp );

	tmp = get( Key(firstKeys, Key::Font, Key::ScriptOpt) );
	if ( !tmp.empty() )
		style.setScriptOpt( tmp );

	return style;
}
/**********************************************/
/**********************************************/
bool Element::hasKey( const Key& key ) const
{
	return (_settings.count( key ) > 0);
}
/**********************************************/
/**********************************************/
ustring Element::get( const ustring& key ) const
{
	return get( Key(key) );
}
/**********************************************/
/**********************************************/
ustring Element::get( const Key& key ) const
{
	if ( hasKey(key) )
		return _settings.at( key );
	return "";
}
/**********************************************/
/**********************************************/
ustring Element::get( const Key::Keys& key, const Key::Keys& key2, const Key::Keys& key3 ) const
{
	return get( Key(key,key2,key3) );
}
ustring Element::get( const UList<Key::Keys>& key ) const
{
	return get( Key(key) );
}
bool Element::get( ustring& str, const Key& key ) const
{
	if ( !this->hasKey(key) )
		if ( _template == nullptr || (_template != nullptr && !_template->hasKey(key)) )
			if ( _parent == nullptr || (_parent != nullptr && !_parent->hasKey(key)) )
			return false;
	str = this->get(key);
	return true;
}
bool Element::get( ustring& str, const Key::Keys& key, const Key::Keys& key2, const Key::Keys& key3 ) const
{
	return this->get(str, Key(key, key2, key3));
}
bool Element::get( Key& value, const Key& key ) const
{
	ustring str;
	if ( !this->get(str, key) )
		return false;
	value = Key(str);
	return true;
}
bool Element::get( Key& value, const Key::Keys& key, const Key::Keys& key2, const Key::Keys& key3 ) const
{
	return this->get(value, Key(key, key2, key3));
}
bool Element::get( int& i, const Key& key ) const
{
	ustring str;
	if ( !this->get(str, key) )
		return false;
	i = str.toInt();
	return true;
}
bool Element::get( int& i, const Key::Keys& key, const Key::Keys& key2, const Key::Keys& key3 ) const
{
	return this->get(i, Key(key, key2, key3));
}
bool Element::get( double& d, const Key& key ) const
{
	ustring str;
	if ( !this->get(str, key) )
		return false;
	d = str.toDouble();
	return true;
}
bool Element::get( double& d, const Key::Keys& key, const Key::Keys& key2, const Key::Keys& key3 ) const
{
	return this->get(d, Key(key, key2, key3));
}
bool Element::get( Point& point, const Key& key ) const
{
	ustring str;
	if ( !this->get(str, key) )
		return false;
	point = Point(str);
	return true;
}
bool Element::get( Point& point, const Key::Keys& key, const Key::Keys& key2, const Key::Keys& key3 ) const
{
	return this->get(point, Key(key, key2, key3));
}
bool Element::get( Size& size, const Key& key ) const
{
	ustring str;
	if ( !this->get(str, key) )
		return false;
	size = Size(str);
	return true;
}
bool Element::get( Size& size, const Key::Keys& key, const Key::Keys& key2, const Key::Keys& key3 ) const
{
	if ( !this->get( size, Key(key, key2, key3) ) )
		return false;
	return true;
}
bool Element::get( Rect& rect, const Key& key ) const
{
	ustring str;
	if ( !this->get(str, key) )
		return false;
	rect = Rect(str);
	return true;
}
bool Element::get( Rect& rect, const Key::Keys& key, const Key::Keys& key2, const Key::Keys& key3 ) const
{
	return this->get(rect, Key(key, key2, key3));
}
bool Element::get( LineStyle& line_style, const Key& key ) const
{
	ustring str;
	if ( !this->get(str, key) )
		return false;
	line_style = getLineStyle();
	return true;
}
bool Element::get( LineStyle& line_style, const Key::Keys& key, const Key::Keys& key2, const Key::Keys& key3 ) const
{
	return this->get(line_style, Key(key, key2, key3));
}
/**********************************************/
/**********************************************/
Key Element::getKey( const Key::Keys& key, const Key::Keys& key2, const Key::Keys& key3 ) const
{
	return Key::toKeys(get( Key(key,key2,key3) ));
}
/**********************************************/
/**********************************************/
bool Element::set( const ustring& key, const ustring& value )
{
	return set( Key(key), value );
}
/**********************************************/
/**********************************************/
bool Element::set( const Key& key, const ustring& value )
{
	if ( hasKey(key) )
		_settings.at(key) = value;
	else
		_settings.emplace( key, value );
	return true;
}
bool Element::set( const Key::Keys& key, const ustring& value )
{
	return set( Key(key), value );
}
bool Element::set( const UList<Key::Keys>& key, const ustring& value )
{
	return set( Key(key), value );
}
/**********************************************/
/**********************************************/
bool Element::set( UList<ustring>& args, bool log )
{
	if ( log )
		UConsole::level1("set "+typeString()+" \""+get(Key::Name)+"\"");
	for ( int it = 0; it < args.size(); it+=2 )
		if ( it+1 < args.size() )
			if ( set( Key(args.at(it)), args.at(it+1) ) && log )
				 std::cout << "\n    "+Key(args.at(it)).fullKeys()+": "+args.at(it+1);
	if ( log )
		std::cout << std::endl;
	return true;
}
/**********************************************/
/**********************************************/
bool Element::reset( UList<ustring>&, bool )
{
	return true;
}
/**********************************************/
/**********************************************/
bool Element::unset( const ustring& key )
{
	if ( Key(key) != Key::Name )
	{
		if ( hasKey(key) )
			_settings.erase( key );
		return true;
	}
	return false;
}
/**********************************************/
/**********************************************/
void Element::unset( const UList<ustring>& args, bool log )
{
	if ( log ) log = false;
	for ( int it = 0; it < args.size(); ++it )
		unset( args.at(it) );
}
/**********************************************/
/**********************************************/
void Element::remove( const ustring& key )
{
	if ( hasKey(key) )
		_settings.erase(key);
}
/**********************************************/
/**********************************************/
void Element::clear()
{
	ustring name;
	if ( hasKey(Key::Name) )
		name = get(Key::Name);
	_settings.clear();
	set( Key::Name, name );
}
/**********************************************/
/**********************************************/
void Element::show()
{
	ustring str_typ = typeString().toFirstUpper();
	ustring name = get(Key::Name);
	if ( name.empty() )
		name = "<NoName>";
	std::cout << "    " << str_typ << ": " << name << std::endl;
	std::cout << "    KEYS              |  VALUES" << std::endl;
	std::cout << "    -------------------------------------" << std::endl;
	unsigned long len = 16;
	for ( auto const &pair: _settings )
	{
		ustring key = pair.first.fullKeys(), tmp;
		unsigned long size = key.length();
		for ( unsigned long it = 0; it < len; ++it )
		{
			if ( it < size )
				tmp += key.at(it);
			else
				tmp += " ";
		}
//		if ( size > len )
//			size = len;
//		for ( unsigned long it = 0; it < size; ++it )
//			tmp += key.at(it);
//		for ( unsigned long it = tmp.length(); it < len; ++it )
//			tmp.insert(tmp.begin(), 1, ' ');
		std::cout << "    " << tmp << "  |  " << pair.second << std::endl;
	}
	std::cout << "    -------------------------------------" << std::endl;
	std::cout << std::endl;
}
/**********************************************/
/**********************************************/
void Element::shortShow()
{
	ustring str_typ = typeString();
	for ( unsigned long it = str_typ.size(); it < 6; ++it )
		str_typ += " ";
	ustring num = ustring(index());
	for (unsigned long jt = num.size(); jt < 5; ++jt)
		num = " " + num;
	std::cout << "      "
		 << str_typ << "  | "
		 << num << "  |  "
		 << get(Key::Name) << std::endl;
}
/**********************************************/
/**********************************************/
void Element::prepare( Cairo* /*drawing*/ )
{

}
/**********************************************/
/**********************************************/
void Element::draw( Cairo* /*drawing*/ )
{

}
/**********************************************/
/**********************************************/
Rect Element::boundingRect() const
{
	return _boundingRect;
}
/**********************************************/
/**********************************************/
