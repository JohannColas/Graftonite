#include "Axis.h"
/**********************************************/
#include <cmath>
/**********************************************/
#include "Commons/Cairo.h"
/**********************************************/
#include "Graph.h"
#include "Layer.h"
/**********************************************/
/**********************************************/
Axis::~Axis()
{
}
Axis::Axis( Graph* parent )
	: Element(parent)
{
	init();
}
Axis::Axis( const ustring& name, unsigned index, Graph* parent, bool log )
	: Element(parent)
{
	init();
	Element::init( name, index, log );
}
void Axis::init()
{
	_type = Key::Axis;
//	_boundingRect = {100, 100, 1200, 800};
	// Default settings
	Element::set( Key::Min, "0" );
	Element::set( Key::Max, "1" );
	Element::set( Key::Type, "X" );
	Element::set( Key::Scale, "linear" );
	Element::set( Key::Position, "bottom" );
	Element::set( Key(Key::Line, Key::Width), "3" );
	Element::set( Key(Key::Line, Key::Cap), "square" );
	Element::set( Key(Key::Ticks, Key::Position), "outside" );
	Element::set( Key(Key::Ticks, Key::Increment), "0.1" );
	Element::set( Key(Key::Ticks, Key::Size), "10" );
	Element::set( Key(Key::Minor, Key::Ticks, Key::Increment), "0.05" );
	Element::set( Key(Key::Minor, Key::Ticks, Key::Size), "5" );
	Element::set( Key(Key::Labels, Key::Position), "outside" );
	Element::set( Key(Key::Labels, Key::Font, Key::Size), "24" );
	Element::set( Key(Key::Title, Key::Position), "outside" );
	Element::set( Key(Key::Title, Key::Text), "Title" );
	Element::set( Key(Key::Title, Key::Font, Key::Size), "30" );
}
/**********************************************/
/**********************************************/
bool Axis::set( const Key& key, const ustring& value )
{
	if ( key == Key::Name ||
		 key == Key::Template ||
		 key == Key::Layer ||
		 key == Key::Frame ||
		 key == Key::Hide ||
		 key == Key::Min ||
		 key == Key::Max ||
		 key == Key::Scale ||
		 key == Key::LinkTo ||
		 key == Key::Line ||
		 key.isLineStyle({Key::Line}, true, false) ||
		 key == Key(Key::Ticks, Key::Position) ||
		 key == Key(Key::Ticks, Key::Increment) ||
		 key == Key(Key::Ticks, Key::Numbers) ||
		 key == Key(Key::Ticks, Key::Size) ||
		 key == Key(Key::Minor, Key::Ticks, Key::Increment) ||
		 key == Key(Key::Minor, Key::Ticks, Key::Numbers) ||
		 key == Key(Key::Minor, Key::Ticks, Key::Size) ||
		 key == Key(Key::Labels, Key::Position) ||
		 key.isFontStyle(Key::Labels) ||
		 key == Key(Key::Title, Key::Position) ||
		 key == Key(Key::Title, Key::Text) ||
		 key.isFontStyle(Key::Title) )
	{
		return Element::set( key, value );
	}
	else if ( key == Key::Type )
	{
		if ( value == "X" )
		{
			Element::set( Key::Position, Key::toString(Key::Bottom) );
			Element::set( Key(Key::Labels, Key::Font, Key::Anchor), Key::toString(Key::Top) );
			Element::set( Key(Key::Title, Key::Font, Key::Anchor), Key::toString(Key::Top) );
		}
		else if ( value == "Y" )
		{
			Element::set( Key::Position, Key::toString(Key::Left) );
			Element::set( Key(Key::Labels, Key::Font, Key::Anchor), Key::toString(Key::Right) );
			Element::set( Key(Key::Title, Key::Font, Key::Anchor), Key::toString(Key::Right) );
		}
		if ( value != get(Key::Type) )
		{
			Element::set( Key(Key::Ticks, Key::Position), Key::toString(Key::Outside) );
			Element::set( Key(Key::Labels, Key::Position), Key::toString(Key::Outside) );
			Element::set( Key(Key::Title, Key::Position), Key::toString(Key::Outside) );
		}
		return Element::set( key, value );
	}
	else if ( key == Key::Position )
	{
		return Element::set( key, value );
	}
	else if ( key == Key::Ticks )
	{

	}
	else if ( key == Key::Labels )
	{

	}
	else if ( key == Key::Title )
	{

	}
	return false;
}
/**********************************************/
/**********************************************/
void Axis::prepare( Cairo* /*drawing*/ )
{
	Element::getTemplate();
	Frame* frame = _parent->frame( get(Key::Frame) );
	if ( frame )
	{
		_frameRect = {frame->get(Key::X).toDouble(),
					  frame->get(Key::Y).toDouble(),
					  frame->get(Key::Width).toDouble(),
					  frame->get(Key::Height).toDouble()};
		_frameRect = frame->boundingRect();
	}
}
/**********************************************/
/**********************************************/
void Axis::draw( Cairo* drawing )
{
	Frame* frame = _parent->frame( get(Key::Frame) );
	if ( frame )
	{
		_frameRect = {frame->get(Key::X).toDouble(),
					  frame->get(Key::Y).toDouble(),
					  frame->get(Key::Width).toDouble(),
					  frame->get(Key::Height).toDouble()};
		_frameRect = frame->boundingRect();
	}
//	double x_min = 0,
//			x_max = 0,
//			y_min = 0,
//			y_max = 0;
	CalculateScaleCoef();
	//
	ustring tmp;
	// -----------------------
	//
	// Axis Settings
	ustring type = "X";//get(Key::Type);
	get( type, Key::Type );
	Key axispos = Key::Bottom;
	get( axispos, Key::Position );
	double line_pos = 0;
	ustring line_dir = "";
	UList<double> line_limits; line_limits.append({0,0});
	LineStyle linestyle = getLineStyle();
	// -----------------------
	//
	// Ticks Settings
	Key tickpos = Key::Outside;
	get( tickpos, Key::Ticks, Key::Position );
	double ticksize = 10;
	get( ticksize, Key::Ticks, Key::Size );
	double minorticksize = 5;
	get( minorticksize, Key::Minor, Key::Ticks, Key::Size );
	double ticks_pos = 0;
	ustring ticks_dir = "";
	UList<double> ticks_limits;
	double minorticks_pos = 0;
	UList<double> minorticks_limits;
	// -----------------------
	//
	// Labels Settings
	Key labelspos = Key::Outside;
	get( labelspos, Key::Labels, Key::Position );
	UList<double> labelsshift = {0,0};
	Point labels_shift = {0,0};
	get( labels_shift, Key::Labels, Key::Shift );
	Key::Keys lb_anc = Key::Top;
	Key labels_anchor = Key::Top;
	get( labels_anchor, Key::Labels, Key::Anchor );
	tmp = get( Key::Labels, Key::Anchor );
	if ( !tmp.empty() )
		lb_anc = Key::toKeys( tmp );
	UList<ustring> labels;
	double labels_pos = 0;
	TextStyle labelsstyle = getTextStyle( Key::Labels );
	labelsstyle.setAnchor( lb_anc );
	// -----------------------
	//
	// Title Settings
	Key titlepos = Key::Outside;
	get( titlepos, Key::Title, Key::Position );
	ustring title = "Title";
	get( title, Key::Title, Key::Text );
	UList<double> titleshift = {0,0};
	Point title_shift = {0,0};
	get( title_shift, Key::Title, Key::Shift );
	UList<double> title_pos = {0,0};
	Point title_position = {0,0};
	get( title_position, Key::Title, Key::Position );
	Key::Keys tl_anc = Key::Top;
	Key title_anchor = Key::Top;
	get( title_anchor, Key::Title, Key::Anchor );
	tmp = get( Key::Title, Key::Anchor );
	if ( !tmp.empty() )
		tl_anc = Key::toKeys( tmp );
	ustring title_transfo = "";
	TextStyle titlestyle = getTextStyle( Key::Title );
	titlestyle.setAnchor( tl_anc );
	//
	// -----------------------
	//
	if ( type == "X" )
	{
		// -----------------------
		_line = _frameRect.line( axispos );
		drawing->drawLine( _line, linestyle );
		_boundingRect = _line;
		// -----------------------
		// Draw X Axis Ticks (major & minor)
		ticks_pos = _line.y1();
		minorticks_pos = _line.y1();
		if ( tickpos == Key::Top ||
			 (tickpos == Key::Outside && axispos == Key::Top) ||
			 (tickpos == Key::Inside && axispos == Key::Bottom) )
		{
			ticks_pos = _line.y2() - ticksize;
			minorticks_pos = _line.y2() - minorticksize;
		}
		else if ( tickpos == Key::Center )
		{
			ticks_pos = _line.y2() - 0.5*ticksize;
			minorticks_pos = _line.y2() - 0.5*minorticksize;
		}
		// calculate ticks_limits
		CalculateTicksPosition( ticks_limits, minorticks_limits, labels );
		for ( double xpos : ticks_limits )
		{
			Line tick = Line( xpos, ticks_pos, ticksize, Line::Vertical );
			_ticks.append( tick );
			_boundingRect.expand( tick );
			drawing->drawLine( tick, linestyle );
		}
		for ( double xpos : minorticks_limits )
		{
			Line minor_tick = Line( xpos, minorticks_pos, minorticksize, Line::Vertical );
			_minor_ticks.append( minor_tick );
			_boundingRect.expand( minor_tick );
			drawing->drawLine( minor_tick, linestyle );
		}
		// -----------------------
		// Draw X Axis Labels
		labels_pos = ticks_pos + ticksize + labelsshift.at(1);
		if (labelspos == Key::Top ||
			(labelspos == Key::Outside && axispos == Key::Top) ||
			(labelspos == Key::Inside && axispos == Key::Bottom) )
		{
			labels_pos = ticks_pos - labelsshift.at(1);
		}
		else if ( labelspos == Key::Center )
		{
			labels_pos = line_pos + labelsshift.at(1);
		}
		// ---------------
		// Open X Axis Labels Group
		labelsstyle.setAnchor( Key::Top );
		//		// ---------------
		// Draw each label
		for ( int it = 0; it < labels.size(); ++it )
		{
			StyledText s_text;
			s_text.setText( labels.at(it) );
			s_text.setPos( Point(ticks_limits.at(it), labels_pos) );
			s_text.setStyle( labelsstyle );
			_labels.append( s_text );
			Rect tmp = drawing->drawText( labels.at(it), ticks_limits.at(it), labels_pos, labelsstyle );
			_boundingRect.expand( tmp );
		}
		// ---------------
		// Draw X Axis Title
		title_pos = { _frameRect.x() + 0.5*_frameRect.width() + titleshift.at(0), _boundingRect.y()+_boundingRect.height() + titleshift.at(1) };
		if ( ticks_pos + ticksize > _boundingRect.y()+_boundingRect.height() )
			title_pos.replace(1, ticks_pos + ticksize + titleshift.at(1));
		if (titlepos == Key::Top ||
			(titlepos == Key::Outside && axispos == Key::Top) ||
			(titlepos == Key::Inside && axispos == Key::Bottom) )
		{
			title_pos.replace( 1, _boundingRect.y()+_boundingRect.height() - titleshift.at(1) );
			if ( ticks_pos < _boundingRect.y()+_boundingRect.height() )
				title_pos.replace( 1, ticks_pos - titleshift.at(1) );
		}
		titlestyle.setAnchor( Key::Top );
		Rect dde = drawing->drawText( title, title_pos.at(0), title_pos.at(1), titlestyle );
		_boundingRect.expand( dde );
		// Draw X Axis END
	}
	// -----------------------
	// -----------------------
	// Draw Y Axis
	// -----------------------
	else if ( type == "Y" )
	{
		// ---------------
		// Draw Y Axis Line
		_line = _frameRect.line( axispos );
		drawing->drawLine( _line, linestyle );
		_boundingRect = _line;
		// ---------------
		// Draw Y Axis Ticks (major & minor)
		ticks_dir = "h";
		ticks_pos = _line.x1() - ticksize;
		minorticks_pos = _line.x1() - minorticksize;
		if ( tickpos == Key::Right ||
			 (tickpos == Key::Outside && axispos == Key::Right) ||
			 (tickpos == Key::Inside && axispos == Key::Left) )
		{
			ticks_pos = _line.x1();
			minorticks_pos = _line.x1();
		}
		else if ( tickpos == Key::Center )
		{
			ticks_pos = _line.x1() - 0.5*ticksize;
			minorticks_pos = _line.x1() - 0.5*minorticksize;
		}
		// calculate ticks_limits
		CalculateTicksPosition( ticks_limits, minorticks_limits, labels	, true );
		for ( double ypos : ticks_limits )
		{
			Line tick = Line( ticks_pos, ypos, ticksize, Line::Horizontal );
			_ticks.append( tick );
			_boundingRect.expand( tick );
			drawing->drawLine( tick, linestyle );
		}
		for ( double ypos : minorticks_limits )
		{
			Line minor_tick = Line( minorticks_pos, ypos, minorticksize, Line::Horizontal );
			_minor_ticks.append( minor_tick );
			_boundingRect.expand( minor_tick );
			drawing->drawLine( minor_tick, linestyle );
		}
		// ---------------
		// Draw Y Axis Labels
		labels_pos = ticks_pos - labelsshift.at(0);
		if ( labelspos == Key::Right ||
			 (labelspos == Key::Outside && axispos == Key::Right) ||
			 (labelspos == Key::Inside && axispos == Key::Left) )
		{
			labels_pos = ticks_pos + ticksize + labelsshift.at(0);
		}
		else if ( labelspos == Key::Center )
		{
			labels_pos = line_pos + labelsshift.at(0);
		}
		// ---------------
		// Open Y Axis Labels Group
		labelsstyle.setAnchor( Key::Right );
		// ---------------
		// Draw each label
		for ( int it = 0; it < labels.size(); ++it )
		{
			Rect tmp = drawing->drawText( labels.at(it), labels_pos, ticks_limits.at(it), labelsstyle );
			_boundingRect.expand( tmp );
		}
		// ---------------
		// Draw Y Axis Title
		title_pos = {_boundingRect.x() - titleshift.at(0), _frameRect.y()+0.5*_frameRect.height() + titleshift.at(1)};
		if ( ticks_pos < _boundingRect.x() )
			title_pos.replace(0, ticks_pos - titleshift.at(0));
		if ( titlepos == Key::Right ||
			 (titlepos == Key::Outside && axispos == Key::Right) ||
			 (titlepos == Key::Inside && axispos == Key::Left) )
		{
			title_pos.replace(0, _boundingRect.x()+_boundingRect.width() - titleshift.at(0));
			if ( ticks_pos + ticksize > _boundingRect.x()+_boundingRect.width() )
				title_pos.replace(0, ticks_pos + ticksize + titleshift.at(0));
		}
		titlestyle.setAnchor( Key::Bottom );
		titlestyle.setRotation( 90.0 );
		Rect dde = drawing->drawText( title, title_pos.at(0), title_pos.at(1), titlestyle );
		_boundingRect.expand( dde );
		// Draw Y Axis END
	}

	// TEST
	LineStyle styl;
	styl.setColor( Color("red6") );
	styl.setWidth( 0.5 );
	ShapeStyle style;
	style.setLine( styl );
	drawing->drawRect( _boundingRect, style );
	// TEST END

	_boundingRect = _frameRect;
}
/*------------------------------*/
/*------------------------------*/
void Axis::CalculateTicksPosition( UList<double>& tickspos, UList<double>& minortickspos, UList<ustring>& labels, bool isYaxis )
{
	double tickinterval;
	ustring tk_inc = get(Key::Ticks, Key::Increment);
	if ( tk_inc.empty() )
	{
		ustring tk_nb = get(Key::Ticks, Key::Numbers);
		double tick_nb = 10;
		if ( !tk_nb.empty() )
			tick_nb = tk_nb.toDouble();
		tickinterval = (_max-_min)/tick_nb;
	}
	else
		tickinterval = tk_inc.toDouble();
	double minortickinterval;// = get(Key(Key::Minor, Key::Ticks, Key::Increment)).toDouble();
	ustring mn_tk_inc = get(Key::Minor, Key::Ticks, Key::Increment);
	if ( mn_tk_inc.empty() )
	{
		ustring mn_tk_nb = get(Key::Minor, Key::Ticks, Key::Numbers);
		double mn_tick_nb = 20;
		if ( !mn_tk_nb.empty() )
			mn_tick_nb = mn_tk_nb.toDouble();
		minortickinterval = (_max-_min)/mn_tick_nb;
	}
	else
		minortickinterval = mn_tk_inc.toDouble();
	char axis_dir = 1; if ( _min > _max ) axis_dir = -1;

	int quot = (int)(_min/tickinterval);
	if ( _min > 0 && _max > _min ) ++quot;
	else if ( _min < 0 && _max < _min ) --quot;

	int minorquot = (int)(_min/minortickinterval);
	if ( _min > 0 && _max > _min ) ++minorquot;
	else if ( _min < 0 && _max < _min ) --minorquot;

	double posp = quot*tickinterval;
	double pos = GetGraphCoord( posp );

	int it = 0;
	double minorposp = minorquot*minortickinterval;
	double minorpos = GetGraphCoord( minorposp );
	while (( (minorpos+1 < pos && !isYaxis) ||
			 (minorpos-1 > pos && isYaxis) )&&it<100 )
	{
		minortickspos.append( minorpos );
		minorposp += axis_dir*minortickinterval;
		minorpos = GetGraphCoord( minorposp );
		++it;
	}
	it = 0;
	while (( (pos-1 < _frameRect.x()+_frameRect.width() && !isYaxis) ||
			 (pos+1 > _frameRect.y() && isYaxis) )&&it<100)
	{
		tickspos.append( pos );
		// -----------
		// Get Labels
		labels.append( ustring(posp) );
		// -----------
		int jt = 0;
		double minorposp = _minortickinterval;
		double minorpos = GetGraphCoord( posp + axis_dir*minorposp );
		while (( minorposp < _tickinterval &&
				 ((minorpos-1 < _frameRect.x()+_frameRect.width() && !isYaxis) ||
				  (minorpos+1 > _frameRect.y() && isYaxis) ) )&&jt<100)
		{
			minortickspos.append( minorpos );
			minorposp += _minortickinterval;
			minorpos = GetGraphCoord( posp + axis_dir*minorposp );
			++jt;
		}
		posp += axis_dir*_tickinterval;
		pos = GetGraphCoord( posp );
		++it;
	}
}
/*------------------------------*/
/*------------------------------*/
double Axis::round( double value )
{
	double val = value;
	return val;
}
/*------------------------------*/
/*------------------------------*/
void Axis::CalculateScaleCoef()
{
	ustring type = get( Key::Type );
	ustring scale = get( Key::Scale );
	double scaleOpt = get( Key(Key::Scale, Key::Option) ).toDouble();
	double po = _frameRect.x();
	double pm = _frameRect.x()+_frameRect.width();
	if ( type == "Y" )
	{
		po = _frameRect.y()+_frameRect.height();
		pm = _frameRect.y();
	}
	if ( scale == "log10" )
	{
		if ( _min <= 0 )
			_min = 0.1;
		if ( _max <= 0 )
			_max = 1;
		_scaleA = ( pm - po ) / ( log10(_max) - log10(_min) );
		_scaleB = po - _scaleA*log10(_min);
	}
	else if ( scale == "log" )
	{
		if ( _min <= 0 )
			_min = 0.1;
		if ( _max <= 0 )
			_max = 1;
		_scaleA = ( pm - po ) / ( log(_max) - log(_min) );
		_scaleB = po - _scaleA*log(_min);
	}
	else if ( scale == "logx" )
	{
		if ( _min <= 0 )
			_min = 0.1;
		if ( _max <= 0 )
			_max = 1;
		_scaleA = log(scaleOpt) * ( pm - po ) / ( log(_max) - log(_min) );
		_scaleB = po - _scaleA*log(_min)/log(scaleOpt);
	}
	else if ( scale == "reciprocal" )
	{
		if ( _min == 0 )
			_min = 1;
		if ( _max == 0 )
			_max = 10;
		_scaleA = ( pm - po ) / ( 1/_max - 1/_min );
		_scaleB = po - _scaleA/_min;
	}
	else if ( scale == "offsetreciprocal" )
	{
		if ( _min+scaleOpt == 0 )
			_min = 1-scaleOpt;
		if ( _max+scaleOpt == 0 )
			_max = 10-scaleOpt;
		_scaleA = ( pm - po ) / ( 1/(_max+scaleOpt) - 1/(_min+scaleOpt) );
		_scaleB = po - _scaleA/(_min+scaleOpt);
	}
	else
	{
		_scaleA = ( pm - po ) / ( _max - _min );
		_scaleB = po - _scaleA*_min;
	}
}
/*------------------------------*/
/*------------------------------*/
double Axis::GetGraphCoord( double coord )
{
	ustring scale = get( Key::Scale );
	double scaleOpt = get( Key(Key::Scale, Key::Option) ).toDouble();
	if ( scale == "log10" )
		return _scaleA*log10(coord)+_scaleB;
	else if ( scale == "log" )
		return _scaleA*log(coord)+_scaleB;
	else if ( scale == "logx" )
		return _scaleA*log(coord)/log(scaleOpt)+_scaleB;
	else if ( scale == "reciprocal" )
		return _scaleA*(1/coord)+_scaleB;
	else if ( scale == "offsetreciprocal" )
		return _scaleA*(1/(coord+scaleOpt))+_scaleB;
	return _scaleA*coord+_scaleB;
}
/*------------------------------*/
/*------------------------------*/
Rect Axis::frameRect() const
{
	return _frameRect;
}
/**********************************************/
/**********************************************/
