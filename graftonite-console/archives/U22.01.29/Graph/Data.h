#ifndef DATA_H
#define DATA_H
/**********************************************/
#include "Element.h"
/**********************************************/
/**********************************************/
/**********************************************/
class Data
		: public Element
{
	UList<UList<ustring>> _data;
public:
	~Data();
	Data( Graph* parent = nullptr );
	Data( const ustring& name, unsigned index, Graph* parent = nullptr, bool log = false );
	void init();
	/**********************************************/
	bool set( const Key& key, const ustring& value ) override;
	// Output
	UList<UList<ustring>> data() const;
	void initData();
	UList<ustring> getData( const ustring& index );
	UList<ustring> getColumn( int index );
	UList<ustring> getRow( int index );
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // DATA_H
