#include "Frame.h"
/**********************************************/
#include "Commons/Cairo.h"
/**********************************************/
/**********************************************/
Frame::Frame( Graph* parent )
	: Element(parent)
{
	init();
}
Frame::Frame( const ustring& name, unsigned index, Graph* parent, bool log )
	: Element(parent)
{
	init();
	Element::init( name, index, log );
}
void Frame::init()
{
	_type = Key::Frame;
	// Default Settings
	Element::set( Key::X, "150");
	Element::set( Key::Y, "100");
	Element::set( Key::Width, "1200");
	Element::set( Key::Height, "775");
	//_boundingRect = { 150, 100, 1200, 775 };
	Element::set( Key::Hide, "true");
}
/**********************************************/
/**********************************************/
bool Frame::set( const Key& key, const ustring& value )
{
	if ( key == Key::Name ||
		 key == Key::Template ||
		 key == Key::X ||
		 key == Key::Y ||
		 key == Key::Width ||
		 key == Key::Height ||
		 key.isLineStyle({Key::Borders}, false) ||
		 key.isFillStyle(Key::Background)
		 )
	{
		return Element::set( key, value );
	}
	else if ( key == Key::Position )
	{
		if ( !value.contains("x") ) return false;
		UList<ustring> list = value.split("x");
		set( Key::X, list.at(0) );
		set( Key::Y, list.at(1) );
	}
	else if ( key == Key::Size )
	{
		if ( !value.contains("x") ) return false;
		UList<ustring> list = value.split("x");
		set( Key::Width, list.at(0) );
		set( Key::Height, list.at(1) );
	}
	else if ( key == Key::Hide )
	{
		Element::set( key, value );
		if ( !hasKey(Key(Key::Background, Key::Color)) )
			Element::set( Key(Key::Background, Key::Color), "transparent");
		if ( !hasKey(Key(Key::Borders, Key::Color)) )
			Element::set( Key(Key::Borders, Key::Color), "black");
	}
	else if ( key == Key::Background )
	{
	}
	else if ( key == Key::Borders )
	{
	}
	return false;
}
/**********************************************/
/**********************************************/
void Frame::prepare( Cairo* )
{
	Element::getTemplate();
	// Set Geometry
	if ( hasKey(Key::X) )
		_boundingRect.setX( stod(get(Key::X)) );
	if ( hasKey(Key::Y) )
		_boundingRect.setY( stod(get(Key::Y)) );
	if ( hasKey(Key::Width) )
		_boundingRect.setWidth( stod(get(Key::Width)) );
	if ( hasKey(Key::Height) )
		_boundingRect.setHeight( stod(get(Key::Height)) );
	// Set Style
	FillStyle fill_style = getFillStyle();
	LineStyle line_style = getLineStyle( {Key::Borders} );
	_style.setFill( fill_style );
	_style.setLine( line_style );
}
/**********************************************/
/**********************************************/
void Frame::draw( Cairo* drawing )
{
	if ( hasKey(Key::Hide) && get(Key::Hide).toBool() )
		return;
	// Draw Rect
	drawing->drawRect( _boundingRect, style() );
}
/**********************************************/
/**********************************************/
ShapeStyle Frame::style() const
{
	return _style;
}
/**********************************************/
/**********************************************/
