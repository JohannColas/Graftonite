#include "File.h"
/**********************************************/
#include <fstream>
#ifdef _WIN32
   #include <io.h>
   #define access    _access_s
#else
   #include <unistd.h>
#endif

#if __cplusplus == 201402L
// C++14 code
#elif __cplusplus == 201103L
// C++11 code
#elif __cplusplus == 201703L
// C++17 code
#elif __cplusplus >= 202002L
// C++20 code
#else
// C++ code
#endif


//#include <unistd.h>
#include "UString.h"
/**********************************************/
/**********************************************/
ustring File::read( const ustring& path )
{
	ustring contents;
	std::ifstream file;
	file.open( path );
	if ( file.is_open() )
	{
		ustring line;
		while ( file )
		{
			std::getline( file, line );
			contents += line + "\n";
		}
		file.close();
	}
	return contents;
}
/**********************************************/
/**********************************************/
bool File::read( const ustring& path, UList<ustring>& contents )
{
	std::ifstream file;
	file.open( path );
	if ( file.is_open() )
	{
		ustring line;
		while ( file )
		{
			std::getline( file, line );
			contents += line;
		}
		file.close();
		return true;
	}
	return false;
}
/**********************************************/
/**********************************************/
void File::write( const ustring& path, const ustring& contents )
{
	std::ofstream file;
	file.open( path );
	if ( file.is_open() )
	{
		file << contents << std::endl;
		file.close();
	}
}
/**********************************************/
/**********************************************/
void File::write( const ustring& path, const UList<ustring>& contents )
{
	std::ofstream file;
	file.open( path );
	if ( file.is_open() )
	{
		for ( const ustring& line : contents )
			file << line << std::endl;
		file.close();
	}
}
/**********************************************/
/**********************************************/
void File::append( const ustring& path, const ustring& contents )
{
	ustring contents_to_save = File::read( path );
	contents_to_save += "\n" + contents;
	File::write( path, contents_to_save );
}
/**********************************************/
/**********************************************/
#include <filesystem>
//#include <sys/stat.h>
//#include <unistd.h>
bool File::exists( const ustring& path )
{
	std::filesystem::path file(path.c_str());
	return std::filesystem::exists(file);

//	int res = access( path.c_str(), R_OK );
//	if ( res < 0 )
//	{
//		if ( errno == ENOENT )
//		{
//			 // file does not exist
//		} else if ( errno == EACCES )
//		{
//			 // file exists but is not readable
//		}
//		else
//		{
//			 // FAIL
//		}
//	}
//	else if ( res == 0 )
//		return true;
//	return false;
}

#include <filesystem> // C++17
namespace fs = std::filesystem;
/*! \return True if owner, group and others have read permission,
			i.e. at least 0444.
*/
bool isReadable( const ustring& path )
{
	if ( !File::exists(path) )
		return false;
	fs::path p(path.c_str());
	std::error_code ec; // For noexcept overload usage.
	auto perms = fs::status(p, ec).permissions();
	if ( (perms & fs::perms::owner_read) != fs::perms::none &&
	     (perms & fs::perms::group_read) != fs::perms::none &&
	     (perms & fs::perms::others_read) != fs::perms::none
	    )
	{
		return true;
	}
	return false;
}
/**********************************************/
/**********************************************/
