#ifndef STYLES_H
#define STYLES_H
/**********************************************/
#include <cairo/cairo.h>
#include "Key.h"
class Cairo;
/**********************************************/
#include "Geometry.h"
#include "Color.h"
/**********************************************/
/**********************************************/
/**********************************************/
class FontMetrics
{
	double _ascent = -1;
	double _descent = -1;
	double _height = -1;
	double _tight_height = -1;
	double _width = -1;
	double _tight_width = -1;
	double _x_bearing = -1;
	double _y_bearing = -1;
public:
	FontMetrics();
	FontMetrics( cairo_t* drawing );
	FontMetrics( const ustring& text, cairo_t* drawing );
	void init( cairo_t* drawing );
	void init( const ustring& text, cairo_t* drawing );
	double ascent() const;
	void setAscent( double ascent );
	double descent() const;
	void setDescent( double descent );
	double height() const;
	void setHeight( double height );
	double tightHeight() const;
	void setTightHeight( double tightHeight );
	double width() const;
	void setWidth( double width );
	double tightWidth() const;
	void setTightWidth( double tightWidth );
	double xBearing() const;
	void setXBearing( double xBearing );
	double yBearing() const;
	void setYBearing( double yBearing );
};
/**********************************************/
/**********************************************/
/**********************************************/
class TextStyle
{
public:
	// ------------------------------------
	// For capitalization
	// Font size of small cap is equal to :
	// smallcaps_font_size = 0.8 * original_font_size + 0.1
	// char value of a = 97 / z = 122 and of A = 65 / Z = 90 -> Thus remove 32
	// ------------------------------------
	enum Capitalization
	{
		NoCap,
		SmallCap,
		Cap
	};
	enum Align
	{
		Center,
		Justify,
		Left,
		Right
	};
	enum ScriptOpt
	{
		NormalScript,
		SuperScript,
		SubScript,
	};
private:
	ustring _family = "Sans Serif";
	cairo_font_slant_t _slant = CAIRO_FONT_SLANT_NORMAL;
	cairo_font_weight_t _weight = CAIRO_FONT_WEIGHT_NORMAL;
	bool _underline = false;
	Capitalization _cap = NoCap;
	double _size = 10;
	Color _color;
	Anchor _anchor = Key::Left;
	Align _align = Align::Left;
	ScriptOpt _scriptopt = TextStyle::NormalScript;
	double _rotation = 0.0;
public:
	TextStyle();
	TextStyle( const ustring& style );
	FontMetrics metrics( const ustring& text, cairo_t* drawing ) const;
	FontMetrics metrics( const ustring& text, Cairo& drawing ) const;
	FontMetrics metrics( const ustring& text, Cairo* drawing ) const;
	void set( const ustring& style );
	ustring family() const;
	void setFamily( const ustring& family );
	cairo_font_slant_t slant() const;
	void setSlant( const cairo_font_slant_t& slant );
	void setSlant( const ustring& slant );
	cairo_font_weight_t weight() const;
	void setWeight( const cairo_font_weight_t& weight );
	void setWeight( const ustring& weight );
	bool underline() const;
	void setUnderline( bool underline );
	Capitalization cap() const;
	void setCap( const Capitalization& cap );
	void setCap( const ustring& cap );
	double size() const;
	void setSize( double size );
	Color color() const;
	void setColor( const Color& color );
	Anchor anchor() const;
	void setAnchor( const Anchor& anchor );
	void setAnchor( const Key::Keys& anchor );
	void setAnchor( const ustring& anchor );
	Align align() const;
	void setAlign( const Align& align );
	void setAlign( const ustring& align );
	ScriptOpt scriptOpt() const;
	void setScriptOpt( const ScriptOpt& scriptopt );
	void setScriptOpt( const ustring& scriptopt );
	TextStyle capStyle() const;
	double rotation() const;
	void setRotation( double rotation );
};
/**********************************************/
/**********************************************/
/**********************************************/
class Label
{
	ustring _text;
	// Input Position
	Point _pos;
	// Position of the baseline,
	// calculate in prepare(...)
	Point _text_pos;
	// Anchor
	Anchor _anchor;
	FontMetrics _metrics;
	Rect _boundingRect;
	//
public:
	Label();
	Label( const ustring& text, const Point& pos, const Anchor& anchor );
	//
	ustring text() const;
	void setText( const ustring& text );
	Point pos() const;
	void setPos( const Point& pos );
	Point textPos() const;
	Anchor anchor() const;
	void setAnchor( const Anchor& anchor );
	FontMetrics metrics() const;
	Rect boundingRect() const;
	void prepare( cairo_t*  drawing, const TextStyle& style = TextStyle() );
	void prepare( Cairo*  drawing, const TextStyle& style = TextStyle() );
	void draw( Cairo& drawing, const TextStyle& style = TextStyle() );
	//
};
/**********************************************/
/**********************************************/
/**********************************************/
class StyledText
{
	ustring _text;
	TextStyle _style;
	// Input Position
	Point _pos;
	// Position of the baseline,
	// calculate in prepare(...)
	Point _text_pos;
	// Anchor
	Anchor _anchor;
	FontMetrics _metrics;
	Rect _boundingRect;
public:
	StyledText();
	StyledText( const ustring& text, const TextStyle& style );
	static void separateTextAndStyle( const ustring& formattedText, ustring& text, TextStyle& style );
	static void buildStyledtextList( const ustring& formattedText, const TextStyle& globalStyle, UList<StyledText>& styled_texts );
	static void separateSmallCaps( const StyledText& styledText, UList<StyledText>& styled_texts );
	ustring text() const;
	void setText( const ustring& text );
	TextStyle style() const;
	void setStyle( const TextStyle& style );
	Point pos() const;
	void setPos( const Point& pos );
	Point textPos() const;
	Anchor anchor() const;
	void setAnchor( const Anchor& anchor );
	FontMetrics metrics() const;
	Rect boundingRect() const;
	void prepare( Cairo* drawing );
};
/**********************************************/
/**********************************************/
/**********************************************/
class LineStyle
{
	// Line cap (CAIRO_LINE_CAP_BUTT, CAIRO_LINE_CAP_ROUND, CAIRO_LINE_CAP_SQUARE)
	UList<double> _dash;
	double _dash_offset = 0.0;
	double _width = 3.0;
	Color _color;
	cairo_line_join_t _join = CAIRO_LINE_JOIN_BEVEL;
	double _miter_limit = 10.0;
	cairo_line_cap_t _cap = CAIRO_LINE_CAP_BUTT;
	double _gap = 0.0;
public:
	LineStyle();
	UList<double> dash() const;
	void setDash( const UList<double>& dash );
	double dashOffset() const;
	void setDashOffset( double dashoffset );
	double width() const;
	void setWidth( double width );
	Color color() const;
	void setColor( const Color& color );
	cairo_line_join_t join() const;
	void setJoin( const cairo_line_join_t& join );
	void setJoin( const ustring& join );
	double miterLimit() const;
	void setMiterLimit( double miter_limit );
	cairo_line_cap_t cap() const;
	void setCap( const cairo_line_cap_t& cap );
	void setCap( const ustring& cap );
	double gap() const;
	void setGap( double gap );
};
/**********************************************/
/**********************************************/
/**********************************************/
class FillStyle
{
public:
	enum Type { Flat, LinearGradient, RadialGradient };
private:
	FillStyle::Type _type = FillStyle::Flat;
	Color _color = { 0, 0, 0, 0 };
	cairo_pattern_t* _linear_gradient;
	cairo_pattern_t* _radial_gradient;
public:
	FillStyle();
	Type type() const;
	void setType( const FillStyle::Type& type );
	Color color() const;
	void setColor( const Color& color );
	cairo_pattern_t* linearPattern() const;
	void setLinearPattern( cairo_pattern_t* linear_gradient );
	cairo_pattern_t* radialPattern() const;
	void setRadialPattern( cairo_pattern_t* radial_gradient );
};
/**********************************************/
/**********************************************/
/**********************************************/
class ShapeStyle
{
public:
	enum Shape
	{
		Cercle,
		Rectangle,
		Star,
		Plus,
		Minus
	};
private:
	FillStyle _fill;
	LineStyle _line;
	Key::Keys _anchor;
public:
	ShapeStyle();
	FillStyle fill() const;
	void setFill( const FillStyle& fill );
	LineStyle line() const;
	void setLine( const LineStyle& line );
	Key::Keys anchor() const;
	void setAnchor( const Key::Keys& anchor );
};
/**********************************************/
/**********************************************/
/**********************************************/
class Symbol
{
public:
	enum Symbols
	{
		Cercle,
		HalfCercle,
		Square,
		Rectangle,
		Triangle,
		Polygon,
		Diamond,
		Hourglass,
		Boomerang,
		Star,
		Plus,
		Minus,
		Line,
		Text
	};
	/**********************************************/
private:
	Symbol::Symbols _type = Symbol::Cercle;
	ustring _option;
	Size _size = {12,12};
	Size _option_size;
	double _begin_angle = 0.0;
	double _end_angle = 360.0;
	double _rotation = 360.0;
	double _covering = 60.0;
	/**********************************************/
public:
	Symbol();
	Symbol( const Symbol::Symbols& type );
	/**********************************************/
	Symbol::Symbols type() const;
	void setType( const Symbol::Symbols& type );
	void setType( const ustring& type );
	ustring option() const;
	void setOption( const ustring& option );
	Size size() const;
	void setSize( const Size& size );
	void setSize( const ustring& size );
	Size optionSize() const;
	void setOptionSize( const Size& size );
	void setOptionSize( const ustring& size );
	double beginAngle() const;
	void setBeginAngle( double angle );
	double endAngle() const;
	void setEndAngle( double angle );
	double rotation() const;
	void setRotation( double rotation );
	double covering() const;
	void setCovering( double covering );
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#include <cmath>
#include <ctime>
#include <iomanip>
#include <sstream>
class Format
{
public:
	enum Type
	{
		Default,
		Number,
		Scientific,
		Product,
//		Date,
		Time,
//		TimeDate,
		Fraction,
		Text
	};
private:
	Format::Type _type = Format::Product;
	// Number Format Settings
	bool _forceSign = false;
	ustring _decimalMarker = ","; // '.'
	int _nbDecimal = 2; // '.'
	bool _forceDecimal = true;
	int _nbIntegerDigits = 8;
	ustring _integerFiller = "0";
	// "fs;dm:.;nd:2;fd;ni:8"
	//
	ustring _productBase = "e"; // e, 2, pi, phi,...
	ustring _productMarker = "times"; // "", ".", "dot", "times",...
	// "pb:10;pm:x"
	ustring _exponentBase = "10"; // e, 2, pi, phi,...
	ustring _exponentMaker = "10"; // E, D, e, 2, pi, phi,..
	double _fixedExponent = 0; //
	bool _forceExponentSign = false;
	bool _tightSpacing = false;
	bool _hideUnityMantissa = true;
	bool _hideZeroExponent = true;
	// "em:10;eb:10;fe:8;fes;ts;sum;sze;"
	double _scale = 1.0;
	// "sc:1.0;"
	// Date Format Settings
	ustring _dateSeparator = "/"; // " ", ":", ...
//	ustring _dateOrder = "%d%m%y";
//	ustring _dateFormat = "%d/%m/%Y";
	ustring _timeFormat = "%d/%m/%Y";
//	ustring _Format = "%d/%m/%Y";
	// "ds:/;do:dmy;"
	// DD/MM/YY
	// DD/MM/YYYY
	// DD MMMM YYYY
	// D MMMM YYYY
	// NN D MM YYYY
	// NN D MM YYYY
	// NNNN D MMMM YYYY
	// Time Format Settings
//	ustring _dateformat = "%d/%m/%Y"; // "%u"
//	ustring _timeSeparator = ":"; // "%u"
//	bool _am_pm = false; //
	// "ts::;"
	// HH:MM:SS AM/PM
	// HH:MM:SS
	// Fraction Format Settings
	// ?/?
	// Text Format Settings
	// @

public:
	Format();
	Format( const ustring& format );
	void setOption( const ustring option );
	void setType( const Format::Type& type );
	void setType( const ustring& type );
//	static inline ustring convert( double number, const ustring& format )
//	{
//		return Format::convert( number, Format(format) );
//	}
	static ustring convert( double number, const Format& format );
	ustring convert( double number ) const;
	ustring convert( int number );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // STYLES_H
