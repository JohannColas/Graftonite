#ifndef GEOMETRY_H
#define GEOMETRY_H
/**********************************************/
#include <iostream>
/**********************************************/
class ustring;
#include "Key.h"
/**********************************************/
/**********************************************/
/**********************************************/
class Point
{
	double _x = 0;
	double _y = 0;
public:
	Point();
	Point( double x, double y );
	Point( const Point& point );
	Point( const ustring& point );
	double x() const;
	void setX( double x );
	double y() const;
	void setY( double y );
	void set( const ustring& point );
	friend std::ostream& operator<< ( std::ostream& os, const Point& p )
	{
		os << "{" << p.x() << "," << p.y() << "}";
		return os;
	}
	Point& operator= ( const Point& point );
};
/**********************************************/
/**********************************************/
/**********************************************/
class Anchor
{
public:
	Anchor();
	Anchor( const Key::Keys& anchor );
	bool isLeft() const;
	bool isRight() const;
	bool isHCenter() const;
	bool isTop() const;
	bool isBottom() const;
	bool isVCenter() const;
	bool isBaseline() const;
	Key::Keys _anchor = Key::Top;
};
/**********************************************/
/**********************************************/
/**********************************************/
class Line
{
public:
	enum Orientation
	{
		Vertical,
		Horizontal
	};
private:
	double _x1 = 0;
	double _y1 = 0;
	double _x2 = 0;
	double _y2 = 0;
public:
	Line();
	Line( const Point& p1, const Point& p2 );
	Line( double x1, double y1, double x2, double y2 );
	Line( double x, double y, double length, const Line::Orientation& orientation = Line::Horizontal );
	double x1() const;
	void setX1( double x1 );
	double y1() const;
	void setY1( double y1 );
	void setP1( const Point& p1 );
	Point p1() const;
	double x2() const;
	void setX2( double x2 );
	double y2() const;
	void setY2( double y2 );
	void setP2( const Point& p2 );
	Point p2() const;
	friend std::ostream& operator<< ( std::ostream& os, const Line& l )
	{
		os << "{" << l.x1() << "," << l.y1() << "," << l.x2() << "," << l.y2() << "}";
		return os;
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
class Size
{
	double _width = 0;
	double _height = 0;
public:
	Size();
	Size( double width, double height );
	Size( const ustring& size );
	double width() const;
	void setWidth( double width );
	double height() const;
	void setHeight( double height );
	friend Size operator* ( double d, const Size& size )
	{
		Size new_size;
		new_size.setWidth( d*size.width() );
		new_size.setHeight( d*size.height() );
		return new_size;
	}
	friend Size operator* ( const Size& size, double d )
	{
		return (d*size);
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
class Rect
{
	double _x = 0;
	double _y = 0;
	double _width = 0;
	double _height = 0;
public:
	Rect();
	Rect( double x, double y, double width, double height );
	Rect( const Point& point, const Size& size );
	Rect( const Line& line );
	Rect( const ustring& rect );
	double x() const;
	void setX( double x );
	double y() const;
	void setY( double y );
	Point point() const;
	Point point( const Key& anchor ) const;
	void setPoint( double x, double y );
	void setPoint( const Point& point );
	double width() const;
	void setWidth( double width );
	double height() const;
	void setHeight( double height );
	Size size() const;
	void setSize( double width, double height );
	void setSize( const Size& size );
	double yTop() const;
	double yCenter() const;
	double yBottom() const;
	double xLeft() const;
	double xCenter() const;
	double xRight() const;
	Point topLeft() const;
	Point top() const;
	Point topRight() const;
	Point centerLeft() const;
	Point center() const;
	Point centerRight() const;
	Point bottomLeft() const;
	Point bottom() const;
	Point bottomRight() const;
	Line topLine() const;
	Line hCenterLine() const;
	Line bottomLine() const;
	Line leftLine() const;
	Line vCenterLine() const;
	Line rightLine() const;
	Line line( const Key& pos, bool vcenter = false ) const;
	bool contains( const Point& pos );
	bool contains( const Line& line );
	bool contains( const Rect& rect );
	void expand( const Point& pos );
	void expand( const Line& line );
	void expand( const Rect& rect );
	friend std::ostream& operator<< ( std::ostream& os, const Rect& rect )
	{
		os << "{" << rect.x() << "," << rect.y() << "," << rect.width() << "," << rect.height() << "}";
		return os;
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // GEOMETRY_H
