#ifndef USTRING_H
#define USTRING_H
/**********************************************/
#include <string>
#include <iostream>
/**********************************************/
#include "UList.h"
#include <cairo.h>
/**********************************************/
class Color;
class Point;
class Size;
class Rect;
/**********************************************/
/**********************************************/
/**********************************************/
class ustring
        : public std::string
{
public:
	ustring();
	ustring( const double d );
	ustring( const char c );
	ustring( const int d );
	ustring( const unsigned d );
	ustring( const long d );
	ustring( const char* c_str );
	ustring( const std::string& str );
	ustring( const ustring& str );
	/**********************************************/
	/* Checkers */
	bool contains( const std::string& str ) const;
	bool contains( const char* c_str ) const;
	UList<int> indexesOf( const ustring& str ) const;
	/* modifiers */
	ustring remove( const ustring& str ) const;
	/**********************************************/
	/* Accessors */
	ustring right( int n = 0 ) const;
	/**********************************************/
	/* Conversions */
	bool isInteger() const;
	bool isDouble() const;
	double toDouble() const;
	std::deque<double> toDoubleList() const;
	bool toBool() const;
	int toInt() const;
	unsigned int toUInt() const;
	long toLong() const;
	unsigned long toULong() const;
	char toChar() const;
	unsigned char toUChar() const;
	Color toColor() const;
	ustring toUpper() const;
	ustring toFirstUpper() const;
	ustring toLower() const;
	/**********************************************/
	UList<ustring> split( const ustring& separator, bool keepEmptyParts = false ) const;
	UList<ustring> decomposeCap() const;
	/**********************************************/
	// Assignment Operators Overloading
	ustring& operator= ( const ustring& str );
	ustring& operator= ( const std::string& str );
	bool operator== ( const UList<ustring>& list ) const;
	ustring& operator= ( const char* s );
	ustring& operator= (char c);
	friend std::ostream& operator<<( std::ostream& os, const ustring& str );
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // USTRING_H
