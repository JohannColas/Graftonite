#ifndef GRAFTONITE_H
#define GRAFTONITE_H
/**********************************************/
#include "Graph/Graph.h"
/**********************************************/
/**********************************************/
/**********************************************/
class Graftonite
{
	Graph _graph = Graph();
public:
	Graftonite( int argc, char *argv[] );
	int exec();
	/**********************************************/
	void decomposeCommandLine( const ustring& cmdline, ustring& cmd, UList<ustring>& args );
	void runCommand( const ustring& cmdline );
	/**********************************************/
	void newElement( UList<ustring>& args );
	void reorder( UList<ustring>& args );
	void insert( UList<ustring>& args );
	void deleteElement( UList<ustring>& args );
	void setCurrentElement( UList<ustring>& args );
	void set( UList<ustring>& args );
	void unset( UList<ustring>& args );
	//
	void open( UList<ustring>& args );
	void load( const UList<ustring>& args );
	void run( UList<ustring>& args );
	//
	void save( UList<ustring>& args );
	void exportGraph( UList<ustring>& args );
	//
	void show( UList<ustring>& args );
	//
	void about();
	void version();
	void help( UList<ustring>& args );
	void exit();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // GRAFTONITE_H
