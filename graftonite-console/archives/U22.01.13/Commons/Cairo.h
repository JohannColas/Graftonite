#ifndef DCAIRO_H
#define DCAIRO_H

#include <cairo/cairo.h>
#include <cairo/cairo-pdf.h>
#include <cairo/cairo-svg.h>
#include <cairo/cairo-ps.h>

#include "Styles.h"

#include <math.h>
#include <iostream>
using namespace std;


class Cairo
{
public:
	enum Format
	{
		PNG,
		SVG,
		PDF,
		PS
	};
private:
	Cairo::Format _format = Cairo::PNG;
	cairo_surface_t* _surface = nullptr;
	cairo_t* _drawing = nullptr;
	string path = "/home/colas/Bureau";
	string filename = "graph";
	Size size = {100,100};
	Point current_pos = {0,0};
	double degree = M_PI/180.0;
public:
	Cairo( const Cairo::Format& format = Cairo::SVG );
	Cairo( const string& format, const string& width, const string& height );
	void setPath( const string& path )
	{
		this->path = path;
	}
	void setFileName( const string& filename )
	{
		this->filename = filename;
	}
	void setFormat( const Cairo::Format& format )
	{
		_format = format;
	}
	void setSize( const Size& size )
	{
		this->size = size;
	}
	cairo_t* create();
	void init();
	cairo_surface_t* surface();
	cairo_t* drawing();
	void saveState();
	void restoreState();
	void moveTo( double x, double y );
	void compose();
	void clip();
	void mask( cairo_surface_t* surface );
	void transform();
	void rotate( double angle );
	void setLineStyle( const LineStyle& style = LineStyle() );
	void setFillStyle( const FillStyle& style = FillStyle() );
	void setTextStyle( const TextStyle& style = TextStyle() );
	void drawLine( const Point& p1, const Point& p2, const LineStyle& style = LineStyle() );
	void drawPath( const deque<Point>& points, const LineStyle& style = LineStyle() );
	void drawClosedPath( const deque<Point>& points, const ShapeStyle& style = ShapeStyle() );
	void drawRect( const Point& p1, const Point& p2, const ShapeStyle& style = ShapeStyle() );
	void drawRect( const Point& pos, const Size& size, const ShapeStyle& style = ShapeStyle() );
	void drawRoundedRect( const Point& p1, const Point& p2, double radius, const ShapeStyle& style = ShapeStyle() );
	void drawArc( const Point& center, double radius, double angle_start, double angle_end, const ShapeStyle& style = ShapeStyle() );
	void drawCercle( const Point& center, double radius, const ShapeStyle& style = ShapeStyle() );
	void drawelipse( const Point& center, double x_radius, double y_radius, const ShapeStyle& style = ShapeStyle() );
	void drawShape( const ShapeStyle::Shape& shape, const Size& size, const ShapeStyle& style = ShapeStyle() );
	Rect drawText( const string& text, double x, double y, const TextStyle& style = TextStyle() );
	void drawImage( const string& imagepath, const Point& pos );
	void close();
};

#endif // DCAIRO_H
