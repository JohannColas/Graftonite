#ifndef STYLES_H
#define STYLES_H
/**********************************************/
#include <cairo/cairo.h>
#include "UString.h"
/**********************************************/
/**********************************************/
/**********************************************/
class Color
{
	double _red = 0.0;
	double _green = 0.0;
	double _blue = 0.0;
	double _alpha = 1.0;
public:
	Color();
	Color( double red, double green, double blue, double alpha = 1.0 );
	Color( int red, int green, int blue, int alpha = 255 );
	Color( const string& color );
	double red() const;
	void setRed( double red );
	void setRed( int red );
	double green() const;
	void setGreen( double green );
	void setGreen( int green );
	double blue() const;
	void setBlue( double blue );
	void setBlue( int blue );
	double alpha() const;
	void setAlpha( double alpha );
	void setAlpha( int alpha );
	void setColor( const ustring& color );
	static string getColorByName( const ustring& name );
};
/**********************************************/
/**********************************************/
/**********************************************/
class Point
{
	double _x = 0;
	double _y = 0;
public:
	Point( double x, double y );
	double x() const;
	void setX( double x );
	double y() const;
	void setY( double y );
};
/**********************************************/
/**********************************************/
/**********************************************/
class Size
{
	double _width = 200;
	double _height = 100;
public:
	Size( double width, double height );
	double width() const;
	void setWidth( double width );
	double height() const;
	void setHeight( double height );
};
/**********************************************/
/**********************************************/
/**********************************************/
class Rect
{
	double _x = 0;
	double _y = 0;
	double _width = 200;
	double _height = 100;
public:
	Rect();
	Rect( double x, double y, double width, double height );
	Rect( const Point& point, const Size& size );
	double x() const;
	void setX( double x );
	double y() const;
	void setY( double y );
	Point point() const;
	void setPoint( double x, double y );
	void setPoint( const Point& point );
	double width() const;
	void setWidth( double width );
	double height() const;
	void setHeight( double height );
	Size size() const;
	void setSize( double width, double height );
	void setSize( const Size& size );
};
/**********************************************/
/**********************************************/
/**********************************************/
enum Anchor
{
	Bottom=3,
	Left=5,
	Top=7,
	Right=9,
	Center=1,
	TopLeft=35,
	TopRight=63,
	BottomLeft=15,
	BottomRight=27,
	Baseline=11,
	BaselineLeft=55,
	BaselineRight=99
};
/**********************************************/
/**********************************************/
/**********************************************/
class FontMetrics
{
	double _ascent = -1;
	double _descent = -1;
	double _height = -1;
	double _tight_height = -1;
	double _width = -1;
	double _tight_width = -1;
	double _x_bearing = -1;
	double _y_bearing = -1;
public:
	FontMetrics();
	FontMetrics( cairo_t* drawing );
	FontMetrics( const string& text, cairo_t* drawing );
	void init( cairo_t* drawing );
	void init( const string& text, cairo_t* drawing );
	double ascent() const;
	void setAscent( double ascent );
	double descent() const;
	void setDescent( double descent );
	double height() const;
	void setHeight( double height );
	double tightHeight() const;
	void setTightHeight( double tightHeight );
	double width() const;
	void setWidth( double width );
	double tightWidth() const;
	void setTightWidth( double tightWidth );
	double xBearing() const;
	void setXBearing( double xBearing );
	double yBearing() const;
	void setYBearing( double yBearing );
};
/**********************************************/
/**********************************************/
/**********************************************/
class TextStyle
{
public:
	// ------------------------------------
	// For capitalization
	// Font size of small cap is equal to :
	// smallcaps_font_size = 0.8 * original_font_size + 0.1
	// char value of a = 97 / z = 122 and of A = 65 / Z = 90 -> Thus remove 32
	// ------------------------------------
	enum Capitalization
	{
		NoCap,
		SmallCap,
		Cap
	};
	enum Align
	{
		Center,
		Justify,
		Left,
		Right
	};
	enum ScriptOpt
	{
		NormalScript,
		SuperScript,
		SubScript,
	};
private:
	string _family = "Sans Serif";
	cairo_font_slant_t _slant = CAIRO_FONT_SLANT_NORMAL;
	cairo_font_weight_t _weight = CAIRO_FONT_WEIGHT_NORMAL;
	Capitalization _cap = NoCap;
	double _size = 10;
	Color _color;
	Anchor _anchor = Anchor::Left;
	Align _align = Align::Left;
	ScriptOpt _scriptopt = TextStyle::NormalScript;
	bool _underline = false;
public:
	TextStyle();
	TextStyle( const string& style );
	FontMetrics metrics( const string& text, cairo_t* drawing ) const;
	void set( const string& style );
	string family() const;
	cairo_font_slant_t slant() const;
	cairo_font_weight_t weight() const;
	Capitalization cap() const;
	double size() const;
	void setSize( double size );
	Color color() const;
	Anchor anchor() const;
	void setAnchor( const Anchor& anchor );
	Align align() const;
	ScriptOpt scriptOpt() const;
	bool underline() const;
};
/**********************************************/
/**********************************************/
/**********************************************/
class StyledText
{
	string _text;
	TextStyle _style;
public:
	StyledText();
	StyledText( const string& text, const TextStyle& style );
	string text() const;
	TextStyle style() const;
};
/**********************************************/
/**********************************************/
/**********************************************/
class LineStyle
{
	// Line cap (CAIRO_LINE_CAP_BUTT, CAIRO_LINE_CAP_ROUND, CAIRO_LINE_CAP_SQUARE)
	cairo_line_cap_t _cap = CAIRO_LINE_CAP_BUTT;
	cairo_line_join_t _join = CAIRO_LINE_JOIN_BEVEL;
	double _miter_limit = 10.0;
	double _width = 2.0;
	Color _color;
	deque<double> _dash;
public:
	LineStyle();
	cairo_line_cap_t cap() const;
	void setCap( const cairo_line_cap_t& cap );
	void setCap( const ustring& cap );
	cairo_line_join_t join() const;
	void setJoin( const cairo_line_join_t& join );
	double miterLimit() const;
	void setMiterLimit( double miter_limit );
	double width() const;
	void setWidth( double width );
	Color color() const;
	void setColor( const Color& color );
	deque<double> dash() const;
	void setDash( const deque<double>& dash );
};
/**********************************************/
/**********************************************/
/**********************************************/
class FillStyle
{
public:
	enum Type { Flat, LinearGradient, RadialGradient };
private:
	FillStyle::Type _type = FillStyle::Flat;
	Color _color = { 0, 0, 0, 0 };
	cairo_pattern_t* _linear_gradient;
	cairo_pattern_t* _radial_gradient;
public:
	FillStyle();
	Type type() const;
	void setType( const FillStyle::Type& type );
	cairo_pattern_t* linearPattern() const;
	void setLinearPattern( cairo_pattern_t* linear_gradient );
	cairo_pattern_t* radialPattern() const;
	void setRadialPattern( cairo_pattern_t* radial_gradient );
	Color color() const;
	void setColor( const Color& color );
};
/**********************************************/
/**********************************************/
/**********************************************/
class ShapeStyle
{
public:
	enum Shape
	{
		Cercle,
		Rectangle,
		Star,
		Plus,
		Minus
	};
private:
	FillStyle _fill;
	LineStyle _line;
	Anchor _anchor;
public:
	ShapeStyle();
	FillStyle fill() const;
	void setFill( const FillStyle& fill );
	LineStyle line() const;
	void setLine( const LineStyle& line );
	Anchor anchor() const;
	void setAnchor( const Anchor& anchor );
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // STYLES_H
