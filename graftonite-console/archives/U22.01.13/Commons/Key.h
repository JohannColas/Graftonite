#ifndef KEY_H
#define KEY_H
/**********************************************/
#include "UString.h"
#include "UList.h"
/**********************************************/
/**********************************************/
/**********************************************/
class Key
{
	ustring _key;
	UList<ustring> _abrvs;
public:
	Key();
	Key( const ustring& key );
	Key( const ustring& key, const ustring& abrv );
	Key( const ustring& key, const UList<ustring>& abrv );

	UList<ustring> abrvs() const;
	ustring key() const;
	bool isEmpty() const;

	bool operator==( const ustring& key ) const;
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // KEY_H
