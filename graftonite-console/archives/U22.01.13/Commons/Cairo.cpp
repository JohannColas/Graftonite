#include "Cairo.h"

#include <algorithm>
using namespace std;


Cairo::Cairo( const Cairo::Format& format )
{
	_format = format;
}

Cairo::Cairo( const string& format, const string& width, const string& height )
{
	if ( format == "svg" )
		_format = Cairo::SVG;
	else if ( format == "pdf" )
		_format = Cairo::PDF;
	else if ( format == "png" )
		_format = Cairo::PNG;
	else if ( format == "ps" )
		_format = Cairo::PS;
	size.setWidth( stod( width ) );
	size.setHeight( stod( height ) );
}

cairo_t* Cairo::create()
{
	init();

	return _drawing;
}

void Cairo::init()
{
	//	close();
	switch ( _format )
	{
		case Cairo::PNG:
			_surface = cairo_image_surface_create(
						   CAIRO_FORMAT_ARGB32,
						   size.width(),
						   size.height() );
		break;
		case Cairo::PDF:
			_surface = cairo_pdf_surface_create (
						   string(path + '/' + filename +".pdf").c_str(),
						   size.width(),
						   size.height() );
		break;
		case Cairo::SVG:
			_surface = cairo_svg_surface_create(
						   string(path + '/' + filename +".svg").c_str(),
						   size.width(),
						   size.height() );
		break;
		case Cairo::PS:
			_surface = cairo_ps_surface_create(
						   string(path + '/' + filename +".ps").c_str(),
						   size.width(),
						   size.height() );
		break;
	}

	if ( _surface != nullptr )
		_drawing = cairo_create( _surface );

	// CAIRO_ANTIALIAS_  DEFAULT/NONE/GRAY/SUBPIXEL/FAST/GOOD/BEST
	cairo_set_antialias( _drawing, CAIRO_ANTIALIAS_GOOD );
}

cairo_surface_t* Cairo::surface()
{
	return _surface;
}

cairo_t* Cairo::drawing()
{
	return _drawing;
}

void Cairo::saveState()
{
	cairo_save( _drawing );
}

void Cairo::restoreState()
{
	cairo_restore( _drawing );
}

void Cairo::moveTo( double x, double y )
{
	current_pos.setX( x - current_pos.x() );
	current_pos.setY( y - current_pos.y() );
	cairo_translate( _drawing, current_pos.x(), current_pos.y() );
}

void Cairo::compose()
{
	cairo_t *first_cr, *second_cr;
	cairo_surface_t *first, *second;

	first = cairo_surface_create_similar(cairo_get_target( _drawing ),
										 CAIRO_CONTENT_COLOR_ALPHA, 100, 35);

	second = cairo_surface_create_similar(cairo_get_target( _drawing ),
										  CAIRO_CONTENT_COLOR_ALPHA, 100, 21);

	first_cr = cairo_create(first);
	cairo_set_source_rgb(first_cr, 0, 0, 0.4);
	cairo_rectangle(first_cr, 0, 20, 50, 50);
	cairo_fill(first_cr);

	second_cr = cairo_create(second);
	cairo_set_source_rgb(second_cr, 0.5, 0.5, 0);
	cairo_rectangle(second_cr, 0+10, 40, 50, 50);
	cairo_fill(second_cr);

	// CAIRO_OPERATOR_DEST_OVER,
	// CAIRO_OPERATOR_DEST_IN,
	// CAIRO_OPERATOR_OUT,
	// CAIRO_OPERATOR_ADD,
	// CAIRO_OPERATOR_ATOP,
	// CAIRO_OPERATOR_DEST_ATOP,
	cairo_set_operator(first_cr, CAIRO_OPERATOR_DEST_OVER);
	cairo_set_source_surface(first_cr, second, 0, 0);
	cairo_paint(first_cr);

	cairo_set_source_surface( _drawing, first, 0, 0);
	cairo_paint( _drawing );

	cairo_surface_destroy(first);
	cairo_surface_destroy(second);

	cairo_destroy(first_cr);
	cairo_destroy(second_cr);
}

void Cairo::clip()
{
	cairo_arc( _drawing, 0, 0, 50, 0, 2*M_PI);
	cairo_clip( _drawing );
	cairo_paint( _drawing );
}

void Cairo::mask( cairo_surface_t* surface )
{
	cairo_mask_surface( _drawing, surface, 0, 0);
	cairo_fill( _drawing );
}

void Cairo::transform()
{
	// Translation
	cairo_translate( _drawing, 20, 20);

	// Rotation
	cairo_rotate( _drawing, M_PI*0.5 );

	// Shear
	cairo_matrix_t matrix;
	cairo_matrix_init(&matrix,
					  1.0, 0.5,
					  0.0, 1.0,
					  0.0, 0.0);
	cairo_transform( _drawing, &matrix);

	// Scaling
	cairo_scale( _drawing, 0.6, 0.6);

	// Isolated Transformationcairo_save( _drawing );
	cairo_scale( _drawing, 0.6, 0.6);
	cairo_set_source_rgb( _drawing, 0.8, 0.3, 0.2);
	cairo_rectangle( _drawing, 30, 30, 90, 90);
	cairo_fill( _drawing );
	cairo_restore( _drawing );

}

void Cairo::rotate( double angle )
{
	cairo_rotate( _drawing, angle * M_PI / 180 );
}

void Cairo::setLineStyle( const LineStyle& style )
{
	// Line color
	cairo_set_source_rgba( _drawing, style.color().red(), style.color().green(), style.color().blue(), style.color().alpha() );
	// Line width
	cairo_set_line_width( _drawing, style.width() );
	// Line Dashes
	if ( !style.dash().empty() )
	{
		int dash_len = style.dash().size();
		double dash_tmp[dash_len];
		for ( int dt = 0; dt < dash_len; ++dt )
			dash_tmp[dt] = style.dash().at(dt);
		cairo_set_dash( _drawing, dash_tmp, dash_len, 0 );
	}
	// Line Cap
	cairo_set_line_cap( _drawing, style.cap() );
	// Line Join
	cairo_set_line_join( _drawing, style.join() );
	if ( style.join() == CAIRO_LINE_JOIN_MITER )
		cairo_set_miter_limit( _drawing, style.miterLimit() );
}

void Cairo::setFillStyle( const FillStyle& style )
{
	switch ( style.type() )
	{
		case FillStyle::Flat:
			cairo_set_source_rgba( _drawing, style.color().red(), style.color().green(), style.color().blue(), style.color().alpha() );
		break;
		case FillStyle::LinearGradient:
			cairo_pattern_t *pat3;
			pat3 = cairo_pattern_create_linear(20.0, 260.0, 20.0, 360.0);
			cairo_pattern_add_color_stop_rgb(pat3, 0.1, 0, 0, 0);
			cairo_pattern_add_color_stop_rgb(pat3, 0.5, 1, 1, 0);
			cairo_pattern_add_color_stop_rgb(pat3, 0.9, 0, 0, 0);
			cairo_set_source( _drawing, pat3);
		break;
		case FillStyle::RadialGradient:
			cairo_pattern_t *r1;
			r1 = cairo_pattern_create_radial(30, 30, 10, 30, 30, 90);
			cairo_pattern_add_color_stop_rgba(r1, 0, 1, 1, 1, 1);
			cairo_pattern_add_color_stop_rgba(r1, 1, 0.6, 0.6, 0.6, 1);
			cairo_set_source( _drawing, r1);
		break;
		default:
		break;
	}
}

void Cairo::setTextStyle( const TextStyle& style )
{
	// Text Color
	cairo_set_source_rgba( _drawing, style.color().red(), style.color().green(), style.color().blue(), style.color().alpha() );
	// Text font
	cairo_select_font_face( _drawing,
							style.family().c_str(),
							style.slant(),
							style.weight() );
	// Text Font size
	cairo_set_font_size( _drawing, style.size() );
}

void Cairo::drawLine( const Point& p1, const Point& p2, const LineStyle& style )
{
	cairo_move_to( _drawing, p1.x(), p1.y() );
	cairo_line_to( _drawing, p2.x(), p2.y() );

	setLineStyle( style );
	cairo_stroke( _drawing );
}

void Cairo::drawPath( const deque<Point>& points, const LineStyle& style )
{
	if ( points.empty() )
		return;

	cairo_move_to( _drawing, points.at(0).x(), points.at(0).y() );
	for ( unsigned it = 1; it < points.size(); ++it )
		cairo_line_to( _drawing, points.at(it).x(), points.at(it).y() );

	setLineStyle( style );
	cairo_stroke( _drawing );
}

void Cairo::drawClosedPath( const deque<Point>& points, const ShapeStyle& style )
{
	if ( points.empty() )
		return;

	cairo_move_to( _drawing, points.at(0).x(), points.at(0).y() );
	for ( unsigned it = 1; it < points.size(); ++it )
		cairo_line_to( _drawing, points.at(it).x(), points.at(it).y() );
	cairo_close_path( _drawing );

	setFillStyle( style.fill() );
	cairo_fill_preserve( _drawing );

	setLineStyle( style.line() );
	cairo_stroke( _drawing );
}

void Cairo::drawRect( const Point& p1, const Point& p2, const ShapeStyle& style )
{
	cairo_move_to( _drawing, p1.x(), p1.y() );
	cairo_line_to( _drawing, p2.x(), p1.y() );
	cairo_line_to( _drawing, p2.x(), p2.y() );
	cairo_line_to( _drawing, p1.x(), p2.y() );
	cairo_close_path( _drawing );

	setFillStyle( style.fill() );
	cairo_fill_preserve( _drawing );

	setLineStyle( style.line() );
	cairo_stroke( _drawing );
}

void Cairo::drawRect( const Point& pos, const Size& size, const ShapeStyle& style )
{
	Point p2 = {pos.x()+size.width(), pos.y()+size.height()};
	drawRect( pos, p2, style );
}

void Cairo::drawRoundedRect( const Point& p1, const Point& p2, double radius, const ShapeStyle& style )
{
	double x1 = p1.x();
	double x2 = p2.x();
	if ( x2 < x1 )
	{
		double tmp = x2;
		x2 = x1;
		x1 = tmp;
	}
	double y1 = p1.y();
	double y2 = p2.y();
	if ( y2 < y1 )
	{
		double tmp = y2;
		y2 = y1;
		y1 = tmp;
	}

	cairo_new_path( _drawing );
	cairo_arc( _drawing, x2 - radius, y1 + radius, radius, -0.5*M_PI, 0 );
	cairo_arc( _drawing, x2 - radius, y2 - radius, radius, 0, 0.5*M_PI );
	cairo_arc( _drawing, x1 + radius, y2 - radius, radius, 0.5*M_PI, M_PI );
	cairo_arc( _drawing, x1 + radius, y1 + radius, radius, M_PI, 1.5*M_PI );
	cairo_close_path( _drawing );

	setFillStyle( style.fill() );
	cairo_fill_preserve( _drawing );

	setLineStyle( style.line() );
	cairo_stroke( _drawing );
}

void Cairo::drawArc( const Point& center, double radius, double angle_start, double angle_end, const ShapeStyle& style )
{
	cairo_arc( _drawing, center.x(), center.y(), radius, angle_start * degree, angle_end * degree );

	setFillStyle( style.fill() );
	cairo_fill_preserve( _drawing );

	setLineStyle( style.line() );
	cairo_stroke( _drawing );
}

void Cairo::drawCercle( const Point& center, double radius, const ShapeStyle& style )
{
	cairo_move_to( _drawing, center.x()+radius, center.y() );
	cairo_arc( _drawing, center.x(), center.y(), radius, 0, 2*M_PI );
	cairo_close_path( _drawing );

	setFillStyle( style.fill() );
	cairo_fill_preserve( _drawing );

	setLineStyle( style.line() );
	cairo_stroke( _drawing );
}

void Cairo::drawelipse( const Point& center, double x_radius, double y_radius, const ShapeStyle& style )
{
	double scale = y_radius/x_radius;
	cairo_save( _drawing );
	cairo_scale( _drawing, 1, scale );
	cairo_move_to( _drawing, center.x()+x_radius, center.y()/scale );
	cairo_arc( _drawing, center.x(), center.y()/scale, x_radius, 0, 2*M_PI );
	cairo_close_path( _drawing );
	cairo_restore( _drawing );

	setFillStyle( style.fill() );
	cairo_fill_preserve( _drawing );

	setLineStyle( style.line() );
	cairo_stroke( _drawing );
}

void Cairo::drawShape( const ShapeStyle::Shape& /*shape*/, const Size& /*size*/, const ShapeStyle& style )
{
	setFillStyle( style.fill() );
	cairo_fill_preserve( _drawing );

	setLineStyle( style.line() );
	cairo_stroke( _drawing );
}

Rect Cairo::drawText( const string& text, double x, double y, const TextStyle& style )
{
	setTextStyle( style );

	deque<StyledText> styled_texts;
	deque<string> lines;
	deque<bool> barres;

//	string test = "<style=family:fsdhfhjsd;color:0.15,0.48,0.75;>Ti<b>tle</style> <style=>i.</style><style=>E</style> <style=super;bold>(MPa)</style> dgdsghds";
	string tmp;
	string style_str;
	for ( unsigned it = 0; it < text.size(); ++it )
	{
		string bal_tmp = text.substr(it,7);
		if ( bal_tmp == "<style=" )
		{
			if ( !tmp.empty() )
				styled_texts.push_back({tmp,style}), tmp.clear();
			unsigned beg_index = text.find( ">", it );
			style_str = text.substr(it+7,beg_index-it-7);
			TextStyle sty = style;
			sty.set( style_str );
			unsigned end_index = text.find( "</style>", ++beg_index );
			tmp = text.substr( beg_index, end_index - beg_index );
			styled_texts.push_back( {tmp, sty} );
			tmp.clear();
			it = end_index+7;
		}
		else
			tmp += text.at(it);
	}
	if ( !tmp.empty() )
		styled_texts.push_back({tmp,style}), tmp.clear();

	FontMetrics full_metrics;
	for ( const StyledText& part : styled_texts )
	{
		setTextStyle( part.style() );
		FontMetrics metrics = part.style().metrics( part.text(), _drawing );
		if ( metrics.height() > full_metrics.height() )
		{
			full_metrics.setHeight( metrics.height() );
			full_metrics.setAscent( metrics.ascent() );
		}
		if ( part.style().cap() == TextStyle::NoCap )
		{
			full_metrics.setWidth( full_metrics.width() + metrics.width() );
		}
		else if ( part.style().cap() == TextStyle::SmallCap )
		{
			TextStyle capStyle = part.style();
			capStyle.setSize( 0.8*capStyle.size()+0.1 );
			string tmp;
			for ( unsigned it = 0; it < part.text().length(); it++ )
			{
				string cstr = part.text().substr( it, 1 );
				char c = cstr.at(0);
				if ( c > 96 && c < 123 )
				{
					if ( !tmp.empty() )
					{
						setTextStyle( part.style() );
						FontMetrics metrics = part.style().metrics( tmp, _drawing );
						full_metrics.setWidth( full_metrics.width() + metrics.width() );
						tmp.clear();
					}
					c -= 32;
					char character[1];
					*character = c;
					setTextStyle( capStyle );
					FontMetrics metrics = part.style().metrics( character, _drawing );
					full_metrics.setWidth( full_metrics.width() + metrics.width() );
				}
				else
					tmp.append( cstr );
			}
			if ( !tmp.empty() )
			{
				setTextStyle( part.style() );
				FontMetrics metrics = part.style().metrics( tmp, _drawing );
				full_metrics.setWidth( full_metrics.width() + metrics.width() );
			}
		}
		else if ( part.style().cap() == TextStyle::Cap )
		{
			string tmp = part.text();
			for ( unsigned it = 0; it < part.text().length(); it++ )
			{
				char c = part.text().at(it);
				if ( c > 96 && c < 123 )
					tmp.replace( it, 1, 1, c-32 );
			}
			setTextStyle( part.style() );
			FontMetrics metrics = part.style().metrics( tmp.c_str(), _drawing );
			full_metrics.setWidth( full_metrics.width() + metrics.width() );
		}
	}

	double nx = x;
	if ( style.anchor() == Anchor::Center ||
		 style.anchor() == Anchor::Top ||
		 style.anchor() == Anchor::Bottom )
	{
		nx -= 0.5*full_metrics.width();
	}
	else if ( style.anchor() == Anchor::Right ||
			  style.anchor() == Anchor::BaselineRight ||
			  style.anchor() == Anchor::TopRight ||
			  style.anchor() == Anchor::BottomRight )
	{
		nx -= full_metrics.width();
	}

	double ny = y + full_metrics.ascent();
	if ( style.anchor() == Anchor::Baseline ||
		 style.anchor() == Anchor::BaselineLeft ||
		 style.anchor() == Anchor::BaselineRight )
	{
		ny -= full_metrics.ascent();
	}
	else if ( style.anchor() == Anchor::Center ||
			  style.anchor() == Anchor::Left ||
			  style.anchor() == Anchor::Right )
	{
		ny -= 0.5*full_metrics.height();
	}
	else if ( style.anchor() == Anchor::Bottom ||
			  style.anchor() == Anchor::BottomLeft ||
			  style.anchor() == Anchor::BottomRight )
	{
		ny -= full_metrics.height();
	}
//	ShapeStyle rstyle;
//	rstyle.fill.color = Color(0.9, 0.2, 0.2, 0.0);
//	rstyle.line.color = {0.9, 0.2, 0.2};
//	rstyle.line.width = 0.25;
//	drawRect( {nx,ny-full_metrics.ascent}, Size({full_metrics.width,full_metrics.height}), rstyle);

//	drawLine( {nx+0.5*full_metrics.width,ny-full_metrics.ascent}, {nx+0.5*full_metrics.width,ny-full_metrics.ascent+full_metrics.height } );

	Rect boundingRect( nx,
					   ny-full_metrics.ascent(),
					   full_metrics.width(),
					   full_metrics.height() );

	for ( const StyledText& part : styled_texts )
	{
		setTextStyle( part.style() );
		cairo_text_extents_t te;
		cairo_move_to( _drawing, nx, ny);
		if ( part.style().cap() == TextStyle::NoCap )
		{
			cairo_show_text( _drawing, part.text().c_str() );
			cairo_text_extents( _drawing, part.text().c_str(), &te );
			nx += te.x_advance ;
			ny += 0;
		}
		else if ( part.style().cap() == TextStyle::SmallCap )
		{
			TextStyle capStyle = part.style();
			capStyle.setSize( 0.8*capStyle.size()+0.1 );
			string tmp;
			for ( unsigned it = 0; it < part.text().length(); it++ )
			{
				string cstr = part.text().substr( it, 1 );
				char c = cstr.at(0);
				if ( c > 96 && c < 123 )
				{
					if ( !tmp.empty() )
					{
						setTextStyle( part.style() );
						cairo_show_text( _drawing, tmp.c_str() );
						cairo_text_extents( _drawing, tmp.c_str(), &te );
						nx += te.x_advance ;
						ny += 0;
						cairo_move_to( _drawing, nx, ny);
						tmp.clear();
					}
					setTextStyle( capStyle );
					c -= 32;
					char character[1];
					*character = c;
					cairo_show_text( _drawing, character );
					cairo_text_extents( _drawing, character, &te );
					nx += te.x_advance ;
					ny += 0;
					cairo_move_to( _drawing, nx, ny);
				}
				else
					tmp.append( cstr );
			}
			if ( !tmp.empty() )
			{
				setTextStyle( part.style() );
				cairo_show_text( _drawing, tmp.c_str() );
				cairo_text_extents( _drawing, tmp.c_str(), &te );
			}
		}
		else if ( part.style().cap() == TextStyle::Cap )
		{
			string tmp = part.text();
			for ( unsigned it = 0; it < part.text().length(); it++ )
			{
				char c = part.text().at(it);
				if ( c > 96 && c < 123 )
					tmp.replace( it, 1, 1, c-32 );
			}
			setTextStyle( part.style() );
			cairo_show_text( _drawing, tmp.c_str() );
			cairo_text_extents( _drawing, tmp.c_str(), &te );
			nx += te.x_advance ;
			ny += 0;
		}

	}

	// Text Shading
	//	cairo_set_source_rgb( _drawing, 0, 0, 0);
	//	cairo_move_to( _drawing, x, y);
	//	cairo_show_text( _drawing, text);
	//	cairo_set_source_rgb( _drawing, 0.5, 0.5, 0.5);
	//	cairo_move_to( _drawing, x, y);
	//	cairo_show_text( _drawing, text);
	// Text with gradient
	//	cairo_pattern_t *pat;
	//	pat = cairo_pattern_create_linear(0, 15, 0, 20);
	//	cairo_pattern_set_extend(pat, CAIRO_EXTEND_REPEAT);
	//	cairo_pattern_add_color_stop_rgb(pat, 0.0, 1, 0.6, 0);
	//	cairo_pattern_add_color_stop_rgb(pat, 0.5, 1, 0.3, 0);
	//	cairo_move_to( _drawing, x, y);
	//	cairo_text_path( _drawing, text );
	//	cairo_set_source( _drawing, pat );
	//	cairo_fill( _drawing );
	// Draw Text
	return boundingRect;
}

void Cairo::drawImage( const string& imagepath, const Point& pos )
{
	cairo_surface_t *image = cairo_image_surface_create_from_png( imagepath.c_str() );
	cairo_set_source_surface( _drawing, image, pos.x(), pos.y() );
	cairo_paint( _drawing );
}

void Cairo::close()
{
	if ( _format == Cairo::PNG )
		cairo_surface_write_to_png( _surface,
									string(path + '/' + filename +".png").c_str() );

	if ( _drawing != nullptr )
		cairo_destroy( _drawing ), _drawing = nullptr;
	if ( _surface != nullptr )
		cairo_surface_destroy(_surface), _surface = nullptr;
}
