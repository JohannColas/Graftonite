#ifndef UCONSOLE_H
#define UCONSOLE_H
/**********************************************/
#include <termios.h>
/**********************************************/
#include "Commons/UString.h"
class Graftonite;
/**********************************************/
/**********************************************/
/**********************************************/
class UConsole
{
	static inline ustring _progname = "graftonite";
	// Terminal Variables & Commands
	static inline struct termios old_tio = termios(), new_tio = termios();
	/**********************************************/
	static inline Graftonite* _app = nullptr;
	/**********************************************/
	static inline vector<ustring> cmd_history;
	/**********************************************/
public:
	/* Terminal Core */
	static void init();
	static void close();
	static void exec( Graftonite* app = nullptr );
	static void clear();
	static inline vector<ustring> CommandHistory()
	{
		return cmd_history;
	}
	/**********************************************/
	/**********************************************/
	/* Cursor Positionning */
	static void changeCursorCol( int delta );
	static void moveCursorBackward( int delta );
	static void moveCursorForward( int delta );
	static void saveCursorPos();
	static void restoreCursorPos();
	/**********************************************/
	/**********************************************/
	/* Colors */
	static void normalColor();
	static void cmdInputColor();
	static void cmdColor();
	static void warningColor();
	static void errorColor();
	static void testColors();
	/**********************************************/
	/**********************************************/
	/* Messages */
	static void warning_message( const ustring& message );
	static void error_message( const ustring& message );
	/**********************************************/
	/**********************************************/
	/* Dialogs */
	static bool confirmationDialog( const ustring& message );
	/**********************************************/
	/**********************************************/
	/* Command Lines Format */
	static void startCommandLine();
	static void clearCommandLine();
	static void drawCommandLine( const ustring& cmdline );
	static void deleteToEndLine();
	/**********************************************/
	/**********************************************/
	enum KEYS
	{
		ESC = 27,
		UP_ARROW = 65,
		DOWN_ARROW = 66,
		RIGHT_ARROW = 67,
		LEFT_ARROW = 68,
		FN_RIGHT = 70,
		FN_LEFT = 72,
		DEL = 126,
		RETURN = 10,
		BACKSPACE = 127,
	};
	/**********************************************/
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // UCONSOLE_H
// ---------------------------------------
// ---------------------------------------
// ---------------------------------------
// HELP ----------------------------------
// Console Cursor Position
//- Position the Cursor:
//  \033[<L>;<C>H
//     Or
//  \033[<L>;<C>f
//  puts the cursor at line L and column C.
//- Move the cursor up N lines:
//  \033[<N>A
//- Move the cursor down N lines:
//  \033[<N>B
//- Move the cursor forward N columns:
//  \033[<N>C
//- Move the cursor backward N columns:
//  \033[<N>D

//- Clear the screen, move to (0,0):
//  \033[2J
//- Erase to end of line:
//  \033[K

//- Save cursor position:
//  \033[s
//- Restore cursor position:
//  \033[u
// ---------------------------------------
// ---------------------------------------
// ---------------------------------------

