#ifndef ULIST_H
#define ULIST_H
/**********************************************/
#include <deque>
#include <vector>
#include <iostream>
using namespace std;
/**********************************************/
/**********************************************/
/**********************************************/
template <class T>
class UList
{
private:
	deque<T> _list;
public:
	UList();
	UList( const UList& list );
	UList( const deque<T>& list );
	UList( const std::initializer_list<T>& args );
	unsigned long size() const;
	T at( unsigned long index ) const;
	void pushFirst( T const& );
	void append( T const& );
	void append( UList<T> const& );
	void popFirst();
	void popLast();
	T first() const;
	T last() const;
	T takeFirst();
	T takeLast();
	T takeAt( unsigned long index );
	void replace( unsigned long index, const T& el );
	bool contains( const T& el ) const;
	void clear();
	void deleteAll();

	bool isEmpty() const
	{
		return _list.empty();
	}
	bool isValid( unsigned index ) const
	{
		return (index >= 0 || index < size());
	}

	// -----------------------------------
	typename std::deque<T>::iterator begin()
	{
		return _list.begin();
	}
	typename std::deque<T>::const_iterator begin() const
	{
		return _list.begin();
	}
	typename std::deque<T>::const_iterator cbegin() const
	{
		return begin();
	}
	typename std::deque<T>::iterator end()
	{
		return _list.end();
	}
	typename std::deque<T>::const_iterator end() const
	{
		return _list.end();
	}
	typename std::deque<T>::const_iterator cend() const
	{
		return end();
	}
	// -----------------------------------
	UList<T>& operator= ( const UList<T>& list )
	{
		(*this).clear();
		for ( auto el : list )
			(*this).append(el);
		return *this;
	}
	// -----------------------------------
};

template <class T>
UList<T>::UList()
{

}

template<class T>
UList<T>::UList( const UList& list )
{
	for ( auto el : list )
		this->append(el);
}

template<class T>
UList<T>::UList( const deque<T>& list )
{
	_list = list;
}

template<class T>
UList<T>::UList( const std::initializer_list<T>& args )
{
	for ( auto el : args )
		this->append(el);
}

template<class T>
unsigned long UList<T>::size() const
{
	return _list.size();
}

template<class T>
T UList<T>::at( unsigned long index ) const
{
	return _list.at(index);
}

template <class T>
void UList<T>::pushFirst( T const& elem )
{
	_list.push_front(elem);
}

template <class T>
void UList<T>::append( T const& elem )
{
	_list.push_back(elem);
}

template<class T>
void UList<T>::append( const UList<T>& list )
{
	for ( auto el : list )
		this->append( el );
}

template <class T>
T UList<T>::takeFirst()
{
	T el = _list.front();
	_list.pop_front();
	return el;
}

template <class T>
T UList<T>::takeLast()
{
	T el = _list.back();
	_list.pop_back();
	return el;
}

template<class T>
T UList<T>::takeAt( unsigned long index )
{
	T tmp= _list.at(index);
	_list.erase(begin()+index);
	return tmp;
}

template<class T>
void UList<T>::replace( unsigned long index, const T& el )
{
	if ( isValid(index) )
		_list.at(index) = el;
}

template<class T>
void UList<T>::clear()
{
	while ( !this->isEmpty() )
		_list.pop_back();
}

template<class T>
void UList<T>::deleteAll()
{
	while ( !this->isEmpty() )
		delete this->takeLast();
}

template <class T>
void UList<T>::popFirst() {
	if (isEmpty()) {
		//      throw out_of_range("Stack<>::pop(): empty stack");
		return;
	}

	// remove last element
	_list.pop_front();
}

template <class T>
void UList<T>::popLast() {
	if (isEmpty()) {
		//      throw out_of_range("Stack<>::pop(): empty stack");
		return;
	}

	// remove last element
	_list.pop_back();
}

template<class T>
T UList<T>::first() const
{
	return _list.front();
}

template<class T>
T UList<T>::last() const
{
	return _list.back();
}


template<class T>
std::ostream& operator<<( std::ostream& out, const UList<T>& list )
{
	out  << "{";
	for ( unsigned long it = 0; (it < 1) && !list.isEmpty(); ++it )
		out << list.at(it);
	for ( unsigned long it = 1; it < list.size(); ++it )
		out << " " << list.at(it);
	out << "}";
	return out;
}

template<class T>
bool UList<T>::contains( const T& el ) const
{
	for ( auto lel : _list )
		if ( lel == el )
			return true;
	return false;
}

/**********************************************/
/**********************************************/
/**********************************************/
#endif // ULIST_H
