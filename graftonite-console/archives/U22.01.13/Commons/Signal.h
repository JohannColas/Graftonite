#ifndef SIGNAL_H
#define SIGNAL_H
/**********************************************/
#include <functional>
#include <vector>
/**********************************************/
/**********************************************/
/**********************************************/
template<typename ... ARGS>
class Signal {
public:
	void operator()( ARGS... args ) const
	{
		for( const auto& slot : _slots )
			slot(args...);
	}
	void send( ARGS... args ) const
	{
		for( const auto& slot : _slots )
			slot(args...);
	}

	template<class T>
	int connect( T* t, void(T::* fn)(ARGS...) )
	{
		const auto lambda = [&t, fn](ARGS... args)
		{
			(*t.*fn)( std::forward<ARGS>( args )... );
		};
		return connect( lambda );
	}

	int connect( std::function<void(ARGS...)> slot )
	{
		_slots.emplace_back( std::move( slot ) );
		return _slots.size() - 1;
	}

private:
	std::vector<std::function<void(ARGS...)>> _slots;
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SIGNAL_H
