#ifndef USTRING_H
#define USTRING_H
/**********************************************/
#include <string>
#include <iostream>
using namespace std;
/**********************************************/
#include "UList.h"
/**********************************************/
class Key;
/**********************************************/
/**********************************************/
/**********************************************/
class ustring : public string
{
public:
	ustring();
	ustring( const double d );
	ustring( const char* c_str );
	ustring( const string& str );
	ustring( const ustring& str );
	ustring( const Key& str );
	/**********************************************/
	bool contains( const string& str ) const;
	bool contains( const char* c_str ) const;
	bool isEqual( const Key& str ) const;
	/**********************************************/
	/* Conversions */
	bool isInteger() const
	{
		for ( char c : *this )
			if ( c < 48 || 57 < c )
				return false;
		return true;
	}
	bool isDouble() const
	{
		for ( char c : *this )
			if ( (c < 48 || 57 < c) && c != '.' )
				return false;
		return this->contains(".");
	}
	double toDouble() const
	{
		double d = 0;
		if ( !empty() )
			d = stod(*this);
		return d;
	}
	deque<double> toDequeDouble() const
	{
		deque<double> ddbl;
		UList<ustring> lst = this->split(",");
		for ( ustring it : lst )
			ddbl.push_back( it.toDouble() );
		return ddbl;
	}
	int toInt() const
	{
		return 0;
	}
	unsigned int toUInt() const
	{
		return 0;
	}
	long toLong() const
	{
		return 0;
	}
	unsigned long toULong() const
	{
		return 0;
	}
	char toChar() const
	{
		return 0;
	}
	unsigned char toUChar() const
	{
		return 0;
	}
//	Keys::Key toKey() const
//	{
//		return 0;
//	}
	char toColor() const
	{
//		Color color;
		return 0;
	}
	/**********************************************/
	UList<ustring> split( const ustring& separator, bool keepEmptyParts = false ) const;
	/**********************************************/
	// Assignment Operators Overloading
	ustring& operator= ( const ustring& str )
	{
		(*this).clear();
		(*this) += str;
		return *this;
	}
	ustring& operator= ( const string& str )
	{
		(*this).clear();
		(*this) += str;
		return *this;
	}
	bool operator== ( const UList<ustring>& list ) const
	{
		for ( auto el : list )
			if ( *this == el || *this == ("-"+el) )
				return true;
		return false;
	}
	ustring& operator= ( const char* s )
	{
		(*this).clear();
		(*this) += s;
		return *this;
	}
	ustring& operator= (char c)
	{
		(*this).clear();
		(*this) += c;
		return *this;
	}
	friend ostream& operator<<( ostream& os, const ustring& str );
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // USTRING_H
