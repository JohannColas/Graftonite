#include "Key.h"
/**********************************************/
/**********************************************/
/**********************************************/
Key::Key()
{

}
/**********************************************/
/**********************************************/
Key::Key( const ustring& key )
{
	_key = key;
}
/**********************************************/
/**********************************************/
Key::Key( const ustring& key, const ustring& abrv )
{
	_key = key;
	_abrvs.append(abrv);
}
/**********************************************/
/**********************************************/
Key::Key( const ustring& key, const UList<ustring>& abrv )
{
	_key = key;
	_abrvs = abrv;
}
/**********************************************/
/**********************************************/
UList<ustring> Key::abrvs() const
{
	return _abrvs;
}
/**********************************************/
/**********************************************/
ustring Key::key() const
{
	return _key;
}
/**********************************************/
/**********************************************/
bool Key::isEmpty() const
{
	return _key.empty();
}
/**********************************************/
/**********************************************/
bool Key::operator==(const ustring& key) const
{
	bool res = key == this->key() || key == ("-"+this->key());
	for ( auto abrv : _abrvs )
		res = res || key == abrv || key == ("-"+abrv);
	return res;
}
/**********************************************/
/**********************************************/
