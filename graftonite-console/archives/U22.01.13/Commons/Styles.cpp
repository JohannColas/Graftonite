#include "Styles.h"
/**********************************************/
/**********************************************/
/**********************************************/
Color::Color()
{
}
Color::Color( double red, double green, double blue, double alpha )
{
	setRed( red );
	setGreen( green );
	setBlue( blue );
	setAlpha( alpha );
}
Color::Color( int red, int green, int blue, int alpha )
{
	setRed( red );
	setGreen( green );
	setBlue( blue );
	setAlpha( alpha );
}
Color::Color( const string& color )
{
	setColor( color );
}
double Color::red() const
{
	return _red;
}
void Color::setRed( double red )
{
	_red = red;
}
void Color::setRed( int red )
{
	_red = red/255.0;
}
double Color::green() const
{
	return _green;
}
void Color::setGreen( double green )
{
	_green = green;
}
void Color::setGreen( int green )
{
	_green = green/255.0;
}
double Color::blue() const
{
	return _red;
}
void Color::setBlue( double blue )
{
	_blue = blue;
}
void Color::setBlue( int blue )
{
	_blue = blue/255.0;
}
double Color::alpha() const
{
	return _alpha;
}
void Color::setAlpha( double alpha )
{
	_alpha = alpha;
}
void Color::setAlpha( int alpha )
{
	_alpha = alpha/255.0;
}
void Color::setColor( const ustring& color )
{
	ustring color_tmp = color;
	if ( !color_tmp.contains(",") )
		color_tmp = Color::getColorByName(color);

	UList<ustring> rgba = color_tmp.split(",");
	if ( rgba.size() < 3 )
		return;
	for ( unsigned it = 0; it < rgba.size(); ++it )
	{
		ustring tmp = rgba.at(it);
		if ( it == 0 )
		{
			if ( tmp.isInteger() )
				setRed( tmp.toInt() );
			else
				setRed( tmp.toDouble() );
		}
		else if ( it == 1 )
		{
			if ( tmp.isInteger() )
				setGreen( tmp.toInt() );
			else
				setGreen( tmp.toDouble() );
		}
		else if ( it == 2 )
		{
			if ( tmp.isInteger() )
				setBlue( tmp.toInt() );
			else
				setBlue( tmp.toDouble() );
		}
		else if ( it == 3 )
		{
			if ( tmp.isInteger() )
				setAlpha( tmp.toInt() );
			else
				setAlpha( tmp.toDouble() );
		}
		else
			break;
	}
}
string Color::getColorByName( const ustring& name )
{
	if ( name == "transparent" )
		return "0,0,0,0";
	else if ( name == "red" )
		return "255,0,0,255";
	else if ( name == "blue" )
		return "0,0,255,255";
	else if ( name == "green" )
		return "0,255,0,255";
	else
		return "0,0,0";
}
/**********************************************/
/**********************************************/
/**********************************************/
Point::Point( double x, double y )
{
	setX(x);
	setY(y);
}
double Point::x() const
{
	return _x;
}
void Point::setX( double x )
{
	_x = x;
}
double Point::y() const
{
	return _y;
}
void Point::setY( double y )
{
	_y = y;
}
/**********************************************/
/**********************************************/
/**********************************************/
Size::Size( double width, double height )
{
	setWidth(width);
	setHeight(height);
}
double Size::width() const
{
	return _width;
}
void Size::setWidth( double width )
{
	_width = width;
}
double Size::height() const
{
	return _height;
}
void Size::setHeight( double height )
{
	_height = height;
}
/**********************************************/
/**********************************************/
/**********************************************/
Rect::Rect()
{

}
Rect::Rect( double x, double y, double width, double height )
{
	setX(x);
	setY(y);
	setWidth(width);
	setHeight(height);
}
Rect::Rect( const Point& point, const Size& size )
{
	setX(point.x());
	setY(point.y());
	setWidth(size.width());
	setHeight(size.height());
}
double Rect::x() const
{
	return _x;
}
void Rect::setX( double x )
{
	_x = x;
}
double Rect::y() const
{
	return _y;
}
void Rect::setY( double y )
{
	_y = y;
}
Point Rect::point() const
{
	return Point( _x, _y );
}
void Rect::setPoint( double x, double y )
{
	setX(x);
	setY(y);
}
void Rect::setPoint( const Point& point )
{
	setX(point.x());
	setY(point.y());
}
double Rect::width() const
{
	return _width;
}
void Rect::setWidth( double width )
{
	_width = width;
}
double Rect::height() const
{
	return _height;
}
void Rect::setHeight( double height )
{
	_height = height;
}
Size Rect::size() const
{
	return Size( _width, _height );
}
void Rect::setSize( double width, double height )
{
	setWidth(width);
	setHeight(height);
}
void Rect::setSize( const Size& size )
{
	setWidth(size.width());
	setHeight(size.height());
}
/**********************************************/
/**********************************************/
/**********************************************/
FontMetrics::FontMetrics()
{

}
FontMetrics::FontMetrics( cairo_t* drawing )
{
	init( drawing );
}
FontMetrics::FontMetrics( const string& text, cairo_t* drawing )
{
	init( text, drawing );
}
void FontMetrics::init( cairo_t* drawing )
{
	cairo_font_extents_t fe;
	cairo_font_extents( drawing, &fe );
	setAscent( fe.ascent );
	setDescent( fe.descent );
	setHeight( fe.height );
}
void FontMetrics::init( const string& text, cairo_t* drawing )
{
	init( drawing );
	cairo_text_extents_t te;
	cairo_text_extents( drawing, text.c_str(), &te );
	setTightHeight( te.height );
	setWidth( te.x_advance );
	setTightWidth( te.width );
	setXBearing( te.x_bearing );
	setYBearing( te.y_bearing );
}
double FontMetrics::ascent() const
{
	return _ascent;
}
void FontMetrics::setAscent( double ascent )
{
	_ascent = ascent;
}
double FontMetrics::descent() const
{
	return _descent;
}
void FontMetrics::setDescent( double descent )
{
	_descent = descent;
}
double FontMetrics::height() const
{
	return _height;
}
void FontMetrics::setHeight( double height )
{
	_height = height;
}
double FontMetrics::tightHeight() const
{
	return _tight_height;
}
void FontMetrics::setTightHeight( double tightHeight )
{
	_tight_height = tightHeight;
}
double FontMetrics::width() const
{
	return _width;
}
void FontMetrics::setWidth( double width )
{
	_width = width;
}
double FontMetrics::tightWidth() const
{
	return _tight_width;
}
void FontMetrics::setTightWidth( double tightWidth )
{
	_tight_width = tightWidth;
}
double FontMetrics::xBearing() const
{
	return _x_bearing;
}
void FontMetrics::setXBearing( double xBearing )
{
	_x_bearing = xBearing;
}
double FontMetrics::yBearing() const
{
	return _y_bearing;
}
void FontMetrics::setYBearing( double yBearing )
{
	_y_bearing = yBearing;
}
/**********************************************/
/**********************************************/
/**********************************************/
TextStyle::TextStyle()
{

}
TextStyle::TextStyle( const string& style )
{
	set( style );
}
FontMetrics TextStyle::metrics( const string& text, cairo_t* drawing ) const
{
	return FontMetrics( text, drawing );
}
void TextStyle::set( const string& style )
{
	deque<string> options;
	string tmp = style;
	unsigned long it = tmp.find(';');
	while ( it != string::npos )
	{
		options.push_back( tmp.substr(0,it) );
		tmp = tmp.substr( it+1 );
		it = tmp.find(';', 0 );
	}
	if ( !tmp.empty() )
		options.push_back( tmp );
	for ( auto str : options )
	{
		unsigned long it = str.find(':', 0 );

		if ( it == string::npos )
		{
			if ( str == "bold" )
				_weight = CAIRO_FONT_WEIGHT_BOLD;
			if ( str == "notbold" )
				_weight = CAIRO_FONT_WEIGHT_NORMAL;
			if ( str == "italic" )
				_slant = CAIRO_FONT_SLANT_ITALIC;
			if ( str == "oblique" )
				_slant = CAIRO_FONT_SLANT_OBLIQUE;
			if ( str == "notitalic" )
				_slant = CAIRO_FONT_SLANT_NORMAL;
			if ( str == "underline" )
				_underline = true;
			if ( str == "nounderline" )
				_underline = false;
			if ( str == "superscript" )
				_scriptopt = ScriptOpt::SuperScript;
			if ( str == "subscript" )
				_scriptopt = ScriptOpt::SubScript;
			if ( str == "normalscript" )
				_scriptopt = ScriptOpt::NormalScript;
			if ( str == "nocap" )
				_cap = TextStyle::NoCap;
			if ( str == "cap" )
				_cap = TextStyle::Cap;
			if ( str == "smallcap" )
				_cap = TextStyle::SmallCap;
			if ( str == "normal" )
			{
				_weight = CAIRO_FONT_WEIGHT_NORMAL;
				_slant = CAIRO_FONT_SLANT_NORMAL;
				_cap = TextStyle::NoCap;
				_scriptopt = ScriptOpt::NormalScript;
				_underline = false;
			}
		}
		else
		{
			string key, value;
			key = str.substr( 0, it );
			value = str.substr( it+1 );
			if ( key == "family" )
				_family = value;
			if ( key == "color" )
				_color = value;
			if ( key == "size" )
				_size = stod(value);
		}
	}
}
string TextStyle::family() const
{
	return _family;
}
cairo_font_slant_t TextStyle::slant() const
{
	return _slant;
}
cairo_font_weight_t TextStyle::weight() const
{
	return _weight;
}
TextStyle::Capitalization TextStyle::cap() const
{
	return _cap;
}
double TextStyle::size() const
{
	return _size;
}
void TextStyle::setSize( double size )
{
	_size = size;
}
Color TextStyle::color() const
{
	return _color;
}
Anchor TextStyle::anchor() const
{
	return _anchor;
}
void TextStyle::setAnchor( const Anchor& anchor )
{
	_anchor = anchor;
}
TextStyle::Align TextStyle::align() const
{
	return _align;
}
TextStyle::ScriptOpt TextStyle::scriptOpt() const
{
	return _scriptopt;
}
bool TextStyle::underline() const
{
	return _underline;
}
/**********************************************/
/**********************************************/
/**********************************************/
StyledText::StyledText()
{

}
StyledText::StyledText( const string& text, const TextStyle& style )
{
	_text = text;
	_style = style;
}
string StyledText::text() const
{
	return _text;
}
TextStyle StyledText::style() const
{
	return _style;
}
/**********************************************/
/**********************************************/
/**********************************************/
LineStyle::LineStyle()
{

}
cairo_line_cap_t LineStyle::cap() const
{
	return _cap;
}
void LineStyle::setCap( const cairo_line_cap_t& cap )
{
	_cap = cap;
}
void LineStyle::setCap( const ustring& cap )
{
	if ( cap == "round" || cap == "r" )
		_cap = CAIRO_LINE_CAP_ROUND;
	else if ( cap == "butt" || cap == "b" )
		_cap = CAIRO_LINE_CAP_BUTT;
	else if ( cap == "square" || cap == "s" )
		_cap = CAIRO_LINE_CAP_SQUARE;
}
cairo_line_join_t LineStyle::join() const
{
	return _join;
}
void LineStyle::setJoin( const cairo_line_join_t& join )
{
	_join = join;
}
double LineStyle::miterLimit() const
{
	return _miter_limit;
}
void LineStyle::setMiterLimit( double miter_limit )
{
	_miter_limit = miter_limit;
}
double LineStyle::width() const
{
	return _width;
}
void LineStyle::setWidth( double width )
{
	_width = width;
}
Color LineStyle::color() const
{
	return _color;
}
void LineStyle::setColor( const Color& color )
{
	_color = color;
}
deque<double> LineStyle::dash() const
{
	return _dash;
}
void LineStyle::setDash( const deque<double>& dash )
{
	_dash = dash;
}
/**********************************************/
/**********************************************/
/**********************************************/
FillStyle::FillStyle()
{

}
FillStyle::Type FillStyle::type() const
{
	return _type;
}
void FillStyle::setType( const FillStyle::Type& type )
{
	_type = type;
}
cairo_pattern_t* FillStyle::linearPattern() const
{
	return _linear_gradient;
}
void FillStyle::setLinearPattern( cairo_pattern_t* linear_gradient )
{
	_linear_gradient = linear_gradient;
}
cairo_pattern_t* FillStyle::radialPattern() const
{
	return _radial_gradient;
}
void FillStyle::setRadialPattern( cairo_pattern_t* radial_gradient )
{
	_radial_gradient = radial_gradient;
}
Color FillStyle::color() const
{
	return _color;
}
void FillStyle::setColor( const Color& color )
{
	_color = color;
}
/**********************************************/
/**********************************************/
/**********************************************/
ShapeStyle::ShapeStyle()
{

}
FillStyle ShapeStyle::fill() const
{
	return _fill;
}
void ShapeStyle::setFill( const FillStyle& fill )
{
	_fill = fill;
}
LineStyle ShapeStyle::line() const
{
	return _line;
}
void ShapeStyle::setLine( const LineStyle& line )
{
	_line = line;
}
Anchor ShapeStyle::anchor() const
{
	return _anchor;
}
void ShapeStyle::setAnchor( const Anchor& anchor )
{
	_anchor = anchor;
}
/**********************************************/
/**********************************************/
/**********************************************/
