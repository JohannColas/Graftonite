#include "UString.h"
/**********************************************/
#include "Key.h"
/**********************************************/
/**********************************************/
ustring::ustring() : string()
{

}

ustring::ustring( const double d ) : string()
{
	ustring strd = to_string(d);
	char dec = 0;
	if ( strd.contains(".") ) dec = '.';
	else if ( strd.contains(",") ) dec = ',';
	while ( strd.back() == '0' )
		strd.pop_back();
	if ( strd.back() == dec )
		strd.pop_back();
	if ( strd == "-0" )
		strd = "0";
	(*this) = strd;
}
/**********************************************/
ustring::ustring( const char* c_str ) : string(c_str)
{

}
/**********************************************/
ustring::ustring( const string& str ) : string(str)
{

}
/**********************************************/
ustring::ustring( const ustring& str ) : string(str)
{

}
/**********************************************/
/**********************************************/
ustring::ustring( const Key& str ) : string(str.key())
{

}
/**********************************************/
/**********************************************/
bool ustring::contains( const string& str ) const
{
	return (this->find(str) != string::npos);
}
/**********************************************/
/**********************************************/
bool ustring::contains( const char* c_str ) const
{
	return (this->find(c_str) != string::npos);
}
/**********************************************/
/**********************************************/
bool ustring::isEqual( const Key& key ) const
{
	return (key == *this);
}
/**********************************************/
/**********************************************/
UList<ustring> ustring::split( const ustring& separator, bool keepEmptyParts ) const
{
	UList<ustring> list;
	unsigned long ind0 = 0,
			ind1,
			strsz = separator.size();;
	do
	{
		ind1 = this->find( separator, ind0 );
		string tmp = this->substr(ind0, ind1-ind0);

		if ( !tmp.empty() || (tmp.empty() && keepEmptyParts) )
			list.append(tmp);

		ind0 = ind1+strsz;
	}
	while ( ind1 != string::npos );

	return list;
}
/**********************************************/
/**********************************************/
ostream& operator<<( ostream& os, const ustring& str )
{
	for ( auto c : str )
		os << c;
	return os;
}
/**********************************************/
/**********************************************/
