#ifndef GRAPH_H
#define GRAPH_H
/**********************************************/
#include "Element.h"
#include "Layer.h"
#include "Frame.h"
#include "Axis.h"
#include "Plot.h"
#include "Legend.h"
#include "Title.h"
#include "Commons/Cairo.h"
/**********************************************/
/**********************************************/
/**********************************************/
class Graph : public Element
{
	string name = "";
	Cairo::Format _format = Cairo::PNG;
	double width = 1200;
	double height = 800;
	FillStyle sty_background;
	LineStyle sty_borders;
//	ustring _workingPath = "/home/colas/Bureau";
	/**********************************************/
	Element* _currentEl = 0;
	/**********************************************/
	UList<Layer*> _layers;
	UList<Frame*> _frames;
	UList<Axis*> _axes;
	UList<Plot*> _plots;
	UList<Legend*> _legends;
	UList<Title*> _titles;
	/**********************************************/
public:
	~Graph();
	Graph();
	/**********************************************/
	Layer* layer( const ustring& name );
	UList<Layer*> layers();
	Frame* frame( const ustring& name );
	UList<Frame*> frames();
	Axis* axis( const ustring& name );
	UList<Axis*> axes();
	Plot* plot( const ustring& name );
	UList<Plot*> plots();
	Legend* legend( const ustring& name );
	UList<Legend*> legends();
	Title* title( const ustring& name );
	UList<Title*> titles();
	Element* element( const Element::Type& type, const ustring& name );
	/**********************************************/
	void newElement( const Element::Type& type = Element::NOTYPE, const ustring& name = ustring() );
	void removeElement( const Element::Type& type = Element::NOTYPE, const ustring& name = ustring() );
	void setCurrent( UList<ustring>& args );
	void set( const ustring& key, const ustring& value ) override;
	void set( const UList<ustring>& args ) override;
	void unset( const UList<ustring>& args ) override;
	void showAll( const Element::Type& type = Element::NOTYPE, const ustring& name = ustring() );
	void showCurrent();
	void draw( Cairo& drawing ) override;
	void exportGraph();
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // GRAPH_H
