#include "Plot.h"
/**********************************************/
#include "Commons/Cairo.h"
/**********************************************/
#include "Graph.h"
/**********************************************/
/**********************************************/
Plot::Plot( Graph* parent ) : Element(parent)
{
	_type = Element::PLOT;
	this->set( Keys::Name, "plot1" );
	this->set( Keys::Axis1, "0" );
	this->set( Keys::Axis2, "1" );
}
/**********************************************/
/**********************************************/
void Plot::set( const ustring& key, const ustring& value )
{
	ustring key2;
	if ( key.isEqual(Keys::Name) )
		key2 = Keys::Name;
	else if ( key.isEqual(Keys::Type) )
		key2 = Keys::Type;
	else if ( key.isEqual(Keys::Axis1) )
		key2 = Keys::Axis1;
	else if ( key.isEqual(Keys::Axis2) )
		key2 = Keys::Axis2;
	else if ( key.isEqual(Keys::Axis3) )
		key2 = Keys::Axis3;

	if ( !key2.empty() )
		Element::set( key2, value );
}
/**********************************************/
/**********************************************/
void Plot::draw( Cairo& drawing )
{
	if ( !_parent ) return;
	Axis* axis1 = 0;
	Axis* axis2 = 0;
	UList<Axis*> _axes;
	axis1 = _parent->axis( get(Keys::Axis1) );
	axis2 = _parent->axis( get(Keys::Axis2) );
	if ( !axis1 || !axis2 ) return;

	vector<vector<double>> graphedData;
	deque<Point> graphData;
	vector<vector<double>> _data = {{0,0},{0.1,0.2},{0.3,0.5},{0.7,0.8},{0.9,0.9}};
	for ( vector<double> point : _data )
	{
		double x = axis1->GetGraphCoord(point.at(0));
		double y = axis2->GetGraphCoord(point.at(1));
		graphedData.push_back( {x,y} );
		graphData.push_back( {x, y} );
	}
	LineStyle style;
	style.setColor( {0.8,0.3,0.15} );
	drawing.drawPath( graphData, style );
}
/**********************************************/
/**********************************************/
