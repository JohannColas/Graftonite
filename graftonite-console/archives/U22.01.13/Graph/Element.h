#ifndef ELEMENT_H
#define ELEMENT_H
/**********************************************/
#include <map>
#include <deque>
#include <string>
using namespace std;
/**********************************************/
#include "Commons/Key.h"
#include "Commons/UList.h"
/**********************************************/
class Graph;
class Cairo;
/**********************************************/
namespace Keys
{
//	enum Keys
//	{
//		NoKey
//		Name,
//		Graph,
//		Layer,
//		Frame,
//		Axis,
//		Plot,
//		Legend,
//		Title,
//		x,
//		y,
//		Position,
//		Width,
//		Height,
//		Size,
//		Type,
//		Format
//	};
	static inline Key Name("name", "n");
	// Element types
	static inline Key Graph("graph", "g");
	static inline Key Layer("layer", "l");
	static inline Key Frame("frame", "f");
	static inline Key Axis("axis", "a");
	static inline Key Plot("plot", "p");
	static inline Key Legend("legend", "c");
	static inline Key Title("title", "t");
	static inline Key Shape("shape", "s");

	// Geometry Settings
	static inline Key X("x");
	static inline Key Y("y");
	static inline Key Pos("pos", "p");
	static inline Key Width("width", "w");
	static inline Key Height("height", "h");
	static inline Key Size("size", "s");
	static inline Key Type("type", "ty");
	static inline Key Format("format", "f");

	//  Settings
	static inline Key Background("background", "bg");
	static inline Key BackgroundColor("background.color", "bgc");
	static inline Key BackgroundLinearPattern("background.linearpattern", "bglp");
	static inline Key BackgroundRadialPattern("background.radialpattern", "bgrp");
	static inline Key Borders("Borders", "bd");
	static inline Key BordersDash("Borders.dash", "bdd");
	static inline Key BordersColor("Borders.color", "bdc");
	static inline Key BordersWidth("Borders.width", "bdw");
	static inline Key BordersJoin("Borders.join", "bdj");
	static inline Key BordersMiterLimit("Borders.miterlimit", "bdml");

	// Positions, Anchors & Alignment
	static inline Key Center("center", "c");
	static inline Key Bottom("bottom", "b");
	static inline Key Top("top", "t");
	static inline Key Right("right", "r");
	static inline Key Left("left", "l");
	static inline Key Outside("outside", "o");
	static inline Key Inside("inside", "i");
	static inline Key BottomLeft("bottomleft", "bl");
	static inline Key BottomRight("bottomright", "br");
	static inline Key TopLeft("topleft", "tl");
	static inline Key TopRight("topright", "tr");
	static inline Key Baseline("baseline", "bs");
	static inline Key BaselineLeft("baselineleft", "bsl");
	static inline Key BaselineRight("baselineright", "bsr");
	static inline Key Justify("justify", "j");

	// Specific Axis Settings
	static inline Key Hide("hide", "hd");
	static inline Key Min("min", "mn");
	static inline Key Max("max", "mx");
	static inline Key Scale("scale", "s");
	static inline Key ScaleOption("scale.option", "so");
	static inline Key LinkTo("linkto", "lk");

	// Line Settings
	static inline Key Line("line", "l");
	static inline Key LineType("line.type", "lt");
	static inline Key LineDash("line.dash", "ld");
	static inline Key LineColor("line.color", "lc");
	static inline Key LineWidth("line.width", "lw");
	static inline Key LineJoin("line.join", "lj");
	static inline Key LineMiterLimit("line.miterlimit", "lml");
	static inline Key LineCap("line.cap", "lcp");
	static inline Key LineGap("line.gap", "lg");

	// Ticks Settings
	static inline Key Ticks("ticks", "tk");
	static inline Key TicksPos("ticks.pos", "tkp");
	static inline Key TicksIncrem("ticks.increment", "tki");
	static inline Key TicksNumbers("ticks.numbers", "tkn");
	static inline Key TicksSize("ticks.size", "tks");
	static inline Key MinorTicks("minor.ticks", "mtk");
	static inline Key MinorTicksPos("minor.ticks.pos", "mtkp");
	static inline Key MinorTicksIncrem("minor.ticks.increment", "mtki");
	static inline Key MinorTicksNumbers("minor.ticks.numbers", "mtkn");
	static inline Key MinorTicksSize("minor.ticks.size", "mtks");

	// Labels Settings
	static inline Key Labels("labels", "lb");
	static inline Key LabelsPos("labels.pos", "lbp");
	static inline Key LabelsShift("labels.shift", "lbs");
	static inline Key LabelsAnchors("labels.anchors", "lba");
	static inline Key LabelsTransform("labels.transform", "lbt");
	static inline Key LabelsFormat("labels.format", "lbfm");
	static inline Key LabelsFont("labels.font", "lbft");

	// Title Settings
//	static inline Key Title("labels", "lb");
	static inline Key TitleText("title.text", "tltx");
	static inline Key TitlePos("title.pos", "tlp");
	static inline Key TitleShift("title.shift", "tls");
	static inline Key TitleAnchors("title.anchors", "tla");
	static inline Key TitleTransform("title.transform", "tlt");
	static inline Key TitleFont("title.font", "tlft");

	// Grids Settings
//	static inline ustring Grids = "grids";
	static inline Key Grids("grids", "gd");
	static inline Key GridsPos("grids.pos", "gdp");
	static inline Key GridsShape("grids.dash", "gdd");
	static inline Key GridsColor("grids.color", "gdc");
	static inline Key GridsWidth("grids.width", "gdw");
	static inline Key GridsCap("grids.cap", "gdcp");
	static inline Key MinorGrids("minor.grids", "mgd");
	static inline Key MinorGridsShape("minor.grids.dash", "mgdd");
	static inline Key MinorGridsColor("minor.grids.color", "mgdc");
	static inline Key MinorGridsWidth("minor.grids.width", "mdgdw");

	// Plot Settings
	static inline Key Axes("axes", "a");
	static inline Key Axis1("axis1", "a1");
	static inline Key Axis2("axis2", "a2");
	static inline Key Axis3("axis3", "a3");
	static inline Key Data("data", "d");
	static inline Key Data1("data1", "d1");
	static inline Key Data2("data2", "d2");
	static inline Key Data3("data3", "d3");
	static inline Key Coordinates("coordinates", "cor");
	static inline Key Symbols("symbols", "s");
	static inline Key SymbolsType("symbols.type", "st");
	static inline Key SymbolsShape("symbols.shape", "ssh");
	static inline Key SymbolsSize("symbols.size", "ss");
	static inline Key SymbolsOption("symbols.option", "so");
	static inline Key SymbolsFill("symbols.fill", "sf");
	static inline Key SymbolsFillColor("symbols.fill.color", "sfc");
	static inline Key SymbolsLine("symbols.line", "sl");
	static inline Key SymbolsLineColor("symbols.line.color", "slc");
	static inline Key SymbolsLineWidth("symbols.line.width", "slw");
	static inline Key SymbolsLineCap("symbols.line.cap", "slcp");
	static inline Key SymbolsLineJoin("symbols.line.join", "slj");
	static inline Key SymbolsLineMiterLimit("symbols.line.miterlimit", "slml");
	static inline Key ErrorBars("errorbars", "eb");
	static inline Key Bars("bars", "br");
	static inline Key Area("area", "ar");


	static inline ustring WorkingPath = "workingpath";
	static inline ustring General = "general";
	static inline ustring Option = "option";
	static inline ustring Increment = "increment";
	static inline ustring Numbers = "numbers";
	static inline ustring Minor = "minor";
	static inline ustring Position = "position";
//	static inline ustring Format = "format";
	static inline ustring Font = "font";
	static inline ustring Anchors = "anchors";
	static inline ustring Shift = "shift";
	static inline ustring Transform = "transform";
	static inline ustring Text = "text";
	static inline ustring Separator = ".";

//	static inline Key getKey( const ustring& key )
//	{
//		if ( key == "-n" || key == "-name" )
//			return Keys::Name;
//		else if ( key == "-graph" || key == "-g" )
//			return Keys::Graph;
//		else if ( key == "-layer" || key == "-lay" )
//			return Keys::Layer;
//		else if ( key == "-frame" || key == "-f" )
//			return Keys::Frame;
//		else if ( key == "-axis" || key == "-a" )
//			return Keys::Axis;
//		else if ( key == "-plot" || key == "-p" )
//			return Keys::Plot;
//		else if ( key == "-legend" || key == "-l" )
//			return Keys::Legend;
//		else if ( key == "-title" || key == "-t" )
//			return Keys::Title;
//		else if ( key == "-shape" || key == "-s" )
//			return Keys::Shape;
//		else if ( key == "-width" || key == "-w" )
//			return Keys::Width;
//		else if ( key == "-height" || key == "-h" )
//			return Keys::Height;
//		else if ( key == "-size" || key == "-size" )
//			return Keys::Size;
//		else if ( key == "-background" || key == "-bg" )
//			return Keys::Background;
//		else if ( key == "-background.color" || key == "-bgc" )
//			return Keys::BackgroundColor;
//		else if ( key == "-" || key == "-" )
//			return ;
//		return Keys::NoKey;
//	}
}
/**********************************************/
/**********************************************/
/**********************************************/
class Element
{
public:
	enum Type : char
	{
		NOTYPE,
		GRAPH,
		LAYER,
		FRAME,
		AXIS,
		PLOT,
		LEGEND,
		TITLE
	};
	/**********************************************/
	map<ustring,ustring> _settings;
	Element::Type _type = Element::NOTYPE;
	unsigned _index = -1;
	Graph* _parent = 0;
	/**********************************************/
public:
	virtual ~Element();
	Element( Graph* parent = 0 );
	/**********************************************/
	Element::Type type() const;
	ustring typeString() const;
	void setType( const Element::Type& type );
	unsigned index() const { return _index; }
	void setIndex( unsigned index) { _index = index; }
	static Element::Type getType( const ustring& type );
	/**********************************************/
	bool hasKey( const ustring& key ) const;
	ustring get( const ustring& key ) const;
	ustring get( const Key& key ) const;
//	ustring get( const Key& list ) const;
	void set( const Key& key, const ustring& value );
	virtual void set( const ustring& key, const ustring& value );
	virtual void set( const pair<ustring,ustring>& pair );
	void set( const map<ustring,ustring>& args );
	virtual void set( const UList<ustring>& args );
	virtual void reset( const UList<ustring>& /*args*/ ){};
	virtual void unset( const ustring& key );
	virtual void unset( const UList<ustring>& args );
	virtual void remove( const ustring& key );
	void clear();
	/**********************************************/
	void show();
	void shortShow();
	virtual void draw( Cairo& drawing );
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ELEMENT_H
