#include "Graph.h"
/**********************************************/
#include "Commons/Cairo.h"
/**********************************************/
#include <iostream>
#include <fstream>
#include "Commons/UConsole.h"
#include "Commons/XML.h"
using namespace std;
/**********************************************/
/**********************************************/
Graph::~Graph()
{
	_layers.deleteAll();
	_frames.deleteAll();
	_axes.deleteAll();
	_plots.deleteAll();
	_legends.deleteAll();
	_titles.deleteAll();
}
/**********************************************/
/**********************************************/
Graph::Graph() : Element()
{
	_type = Element::GRAPH;
	Element::set( "name", "Graph");
	Element::set( "width", "1600");
	Element::set( "height", "1000");
	Element::set( Keys::Format, "png");

	Frame* layer = new Frame(this);
	layer->set( "name", "frame1" );
	layer->set( "width", "1000" );
	layer->set( "height", "800" );
	_frames.append( layer );
	layer->setIndex(0);
//	Frame* layer1 = new Frame(this);
//	layer1->set( "name", "This is a frame" );
//	layer1->set( "width", "42" );
//	layer1->set( "height", "1440" );
//	_frames.append( layer1 );
//	layer1->setIndex(1);
	Axis* axis = new Axis(this);
	axis->set( "name", "AxisX" );
	axis->set( "frame", "frame1" );
	axis->set( "min", "0" );
	axis->set( "max", "1" );
	axis->set( "type", "X" );
	axis->setIndex(0);
	_axes.append( axis );
	Axis* axisy = new Axis(this);
	axisy->set( "name", "AxisY" );
	axisy->set( "frame", "frame1" );
	axisy->set( "min", "0" );
	axisy->set( "max", "1" );
	axisy->set( "type", "Y" );
	axisy->set( "pos", "center" );
	axisy->setIndex(1);
	_axes.append( axisy );
}
/**********************************************/
/**********************************************/
Layer* Graph::layer( const ustring& name )
{
	for ( unsigned it = 0; it < _layers.size(); ++it )
		if ( _layers.at(it)->get("name") == name || ustring(it) == name  )
			return _layers.at(it);
	return 0;
}
/**********************************************/
/**********************************************/
UList<Layer*> Graph::layers()
{
	return _layers;
}
/**********************************************/
/**********************************************/
Frame* Graph::frame( const ustring& name )
{
	for ( unsigned it = 0; it < _frames.size(); ++it )
		if ( _frames.at(it)->get("name") == name || ustring(it) == name  )
			return _frames.at(it);
	return 0;
}
/**********************************************/
/**********************************************/
UList<Frame*> Graph::frames()
{
	return _frames;
}
/**********************************************/
/**********************************************/
Axis* Graph::axis( const ustring& name )
{
	for ( unsigned it = 0; it < _axes.size(); ++it )
		if ( _axes.at(it)->get("name") == name || ustring(it) == name  )
			return _axes.at(it);
	return 0;
}
/**********************************************/
/**********************************************/
UList<Axis*> Graph::axes()
{
	return _axes;
}
/**********************************************/
/**********************************************/
Plot* Graph::plot( const ustring& name )
{
	for ( unsigned it = 0; it < _plots.size(); ++it )
		if ( _plots.at(it)->get("name") == name || ustring(it) == name  )
			return _plots.at(it);
	return 0;
}
/**********************************************/
/**********************************************/
UList<Plot*> Graph::plots()
{
	return _plots;
}
/**********************************************/
/**********************************************/
Legend* Graph::legend( const ustring& name )
{
	for ( unsigned it = 0; it < _legends.size(); ++it )
		if ( _legends.at(it)->get("name") == name || ustring(it) == name  )
			return _legends.at(it);
	return 0;
}
/**********************************************/
/**********************************************/
UList<Legend*> Graph::legends()
{
	return _legends;
}
/**********************************************/
/**********************************************/
Title* Graph::title( const ustring& name )
{
	for ( unsigned it = 0; it < _titles.size(); ++it )
		if ( _titles.at(it)->get(Keys::Name) == name || ustring(it) == name  )
			return _titles.at(it);
	return 0;
}
/**********************************************/
/**********************************************/
UList<Title*> Graph::titles()
{
	return _titles;
}
/**********************************************/
/**********************************************/
Element* Graph::element( const Element::Type& type, const ustring& name )
{
	Element* el = 0;
	if ( type == Element::GRAPH )
		el = this;
	else if ( type == Element::LAYER )
		el = this->layer(name);
	else if ( type == Element::FRAME )
		el = this->frame(name);
	else if ( type == Element::AXIS )
		el = this->axis(name);
	else if (type == Element::PLOT )
		el = this->plot(name);
	else if ( type == Element::LEGEND )
		el = this->legend(name);
	else if ( type == Element::TITLE )
		el = this->title(name);
	return el;
}
/**********************************************/
/**********************************************/
void Graph::newElement( const Element::Type& type, const ustring& name )
{
	if ( type == Element::GRAPH || type == Element::NOTYPE )
	{
		if ( name.empty() )
		{
		}
	}
	if ( type == Element::LAYER || type == Element::NOTYPE )
	{
		if ( name.empty() )
		{
		}
		Layer* layer = new Layer(this);
		layer->set( "name", name );
		layer->setIndex(_layers.size());
		_layers.append( layer );
	}
	if ( type == Element::FRAME || type == Element::NOTYPE )
	{
		if ( name.empty() )
		{
		}
		Frame* frame = new Frame(this);
		frame->set( "name", name );
		frame->setIndex(_layers.size());
		_frames.append( frame );
	}
	if ( type == Element::AXIS || type == Element::NOTYPE )
	{
		if ( name.empty() )
		{
		}
		Axis* axis = new Axis(this);
		axis->set( "name", name );
		axis->setIndex(_layers.size());
		_axes.append( axis );
	}
	if ( type == Element::PLOT || type == Element::NOTYPE )
	{
		if ( name.empty() )
		{
		}
		Plot* plot = new Plot(this);
		plot->set( "name", name );
		plot->setIndex(_layers.size());
		_plots.append( plot );
	}
	if ( type == Element::LEGEND || type == Element::NOTYPE )
	{
		if ( name.empty() )
		{
		}
		Legend* legend = new Legend();
		legend->set( "name", name );
		legend->setIndex(_layers.size());
		_legends.append( legend );
	}
	if ( type == Element::TITLE || type == Element::NOTYPE )
	{
		if ( name.empty() )
		{
		}
		Title* title = new Title();
		title->set( "name", name );
		title->setIndex(_layers.size());
		_titles.append( title );
	}
}
/**********************************************/
/**********************************************/
void Graph::removeElement( const Element::Type& type, const ustring& name )
{
	if ( type == Element::LAYER || type == Element::NOTYPE )
	{
		if ( name.empty() )
		{
			if ( UConsole::confirmationDialog("Are you sure to delete all layers ? (y/n) : ") )
				_layers.clear();
		}
		else
		{
			UList<Layer*> temp;
			for (unsigned long it = 0; it < layers().size(); ++it)
			{
				ustring num = to_string(it);
				if ( _layers.at(it)->get("name") != name && num != name )
				{
					temp.append( _layers.at(it) );
				}
				else
					cout << " Element removed : " << it << " - " << _layers.at(it)->get("name") << endl;
			}
			_layers.clear();
			_layers = temp;
		}
	}
	if ( type == Element::FRAME || type == Element::NOTYPE )
	{
		if ( name.empty() )
		{
			if ( UConsole::confirmationDialog("Are you sure to delete all frames ? (y/n) : ") )
				_frames.clear();
		}
		else
		{
			UList<Frame*> temp;
			for (unsigned long it = 0; it < _frames.size(); ++it)
			{
				ustring num = to_string(it);
				if ( _frames.at(it)->get("name") != name && num != name )
				{
					temp.append( _frames.at(it) );
				}
				else
					cout << " Element removed : " << it << " - " << _frames.at(it)->get("name") << endl;
			}
			_frames.clear();
			_frames = temp;
		}
	}
	if ( type == Element::AXIS || type == Element::NOTYPE )
	{
		if ( name.empty() )
		{
			if ( UConsole::confirmationDialog("Are you sure to delete all axes ? (y/n) : ") )
				_axes.clear();
		}
		else
		{
			UList<Axis*> temp;
			for (unsigned long it = 0; it < _axes.size(); ++it)
			{
				ustring num = to_string(it);
				if ( _axes.at(it)->get("name") != name && num != name )
				{
					temp.append( _axes.at(it) );
				}
				else
					cout << " Element removed : " << it << " - " << _axes.at(it)->get("name") << endl;
			}
			_axes.clear();
			_axes = temp;
		}
	}
	if ( type == Element::PLOT || type == Element::NOTYPE )
	{
		if ( name.empty() )
		{
			if ( UConsole::confirmationDialog("Are you sure to delete all plots ? (y/n) : ") )
				_plots.clear();
		}
		else
		{
			UList<Plot*> temp;
			for (unsigned long it = 0; it < _plots.size(); ++it)
			{
				ustring num = to_string(it);
				if ( _plots.at(it)->get("name") != name && num != name )
				{
					temp.append( _plots.at(it) );
				}
				else
					cout << " Element removed : " << it << " - " << _plots.at(it)->get("name") << endl;
			}
			_plots.clear();
			_plots = temp;
		}
	}
	if ( type == Element::LEGEND || type == Element::NOTYPE )
	{
		if ( name.empty() )
		{
			if ( UConsole::confirmationDialog("Are you sure to delete all legends ? (y/n) : ") )
				_legends.clear();
		}
		else
		{
			UList<Legend*> temp;
			for (unsigned long it = 0; it < _legends.size(); ++it)
			{
				ustring num = to_string(it);
				if ( _legends.at(it)->get("name") != name && num != name )
				{
					temp.append( _legends.at(it) );
				}
				else
					cout << " Element removed : " << it << " - " << _legends.at(it)->get("name") << endl;
			}
			_legends.clear();
			_legends = temp;
		}
	}
	if ( type == Element::TITLE || type == Element::NOTYPE )
	{
		if ( name.empty() )
		{
			if ( UConsole::confirmationDialog("Are you sure to delete all titles ? (y/n) : ") )
				_titles.clear();
		}
		else
		{
			UList<Title*> temp;
			for (unsigned long it = 0; it < _titles.size(); ++it)
			{
				ustring num = to_string(it);
				if ( _titles.at(it)->get("name") != name && num != name )
				{
					temp.append( _titles.at(it) );
				}
				else
					cout << " Element removed : " << it << " - " << _titles.at(it)->get("name") << endl;
			}
			_titles.clear();
			_titles = temp;
		}
	}
}
/**********************************************/
/**********************************************/
void Graph::setCurrent( UList<ustring>& args )
{
	if ( args.isEmpty() )
		return;
	Element::Type eltype = Element::getType(args.at(0));
	if ( eltype == Element::GRAPH )
	{
		_currentEl = this;
	}
	else
	{
		if ( args.size() < 2 )
			return;
		Element* el = this->element( eltype, args.at(1) );
		if ( !el )
			return;
		_currentEl = el;
	}
	cout << "Current element : " << _currentEl->get(Keys::Name);
}
/**********************************************/
/**********************************************/
void Graph::set( const ustring& key, const ustring& value )
{
	Key key2;
	if ( key.isEqual(Keys::Name) )
	{
		key2 = Keys::Name;
		name = value;
	}
	// Geometry Settings
	else if ( key.isEqual(Keys::Width) )
	{
		key2 = Keys::Width;
		width = stod(value);
	}
	else if ( key.isEqual(Keys::Height) )
	{
		key2 = Keys::Height;
		height = stod(value);
	}
	else if ( key.isEqual(Keys::Size) )
	{
		if ( !value.contains("x") ) return;
		UList<ustring> list = value.split("x");
		if ( list.size() < 2 )
			return;
//		set( Keys::Width, list.at(0) );
//		set( Keys::Height, list.at(1) );
		width = stod(list.at(0));
		height = stod(list.at(1));
	}
	// Style Settings
	else if ( key == "-bk" || key == "-background" )
	{
		key2 = Keys::Background;
	}
	else if ( key == "-bd" || key == "-borders" )
	{
		key2 = Keys::Borders;
	}
	else if ( key.isEqual(Keys::Format) )
	{
		key2 = Keys::Format;
	}
	else if ( key.isEqual(Keys::Line) )
	{
		key2 = Keys::Line;
	}
	else if ( key.isEqual(Keys::LineDash) )
	{
		key2 = Keys::LineDash;

	}
	else if ( key.isEqual(Keys::LineColor) )
	{
		key2 = Keys::LineColor;
		sty_borders.setColor( value );
	}
	else if ( key.isEqual(Keys::LineWidth) )
	{
		key2 = Keys::LineWidth;
		sty_borders.setWidth( value.toDouble() );
	}
	else if ( key.isEqual(Keys::LineCap) )
	{
		key2 = Keys::LineCap;
	}
	else if ( key.isEqual(Keys::LineJoin) )
	{
		key2 = Keys::LineJoin;
	}
	else if ( key.isEqual(Keys::LineMiterLimit) )
	{
		key2 = Keys::LineMiterLimit;
	}

	if ( !key2.isEmpty() )
		Element::set( key2, value );
}
/**********************************************/
/**********************************************/
void Graph::set( const UList<ustring>& args )
{
	UList<ustring> args2 = args;
	ustring eltype;
	if ( args2.isEmpty() ) return;
	eltype = args2.first();
	Element::Type type = Element::getType(eltype);

	Element* el = 0;
	if ( type == Element::NOTYPE )
	{
		el = _currentEl;
	}
	else if ( type == Element::GRAPH )
	{
		Element::set(args2);
	}
	else
	{
		ustring name;
		if ( args2.isEmpty() ) return;
		name = args2.at(1);
		args2.takeFirst();args2.takeFirst();
		el = element( Element::getType(eltype), name );
	}

	if ( el )
	{
		if ( el == this )
			Element::set( args2 );
		else
			el->set(args2);
	}
}
/**********************************************/
/**********************************************/
void Graph::unset( const UList<ustring>& args )
{
	UList<ustring> args2 = args;
	ustring eltype;
	if ( args2.isEmpty() ) return;
	eltype = args2.takeFirst();
	Element::Type type = Element::getType(eltype);

	Element* el = 0;
	if ( type == Element::NOTYPE )
	{
		el = _currentEl;
	}
	else if ( type == Element::GRAPH )
	{
		Element::unset(args2);
	}
	else
	{
		ustring name;
		if ( args2.isEmpty() ) return;
		name = args2.takeFirst();
		el = element( Element::getType(eltype), name );
	}

	if ( el )
		el->unset(args2);
}
/**********************************************/
/**********************************************/
void Graph::showAll( const Element::Type& type, const ustring& name )
{
	if ( type == Element::GRAPH || type == Element::NOTYPE )
	{
		if ( name.empty() )
		{
			cout << "    " << "GRAPH" << endl;
		}
		else
		{
			this->show();
		}
	}
	if ( type == Element::LAYER || type == Element::NOTYPE )
	{
		if ( name.empty() )
		{
			cout << "    LAYERS   --------" << endl;
			if ( layers().isEmpty() )
				cout << "      " << "NO LAYERS" << endl;
			for ( Layer* layer : layers() )
				layer->shortShow();
		}
		else
		{
			Layer* layer = this->layer(name);
			if ( layer )
				layer->show();
		}
	}
	if ( type == Element::FRAME || type == Element::NOTYPE )
	{
		if ( name.empty() )
		{
			cout << "    FRAMES   --------" << endl;
			if ( _frames.isEmpty() )
				cout << "      " << "NO FRAMES" << endl;
			for ( Frame* frame : frames() )
				frame->shortShow();
		}
		else
		{
			Frame* frame = this->frame(name);
			if ( frame )
				frame->show();
		}
	}
	if ( type == Element::AXIS || type == Element::NOTYPE )
	{
		if ( name.empty() )
		{
			cout << "    AXES     --------" << endl;
			if ( axes().isEmpty() )
				cout << "      " << "NO AXES" << endl;
			for ( Axis* axis : axes() )
				axis->shortShow();
		}
		else
		{
			Axis* axis = this->axis(name);
			if ( axis )
				axis->show();
		}
	}
	if ( type == Element::PLOT || type == Element::NOTYPE )
	{
		if ( name.empty() )
		{
			cout << "    PLOTS    --------" << endl;
			if ( plots().isEmpty() )
				cout << "      " << "NO PLOTS" << endl;
			for ( Plot* plot : plots() )
				plot->shortShow();
		}
		else
		{
			Plot* plot = this->plot(name);
			if ( plot )
				plot->show();
		}
	}
	if ( type == Element::LEGEND || type == Element::NOTYPE )
	{
		if ( name.empty() )
		{
			cout << "    LEGENDS  --------" << endl;
			if ( legends().isEmpty() )
				cout << "      " << "NO LEGENDS" << endl;
			for ( Legend* legend : legends() )
				legend->shortShow();
		}
		else
		{
			Legend* legend = this->legend(name);
			if ( legend )
				legend->show();
		}
	}
	if ( type == Element::TITLE || type == Element::NOTYPE )
	{
		if ( name.empty() )
		{
			cout << "    TITLES   --------" << endl;
			if ( titles().isEmpty() )
				cout << "      " << "NO TITLE" << endl;
			for ( Title* title : titles() )
				title->shortShow();
		}
		else
		{
			Title* title = this->title(name);
			if ( title )
				title->show();
		}
	}
}
/**********************************************/
/**********************************************/
void Graph::showCurrent()
{
	showAll( _currentEl->type(), _currentEl->get(Keys::Name) );
}
/**********************************************/
/**********************************************/
void Graph::draw( Cairo& drawing )
{
	FillStyle fill_style;
	LineStyle line_style;
	ustring strokewidth = "6";
	ustring opacity;
	ustring stroke = "0,0.3,0.8";
	ustring strokelinejoin;
	ustring strokemiterlimit;
	ustring strokelinecap;
	ustring strokedasharray;
	ustring strokedashoffset;
	ustring strokeopacity;
	ustring fill = "0.8,0.0,0.0";
	double line_width = stod( strokewidth );
	line_style.setWidth( line_width );
	line_style.setColor( stroke );
	fill_style.setColor( fill );
	double width = stod( get(Keys::Width) );
	double height = stod( get(Keys::Height) );
	ShapeStyle style;
	style.setFill( fill_style );
	style.setLine( line_style );
	drawing.drawRect( {0.5*line_width,0.5*line_width}, Size({width-line_width, height-line_width}), style );

	for ( Frame* frame : _frames )
		frame->draw( drawing );
	for ( Axis* axis : _axes )
		axis->draw( drawing );
	for ( Plot* plot : _plots )
		plot->draw( drawing );
	for ( Legend* legend : _legends )
		legend->draw( drawing );
	for ( Title* title : _titles )
		title->draw( drawing );

	drawing.close();
}
/**********************************************/
/**********************************************/
void Graph::exportGraph()
{
	Cairo cairo = Cairo( get(Keys::Format),
						 ustring(width),
						 ustring(height) );

	ustring workingPath = get(Keys::WorkingPath);
	if ( workingPath.empty() )
		workingPath = "/home/colas/Bureau";
	cairo.setPath( workingPath );
	cairo.setFileName( "A graph" );
	cairo.init();

	this->draw( cairo );
}
/**********************************************/
/**********************************************/
