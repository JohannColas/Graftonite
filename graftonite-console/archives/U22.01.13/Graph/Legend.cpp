#include "Legend.h"
/**********************************************/
#include "Commons/Cairo.h"
/**********************************************/
/**********************************************/
Legend::Legend( Graph* parent ) : Element(parent)
{
	_type = Element::LEGEND;
}
/**********************************************/
/**********************************************/
void Legend::set( const ustring& key, const ustring& value )
{
	ustring key2;
	if ( key.isEqual(Keys::Name) )
		key2 = Keys::Name;

	if ( !key2.empty() )
		Element::set( key2, value );
}
/**********************************************/
/**********************************************/
void Legend::draw( Cairo& /*drawing*/ )
{

}
/**********************************************/
/**********************************************/
