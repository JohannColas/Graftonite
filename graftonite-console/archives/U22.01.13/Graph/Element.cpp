#include "Element.h"
/**********************************************/
#include <iostream>
/**********************************************/
/**********************************************/
Element::~Element()
{

}
/**********************************************/
Element::Element( Graph* parent )
{
	_parent = parent;
}

Element::Type Element::type() const
{
	return _type;
}

ustring Element::typeString() const
{
	if ( type() == Element::GRAPH )
		return "GRAPH";
	else if ( type() == Element::LAYER )
		return "LAYER";
	else if ( type() == Element::FRAME )
		return "FRAME";
	else if ( type() == Element::AXIS )
		return "AXIS";
	else if ( type() == Element::PLOT )
		return "PLOT";
	else if ( type() == Element::LEGEND )
		return "LEGEND";
	else if ( type() == Element::TITLE )
		return "TITLE";
	return "";
}

void Element::setType( const Element::Type& type )
{
	_type = type;
}

Element::Type Element::getType( const ustring& type )
{
	if ( type.isEqual(Keys::Graph) )
		return Element::GRAPH;
	else if ( type.isEqual(Keys::Layer) )
		return Element::LAYER;
	else if ( type.isEqual(Keys::Frame) )
		return Element::FRAME;
	else if ( type.isEqual(Keys::Axis) )
		return Element::AXIS;
	else if ( type.isEqual(Keys::Plot) )
		return Element::PLOT;
	else if ( type.isEqual(Keys::Legend) )
		return Element::LEGEND;
	else if ( type.isEqual(Keys::Title) )
		return Element::TITLE;
	return Element::NOTYPE;
}
/**********************************************/
/**********************************************/
bool Element::hasKey( const ustring& key ) const
{
	if ( _settings.count( key ) > 0 )
		return true;
	return false;
}
/**********************************************/
/**********************************************/
ustring Element::get( const ustring& key ) const
{
	if ( hasKey(key) )
		return _settings.at( key );
	return "";
}
/**********************************************/
/**********************************************/
ustring Element::get( const Key& key ) const
{
	return get( key.key() );
}
/**********************************************/
/**********************************************/
void Element::set( const ustring& key, const ustring& value )
{
	if ( hasKey(key) )
		_settings.at(key) = value;
	else
		_settings.emplace( key, value );
}
/**********************************************/
/**********************************************/
void Element::set( const Key& key, const ustring& value )
{
	if ( hasKey(key.key()) )
		_settings.at(key.key()) = value;
	else
		_settings.emplace( key.key(), value );
}
/**********************************************/
/**********************************************/
void Element::set( const pair<ustring,ustring>& pair )
{
	set( pair.first, pair.second );
}
/**********************************************/
/**********************************************/
void Element::set( const map<ustring,ustring>& args )
{
	for ( auto const& pair: args )
		set( pair );
}
/**********************************************/
/**********************************************/
void Element::set( const UList<ustring>& args )
{
	for ( unsigned it = 0; it < args.size(); it+=2 )
		if ( it+1 < args.size() )
			set( args.at(it), args.at(it+1) );
}
/**********************************************/
/**********************************************/
void Element::unset( const ustring& key )
{
	if ( !key.isEqual(Keys::Name) )
		_settings.erase( key );
}
/**********************************************/
/**********************************************/
void Element::unset( const UList<ustring>& args )
{
	for ( unsigned it = 0; it < args.size(); ++it )
		unset( args.at(it) );
}
/**********************************************/
/**********************************************/
void Element::remove( const ustring& key )
{
	if ( hasKey(key) )
		_settings.erase(key);
}
/**********************************************/
/**********************************************/
void Element::clear()
{
	ustring name;
	if ( hasKey("name") )
		name = get("name");
	_settings.clear();
	set( "name", name );
}
/**********************************************/
/**********************************************/
void Element::show()
{
	ustring str_typ = typeString();
	ustring name = "<NoName>";
	if ( hasKey("name") )
		name = get("name");
	cout << "    " << str_typ << ": " << name << endl;
	cout << "                KEYS  |  VALUES" << endl;
	cout << "    -------------------------------------" << endl;
	unsigned long len = 16;
	for ( auto const &pair: _settings )
	{
		ustring key;
		unsigned long size = pair.first.length();
		if ( size > len )
			size = len;
		for ( unsigned long it = 0; it < size; ++it )
			key += pair.first.at(it);
		for ( unsigned long it = key.length(); it < len; ++it )
			/*key = " " + */key.insert(key.begin(), 1, ' ');
		cout << "    " << key << "  |  " << pair.second << endl;
	}
	cout << "    -------------------------------------" << endl;
	cout << endl;
}
/**********************************************/
/**********************************************/
void Element::shortShow()
{
	ustring str_typ = typeString();
	for ( unsigned long it = str_typ.size(); it < 6; ++it )
		str_typ += " ";
	ustring num = ustring(index());
	for (unsigned long jt = num.size(); jt < 5; ++jt)
		num = " " + num;
	cout << "      "
		 << str_typ << "  | "
		 << num << "  |  "
		 << get("name") << endl;
}
/**********************************************/
/**********************************************/
void Element::draw( Cairo& /*drawing*/ )
{

}
/**********************************************/
/**********************************************/
