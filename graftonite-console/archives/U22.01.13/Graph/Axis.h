#ifndef AXIS_H
#define AXIS_H
/**********************************************/
#include "Element.h"
#include <vector>
/**********************************************/
/**********************************************/
/**********************************************/
class Axis : public Element
{
	std::vector<double> _boundingRect = {100, 100, 1200, 800};
	ustring name = "axis";
	double _min = 0;
	double _max = 1;
	double _scaleA = 0.1;
	double _scaleB = 0;
	double _tickinterval = 0.1;
	double _minortickinterval = 0.05;
public:
	Axis( Graph* parent = 0 );
	/**********************************************/
	vector<double> graphRect() const { return _boundingRect; }
	void set( const ustring& key, const ustring& value ) override;
	void draw( Cairo& drawing ) override;
	/**********************************************/
	void CalculateTicksPosition( UList<double>& tickspos, UList<double>& minortickspos, UList<ustring>& labels, bool isYaxis = false );
	double round( double value );
	void CalculateScaleCoef();
	double GetGraphCoord( double coord );
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // AXIS_H
