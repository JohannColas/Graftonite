#include "Title.h"
/**********************************************/
/**********************************************/
/**********************************************/
Title::Title( Graph* parent ) : Element(parent)
{
	_type = Element::TITLE;
}
/**********************************************/
/**********************************************/
void Title::set( const ustring& key, const ustring& value )
{
	ustring key2;
	if ( key.isEqual(Keys::Name) )
		key2 = Keys::Name;

	if ( !key2.empty() )
		Element::set( key2, value );
}
/**********************************************/
/**********************************************/
void Title::draw( Cairo& /*drawing*/ )
{

}
/**********************************************/
/**********************************************/
