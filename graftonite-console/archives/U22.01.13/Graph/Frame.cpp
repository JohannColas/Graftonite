#include "Frame.h"
/**********************************************/
#include "Commons/Cairo.h"
/**********************************************/
#include "Commons/XML.h"
/**********************************************/
/**********************************************/
Frame::Frame( Graph* parent ) : Element(parent)
{
	_type = Element::FRAME;
	// Default Settings
	this->set( "x", "150" );
	this->set( "y", "100" );
	this->set( "width", "1200" );
	this->set( "height", "800" );
}
/**********************************************/
/**********************************************/
void Frame::set( const ustring& key, const ustring& value )
{
	Key key2;
	if ( key.isEqual(Keys::Name) )
		key2 = Keys::Name;
	// Geometry Settings
	else if ( key.isEqual(Keys::X) )
		key2 = Keys::X;
	else if ( key.isEqual(Keys::Y) )
		key2 = Keys::Y;
	else if ( key.isEqual(Keys::Width) )
		key2 = Keys::Width;
	else if ( key.isEqual(Keys::Height) )
		key2 = Keys::Height;
	else if ( key.isEqual(Keys::Pos) )
	{
		if ( !value.contains("x") ) return;
		UList<ustring> list = value.split("x");
		set( Keys::X, list.at(0) );
		set( Keys::Y, list.at(1) );
		return;
	}
	else if ( key.isEqual(Keys::Size) )
	{
		if ( !value.contains("x") ) return;
		UList<ustring> list = value.split("x");
		set( Keys::Width, list.at(0) );
		set( Keys::Height, list.at(1) );
	}
	// Style Settings
	else if ( key == "-bk" || key == "-background" )
		key2 = Keys::Background;
	else if ( key == "-bd" || key == "-borders" )
		key2 = Keys::Borders;

	if ( !key2.isEmpty() )
		Element::set( key2, value );
}
/**********************************************/
/**********************************************/
void Frame::draw( Cairo& drawing )
{
	ShapeStyle style;
	FillStyle fill_style;
	LineStyle line_style;
	line_style.setColor( {1.0,1.0,1.0} );
	line_style.setWidth( 3.0 );
	fill_style.setColor( {0.83,0.23,0.18, 0.76} );
	style.setFill( fill_style );
	style.setLine( line_style );
	double x = stod(get(Keys::X));
	double y = stod(get(Keys::Y));
	double w = stod(get(Keys::Width));
	double h = stod(get(Keys::Height));
	drawing.drawRect( {x,y}, Size({w,h}), style );
}
/**********************************************/
/**********************************************/
