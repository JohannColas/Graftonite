#include "Axis.h"
/**********************************************/
#include <cmath>
/**********************************************/
#include "Commons/Cairo.h"
/**********************************************/
#include "Commons/XML.h"
#include "Graph.h"
#include "Layer.h"
/**********************************************/
/**********************************************/
Axis::Axis( Graph* parent ) : Element(parent)
{
	_type = Element::AXIS;
	// Default settings
	this->set( Keys::Min, "0" );
	this->set( Keys::Max, "1" );
	this->set( Keys::Type, "X" );
	this->set( Keys::Scale, "linear" );
	this->set( Keys::Pos, "bottom" );
	this->set( Keys::TicksIncrem, "0.1" );
	this->set( Keys::TicksSize, "8" );
	this->set( Keys::MinorTicksIncrem, "0.05" );
	this->set( Keys::MinorTicksSize, "4" );
	this->set( Keys::LabelsPos, "outside" );
	this->set( Keys::TitleText, "Axis X" );
	this->set( Keys::TitlePos, "outside" );
}
/**********************************************/
/**********************************************/
void Axis::set( const ustring& key, const ustring& value )
{
	ustring key2;
	if ( key.isEqual(Keys::Name) )
//	{
		key2 = Keys::Name;
//		name = value;
//	}
	else if ( key.isEqual(Keys::Layer) )
		key2 = Keys::Layer;
	else if ( key.isEqual(Keys::Frame) )
		key2 = Keys::Frame;
	else if ( key.isEqual(Keys::Hide) )
		key2 = Keys::Hide;
	// Axis Settings
	else if ( key.isEqual(Keys::Type) )
		key2 = Keys::Type;
	else if ( key.isEqual(Keys::Min) )
		key2 = Keys::Min;
	else if ( key.isEqual(Keys::Max) )
		key2 = Keys::Max;
	else if ( key.isEqual(Keys::Scale) )
		key2 = Keys::Scale;
	else if ( key.isEqual(Keys::ScaleOption) )
		key2 = Keys::ScaleOption;
	else if ( key.isEqual(Keys::LinkTo) )
		key2 = Keys::LinkTo;
	// Line Settings
	else if ( key.isEqual(Keys::Line) )
	{
		key2 = Keys::Line;
	}
	else if ( key.isEqual(Keys::LineDash) )
		key2 = Keys::LineDash;
	else if ( key.isEqual(Keys::LineColor) )
		key2 = Keys::LineColor;
	else if ( key.isEqual(Keys::LineWidth) )
		key2 = Keys::LineWidth;
	else if ( key.isEqual(Keys::LineCap) )
		key2 = Keys::LineCap;
	// Ticks Settings
	else if ( key.isEqual(Keys::Ticks) )
	{
		key2 = Keys::Ticks;
	}
	else if ( key.isEqual(Keys::TicksPos) )
		key2 = Keys::TicksPos;
	else if ( key.isEqual(Keys::TicksIncrem) )
		key2 = Keys::TicksIncrem;
	else if ( key.isEqual(Keys::TicksNumbers) )
		key2 = Keys::TicksNumbers;
	else if ( key.isEqual(Keys::TicksSize) )
		key2 = Keys::TicksSize;
	else if ( key.isEqual(Keys::MinorTicks) )
	{
		key2 = Keys::MinorTicks;
	}
	else if ( key.isEqual(Keys::MinorTicksPos) )
		key2 = Keys::MinorTicksPos;
	else if ( key.isEqual(Keys::MinorTicksIncrem) )
		key2 = Keys::MinorTicksIncrem;
	else if ( key.isEqual(Keys::MinorTicksNumbers) )
		key2 = Keys::MinorTicksNumbers;
	else if ( key.isEqual(Keys::MinorTicksSize) )
		key2 = Keys::MinorTicksSize;
	// Labels Settings
	else if ( key.isEqual(Keys::LabelsPos) )
		key2 = Keys::LabelsPos;
	else if ( key.isEqual(Keys::LabelsShift) )
		key2 = Keys::LabelsShift;
	else if ( key.isEqual(Keys::LabelsAnchors) )
		key2 = Keys::LabelsAnchors;
	else if ( key.isEqual(Keys::LabelsTransform) )
		key2 = Keys::LabelsTransform;
	else if ( key.isEqual(Keys::LabelsFormat) )
		key2 = Keys::LabelsFormat;
	else if ( key.isEqual(Keys::LabelsFont) )
		key2 = Keys::LabelsFont;
	// Title Settings
	else if ( key.isEqual(Keys::TitleText) )
		key2 = Keys::TitleText;
	else if ( key.isEqual(Keys::TitlePos) )
		key2 = Keys::TitlePos;
	else if ( key.isEqual(Keys::TitleShift) )
		key2 = Keys::TitleShift;
	else if ( key.isEqual(Keys::TitleAnchors) )
		key2 = Keys::TitleAnchors;
	else if ( key.isEqual(Keys::TitleTransform) )
		key2 = Keys::TitleTransform;
	else if ( key.isEqual(Keys::TitleFont) )
		key2 = Keys::TitleFont;
	// Grids Settings
	else if ( key.isEqual(Keys::Grids) )
	{
		key2 = Keys::Grids;
	}
	else if ( key.isEqual(Keys::GridsPos) )
		key2 = Keys::GridsPos;
	else if ( key.isEqual(Keys::GridsShape) )
		key2 = Keys::GridsShape;
	else if ( key.isEqual(Keys::GridsColor) )
		key2 = Keys::GridsColor;
	else if ( key.isEqual(Keys::GridsWidth) )
		key2 = Keys::GridsWidth;
	else if ( key.isEqual(Keys::GridsCap) )
		key2 = Keys::GridsCap;
	else if ( key.isEqual(Keys::MinorGrids) )
	{
		key2 = Keys::MinorGrids;
	}
	else if ( key.isEqual(Keys::MinorGridsShape) )
		key2 = Keys::MinorGridsShape;
	else if ( key.isEqual(Keys::MinorGridsColor) )
		key2 = Keys::MinorGridsColor;
	else if ( key.isEqual(Keys::MinorGridsWidth) )
		key2 = Keys::MinorGridsWidth;

	if ( !key2.empty() )
		Element::set( key2, value );
}
/**********************************************/
/**********************************************/
void Axis::draw( Cairo& drawing )
{
	Frame* frame = _parent->frame( get(Keys::Frame) );
	if ( frame )
	{
		_boundingRect = {frame->get(Keys::X).toDouble(),
						 frame->get(Keys::Y).toDouble(),
						 frame->get(Keys::Width).toDouble(),
						 frame->get(Keys::Height).toDouble()};
	}
	CalculateScaleCoef();
	// Open Axis Group
//	contents += "\t"+XML::beginGroup( "g", {{"id", "axis-"+get(Keys::Name)}} )+"\n";
	//
	double line_pos = 0;
	string line_dir = "";
	UList<double> line_limits; line_limits.append({0,0});
	//
	double ticks_pos = 0;
	string ticks_dir = "";
	UList<double> ticks_limits;
	double minorticks_pos = 0;
	UList<double> minorticks_limits;
	//
	UList<ustring> labels;
	double labels_pos = 0;
	UList<string> labels_anchor;
	//
	UList<double> title_pos = {0,0};

	UList<string> title_anchor;
	string title_transfo = "";
	// -----------------------
	// -----------------------
	// Draw X Axis
	// -----------------------
	ustring type = get(Keys::Type);
	if ( type.empty() ) type = "X";
	ustring axispos = get(Keys::Pos);
	ustring tickpos = get(Keys::TicksPos);
	ustring labelspos = get(Keys::LabelsPos);
	ustring titlepos = get(Keys::TitlePos);
	ustring title = get(Keys::TitleText);
	double ticksize = get(Keys::TicksSize).toDouble();
	if ( ticksize == 0 ) ticksize = 10;
	double minorticksize = get(Keys::MinorTicksSize).toDouble();
	if ( minorticksize == 0 ) minorticksize = 5;
	UList<double> labelsshift = {5,0};// = get("lb-shift").toDouble();
	UList<double> titleshift = {5,5};//get(Keys::TitleShift).toDouble();
	LineStyle linestyle;
	ustring tmp = get(Keys::LineColor);
	if ( !tmp.empty() )
		linestyle.setColor( tmp );
	tmp = get(Keys::LineCap);
	if ( !tmp.empty() )
		linestyle.setCap( tmp );
	tmp = get(Keys::LineDash);
	if ( !tmp.empty() )
		linestyle.setDash( tmp.toDequeDouble() );
	tmp = get(Keys::LineMiterLimit);
	if ( !tmp.empty() )
		linestyle.setMiterLimit( tmp.toDouble() );
	tmp = get(Keys::LineWidth);
	if ( !tmp.empty() )
		linestyle.setWidth( tmp.toDouble() );
	TextStyle labelsstyle;
	TextStyle titlestyle;
	if ( type == "X" )
	{
		// ---------------
		// Draw X Axis Line
		line_pos = _boundingRect.at(1)+_boundingRect.at(3);
		if ( axispos == "center" )
			line_pos = _boundingRect.at(1)+0.5*_boundingRect.at(3);
		else if ( axispos == "bottom" )
			line_pos = _boundingRect.at(1);
		drawing.drawLine( {GetGraphCoord(_min), line_pos}, {GetGraphCoord(_max),line_pos}, linestyle );
		// ---------------
		// Draw X Axis Ticks (major & minor)
		ticks_pos = line_pos;
		minorticks_pos = line_pos;
		if ( tickpos == "top" ||
			 (tickpos == "outside" && axispos == "top") ||
			 (tickpos == "inside" && axispos == "bottom") )
		{
			ticks_pos = line_pos - ticksize;
			minorticks_pos = line_pos - minorticksize;
		}
		else if ( tickpos == "center" )
		{
			ticks_pos = line_pos - 0.5*ticksize;
			minorticks_pos = line_pos - 0.5*minorticksize;
		}
		// calculate ticks_limits
		CalculateTicksPosition( ticks_limits, minorticks_limits, labels );
		for ( double xpos : ticks_limits )
		{
			drawing.drawLine( {xpos, ticks_pos}, {xpos,ticks_pos+ticksize}, linestyle );
		}
		for ( double xpos : minorticks_limits )
		{
			drawing.drawLine( {xpos, minorticks_pos}, {xpos,minorticks_pos+minorticksize}, linestyle );
		}
		// ---------------
		// Draw X Axis Labels
		labels_pos = ticks_pos + ticksize + labelsshift.at(1);
		labels_anchor = {"middle", "text-before-edge"};
		if (labelspos == "top" ||
			 (labelspos == "outside" && axispos == "top") ||
			 (labelspos == "inside" && axispos == "bottom") )
		{
			labels_pos = ticks_pos - labelsshift.at(1);
			labels_anchor.replace(0, "text-after-edge");
		}
		else if ( labelspos == "center" )
		{
			labels_pos = line_pos + labelsshift.at(1);
			labels_anchor.replace(0, "middle");
		}
		// ---------------
		// Open X Axis Labels Group
		labelsstyle.setAnchor( Anchor::Top );
		labelsstyle.setSize( 24.0 );
//		// ---------------
		// Draw each label
		Rect rect;
		for ( unsigned it = 0; it < labels.size(); ++it )
		{
			Rect tmp = drawing.drawText( labels.at(it), ticks_limits.at(it), labels_pos, labelsstyle );
			if ( it == 0 )
			{
				rect.setY( tmp.y() );
				rect.setHeight( tmp.height() );
			}
			else
			{
				if ( tmp.y() > rect.y() )
					rect.setY( tmp.y() );
				if ( tmp.height() > rect.height() )
					rect.setHeight( tmp.height() );
			}
		}
		// ---------------
		// Draw X Axis Title
		title_pos = { _boundingRect.at(0) + 0.5*_boundingRect.at(2) + titleshift.at(0), rect.y()+rect.height() + titleshift.at(1) };
		if ( ticks_pos + ticksize > rect.y()+rect.height() )
			title_pos.replace(1, ticks_pos + ticksize + titleshift.at(1));
		title_anchor = {"middle", "text-before-edge"};
		if (titlepos == "top" ||
			 (titlepos == "outside" && axispos == "top") ||
			 (titlepos == "inside" && axispos == "bottom") )
		{
			title_pos.replace( 1, rect.y() - titleshift.at(1) );
			if ( ticks_pos < rect.y() )
				title_pos.replace( 1, ticks_pos - titleshift.at(1) );
			title_anchor.replace( 1, "text-after-edge" );
		}
		titlestyle.setAnchor( Anchor::Top );
		titlestyle.setSize( 30.0);
		drawing.drawText( title, title_pos.at(0), title_pos.at(1), titlestyle );
		// Draw X Axis END
	}
	// -----------------------
	// -----------------------
	// Draw Y Axis
	// -----------------------
	else if ( type == "Y" )
	{
		// ---------------
		// Draw Y Axis Line
		line_pos = _boundingRect.at(0);
		if ( axispos == "center" )
			line_pos = _boundingRect.at(0)+0.5*_boundingRect.at(2);
		else if ( axispos == "right" )
			line_pos = _boundingRect.at(0)+_boundingRect.at(2);
		drawing.drawLine( {line_pos, GetGraphCoord(_min)}, {line_pos, GetGraphCoord(_max)}, linestyle );
		// ---------------
		// Draw Y Axis Ticks (major & minor)
		ticks_dir = "h";
		ticks_pos = line_pos - ticksize;
		minorticks_pos = line_pos - minorticksize;
		if ( tickpos == "right" ||
			 (tickpos == "outside" && axispos == "right") ||
			 (tickpos == "inside" && axispos == "left") )
		{
			ticks_pos = line_pos;
			minorticks_pos = line_pos;
		}
		else if ( tickpos == "center" )
		{
			ticks_pos = line_pos - 0.5*ticksize;
			ticks_pos = line_pos - 0.5*minorticksize;
		}
		// calculate ticks_limits
		CalculateTicksPosition( ticks_limits, minorticks_limits, labels	, true );
		for ( double ypos : ticks_limits )
		{
			drawing.drawLine( {ticks_pos, ypos}, {ticks_pos+ticksize, ypos}, linestyle );
		}
		for ( double ypos : minorticks_limits )
		{
			drawing.drawLine( {minorticks_pos, ypos}, {minorticks_pos+minorticksize, ypos}, linestyle );
		}
		// ---------------
		// Draw Y Axis Labels
		labels_pos = ticks_pos - labelsshift.at(0);
		labels_anchor = {"end", "middle"};
		if ( labelspos == "right" ||
			 (labelspos == "outside" && axispos == "right") ||
			 (labelspos == "inside" && axispos == "left") )
		{
			labels_pos = ticks_pos + ticksize + labelsshift.at(0);
			labels_anchor.replace(0, "middle");
		}
		else if ( labelspos == "center" )
		{
			labels_pos = line_pos + labelsshift.at(0);
			labels_anchor.replace(0, "middle");
		}
		// ---------------
		// Open Y Axis Labels Group
		labelsstyle.setAnchor( Anchor::Right );
		labelsstyle.setSize( 24.0);
		// ---------------
		// Draw each label
		Rect rect;
		for ( unsigned it = 0; it < labels.size(); ++it )
		{
			Rect tmp = drawing.drawText( labels.at(it), labels_pos, ticks_limits.at(it), labelsstyle );
			if ( it == 0 )
			{
				rect.setX( tmp.x() );
				rect.setWidth( tmp.width() );
			}
			else
			{
				if ( tmp.x() < rect.x() )
					rect.setX( tmp.x() );
				if ( tmp.width() > rect.width() )
					rect.setWidth( tmp.width() );
			}
		}
		// ---------------
		// Draw Y Axis Title
		title_pos = {rect.x() - titleshift.at(0), _boundingRect.at(1)+0.5*_boundingRect.at(3) + titleshift.at(1)};
		if ( ticks_pos < rect.x() )
			title_pos.replace(0, ticks_pos - titleshift.at(0));
		title_anchor = {"middle", "text-after-edge"};
		if ( titlepos == "right" ||
			 (titlepos == "outside" && axispos == "right") ||
			 (titlepos == "inside" && axispos == "left") )
		{
			title_pos.replace(0, rect.x()+rect.width() - titleshift.at(0));
			if ( ticks_pos + ticksize > rect.x()+rect.width() )
				title_pos.replace(0, ticks_pos + ticksize + titleshift.at(0));
			title_anchor.replace( 0, "text-after-edge" );
		}
		title_transfo = "transform=\"rotate(-90 "+ustring(title_pos.at(0))+" "+ustring(title_pos.at(1))+")\"";

		drawing.saveState();
		drawing.moveTo( title_pos.at(0), title_pos.at(1) );
		drawing.rotate( -90.0 );
		titlestyle.setAnchor( Anchor::Bottom );
		titlestyle.setSize( 30.0 );
		drawing.drawText( title, 0, 0, titlestyle );
		drawing.restoreState();
		// Draw Y Axis END
	}
}
/*------------------------------*/
/*------------------------------*/
void Axis::CalculateTicksPosition( UList<double>& tickspos, UList<double>& minortickspos, UList<ustring>& labels, bool isYaxis )
{
	double tickinterval = get(Keys::TicksIncrem).toDouble();
	double minortickinterval = get(Keys::MinorTicksIncrem).toDouble();
	char axis_dir = 1; if ( _min > _max ) axis_dir = -1;

	int quot = (int)(_min/tickinterval);
	if ( _min > 0 && _max > _min ) ++quot;
	else if ( _min < 0 && _max < _min ) --quot;

	int minorquot = (int)(_min/minortickinterval);
	if ( _min > 0 && _max > _min ) ++minorquot;
	else if ( _min < 0 && _max < _min ) --minorquot;

	double posp = quot*tickinterval;
	double pos = GetGraphCoord( posp );

	int it = 0;
	double minorposp = minorquot*minortickinterval;
	double minorpos = GetGraphCoord( minorposp );
	while (( (minorpos+1 < pos && !isYaxis) ||
			(minorpos-1 > pos && isYaxis) )&&it<100 )
	{
		minortickspos.append( minorpos );
		minorposp += axis_dir*minortickinterval;
		minorpos = GetGraphCoord( minorposp );
		++it;
	}
	it = 0;
	while (( (pos-1 < _boundingRect.at(0)+_boundingRect.at(2) && !isYaxis) ||
			(pos+1 > _boundingRect.at(1) && isYaxis) )&&it<100)
	{
		tickspos.append( pos );
		// -----------
		// Get Labels
		labels.append( ustring(posp) );
		// -----------
		int jt = 0;
		double minorposp = _minortickinterval;
		double minorpos = GetGraphCoord( posp + axis_dir*minorposp );
		while (( minorposp < _tickinterval &&
				((minorpos-1 < _boundingRect.at(0)+_boundingRect.at(2) && !isYaxis) ||
				 (minorpos+1 > _boundingRect.at(1) && isYaxis) ) )&&jt<100)
		{
			minortickspos.append( minorpos );
			minorposp += _minortickinterval;
			minorpos = GetGraphCoord( posp + axis_dir*minorposp );
			++jt;
		}
		posp += axis_dir*_tickinterval;
		pos = GetGraphCoord( posp );
		++it;
	}
}
/*------------------------------*/
/*------------------------------*/
double Axis::round( double value )
{
	double val = value;
	return val;
}
/*------------------------------*/
/*------------------------------*/
void Axis::CalculateScaleCoef()
{
	ustring type = get( Keys::Type );
	ustring scale = get( Keys::Scale );
	double scaleOpt = get( Keys::ScaleOption ).toDouble();
	double po = _boundingRect.at(0);
	double pm = _boundingRect.at(0)+_boundingRect.at(2);
	if ( type == "Y" )
	{
		po = _boundingRect.at(1)+_boundingRect.at(3);
		pm = _boundingRect.at(1);
	}
	if ( scale == "log10" )
	{
		if ( _min <= 0 )
			_min = 0.1;
		if ( _max <= 0 )
			_max = 1;
		_scaleA = ( pm - po ) / ( log10(_max) - log10(_min) );
		_scaleB = po - _scaleA*log10(_min);
	}
	else if ( scale == "log" )
	{
		if ( _min <= 0 )
			_min = 0.1;
		if ( _max <= 0 )
			_max = 1;
		_scaleA = ( pm - po ) / ( log(_max) - log(_min) );
		_scaleB = po - _scaleA*log(_min);
	}
	else if ( scale == "logx" )
	{
		if ( _min <= 0 )
			_min = 0.1;
		if ( _max <= 0 )
			_max = 1;
		_scaleA = log(scaleOpt) * ( pm - po ) / ( log(_max) - log(_min) );
		_scaleB = po - _scaleA*log(_min)/log(scaleOpt);
	}
	else if ( scale == "reciprocal" )
	{
		if ( _min == 0 )
			_min = 1;
		if ( _max == 0 )
			_max = 10;
		_scaleA = ( pm - po ) / ( 1/_max - 1/_min );
		_scaleB = po - _scaleA/_min;
	}
	else if ( scale == "offsetreciprocal" )
	{
		if ( _min+scaleOpt == 0 )
			_min = 1-scaleOpt;
		if ( _max+scaleOpt == 0 )
			_max = 10-scaleOpt;
		_scaleA = ( pm - po ) / ( 1/(_max+scaleOpt) - 1/(_min+scaleOpt) );
		_scaleB = po - _scaleA/(_min+scaleOpt);
	}
	else
	{
		_scaleA = ( pm - po ) / ( _max - _min );
		_scaleB = po - _scaleA*_min;
	}
}
/*------------------------------*/
/*------------------------------*/
double Axis::GetGraphCoord( double coord )
{
	ustring scale = get(Keys::Scale);
	double scaleOpt = get(Keys::ScaleOption).toDouble();
	if ( scale == "log10" )
		return _scaleA*log10(coord)+_scaleB;
	else if ( scale == "log" )
		return _scaleA*log(coord)+_scaleB;
	else if ( scale == "logx" )
		return _scaleA*log(coord)/log(scaleOpt)+_scaleB;
	else if ( scale == "reciprocal" )
		return _scaleA*(1/coord)+_scaleB;
	else if ( scale == "offsetreciprocal" )
		return _scaleA*(1/(coord+scaleOpt))+_scaleB;
	return _scaleA*coord+_scaleB;
}
/**********************************************/
/**********************************************/
