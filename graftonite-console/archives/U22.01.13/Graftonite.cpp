#include "Graftonite.h"
/**********************************************/
#include "Commons/UConsole.h"
/**********************************************/
#include "Commons/File.h"
/**********************************************/
/**********************************************/
/**********************************************/
Graftonite::Graftonite( int argc, char *argv[] )
{
	// ---------------------
	// TO FINISH
	cout << "\e[1m"; // Bold Text
	cout << endl << "----------------------------" << endl;
	cout << endl << "\x1B[34m" << " Welcome to Graftonite !" << endl;
	UConsole::normalColor();
	cout << "\e[1m"; // Bold Text
	cout << endl << "----------------------------" << endl;
	UConsole::normalColor();
	cout << "\e[0m";
	// ---------------------

	UList<ustring> sys_args;
	for ( int it = 1; it < argc; ++it )
		sys_args.append( argv[it] );

	bool open_interface = true;
	if ( !sys_args.isEmpty() )
	{
		ustring cmd = sys_args.takeFirst();
		if ( cmd.contains("help") || cmd == "-h" )
		{
//			help();
			open_interface = false;
		}
		else if ( cmd.contains("load") || cmd == "-l" )
		{
			ustring arg = argv[2];
			load( arg.split("#") );
		}
		else if ( cmd.contains("run") || cmd == "-r t" )
		{
			ustring arg = argv[2];
			runCommand(arg);
			open_interface = false;
		}
	}
	if ( open_interface )
		cout << endl;
}
/**********************************************/
int Graftonite::exec()
{
	UConsole::exec( this );
	exit();
	return 0;
}
/**********************************************/
/**********************************************/
void Graftonite::decomposeCommandLine( const ustring& cmdline, ustring& cmd, UList<ustring>& args )
{
	args.clear();
	unsigned long len = cmdline.length();

	ustring args_tmp;
	unsigned it;
	for ( it = 0; it < cmdline.size(); ++it  )
	{
		char c = cmdline.at(it);
		if ( c == ' ' )
		{
			if ( cmd.empty() )
				continue;
			break;
		}
		else
			cmd += c;
	}

	bool argMode = true;
	char limChar = 0;
//	ustring arg;
//	for ( ; it < cmdline.size(); ++it )
//	{
//		char c = cmdline.at(it);
//	}

	ustring arg;
	for ( ; it < len; ++it)
	{
		char c = cmdline.at(it);
		if ( c == ' ' && !limChar )
		{
			if ( argMode )
			{
				if ( !arg.empty() )
					args.append( arg );
				arg.clear();
			}
			argMode = false;
			continue;
		}
		else if ( c == '\"' || c == '\'' || c == '<' || c == '>' )
		{
			if ( c == limChar || (limChar == '<' && c == '>') )
			{
				limChar = 0;
				if ( !arg.empty() )
					args.append( arg );
				arg.clear();
				continue;
			}
			else if ( !limChar )
			{
				limChar = c;
				continue;
			}
		}
		else
		{
			argMode = true;
		}
		if ( argMode || limChar )
		{
			arg += c;
		}
	}
	if ( !arg.empty() )
		args.append(arg);
}
/**********************************************/
void Graftonite::runCommand( const ustring& cmdline )
{
	cout << endl;
//	cout << endl << cmdline << endl;
//	ustring inst/*, args*/;
//	int ind = 0;
//	for ( unsigned it = 0; it < cmdline.size(); ++it )
//	{
//		char c = cmdline.at(it);
//		if ( c == ' ' )
//		{
//			if ( inst.empty() )
//				continue;
//			ind = it + 1;
//			break;
//		}
//		inst += c;
//	}
//	args = cmdline.substr( ind );

	if ( cmdline.empty() ) return;
	ustring cmd;
	UList<ustring> args;
	decomposeCommandLine( cmdline, cmd, args );
//	if ( args.isEmpty() ) return;
//	cmd = args.first();
//	args.popFirst();

	if ( cmd.contains("new") || cmd == "n" )
		newElement(args);
	else if ( cmd.contains("reorder") || cmd == "=" )
		reorder(args);
	else if ( cmd.contains("insert") || cmd == "i" )
		insert(args);
	else if ( cmd.contains("delete") || cmd == "d" )
		deleteElement(args);
	else if ( cmd.contains("current") || cmd == "c" )
		setCurrentElement(args);
	else if ( cmd.contains("set") || cmd == "s" )
		set(args);
	else if ( cmd.contains("unset") || cmd == "u" )
		unset(args);
	else if ( cmd.contains("open") || cmd == "o" )
		open(args);
	else if ( cmd.contains("load") || cmd == "l" )
		load(args);
	else if ( cmd.contains("run") || cmd == "r" )
		run(args);
	else if ( cmd.contains("save") || cmd == "e" )
		save(args);
	else if ( cmd.contains("export") || cmd == "x" )
		exportGraph(args);
	else if ( cmd.contains("show") || cmd == "w" )
		show(args);
	else if ( cmd.contains("about") || cmd == "a" )
		about();
	else if ( cmd.contains("version") || cmd == "v" )
		version();
	else if ( cmd.contains("help") || cmd == "h" )
		help(args);
	else if ( cmd.contains("clear") || cmd == "k" )
		UConsole::clear();
	else if ( cmd.contains("exit") || cmd == "q" )
	{}//exit();
	else if ( cmd.contains("colors") )
		UConsole::testColors();
	else
		UConsole::warning_message("Unknown command !");
}
/**********************************************/
void Graftonite::newElement( UList<ustring>& args )
{
	if ( args.size() < 2 )
		UConsole::error_message("No arguments !!");
	else
	{
		ustring cmd = args.at(0),
				name = args.at(1);
		_graph.newElement( Element::getType(cmd), name );
		set( args );
	}
}
/**********************************************/
void Graftonite::reorder( UList<ustring>& /*args*/ )
{

}
/**********************************************/
void Graftonite::insert( UList<ustring>& /*args*/ )
{

}
/**********************************************/
void Graftonite::deleteElement( UList<ustring>& args )
{
	if ( args.isEmpty() )
	{
		UConsole::error_message("No arguments !!");
		return;
	}
	ustring cmd;
	if ( args.isEmpty() ) return;
	cmd = args.takeFirst();
	ustring name;
	if ( !args.isEmpty() )
	{
		name = args.takeFirst();
	}
	_graph.removeElement( Element::getType(cmd), name );
}
/**********************************************/
void Graftonite::setCurrentElement( UList<ustring>& args )
{
	_graph.setCurrent( args );
	cout << endl << endl;
}
/**********************************************/
void Graftonite::set( UList<ustring>& args )
{
	if ( args.isEmpty() )
		UConsole::error_message("No arguments !!");
	else
	{
		_graph.set( args ); // TO FINISH !
	}
	cout << endl;
}
/**********************************************/
void Graftonite::unset( UList<ustring>& args )
{
	if ( args.isEmpty() )
		UConsole::error_message("No arguments !!");
	else
		_graph.unset( args ); // TO CHECK !
	cout << endl << endl;
}
/**********************************************/
void Graftonite::open( UList<ustring>& args )
{
	if ( args.isEmpty() )
		UConsole::error_message("Missing file path !!");
	else
	{
		ustring filepath;
		if ( args.isEmpty() ) return;
		filepath = args.takeFirst();
		cout << "Opening : " << filepath;
	}
	cout << endl << endl;
}
/**********************************************/
void Graftonite::load( const UList<ustring>& args )
{
	if ( args.isEmpty() )
		UConsole::error_message("Missing file path !!");
	else
	{
		ustring filepath;
		if ( args.isEmpty() ) return;
		filepath = args.at(0);
		cout << "Loading : " << filepath;
		string contents = File::read( filepath );
		cout << endl << contents;
	}
	cout << endl << endl;
}
/**********************************************/
void Graftonite::run( UList<ustring>& args )
{
	if ( args.isEmpty() )
		UConsole::error_message("Missing file path !!");
	else
	{
		ustring filepath;
		if ( args.isEmpty() ) return;
		filepath = args.takeFirst();
		cout << "Running : " << filepath << endl;
		ustring contents = File::read( filepath );
		UList<ustring> contents2 = contents.split("\n");
		for ( auto line : contents2 )
		{
			if ( line.substr(0,2) != "//" && !line.empty() )
			{
				cout << endl;
				UConsole::drawCommandLine(line);
				runCommand(line);
			}
		}
	}
	cout << endl << endl;
}
/**********************************************/
void Graftonite::save( UList<ustring>& args )
{
	if ( args.isEmpty() )
		UConsole::error_message("Missing file path !!");
	else
	{
	}
	cout << endl << endl;
}
/**********************************************/
void Graftonite::exportGraph( UList<ustring>& args )
{
	if ( args.isEmpty() )
		UConsole::error_message("Missing file path !!");
	else
		_graph.exportGraph();
	cout << endl << endl;
}
/**********************************************/
void Graftonite::show( UList<ustring>& args )
{
	if ( args.isEmpty() )
		_graph.showAll();
	else
	{
		ustring arg1;
		if ( args.isEmpty() ) return;
		arg1 = args.takeFirst();
		if ( arg1.contains("all") )
			_graph.showAll();
		else if ( arg1.contains("current") )
			_graph.showCurrent();
		else if ( arg1.contains("layers") )
			_graph.showAll( Element::LAYER );
		else if ( arg1.contains("frames") )
			_graph.showAll( Element::FRAME );
		else if ( arg1.contains("axes") )
			_graph.showAll( Element::AXIS );
		else if ( arg1.contains("plots") )
			_graph.showAll( Element::PLOT );
		else if ( arg1.contains("legends") )
			_graph.showAll( Element::LEGEND );
		else if ( arg1.contains("titles") )
			_graph.showAll( Element::TITLE );
		else
		{
			ustring name;
			if ( !args.isEmpty() )
			{
				name = args.takeFirst();
			}
			if ( arg1.contains("graph") )
				_graph.showAll( Element::GRAPH, name );
			else if ( arg1 == "-layer" || arg1 == "layer" )
				_graph.showAll( Element::LAYER, name );
			else if ( arg1 == "-frame" || arg1 == "frame" )
				_graph.showAll( Element::FRAME, name );
			else if ( arg1 == "-axis" || arg1 == "axis" )
				_graph.showAll( Element::AXIS, name );
			else if ( arg1 == "-plot" || arg1 == "plot" )
				_graph.showAll( Element::PLOT, name );
			else if ( arg1 == "-legend" || arg1 == "legend" )
				_graph.showAll( Element::LEGEND, name );
			else if ( arg1 == "-title" || arg1 == "title" )
				_graph.showAll( Element::TITLE, name );
		}
	}
	cout << endl << endl;
}
/**********************************************/
void Graftonite::about()
{
	// TO FINISH

	cout << endl << endl;
}
/**********************************************/
void Graftonite::version()
{
	// TO FINISH

	cout << endl << endl;
}
/**********************************************/
void Graftonite::help( UList<ustring>& /*args*/ )
{
	// TO FINISH
	cout << "Help manual : " << endl << "\t blabla";

	cout << endl << endl;
}
/**********************************************/
void Graftonite::exit()
{
	UConsole::normalColor();
	cout << "\e[1m"; // Bold Text
	cout << endl << "----------------------------" << endl;
	cout << endl << "Thank you for using Graftonite !";
	cout << endl << "See you later !" << endl;
	cout << endl << "----------------------------" << endl;
	cout << "\e[0m" << endl << endl;
	// ---------------------
}
/**********************************************/
/**********************************************/
/**********************************************/
