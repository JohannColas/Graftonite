TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle

SOURCES += \
	Commons/Cairo.cpp \
        Commons/File.cpp \
	Commons/Key.cpp \
        Commons/System.cpp \
	Commons/UConsole.cpp \
        Commons/UString.cpp \
        Commons/XML.cpp \
	Graftonite.cpp \
        Graph/Axis.cpp \
        Graph/Element.cpp \
        Graph/Frame.cpp \
        Graph/Graph.cpp \
        Graph/Layer.cpp \
        Graph/Legend.cpp \
        Graph/Plot.cpp \
        Graph/Title.cpp \
	SVGGraph.cpp \
        main.cpp

HEADERS += \
	Commons/Cairo.h \
	Commons/File.h \
	Commons/Key.h \
	Commons/Signal.h \
	Commons/System.h \
	Commons/UConsole.h \
	Commons/UList.h \
	Commons/UString.h \
	Commons/XML.h \
	Graftonite.h \
	Graph/Axis.h \
	Graph/Element.h \
	Graph/Frame.h \
	Graph/Graph.h \
	Graph/Layer.h \
	Graph/Legend.h \
	Graph/Plot.h \
	Graph/Title.h \
	SVGGraph.h

QT += widgets


INCLUDEPATH += \
/usr/include/cairo

DEPENDPATH += \
/usr/include/cairo

LIBS += \
    -L/usr/lib \
    -lcairo
