#include "SVGGraph.h"
/**********************************************/
#include <unistd.h>
/**********************************************/
#include "Commons/File.h"
/**********************************************/
/**********************************************/
/**********************************************/
void SVGGraph::decomposeCmdLine( const ustring& cmdline, UList<ustring>& args )
{
	args.clear();
	ustring key, arg;
	bool argMode = true;
	char limChar = 0;
	unsigned long len = cmdline.length();
	vector<ustring> cmdlist;
	ustring elm;
	for ( unsigned long it = 0; it < len; ++it)
	{
		char c = cmdline.at(it);
		if ( c == ' ' && !limChar )
		{
			if ( argMode )
			{
				if ( !elm.empty() )
					args.append( elm );
				elm.clear();
			}
			argMode = false;
			continue;
		}
		else if ( c == '\"' || c == '\'' || c == '<' || c == '>' )
		{
			if ( c == limChar || (limChar == '<' && c == '>') )
			{
				limChar = 0;
				if ( !elm.empty() )
					args.append( elm );
				elm.clear();
				continue;
			}
			else if ( !limChar )
			{
				limChar = c;
				continue;
			}
		}
		else
		{
			argMode = true;
		}
		if ( argMode || limChar )
		{
			elm += c;
		}
	}
	if ( !elm.empty() )
		args.append(elm);
}
/**********************************************/
/**********************************************/
void SVGGraph::analyseCommand( const ustring& cmdline )
{
	if ( cmdline.empty() ) return;
	ustring cmd;
	ustring primaryArg;
	//	map<string,string> args;
	UList<ustring> args;
	decomposeCmdLine( cmdline, args );
	if ( args.isEmpty() ) return;
	cmd = args.first();
	args.popFirst();

	if ( cmd.contains("new") || cmd == "n" )
		SVGGraph::newel(args);
	else if ( cmd.contains("reorder") || cmd == "=" )
		SVGGraph::reorder(args);
	else if ( cmd.contains("insert") || cmd == "i" )
		SVGGraph::insert(args);
	else if ( cmd.contains("delete") || cmd == "d" )
		SVGGraph::deleteel(args);
	else if ( cmd.contains("current") || cmd == "c" )
		SVGGraph::setcurrent(args);
	else if ( cmd.contains("set") || cmd == "s" )
		SVGGraph::set(args);
	else if ( cmd.contains("unset") || cmd == "u" )
		SVGGraph::unset(args);
	else if ( cmd.contains("open") || cmd == "o" )
		SVGGraph::open(args);
	else if ( cmd.contains("load") || cmd == "l" )
		SVGGraph::load(args);
	else if ( cmd.contains("run") || cmd == "r" )
		SVGGraph::run(args);
	else if ( cmd.contains("save") || cmd == "e" )
		SVGGraph::save(args);
	else if ( cmd.contains("export") || cmd == "x" )
		SVGGraph::exportg(args);
	else if ( cmd.contains("show") || cmd == "w" )
		SVGGraph::show(args);
	else if ( cmd.contains("about") || cmd == "a" )
		SVGGraph::about();
	else if ( cmd.contains("version") || cmd == "v" )
		SVGGraph::version();
	else if ( cmd.contains("help") || cmd == "h" )
		SVGGraph::help(args);
	else if ( cmd.contains("clear") || cmd == "k" )
		SVGGraph::clear();
	else if ( cmd.contains("exit") || cmd == "q" )
		SVGGraph::exit();
	else if ( cmd.contains("colors") )
		SVGGraph::testColors();
	else
		SVGGraph::warning_message("Unknown command !");
}
/**********************************************/
/**********************************************/
void SVGGraph::newel( UList<ustring>& args )
{
	// Working !
	if ( args.size() < 2 )
		SVGGraph::error_message("No arguments !!");
	else
	{
		ustring cmd = args.at(0),
				name = args.at(1);
		_graph.newElement( Element::getType(cmd), name );
		SVGGraph::set( args );
	}
}
/**********************************************/
/**********************************************/
void SVGGraph::reorder( UList<ustring>& /*args*/ )
{
	// TO FINISH !
}
/**********************************************/
/**********************************************/
void SVGGraph::insert( UList<ustring>& /*args*/ )
{
	// TO FINISH !
}
/**********************************************/
/**********************************************/
void SVGGraph::deleteel( UList<ustring>& args )
{
	// Working !
	if ( args.isEmpty() )
	{
		SVGGraph::error_message("No arguments !!");
		return;
	}
	ustring cmd;
	if ( args.isEmpty() ) return;
	cmd = args.takeFirst();
	ustring name;
	if ( !args.isEmpty() )
	{
		name = args.takeFirst();
	}
	_graph.removeElement( Element::getType(cmd), name );
}
/**********************************************/
/**********************************************/
void SVGGraph::setcurrent( UList<ustring>& args )
{
	// Working !
	_graph.setCurrent( args );
}
/**********************************************/
/**********************************************/
void SVGGraph::set( UList<ustring>& args )
{
	// Working !
	if ( args.isEmpty() )
		SVGGraph::error_message("No arguments !!");
	else
	{
		_graph.set( args ); // TO FINISH !
	}
}
/**********************************************/
/**********************************************/
void SVGGraph::unset( UList<ustring>& args )
{
	// Working !
	if ( args.isEmpty() )
		SVGGraph::error_message("No arguments !!");
	else
		_graph.unset( args ); // TO CHECK !
}
/**********************************************/
/**********************************************/
void SVGGraph::open( UList<ustring>& args )
{
	 // TO CHECK !
	if ( args.isEmpty() )
		SVGGraph::error_message("Missing file path !!");
	else
	{
		ustring filepath;
		if ( args.isEmpty() ) return;
		filepath = args.takeFirst();
		cout << endl << "Opening : " << filepath << endl;
	}
}
/**********************************************/
/**********************************************/
void SVGGraph::load( UList<ustring>& args )
{
	 // TO CHECK !
	if ( args.isEmpty() )
		SVGGraph::error_message("Missing file path !!");
	else
	{
		ustring filepath;
		if ( args.isEmpty() ) return;
		filepath = args.takeFirst();
		cout << endl << "Loading : " << filepath << endl;
		string contents = File::read( filepath );
		cout << contents << endl;
	}
}
/**********************************************/
/**********************************************/
void SVGGraph::run( UList<ustring>& args )
{
	 // TO CHECK !
	if ( args.isEmpty() )
		SVGGraph::error_message("Missing file path !!");
	else
	{
		ustring filepath;
		if ( args.isEmpty() ) return;
		filepath = args.takeFirst();
		cout << endl << "Running : " << filepath << endl;
		ustring contents = File::read( filepath );
		UList<ustring> contents2 = contents.split("\n");
		for ( auto line : contents2 )
		{
			if ( line.substr(0,2) != "//" && !line.empty() )
			{
				cout << endl;
				SVGGraph::drawCommandLine(line);
				SVGGraph::analyseCommand(line);
			}
		}
		cout <<endl;
	}
}
/**********************************************/
/**********************************************/
void SVGGraph::save( UList<ustring>& args )
{
	// WORKING !
	if ( args.isEmpty() )
		SVGGraph::error_message("Missing file path !!");
	else
		_graph.save( args.at(0) ); // TO CHECK !
}
/**********************************************/
/**********************************************/
void SVGGraph::exportg( UList<ustring>& args )
{
	// TO FINISH
	if ( args.isEmpty() )
		SVGGraph::error_message("Missing file path !!");
	else
		_graph.save( args.at(0) ); // TO CHECK !
}
/**********************************************/
/**********************************************/
void SVGGraph::show( UList<ustring>& args )
{
	// Working !
	if ( args.isEmpty() )
		_graph.showAll();
	else
	{
		ustring arg1;
		if ( args.isEmpty() ) return;
		arg1 = args.takeFirst();
		if ( arg1.contains("all") )
			_graph.showAll();
		else if ( arg1.contains("current") )
			_graph.showCurrent();
		else if ( arg1.contains("layers") )
			_graph.showAll( Element::LAYER );
		else if ( arg1.contains("frames") )
			_graph.showAll( Element::FRAME );
		else if ( arg1.contains("axes") )
			_graph.showAll( Element::AXIS );
		else if ( arg1.contains("plots") )
			_graph.showAll( Element::PLOT );
		else if ( arg1.contains("legends") )
			_graph.showAll( Element::LEGEND );
		else if ( arg1.contains("titles") )
			_graph.showAll( Element::TITLE );
		else
		{
			ustring name;
			if ( !args.isEmpty() )
			{
				name = args.takeFirst();
			}
			if ( arg1.contains("graph") )
				_graph.showAll( Element::GRAPH, name );
			else if ( arg1 == "-layer" || arg1 == "layer" )
				_graph.showAll( Element::LAYER, name );
			else if ( arg1 == "-frame" || arg1 == "frame" )
				_graph.showAll( Element::FRAME, name );
			else if ( arg1 == "-axis" || arg1 == "axis" )
				_graph.showAll( Element::AXIS, name );
			else if ( arg1 == "-plot" || arg1 == "plot" )
				_graph.showAll( Element::PLOT, name );
			else if ( arg1 == "-legend" || arg1 == "legend" )
				_graph.showAll( Element::LEGEND, name );
			else if ( arg1 == "-title" || arg1 == "title" )
				_graph.showAll( Element::TITLE, name );
		}
	}
	cout << endl;
}
/**********************************************/
/**********************************************/
void SVGGraph::about()
{
	// TO FINISH

}
/**********************************************/
/**********************************************/
void SVGGraph::version()
{
	// TO FINISH

}
/**********************************************/
/**********************************************/
void SVGGraph::help( UList<ustring>& /*args*/ )
{
	// TO FINISH
	cout << endl << "Help manual : " << endl << "\t blabla" << endl;

}
/**********************************************/
/**********************************************/
void SVGGraph::exit()
{
	// TO FINISH
	cout << endl << endl << "Thank you for using SVGGraph !" << endl;
	cout << "See you later !" << endl;
}
/**********************************************/
/**********************************************/
// Terminal Commands
/**********************************************/
/**********************************************/
void SVGGraph::init()
{
	//	struct termios old_tio, new_tio;
	/* get the terminal settings for stdin */
	tcgetattr( STDIN_FILENO, &old_tio );
	/* we want to keep the old setting to restore them a the end */
	new_tio = old_tio;
	/* disable canonical mode (buffered i/o) and local echo */
	new_tio.c_lflag &=( ~ICANON & ~ECHO );
	/* set the new settings immediately */
	tcsetattr( STDIN_FILENO, TCSANOW, &new_tio );
}
/**********************************************/
/**********************************************/
void SVGGraph::close()
{
	/* restore the former settings */
	tcsetattr( STDIN_FILENO, TCSANOW, &old_tio );
}
/**********************************************/
/**********************************************/
void SVGGraph::exec( int argc, char *argv[] )
{
	// ---------------------
	// TO FINISH
	cout << "\e[1m"; // Bold Text
	cout << endl << "----------------------------" << endl;
	cout << endl << "\x1B[34m" << " Welcome to SVGGraph !" << endl;
	SVGGraph::normalColor();
	cout << "\e[1m"; // Bold Text
	cout << endl << "----------------------------" << endl;
	SVGGraph::normalColor();
	cout << "\e[0m";
	// ---------------------

	UList<ustring> sys_args;
	for ( int it = 1; it < argc; ++it )
		sys_args.append( argv[it] );

	bool open_interface = true;
	if ( !sys_args.isEmpty() )
	{
		ustring cmd = sys_args.takeFirst();
		if ( cmd.contains("help") || cmd == "-h" )
		{
//			graph.help();
			open_interface = false;
		}
		else if ( cmd.contains("load") || cmd == "-l" )
		{
			ustring arg = argv[2];
//			graph.load(arg);
		}
		else if ( cmd.contains("run") || cmd == "-r t" )
		{
			ustring arg = argv[2];
//			graph.run(arg);
			open_interface = false;
		}
	}
	cout << endl;



	if ( !open_interface )
		return;
	/* --------------------------- */
	// Terminal Initialisation
	SVGGraph::init();
	/* --------------------------- */
	unsigned char c;
	ustring cur_cmd;
	vector<ustring> cmd_history;
	int history_index = -1;
	int cursor_pos = 0;
	bool exit = false;
	SVGGraph::startCommandLine();
	do
	{
		if ( !cur_cmd.empty() )
			SVGGraph::startCommandLine();
		cursor_pos = 0;
		history_index = -1;
		cur_cmd.clear();
		do
		{
			c = getchar();
			if ( c == KEYS::ESC ) // escape key
			{
				char new_c = getchar();
				if ( new_c == 91 )
				{
					new_c = getchar();
					if ( new_c == KEYS::UP_ARROW ) // Up Arrow
					{
						SVGGraph::clearCommandLine();
						cur_cmd.clear();
						if ( history_index == -1 )
						{
							history_index = cmd_history.size()-1;
						}
						else
							--history_index;
						if ( history_index < 0 )
							history_index = 0;
						if ( history_index != -1 && cmd_history.size() > 0 )
						{
							cur_cmd = cmd_history.at(history_index);
							SVGGraph::clearCommandLine();
							SVGGraph::drawCommandLine( cur_cmd );
							cursor_pos = cur_cmd.length();
						}
					}
					else if ( new_c == KEYS::DOWN_ARROW ) // Down Arrow
					{
						SVGGraph::clearCommandLine();
						cur_cmd.clear();
						cursor_pos = 0;
						if ( history_index > -1 )
							++history_index;
						if ( history_index > (int)cmd_history.size()-1 )
							history_index = -1;
						if ( history_index != -1 && cmd_history.size() > 0 )
						{
							cur_cmd = cmd_history.at(history_index);
							SVGGraph::clearCommandLine();
							SVGGraph::drawCommandLine( cur_cmd );
							cursor_pos = cur_cmd.length();
						}
					}
					else if ( new_c == KEYS::RIGHT_ARROW ) // Right Arrow
					{
						if ( !cur_cmd.empty() )
						{
							++cursor_pos;
							if ( cursor_pos < (int)cur_cmd.length()+1 )
								SVGGraph::moveCursorForward(1);
							if ( cursor_pos > (int)cur_cmd.length() )
								cursor_pos = cur_cmd.length();
						}
					}
					else if ( new_c == KEYS::LEFT_ARROW )// Left Arrow
					{
						if ( !cur_cmd.empty() )
						{
							--cursor_pos;
							if ( cursor_pos >= 0 )
								SVGGraph::moveCursorBackward(1);
							if ( cursor_pos < 0 )
								cursor_pos = 0;
						}
					}
					else if ( new_c == KEYS::FN_RIGHT )// FN + Right Arrow
					{
						if ( !cur_cmd.empty() && cursor_pos < (int)cur_cmd.length() )
						{
							int diff = cur_cmd.length()-cursor_pos;
							if ( diff > 0 )
							{
								SVGGraph::moveCursorForward(diff);
								cursor_pos = cur_cmd.length();
							}
						}
					}
					else if ( new_c == KEYS::FN_LEFT )// FN + Left Arrow
					{
						if ( !cur_cmd.empty() && cursor_pos > 0 )
						{
								SVGGraph::moveCursorBackward(cursor_pos);
								cursor_pos=0;
						}
					}
					else if ( new_c == 51 )
					{
						new_c = getchar();
						if ( new_c == KEYS::DEL ) // delete key
						{
							if ( cursor_pos < (int)cur_cmd.length() )
							{
								int old_dif = cur_cmd.length() - cursor_pos;
								cur_cmd.erase( cursor_pos, 1 );
								SVGGraph::clearCommandLine();
								SVGGraph::drawCommandLine( cur_cmd );
								if ( old_dif > 0 )
								{
									SVGGraph::moveCursorBackward(old_dif);
									SVGGraph::moveCursorForward(1);
								}
							}
						}
					}
				}
				else // escape key
				{
					exit = true;
					SVGGraph::exit();
					break;
				}
			}
			else if ( c == KEYS::RETURN )
			{
				SVGGraph::normalColor();
				SVGGraph::analyseCommand( cur_cmd );
				history_index = -1;
				cursor_pos = 0;
				if ( !cur_cmd.empty() )
					cmd_history.push_back( cur_cmd );
			}
			else if ( c == KEYS::BACKSPACE ) // BACKSPACE KEY
			{
				// Reset Current Cmd Index
				history_index = -1;
				//
				int old_dif = cur_cmd.length() - cursor_pos;
				--cursor_pos;
				if ( cur_cmd.length() > 0 && cursor_pos >= 0 )
				{
					cur_cmd.erase( cursor_pos, 1 );
					SVGGraph::clearCommandLine();
					SVGGraph::drawCommandLine( cur_cmd );
					SVGGraph::moveCursorBackward(old_dif);
				}
				if ( cursor_pos < 0 )
					cursor_pos = 0;
			}
			else
			{
				int old_dif = cur_cmd.length() - cursor_pos;
				cur_cmd.insert( cursor_pos, 1, c );
				++cursor_pos;
				SVGGraph::clearCommandLine();
				SVGGraph::drawCommandLine( cur_cmd );
				SVGGraph::moveCursorBackward(old_dif);
			}
		}
		while ( c != KEYS::RETURN );
	}
	while( cur_cmd.substr(0,4) != "exit" && !exit );

	cout << endl;
}
/**********************************************/
/**********************************************/
void SVGGraph::clear()
{
	printf( "\033[2J" );
	printf( "\033[0;0f" );
}
/**********************************************/
/**********************************************/
void SVGGraph::changeCursorCol( int delta )
{
	if ( delta < 0 )
		moveCursorBackward( delta );
	else
		moveCursorForward( delta );
}
/**********************************************/
/**********************************************/
void SVGGraph::moveCursorBackward( int delta )
{
	if ( delta > 0 )
		printf( "\033[%dD", delta );
}
/**********************************************/
/**********************************************/
void SVGGraph::moveCursorForward( int delta )
{
	if ( delta > 0 )
		printf( "\033[%dC", delta );
}
/**********************************************/
/**********************************************/
void SVGGraph::saveCursorPos()
{
	printf("\033[s"); // save cursor position
}
/**********************************************/
/**********************************************/
void SVGGraph::restoreCursorPos()
{
	printf("\033[u"); // restore cursor position
}
/**********************************************/
/**********************************************/
void SVGGraph::deleteToEndLine()
{
	printf("\033[K"); // delete char from cursor position to end line
}
/**********************************************/
/**********************************************/
void SVGGraph::normalColor()
{
	printf( "\033[0m" );
}
/**********************************************/
/**********************************************/
void SVGGraph::cmdInputColor()
{
	printf( "\033[1;36m" );
}
/**********************************************/
/**********************************************/
void SVGGraph::cmdColor()
{
	printf( "\x1B[93m" );
}
/**********************************************/
/**********************************************/
void SVGGraph::warningColor()
{
	printf( "\x1B[33m" );
}
/**********************************************/
/**********************************************/
void SVGGraph::errorColor()
{
	printf( "\x1B[31m" );
}
/**********************************************/
/**********************************************/
void SVGGraph::testColors()
{
	printf("\n");
	printf("\x1B[31mTexting\033[0m\t\t");//ROUGE
	printf("\x1B[32mTexting\033[0m\t\t");//VERT
	printf("\x1B[33mTexting\033[0m\t\t");//ORANGE
	printf("\x1B[34mTexting\033[0m\t\t");//BLEU
	printf("\x1B[35mTexting\033[0m\n");//VIOLET

	printf("\x1B[36mTexting\033[0m\t\t");//BLEUVERT
	printf("\x1B[36mTexting\033[0m\t\t");//BLEUVERT
	printf("\x1B[36mTexting\033[0m\t\t");//BLEUVERT
	printf("\x1B[37mTexting\033[0m\t\t");//BLANC
	printf("\x1B[93mTexting\033[0m\n");//JAUNE

	printf("\033[3;42;30mTexting\033[0m\t\t");
	printf("\033[3;43;30mTexting\033[0m\t\t");
	printf("\033[3;44;30mTexting\033[0m\t\t");
	printf("\033[3;104;30mTexting\033[0m\t\t");
	printf("\033[3;100;30mTexting\033[0m\n");

	printf("\033[3;47;35mTexting\033[0m\t\t");
	printf("\033[2;47;35mTexting\033[0m\t\t");
	printf("\033[1;47;35mTexting\033[0m\t\t");
	printf("\n");
}
/**********************************************/
/**********************************************/
void SVGGraph::warning_message( const ustring& message )
{
	SVGGraph::warningColor();
	printf( "\nWarning: %s\n\n", message.c_str() );
	SVGGraph::normalColor();
}
/**********************************************/
/**********************************************/
void SVGGraph::error_message( const ustring& message )
{
	SVGGraph::errorColor();
	printf( "\nError: %s\n\n", message.c_str() );
	SVGGraph::normalColor();
}
/**********************************************/
/**********************************************/
void SVGGraph::startCommandLine()
{
	SVGGraph::cmdInputColor();
	printf( "%s> ", _progname.c_str() );
	SVGGraph::normalColor();
	SVGGraph::saveCursorPos();
}
/**********************************************/
/**********************************************/
void SVGGraph::clearCommandLine()
{
	SVGGraph::restoreCursorPos();
	SVGGraph::deleteToEndLine();
}
/**********************************************/
/**********************************************/
void SVGGraph::drawCommandLine( const ustring& cmdline )
{
	bool first_space_found = false;
	SVGGraph::cmdColor();
	for ( char c : cmdline )
	{
		if ( c == ' ' && !first_space_found )
		{
			SVGGraph::normalColor();
			printf( " " );
			first_space_found = true;
		}
		else
			printf( "%c", c );
	}
	SVGGraph::normalColor();
}
/**********************************************/
/**********************************************/
bool SVGGraph::confirmationDialog( const ustring& message )
{
	std::cout << std::endl << message;
	char c = 'n';
	std::cin >> c;
	std::cout << c << std::endl;
	return (c == 'Y' || c == 'y');
}
/**********************************************/
/**********************************************/
