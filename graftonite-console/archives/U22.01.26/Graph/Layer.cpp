#include "Layer.h"
/**********************************************/
/**********************************************/
/**********************************************/
Layer::~Layer()
{

}
/**********************************************/
/**********************************************/
Layer::Layer( Graph* parent )
	: Element(parent)
{
	init();
}
Layer::Layer( const ustring& name, unsigned index, Graph* parent, bool log )
	: Element(parent)
{
	init();
	Element::init( name, index, log );
}
void Layer::init()
{
	_type = Key::Layer;
}
/**********************************************/
/**********************************************/
bool Layer::set( const Key& key, const ustring& value )
{
	if ( key == Key::Name ||
		 key == Key::Color )
		return Element::set( key, value );

	return false;
}
/**********************************************/
/**********************************************/
