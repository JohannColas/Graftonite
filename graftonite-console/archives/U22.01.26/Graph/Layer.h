#ifndef LAYER_H
#define LAYER_H
/**********************************************/
#include "Element.h"
/**********************************************/
/**********************************************/
/**********************************************/
class Layer : public Element
{
public:
	~Layer();
	Layer( Graph* parent = nullptr );
	Layer( const ustring& name, unsigned index, Graph* parent = nullptr, bool log = false );
	void init();
	/**********************************************/
	bool set( const Key& key, const ustring& value ) override;
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LAYER_H
