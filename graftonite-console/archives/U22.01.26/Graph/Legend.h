#ifndef LEGEND_H
#define LEGEND_H
/**********************************************/
#include "Element.h"
/**********************************************/
/**********************************************/
/**********************************************/
class Legend : public Element
{
public:
	Legend( Graph* parent = nullptr );
	Legend( const ustring& name, unsigned index, Graph* parent = nullptr, bool log = false );
	void init();
	/**********************************************/
	bool set( const Key& key, const ustring& value ) override;
	void draw( Cairo* drawing ) override;
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LEGEND_H
