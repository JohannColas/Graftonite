#include "Legend.h"
/**********************************************/
#include "Commons/Cairo.h"
/**********************************************/
#include "Graph.h"
/**********************************************/
/**********************************************/
Legend::Legend( Graph* parent )
	: Element(parent)
{
	init();
}
Legend::Legend( const ustring& name, unsigned index, Graph* parent, bool log )
	: Element(parent)
{
	init();
	Element::init( name, index, log );
}
void Legend::init()
{
	_type = Key::Legend;
}
/**********************************************/
/**********************************************/
bool Legend::set( const Key& key, const ustring& value )
{
	if ( key == Key::Name )
	{

		return Element::set( key, value );
	}
	return false;
}
/**********************************************/
/**********************************************/
void Legend::draw( Cairo* drawing )
{
	double scale = 1;
	Rect rec = {180,110,50,20};

	drawing->saveState();
	drawing->scale( scale );

	Plot* plot = _parent->plot("0");
	if ( plot )
	{
		double gap = plot->line_gap;
		LineStyle style = plot->line_style;
		if ( gap > 0 )
		{
			drawing->drawLine( rec.centerLeft(), {rec.xCenter()-gap, rec.yCenter()}, style );
			drawing->drawLine( {rec.xCenter()+gap, rec.yCenter()}, rec.centerRight(), style );
		}
		else
			drawing->drawLine( rec.centerLeft(), rec.centerRight(), style );

		Symbol symbol = plot->symbol;
		ShapeStyle symbol_style = plot->symbol_style;
		drawing->drawSymbols( symbol, rec.center(), symbol.size(), symbol_style );

		TextStyle tsty;
		tsty.setAnchor( Key::Left );
		tsty.setSize( 24 );
		drawing->drawText( "POppppp", rec.xRight()+5, rec.yCenter(), tsty );
	}
	else
		std::cout << "plot not find !" << std::endl;
	drawing->restoreState();
}
/**********************************************/
/**********************************************/
