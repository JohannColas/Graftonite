#include "Cairo.h"
/**********************************************/
/**********************************************/
/**********************************************/
Cairo::Cairo( const Cairo::Format& format )
{
	_format = format;
}
/**********************************************/
/**********************************************/
Cairo::Cairo( const ustring& format, const ustring& width, const ustring& height )
{
	if ( format == "svg" )
		_format = Cairo::SVG;
	else if ( format == "pdf" )
		_format = Cairo::PDF;
	else if ( format == "png" )
		_format = Cairo::PNG;
	else if ( format == "ps" )
		_format = Cairo::PS;
	size.setWidth( width.toDouble() );
	size.setHeight( height.toDouble() );
}
/**********************************************/
/**********************************************/
void Cairo::setPath(const ustring& path)
{
	this->path = path;
}
/**********************************************/
/**********************************************/
void Cairo::setFileName(const ustring& filename)
{
	this->filename = filename;
}
/**********************************************/
/**********************************************/
void Cairo::setFormat(const Cairo::Format& format)
{
	_format = format;
}
/**********************************************/
/**********************************************/
void Cairo::setSize(const Size& size)
{
	this->size = size;
}
/**********************************************/
/**********************************************/
cairo_t* Cairo::create()
{
	init();

	return _drawing;
}
/**********************************************/
/**********************************************/
void Cairo::init()
{
	//	close();
	switch ( _format )
	{
		case Cairo::PNG:
			_surface = cairo_image_surface_create(
						   CAIRO_FORMAT_ARGB32,
						   size.width(),
						   size.height() );
		break;
		case Cairo::PDF:
			_surface = cairo_pdf_surface_create (
						   ustring(path + '/' + filename +".pdf").c_str(),
						   size.width(),
						   size.height() );
		break;
		case Cairo::SVG:
			_surface = cairo_svg_surface_create(
						   ustring(path + '/' + filename +".svg").c_str(),
						   size.width(),
						   size.height() );
		break;
		case Cairo::PS:
			_surface = cairo_ps_surface_create(
						   ustring(path + '/' + filename +".ps").c_str(),
						   size.width(),
						   size.height() );
		break;
	}

	if ( _surface != nullptr )
		_drawing = cairo_create( _surface );

	// CAIRO_ANTIALIAS_  DEFAULT/NONE/GRAY/SUBPIXEL/FAST/GOOD/BEST
	cairo_set_antialias( _drawing, CAIRO_ANTIALIAS_GOOD );
}
/**********************************************/
/**********************************************/
cairo_surface_t* Cairo::surface()
{
	return _surface;
}
/**********************************************/
/**********************************************/
cairo_t* Cairo::drawing()
{
	return _drawing;
}
/**********************************************/
/**********************************************/
void Cairo::saveState()
{
	cairo_save( _drawing );
}
/**********************************************/
/**********************************************/
void Cairo::restoreState()
{
	cairo_restore( _drawing );
}
/**********************************************/
/**********************************************/
void Cairo::moveTo( double x, double y )
{
	current_pos.setX( x - current_pos.x() );
	current_pos.setY( y - current_pos.y() );
	cairo_translate( _drawing, current_pos.x(), current_pos.y() );
}
/**********************************************/
/**********************************************/
void Cairo::compose()
{
	cairo_t *first_cr, *second_cr;
	cairo_surface_t *first, *second;

	first = cairo_surface_create_similar(cairo_get_target( _drawing ),
										 CAIRO_CONTENT_COLOR_ALPHA, 100, 35);

	second = cairo_surface_create_similar(cairo_get_target( _drawing ),
										  CAIRO_CONTENT_COLOR_ALPHA, 100, 21);

	first_cr = cairo_create(first);
	cairo_set_source_rgb(first_cr, 0, 0, 0.4);
	cairo_rectangle(first_cr, 0, 20, 50, 50);
	cairo_fill(first_cr);

	second_cr = cairo_create(second);
	cairo_set_source_rgb(second_cr, 0.5, 0.5, 0);
	cairo_rectangle(second_cr, 0+10, 40, 50, 50);
	cairo_fill(second_cr);

	// CAIRO_OPERATOR_DEST_OVER,
	// CAIRO_OPERATOR_DEST_IN,
	// CAIRO_OPERATOR_OUT,
	// CAIRO_OPERATOR_ADD,
	// CAIRO_OPERATOR_ATOP,
	// CAIRO_OPERATOR_DEST_ATOP,
	cairo_set_operator(first_cr, CAIRO_OPERATOR_DEST_OVER);
	cairo_set_source_surface(first_cr, second, 0, 0);
	cairo_paint(first_cr);

	cairo_set_source_surface( _drawing, first, 0, 0);
	cairo_paint( _drawing );

	cairo_surface_destroy(first);
	cairo_surface_destroy(second);

	cairo_destroy(first_cr);
	cairo_destroy(second_cr);
}
/**********************************************/
/**********************************************/
void Cairo::clip()
{
	cairo_arc( _drawing, 0, 0, 50, 0, 2*M_PI );
	cairo_clip( _drawing );
	cairo_paint( _drawing );
}
/**********************************************/
/**********************************************/
void Cairo::mask( cairo_surface_t* surface )
{
	cairo_mask_surface( _drawing, surface, 0, 0);
	cairo_fill( _drawing );
}
/**********************************************/
/**********************************************/
void Cairo::transform()
{
	// Translation
	cairo_translate( _drawing, 20, 20);

	// Rotation
	cairo_rotate( _drawing, M_PI*0.5 );

	// Shear
	cairo_matrix_t matrix;
	cairo_matrix_init(&matrix,
					  1.0, 0.5,
					  0.0, 1.0,
					  0.0, 0.0);
	cairo_transform( _drawing, &matrix);

	// Scaling
	cairo_scale( _drawing, 0.6, 0.6);

	// Isolated Transformationcairo_save( _drawing );
	cairo_scale( _drawing, 0.6, 0.6);
	cairo_set_source_rgb( _drawing, 0.8, 0.3, 0.2);
	cairo_rectangle( _drawing, 30, 30, 90, 90);
	cairo_fill( _drawing );
	cairo_restore( _drawing );

}
/**********************************************/
/**********************************************/
void Cairo::rotate( double angle )
{
	cairo_rotate( _drawing, angle * M_PI / 180 );
}
void Cairo::scale( double scale )
{
	cairo_scale( _drawing, scale, scale );
}
/**********************************************/
/**********************************************/
void Cairo::setLineStyle( const LineStyle& style )
{
	// Line color
	cairo_set_source_rgba( _drawing, style.color().red(), style.color().green(), style.color().blue(), style.color().alpha() );
	// Line width
	cairo_set_line_width( _drawing, style.width() );
	// Line Dashes
	if ( !style.dash().isEmpty() )
	{
		int dash_len = style.dash().size();
		double dash_tmp[dash_len];
		for ( int dt = 0; dt < dash_len; ++dt )
			dash_tmp[dt] = style.dash().at(dt);
		cairo_set_dash( _drawing, dash_tmp, dash_len, 0 );
	}
	// Line Cap
	cairo_set_line_cap( _drawing, style.cap() );
	// Line Join
	cairo_set_line_join( _drawing, style.join() );
	if ( style.join() == CAIRO_LINE_JOIN_MITER )
		cairo_set_miter_limit( _drawing, style.miterLimit() );
}
/**********************************************/
/**********************************************/
void Cairo::setFillStyle( const FillStyle& style )
{
	switch ( style.type() )
	{
		case FillStyle::Flat:
			cairo_set_source_rgba( _drawing, style.color().red(), style.color().green(), style.color().blue(), style.color().alpha() );
		break;
		case FillStyle::LinearGradient:
			cairo_pattern_t *pat3;
			pat3 = cairo_pattern_create_linear(20.0, 260.0, 20.0, 360.0);
			cairo_pattern_add_color_stop_rgb(pat3, 0.1, 0, 0, 0);
			cairo_pattern_add_color_stop_rgb(pat3, 0.5, 1, 1, 0);
			cairo_pattern_add_color_stop_rgb(pat3, 0.9, 0, 0, 0);
			cairo_set_source( _drawing, pat3);
		break;
		case FillStyle::RadialGradient:
			cairo_pattern_t *r1;
			r1 = cairo_pattern_create_radial(30, 30, 10, 30, 30, 90);
			cairo_pattern_add_color_stop_rgba(r1, 0, 1, 1, 1, 1);
			cairo_pattern_add_color_stop_rgba(r1, 1, 0.6, 0.6, 0.6, 1);
			cairo_set_source( _drawing, r1);
		break;
		default:
		break;
	}
}
/**********************************************/
/**********************************************/
void Cairo::setTextStyle( const TextStyle& style )
{
	// Text Color
	cairo_set_source_rgba( _drawing, style.color().red(), style.color().green(), style.color().blue(), style.color().alpha() );
	// Text font
	cairo_select_font_face( _drawing,
							style.family().c_str(),
							style.slant(),
							style.weight() );
	// Text Font size
//	cairo_set_font_size( _drawing, style.size() );
//	if ( style.cap() == TextStyle::SmallCap )
//		cairo_set_font_size( _drawing, 0.8*style.size()+0.1 );
//	else
	    cairo_set_font_size( _drawing, style.size() );
}
/**********************************************/
/**********************************************/
void Cairo::drawLine( const Line& line, const LineStyle& style )
{
	drawLine( line.p1(), line.p2(), style );
}
/**********************************************/
/**********************************************/
void Cairo::drawLine( const Point& p1, const Point& p2, const LineStyle& style )
{
	cairo_move_to( _drawing, p1.x(), p1.y() );
	cairo_line_to( _drawing, p2.x(), p2.y() );

	setLineStyle( style );
	cairo_stroke( _drawing );
}
/**********************************************/
/**********************************************/
void Cairo::drawPath( const UList<Point>& points, const LineStyle& style )
{
	if ( points.isEmpty() )
		return;

	cairo_move_to( _drawing, points.at(0).x(), points.at(0).y() );
	for ( int it = 1; it < points.size(); ++it )
		cairo_line_to( _drawing, points.at(it).x(), points.at(it).y() );

	setLineStyle( style );
	cairo_stroke( _drawing );
}
void Cairo::drawGappedPath( const UList<Point>& points, double gap, const LineStyle& style )
{
	if ( points.isEmpty() )
		return;
	cairo_move_to( _drawing, points.first().x(), points.first().y() );
	for ( int it = 0; it+1 < points.size(); ++it )
	{
		Point p1 = points.at(it);
		Point p2 = points.at(it+1);
		double x1 = p1.x(), x2 = p2.x();
		double y1 = p1.y(), y2 = p2.y();
		if ( gap > 0 )
		{
			double factor = gap/(pow(pow(x2-x1,2)+pow(y2-y1,2),0.5));
			if ( factor < 0.47 )
			{
				double dx = factor*(x2-x1);
				double dy = factor*(y2-y1);
				x1 += dx, x2 -= dx;
				y1 += dy, y2 -= dy;
			}
			cairo_move_to( _drawing, x1, y1 );
		}
		cairo_line_to( _drawing, x2, y2 );
	}

	setLineStyle( style );
	cairo_stroke( _drawing );
}
/**********************************************/
/**********************************************/
void Cairo::drawClosedPath( const UList<Point>& points, const ShapeStyle& style )
{
	if ( points.isEmpty() )
		return;

	cairo_move_to( _drawing, points.at(0).x(), points.at(0).y() );
	for ( int it = 1; it < points.size(); ++it )
		cairo_line_to( _drawing, points.at(it).x(), points.at(it).y() );
	cairo_close_path( _drawing );

	setFillStyle( style.fill() );
	cairo_fill_preserve( _drawing );

	setLineStyle( style.line() );
	cairo_stroke( _drawing );
}
/**********************************************/
/**********************************************/
void Cairo::drawRect( const Rect& rect, const ShapeStyle& style )
{
	drawRect( rect.point(), rect.size(), style );
}
/**********************************************/
/**********************************************/
void Cairo::drawRect( const Point& pos, const Size& size, const ShapeStyle& style )
{
	Point p2 = {pos.x()+size.width(), pos.y()+size.height()};
	drawRect( pos, p2, style );
}
/**********************************************/
/**********************************************/
void Cairo::drawRect( const Point& p1, const Point& p2, const ShapeStyle& style )
{
	cairo_move_to( _drawing, p1.x(), p1.y() );
	cairo_line_to( _drawing, p2.x(), p1.y() );
	cairo_line_to( _drawing, p2.x(), p2.y() );
	cairo_line_to( _drawing, p1.x(), p2.y() );
	cairo_close_path( _drawing );

	setFillStyle( style.fill() );
	cairo_fill_preserve( _drawing );

	setLineStyle( style.line() );
	cairo_stroke( _drawing );
}
/**********************************************/
/**********************************************/
void Cairo::drawRoundedRect( const Point& p1, const Point& p2, double radius, const ShapeStyle& style )
{
	double x1 = p1.x();
	double x2 = p2.x();
	if ( x2 < x1 )
	{
		double tmp = x2;
		x2 = x1;
		x1 = tmp;
	}
	double y1 = p1.y();
	double y2 = p2.y();
	if ( y2 < y1 )
	{
		double tmp = y2;
		y2 = y1;
		y1 = tmp;
	}

	cairo_new_path( _drawing );
	cairo_arc( _drawing, x2 - radius, y1 + radius, radius, -0.5*M_PI, 0 );
	cairo_arc( _drawing, x2 - radius, y2 - radius, radius, 0, 0.5*M_PI );
	cairo_arc( _drawing, x1 + radius, y2 - radius, radius, 0.5*M_PI, M_PI );
	cairo_arc( _drawing, x1 + radius, y1 + radius, radius, M_PI, 1.5*M_PI );
	cairo_close_path( _drawing );

	setFillStyle( style.fill() );
	cairo_fill_preserve( _drawing );

	setLineStyle( style.line() );
	cairo_stroke( _drawing );
}
/**********************************************/
/**********************************************/
void Cairo::drawArc( const Point& center, double radius, double angle_start, double angle_end, const ShapeStyle& style )
{
	cairo_arc( _drawing, center.x(), center.y(), radius, angle_start * degree, angle_end * degree );

	setFillStyle( style.fill() );
	cairo_fill_preserve( _drawing );

	setLineStyle( style.line() );
	cairo_stroke( _drawing );
}
void Cairo::drawArc( const Point& center, const Size& size, double angle_start, double angle_end, const ShapeStyle& style )
{
	double x_radius = 0.5*size.width();
	double y_radius = 0.5*size.height();
	double scale = y_radius/x_radius;
	cairo_save( _drawing );
	cairo_scale( _drawing, 1, scale );
	cairo_move_to( _drawing, center.x()+x_radius, center.y()/scale );
	cairo_arc( _drawing, center.x(), center.y()/scale, x_radius*degree, angle_start, angle_end*degree );
	cairo_close_path( _drawing );
	cairo_restore( _drawing );

	setFillStyle( style.fill() );
	cairo_fill_preserve( _drawing );

	setLineStyle( style.line() );
	cairo_stroke( _drawing );
}
/**********************************************/
/**********************************************/
void Cairo::drawCercle( const Point& center, double radius, const ShapeStyle& style )
{
	cairo_move_to( _drawing, center.x()+radius, center.y() );
	cairo_arc( _drawing, center.x(), center.y(), radius, 0, 2*M_PI );
	cairo_close_path( _drawing );

	setFillStyle( style.fill() );
	cairo_fill_preserve( _drawing );

	setLineStyle( style.line() );
	cairo_stroke( _drawing );
}
/**********************************************/
/**********************************************/
void Cairo::drawEllipse( const Point& center, double x_radius, double y_radius, const ShapeStyle& style )
{
	double scale = y_radius/x_radius;
	cairo_save( _drawing );
	cairo_scale( _drawing, 1, scale );
	cairo_move_to( _drawing, center.x()+x_radius, center.y()/scale );
	cairo_arc( _drawing, center.x(), center.y()/scale, x_radius, 0, 2*M_PI );
	cairo_close_path( _drawing );
	cairo_restore( _drawing );

	setFillStyle( style.fill() );
	cairo_fill_preserve( _drawing );

	setLineStyle( style.line() );
	cairo_stroke( _drawing );
}
/**********************************************/
/**********************************************/
void Cairo::drawShape( const ShapeStyle::Shape& /*shape*/, const Size& /*size*/, const ShapeStyle& style )
{
	setFillStyle( style.fill() );
	cairo_fill_preserve( _drawing );

	setLineStyle( style.line() );
	cairo_stroke( _drawing );
}
/**********************************************/
/**********************************************/
Rect Cairo::drawText( const ustring& text, double x, double y, const TextStyle& style )
{
	UList<StyledText> styled_texts;
	UList<ustring> lines;
	UList<bool> barres;

	ustring tmp;
	TextStyle sty = style;
	for ( unsigned it = 0; it < text.size(); ++it )
	{
		ustring bal_tmp = text.substr(it,7);
		if ( bal_tmp == "<style=" )
		{
			if ( !tmp.empty() )
				styled_texts.append({tmp,style}), tmp.clear();
			unsigned beg_index = text.find( ">", it );
			ustring style_str = text.substr(it+7,beg_index-it-7);
			sty = style;
			sty.set( style_str );
			it = beg_index;
		}
		else if ( bal_tmp == "</style"  )
		{
			styled_texts.append( {tmp, sty} );
			tmp.clear();
			it += 7;
		}
		else
			tmp += text.at(it);
	}
	if ( !tmp.empty() )
		styled_texts.append({tmp,style}), tmp.clear();

	for ( int it = 0; it < styled_texts.size(); ++it )
	{
		StyledText npart = styled_texts.at(it);
		if ( npart.style().cap() == TextStyle::Cap )
		{
			npart.setText( npart.text().toUpper() );
			styled_texts.replace( it, npart );
		}
		else if ( npart.style().cap() == TextStyle::SmallCap )
		{
			StyledText nnpart;
			TextStyle style = npart.style();
			UList<ustring> dcaps = npart.text().decomposeCap();
			int it2 = it;
			for ( ustring te : dcaps )
			{
				if ( !te.empty() && 'a' <= te.at(0) && te.at(0) <= 'z' )
				{
					nnpart.setText( te.toUpper() );
					nnpart.setStyle( style.capStyle() );
				}
				else
				{
					nnpart.setText( te );
					nnpart.setStyle( style );
				}
				if ( it2 < styled_texts.size() )
					styled_texts.insert( it2, nnpart );
				else
					styled_texts.append( npart );
				++it2;
			}
			it = it2-1;
			styled_texts.removeAt(it2);
		}
	}


	Rect boundingRect;
	double nx = x, ny = y;
	boundingRect.setX( x );
	int it = 0;
	for ( StyledText part : styled_texts )
	{
		part.setPos( Point(nx, ny) );
		part.prepare( this );

//		Label lb( part.text(), {nx,ny}, part.style().anchor() );
//		lb.prepare( this/* _drawing*/, part.style() );
//		// Draw Text
//		drawSimpleText( part.text(), lb.textPos(), part.style() );
//		// Geometry
//		nx += lb.boundingRect().width();

//		if ( boundingRect.x() > lb.boundingRect().x() || it == 0 )
//			boundingRect.setX( lb.boundingRect().x() );
//		if ( boundingRect.y() > lb.boundingRect().y() || it == 0 )
//			boundingRect.setY( lb.boundingRect().y() );
//		if ( boundingRect.height() < lb.boundingRect().height() || it == 0)
//			boundingRect.setHeight( lb.boundingRect().height() );
		// Draw Text
		this->drawSimpleText( part.text(), part.textPos(), part.style() );
		// Geometry
		nx += part.boundingRect().width();
		//
		if ( boundingRect.x() > part.boundingRect().x() || it == 0 )
			boundingRect.setX( part.boundingRect().x() );
		if ( boundingRect.y() > part.boundingRect().y() || it == 0 )
			boundingRect.setY( part.boundingRect().y() );
		if ( boundingRect.height() < part.boundingRect().height() || it == 0)
			boundingRect.setHeight( part.boundingRect().height() );
		//
		++it;
	}
	boundingRect.setWidth( nx-x );
	// Frame for text
//	LineStyle styl;
//	styl.setColor( Color("red6") );
//	styl.setWidth( 0.5 );
//	ShapeStyle sthy;
//	sthy.setLine( styl );
//	drawRect( boundingRect, sthy );
	//

	// Text Shading
	//	cairo_set_source_rgb( _drawing, 0, 0, 0);
	//	cairo_move_to( _drawing, x, y);
	//	cairo_show_text( _drawing, text);
	//	cairo_set_source_rgb( _drawing, 0.5, 0.5, 0.5);
	//	cairo_move_to( _drawing, x, y);
	//	cairo_show_text( _drawing, text);
	// Text with gradient
	//	cairo_pattern_t *pat;
	//	pat = cairo_pattern_create_linear(0, 15, 0, 20);
	//	cairo_pattern_set_extend(pat, CAIRO_EXTEND_REPEAT);
	//	cairo_pattern_add_color_stop_rgb(pat, 0.0, 1, 0.6, 0);
	//	cairo_pattern_add_color_stop_rgb(pat, 0.5, 1, 0.3, 0);
	//	cairo_move_to( _drawing, x, y);
	//	cairo_text_path( _drawing, text );
	//	cairo_set_source( _drawing, pat );
	//	cairo_fill( _drawing );
	// Draw Text
	return boundingRect;
}
void Cairo::drawSimpleText( const ustring& text, const Point& pos, const TextStyle& style )
{
//	if ( style.cap() == TextStyle::SmallCap )
//		setTextStyle()
	setTextStyle( style );
	cairo_move_to( _drawing, pos.x(), pos.y() );
	cairo_show_text( _drawing, text.c_str() );
}
/**********************************************/
/**********************************************/
void Cairo::drawImage( const ustring& imagepath, const Point& pos )
{
	cairo_surface_t *image = cairo_image_surface_create_from_png( imagepath.c_str() );
	cairo_set_source_surface( _drawing, image, pos.x(), pos.y() );
	cairo_paint( _drawing );
}
/**********************************************/
/**********************************************/
void Cairo::drawSymbols( const Symbol& symbol, const Point& pos, const Size& size, const ShapeStyle& style )
{

	Symbol::Symbols type = symbol.type();
	ustring option = symbol.option();
	double covering = 0.01*symbol.covering();// coverage
	double rotation = symbol.rotation(); // angle
	while ( rotation > 360.0 )
		rotation -= 360.0;
	double begin_angle = symbol.beginAngle();
	while ( begin_angle < 0 )
		begin_angle += 360.0;
	while ( begin_angle > 360.0 )
		begin_angle -= 360.0;
	double final_angle = begin_angle+rotation;
	while ( final_angle < 0 )
		final_angle += 360.0;
	while ( final_angle > 360.0 )
		final_angle -= 360.0;
	//	Size size = symbol.size();
	double w = size.width(), h = size.height();

	cairo_save( _drawing );
	if ( type == Symbol::Cercle )
	{
		double x_radius = 0.5*w;
		double y_radius = 0.5*h;
		double scale = y_radius/x_radius;
		cairo_scale( _drawing, 1, scale );
		cairo_arc_negative( _drawing, pos.x(), pos.y()/scale, x_radius, -begin_angle*degree, -final_angle*degree );
		if ( covering > 0 )
		{
			double x_radius = covering*0.5*w;
			double y_radius = covering*0.5*h;
			double scale = y_radius/x_radius;
			cairo_restore( _drawing );
			cairo_save( _drawing );
			cairo_scale( _drawing, 1, scale );
			if ( rotation == 360.0 )
			{
				cairo_close_path( _drawing );
				cairo_new_sub_path( _drawing );
			}
			cairo_arc( _drawing, pos.x(), pos.y()/scale, x_radius, -final_angle*degree, -begin_angle*degree );
		}
	}
	else if ( type == Symbol::Square ||
			  type == Symbol::Triangle ||
			  type == Symbol::Polygon ||
			  type == Symbol::Star
			  )
	{
		int nbIT = 4;
		double diffangle = 0;
		double scale_internal = -1;
		if ( type == Symbol::Square )
		{
			diffangle = 45;
		}
		else if ( type == Symbol::Triangle )
		{
			nbIT = 3;
			diffangle = 90;
		}
		else if ( type == Symbol::Polygon )
		{
			if ( !option.empty() )
				nbIT = option.toInt();
			diffangle = 90;
			if ( nbIT == 4 )
				diffangle = 45;
		}
		else if ( type == Symbol::Star )
		{
			diffangle = 90;
			if ( !option.empty() )
				nbIT = 2*option.toInt();
			else
				nbIT = 8;
			scale_internal = 0.5;
		}
		cairo_new_sub_path( _drawing );
		for ( int it = 0; it < nbIT; ++it )
		{
			double ang = -( (360.0/nbIT)*it + begin_angle + diffangle )*degree;
			double dx  =  0.5*w*cos( ang );
			double dy  =  0.5*h*sin( ang );
			if ( scale_internal >= 0 &&  it % 2 == 1 )
			{
				dx *= scale_internal;
				dy *= scale_internal;
			}
			if ( it == 0 )
				cairo_move_to( _drawing, pos.x()+dx, pos.y()+dy );
			else
				cairo_line_to( _drawing, pos.x()+dx, pos.y()+dy );
		}
		cairo_close_path( _drawing );
		if ( covering > 0  )
		{
			cairo_new_sub_path( _drawing );
			for ( int it = 0; it < nbIT; it++ )
			{
				double ang = -( (360.0/nbIT)*it + begin_angle + diffangle )*degree;
				double dx  =  0.5*w*covering*cos( ang );
				double dy  =  0.5*h*covering*sin( ang );
				if ( scale_internal >= 0 &&  it % 2 == 1 )
				{
					dx *= scale_internal;
					dy *= scale_internal;
				}
				if ( it == 0 )
					cairo_move_to( _drawing, pos.x()+dx, pos.y()+dy );
				else
					cairo_line_to( _drawing, pos.x()+dx, pos.y()+dy );
			}
		}
	}
	else if ( type == Symbol::Plus ||
			  type == Symbol::Minus/* ||
			  type == Symbol::Cross ||
			  type == Symbol::Asterix*/ )
	{
		int nbIT = 4;
		double diffangle = 0;
		if ( type == Symbol::Minus )
		{
			nbIT = 2;
		}
		cairo_new_sub_path( _drawing );
		double angle = begin_angle + diffangle;
		for ( int it = 0; it < nbIT; ++it )
		{
			double ang = -angle*degree;
			double dx  =  0.5*w*cos( ang );
			double dy  =  0.5*h*sin( ang );
			if ( it % 2 == 0 )
			{
				cairo_move_to( _drawing, pos.x()+dx, pos.y()+dy );
				angle += 180.0;
			}
			else
			{
				cairo_line_to( _drawing, pos.x()+dx, pos.y()+dy );
				angle += 360.0/nbIT;
			}
		}
	}
	else if ( type == Symbol::Text )
	{
		setLineStyle( style.line() );
		TextStyle text_style;
		text_style.setSize( h );
		text_style.setColor( style.line().color() );
		setTextStyle( text_style );
		FontMetrics fm;
		fm.init( option, _drawing );
		double dx = -0.5*fm.width();
		double dy = -0.5*fm.yBearing();
		cairo_move_to( _drawing, pos.x()+dx, pos.y()+dy );
		cairo_show_text( _drawing, option.c_str() );
	}
	cairo_close_path( _drawing );
	cairo_restore( _drawing );

	cairo_set_fill_rule( _drawing, CAIRO_FILL_RULE_EVEN_ODD );
	setFillStyle( style.fill() );
	cairo_fill_preserve( _drawing );
	setLineStyle( style.line() );
	cairo_stroke( _drawing );
}
/**********************************************/
/**********************************************/
void Cairo::close()
{
	if ( _format == Cairo::PNG )
		cairo_surface_write_to_png( _surface,
									ustring(path + '/' + filename +".png").c_str() );

	if ( _drawing != nullptr )
		cairo_destroy( _drawing ), _drawing = nullptr;
	if ( _surface != nullptr )
		cairo_surface_destroy(_surface), _surface = nullptr;
}
/**********************************************/
/**********************************************/
