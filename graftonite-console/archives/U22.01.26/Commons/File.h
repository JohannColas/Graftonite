#ifndef FILE_H
#define FILE_H
/**********************************************/
#include "UString.h"
/**********************************************/
/**********************************************/
/**********************************************/
class File
{
public:
	static ustring read( const ustring& path );
	static bool read( const ustring& path, UList<ustring>& contents );
	static void write( const ustring& path, const ustring& contents );
	static void write( const ustring& path, const UList<ustring>& contents );
	static void append( const ustring& path, const ustring& contents );
	static bool exists( const ustring& path );
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // FILE_H
