#include "Styles.h"
/**********************************************/
#include "Cairo.h"
/**********************************************/
/**********************************************/
/**********************************************/
FontMetrics::FontMetrics()
{

}
FontMetrics::FontMetrics( cairo_t* drawing )
{
	init( drawing );
}
FontMetrics::FontMetrics( const ustring& text, cairo_t* drawing )
{
	init( text, drawing );
}
void FontMetrics::init( cairo_t* drawing )
{
	cairo_font_extents_t fe;
	cairo_font_extents( drawing, &fe );
	setAscent( fe.ascent );
	setDescent( fe.descent );
	setHeight( fe.height );
}
void FontMetrics::init( const ustring& text, cairo_t* drawing )
{
	init( drawing );
	cairo_text_extents_t te;
	cairo_text_extents( drawing, text.c_str(), &te );
	setTightHeight( te.height );
	setWidth( te.x_advance );
	setTightWidth( te.width );
	setXBearing( te.x_bearing );
	setYBearing( te.y_bearing );
}
double FontMetrics::ascent() const
{
	return _ascent;
}
void FontMetrics::setAscent( double ascent )
{
	_ascent = ascent;
}
double FontMetrics::descent() const
{
	return _descent;
}
void FontMetrics::setDescent( double descent )
{
	_descent = descent;
}
double FontMetrics::height() const
{
	return _height;
}
void FontMetrics::setHeight( double height )
{
	_height = height;
}
double FontMetrics::tightHeight() const
{
	return _tight_height;
}
void FontMetrics::setTightHeight( double tightHeight )
{
	_tight_height = tightHeight;
}
double FontMetrics::width() const
{
	return _width;
}
void FontMetrics::setWidth( double width )
{
	_width = width;
}
double FontMetrics::tightWidth() const
{
	return _tight_width;
}
void FontMetrics::setTightWidth( double tightWidth )
{
	_tight_width = tightWidth;
}
double FontMetrics::xBearing() const
{
	return _x_bearing;
}
void FontMetrics::setXBearing( double xBearing )
{
	_x_bearing = xBearing;
}
double FontMetrics::yBearing() const
{
	return _y_bearing;
}
void FontMetrics::setYBearing( double yBearing )
{
	_y_bearing = yBearing;
}
/**********************************************/
/**********************************************/
/**********************************************/
TextStyle::TextStyle()
{

}
TextStyle::TextStyle( const ustring& style )
{
	set( style );
}
FontMetrics TextStyle::metrics( const ustring& text, cairo_t* drawing ) const
{
	return FontMetrics( text, drawing );
}
FontMetrics TextStyle::metrics( const ustring& text, Cairo& drawing ) const
{
	drawing.setTextStyle( *this );
	return FontMetrics( text, drawing.drawing() );
}
FontMetrics TextStyle::metrics( const ustring& text, Cairo* drawing ) const
{
	drawing->setTextStyle( *this );
	return FontMetrics( text, drawing->drawing() );
}
void TextStyle::set( const ustring& style )
{
	std::deque<ustring> options;
	ustring tmp = style;
	unsigned long it = tmp.find(';');
	while ( it != std::string::npos )
	{
		options.push_back( tmp.substr(0,it) );
		tmp = tmp.substr( it+1 );
		it = tmp.find(';', 0 );
	}
	if ( !tmp.empty() )
		options.push_back( tmp );
	for ( auto str : options )
	{
		unsigned long it = str.find(':', 0 );

		if ( it == std::string::npos )
		{
			if ( str == "bold" )
				_weight = CAIRO_FONT_WEIGHT_BOLD;
			else if ( str == "notbold" )
				_weight = CAIRO_FONT_WEIGHT_NORMAL;
			else if ( str == "italic" )
				_slant = CAIRO_FONT_SLANT_ITALIC;
			else if ( str == "oblique" )
				_slant = CAIRO_FONT_SLANT_OBLIQUE;
			else if ( str == "notitalic" )
				_slant = CAIRO_FONT_SLANT_NORMAL;
			else if ( str == "underline" )
				_underline = true;
			else if ( str == "nounderline" )
				_underline = false;
			else if ( str == "superscript" )
				_scriptopt = ScriptOpt::SuperScript;
			else if ( str == "subscript" )
				_scriptopt = ScriptOpt::SubScript;
			else if ( str == "normalscript" )
				_scriptopt = ScriptOpt::NormalScript;
			else if ( str == "nocap" )
				_cap = TextStyle::NoCap;
			else if ( str == "cap" )
				_cap = TextStyle::Cap;
			else if ( str == "smallcap" )
				_cap = TextStyle::SmallCap;
			else if ( str == "normal" )
			{
				_weight = CAIRO_FONT_WEIGHT_NORMAL;
				_slant = CAIRO_FONT_SLANT_NORMAL;
				_cap = TextStyle::NoCap;
				_scriptopt = ScriptOpt::NormalScript;
				_underline = false;
			}
		}
		else
		{
			ustring key, value;
			key = str.substr( 0, it );
			value = str.substr( it+1 );
			if ( key == "family" )
				_family = value;
			if ( key == "color" )
				_color = value;
			if ( key == "size" )
				_size = stod(value);
		}
	}
}
ustring TextStyle::family() const
{
	return _family;
}

void TextStyle::setFamily( const ustring& family )
{
	_family = family;
}
cairo_font_slant_t TextStyle::slant() const
{
	return _slant;
}
void TextStyle::setSlant( const cairo_font_slant_t& slant )
{
	_slant = slant;
}
void TextStyle::setSlant( const ustring& slant )
{
	if ( slant == "italic" )
		_slant = CAIRO_FONT_SLANT_ITALIC;
	else if ( slant == "oblique" )
		_slant = CAIRO_FONT_SLANT_OBLIQUE;
	else
		_slant = CAIRO_FONT_SLANT_NORMAL;
}
cairo_font_weight_t TextStyle::weight() const
{
	return _weight;
}
void TextStyle::setWeight( const cairo_font_weight_t& weight )
{
	_weight = weight;
}
void TextStyle::setWeight( const ustring& weight )
{
	if ( weight == "bold" )
		_weight = CAIRO_FONT_WEIGHT_BOLD;
	else
		_weight = CAIRO_FONT_WEIGHT_NORMAL;
}
bool TextStyle::underline() const
{
	return _underline;
}
void TextStyle::setUnderline( bool underline )
{
	_underline = underline;
}
TextStyle::Capitalization TextStyle::cap() const
{
	return _cap;
}
void TextStyle::setCap( const Capitalization& cap )
{
	_cap = cap;
}
void TextStyle::setCap( const ustring& cap )
{
	if ( cap == "nocap" )
		_cap = TextStyle::NoCap;
	else if ( cap == "cap" )
		_cap = TextStyle::Cap;
	else if ( cap == "smallcap" )
		_cap = TextStyle::SmallCap;
}
double TextStyle::size() const
{
	return _size;
}
void TextStyle::setSize( double size )
{
	_size = size;
}
Color TextStyle::color() const
{
	return _color;
}
void TextStyle::setColor( const Color& color )
{
	_color = color;
}
Anchor TextStyle::anchor() const
{
	return _anchor;
}
void TextStyle::setAnchor( const Anchor& anchor )
{
	_anchor = anchor;
}
void TextStyle::setAnchor( const Key::Keys& anchor )
{
	_anchor = anchor;
}
void TextStyle::setAnchor( const ustring& anchor )
{
	_anchor = Key::toKeys(anchor);
}
TextStyle::Align TextStyle::align() const
{
	return _align;
}
void TextStyle::setAlign( const Align& align )
{
	_align = align;
}
void TextStyle::setAlign( const ustring& /*align*/ )
{
}
TextStyle::ScriptOpt TextStyle::scriptOpt() const
{
	return _scriptopt;
}
void TextStyle::setScriptOpt( const ScriptOpt& scriptopt )
{
	_scriptopt = scriptopt;
}
void TextStyle::setScriptOpt( const ustring& /*scriptopt*/ )
{
}
TextStyle TextStyle::capStyle() const
{
	TextStyle cap_style = *this;
	cap_style.setSize( 0.8*this->size()+0.1 );
	return cap_style;
}
/**********************************************/
/**********************************************/
/**********************************************/
StyledText::StyledText()
{

}
StyledText::StyledText( const ustring& text, const TextStyle& style )
{
	_text = text;
	_style = style;
}
ustring StyledText::text() const
{
	return _text;
}
void StyledText::setText( const ustring& text )
{
	_text = text;
}
TextStyle StyledText::style() const
{
	return _style;
}
void StyledText::setStyle( const TextStyle& style )
{
	_style = style;
}
Point StyledText::pos() const
{
	return _pos;
}
Point StyledText::textPos() const
{
	return _text_pos;
}
void StyledText::setPos( const Point& pos )
{
	_pos = pos;
	_text_pos = pos;
}
Anchor StyledText::anchor() const
{
	return _anchor;
}
void StyledText::setAnchor( const Anchor& anchor )
{
	_anchor = anchor;
}
FontMetrics StyledText::metrics() const
{
	return _metrics;
}
Rect StyledText::boundingRect() const
{
	return _boundingRect;
}
void StyledText::prepare( Cairo* drawing )
{
	// Get metrics
	_metrics = style().metrics( text(), drawing );
	//
	// Set X Position
	if ( style().anchor().isHCenter() )
		_text_pos.setX( _pos.x() - 0.5*_metrics.width() );
	else if ( style().anchor().isRight()  )
		_text_pos.setX( _pos.x() - _metrics.width() );
	// Set X & Width BoundingRect
	_boundingRect.setX( _text_pos.x() );
	_boundingRect.setWidth( _metrics.width() );
	//
	// Set Y position
	if ( style().anchor().isTop() )
		_text_pos.setY( _pos.y() + _metrics.ascent() );
	else if ( style().anchor().isVCenter() )
		_text_pos.setY( _pos.y() + _metrics.ascent() - 0.5*_metrics.height() );
	else if ( style().anchor().isBottom() )
		_text_pos.setY( _pos.y() - _metrics.descent() );
	// Set Y & Height BoundingRect
	_boundingRect.setY( _text_pos.y()-_metrics.ascent() );
	_boundingRect.setHeight( _metrics.height() );
	//
}
/**********************************************/
/**********************************************/
/**********************************************/
LineStyle::LineStyle()
{

}
UList<double> LineStyle::dash() const
{
	return _dash;
}
void LineStyle::setDash( const UList<double>& dash )
{
	_dash = dash;
}
double LineStyle::dashOffset() const
{
	return _dash_offset;
}
void LineStyle::setDashOffset( double dashoffset )
{
	_dash_offset = dashoffset;
}
double LineStyle::width() const
{
	return _width;
}
void LineStyle::setWidth( double width )
{
	_width = width;
}
Color LineStyle::color() const
{
	return _color;
}
void LineStyle::setColor( const Color& color )
{
	_color = color;
}
cairo_line_join_t LineStyle::join() const
{
	return _join;
}
void LineStyle::setJoin( const cairo_line_join_t& join )
{
	_join = join;
}
void LineStyle::setJoin( const ustring& join )
{
	Key::Keys key = Key::toKeys(join);
	if ( key == Key::Round )
		_join = CAIRO_LINE_JOIN_ROUND;
	else if ( key == Key::Miter )
		_join = CAIRO_LINE_JOIN_MITER;
	else
		_join = CAIRO_LINE_JOIN_BEVEL;
}
double LineStyle::miterLimit() const
{
	return _miter_limit;
}
void LineStyle::setMiterLimit( double miter_limit )
{
	_miter_limit = miter_limit;
}
cairo_line_cap_t LineStyle::cap() const
{
	return _cap;
}
void LineStyle::setCap( const cairo_line_cap_t& cap )
{
	_cap = cap;
}
void LineStyle::setCap( const ustring& cap )
{
	Key::Keys key = Key::toKeys(cap);
	if ( key == Key::Round )
		_cap = CAIRO_LINE_CAP_ROUND;
	else if ( key == Key::Square )
		_cap = CAIRO_LINE_CAP_SQUARE;
	else
		_cap = CAIRO_LINE_CAP_BUTT;
}
double LineStyle::gap() const
{
	return _gap;
}
void LineStyle::setGap( double gap )
{
	_gap = gap;
}
/**********************************************/
/**********************************************/
/**********************************************/
FillStyle::FillStyle()
{

}
FillStyle::Type FillStyle::type() const
{
	return _type;
}
void FillStyle::setType( const FillStyle::Type& type )
{
	_type = type;
}
Color FillStyle::color() const
{
	return _color;
}
void FillStyle::setColor( const Color& color )
{
	_color = color;
}
cairo_pattern_t* FillStyle::linearPattern() const
{
	return _linear_gradient;
}
void FillStyle::setLinearPattern( cairo_pattern_t* linear_gradient )
{
	_linear_gradient = linear_gradient;
}
cairo_pattern_t* FillStyle::radialPattern() const
{
	return _radial_gradient;
}
void FillStyle::setRadialPattern( cairo_pattern_t* radial_gradient )
{
	_radial_gradient = radial_gradient;
}
/**********************************************/
/**********************************************/
/**********************************************/
ShapeStyle::ShapeStyle()
{

}
FillStyle ShapeStyle::fill() const
{
	return _fill;
}
void ShapeStyle::setFill( const FillStyle& fill )
{
	_fill = fill;
}
LineStyle ShapeStyle::line() const
{
	return _line;
}
void ShapeStyle::setLine( const LineStyle& line )
{
	_line = line;
}
Key::Keys ShapeStyle::anchor() const
{
	return _anchor;
}
void ShapeStyle::setAnchor( const Key::Keys& anchor )
{
	_anchor = anchor;
}
/**********************************************/
/**********************************************/
/**********************************************/
Symbol::Symbol()
{
}
Symbol::Symbol( const Symbol::Symbols& type )
{
	_type = type;
}
Symbol::Symbols Symbol::type() const
{
	return _type;
}
void Symbol::setType( const Symbol::Symbols& type )
{
	_type = type;
}
void Symbol::setType( const ustring& type )
{
	if ( type == "cercle" )
		_type = Symbol::Cercle;
	else if ( type == "halfcercle" )
		_type = Symbol::HalfCercle;
	else if ( type == "square" )
		_type = Symbol::Square;
	else if ( type == "triangle" )
		_type = Symbol::Triangle;
	else if ( type == "polygon" )
		_type = Symbol::Polygon;
	else if ( type == "diamond" )
		_type = Symbol::Polygon;
	else if ( type == "hourglass" )
		_type = Symbol::Hourglass;
	else if ( type == "boomerang" )
		_type = Symbol::Boomerang;
	else if ( type == "star" )
		_type = Symbol::Star;
	else if ( type == "plus" )
		_type = Symbol::Plus;
	else if ( type == "minus" )
		_type = Symbol::Minus;
	else if ( type == "line" )
		_type = Symbol::Line;
	else if ( type == "text" )
		_type = Symbol::Text;
}
ustring Symbol::option() const
{
	return _option;
}
void Symbol::setOption( const ustring& option )
{
	_option = option;
}
Size Symbol::size() const
{
	return _size;
}
void Symbol::setSize( const Size& size )
{
	_size = size;
}
void Symbol::setSize( const ustring& size )
{
	_size = Size(size);
}
Size Symbol::optionSize() const
{
	return _option_size;
}
void Symbol::setOptionSize( const Size& size )
{
	_option_size = size;
}
void Symbol::setOptionSize( const ustring& size )
{
	_option_size = Size(size);
}
double Symbol::beginAngle() const
{
	return _begin_angle;
}
void Symbol::setBeginAngle( double angle )
{
	_begin_angle = angle;
}
double Symbol::endAngle() const
{
	return _end_angle;
}
void Symbol::setEndAngle( double angle )
{
	_end_angle = angle;
}
double Symbol::rotation() const
{
	return _rotation;
}
void Symbol::setRotation( double rotation )
{
	_rotation = rotation;
}
double Symbol::covering() const
{
	return _covering;
}
void Symbol::setCovering( double covering )
{
	_covering = covering;
}
/**********************************************/
/**********************************************/
/**********************************************/
Label::Label()
{
}
Label::Label( const ustring& text, const Point& pos, const Anchor& anchor )
{
	setText(text);
	setPos(pos);
	setAnchor(anchor);
}
ustring Label::text() const
{
	return _text;
}
void Label::setText( const ustring& text )
{
	_text = text;
}
Point Label::pos() const
{
	return _pos;
}
Point Label::textPos() const
{
	return _text_pos;
}
void Label::setPos( const Point& pos )
{
	_pos = pos;
	_text_pos = pos;
}
Anchor Label::anchor() const
{
	return _anchor;
}
void Label::setAnchor( const Anchor& anchor )
{
	_anchor = anchor;
}
FontMetrics Label::metrics() const
{
	return _metrics;
}
Rect Label::boundingRect() const
{
	return _boundingRect;
}
void Label::prepare( cairo_t* drawing, const TextStyle& style )
{
	// Get metrics
	if ( style.cap() == TextStyle::SmallCap )
		_metrics = style.capStyle().metrics( text(), drawing );
	else
		_metrics = style.metrics( text(), drawing );

	//
	// Set X Position
	if ( style.anchor().isHCenter() )
		_text_pos.setX( _pos.x() - 0.5*_metrics.width() );
	else if ( style.anchor().isRight()  )
		_text_pos.setX( _pos.x() - _metrics.width() );
	// Set X & Width BoundingRect
	_boundingRect.setX( _text_pos.x() );
	_boundingRect.setWidth( _metrics.width() );
	//
	// Set Y position
//	_text_pos.setY( _pos.y() );
	if ( style.anchor().isTop() )
		_text_pos.setY( _pos.y() + _metrics.ascent() );
	else if ( style.anchor().isVCenter() )
		_text_pos.setY( _pos.y() + _metrics.ascent() - 0.5*_metrics.height() );
	else if ( style.anchor().isBottom() )
		_text_pos.setY( _pos.y() - _metrics.descent() );
	// Set Y & Height BoundingRect
	_boundingRect.setY( _text_pos.y()-_metrics.ascent() );
	_boundingRect.setHeight( _metrics.height() );
	//
}
void Label::prepare( Cairo* drawing, const TextStyle& style )
{
	// Get metrics
	if ( style.cap() == TextStyle::SmallCap )
		_metrics = style.capStyle().metrics( text(), drawing );
	else
		_metrics = style.metrics( text(), drawing );
	//
	// Set X Position
	if ( style.anchor().isHCenter() )
		_text_pos.setX( _pos.x() - 0.5*_metrics.width() );
	else if ( style.anchor().isRight()  )
		_text_pos.setX( _pos.x() - _metrics.width() );
	// Set X & Width BoundingRect
	_boundingRect.setX( _text_pos.x() );
	_boundingRect.setWidth( _metrics.width() );
	//
	// Set Y position
	if ( style.anchor().isTop() )
		_text_pos.setY( _pos.y() + _metrics.ascent() );
	else if ( style.anchor().isVCenter() )
		_text_pos.setY( _pos.y() + _metrics.ascent() - 0.5*_metrics.height() );
	else if ( style.anchor().isBottom() )
		_text_pos.setY( _pos.y() - _metrics.descent() );
	// Set Y & Height BoundingRect
	_boundingRect.setY( _text_pos.y()-_metrics.ascent() );
	_boundingRect.setHeight( _metrics.height() );
	//
}
void Label::draw( Cairo& drawing, const TextStyle& style )
{
	drawing.setTextStyle( style );
	drawing.drawSimpleText( text(), _text_pos, style );
}
/**********************************************/
/**********************************************/
/**********************************************/

