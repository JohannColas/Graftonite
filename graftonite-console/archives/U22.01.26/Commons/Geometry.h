#ifndef GEOMETRY_H
#define GEOMETRY_H
/**********************************************/
#include <iostream>
/**********************************************/
class ustring;
#include "Key.h"
/**********************************************/
/**********************************************/
/**********************************************/
class Point
{
	double _x = 0;
	double _y = 0;
public:
	Point();
	Point( double x, double y );
	Point( const Point& point );
	double x() const;
	void setX( double x );
	double y() const;
	void setY( double y );
	friend std::ostream& operator<< ( std::ostream& os, const Point& p )
	{
		os << "{" << p.x() << "," << p.y() << "}";
		return os;
	}
	Point& operator= ( const Point& point )
	{
		this->setX( point.x() );
		this->setY( point.y() );
		return *this;
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
class Anchor
{
public:
	Anchor()
	{
	}
	Anchor( const Key::Keys& anchor )
	{
		_anchor = anchor;
	}
	bool isLeft() const
	{
		return ( _anchor == Key::Left ||
				_anchor == Key::BaselineLeft ||
				_anchor == Key::TopLeft ||
				_anchor == Key::BottomLeft );
	}
	bool isRight() const
	{
		return ( _anchor == Key::Right ||
				_anchor == Key::BaselineRight ||
				_anchor == Key::TopRight ||
				_anchor == Key::BottomRight );
	}
	bool isHCenter() const
	{
		return ( _anchor == Key::Center ||
				_anchor == Key::Baseline ||
				_anchor == Key::Top ||
				_anchor == Key::Bottom );
	}
	bool isTop() const
	{
		return ( _anchor == Key::Top ||
				_anchor == Key::TopLeft ||
				_anchor == Key::TopRight );
	}
	bool isBottom() const
	{
		return ( _anchor == Key::Bottom ||
				_anchor == Key::BottomLeft ||
				_anchor == Key::BottomRight );
	}
	bool isVCenter() const
	{
		return ( _anchor == Key::Center ||
				_anchor == Key::Left ||
				_anchor == Key::Right );
	}
	bool isBaseline() const
	{
		return ( _anchor == Key::Baseline ||
				_anchor == Key::BaselineLeft ||
				_anchor == Key::BaselineRight );
	}
	Key::Keys _anchor = Key::Top;
};
/**********************************************/
/**********************************************/
/**********************************************/
class Line
{
public:
	enum Orientation
	{
		Vertical,
		Horizontal
	};
private:
	double _x1 = 0;
	double _y1 = 0;
	double _x2 = 0;
	double _y2 = 0;
public:
	Line()
	{

	}
	Line( const Point& p1, const Point& p2 )
	{
		_x1 = p1.x();
		_y1 = p1.y();
		_x2 = p2.x();
		_y2 = p2.y();
	}
	Line( double x1, double y1, double x2, double y2 )
	{
		_x1 = x1;
		_y1 = y1;
		_x2 = x2;
		_y2 = y2;
	}
	Line( double x, double y, double length, const Line::Orientation& orientation = Line::Horizontal )
	{
		_x1 = x;
		_y1 = y;
		if ( orientation == Line::Horizontal )
		{
			_x2 = x + length;
			_y2 = y;
		}
		else
		{
			_x2 = x;
			_y2 = y + length;
		}
	}
	double x1() const
	{
		return _x1;
	}
	void setX1( double x1 )
	{
		_x1 = x1;
	}
	double y1() const
	{
		return _y1;
	}
	void setY1( double y1 )
	{
		_y1 = y1;
	}
	void setP1( const Point& p1 )
	{
		_x1 = p1.x();
		_y1 = p1.y();
	}
	Point p1() const
	{
		return Point( _x1, _y1 );
	}
	double x2() const
	{
		return _x2;
	}
	void setX2( double x2 )
	{
		_x2 = x2;
	}
	double y2() const
	{
		return _y2;
	}
	void setY2( double y2 )
	{
		_y2 = y2;
	}
	void setP2( const Point& p2 )
{
	_x2 = p2.x();
	_y2 = p2.y();
}
	Point p2() const
	{
		return Point( _x2, _y2 );
	}
	friend std::ostream& operator<< ( std::ostream& os, const Line& l )
	{
		os << "{" << l.x1() << "," << l.y1() << "," << l.x2() << "," << l.y2() << "}";
		return os;
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
class Size
{
	double _width = 0;
	double _height = 0;
public:
	Size();
	Size( double width, double height );
	Size( const ustring& size );
	double width() const;
	void setWidth( double width );
	double height() const;
	void setHeight( double height );
	friend Size operator* ( double d, const Size& size )
	{
		Size new_size;
		new_size.setWidth( d*size.width() );
		new_size.setHeight( d*size.height() );
		return new_size;
	}
	friend Size operator* ( const Size& size, double d )
	{
		return (d*size);
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
class Rect
{
	double _x = 0;
	double _y = 0;
	double _width = 200;
	double _height = 100;
public:
	Rect();
	Rect( double x, double y, double width, double height );
	Rect( const Point& point, const Size& size );
	Rect( const Line& line );
	double x() const;
	void setX( double x );
	double y() const;
	void setY( double y );
	Point point() const;
	void setPoint( double x, double y );
	void setPoint( const Point& point );
	double width() const;
	void setWidth( double width );
	double height() const;
	void setHeight( double height );
	Size size() const;
	void setSize( double width, double height );
	void setSize( const Size& size );
	double yTop() const
	{
		return _y;
	}
	double yCenter() const
	{
		return (_y+0.5*_height);
	}
	double yBottom() const
	{
		return (_y+_height);
	}
	double xLeft() const
	{
		return _x;
	}
	double xCenter() const
	{
		return (_x+0.5*_width);
	}
	double xRight() const
	{
		return (_x+_width);
	}
	Point topLeft() const
	{
		return Point( xLeft(), yTop() );
	}
	Point top() const
	{
		return Point( xCenter(), yTop() );
	}
	Point topRight() const
	{
		return Point( xRight(), yTop() );
	}
	Point centerLeft() const
	{
		return Point( xLeft(), yCenter() );
	}
	Point center() const
	{
		return Point( xCenter(), yCenter() );
	}
	Point centerRight() const
	{
		return Point( xRight(), yCenter() );
	}
	Point bottomLeft() const
	{
		return Point( xLeft(), yBottom() );
	}
	Point bottom() const
	{
		return Point( xCenter(), yBottom() );
	}
	Point bottomRight() const
	{
		return Point( xRight(), yBottom() );
	}
	Line topLine() const
	{
		return Line(topLeft(), topRight());
	}
	Line hCenterLine() const
	{
		return Line(centerLeft(), centerRight());
	}
	Line bottomLine() const
	{
		return Line(bottomLeft(), bottomRight());
	}
	Line leftLine() const
	{
		return Line(topLeft(), bottomLeft());
	}
	Line vCenterLine() const
	{
		return Line(top(), bottom());
	}
	Line rightLine() const
	{
		return Line(topRight(), bottomRight());
	}
	Line line( const Key& pos, bool vcenter = false ) const
	{
		if ( pos == Key::Top )
			return topLine();
		if ( pos == Key::Bottom )
			return bottomLine();
		if ( pos == Key::Left )
			return leftLine();
		if ( pos == Key::Right )
			return rightLine();
		if ( pos == Key::Center && vcenter )
			return vCenterLine();
		return hCenterLine();
	}
	bool contains( const Point& pos )
	{
		if ( xLeft() <= pos.x() &&
			 pos.x() <= xRight() &&
			 yTop() <= pos.y() &&
			 pos.y() <= yBottom() )
			return true;
		return false;
	}
	bool contains( const Line& line )
	{
		if ( this->contains(line.p1()) &&
			 this->contains(line.p2()) )
			return true;
		return false;
	}
	bool contains( const Rect& rect )
	{

		if ( this->contains(rect.topLeft()) &&
			 this->contains(rect.bottomRight()) )
			return true;
		return false;
	}
	void expand( const Point& pos )
	{
		if ( this->contains(pos) )
			return;
		double px = pos.x(), py = pos.y();
		double xl = xLeft(), xr = xRight(),
				yt = yTop(), yb = yBottom();
		if ( px < xl )
		{
			_x = px;
			_width = xr - _x;
		}
		else if ( px > xr )
			_width = px - _x;
		if ( py < yt )
		{
			_y = py;
			_height = yb - _y;
		}
		else if ( py > yb )
			_height = py - _y;
	}
	void expand( const Line& line )
	{
		if ( this->contains(line) )
			return;
		this->expand( line.p1() );
		this->expand( line.p2() );
	}
	void expand( const Rect& rect )
	{
		if ( this->contains(rect) )
			return;
		this->expand( rect.topLeft() );
		this->expand( rect.bottomRight() );
	}
	friend std::ostream& operator<< ( std::ostream& os, const Rect& rect )
	{
		os << "{" << rect.x() << "," << rect.y() << "," << rect.width() << "," << rect.height() << "}";
		return os;
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // GEOMETRY_H
