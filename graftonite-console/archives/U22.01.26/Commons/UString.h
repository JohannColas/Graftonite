#ifndef USTRING_H
#define USTRING_H
/**********************************************/
#include <string>
#include <iostream>
/**********************************************/
#include "UList.h"
#include <cairo.h>
/**********************************************/
class Color;
class Point;
class Size;
class Rect;
/**********************************************/
/**********************************************/
/**********************************************/
class ustring
        : public std::string
{
public:
	ustring();
	ustring( const double d );
	ustring( const char c );
	ustring( const int d );
	ustring( const unsigned d );
	ustring( const long d );
	ustring( const char* c_str );
	ustring( const std::string& str );
	ustring( const ustring& str );
	/**********************************************/
	bool contains( const std::string& str ) const;
	bool contains( const char* c_str ) const;
	ustring remove( const ustring& str ) const;
	/**********************************************/
	/* Conversions */
	bool isInteger() const
	{
		for ( char c : *this )
			if ( c < '0' || '9' < c )
				return false;
		return true;
	}
	bool isDouble() const
	{
		for ( char c : *this )
			if ( (c < '0' || '9' < c) && c != '.' )
				return false;
		return this->contains(".");
	}
	double toDouble() const
	{
		double d = 0;
		if ( !empty() )
			d = stod(*this);
		return d;
	}
	std::deque<double> toDoubleList() const
	{
		UList<ustring> lst = this->split(",");
		std::deque<double> d_lst;
		for ( const ustring& el : lst )
			d_lst.push_back( el.toDouble() );
		return d_lst;
	}
	bool toBool() const
	{
		if ( this->isInteger() )
			return stoi(*this);
		else if ( *this == "true" || *this == "t" )
			return true;
		return false;
	}
	int toInt() const
	{
		return stoi(*this);
	}
	unsigned int toUInt() const
	{
		return stoi(*this);
	}
	long toLong() const
	{
		return stol(*this);
	}
	unsigned long toULong() const
	{
		return stoul(*this);
	}
	char toChar() const
	{
		return this->at(0);
	}
	unsigned char toUChar() const
	{
		return 0;
	}
	Color toColor() const;
	ustring toUpper() const
	{
	    ustring str;
	    for ( char c : *this )
	    {
		if ( 'a' <= c &&  c <= 'z' )
		    str += (c+'A'-'a');
		else
		    str += c;
	    }
	    return str;
	}
	ustring toFirstUpper() const
	{
	    ustring str;
	    for ( unsigned it = 0; it < this->size(); ++it)
	    {
		char c = this->at(it);
		if ( it == 0 && 'a' <= c &&  c <= 'z' )
		    str += (c+'A'-'a');
		else
		    str += c;
	    }
	    return str;
	}
	ustring toLower() const
	{
	    ustring str;
	    for ( char c : *this )
	    {
		if ( 'A' <= c &&  c <= 'Z' )
		    str += (c+'a'-'A');
		else
		    str += c;
	    }
	    return str;
	}
	/**********************************************/
	UList<ustring> split( const ustring& separator, bool keepEmptyParts = false ) const;
	UList<ustring> decomposeCap() const;
	/**********************************************/
	// Assignment Operators Overloading
	ustring& operator= ( const ustring& str )
	{
		(*this).clear();
		(*this) += str;
		return *this;
	}
	ustring& operator= ( const std::string& str )
	{
		(*this).clear();
		(*this) += str;
		return *this;
	}
	bool operator== ( const UList<ustring>& list ) const
	{
		for ( const ustring& el : list )
			if ( *this == el || *this == ("-"+el) )
				return true;
		return false;
	}
	ustring& operator= ( const char* s )
	{
		(*this).clear();
		(*this) += s;
		return *this;
	}
	ustring& operator= (char c)
	{
		(*this).clear();
		(*this) += c;
		return *this;
	}
	friend std::ostream& operator<<( std::ostream& os, const ustring& str );
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // USTRING_H
