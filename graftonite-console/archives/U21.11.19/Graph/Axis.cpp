#include "Axis.h"
/**********************************************/
#include <cmath>
/**********************************************/
#include "Commons/XML.h"
#include "Graph.h"
#include "Layer.h"
/**********************************************/
/**********************************************/
Axis::Axis( Graph* parent ) : Element(parent)
{
	_type = Element::AXIS;
}
/**********************************************/
/**********************************************/
ustring Axis::draw()
{
	if ( !_parent ) return "";
	ustring contents;
	Layer* layer = _parent->layer( get("layer") );
	if ( layer )
	{
//		_boundingRect = {layer->get("x").toDouble(), layer->get("y").toDouble(), layer->get("width").toDouble(), layer->get("height").toDouble()};
	}
	CalculateScaleCoef();
	// Open Axis Group
	contents += "\t"+XML::beginGroup( "g", {{"id", get("name")}} )+"\n";
	//
	double line_pos = 0;
	string line_dir = "";
	UList<double> line_limits; line_limits.append({0,0});
	//
	double ticks_pos = 0;
	string ticks_dir = "";
	UList<double> ticks_limits;
	double minorticks_pos = 0;
	UList<double> minorticks_limits;
	//
	UList<ustring> labels;
	double labels_pos = 0;
	UList<string> labels_anchor;
	//
	UList<double> title_pos = {0,0};

	UList<string> title_anchor;
	string title_transfo = "";
	// -----------------------
	// -----------------------
	// Draw X Axis
	// -----------------------
	ustring type = get("type");
	if ( type.empty() ) type = "X";
	ustring axispos = get("pos");
	ustring tickpos = get("ticks-pos");
	ustring labelspos = get("lb-pos");
	ustring titlepos = get("tl-pos");
	ustring title = get("title");
	double ticksize = get("ticks-size").toDouble();
	if ( ticksize == 0 ) ticksize = 10;
	double minorticksize = get("mn-tk-size").toDouble();
	if ( minorticksize == 0 ) minorticksize = 5;
	UList<double> labelsshift = {5,0};// = get("lb-shift").toDouble();
	double titleshift = get("tl-shift").toDouble();
	if ( type == "X" )
	{
		// Open Axis Line Group
		contents += "\t\t<g id=\"axis-line\" stroke=\"black\" stroke-linecap=\"round\" stroke-width=\"3px\">\n";
		// ---------------
		// Draw X Axis Line
		line_pos = _boundingRect.at(1)+_boundingRect.at(3);
		if ( axispos == "center" )
			line_pos = _boundingRect.at(1)+0.5*_boundingRect.at(3);
		else if ( axispos == "bottom" )
			line_pos = _boundingRect.at(1);
		contents += "\t\t\t" + XML::drawLine( GetGraphCoord(_min), line_pos, "H", GetGraphCoord(_max) ) + "\n";
		// ---------------
		// Draw X Axis Ticks (major & minor)
		ticks_pos = line_pos;
		minorticks_pos = line_pos;
		if ( tickpos == "top" ||
			 (tickpos == "outside" && axispos == "top") ||
			 (tickpos == "inside" && axispos == "bottom") )
		{
			ticks_pos = line_pos - ticksize;
			minorticks_pos = line_pos - minorticksize;
		}
		else if ( tickpos == "center" )
		{
			ticks_pos = line_pos - 0.5*ticksize;
			minorticks_pos = line_pos - 0.5*minorticksize;
		}
		// calculate ticks_limits
		CalculateTicksPosition( ticks_limits, minorticks_limits, labels );
		for ( double xpos : ticks_limits )
		{
			contents += "\t\t\t" + XML::drawLine( xpos, ticks_pos, "v", ticksize) + "\n";
		}
		for ( double xpos : minorticks_limits )
		{
			contents += "\t\t\t" + XML::drawLine( xpos, minorticks_pos, "v", minorticksize) + "\n";
		}
		// Close X Axis Line Group
		contents += "\t\t</g>\n";
		// ---------------
		// Draw X Axis Labels
		labels_pos = ticks_pos + ticksize + labelsshift.at(1);
		labels_anchor = {"middle", "text-before-edge"};
		if (labelspos == "top" ||
			 (labelspos == "outside" && axispos == "top") ||
			 (labelspos == "inside" && axispos == "bottom") )
		{
			labels_pos = ticks_pos - labelsshift.at(1);
//			labels_anchor = {labels_anchor.at(0), "text-after-edge"};
			labels_anchor.replace(0, "text-after-edge");
		}
		else if ( labelspos == "center" )
		{
			labels_pos = line_pos + labelsshift.at(1);
//			labels_anchor = {labels_anchor.at(0), "middle"};
			labels_anchor.replace(0, "middle");
		}
		// ---------------
		// Open X Axis Labels Group
		contents += "\t\t<g id=\"axis-ticklabels\" text-anchor=\""+labels_anchor.at(0)+"\" dominant-baseline=\""+labels_anchor.at(1)+"\" font-size=\"20pt\" transform=\"translate(0,"+ustring(labels_pos)+")\">\n";
		// ---------------
		// Draw each label
		for ( unsigned it = 0; it < labels.size(); ++it )
		{
			contents += "\t\t\t"+XML::drawText( "x", ticks_limits.at(it), labels.at(it) )+"\n";
		}
		// Close X Axis Labels Group
		contents += "\t\t</g>\n";
		// ---------------
		// Draw X Axis Title
		title_pos = { _boundingRect.at(0) + 0.5*_boundingRect.at(2) + titleshift, labels_pos + titleshift };
		if ( ticks_pos + ticksize > labels_pos )
//			title_pos = {title_pos.at(0), ticks_pos + ticksize + titleshift};
			title_pos.replace(1, ticks_pos + ticksize + titleshift);
		title_anchor = {"middle", "text-before-edge"};
		if (titlepos == "top" ||
			 (titlepos == "outside" && axispos == "top") ||
			 (titlepos == "inside" && axispos == "bottom") )
		{
			title_pos.replace( 1, labels_pos - titleshift );
			if ( ticks_pos < labels_pos )
				title_pos.replace( 1, ticks_pos - titleshift );
//			title_anchor = {title_anchor.at(0), "text-after-edge"};
			title_anchor.replace( 1, "text-after-edge" );
		}
		contents += "\t\t<g id=\"axis-ticklabels\" text-anchor=\""+title_anchor.at(0)+"\" dominant-baseline=\""+title_anchor.at(1)+"\" font-size=\"24pt\">\n";
		contents += "\t\t\t"+XML::drawText( title_pos.at(0),title_pos.at(1), title, title_transfo )+"\n";
		contents += "\t\t</g>\n";
		// Draw X Axis END
	}
	// -----------------------
	// -----------------------
	// Draw Y Axis
	// -----------------------
	else if ( type == "Y" )
	{
		// Open Axis Line Group
		contents += "\t\t<g id=\"axis-line\" stroke=\"black\" stroke-linecap=\"round\" stroke-width=\"3px\">\n";
		// ---------------
		// Draw Y Axis Line
		line_pos = _boundingRect.at(0);
		if ( axispos == "center" )
			line_pos = _boundingRect.at(0)+0.5*_boundingRect.at(2);// _boundingRect.center().x();
		else if ( axispos == "right" )
			line_pos = _boundingRect.at(0)+_boundingRect.at(2);//_boundingRect.right();
		contents += "\t\t\t" + XML::drawLine( line_pos, GetGraphCoord(_min), "V", GetGraphCoord(_max)) + "\n";
		// ---------------
		// Draw Y Axis Ticks (major & minor)
		ticks_dir = "h";
		ticks_pos = line_pos - ticksize;
		minorticks_pos = line_pos - minorticksize;
		if ( tickpos == "right" ||
			 (tickpos == "outside" && axispos == "right") ||
			 (tickpos == "inside" && axispos == "left") )
		{
			ticks_pos = line_pos;
			minorticks_pos = line_pos;
		}
		else if ( tickpos == "center" )
		{
			ticks_pos = line_pos - 0.5*ticksize;
			ticks_pos = line_pos - 0.5*minorticksize;
		}
		// calculate ticks_limits
		CalculateTicksPosition( ticks_limits, minorticks_limits, labels	, true );
		for ( double ypos : ticks_limits )
		{
			contents += "\t\t\t" + XML::drawLine( ticks_pos, ypos, "h", ticksize) + "\n";
		}
		for ( double ypos : minorticks_limits )
		{
			contents += "\t\t\t" + XML::drawLine( minorticks_pos, ypos, "h", minorticksize) + "\n";
		}
		// Close Y Axis Line Group
		contents += "\t\t</g>\n";
		// ---------------
		// Draw Y Axis Labels
		labels_pos = ticks_pos - labelsshift.at(0);
		labels_anchor = {"end", "middle"};
		if ( labelspos == "right" ||
			 (labelspos == "outside" && axispos == "right") ||
			 (labelspos == "inside" && axispos == "left") )
		{
			labels_pos = ticks_pos + ticksize + labelsshift.at(0);
//			labels_anchor = {labels_anchor.at(0), "middle"};
			labels_anchor.replace(0, "middle");
		}
		else if ( labelspos == "center" )
		{
			labels_pos = line_pos + labelsshift.at(0);
//			labels_anchor = {labels_anchor.at(0), "middle"};
			labels_anchor.replace(0, "middle");
		}
		// ---------------
		// Open Y Axis Labels Group
		contents += "\t\t<g id=\"axis-ticklabels\" text-anchor=\""+labels_anchor.at(0)+"\" dominant-baseline=\""+labels_anchor.at(1)+"\" font-size=\"20pt\" transform=\"translate("+ustring(labels_pos)+",0)\">\n";
		// ---------------
		// Draw each label
		for ( unsigned it = 0; it < labels.size(); ++it )
		{
			contents += "\t\t\t"+XML::drawText( "y", ticks_limits.at(it), labels.at(it) )+"\n";
		}
		// Close Y Axis Labels Group
		contents += "\t\t</g>\n";
		// ---------------
		// Draw Y Axis Title
		title_pos = {labels_pos - titleshift, _boundingRect.at(1)+0.5*_boundingRect.at(3) + titleshift};
		if ( ticks_pos < labels_pos )
//			title_pos = {title_pos.at(0), ticks_pos - titleshift};
			title_pos.replace(0, ticks_pos - titleshift);
//		title_pos.replace( 0, ticks_pos - titleshift );
		title_anchor = {"middle", "text-after-edge"};
		if ( titlepos == "right" ||
			 (titlepos == "outside" && axispos == "right") ||
			 (titlepos == "inside" && axispos == "left") )
		{
//			title_pos = {title_pos.at(0), labels_pos - titleshift};
			title_pos.replace(0, labels_pos - titleshift);
//			title_pos.replace( 0, labels_pos + titleshift.x() );
			if ( ticks_pos + ticksize > labels_pos )
//				title_pos = {title_pos.at(0),  ticks_pos + ticksize + titleshift};
				title_pos.replace(0, ticks_pos + ticksize + titleshift);
//				title_pos.replace( 0, ticks_pos + ticksize + titleshift.x() );
//			title_anchor.replace( 1, "text-before-edge");
//			title_anchor = {title_anchor.at(0), "text-after-edge"};
			title_anchor.replace( 0, "text-after-edge" );
		}
		title_transfo = "transform=\"rotate(-90 "+ustring(title_pos.at(0))+" "+ustring(title_pos.at(1))+")\"";
		contents += "\t\t<g id=\"axis-title\" text-anchor=\""+title_anchor.at(0)+"\" dominant-baseline=\""+title_anchor.at(1)+"\" font-size=\"24pt\">\n";
		contents += "\t\t\t"+XML::drawText( title_pos.at(0),title_pos.at(1), title, title_transfo )+"\n";
		contents += "\t\t</g>\n";
		// Draw Y Axis END
	}
	// -----------------------
	// Close Axis Group
	contents += "\t</g>\n";
	//
	return contents;
}
/*------------------------------*/
/*------------------------------*/
void Axis::CalculateTicksPosition( UList<double>& tickspos, UList<double>& minortickspos, UList<ustring>& labels, bool isYaxis )
{
	double tickinterval = get( "tick-int" ).toDouble();
	double minortickinterval = get( "mn-tick-int" ).toDouble();
	char axis_dir = 1; if ( _min > _max ) axis_dir = -1;

	int quot = (int)(_min/tickinterval);
	if ( _min > 0 && _max > _min ) ++quot;
	else if ( _min < 0 && _max < _min ) --quot;

	int minorquot = (int)(_min/minortickinterval);
	if ( _min > 0 && _max > _min ) ++minorquot;
	else if ( _min < 0 && _max < _min ) --minorquot;

	double posp = quot*tickinterval;
	double pos = GetGraphCoord( posp );

	int it = 0;
	double minorposp = minorquot*minortickinterval;
	double minorpos = GetGraphCoord( minorposp );
	while (( (minorpos+1 < pos && !isYaxis) ||
			(minorpos-1 > pos && isYaxis) )&&it<100 )
	{
		minortickspos.append( minorpos );
		minorposp += axis_dir*minortickinterval;
		minorpos = GetGraphCoord( minorposp );
		++it;
	}
	it = 0;
	while (( (pos-1 < _boundingRect.at(0)+_boundingRect.at(2) && !isYaxis) ||
			(pos+1 > _boundingRect.at(1) && isYaxis) )&&it<100)
	{
		tickspos.append( pos );
		// -----------
		// Get Labels
		labels.append( ustring(posp) );
		// -----------
		int jt = 0;
		double minorposp = _minortickinterval;
		double minorpos = GetGraphCoord( posp + axis_dir*minorposp );
		while (( minorposp < _tickinterval &&
				((minorpos-1 < _boundingRect.at(0)+_boundingRect.at(2) && !isYaxis) ||
				 (minorpos+1 > _boundingRect.at(1) && isYaxis) ) )&&jt<100)
		{
			minortickspos.append( minorpos );
			minorposp += _minortickinterval;
			minorpos = GetGraphCoord( posp + axis_dir*minorposp );
			++jt;
		}
		posp += axis_dir*_tickinterval;
		pos = GetGraphCoord( posp );
		++it;
	}
}
/*------------------------------*/
/*------------------------------*/
double Axis::round( double value )
{
	double val = value;
	return val;
}
/*------------------------------*/
/*------------------------------*/
ustring Axis::toString( double value )
{
	double tickinterval = get( "tick-int" ).toDouble();
	ustring num = ustring(tickinterval);
//	int it = num.length() - num.find_last_of(".") - 1;
	ustring temp = ustring(value);//,'f',it);
//	if ( temp.contains(".") )
//		while ( temp.at(temp.size()-1) == '0' )
//			temp = temp.erase( temp.length()-1, 1);
//	if ( temp.at(temp.size()-1) == '.' )
//		temp = temp.erase( temp.length()-1, 1);
	if ( temp == "-0" )
		temp = "0";
	return temp;
}
/*------------------------------*/
/*------------------------------*/
void Axis::CalculateScaleCoef()
{
	ustring type = get( Keys::Axis_Type );
	ustring scale = get( "scale" );
	double scaleOpt = get( "scaleOpt" ).toDouble();
	double po = _boundingRect.at(0);
	double pm = _boundingRect.at(0)+_boundingRect.at(2);
	if ( type == "Y" )
	{
		po = _boundingRect.at(1)+_boundingRect.at(3);
		pm = _boundingRect.at(1);
	}
	if ( scale == "log10" )
	{
		if ( _min <= 0 )
			_min = 0.1;
		if ( _max <= 0 )
			_max = 1;
		_scaleA = ( pm - po ) / ( log10(_max) - log10(_min) );
		_scaleB = po - _scaleA*log10(_min);
	}
	else if ( scale == "log" )
	{
		if ( _min <= 0 )
			_min = 0.1;
		if ( _max <= 0 )
			_max = 1;
		_scaleA = ( pm - po ) / ( log(_max) - log(_min) );
		_scaleB = po - _scaleA*log(_min);
	}
	else if ( scale == "logx" )
	{
		if ( _min <= 0 )
			_min = 0.1;
		if ( _max <= 0 )
			_max = 1;
		_scaleA = log(scaleOpt) * ( pm - po ) / ( log(_max) - log(_min) );
		_scaleB = po - _scaleA*log(_min)/log(scaleOpt);
	}
	else if ( scale == "reciprocal" )
	{
		if ( _min == 0 )
			_min = 1;
		if ( _max == 0 )
			_max = 10;
		_scaleA = ( pm - po ) / ( 1/_max - 1/_min );
		_scaleB = po - _scaleA/_min;
	}
	else if ( scale == "offsetreciprocal" )
	{
		if ( _min+scaleOpt == 0 )
			_min = 1-scaleOpt;
		if ( _max+scaleOpt == 0 )
			_max = 10-scaleOpt;
		_scaleA = ( pm - po ) / ( 1/(_max+scaleOpt) - 1/(_min+scaleOpt) );
		_scaleB = po - _scaleA/(_min+scaleOpt);
	}
	else
	{
		_scaleA = ( pm - po ) / ( _max - _min );
		_scaleB = po - _scaleA*_min;
	}
}
/*------------------------------*/
/*------------------------------*/
double Axis::GetGraphCoord( double coord )
{
	ustring scale = get( "scale" );
	double scaleOpt = get( "scaleOpt" ).toDouble();
	if ( scale == "log10" )
		return _scaleA*log10(coord)+_scaleB;
	else if ( scale == "log" )
		return _scaleA*log(coord)+_scaleB;
	else if ( scale == "logx" )
		return _scaleA*log(coord)/log(scaleOpt)+_scaleB;
	else if ( scale == "reciprocal" )
		return _scaleA*(1/coord)+_scaleB;
	else if ( scale == "offsetreciprocal" )
		return _scaleA*(1/(coord+scaleOpt))+_scaleB;
	return _scaleA*coord+_scaleB;
}
/**********************************************/
/**********************************************/
