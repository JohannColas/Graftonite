#include "Element.h"
/**********************************************/
#include <iostream>
/**********************************************/
/**********************************************/
Element::~Element()
{

}
/**********************************************/
Element::Element( Graph* parent )
{
	_parent = parent;
}
/**********************************************/
/**********************************************/
//bool Element::contain( const ustring& str1, const ustring& str2 ) const
//{
//	return (str1.find_first_of(str2) != string::npos);
//}
/**********************************************/
/**********************************************/
bool Element::hasKey( const ustring& key ) const
{
	if ( _settings.count( key ) > 0 )
		return true;
	return false;
}
/**********************************************/
/**********************************************/
ustring Element::get( const ustring& key ) const
{
	if ( hasKey(key) )
		return _settings.at( key );
	return "";
}
/**********************************************/
/**********************************************/
void Element::set( const ustring& key, const ustring& value )
{
	ustring key2;
	if ( key == "-n" || key.contains("name") )
		key2 = Keys::Name;
	else if ( (key == "-l" || key.contains("layer")) && _type == Element::AXIS )
		key2 = Keys::Layer;
	else if ( key == "-a" || key == "-axis" || key == "axis" )
		key2 = Keys::Axis;
	else if ( key == "-ty" || key == "-type" || key == "type" )
		key2 = Keys::Type;
	else if ( (key == "-a1" || key.contains("axis1")) && _type == Element::PLOT )
		key2 = "axis1";
	else if ( (key == "-a2" || key.contains("axis2")) && _type == Element::PLOT )
		key2 = "axis2";
	else if ( (key == "-a3" || key.contains("axis3")) && _type == Element::PLOT )
		key2 = "axis3";
	// Geometry Settings
	else if ( key == "-x" ) key2 = Keys::X;
	else if ( key == "-y" ) key2 = Keys::Y;
	else if ( key == "-w" || key == "-width" || key == "width" ) key2 = Keys::Width;
	else if ( key == "-h" || key == "-height" || key == "height" ) key2 = Keys::Height;
	else if ( key == "-p" || key == "-pos" || key == "pos" )
	{
		key2 = "pos";
//		string x, y;
//		set( "-x", x );
//		set( "-y", y );
//		return;
	}
	else if ( key == "-s" || key == "-size" )
	{
		key2 = "size";
//		string width, height;
//		set( "-w", width );
//		set( "-h", height );
		return;
	}
	//
	else if ( key == "-bk" || key == "-background" ) key2 = "background";
	else if ( key == "-bd" || key == "-border" ) key2 = "borders";
	else if ( key == "-tx" || key == "-text" ) key2 = "text";
	else if ( key == "-ft" || key == "-font" ) key2 = "font";
	else if ( key == "-hide" ) key2 = "hide";
	// Axis Settings
	else if ( key == "-ty" || key == "-type" ) key2 = Keys::Axis_Type;
	else if ( key == "-min" || key == "min" ) key2 = "min";
	else if ( key == "-max" || key == "max" ) key2 = "max";
	else if ( key == "-sc" || key == "-scale" || key == "scale" ) key2 = "scale";
	else if ( key == "-lk" || key == "-link" ) key2 = "link";
	// Line Settings
	else if ( key == "-ln" || key == "-line"  ) key2 = "line";
	else if ( key == "-lncol" ) key2 = "line-color";
	else if ( key == "-lnw" ) key2 = "line-width";
	else if ( key == "-lnjn" ) key2 = "line-join";
	else if ( key == "-lncp" ) key2 = "line-cap";
	else if ( key == "-lnshp" ) key2 = "line-shape";
	// Ticks Settings
	else if ( key == "-tkpos" ) key2 = "ticks-pos";
	else if ( key == "-tkinc" ) key2 = "ticks-inc";
	else if ( key == "-tknb" ) key2 = "ticks-numb";
	else if ( key == "-tksz" ) key2 = "ticks-size";
	// Minor Ticks Settings
	else if ( key == "-mtkinc" ) key2 = "minor-ticks-inc";
	else if ( key == "-mtknb" ) key2 = "minor-ticks-numb";
	else if ( key == "-mtksz" ) key2 = "minor-ticks-size";
	// Labels Settings
	else if ( key == "-lbpos" ) key2 = "labels-pos";
	else if ( key == "-lbalign" ) key2 = "labels-align";
	else if ( key == "-lbfmt" ) key2 = "labels-format";
	else if ( key == "-lbft" ) key2 = "labels-font";
	// Title Settings
	else if ( key == "-tltx" ) key2 = "title-text";
	else if ( key == "-tlpos" ) key2 = "title-pos";
	else if ( key == "-tlalign" ) key2 = "title-align";
	else if ( key == "-tlfmt" ) key2 = "title-format";
	else if ( key == "-tlft" ) key2 = "title-font";
	//
	if ( hasKey(key2) )
		_settings.at(key2) = value;
	else
		_settings.emplace( key2, value );
}
/**********************************************/
/**********************************************/
void Element::set( const pair<ustring,ustring>& pair )
{
	set( pair.first, pair.second );
}
/**********************************************/
/**********************************************/
void Element::set( const map<ustring,ustring>& args )
{
	for ( auto const& pair: args )
		set( pair );
}
/**********************************************/
/**********************************************/
void Element::set( const UList<ustring>& args )
{
	for ( unsigned it = 0; it < args.size(); it+=2 )
		if ( it+1 < args.size() )
			set( args.at(it), args.at(it+1) );
}
/**********************************************/
/**********************************************/
void Element::unset( const ustring& key )
{
	_settings.erase( key );
}
/**********************************************/
/**********************************************/
void Element::unset( const UList<ustring>& args )
{
	for ( unsigned it = 0; it < args.size(); ++it )
		unset( args.at(it) );
}
/**********************************************/
/**********************************************/
void Element::remove( const ustring& key )
{
	if ( hasKey(key) )
		_settings.erase(key);
}
/**********************************************/
/**********************************************/
void Element::clear()
{
	ustring name;
	if ( hasKey("name") ) name = get("name");
	_settings.clear();
	set( "name", name );
}
/**********************************************/
/**********************************************/
void Element::show()
{
	if ( type() == Element::NOTYPE ) return;
	ustring str_typ = "GRAPH";
	if ( type() == Element::LAYER )
	{
		str_typ = "LAYER";
	}
	else if ( type() == Element::AXIS )
		str_typ = "AXIS";
	else if ( type() == Element::PLOT )
		str_typ = "PLOT";
	else if ( type() == Element::LEGEND )
		str_typ = "LEGEND";
	else if ( type() == Element::TITLE )
	{
		str_typ = "TITLE";
	}
	ustring name = "<NoName>";
	if ( hasKey("name") ) name = get("name");
	cout << "    " << str_typ << ": " << name << "\n";
	cout << "        " << "KEYS    " << "|" << "  VALUES" << "\n";
	cout << "      " << "----------|------------" << "\n";
	for ( auto const &pair: _settings )
	{
		ustring key;
		unsigned long size = pair.first.length();
		if ( size > 6 ) size = 6;
		for ( unsigned long it = 0; it < size; ++it )
			key += pair.first.at(it);
		for ( unsigned long it = key.length(); it < 6; ++it )
			key += ' ';
		cout << "        " << key << "  |  " << pair.second << "\n";
	}
	cout << "\n";
}
/**********************************************/
/**********************************************/
ustring Element::draw()
{
	return "";
}
/**********************************************/
/**********************************************/
