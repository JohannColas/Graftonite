#include "Graph.h"
/**********************************************/
#include <iostream>
#include <fstream>
#include "Terminal.h"
#include "Commons/XML.h"
using namespace std;
/**********************************************/
/**********************************************/
Graph::~Graph()
{
	_layers.deleteAll();
	_frames.deleteAll();
	_axes.deleteAll();
	_plots.deleteAll();
	_legends.deleteAll();
	_titles.deleteAll();
//	while ( !_layers.empty() )
//	{
//		delete _layers.back();
//		_layers.pop_back();
//	}
//	while ( !_frames.empty() )
//	{
//		delete _frames.back();
//		_frames.pop_back();
//	}
//	while ( !_axes.isEmpty() )
//	{
//		delete _axes.back();
//		_axes.pop_back();
//	}
//	while ( !_plots.empty() )
//	{
//		delete _plots.back();
//		_plots.pop_back();
//	}
//	while ( !_legends.empty() )
//	{
//		delete _legends.back();
//		_legends.pop_back();
//	}
//	while ( !_titles.empty() )
//	{
//		delete _titles.back();
//		_titles.pop_back();
//	}
}
/**********************************************/
/**********************************************/
Graph::Graph() : Element()
{
	_type = Element::GRAPH;
	Element::set( "width", "1600");
	Element::set( "height", "1000");

	Layer* layer = new Layer(this);
	layer->set( "name", "layer1" );
	layer->set( "width", "1920" );
	layer->set( "height", "1440" );
	_layers.append( layer );
	Layer* layer1 = new Layer(this);
	layer1->set( "name", "Thi is a layer" );
	layer1->set( "width", "42" );
	layer1->set( "height", "1440" );
	_layers.append( layer1 );
	Axis* axis = new Axis(this);
	axis->set( "name", "AxisX" );
	axis->set( "min", "0" );
	axis->set( "max", "1" );
	axis->set( "type", "X" );
	_axes.append( axis );
	Axis* axisy = new Axis(this);
	axisy->set( "name", "AxisY" );
	axisy->set( "min", "0" );
	axisy->set( "max", "1" );
	axisy->set( "type", "Y" );
	axisy->set( "pos", "center" );
	_axes.append( axisy );
}
/**********************************************/
/**********************************************/
Layer* Graph::layer( const ustring& name )
{
	for ( unsigned it = 0; it < _layers.size(); ++it )
		if ( _layers.at(it)->get("name") == name || ustring(it) == name  )
			return _layers.at(it);
	return 0;
}
/**********************************************/
/**********************************************/
UList<Layer*> Graph::layers()
{
	return _layers;
}
/**********************************************/
/**********************************************/
Frame* Graph::frame( const ustring& name )
{
	for ( unsigned it = 0; it < _frames.size(); ++it )
		if ( _frames.at(it)->get("name") == name || ustring(it) == name  )
			return _frames.at(it);
	return 0;
}
/**********************************************/
/**********************************************/
UList<Frame*> Graph::frames()
{
	return _frames;
}
/**********************************************/
/**********************************************/
Axis* Graph::axis( const ustring& name )
{
	for ( unsigned it = 0; it < _axes.size(); ++it )
		if ( _axes.at(it)->get("name") == name || ustring(it) == name  )
			return _axes.at(it);
	return 0;
}
/**********************************************/
/**********************************************/
UList<Axis*> Graph::axes()
{
	return _axes;
}
/**********************************************/
/**********************************************/
Plot* Graph::plot( const ustring& name )
{
	for ( unsigned it = 0; it < _plots.size(); ++it )
		if ( _plots.at(it)->get("name") == name || ustring(it) == name  )
			return _plots.at(it);
	return 0;
}
/**********************************************/
/**********************************************/
UList<Plot*> Graph::plots()
{
	return _plots;
}
/**********************************************/
/**********************************************/
Legend* Graph::legend( const ustring& name )
{
	for ( unsigned it = 0; it < _legends.size(); ++it )
		if ( _legends.at(it)->get("name") == name || ustring(it) == name  )
			return _legends.at(it);
	return 0;
}
/**********************************************/
/**********************************************/
UList<Legend*> Graph::legends()
{
	return _legends;
}
/**********************************************/
/**********************************************/
Title* Graph::title( const ustring& name )
{
	for ( unsigned it = 0; it < _titles.size(); ++it )
		if ( _titles.at(it)->get("name") == name || ustring(it) == name  )
			return _titles.at(it);
	return 0;
}
/**********************************************/
/**********************************************/
UList<Title*> Graph::titles()
{
	return _titles;
}
/**********************************************/
/**********************************************/
void Graph::newElement( const Element::Type& type, const ustring& name )
{
	cout << endl;
	if ( type == Element::GRAPH || type == Element::NOTYPE )
	{
		if ( name.empty() )
		{
		}
	}
	if ( type == Element::LAYER || type == Element::NOTYPE )
	{
		if ( name.empty() )
		{
		}
		Layer* layer = new Layer(this);
		layer->set( "name", name );
		_layers.append( layer );
	}
	if ( type == Element::FRAME || type == Element::NOTYPE )
	{
		if ( name.empty() )
		{
		}
		Frame* frame = new Frame(this);
		frame->set( "name", name );
		_frames.append( frame );
	}
	if ( type == Element::AXIS || type == Element::NOTYPE )
	{
		if ( name.empty() )
		{
		}
		Axis* axis = new Axis(this);
		axis->set( "name", name );
		_axes.append( axis );
	}
	if ( type == Element::PLOT || type == Element::NOTYPE )
	{
		if ( name.empty() )
		{
		}
		Plot* plot = new Plot();
		plot->set( "name", name );
		_plots.append( plot );
	}
	if ( type == Element::LEGEND || type == Element::NOTYPE )
	{
		if ( name.empty() )
		{
		}
		Legend* legend = new Legend();
		legend->set( "name", name );
		_legends.append( legend );
	}
	if ( type == Element::TITLE || type == Element::NOTYPE )
	{
		if ( name.empty() )
		{
		}
		Title* title = new Title();
		title->set( "name", name );
		_titles.append( title );
	}
}
/**********************************************/
/**********************************************/
void Graph::removeElement( const Element::Type& type, const ustring& name )
{
	cout << endl;
	if ( type == Element::LAYER || type == Element::NOTYPE )
	{
		if ( name.empty() )
		{
			if ( Terminal::confirmationDialog("  Are you sure to delete all layers ? (Y/y): ") )
				_layers.clear();
		}
		else
		{
			UList<Layer*> temp;
			for (unsigned long it = 0; it < layers().size(); ++it)
			{
				ustring num = to_string(it);
				if ( _layers.at(it)->get("name") != name && num != name )
				{
					temp.append( _layers.at(it) );
				}
				else
					cout << " Element removed : " << it << " - " << _layers.at(it)->get("name") << endl;
			}
			_layers.clear();
			_layers = temp;
			//			std::cout << '\n';
			//			for (unsigned long it = 0; it < layers().size(); ++it)
			//			{
			//				string num = to_string(it);
			//				if ( _layers.at(it).get("name") == name || num == name )
			//				{
			//					cout << " Element to remove : " << it << " " << _layers.at(it).get("name") << endl;
			////					delete &_layers.at(it);
			//					_layers.erase(layers().begin()+it);
			//				}
			//			}
		}
	}
	if ( type == Element::FRAME || type == Element::NOTYPE )
	{
		if ( name.empty() )
		{
			if ( Terminal::confirmationDialog("  Are you sure to delete all frames ? (Y/y): ") )
				_frames.clear();
		}
		else
		{
			UList<Frame*> temp;
			for (unsigned long it = 0; it < _frames.size(); ++it)
			{
				ustring num = to_string(it);
				if ( _frames.at(it)->get("name") != name && num != name )
				{
					temp.append( _frames.at(it) );
				}
				else
					cout << " Element removed : " << it << " - " << _frames.at(it)->get("name") << endl;
			}
			_frames.clear();
			_frames = temp;
			//			int i = 0;
			//			for (auto it = frames().begin(); it < frames().end(); ++it)
			//			{
			//				string num = to_string(i);
			//				if ( _frames.at(i).get("name") == name || num == name )
			//					_frames.erase(it);
			//				++i;
			//			}
		}
	}
	if ( type == Element::AXIS || type == Element::NOTYPE )
	{
		if ( name.empty() )
		{
			if ( Terminal::confirmationDialog("  Are you sure to delete all axes ? (Y/y): ") )
				_axes.clear();
		}
		else
		{
			//			int i = 0;
			//			for (auto it = axes().begin(); it < axes().end(); ++it)
			//			{
			//				string num = to_string(i);
			//				if ( _axes.at(i).get("name") == name || num == name )
			//					_axes.erase(it);
			//				++i;
			//			}
			UList<Axis*> temp;
			for (unsigned long it = 0; it < _axes.size(); ++it)
			{
				ustring num = to_string(it);
				if ( _axes.at(it)->get("name") != name && num != name )
				{
					temp.append( _axes.at(it) );
				}
				else
					cout << " Element removed : " << it << " - " << _axes.at(it)->get("name") << endl;
			}
			_axes.clear();
			_axes = temp;
		}
	}
	if ( type == Element::PLOT || type == Element::NOTYPE )
	{
		if ( name.empty() )
		{
			if ( Terminal::confirmationDialog("  Are you sure to delete all plots ? (Y/y): ") )
				_plots.clear();
		}
		else
		{
			//			int i = 0;
			//			for (auto it = plots().begin(); it < plots().end(); ++it)
			//			{
			//				string num = to_string(i);
			//				if ( _plots.at(i).get("name") == name || num == name )
			//					_plots.erase(it);
			//				++i;
			//			}
			UList<Plot*> temp;
			for (unsigned long it = 0; it < _plots.size(); ++it)
			{
				ustring num = to_string(it);
				if ( _plots.at(it)->get("name") != name && num != name )
				{
					temp.append( _plots.at(it) );
				}
				else
					cout << " Element removed : " << it << " - " << _plots.at(it)->get("name") << endl;
			}
			_plots.clear();
			_plots = temp;
		}
	}
	if ( type == Element::LEGEND || type == Element::NOTYPE )
	{
		if ( name.empty() )
		{
			if ( Terminal::confirmationDialog("  Are you sure to delete all legends ? (Y/y): ") )
				_legends.clear();
		}
		else
		{
			//			int i = 0;
			//			for (auto it = legends().begin(); it < legends().end(); ++it)
			//			{
			//				string num = to_string(i);
			//				if ( _legends.at(i).get("name") == name || num == name )
			//					_legends.erase(it);
			//				++i;
			//			}
			UList<Legend*> temp;
			for (unsigned long it = 0; it < _legends.size(); ++it)
			{
				ustring num = to_string(it);
				if ( _legends.at(it)->get("name") != name && num != name )
				{
					temp.append( _legends.at(it) );
				}
				else
					cout << " Element removed : " << it << " - " << _legends.at(it)->get("name") << endl;
			}
			_legends.clear();
			_legends = temp;
		}
	}
	if ( type == Element::TITLE || type == Element::NOTYPE )
	{
		if ( name.empty() )
		{
			if ( Terminal::confirmationDialog("  Are you sure to delete all titles ? (Y/y): ") )
				_titles.clear();
		}
		else
		{
			//			int i = 0;
			//			for (auto it = titles().begin(); it < titles().end(); ++it)
			//			{
			//				string num = to_string(i);
			//				if ( _titles.at(i).get("name") == name || num == name )
			//					_titles.erase(it);
			//				++i;
			//			}
			UList<Title*> temp;
			for (unsigned long it = 0; it < _titles.size(); ++it)
			{
				ustring num = to_string(it);
				if ( _titles.at(it)->get("name") != name && num != name )
				{
					temp.append( _titles.at(it) );
				}
				else
					cout << " Element removed : " << it << " - " << _titles.at(it)->get("name") << endl;
			}
			_titles.clear();
			_titles = temp;
		}
	}
}
/**********************************************/
/**********************************************/
void Graph::setCurrent( UList<ustring>& args )
{
	UList<ustring> ret;
	for ( auto el : args )
		ret.append( el );

	ustring eltype;
	if ( args.isEmpty() ) return;
	eltype = ret.takeFirst();
//	eltype = args.first();
//	args.popFirst();
	ustring name;
	if ( args.isEmpty() ) return;
	name = ret.takeFirst();
//	name = args.first();
//	args.popFirst();

	Element* el = 0;
	if ( eltype.contains("graph") ) el = this;
	else if ( eltype.contains("layer") ) el = this->layer(name);
	else if ( eltype.contains("frame") ) el = this->frame(name);
	else if ( eltype.contains("axis") ) el = this->axis(name);
	else if ( eltype.contains("plot") ) el = this->plot(name);
	else if ( eltype.contains("legend") ) el = this->legend(name);
	else if ( eltype.contains("title") ) el = this->title(name);
	if ( el ) _currentEl = el;
	cout << endl << "Current element : " << _currentEl->get(Keys::Name) << endl;
}
/**********************************************/
/**********************************************/
void Graph::set( UList<ustring>& args )
{
	ustring eltype;
	if ( args.isEmpty() ) return;
	eltype = args.first();

	Element* el = 0;
	if ( !eltype.contains("graph") &&
		 !eltype.contains("layer") &&
		 !eltype.contains("frame") &&
		 !eltype.contains("axis") &&
		 !eltype.contains("plot") &&
		 !eltype.contains("legend") &&
		 !eltype.contains("title") )
	{

		el = _currentEl;
	}
	else
	{
		args.popFirst();
		ustring name;
		if ( args.isEmpty() ) return;
		name = args.first();
		args.popFirst();
		if ( args.isEmpty() ) return;
		if ( eltype.contains("graph") ) el = this;
		else if ( eltype.contains("layer") ) el = this->layer(name);
		else if ( eltype.contains("frame") ) el = this->frame(name);
		else if ( eltype.contains("axis") ) el = this->axis(name);
		else if ( eltype.contains("plot") ) el = this->plot(name);
		else if ( eltype.contains("legend") ) el = this->legend(name);
		else if ( eltype.contains("title") ) el = this->title(name);
	}

	if ( el ) el->set(args);
}
/**********************************************/
/**********************************************/
void Graph::unset( UList<ustring>& args )
{
	ustring arg1;
	if ( args.isEmpty() ) return;
	arg1 = args.first();
	args.popFirst();
	if ( arg1.contains("graph") )
	{
		this->unset( args );
	}
	else
	{
		ustring name;
		if ( args.isEmpty() ) return;
		name = args.first();
		args.popFirst();
		if ( args.isEmpty() ) return;
		if ( arg1.contains("layer") )
		{
			for ( unsigned it = 0; it < _layers.size(); ++it )
			{
				string num = to_string(it);
				if ( _layers.at(it)->get("name") == name || num == name  )
				{
					_layers.at(it)->unset( args );
				}
			}
		}
		else if ( arg1.contains("frame") )
		{
			for ( unsigned it = 0; it < _frames.size(); ++it )
			{
				string num = to_string(it);
				if ( _frames.at(it)->get("name") == name || num == name  )
				{
					_frames.at(it)->unset( args );
				}
			}
		}
		else if ( arg1.contains("axis") )
		{
			for ( unsigned it = 0; it < _axes.size(); ++it )
			{
				string num = to_string(it);
				if ( _axes.at(it)->get("name") == name || num == name  )
				{
					_axes.at(it)->unset( args );
				}
			}
		}
		else if ( arg1.contains("plot") )
		{
			for ( unsigned it = 0; it < _plots.size(); ++it )
			{
				string num = to_string(it);
				if ( _plots.at(it)->get("name") == name || num == name  )
				{
					_plots.at(it)->unset( args );
				}
			}
		}
		else if ( arg1.contains("legend") )
		{
			for ( unsigned it = 0; it < _legends.size(); ++it )
			{
				string num = to_string(it);
				if ( _legends.at(it)->get("name") == name || num == name  )
				{
					_legends.at(it)->unset( args );
				}
			}
		}
		else if ( arg1.contains("title") )
		{
			for ( unsigned it = 0; it < _titles.size(); ++it )
			{
				string num = to_string(it);
				if ( _titles.at(it)->get("name") == name || num == name  )
				{
					_titles.at(it)->unset( args );
				}
			}
		}
	}
}
/**********************************************/
/**********************************************/
void Graph::showAll( const Element::Type& type, const ustring& name )
{
	cout << endl;
	if ( type == Element::GRAPH || type == Element::NOTYPE )
	{
		if ( name.empty() )
		{
			cout << "    " << "GRAPH" << endl;
		}
		else
		{
			this->show();
		}
	}
	if ( type == Element::LAYER || type == Element::NOTYPE )
	{
		if ( name.empty() )
		{
			cout << "    LAYERS  --------" << endl;
			if ( layers().isEmpty() )
				cout << "      " << "NO LAYERS" << endl;
			for (unsigned long it = 0; it < layers().size(); ++it)
			{
				string num = to_string(it);
				for (unsigned long jt = num.size(); jt < 4; ++jt)
					num = " " + num;
				cout << "      "
					 << "LAYER   | "
					 << num << "  |  "
					 << layers().at(it)->get("name") << endl;
			}
		}
		else
		{
			//for (unsigned long it = 0; it < layers().size(); ++it)
			//{
			//	string num = to_string(it);
			//	if ( layers().at(it)->get("name") == name || num == name )
			//		layers().at(it)->show();
			//}
			layer(name)->show();
		}
	}
	if ( type == Element::FRAME || type == Element::NOTYPE )
	{
		if ( name.empty() )
		{
			cout << "    FRAMES  --------" << endl;
			if ( _frames.isEmpty() )
				cout << "      " << "NO FRAMES" << endl;
			for (unsigned long it = 0; it < frames().size(); ++it)
			{
				string num = to_string(it);
				for (unsigned long jt = num.size(); jt < 4; ++jt)
					num = " " + num;
				cout << "      "
					 << "FRAME   | "
					 << num << "  |  "
					 << frames().at(it)->get("name") << endl;
			}
		}
		else
			frame(name)->show();
	}
	if ( type == Element::AXIS || type == Element::NOTYPE )
	{
		if ( name.empty() )
		{
			cout << "    AXES    --------" << endl;
			if ( axes().isEmpty() )
				cout << "      " << "NO AXES" << endl;
			for (unsigned long it = 0; it < axes().size(); ++it)
			{
				string num = to_string(it);
				for (unsigned long jt = num.size(); jt < 4; ++jt)
					num = " " + num;
				cout << "      "
					 << "AXIS    | "
					 << num << "  |  "
					 << axes().at(it)->get("name") << endl;
			}
		}
		else
			axis(name)->show();
	}
	if ( type == Element::PLOT || type == Element::NOTYPE )
	{
		if ( name.empty() )
		{
			cout << "    PLOTS   --------" << endl;
			if ( plots().isEmpty() )
				cout << "      " << "NO PLOTS" << endl;
			for (unsigned long it = 0; it < plots().size(); ++it)
			{
				string num = to_string(it);
				for (unsigned long jt = num.size(); jt < 4; ++jt)
					num = " " + num;
				cout << "      "
					 << "PLOTS   | "
					 << num << "  |  "
					 << plots().at(it)->get("name") << endl;
			}
		}
		else
			plot(name)->show();
	}
	if ( type == Element::LEGEND || type == Element::NOTYPE )
	{
		if ( name.empty() )
		{
			cout << "    LEGENDS --------" << endl;
			if ( legends().isEmpty() )
				cout << "      " << "NO LEGENDS" << endl;
			for (unsigned long it = 0; it < legends().size(); ++it)
			{
				string num = to_string(it);
				for (unsigned long jt = num.size(); jt < 4; ++jt)
					num = " " + num;
				cout << "      "
					 << "LEGEND  | "
					 << num << "  |  "
					 << legends().at(it)->get("name") << endl;
			}
		}
		else
			legend(name)->show();
	}
	if ( type == Element::TITLE || type == Element::NOTYPE )
	{
		if ( name.empty() )
		{
			cout << "    TITLES  --------" << endl;
			if ( titles().isEmpty() )
				cout << "      " << "NO TITLE" << endl;
			for (unsigned long it = 0; it < titles().size(); ++it)
			{
				string num = to_string(it);
				for (unsigned long jt = num.size(); jt < 4; ++jt)
					num = " " + num;
				cout << "      "
					 << "TITLE   | "
					 << num << "  |  "
					 << titles().at(it)->get("name") << endl;
			}
		}
		else
			title(name)->show();
	}
}
/**********************************************/
/**********************************************/
void Graph::showCurrent()
{
	showAll( _currentEl->type(), _currentEl->get(Keys::Name) );
}
/**********************************************/
/**********************************************/
ustring Graph::draw()
{
	ustring contents ="<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
	_settings.emplace( "xmlns", "http://www.w3.org/2000/svg");
	_settings.emplace( "xmlns:xlink", "http://www.w3.org/1999/xlink" );
	_settings.emplace( "version", "1.0" );
	contents += XML::beginGroup( "svg", _settings ) + "\n";
	for ( Frame* frame : _frames )
	{
		contents += frame->draw() + "\n";
	}
	for ( Axis* axis : _axes )
	{
		contents += axis->draw() + "\n";
	}
	for ( Plot* plot : _plots )
	{
		contents += plot->draw() + "\n";
	}
	for ( Legend* legend : _legends )
	{
		contents += legend->draw() + "\n";
	}
	for ( Title* title : _titles )
	{
		contents += title->draw() + "\n";
	}
	contents += XML::endGroup( "svg" );
	_settings.erase( "xmlns" );
	_settings.erase( "xmlns:xlink" );
	_settings.erase( "version" );
	return contents;
}
/**********************************************/
/**********************************************/
void Graph::save( const ustring& path )
{
	ustring workingPath = get(Keys::WorkingPath);
	if ( workingPath.empty() )
		workingPath = "/home/colas/Bureau";

//	ustring workingPath = get( "workingpath" );

	ustring fullpath = path;
	if ( path.front() != '/' && !workingPath.empty() )
		fullpath = workingPath + '/' + path;
	if ( fullpath.substr(fullpath.length()-4,4) != ".svg" )
		fullpath += ".svg";
	cout << endl << "Graph is saved at : " << fullpath << endl;
	std::ofstream out(fullpath);
	out << this->draw();
	out.close();
}
/**********************************************/
/**********************************************/
