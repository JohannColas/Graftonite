#ifndef GRAPH_H
#define GRAPH_H
/**********************************************/
//#include <deque>
//using namespace std;
/**********************************************/
//#include "Commons/UString.h"
/**********************************************/
#include "Element.h"
#include "Layer.h"
#include "Frame.h"
#include "Axis.h"
#include "Plot.h"
#include "Legend.h"
#include "Title.h"
/**********************************************/
/**********************************************/
/**********************************************/
class Graph : public Element
{
//	ustring _workingPath = "/home/colas/Bureau";
	/**********************************************/
	Element* _currentEl = 0;
	/**********************************************/
	UList<Layer*> _layers;
	UList<Frame*> _frames;
	UList<Axis*> _axes;
	UList<Plot*> _plots;
	UList<Legend*> _legends;
	UList<Title*> _titles;
	/**********************************************/
public:
	~Graph();
	Graph();
	/**********************************************/
	Layer* layer( const ustring& name );
	UList<Layer*> layers();
	Frame* frame( const ustring& name );
	UList<Frame*> frames();
	Axis* axis( const ustring& name );
	UList<Axis*> axes();
	Plot* plot( const ustring& name );
	UList<Plot*> plots();
	Legend* legend( const ustring& name );
	UList<Legend*> legends();
	Title* title( const ustring& name );
	UList<Title*> titles();
	/**********************************************/
	void newElement( const Element::Type& type = Element::NOTYPE, const ustring& name = ustring() );
	void removeElement( const Element::Type& type = Element::NOTYPE, const ustring& name = ustring() );
	void setCurrent( UList<ustring>& args );
	void set( UList<ustring>& args );
	void unset( UList<ustring>& args );
	void showAll( const Element::Type& type = Element::NOTYPE, const ustring& name = ustring() );
	void showCurrent();
	ustring draw() override;
	void save( const ustring& path );
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // GRAPH_H
