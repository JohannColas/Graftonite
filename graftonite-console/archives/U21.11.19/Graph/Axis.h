#ifndef AXIS_H
#define AXIS_H
/**********************************************/
#include "Element.h"
#include <vector>
/**********************************************/
/**********************************************/
/**********************************************/
class Axis : public Element
{
	std::vector<double> _boundingRect = {100, 100, 1200, 800};
	double _min = 0;
	double _max = 1;
	double _scaleA = 0.1;
	double _scaleB = 0;
	double _tickinterval = 0.1;
	double _minortickinterval = 0.05;
public:
	Axis( Graph* parent = 0 );
	/**********************************************/
	ustring draw() override;
	/**********************************************/
	ustring general( const ustring& key )
	{
		return get( Keys::General + Keys::Separator + key );
	}
	void setGeneral( const ustring& key, const ustring& value )
	{
		set( Keys::General + Keys::Separator + key, value );
	}
	ustring line( const ustring& key )
	{
		return get( Keys::Line + Keys::Separator + key );
	}
	void setLine( const ustring& key, const ustring& value )
	{
		set( Keys::Line + Keys::Separator + key, value );
	}
	ustring ticks( const ustring& key )
	{
		return get( Keys::Ticks + Keys::Separator + key );
	}
	void setTicks( const ustring& key, const ustring& value )
	{
		set( Keys::Ticks + Keys::Separator + key, value );
	}
	ustring minorTicks( const ustring& key )
	{
		return get( Keys::Minor + Keys::Separator + Keys::Ticks + Keys::Separator + key );
	}
	void setMinorTicks( const ustring& key, const ustring& value )
	{
		set( Keys::Minor + Keys::Separator + Keys::Ticks + Keys::Separator + key, value );
	}
	ustring labels( const ustring& key )
	{
		return get( Keys::Labels + Keys::Separator + key );
	}
	void setLabels( const ustring& key, const ustring& value )
	{
		set( Keys::Labels + Keys::Separator + key, value );
	}
	ustring title( const ustring& key )
	{
		return get( Keys::Title + Keys::Separator + key );
	}
	void setTitle( const ustring& key, const ustring& value )
	{
		set( Keys::Title + Keys::Separator + key, value );
	}
	ustring grids( const ustring& key )
	{
		return get( Keys::Grids + Keys::Separator + key );
	}
	void setGrids( const ustring& key, const ustring& value )
	{
		set( Keys::Grids + Keys::Separator + key, value );
	}
	ustring minorGrids( const ustring& key )
	{
		return get( Keys::Minor + Keys::Separator + Keys::Grids + Keys::Separator + key );
	}
	void setMinorGrids( const ustring& key, const ustring& value )
	{
		set( Keys::Minor + Keys::Separator + Keys::Grids + Keys::Separator + key, value );
	}
	/**********************************************/
	void CalculateTicksPosition( UList<double>& tickspos, UList<double>& minortickspos, UList<ustring>& labels, bool isYaxis = false );
	double round( double value );
	ustring toString( double value );
	void CalculateScaleCoef();
	double GetGraphCoord( double coord );
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // AXIS_H
