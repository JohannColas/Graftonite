#ifndef ELEMENT_H
#define ELEMENT_H
/**********************************************/
#include <map>
#include <deque>
//#include <vector>
#include <string>
using namespace std;
/**********************************************/
//#include "Commons/XML.h"
#include "Commons/UString.h"
#include "Commons/UList.h"
/**********************************************/
class Graph;
/**********************************************/
namespace Keys
{
	static inline ustring Name = "name";
	static inline ustring Layer = "layer";
	static inline ustring Axis = "axis";
	static inline ustring Axis1 = "axis1";
	static inline ustring Axis2 = "axis2";
	static inline ustring Axis3 = "axis3";
	static inline ustring X = "x";
	static inline ustring Y = "y";
	static inline ustring Width = "width";
	static inline ustring Height = "height";
	static inline ustring Pos = "pos";
	static inline ustring Axis_Pos = "axis-pos";
	static inline ustring Axis_Type = "axis-type";
	static inline ustring Axis_Scale = "axis-scale";
	static inline ustring Axis_Scale_Opt = "axis-scale-opt";
	static inline ustring Ticks_Pos = "ticks-pos";
	static inline ustring Ticks_Increm = "ticks-increment";
	static inline ustring Ticks_Numbers = "ticks-numbers";
	static inline ustring Ticks_Size = "ticks-size";
	static inline ustring Minor_Ticks_Pos = "minor-ticks-pos";
	static inline ustring Minor_Ticks_Increm = "minor-ticks-increment";
	static inline ustring Minor_Ticks_Numbers = "minor-ticks-numbers";
	static inline ustring Minor_Ticks_Size = "minor-ticks-size";
	static inline ustring Labels_Pos = "labels-pos";
	static inline ustring Labels_Shift = "labels-shift";
	static inline ustring Labels_Anchors = "labels-anchors";
	static inline ustring Labels_Transform = "labels-transform";
	static inline ustring Title_Pos = "title-pos";
	static inline ustring Title_Shift = "title-shift";
	static inline ustring Title_Anchors = "title-anchors";
	static inline ustring Title_Tranform = "title-transform";

	static inline ustring WorkingPath = "workingpath";
	static inline ustring General = "general";
	static inline ustring Scale = "scale";
	static inline ustring Option = "option";
	static inline ustring Type = "type";
	static inline ustring Line = "line";
	static inline ustring Ticks = "ticks";
	static inline ustring Increment = "increment";
	static inline ustring Numbers = "numbers";
	static inline ustring Minor = "minor";
	static inline ustring Size = "size";
	static inline ustring Position = "position";
	static inline ustring Labels = "labels";
	static inline ustring Format = "format";
	static inline ustring Font = "font";
	static inline ustring Anchors = "anchors";
	static inline ustring Shift = "shift";
	static inline ustring Transform = "transform";
	static inline ustring Title = "title";
	static inline ustring Text = "text";
	static inline ustring Separator = ".";
	static inline ustring Grids = "grids";

//	static inline ustring  = "";

//	enum Keys : unsigned char {
//		unknown = 0,
////		Name = 1,
//		orange = 2,
//		white = 4,
//		gold = 255,
//		black = 0x10
//	};

//	string getColor( char key )
//	{
//		switch ( key )
//		{
////		case Keys::Name: return "name";
//		case Keys::orange: return "orange";
//		case Keys::white: return "white";
//		case Keys::gold: return "gold";
//		case Keys::black: return "black";
//		default: return "[UNKNOWN]";
//		}
//	}
//	char getColor( string key )
//	{
//		/*if ( key == "name" ) return Keys::red;
//		else */if ( key == "orange" ) return Keys::orange;
//		else if ( key == "white" ) return Keys::white;
//		else if ( key == "gold" ) return Keys::gold;
//		else if ( key == "black" ) return Keys::black;
//		else return Keys::unknown;
//	}
}
//using namespace Keys;
/**********************************************/
/**********************************************/
/**********************************************/
class Element
{
public:
	enum Type : char
	{
		NOTYPE,
		GRAPH,
		LAYER,
		FRAME,
		AXIS,
		PLOT,
		LEGEND,
		TITLE
	};
	/**********************************************/
	map<ustring,ustring> _settings;
	Element::Type _type = Element::NOTYPE;
	Graph* _parent = 0;
	/**********************************************/
public:
	virtual ~Element();
	Element( Graph* parent = 0 );
	/**********************************************/
	virtual Element::Type type() const { return _type; }
	virtual void setType( const Element::Type& type ) { _type = type; }
	/**********************************************/
	//	bool contain( const ustring& str1, const string& str2 ) const;
	bool hasKey( const ustring& key ) const;
	ustring get( const ustring& key ) const;
	void set( const ustring& key, const ustring& value );
	void set( const pair<ustring,ustring>& pair );
	void set( const map<ustring,ustring>& args );
	void set( const UList<ustring>& args );
	void unset( const ustring& key );
	void unset( const UList<ustring>& args );
	void remove( const ustring& key );
	void clear();
	/**********************************************/
	virtual void show();
	virtual ustring draw();
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ELEMENT_H
