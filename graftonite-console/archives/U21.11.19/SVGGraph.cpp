#include "SVGGraph.h"
/**********************************************/
#include <unistd.h>
/**********************************************/
#include "Commons/File.h"
/**********************************************/
/**********************************************/
/**********************************************/
void SVGGraph::decomposeCmdLine( const ustring& cmdline, UList<ustring>& args )
{
	args.clear();
	ustring key, arg;
	bool argMode = true;
	char limChar = 0;
	unsigned long len = cmdline.length();
	vector<ustring> cmdlist;
	ustring elm;
	for ( unsigned long it = 0; it < len; ++it)
	{
		char c = cmdline.at(it);
		if ( c == ' ' && !limChar )
		{
			if ( argMode )
			{
				if ( !elm.empty() )
					args.append( elm );
				elm.clear();
			}
			argMode = false;
			continue;
		}
		else if ( c == '\"' || c == '\'' || c == '<' || c == '>' )
		{
			if ( c == limChar || (limChar == '<' && c == '>') )
			{
				limChar = 0;
				if ( !elm.empty() )
					args.append( elm );
				elm.clear();
				continue;
			}
			else if ( !limChar )
			{
				limChar = c;
				continue;
			}
		}
		else
		{
			argMode = true;
		}
		if ( argMode || limChar )
		{
			elm += c;
		}
	}
	if ( !elm.empty() )
		args.append(elm);
}
/**********************************************/
/**********************************************/
void SVGGraph::analyseCommand( const ustring& cmdline )
{
	if ( cmdline.empty() ) return;
	ustring cmd;
	ustring primaryArg;
	//	map<string,string> args;
	UList<ustring> args;
	decomposeCmdLine( cmdline, args );
	if ( args.isEmpty() ) return;
	cmd = args.first();
	args.popFirst();

	if ( cmd.contains("new") || cmd == "n" )
		newel(args);
	else if ( cmd.contains("reorder") || cmd == "r" )
		reorder(args);
	else if ( cmd.contains("insert") || cmd == "i" )
		insert(args);
	else if ( cmd.contains("delete") || cmd == "d" )
		deleteel(args);
	else if ( cmd.contains("current") || cmd == "c" )
		setcurrent(args);
	else if ( cmd.contains("set") || cmd == "s" )
		set(args);
	else if ( cmd.contains("unset") || cmd == "u" )
		unset(args);
	else if ( cmd.contains("open") || cmd == "o" )
		open(args);
	else if ( cmd.contains("load") || cmd == "l" )
		load(args);
	else if ( cmd.contains("save") || cmd == "e" )
		save(args);
	else if ( cmd.contains("export") || cmd == "x" )
		exportg(args);
	else if ( cmd.contains("show") || cmd == "w" )
		show(args);
	else if ( cmd.contains("about") || cmd == "a" )
		about();
	else if ( cmd.contains("version") || cmd == "v" )
		version();
	else if ( cmd.contains("help") || cmd == "h" )
		help(args);
	else if ( cmd.contains("clear") || cmd == "k" )
		SVGGraph::clear();
	else if ( cmd.contains("exit") || cmd == "q" )
		exit();
	else if ( cmd.contains("colors") )
		SVGGraph::testColors();
	else
		SVGGraph::warning_message("Unknown command !");
}
/**********************************************/
/**********************************************/
void SVGGraph::newel( UList<ustring>& args )
{
	if ( args.isEmpty() )
		SVGGraph::error_message("No arguments !!");
	else
	{
		//			newObject( args );
		ustring arg1;
		if ( args.isEmpty() ) return;
		arg1 = args.first();
		args.popFirst();
		ustring name;
		if ( !args.isEmpty() )
		{
			name = args.first();
			args.popFirst();
		}
		if ( arg1.contains("graph") )
			_graph.newElement( Element::GRAPH, name );
		else if ( arg1 == "-layer" || arg1 == "layer" )
			_graph.newElement( Element::LAYER, name );
		else if ( arg1 == "-frame" || arg1 == "frame" )
			_graph.newElement( Element::FRAME, name );
		else if ( arg1 == "-axis" || arg1 == "axis" )
			_graph.newElement( Element::AXIS, name );
		else if ( arg1 == "-plot" || arg1 == "plot" )
			_graph.newElement( Element::PLOT, name );
		else if ( arg1 == "-legend" || arg1 == "legend" )
			_graph.newElement( Element::LEGEND, name );
		else if ( arg1 == "-title" || arg1 == "title" )
			_graph.newElement( Element::TITLE, name );
	}
}
/**********************************************/
/**********************************************/
void SVGGraph::reorder( UList<ustring>& /*args*/ )
{

}
/**********************************************/
/**********************************************/
void SVGGraph::insert( UList<ustring>& /*args*/ )
{

}
/**********************************************/
/**********************************************/
void SVGGraph::deleteel( UList<ustring>& args )
{
	if ( args.isEmpty() )
	{
		SVGGraph::error_message("No arguments !!");
		return;
	}
	ustring arg1;
	if ( args.isEmpty() ) return;
	arg1 = args.first();
	args.popFirst();
//		if ( args.isEmpty() )
//		{
//			SVGGraph::error_message("No name specified !!");
//			return;
//		}
	ustring name;
	if ( !args.isEmpty() )
	{
		name = args.first();
		args.popFirst();
	}
	/*if ( contains(arg1, "graph") )
		_graph.removeElement( Element::GRAPH, name );
	else */if ( arg1.contains("layer") )
		_graph.removeElement( Element::LAYER, name );
	else if ( arg1.contains("frame") )
		_graph.removeElement( Element::FRAME, name );
	else if ( arg1.contains("axis") || arg1.contains("axes") )
		_graph.removeElement( Element::AXIS, name );
	else if ( arg1.contains("plot") )
		_graph.removeElement( Element::PLOT, name );
	else if ( arg1.contains("legend") )
		_graph.removeElement( Element::LEGEND, name );
	else if ( arg1.contains("title") )
		_graph.removeElement( Element::TITLE, name );
	else if ( arg1.contains("all") )
		_graph.removeElement( Element::NOTYPE );
}
/**********************************************/
/**********************************************/
void SVGGraph::setcurrent( UList<ustring>& args )
{
	_graph.setCurrent( args );
}
/**********************************************/
/**********************************************/
void SVGGraph::set( UList<ustring>& args )
{
	if ( args.isEmpty() )
		SVGGraph::error_message("No arguments !!");
	else
	{
		_graph.set( args );
	}
	cout << endl;
}
/**********************************************/
/**********************************************/
void SVGGraph::unset( UList<ustring>& args )
{
	_graph.unset( args );
}
/**********************************************/
/**********************************************/
void SVGGraph::open( UList<ustring>& args )
{
	if ( args.isEmpty() )
		SVGGraph::error_message("Missing file path !!");
	else
	{
		ustring filepath;
		if ( args.isEmpty() ) return;
		filepath = args.first();
		args.popFirst();
		cout << endl << "Opening : " << filepath << endl;
	}
}
/**********************************************/
/**********************************************/
void SVGGraph::load( UList<ustring>& args )
{
	if ( args.isEmpty() )
		SVGGraph::error_message("Missing file path !!");
	else
	{
		ustring filepath;
		if ( args.isEmpty() ) return;
		filepath = args.first();
		args.popFirst();
		cout << endl << "Loading : " << filepath << endl;
		string contents = File::read( filepath );
		cout << contents << endl;
	}
}
/**********************************************/
/**********************************************/
void SVGGraph::run( UList<ustring>& args )
{
	if ( args.isEmpty() )
		SVGGraph::error_message("Missing file path !!");
	else
	{
		ustring filepath;
		if ( args.isEmpty() ) return;
		filepath = args.first();
		args.popFirst();
		cout << endl << "Running : " << filepath << endl;
		string contents = File::read( filepath );
		cout << contents << endl;
	}
}
/**********************************************/
/**********************************************/
void SVGGraph::save( UList<ustring>& args )
{
	if ( args.isEmpty() )
		SVGGraph::error_message("Missing file path !!");
	else
		_graph.save( args.at(0) );
}
/**********************************************/
/**********************************************/
void SVGGraph::exportg( UList<ustring>& args )
{
	if ( args.isEmpty() )
		SVGGraph::error_message("Missing file path !!");
	else
		_graph.save( args.at(0) );
}
/**********************************************/
/**********************************************/
void SVGGraph::show( UList<ustring>& args )
{
	if ( args.isEmpty() )
		_graph.showAll();
	else
	{
		ustring arg1;
		if ( args.isEmpty() ) return;
		arg1 = args.first();
		args.popFirst();
		if ( arg1.contains("all") )
			_graph.showAll();
		else if ( arg1.contains("current") )
			_graph.showCurrent();
		else if ( arg1.contains("layers") )
			_graph.showAll( Element::LAYER );
		else if ( arg1.contains("frames") )
			_graph.showAll( Element::FRAME );
		else if ( arg1.contains("axes") )
			_graph.showAll( Element::AXIS );
		else if ( arg1.contains("plots") )
			_graph.showAll( Element::PLOT );
		else if ( arg1.contains("legends") )
			_graph.showAll( Element::LEGEND );
		else if ( arg1.contains("titles") )
			_graph.showAll( Element::TITLE );
		else
		{
			ustring name;
			if ( !args.isEmpty() )
			{
				name = args.first();
				args.popFirst();
			}
			if ( arg1.contains("graph") )
				_graph.showAll( Element::GRAPH, name );
			else if ( arg1 == "-layer" || arg1 == "layer" )
				_graph.showAll( Element::LAYER, name );
			else if ( arg1 == "-frame" || arg1 == "frame" )
				_graph.showAll( Element::FRAME, name );
			else if ( arg1 == "-axis" || arg1 == "axis" )
				_graph.showAll( Element::AXIS, name );
			else if ( arg1 == "-plot" || arg1 == "plot" )
				_graph.showAll( Element::PLOT, name );
			else if ( arg1 == "-legend" || arg1 == "legend" )
				_graph.showAll( Element::LEGEND, name );
			else if ( arg1 == "-title" || arg1 == "title" )
				_graph.showAll( Element::TITLE, name );
		}
	}
	cout << endl;
}
/**********************************************/
/**********************************************/
void SVGGraph::about()
{

}
/**********************************************/
/**********************************************/
void SVGGraph::version()
{

}
/**********************************************/
/**********************************************/
void SVGGraph::help( UList<ustring>& /*args*/ )
{
	cout << endl << "Help manual : " << endl << "\t blabla" << endl;

}
/**********************************************/
/**********************************************/
void SVGGraph::exit()
{
	cout << endl << endl << "Thank you for using SVGGraph !" << endl;
	cout << "See you later !" << endl;
}
/**********************************************/
/**********************************************/
// Terminal Commands
/**********************************************/
/**********************************************/
void SVGGraph::init()
{
	//	struct termios old_tio, new_tio;
	/* get the terminal settings for stdin */
	tcgetattr( STDIN_FILENO, &old_tio );
	/* we want to keep the old setting to restore them a the end */
	new_tio = old_tio;
	/* disable canonical mode (buffered i/o) and local echo */
	new_tio.c_lflag &=( ~ICANON & ~ECHO );
	/* set the new settings immediately */
	tcsetattr( STDIN_FILENO, TCSANOW, &new_tio );
}
/**********************************************/
/**********************************************/
void SVGGraph::close()
{
	/* restore the former settings */
	tcsetattr( STDIN_FILENO, TCSANOW, &old_tio );
}
/**********************************************/
/**********************************************/
void SVGGraph::start()
{

}
/**********************************************/
/**********************************************/
void SVGGraph::clear()
{
	printf( "\033[2J" );
	printf( "\033[0;0f" );
}
/**********************************************/
/**********************************************/
void SVGGraph::changeCursorCol( int delta )
{
	if ( delta < 0 )
		moveCursorBackward( -delta );
	else
		moveCursorForward( delta );
}
/**********************************************/
/**********************************************/
void SVGGraph::moveCursorBackward( int delta )
{
	if ( delta > 0 )
		printf( "\033[%dD", delta );
}
/**********************************************/
/**********************************************/
void SVGGraph::moveCursorForward( int delta )
{
	if ( delta > 0 )
		printf( "\033[%dC", delta );
}
/**********************************************/
/**********************************************/
void SVGGraph::saveCursorPos()
{
	printf("\033[s"); // save cursor position
}
/**********************************************/
/**********************************************/
void SVGGraph::restoreCursorPos()
{
	printf("\033[u"); // restore cursor position
}
/**********************************************/
/**********************************************/
void SVGGraph::deleteToEndLine()
{
	printf("\033[K"); // delete char from cursor position to end line
}
/**********************************************/
/**********************************************/
void SVGGraph::normalColor()
{
	printf( "\033[0m" );
}
/**********************************************/
/**********************************************/
void SVGGraph::cmdInputColor()
{
	printf( "\033[1;36m" );
}
/**********************************************/
/**********************************************/
void SVGGraph::cmdColor()
{
	printf( "\x1B[93m" );
}
/**********************************************/
/**********************************************/
void SVGGraph::warningColor()
{
	printf( "\x1B[33m" );
}
/**********************************************/
/**********************************************/
void SVGGraph::errorColor()
{
	printf( "\x1B[31m" );
}
/**********************************************/
/**********************************************/
void SVGGraph::testColors()
{
	printf("\n");
	printf("\x1B[31mTexting\033[0m\t\t");//ROUGE
	printf("\x1B[32mTexting\033[0m\t\t");//VERT
	printf("\x1B[33mTexting\033[0m\t\t");//ORANGE
	printf("\x1B[34mTexting\033[0m\t\t");//BLEU
	printf("\x1B[35mTexting\033[0m\n");//VIOLET

	printf("\x1B[36mTexting\033[0m\t\t");//BLEUVERT
	printf("\x1B[36mTexting\033[0m\t\t");//BLEUVERT
	printf("\x1B[36mTexting\033[0m\t\t");//BLEUVERT
	printf("\x1B[37mTexting\033[0m\t\t");//BLANC
	printf("\x1B[93mTexting\033[0m\n");//JAUNE

	printf("\033[3;42;30mTexting\033[0m\t\t");
	printf("\033[3;43;30mTexting\033[0m\t\t");
	printf("\033[3;44;30mTexting\033[0m\t\t");
	printf("\033[3;104;30mTexting\033[0m\t\t");
	printf("\033[3;100;30mTexting\033[0m\n");

	printf("\033[3;47;35mTexting\033[0m\t\t");
	printf("\033[2;47;35mTexting\033[0m\t\t");
	printf("\033[1;47;35mTexting\033[0m\t\t");
	printf("\n");
}
/**********************************************/
/**********************************************/
void SVGGraph::warning_message( const string& message )
{
	SVGGraph::warningColor();
	printf( "\nWarning: %s\n\n", message.c_str() );
	SVGGraph::normalColor();
}
/**********************************************/
/**********************************************/
void SVGGraph::error_message( const string& message )
{
	SVGGraph::errorColor();
//	cout << endl << ": " << message << endl;
	printf( "\nError: %s\n\n", message.c_str() );
	SVGGraph::normalColor();
}
/**********************************************/
/**********************************************/
void SVGGraph::startCommandLine()
{
	SVGGraph::cmdInputColor();
	printf( "%s> ", progname.c_str() );
	SVGGraph::normalColor();
	SVGGraph::saveCursorPos();
}
/**********************************************/
/**********************************************/
void SVGGraph::clearCommandLine()
{
	SVGGraph::restoreCursorPos();
	SVGGraph::deleteToEndLine();
}
/**********************************************/
/**********************************************/
void SVGGraph::drawCommandLine( const string& cmdline )
{
	clearCommandLine();
	bool first_space_found = false;
	SVGGraph::cmdColor();
	for ( char c : cmdline )
	{
		if ( c == ' ' && !first_space_found )
		{
			SVGGraph::normalColor();
			printf( " " );
			first_space_found = true;
		}
		else
			printf( "%c", c );
	}
}
/**********************************************/
/**********************************************/
bool SVGGraph::confirmationDialog( const string& message )
{
	std::cout << std::endl << message;
	char c = 'n';
	std::cin >> c;
	std::cout << c << std::endl;
	return (c == 'Y' || c == 'y');
}
/**********************************************/
/**********************************************/
