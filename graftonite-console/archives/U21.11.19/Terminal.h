#ifndef TERMINAL_H
#define TERMINAL_H
/**********************************************/
/**********************************************/
#include <termios.h>
#include <map>
#include <string>
using namespace std;
/**********************************************/
/**********************************************/
/**********************************************/
class Terminal
{
	static inline string progname = "svggraph";
	static inline struct termios old_tio = termios(), new_tio = termios();
	/**********************************************/
public:
	static void init();
	static void close();
	static void start();
	static void clear();
	/**********************************************/
	static void changeCursorCol( int delta );
	static void moveCursorBackward( int delta );
	static void moveCursorForward( int delta );
	static void saveCursorPos();
	static void restoreCursorPos();
	static void deleteToEndLine();
	/**********************************************/
	static void normalColor();
	static void cmdInputColor();
	static void cmdColor();
	static void warningColor();
	static void errorColor();
	static void testColors();
	/**********************************************/
	static void warning_message( const string& message );
	static void error_message( const string& message );
	/**********************************************/
	static void startCommandLine();
	static void clearCommandLine();
	static void drawCommandLine( const string& cmdline );
	/**********************************************/
//	static void show( const map<string,string>& cmdline );
	static bool confirmationDialog( const string& message );
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // TERMINAL_H
// ---------------------------------------
// ---------------------------------------
// ---------------------------------------
// HELP ----------------------------------
// Console Cursor Position
//- Position the Cursor:
//  \033[<L>;<C>H
//     Or
//  \033[<L>;<C>f
//  puts the cursor at line L and column C.
//- Move the cursor up N lines:
//  \033[<N>A
//- Move the cursor down N lines:
//  \033[<N>B
//- Move the cursor forward N columns:
//  \033[<N>C
//- Move the cursor backward N columns:
//  \033[<N>D

//- Clear the screen, move to (0,0):
//  \033[2J
//- Erase to end of line:
//  \033[K

//- Save cursor position:
//  \033[s
//- Restore cursor position:
//  \033[u
// ---------------------------------------
// ---------------------------------------
// ---------------------------------------
