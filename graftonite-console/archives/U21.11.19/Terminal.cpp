#include "Terminal.h"
/**********************************************/
/**********************************************/
#include <unistd.h>
#include <termios.h>
#include <stdio.h>
#include <iostream>
/**********************************************/
/**********************************************/
/**********************************************/
void Terminal::init()
{
	//	struct termios old_tio, new_tio;
	/* get the terminal settings for stdin */
	tcgetattr( STDIN_FILENO, &old_tio );
	/* we want to keep the old setting to restore them a the end */
	new_tio = old_tio;
	/* disable canonical mode (buffered i/o) and local echo */
	new_tio.c_lflag &=( ~ICANON & ~ECHO );
	/* set the new settings immediately */
	tcsetattr( STDIN_FILENO, TCSANOW, &new_tio );
}
/**********************************************/
/**********************************************/
void Terminal::close()
{
	/* restore the former settings */
	tcsetattr( STDIN_FILENO, TCSANOW, &old_tio );
}
/**********************************************/
/**********************************************/
void Terminal::start()
{

}
/**********************************************/
/**********************************************/
void Terminal::clear()
{
	printf( "\033[2J" );
	printf( "\033[0;0f" );
}
/**********************************************/
/**********************************************/
void Terminal::changeCursorCol( int delta )
{
	if ( delta < 0 )
		moveCursorBackward( -delta );
	else
		moveCursorForward( delta );
}
/**********************************************/
/**********************************************/
void Terminal::moveCursorBackward( int delta )
{
	if ( delta > 0 )
		printf( "\033[%dD", delta );
}
/**********************************************/
/**********************************************/
void Terminal::moveCursorForward( int delta )
{
	if ( delta > 0 )
		printf( "\033[%dC", delta );
}
/**********************************************/
/**********************************************/
void Terminal::saveCursorPos()
{
	printf("\033[s"); // save cursor position
}
/**********************************************/
/**********************************************/
void Terminal::restoreCursorPos()
{
	printf("\033[u"); // restore cursor position
}
/**********************************************/
/**********************************************/
void Terminal::deleteToEndLine()
{
	printf("\033[K"); // delete char from cursor position to end line
}
/**********************************************/
/**********************************************/
void Terminal::normalColor()
{
	printf( "\033[0m" );
}
/**********************************************/
/**********************************************/
void Terminal::cmdInputColor()
{
	printf( "\033[1;36m" );
}
/**********************************************/
/**********************************************/
void Terminal::cmdColor()
{
	printf( "\x1B[93m" );
}
/**********************************************/
/**********************************************/
void Terminal::warningColor()
{
	printf( "\x1B[33m" );
}
/**********************************************/
/**********************************************/
void Terminal::errorColor()
{
	printf( "\x1B[31m" );
}
/**********************************************/
/**********************************************/
void Terminal::testColors()
{
	printf("\n");
	printf("\x1B[31mTexting\033[0m\t\t");//ROUGE
	printf("\x1B[32mTexting\033[0m\t\t");//VERT
	printf("\x1B[33mTexting\033[0m\t\t");//ORANGE
	printf("\x1B[34mTexting\033[0m\t\t");//BLEU
	printf("\x1B[35mTexting\033[0m\n");//VIOLET

	printf("\x1B[36mTexting\033[0m\t\t");//BLEUVERT
	printf("\x1B[36mTexting\033[0m\t\t");//BLEUVERT
	printf("\x1B[36mTexting\033[0m\t\t");//BLEUVERT
	printf("\x1B[37mTexting\033[0m\t\t");//BLANC
	printf("\x1B[93mTexting\033[0m\n");//JAUNE

	printf("\033[3;42;30mTexting\033[0m\t\t");
	printf("\033[3;43;30mTexting\033[0m\t\t");
	printf("\033[3;44;30mTexting\033[0m\t\t");
	printf("\033[3;104;30mTexting\033[0m\t\t");
	printf("\033[3;100;30mTexting\033[0m\n");

	printf("\033[3;47;35mTexting\033[0m\t\t");
	printf("\033[2;47;35mTexting\033[0m\t\t");
	printf("\033[1;47;35mTexting\033[0m\t\t");
	printf("\n");
}
/**********************************************/
/**********************************************/
void Terminal::warning_message( const string& message )
{
	Terminal::warningColor();
	printf( "\nWarning: %s\n\n", message.c_str() );
	Terminal::normalColor();
}
/**********************************************/
/**********************************************/
void Terminal::error_message( const string& message )
{
	Terminal::errorColor();
//	cout << endl << ": " << message << endl;
	printf( "\nError: %s\n\n", message.c_str() );
	Terminal::normalColor();
}
/**********************************************/
/**********************************************/
void Terminal::startCommandLine()
{
	Terminal::cmdInputColor();
	printf( "%s> ", progname.c_str() );
	Terminal::normalColor();
	Terminal::saveCursorPos();
}
/**********************************************/
/**********************************************/
void Terminal::clearCommandLine()
{
	Terminal::restoreCursorPos();
	Terminal::deleteToEndLine();
}
/**********************************************/
/**********************************************/
void Terminal::drawCommandLine( const string& cmdline )
{
	clearCommandLine();
	bool first_space_found = false;
	Terminal::cmdColor();
	for ( char c : cmdline )
	{
		if ( c == ' ' && !first_space_found )
		{
			Terminal::normalColor();
			printf( " " );
			first_space_found = true;
		}
		else
			printf( "%c", c );
	}
}
/**********************************************/
/**********************************************/
//void Terminal::show( const map<string, string>& args )
//{
//	cout << "        " << "KEYS    " << "|" << "  VALUES" << "\n";
//	cout << "      " << "-----------------------" << "\n";
//	for ( auto const &pair: args )
//	{
//		string key; unsigned long size = pair.first.length();
//		if ( size > 6 ) size = 6;
//		for ( unsigned long it = 0; it < size; ++it )
//			key += pair.first.at(it);
//		for ( unsigned long it = key.length(); it < 8; ++it )
//			key += ' ';
//		cout << "        " << key << "|  " << pair.second << "\n";
//	}
//	cout << "\n";
//}
/**********************************************/
/**********************************************/
bool Terminal::confirmationDialog( const string& message )
{
	std::cout << std::endl << message;
	char c = 'n';
	std::cin >> c;
	std::cout << c << std::endl;
	return (c == 'Y' || c == 'y');
}
/**********************************************/
/**********************************************/
