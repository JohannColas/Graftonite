TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        Commons/File.cpp \
        Commons/Parameters.cpp \
        Commons/System.cpp \
        Commons/UList.cpp \
        Commons/UString.cpp \
        Commons/XML.cpp \
        Graph/Axis.cpp \
        Graph/Element.cpp \
        Graph/Frame.cpp \
        Graph/Graph.cpp \
        Graph/Layer.cpp \
        Graph/Legend.cpp \
        Graph/Plot.cpp \
        Graph/Title.cpp \
        SVGGraph.cpp \
        Terminal.cpp \
        main.cpp

HEADERS += \
	Commons/File.h \
	Commons/Parameters.h \
	Commons/System.h \
	Commons/UList.h \
	Commons/UString.h \
	Commons/XML.h \
	Graph/Axis.h \
	Graph/Element.h \
	Graph/Frame.h \
	Graph/Graph.h \
	Graph/Layer.h \
	Graph/Legend.h \
	Graph/Plot.h \
	Graph/Title.h \
	SVGGraph.h \
	Terminal.h
