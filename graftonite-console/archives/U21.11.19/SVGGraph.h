#ifndef SVGGRAPH_H
#define SVGGRAPH_H
/**********************************************/
#include <termios.h>
/**********************************************/
#include "Commons/UString.h"
/**********************************************/
#include "Graph/Graph.h"
/**********************************************/
/**********************************************/
/**********************************************/
class SVGGraph
{
	static inline Graph _graph = Graph();
	/**********************************************/
public:
	/**********************************************/
	static void decomposeCmdLine( const ustring& cmdline, UList<ustring>& args );
	static void analyseCommand( const ustring& cmd );
	/**********************************************/
	static void newel( UList<ustring>& args );
	static void reorder( UList<ustring>& args );
	static void insert( UList<ustring>& args );
	static void deleteel( UList<ustring>& args );
	static void setcurrent( UList<ustring>& args );
	static void set( UList<ustring>& args );
	static void unset( UList<ustring>& args );
	/**********************************************/
	static void open( UList<ustring>& args );
	static void load( UList<ustring>& args );
	static void run( UList<ustring>& args );
	/**********************************************/
	static void save( UList<ustring>& args );
	static void exportg( UList<ustring>& args );
	/**********************************************/
	static void show( UList<ustring>& args );
	static void about();
	static void version();
	static void help( UList<ustring>& args );
	static void exit();
	/**********************************************/
	// Terminal Variables & Commands
private:
	static inline string progname = "svggraph";
	static inline struct termios old_tio = termios(), new_tio = termios();
	/**********************************************/
public:
	static void init();
	static void close();
	static void start();
	static void clear();
	/**********************************************/
	static void changeCursorCol( int delta );
	static void moveCursorBackward( int delta );
	static void moveCursorForward( int delta );
	static void saveCursorPos();
	static void restoreCursorPos();
	static void deleteToEndLine();
	/**********************************************/
	static void normalColor();
	static void cmdInputColor();
	static void cmdColor();
	static void warningColor();
	static void errorColor();
	static void testColors();
	/**********************************************/
	static void warning_message( const string& message );
	static void error_message( const string& message );
	/**********************************************/
	static void startCommandLine();
	static void clearCommandLine();
	static void drawCommandLine( const string& cmdline );
	/**********************************************/
//	static void show( const map<string,string>& cmdline );
	static bool confirmationDialog( const string& message );
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // SVGGRAPH_H
// ---------------------------------------
// ---------------------------------------
// ---------------------------------------
// HELP ----------------------------------
// Console Cursor Position
//- Position the Cursor:
//  \033[<L>;<C>H
//     Or
//  \033[<L>;<C>f
//  puts the cursor at line L and column C.
//- Move the cursor up N lines:
//  \033[<N>A
//- Move the cursor down N lines:
//  \033[<N>B
//- Move the cursor forward N columns:
//  \033[<N>C
//- Move the cursor backward N columns:
//  \033[<N>D

//- Clear the screen, move to (0,0):
//  \033[2J
//- Erase to end of line:
//  \033[K

//- Save cursor position:
//  \033[s
//- Restore cursor position:
//  \033[u
// ---------------------------------------
// ---------------------------------------
// ---------------------------------------
