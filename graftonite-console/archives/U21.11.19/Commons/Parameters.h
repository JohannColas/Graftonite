#ifndef PARAMETERS_H
#define PARAMETERS_H
/**********************************************/
#include <map>
#include <string>
using namespace std;
/**********************************************/
/**********************************************/
/**********************************************/
class Parameters
{
	map<string,string> _parameters;
	/**********************************************/
public:
	Parameters();
	bool hasKey( const string& key ) const;
	string get( const string& key ) const;
	void set( const string& key, const string& value );
	void set( const pair<string,string>& pair );
	void set( const map<string,string>& args );
	void erase( const string& key );
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // PARAMETERS_H
