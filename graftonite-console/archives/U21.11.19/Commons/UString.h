#ifndef USTRING_H
#define USTRING_H
/**********************************************/
#include <string>
#include <iostream>
using namespace std;
/**********************************************/
/**********************************************/
/**********************************************/
class ustring : public string
{
//	string _text;
public:
	ustring();
	ustring( const double d );
	ustring( const char* c_str );
	ustring( const string& str );
	ustring( const ustring& str );
//	string text() const { return _text; }
	/**********************************************/
	bool contains( const string& str ) const;
	bool contains( const char* c_str ) const;
	double toDouble() const
	{
		double d = 0;
		if ( !empty() )
			d = stod(*this);
		return d;
	}
	/**********************************************/
	// Assignment Operators Overloading
	ustring& operator= ( const ustring& str )
	{
		(*this).clear();
		(*this) += str;
//		for ( auto c : str)
//			(*this) += c;
//		this->_text = str.text();
		return *this;
	}
//	ustring& operator+= ( const ustring& str )
//	{
//		(*this).clear();
////		(*this) += str;
////		for ( auto c : str)
////			(*this) += c;
////		this->_text = str.text();
//		return *this;
//	}
	ustring& operator= ( const string& str )
	{
		(*this).clear();
		(*this) += str;
//		for ( auto c : str)
//			(*this) += c;
//		this->_text = str;
		return *this;
	}
	ustring& operator= ( const char* s )
	{
		(*this).clear();
		(*this) += s;
//		this->_text = s;
		return *this;
	}
	ustring& operator= (char c)
	{
//		this->_text = c;
		(*this).clear();
		(*this) += c;
		return *this;
	}
//	ustring& operator= (initializer_list<char> il)
//	{

//	}
//	ustring& operator= (string&& str) noexcept;
//	bool operator!=(ustring const & l, ustring const & r)
//	{
//		return !(l == r);
//	}
	friend ostream& operator<<( ostream& os, const ustring& str );
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // USTRING_H
