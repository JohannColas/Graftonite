#ifndef FILE_H
#define FILE_H
/**********************************************/
#include <string>
using namespace std;
/**********************************************/
/**********************************************/
/**********************************************/
class File
{
public:
	static string read( const string& path );
	static void write( const string& path, const string& contents );
	static void append( const string& path, const string& contents );
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // FILE_H
