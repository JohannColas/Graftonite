#include "Parameters.h"
/**********************************************/
/**********************************************/
Parameters::Parameters()
{

}
/**********************************************/
/**********************************************/
bool Parameters::hasKey( const string& key ) const
{
	if ( _parameters.count( key ) > 0 )
		return true;
	return false;
}
/**********************************************/
/**********************************************/
string Parameters::get( const string& key ) const
{
	if ( hasKey(key) )
		return _parameters.at( key );
	return "";
}
/**********************************************/
/**********************************************/
void Parameters::set( const string& key, const string& value )
{
	if ( hasKey(key) )
		_parameters.at(key) = value;
	else
		_parameters.emplace( key, value );
}
/**********************************************/
/**********************************************/
void Parameters::set( const pair<string,string>& pair )
{
	set( pair.first, pair.second );
}
/**********************************************/
/**********************************************/
void Parameters::set( const map<string,string>& args )
{
	for ( auto const& pair: args )
		set( pair );
}
/**********************************************/
/**********************************************/
void Parameters::erase( const string& key )
{
	if ( hasKey(key) )
		_parameters.erase(key);
}
/**********************************************/
/**********************************************/
