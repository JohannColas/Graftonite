#include "UString.h"
/**********************************************/
/**********************************************/
/**********************************************/
ustring::ustring() : string()
{

}

ustring::ustring( const double d ) : string()
{
	ustring strd = to_string(d);
	char dec = 0;
	if ( strd.contains(".") ) dec = '.';
	else if ( strd.contains(",") ) dec = ',';
	while ( strd.back() == '0' )
		strd.pop_back();
	if ( strd.back() == dec )
		strd.pop_back();
	if ( strd == "-0" )
		strd = "0";
	(*this) = strd;
}
/**********************************************/
ustring::ustring( const char* c_str ) : string(c_str)
{

}
/**********************************************/
ustring::ustring( const string& str ) : string(str)
{

}
/**********************************************/
ustring::ustring( const ustring& str ) : string(str)
{

}
/**********************************************/
/**********************************************/
bool ustring::contains( const string& str ) const
{
//	return (this->text().find(str) != string::npos);
	return (this->find(str) != string::npos);
}
/**********************************************/
/**********************************************/
bool ustring::contains( const char* c_str ) const
{
//	return (this->text().find(c_str) != string::npos);
	return (this->find(c_str) != string::npos);
}
/**********************************************/
/**********************************************/
ostream& operator<<( ostream& os, const ustring& str )
{

//	os << str.text();
	for ( auto c : str )
		os << c;
	return os;
}
/**********************************************/
/**********************************************/
