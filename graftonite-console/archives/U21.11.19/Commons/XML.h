#ifndef XML_H
#define XML_H
/**********************************************/
#include "Commons/UString.h"
#include <map>
using namespace std;
/**********************************************/
/**********************************************/
/**********************************************/
class XML
{
public:
	static ustring beginGroup( const ustring& tag, const map<ustring,ustring>& attributes )
	{
		ustring str = "<";
		str += tag;
		for ( auto it=attributes.begin(); it!=attributes.end(); ++it )
			str += ' ' + it->first + "=\"" + it->second + '\"';
		str += ">";
		return str;
	}
	static ustring endGroup( const ustring& tag )
	{
		ustring str = "</" + tag + ">";
		return str;
	}
	static inline ustring drawLine( double x0, double y0, ustring dir, double ap1 )
	{
		return "<path d=\"M " + ustring(x0) + "," + ustring(y0) + " "+dir+" " + ustring(ap1) + "\"/>";
	}
	static inline ustring drawText( ustring coord, double ap0, ustring text )
	{
		return "<text "+coord+"=\"" + ustring(ap0) + "\">"+text+"</text>";
	}
	static inline ustring drawText( double xpos, double ypos, ustring text, ustring transfo = "" )
	{
		ustring transfo2 = transfo;
		if ( !transfo2.empty() ) transfo2 = " " + transfo2;
		return "<text x=\"" + ustring(xpos) + "\" y=\"" + ustring(ypos) + "\""+transfo2+">"+text+"</text>";
	}
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // XML_H
