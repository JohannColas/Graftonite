#include "File.h"
/**********************************************/
#include <fstream>
/**********************************************/
/**********************************************/
string File::read( const string& path )
{
	string contents;
	ifstream file;
	file.open( path );
	if ( file.is_open() )
	{
		string line;
		while ( file )
		{
			std::getline( file, line );
			contents += line + "\n";
		}
		file.close();
	}
	return contents;
}
/**********************************************/
/**********************************************/
void File::write( const string& path, const string& contents )
{
	ofstream file;
	file.open( path );
	if ( file.is_open() )
	{
		file << contents << endl;
		file.close();
	}
}
/**********************************************/
/**********************************************/
void File::append(const string& path, const string& contents)
{
	string contents_to_save = File::read( path );
	contents_to_save += "\n" + contents;
	File::write( path, contents_to_save );
}
/**********************************************/
/**********************************************/
