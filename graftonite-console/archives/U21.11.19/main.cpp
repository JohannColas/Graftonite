/**********************************************/
/**********************************************/
#include <iostream>
#include <string.h>
#include <vector>
using namespace std;
/**********************************************/
/**********************************************/
//#include "Terminal.h"
#include "SVGGraph.h"
/**********************************************/
/**********************************************/
enum KEYS
{
	ESC = 27,
	UP_ARROW = 65,
	DOWN_ARROW = 66,
	RIGHT_ARROW = 67,
	LEFT_ARROW = 68,
	FN_RIGHT = 70,
	FN_LEFT = 72,
	DEL = 126,
	RETURN = 10,
	BACKSPACE = 127,
};
/**********************************************/
/**********************************************/
int main( int argc, char *argv[] )
{
	SVGGraph svggraph = SVGGraph();
	cout << endl << "----------------------------" << endl;
	cout << endl << " Welcome to SVGGraph !" << endl;
	cout << endl << "----------------------------" << endl;

	bool open_interface = true;
	if ( argc > 1  )
	{
		ustring argv1 = argv[1];
		if ( argv1.contains("help") )
		{
//			graph.help();
			open_interface = false;
		}
		else if ( argv1.contains("console") )
		{
			open_interface = true;
		}
		else if ( argv1.contains("load")  && argc > 2 )
		{
			ustring arg = argv[2];
//			graph.load(arg);
		}
		else if ( argv1.contains("run") && argc > 2 )
		{
			ustring arg = argv[2];
//			graph.run(arg);
			open_interface = false;
		}
	}
	cout << endl;



	if ( !open_interface )
		return 0;

	/* --------------------------- */
	// Terminal Initialisation
	SVGGraph::init();
	/* --------------------------- */

	unsigned char c;
//	c = getchar();
//	printf( "%d ", c ); // check char code
//	cout << endl;
	ustring cur_cmd;
	vector<ustring> cmd_history;
	int history_index = -1;
	int cursor_pos = 0;
	bool exit = false;
	do
	{
		SVGGraph::startCommandLine();
		cursor_pos = 0;
		history_index = -1;
		cur_cmd.clear();
		do
		{
			c = getchar();
			if ( c == KEYS::ESC ) // escape key
			{
				char new_c = getchar();
				if ( new_c == 91 )
				{
					new_c = getchar();
					if ( new_c == KEYS::UP_ARROW ) // Up Arrow
					{
						SVGGraph::clearCommandLine();
						cur_cmd.clear();
						if ( history_index == -1 )
						{
							history_index = cmd_history.size()-1;
						}
						else
							--history_index;
						if ( history_index < 0 )
							history_index = 0;
						if ( history_index != -1 && cmd_history.size() > 0 )
						{
							cur_cmd = cmd_history.at(history_index);
							SVGGraph::drawCommandLine( cur_cmd );
							cursor_pos = cur_cmd.length();
						}
					}
					else if ( new_c == KEYS::DOWN_ARROW ) // Down Arrow
					{
						SVGGraph::clearCommandLine();
						cur_cmd.clear();
						cursor_pos = 0;
						if ( history_index > -1 )
							++history_index;
						if ( history_index > (int)cmd_history.size()-1 )
							history_index = -1;
						if ( history_index != -1 && cmd_history.size() > 0 )
						{
							cur_cmd = cmd_history.at(history_index);
							SVGGraph::drawCommandLine( cur_cmd );
							cursor_pos = cur_cmd.length();
						}
					}
					else if ( new_c == KEYS::RIGHT_ARROW ) // Right Arrow
					{
						if ( !cur_cmd.empty() )
						{
							++cursor_pos;
							if ( cursor_pos < (int)cur_cmd.length()+1 )
								SVGGraph::moveCursorForward(1);
							if ( cursor_pos > (int)cur_cmd.length() )
								cursor_pos = cur_cmd.length();
						}
					}
					else if ( new_c == KEYS::LEFT_ARROW )// Left Arrow
					{
						if ( !cur_cmd.empty() )
						{
							--cursor_pos;
							if ( cursor_pos >= 0 )
								SVGGraph::moveCursorBackward(1);
							if ( cursor_pos < 0 )
								cursor_pos = 0;
						}
					}
					else if ( new_c == KEYS::FN_RIGHT )// FN + Right Arrow
					{
						if ( !cur_cmd.empty() && cursor_pos < (int)cur_cmd.length() )
						{
							int diff = cur_cmd.length()-cursor_pos;
							if ( diff > 0 )
							{
								SVGGraph::moveCursorForward(diff);
								cursor_pos = cur_cmd.length();
							}
						}
					}
					else if ( new_c == KEYS::FN_LEFT )// FN + Left Arrow
					{
						if ( !cur_cmd.empty() && cursor_pos > 0 )
						{
								SVGGraph::moveCursorBackward(cursor_pos);
								cursor_pos=0;
						}
					}
					else if ( new_c == 51 )
					{
						new_c = getchar();
						if ( new_c == KEYS::DEL ) // delete key
						{
							if ( cursor_pos < (int)cur_cmd.length() )
							{
								int old_dif = cur_cmd.length() - cursor_pos;
								cur_cmd = cur_cmd.erase( cursor_pos, 1 );
								SVGGraph::drawCommandLine( cur_cmd );
								if ( old_dif > 0 )
								{
									SVGGraph::moveCursorBackward(old_dif);
									SVGGraph::moveCursorForward(1);
								}
							}
						}
					}
				}
				else // escape key
				{
					exit = true;
					break;
				}
			}
			else if ( c == KEYS::RETURN )
			{
				SVGGraph::normalColor();
				svggraph.analyseCommand( cur_cmd );
				history_index = -1;
				cursor_pos = 0;
				if ( !cur_cmd.empty() )
					cmd_history.push_back( cur_cmd );
				printf("\n");
			}
			else if ( c == KEYS::BACKSPACE ) // BACKSPACE KEY
			{
				// Reset Current Cmd Index
				history_index = -1;
				//
				int old_dif = cur_cmd.length() - cursor_pos;
				--cursor_pos;
				if ( cur_cmd.length() > 0 && cursor_pos >= 0 )
				{
					cur_cmd.erase( cursor_pos, 1 );
					SVGGraph::drawCommandLine( cur_cmd );
					SVGGraph::moveCursorBackward(old_dif);
				}
				if ( cursor_pos < 0 )
					cursor_pos = 0;
			}
			else
			{
				int old_dif = cur_cmd.length() - cursor_pos;
				cur_cmd.insert( cursor_pos, 1, c );
				++cursor_pos;
				SVGGraph::drawCommandLine( cur_cmd );
				SVGGraph::moveCursorBackward(old_dif);
			}
		}
		while ( c != KEYS::RETURN );
	}
	while( cur_cmd.substr(0,4) != "exit" && !exit );

	cout << endl;

	/* --------------------------- */
	// Terminal Exit
	SVGGraph::close();
	/* --------------------------- */

	return 0;
}
/**********************************************/
/**********************************************/
