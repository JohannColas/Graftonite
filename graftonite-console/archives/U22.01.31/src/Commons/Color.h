#ifndef COLOR_H
#define COLOR_H
/**********************************************/
#include <iostream>
class ustring;
/**********************************************/
/**********************************************/
/**********************************************/
class Color
{
	double _red = 0.0;
	double _green = 0.0;
	double _blue = 0.0;
	double _alpha = 1.0;
public:
	Color();
	Color( double red, double green, double blue, double alpha = 1.0 );
	Color( int red, int green, int blue, int alpha = 255 );
	Color( const ustring& color );
	double red() const;
	void setRed( double red );
	void setRed( int red );
	double green() const;
	void setGreen( double green );
	void setGreen( int green );
	double blue() const;
	void setBlue( double blue );
	void setBlue( int blue );
	double alpha() const;
	void setAlpha( double alpha );
	void setAlpha( int alpha );
	void setColor( const ustring& color );
	static ustring getColorByName( const ustring& name );
	friend std::ostream& operator<<( std::ostream& os, const Color& color )
	{
		os << "{"
		   << color.red() << ", "
		   << color.green() << ", "
		   << color.blue() << ", "
		   << color.alpha() << "}";
		return os;
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // COLOR_H
