#ifndef ULIST_H
#define ULIST_H
/**********************************************/
//#include <deque>
//#include <iostream>
///**********************************************/
///**********************************************/
#include <type_traits>
template <class T>
void deletePtr(T &)
{
}
template <class T>
void deletePtr(T* &ptr)
{
	delete &ptr;
	ptr = nullptr;
}
///**********************************************/
//template <class T>
//class UList
//{
//private:
//	std::deque<T> _list;
//	const bool _auto_delete = false;
//public:
//	~UList();
//	UList();
//	UList( const UList& list );
//	UList( const std::deque<T>& list );
//	UList( const std::initializer_list<T>& args );
//	unsigned long size() const;
//	T& at( unsigned long index );
//	T at( unsigned long index ) const;
//	void pushFirst( T const& );
//	void append( T const& );
//	void append( UList<T> const& );
//	void popFirst();
//	void popLast();
//	T first() const;
//	T last() const;
//	T takeFirst();
//	T takeLast();
//	T takeAt( unsigned long index );
//	void replace( unsigned long index, const T& el );
//	bool contains( const T& el ) const;
//	void clear();
//	void deleteAll();

//	bool isEmpty() const
//	{
//		return _list.empty();
//	}
//	bool isValid( unsigned index ) const
//	{
//		return (index >= 0 || index < size());
//	}

//	// -----------------------------------
//	typename std::deque<T>::iterator begin()
//	{
//		return _list.begin();
//	}
//	typename std::deque<T>::const_iterator begin() const
//	{
//		return _list.begin();
//	}
//	typename std::deque<T>::const_iterator cbegin() const
//	{
//		return begin();
//	}
//	typename std::deque<T>::iterator end()
//	{
//		return _list.end();
//	}
//	typename std::deque<T>::const_iterator end() const
//	{
//		return _list.end();
//	}
//	typename std::deque<T>::const_iterator cend() const
//	{
//		return end();
//	}
//	// -----------------------------------
//	UList<T>& operator= ( const UList<T>& list )
//	{
//		(*this).clear();
//		for ( auto el : list )
//			(*this).append(el);
//		return *this;
//	}
//	UList<T>& operator<< ( const T& el )
//	{
//		(*this).append(el);
//		return *this;
//	}
//	// -----------------------------------
//};
///**********************************************/
//template <class T>
//UList<T>::~UList()
//{
//	if ( _auto_delete )
//		for ( unsigned long it = 0, c = size(); it < c; ++it )
//			deletep( _list[it] );
//}
///**********************************************/
//template <class T>
//UList<T>::UList()
//{

//}
///**********************************************/
//template<class T>
//UList<T>::UList( const UList& list )
//{
//	for ( auto el : list )
//		this->append(el);
//}
///**********************************************/
//template<class T>
//UList<T>::UList( const std::deque<T>& list )
//{
//	_list = list;
//}
///**********************************************/
//template<class T>
//UList<T>::UList( const std::initializer_list<T>& args )
//{
//	for ( auto el : args )
//		this->append(el);
//}
///**********************************************/
//template<class T>
//unsigned long UList<T>::size() const
//{
//	return _list.size();
//}
///**********************************************/
//template<class T>
//T& UList<T>::at( unsigned long index )
//{
//	return _list.at(index);
//}
///**********************************************/
//template<class T>
//T UList<T>::at( unsigned long index ) const
//{
//	return _list.at(index);
//}
///**********************************************/
//template <class T>
//void UList<T>::pushFirst( T const& elem )
//{
//	_list.push_front(elem);
//}
///**********************************************/
//template <class T>
//void UList<T>::append( T const& elem )
//{
//	_list.push_back(elem);
//}
///**********************************************/
//template<class T>
//void UList<T>::append( const UList<T>& list )
//{
//	for ( auto el : list )
//		this->append( el );
//}
///**********************************************/
//template <class T>
//T UList<T>::takeFirst()
//{
//	T el = _list.front();
//	_list.pop_front();
//	return el;
//}
///**********************************************/
//template <class T>
//T UList<T>::takeLast()
//{
//	T el = _list.back();
//	_list.pop_back();
//	return el;
//}
///**********************************************/
//template<class T>
//T UList<T>::takeAt( unsigned long index )
//{
//	T tmp= _list.at(index);
//	_list.erase(begin()+index);
//	return tmp;
//}
///**********************************************/
//template<class T>
//void UList<T>::replace( unsigned long index, const T& el )
//{
//	if ( isValid(index) )
//		_list.at(index) = el;
//}
///**********************************************/
//template<class T>
//void UList<T>::clear()
//{
//	while ( !this->isEmpty() )
//		_list.pop_back();
//}
///**********************************************/
//template<class T>
//void UList<T>::deleteAll()
//{
//	while ( !this->isEmpty() )
//		delete this->takeLast();
//}
///**********************************************/
//template <class T>
//void UList<T>::popFirst() {
//	if (isEmpty()) {
//		//      throw out_of_range("Stack<>::pop(): empty stack");
//		return;
//	}

//	// remove last element
//	_list.pop_front();
//}
///**********************************************/
//template <class T>
//void UList<T>::popLast() {
//	if (isEmpty()) {
//		//      throw out_of_range("Stack<>::pop(): empty stack");
//		return;
//	}

//	// remove last element
//	_list.pop_back();
//}
///**********************************************/
//template<class T>
//T UList<T>::first() const
//{
//	return _list.front();
//}
///**********************************************/
//template<class T>
//T UList<T>::last() const
//{
//	return _list.back();
//}
///**********************************************/
//template<class T>
//std::ostream& operator<<( std::ostream& out, const UList<T>& list )
//{
//	out  << "{";
//	if ( !list.isEmpty() )
//	{
//		out << list.first();
//		for ( unsigned long it = 1; it < list.size(); ++it )
//			out << "," << list.at(it);
//	}
//	out << "}";
//	return out;
//}
///**********************************************/
//template<class T>
//bool UList<T>::contains( const T& el ) const
//{
//	for ( auto lel : _list )
//		if ( lel == el )
//			return true;
//	return false;
//}

/**********************************************/
/**********************************************/
/**********************************************/

/**********************************************/
#include <iostream>
#include <deque>
/**********************************************/
template<typename T>
struct delete_traits
{
//	void destroy(T) {}
	void destroy(T&) {}
};
template<typename T>
struct delete_traits<T*>
{
  void destroy(T* p) { delete p; }
};
template < typename PTR > inline
void do_delete( PTR&& pointer ) { delete pointer ; pointer = nullptr ; }
/**********************************************/
/**********************************************/
template <typename T>
class UList
{
	class UListItem
	{
		T _data;
		UListItem* _parent = nullptr;
		UListItem* _child = nullptr;
	public:
		~UListItem()
		{
		}
		UListItem( T const& data, UListItem* parent = nullptr, UListItem* child = nullptr )
		{
			_data = data;
			setParent( parent );
			setChild( child );
		}
		UListItem( const UListItem& item )
		{
			_data = item.data();
			_parent = item.parent();
			_child = item.child();
		}
		UListItem* parent()
		{
			return _parent;
		}
		UListItem* parent() const
		{
			return _parent;
		}
		void setParent( UListItem* parent )
		{
			_parent = parent;
		}
		UListItem* child()
		{
			return _child;
		}
		UListItem* child() const
		{
			return _child;
		}
		void setChild( UListItem* child )
		{
			_child = child;
			if ( _child != nullptr )
				_child->setParent( this );
		}
		T data() const
		{
			return _data;
		}
		void setData( const T& data )
		{
			_data = data;
		}
	};
	/**********************************************/
	/**********************************************/
	const bool _auto_delete = false;
	UListItem* _first = nullptr;
	UListItem* _last = nullptr;
	/**********************************************/
	/**********************************************/
public:
	class iterator
	{
		friend class UList;
	public:
		iterator( UListItem* item )
		{
			_item = item;
		}
		iterator()
		{
			_item = nullptr;
		}
		bool operator !=(const iterator& other) const
		{
			return _item != other._item;
		}
		bool operator ==( const iterator& other ) const
		{
			return _item == other._item;
		}
		iterator* operator++()
		{
			_item = _item->child();
			return this;
		}
		iterator* operator++( int )
		{
			_item = _item->child();
			return this;
		}
//		T& operator*()
//		{
//			return _item->data();
//		}
		T operator*() const
		{
			return _item->data();
		}
	private:
		UListItem* _item;
	};
	/**********************************************/
	/**********************************************/
	typename UList<T>::iterator begin()
	{
		return iterator(_first);
	}
	typename UList<T>::iterator end()
	{
		return iterator(nullptr);
	}
	typename UList<T>::iterator begin() const
	{
		return iterator(_first);
	}
	typename UList<T>::iterator end() const
	{
		return iterator(nullptr);
	}
	/**********************************************/
	/**********************************************/
public:
	~UList()
	{

	}
	UList()
	{

	}
	UList( const std::deque<T>& list )
	{
		for ( auto el : list )
			this->append(el);
	}
	UList( const std::initializer_list<T>& args )
	{
		for ( auto el : args )
			this->append(el);
	}
	UList( const UList<T>& list )
	{
		this->append( list );
	}
	/**********************************************/
	/**********************************************/
	// Checkers
	/**********************************************/
	int size() const
	{
		int count = 0;
		for ( auto it = this->begin(); it != this->end(); ++it )
			++count;
		return count;
	}
	bool isEmpty() const
	{
		return ( this->size() < 1 );
	}
	bool isValid( int index ) const
	{
		return (index >= 0 && index < size());
	}
	bool contains( const T& el ) const
	{
		for ( auto it = this->begin(); it != this->end(); ++it )
			if ( it._item->data() == el )
				return true;
		return false;
	}
	int firstIndexOf( const T& el ) const
	{
		for ( int it = 0; it < this->size(); ++it )
			if ( this->at(it) == el )
				return it;
		return -1;
	}
	int lastIndexOf( const T& el ) const
	{
		for ( int it = (this->size()-1); it >= 0; --it )
			if ( this->at(it) == el )
				return it;
		return -1;
	}
	UList<int> indexesOf( const T& el ) const
	{
		UList<int> indexes;
		for ( int it = (this->size()-1); it >= 0; --it )
			if ( this->at(it) == el )
				indexes.append(it);
		return indexes;
	}
	/**********************************************/
	/**********************************************/
	// Accessors
	/**********************************************/
//	UListItem* itemAt( int index )
//	{
//		if ( !isValid(index) || _first == nullptr )
//			return nullptr;
//		UListItem* cur = _first;
//		for ( int it = 0; it < this->size(); ++it )
//		{
//			if ( it == index )
//				return cur;
//			cur = cur->child();
//		}
//		return nullptr;
//	}
	UListItem* itemAt( int index ) const
	{
		if ( !isValid(index) || _first == nullptr )
			return nullptr;
		UListItem* cur = _first;
		for ( int it = 0; it < this->size(); ++it )
		{
			if ( it == index )
				return cur;
			cur = cur->child();
		}
		return nullptr;
	}
//	T& at( int index )
//	{
//		UListItem* item = this->itemAt(index);
//		if ( item != nullptr )
//			return item->data();
//		T* ptr = nullptr;
//		return (*ptr);
//	}
	T at( int index ) const
	{
		UListItem* item = this->itemAt(index);
		if ( item != nullptr )
			return item->data();
		return T();
	}
	T first() const
	{
		return firstItem()->data();
	}
//	T& first()
//	{
//		return firstItem()->data();
//	}
	UListItem* firstItem() const
	{
		return _first;
	}
	T last() const
	{
		return lastItem()->data();
	}
	UListItem* lastItem() const
	{
		return _last;
	}
	/**********************************************/
	/**********************************************/
	// Modifiers
	/**********************************************/
	void pushFirst( const T& data )
	{
		UListItem* new_item = new UListItem( data, nullptr, nullptr );
		if ( _first == nullptr )
		{
			_first = new_item;
			_last = new_item;
		}
		else
		{
			new_item->setChild( _first );
			_first = new_item;
		}
	}
	void append( const T& data )
	{
		UListItem* new_item = new UListItem( data, nullptr, nullptr );
		if ( _last == nullptr )
		{
			_first = new_item;
			_last = new_item;
		}
		else
		{
			_last->setChild( new_item );
			_last = new_item;
		}
	}
	void append( const UList<T>& list )
	{
		for ( T data : list )
			this->append( data );
	}
	void insert( int index, const T& data )
	{
		if ( /*!isValid(index)*/ index < 0 )
			return;
		if ( index >= size() )
		{
			this->append( data );
			return;
		}
		UListItem* child = this->itemAt(index);
		UListItem* parent = child->parent();
		UListItem* new_item = new UListItem( data, parent, child );
		if ( parent == nullptr )
			_first = new_item;
		else
			parent->setChild(new_item);
	}
	void insert( int index, const UList<T>& data )
	{
		int it = 0;
		for ( const T& tmp : data )
		{
			this->insert( index+it, tmp );
			++it;
		}
	}
	void replace( int index, const T& el )
	{
		if ( this->isValid(index) )
			this->itemAt(index)->setData(el);
	}
	// Removers
	void remove( const T& data )
	{
		while ( contains(data) )
		{
			int index = this->index(data);
			removeAt(index);
		}
	}
	void removeAt( int index )
	{
		UListItem* item = this->itemAt(index);
		if ( item == nullptr)
			return;
		UListItem* parent = item->parent();
		UListItem* child = item->child();
		if ( parent == nullptr && child == nullptr )
		{
			_first = nullptr;
			_last = nullptr;
		}
		else if ( parent == nullptr )
		{
			_first = child;
			_first->setParent(nullptr);
		}
		else if ( child == nullptr )
		{
			_last = parent;
			_last->setChild(nullptr);
		}
		else
		{
			parent->setChild( child );
		}
		delete item;
	}
	void removeFirst()
	{
		UListItem* item = _first;
		_first = _first->child();
		if ( _first == nullptr )
			_last = nullptr;
		else
			_first->setParent(nullptr);
		delete item;
	}
	void removeLast()
	{
		UListItem* item = _last;
		_last = _last->parent();
		if ( _last == nullptr )
			_first = nullptr;
		else
			_last->setChild(nullptr);
		delete item;
	}
	void clear()
	{
		while ( !this->isEmpty() )
			this->removeFirst();
	}
	void deleteAll()
	{
		while ( !this->isEmpty() )
			delete this->takeFirstItem();
	}
	int index( UListItem* item )
	{
		UListItem* it = _first;
		int ind = 0;
		while ( it != nullptr )
		{
			if ( it == item )
				return ind;
			it = _first->child();
		}
		return -1;
	}
	int index( const T& data )
	{
		UListItem* it = _first;
		int ind = 0;
		while ( it != nullptr )
		{
			if ( it->data() == data )
				return ind;
			it = _first->child();
		}
		return -1;
	}
	// Takers
	UListItem* takeItemAt( int index )
	{
		UListItem* item = this->itemAt(index);
		if ( item != nullptr )
			item = new UListItem( item->data() );
		removeAt(index);
		return item;
	}
	UListItem* takeFirstItem()
	{
		return this->takeItemAt(0);
	}
	UListItem* takeLastItem()
	{
		return this->takeItemAt(size()-1);
	}
	T takeAt( int index)
	{
		UListItem* item = this->takeItemAt(index);
		if ( item != nullptr )
			return item->data();
		return T();
	}
	T takeFirst()
	{
		UListItem* item = this->takeFirstItem();
		if ( item != nullptr )
			return item->data();
		return T();
	}
	T takeLast()
	{
		UListItem* item = this->takeLastItem();
		if ( item != nullptr )
			return item->data();
		return T();
	}
	// -----------------------------------
	/**********************************************/
	/**********************************************/
	// Operators
	/**********************************************/
	UList<T>& operator= ( const UList<T>& list )
	{
		this->clear();
		this->append(list);
		return *this;
	}
	UList<T>& operator+= ( const T& el )
	{
		this->append(el);
		return *this;
	}
	UList<T>& operator<< ( const T& el )
	{
		this->append(el);
		return *this;
	}
	// -----------------------------------
	/**********************************************/
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
template<typename T>
std::ostream& operator<<( std::ostream& os, const UList<T>& list )
{
	os << "{";
	if ( !list.isEmpty() )
	{
		os << list.first();
		for ( int it = 1; it < list.size(); ++it )
			os << "," << list.at(it);
	}
	os << "}";
	return os;
}
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ULIST_H
