#include "Styles.h"
/**********************************************/
#include "Cairo.h"
/**********************************************/
/**********************************************/
/**********************************************/
FontMetrics::FontMetrics()
{

}
FontMetrics::FontMetrics( cairo_t* drawing )
{
	init( drawing );
}
FontMetrics::FontMetrics( const ustring& text, cairo_t* drawing )
{
	init( text, drawing );
}
void FontMetrics::init( cairo_t* drawing )
{
	cairo_font_extents_t fe;
	cairo_font_extents( drawing, &fe );
	setAscent( fe.ascent );
	setDescent( fe.descent );
	setHeight( fe.height );
}
void FontMetrics::init( const ustring& text, cairo_t* drawing )
{
	init( drawing );
	cairo_text_extents_t te;
	cairo_text_extents( drawing, text.c_str(), &te );
	setTightHeight( te.height );
	setWidth( te.x_advance );
	setTightWidth( te.width );
	setXBearing( te.x_bearing );
	setYBearing( te.y_bearing );
}
double FontMetrics::ascent() const
{
	return _ascent;
}
void FontMetrics::setAscent( double ascent )
{
	_ascent = ascent;
}
double FontMetrics::descent() const
{
	return _descent;
}
void FontMetrics::setDescent( double descent )
{
	_descent = descent;
}
double FontMetrics::height() const
{
	return _height;
}
void FontMetrics::setHeight( double height )
{
	_height = height;
}
double FontMetrics::tightHeight() const
{
	return _tight_height;
}
void FontMetrics::setTightHeight( double tightHeight )
{
	_tight_height = tightHeight;
}
double FontMetrics::width() const
{
	return _width;
}
void FontMetrics::setWidth( double width )
{
	_width = width;
}
double FontMetrics::tightWidth() const
{
	return _tight_width;
}
void FontMetrics::setTightWidth( double tightWidth )
{
	_tight_width = tightWidth;
}
double FontMetrics::xBearing() const
{
	return _x_bearing;
}
void FontMetrics::setXBearing( double xBearing )
{
	_x_bearing = xBearing;
}
double FontMetrics::yBearing() const
{
	return _y_bearing;
}
void FontMetrics::setYBearing( double yBearing )
{
	_y_bearing = yBearing;
}
/**********************************************/
/**********************************************/
/**********************************************/
TextStyle::TextStyle()
{

}
TextStyle::TextStyle( const ustring& style )
{
	set( style );
}
FontMetrics TextStyle::metrics( const ustring& text, cairo_t* drawing ) const
{
	return FontMetrics( text, drawing );
}
FontMetrics TextStyle::metrics( const ustring& text, Cairo& drawing ) const
{
	drawing.setTextStyle( *this );
	return FontMetrics( text, drawing.drawing() );
}
FontMetrics TextStyle::metrics( const ustring& text, Cairo* drawing ) const
{
	drawing->setTextStyle( *this );
	return FontMetrics( text, drawing->drawing() );
}
void TextStyle::set( const ustring& style )
{
	std::deque<ustring> options;
	ustring tmp = style;
	unsigned long it = tmp.find(';');
	while ( it != std::string::npos )
	{
		options.push_back( tmp.substr(0,it) );
		tmp = tmp.substr( it+1 );
		it = tmp.find(';', 0 );
	}
	if ( !tmp.empty() )
		options.push_back( tmp );
	for ( auto str : options )
	{
		unsigned long it = str.find(':', 0 );

		if ( it == std::string::npos )
		{
			if ( str == "bold" )
				_weight = CAIRO_FONT_WEIGHT_BOLD;
			else if ( str == "notbold" )
				_weight = CAIRO_FONT_WEIGHT_NORMAL;
			else if ( str == "italic" )
				_slant = CAIRO_FONT_SLANT_ITALIC;
			else if ( str == "oblique" )
				_slant = CAIRO_FONT_SLANT_OBLIQUE;
			else if ( str == "notitalic" )
				_slant = CAIRO_FONT_SLANT_NORMAL;
			else if ( str == "underline" )
				_underline = true;
			else if ( str == "nounderline" )
				_underline = false;
			else if ( str == "superscript" )
				_scriptopt = ScriptOpt::SuperScript;
			else if ( str == "subscript" )
				_scriptopt = ScriptOpt::SubScript;
			else if ( str == "normalscript" )
				_scriptopt = ScriptOpt::NormalScript;
			else if ( str == "nocap" )
				_cap = TextStyle::NoCap;
			else if ( str == "cap" )
				_cap = TextStyle::Cap;
			else if ( str == "smallcap" )
				_cap = TextStyle::SmallCap;
			else if ( str == "normal" )
			{
				_weight = CAIRO_FONT_WEIGHT_NORMAL;
				_slant = CAIRO_FONT_SLANT_NORMAL;
				_cap = TextStyle::NoCap;
				_scriptopt = ScriptOpt::NormalScript;
				_underline = false;
			}
		}
		else
		{
			ustring key, value;
			key = str.substr( 0, it );
			value = str.substr( it+1 );
			if ( key == "family" )
				_family = value;
			if ( key == "color" )
				_color = value;
			if ( key == "size" && value.isDouble() )
				_size = stod(value);
			else if ( str == "rotate" && value.isDouble() )
				_rotation = stod(value);
		}
	}
}
ustring TextStyle::family() const
{
	return _family;
}

void TextStyle::setFamily( const ustring& family )
{
	_family = family;
}
cairo_font_slant_t TextStyle::slant() const
{
	return _slant;
}
void TextStyle::setSlant( const cairo_font_slant_t& slant )
{
	_slant = slant;
}
void TextStyle::setSlant( const ustring& slant )
{
	if ( slant == "italic" )
		_slant = CAIRO_FONT_SLANT_ITALIC;
	else if ( slant == "oblique" )
		_slant = CAIRO_FONT_SLANT_OBLIQUE;
	else
		_slant = CAIRO_FONT_SLANT_NORMAL;
}
cairo_font_weight_t TextStyle::weight() const
{
	return _weight;
}
void TextStyle::setWeight( const cairo_font_weight_t& weight )
{
	_weight = weight;
}
void TextStyle::setWeight( const ustring& weight )
{
	if ( weight == "bold" )
		_weight = CAIRO_FONT_WEIGHT_BOLD;
	else
		_weight = CAIRO_FONT_WEIGHT_NORMAL;
}
bool TextStyle::underline() const
{
	return _underline;
}
void TextStyle::setUnderline( bool underline )
{
	_underline = underline;
}
TextStyle::Capitalization TextStyle::cap() const
{
	return _cap;
}
void TextStyle::setCap( const Capitalization& cap )
{
	_cap = cap;
}
void TextStyle::setCap( const ustring& cap )
{
	if ( cap == "nocap" )
		_cap = TextStyle::NoCap;
	else if ( cap == "cap" )
		_cap = TextStyle::Cap;
	else if ( cap == "smallcap" )
		_cap = TextStyle::SmallCap;
}
double TextStyle::size() const
{
	return _size;
}
void TextStyle::setSize( double size )
{
	_size = size;
}
Color TextStyle::color() const
{
	return _color;
}
void TextStyle::setColor( const Color& color )
{
	_color = color;
}
Anchor TextStyle::anchor() const
{
	return _anchor;
}
void TextStyle::setAnchor( const Anchor& anchor )
{
	_anchor = anchor;
}
void TextStyle::setAnchor( const Key::Keys& anchor )
{
	_anchor = anchor;
}
void TextStyle::setAnchor( const ustring& anchor )
{
	_anchor = Key::toKeys(anchor);
}
TextStyle::Align TextStyle::align() const
{
	return _align;
}
void TextStyle::setAlign( const Align& align )
{
	_align = align;
}
void TextStyle::setAlign( const ustring& /*align*/ )
{
}
TextStyle::ScriptOpt TextStyle::scriptOpt() const
{
	return _scriptopt;
}
void TextStyle::setScriptOpt( const ScriptOpt& scriptopt )
{
	_scriptopt = scriptopt;
}
void TextStyle::setScriptOpt( const ustring& /*scriptopt*/ )
{
}
TextStyle TextStyle::capStyle() const
{
	TextStyle cap_style = *this;
	cap_style.setSize( 0.8*this->size()+0.1 );
	return cap_style;
}
double TextStyle::rotation() const
{
	return _rotation;
}
void TextStyle::setRotation( double rotation )
{
	_rotation = rotation;
}
/**********************************************/
/**********************************************/
/**********************************************/
StyledText::StyledText()
{

}
StyledText::StyledText( const ustring& text, const TextStyle& style )
{
	_style = style;
	StyledText::separateTextAndStyle( text, _text, _style );
//	if ( text.substr(0,7) == "<style=" )
//	{
//		unsigned long end_ind = text.find('>', 6);
//		ustring style_str = text.substr(7, end_ind-7 );
//		_text = text.remove("</style>").substr(end_ind+1);
//		_style.set( style_str );
//	}
//	else
//		_text = text;
}
void StyledText::separateTextAndStyle( const ustring& formattedText, ustring& text, TextStyle& style )
{
	if ( formattedText.substr(0,7) == "<style=" )
	{
		unsigned long end_ind = formattedText.find('>', 6);
		ustring style_str = formattedText.substr(7, end_ind-7 );
		text = formattedText.remove("</style>").substr(end_ind+1);
		style.set( style_str );
	}
	else
		text = formattedText;
}
void StyledText::buildStyledtextList( const ustring& formattedText, const TextStyle& globalStyle, UList<StyledText>& styled_texts )
{
	for ( const ustring& text_part : formattedText.split("</style>") )
	{
		ustring text = "";
		TextStyle style = globalStyle;
		StyledText::separateTextAndStyle( text_part, text, style );

		unsigned long ind = text_part.find("<style=");
		if ( ind != std::string::npos && ind > 0 )
		{
//			styled_texts.append( StyledText( text_part.substr(0,ind-1), globalStyle ) );
			StyledText::separateSmallCaps( StyledText( text_part.substr(0,ind-1), globalStyle ), styled_texts );
			StyledText::separateSmallCaps( StyledText( text_part.substr(ind), globalStyle ), styled_texts );

//			styled_texts.append( StyledText( text_part.substr(ind), globalStyle ) );
		}
		else
			StyledText::separateSmallCaps( StyledText( text_part, globalStyle ), styled_texts );
//			styled_texts.append( StyledText( text_part, globalStyle ) );
	}
}
void StyledText::separateSmallCaps( const StyledText& styledText, UList<StyledText>& styled_texts )
{
	StyledText tmp = styledText;
	if ( tmp.style().cap() == TextStyle::Cap )
	{
		tmp.setText( tmp.text().toUpper() );
		styled_texts.append( tmp );
	}
	else if ( tmp.style().cap() == TextStyle::SmallCap )
	{
		UList<ustring> dcaps = tmp.text().decomposeCap();
		for ( ustring te : dcaps )
		{
			if ( !te.empty() && 'a' <= te.at(0) && te.at(0) <= 'z' )
				styled_texts.append( StyledText(te.toUpper(), tmp.style().capStyle()) );
			else
				styled_texts.append( StyledText(te, tmp.style()) );
		}
	}
	else
		styled_texts.append( styledText );
}
ustring StyledText::text() const
{
	return _text;
}
void StyledText::setText( const ustring& text )
{
	_text = text;
}
TextStyle StyledText::style() const
{
	return _style;
}
void StyledText::setStyle( const TextStyle& style )
{
	_style = style;
}
Point StyledText::pos() const
{
	return _pos;
}
Point StyledText::textPos() const
{
	return _text_pos;
}
void StyledText::setPos( const Point& pos )
{
	_pos = pos;
	_text_pos = pos;
}
Anchor StyledText::anchor() const
{
	return _anchor;
}
void StyledText::setAnchor( const Anchor& anchor )
{
	_anchor = anchor;
}
FontMetrics StyledText::metrics() const
{
	return _metrics;
}
Rect StyledText::boundingRect() const
{
	return _boundingRect;
}
void StyledText::prepare( Cairo* drawing )
{
	// Get metrics
	_metrics = style().metrics( text(), drawing );
	//
	// Set X Position
	if ( style().anchor().isHCenter() )
		_text_pos.setX( _pos.x() - 0.5*_metrics.width() );
	else if ( style().anchor().isRight()  )
		_text_pos.setX( _pos.x() - _metrics.width() );
	// Set X & Width BoundingRect
	_boundingRect.setX( _text_pos.x() );
	_boundingRect.setWidth( _metrics.width() );
	//
	// Set Y position
	if ( style().anchor().isTop() )
		_text_pos.setY( _pos.y() + _metrics.ascent() );
	else if ( style().anchor().isVCenter() )
		_text_pos.setY( _pos.y() + _metrics.ascent() - 0.5*_metrics.height() );
	else if ( style().anchor().isBottom() )
		_text_pos.setY( _pos.y() - _metrics.descent() );
	// Set Y & Height BoundingRect
	_boundingRect.setY( _text_pos.y()-_metrics.ascent() );
	_boundingRect.setHeight( _metrics.height() );
	//
}
/**********************************************/
/**********************************************/
/**********************************************/
LineStyle::LineStyle()
{

}
UList<double> LineStyle::dash() const
{
	return _dash;
}
void LineStyle::setDash( const UList<double>& dash )
{
	_dash = dash;
}
double LineStyle::dashOffset() const
{
	return _dash_offset;
}
void LineStyle::setDashOffset( double dashoffset )
{
	_dash_offset = dashoffset;
}
double LineStyle::width() const
{
	return _width;
}
void LineStyle::setWidth( double width )
{
	_width = width;
}
Color LineStyle::color() const
{
	return _color;
}
void LineStyle::setColor( const Color& color )
{
	_color = color;
}
cairo_line_join_t LineStyle::join() const
{
	return _join;
}
void LineStyle::setJoin( const cairo_line_join_t& join )
{
	_join = join;
}
void LineStyle::setJoin( const ustring& join )
{
	Key::Keys key = Key::toKeys(join);
	if ( key == Key::Round )
		_join = CAIRO_LINE_JOIN_ROUND;
	else if ( key == Key::Miter )
		_join = CAIRO_LINE_JOIN_MITER;
	else
		_join = CAIRO_LINE_JOIN_BEVEL;
}
double LineStyle::miterLimit() const
{
	return _miter_limit;
}
void LineStyle::setMiterLimit( double miter_limit )
{
	_miter_limit = miter_limit;
}
cairo_line_cap_t LineStyle::cap() const
{
	return _cap;
}
void LineStyle::setCap( const cairo_line_cap_t& cap )
{
	_cap = cap;
}
void LineStyle::setCap( const ustring& cap )
{
	Key::Keys key = Key::toKeys(cap);
	if ( key == Key::Round )
		_cap = CAIRO_LINE_CAP_ROUND;
	else if ( key == Key::Square )
		_cap = CAIRO_LINE_CAP_SQUARE;
	else
		_cap = CAIRO_LINE_CAP_BUTT;
}
double LineStyle::gap() const
{
	return _gap;
}
void LineStyle::setGap( double gap )
{
	_gap = gap;
}
/**********************************************/
/**********************************************/
/**********************************************/
FillStyle::FillStyle()
{

}
FillStyle::Type FillStyle::type() const
{
	return _type;
}
void FillStyle::setType( const FillStyle::Type& type )
{
	_type = type;
}
Color FillStyle::color() const
{
	return _color;
}
void FillStyle::setColor( const Color& color )
{
	_color = color;
}
cairo_pattern_t* FillStyle::linearPattern() const
{
	return _linear_gradient;
}
void FillStyle::setLinearPattern( cairo_pattern_t* linear_gradient )
{
	_linear_gradient = linear_gradient;
}
cairo_pattern_t* FillStyle::radialPattern() const
{
	return _radial_gradient;
}
void FillStyle::setRadialPattern( cairo_pattern_t* radial_gradient )
{
	_radial_gradient = radial_gradient;
}
/**********************************************/
/**********************************************/
/**********************************************/
ShapeStyle::ShapeStyle()
{

}
FillStyle ShapeStyle::fill() const
{
	return _fill;
}
void ShapeStyle::setFill( const FillStyle& fill )
{
	_fill = fill;
}
LineStyle ShapeStyle::line() const
{
	return _line;
}
void ShapeStyle::setLine( const LineStyle& line )
{
	_line = line;
}
Key::Keys ShapeStyle::anchor() const
{
	return _anchor;
}
void ShapeStyle::setAnchor( const Key::Keys& anchor )
{
	_anchor = anchor;
}
/**********************************************/
/**********************************************/
/**********************************************/
Symbol::Symbol()
{
}
Symbol::Symbol( const Symbol::Symbols& type )
{
	_type = type;
}
Symbol::Symbols Symbol::type() const
{
	return _type;
}
void Symbol::setType( const Symbol::Symbols& type )
{
	_type = type;
}
void Symbol::setType( const ustring& type )
{
	if ( type == "cercle" )
		_type = Symbol::Cercle;
	else if ( type == "halfcercle" )
		_type = Symbol::HalfCercle;
	else if ( type == "square" )
		_type = Symbol::Square;
	else if ( type == "triangle" )
		_type = Symbol::Triangle;
	else if ( type == "polygon" )
		_type = Symbol::Polygon;
	else if ( type == "diamond" )
		_type = Symbol::Polygon;
	else if ( type == "hourglass" )
		_type = Symbol::Hourglass;
	else if ( type == "boomerang" )
		_type = Symbol::Boomerang;
	else if ( type == "star" )
		_type = Symbol::Star;
	else if ( type == "plus" )
		_type = Symbol::Plus;
	else if ( type == "minus" )
		_type = Symbol::Minus;
	else if ( type == "line" )
		_type = Symbol::Line;
	else if ( type == "text" )
		_type = Symbol::Text;
}
ustring Symbol::option() const
{
	return _option;
}
void Symbol::setOption( const ustring& option )
{
	_option = option;
}
Size Symbol::size() const
{
	return _size;
}
void Symbol::setSize( const Size& size )
{
	_size = size;
}
void Symbol::setSize( const ustring& size )
{
	_size = Size(size);
}
Size Symbol::optionSize() const
{
	return _option_size;
}
void Symbol::setOptionSize( const Size& size )
{
	_option_size = size;
}
void Symbol::setOptionSize( const ustring& size )
{
	_option_size = Size(size);
}
double Symbol::beginAngle() const
{
	return _begin_angle;
}
void Symbol::setBeginAngle( double angle )
{
	_begin_angle = angle;
}
double Symbol::endAngle() const
{
	return _end_angle;
}
void Symbol::setEndAngle( double angle )
{
	_end_angle = angle;
}
double Symbol::rotation() const
{
	return _rotation;
}
void Symbol::setRotation( double rotation )
{
	_rotation = rotation;
}
double Symbol::covering() const
{
	return _covering;
}
void Symbol::setCovering( double covering )
{
	_covering = covering;
}
/**********************************************/
/**********************************************/
/**********************************************/
Label::Label()
{
}
Label::Label( const ustring& text, const Point& pos, const Anchor& anchor )
{
	setText(text);
	setPos(pos);
	setAnchor(anchor);
}
ustring Label::text() const
{
	return _text;
}
void Label::setText( const ustring& text )
{
	_text = text;
}
Point Label::pos() const
{
	return _pos;
}
Point Label::textPos() const
{
	return _text_pos;
}
void Label::setPos( const Point& pos )
{
	_pos = pos;
	_text_pos = pos;
}
Anchor Label::anchor() const
{
	return _anchor;
}
void Label::setAnchor( const Anchor& anchor )
{
	_anchor = anchor;
}
FontMetrics Label::metrics() const
{
	return _metrics;
}
Rect Label::boundingRect() const
{
	return _boundingRect;
}
void Label::prepare( cairo_t* drawing, const TextStyle& style )
{
	// Get metrics
	if ( style.cap() == TextStyle::SmallCap )
		_metrics = style.capStyle().metrics( text(), drawing );
	else
		_metrics = style.metrics( text(), drawing );

	//
	// Set X Position
	if ( style.anchor().isHCenter() )
		_text_pos.setX( _pos.x() - 0.5*_metrics.width() );
	else if ( style.anchor().isRight()  )
		_text_pos.setX( _pos.x() - _metrics.width() );
	// Set X & Width BoundingRect
	_boundingRect.setX( _text_pos.x() );
	_boundingRect.setWidth( _metrics.width() );
	//
	// Set Y position
//	_text_pos.setY( _pos.y() );
	if ( style.anchor().isTop() )
		_text_pos.setY( _pos.y() + _metrics.ascent() );
	else if ( style.anchor().isVCenter() )
		_text_pos.setY( _pos.y() + _metrics.ascent() - 0.5*_metrics.height() );
	else if ( style.anchor().isBottom() )
		_text_pos.setY( _pos.y() - _metrics.descent() );
	// Set Y & Height BoundingRect
	_boundingRect.setY( _text_pos.y()-_metrics.ascent() );
	_boundingRect.setHeight( _metrics.height() );
	//
}
void Label::prepare( Cairo* drawing, const TextStyle& style )
{
	// Get metrics
	if ( style.cap() == TextStyle::SmallCap )
		_metrics = style.capStyle().metrics( text(), drawing );
	else
		_metrics = style.metrics( text(), drawing );
	//
	// Set X Position
	if ( style.anchor().isHCenter() )
		_text_pos.setX( _pos.x() - 0.5*_metrics.width() );
	else if ( style.anchor().isRight()  )
		_text_pos.setX( _pos.x() - _metrics.width() );
	// Set X & Width BoundingRect
	_boundingRect.setX( _text_pos.x() );
	_boundingRect.setWidth( _metrics.width() );
	//
	// Set Y position
	if ( style.anchor().isTop() )
		_text_pos.setY( _pos.y() + _metrics.ascent() );
	else if ( style.anchor().isVCenter() )
		_text_pos.setY( _pos.y() + _metrics.ascent() - 0.5*_metrics.height() );
	else if ( style.anchor().isBottom() )
		_text_pos.setY( _pos.y() - _metrics.descent() );
	// Set Y & Height BoundingRect
	_boundingRect.setY( _text_pos.y()-_metrics.ascent() );
	_boundingRect.setHeight( _metrics.height() );
	//
}
void Label::draw( Cairo& drawing, const TextStyle& style )
{
	drawing.setTextStyle( style );
	drawing.drawSimpleText( text(), _text_pos, style );
}
/**********************************************/
/**********************************************/
/**********************************************/


Format::Format()
{
}

Format::Format(const ustring& format)
{
	UList<ustring> args = format.split(";");
	for ( const ustring& arg : args )
		setOption( arg );
}

void Format::setOption(const ustring option)
{
	UList<ustring> values = option.split(":");
	if ( !values.isEmpty() )
	{
		ustring first_val = values.first();
		// ty : _type
		if ( first_val == "ty" && values.size() > 1 )
		{
			setType( values.at(1) );
		}
		// fs : _forceSign
		else if ( first_val == "fs" )
		{
			_forceSign = true;
		}
		// dm :_decimalMarker
		else if ( first_val == "dm" && values.size() > 1 )
		{
			_decimalMarker = values.at(1);
		}
		// nd : _nbDecimal
		else if ( first_val == "nd" && values.size() > 1 )
		{
			_nbDecimal = std::stoi( values.at(1) );
		}
		// fd : _forceDecimal
		else if ( first_val == "fd" )
		{
			_forceDecimal = true;
		}
		// ni : _nbIntegerDigits
		else if ( first_val == "ni" && values.size() > 1 )
		{
			_nbIntegerDigits = std::stoi( values.at(1) );
		}
		// pb : _productBase
		else if ( first_val == "pb" && values.size() > 1 )
		{
			_productBase = values.at(1);
		}
		// pm :_productMarker
		else if ( first_val == "pm" && values.size() > 1 )
		{
			_productMarker = values.at(1);
		}
		// eb : _exponentBase
		else if ( first_val == "eb" && values.size() > 1 )
		{
			_exponentBase = values.at(1);
		}
		// em :_exponentMaker
		else if ( first_val == "em"  && values.size() > 1)
		{
			_exponentMaker = values.at(1);
		}
		// fe : _fixedExponent
		else if ( first_val == "fe" && values.size() > 1 )
		{
			_fixedExponent = std::stoi( values.at(1) );
		}
		// fes : _forceExponentSign
		else if ( first_val == "fes" )
		{
			_forceExponentSign = true;
		}
		// ts : _tightSpacing
		else if ( first_val == "ts" )
		{
			_tightSpacing = true;
		}
		// hum : _hideUnityMantissa
		else if ( first_val == "hum" )
		{
			_hideUnityMantissa = true;
		}
		// hze : _hideZeroExponent
		else if ( first_val == "hze" )
		{
			_hideZeroExponent = true;
		}
		// sc : _scale
		else if ( first_val == "sc" && values.size() > 1 )
		{
			_scale = std::stod( values.at(1) );
		}
		// tf : _timeFormat
		else if ( first_val == "tf" && values.size() > 1 )
		{
			_timeFormat = std::stod( values.at(1) );
		}
	}
}

void Format::setType(const Type& type)
{
	_type = type;
	if ( _type == Format::Scientific || _type == Format::Product )
		_nbIntegerDigits = 0;
}

void Format::setType(const ustring& type)
{
	if ( type == "n" )
		setType( Format::Number );
	else if ( type == "p" )
		setType( Format::Product );
	else if ( type == "s" )
		setType( Format::Scientific );
	else if ( type == "@" )
		setType( Format::Text );
	//		else if ( type == "d" )
	//			setType( Format::Date );
	else if ( type == "t" )
		setType( Format::Time );
	//		else if ( type == "dt" )
	//			setType( Format::TimeDate );
	else
		setType( Format::Default );
}

ustring Format::convert(double number, const Format& format)
{
	return (format.convert( number ));
}

ustring Format::convert(double number) const
{
	number = number/_scale;
	int _exponent = 0;
	if ( _type == Format::Number )
	{

	}
	if ( _type == Format::Scientific )
	{
		double base;
		if ( _exponentBase == "pi" )
			base = M_PI;
		else if ( _exponentBase == "e" )
			base = M_E;
		else
			base = stod(_exponentBase);
		if ( _fixedExponent != 0 )
		{
			number = number/(pow(base, _fixedExponent));
			_exponent = _fixedExponent;
		}
		else
		{
			double tmp_nb = number;
			if ( tmp_nb < 0 )
				tmp_nb *= -1;
			while ( tmp_nb > 10 )
			{
				tmp_nb = tmp_nb/base;
				++_exponent;
			}
			while ( tmp_nb < 1 )
			{
				tmp_nb *= base;
				--_exponent;
			}
			number = tmp_nb;
		}
	}
	else if ( _type == Format::Product )
	{
		double base;
		if ( _productBase == "pi" )
			base = M_PI;
		else if ( _productBase == "e" )
			base = M_E;
		else
			base = stod(_productBase);
		number = number/base;
	}
	ustring tmp = std::to_string( number );
	int ind = tmp.find('.');
	ustring integers = tmp.substr(0,ind);
	ustring decimals = tmp.substr(ind+1);
	if ( _nbIntegerDigits > (int)integers.size() )
	{
		for ( int it = integers.size(); it < _nbIntegerDigits; ++it )
			integers = "0" + integers;
	}
	if ( _forceDecimal )
	{
		for ( int it = decimals.size(); it < _nbDecimal; ++it )
			decimals += "0";
		while ( (int)decimals.size() > _nbDecimal )
			decimals = decimals.replace(decimals.size()-1, 1, "");
	}
	tmp = integers + _decimalMarker + decimals;
	if ( _forceSign && number >= 0)
		tmp = '+' + tmp;
	ustring pm = _productMarker;
	if ( pm == "dot" )
	{
		pm = "·";
	}
	else if ( pm == "times" )
	{
		pm = "×";
	}
	if ( !_tightSpacing )
	{
		pm = " " + pm + " ";
	}
	if ( _type == Format::Scientific )
	{
		bool unityIsHidden = false;
		if ( _hideUnityMantissa && abs(stoi(integers)) == 1 && stoi(decimals) == 0 )
		{
			tmp = "";
			if ( number < 0)
				tmp = '-' + tmp;
			else if ( _forceSign && number >= 0)
				tmp = '+' + tmp;
			unityIsHidden = true;
		}
		if ( !(_hideZeroExponent && _exponent == 0) )
		{
			if ( !unityIsHidden )
				tmp += pm;
			tmp += _exponentBase +"^";
			if ( _forceExponentSign && _exponent >= 0 )
				tmp += "+";
			tmp += std::to_string(_exponent);
		}
	}
	else if ( _type == Format::Product )
	{
		tmp += pm + _productBase;
	}
	else if ( _type == Format::Time )
	{
		auto t = std::time(nullptr);
		auto tm = *std::localtime(&t);
		//			tm.tm_mday = 299823;
		std::ostringstream oss;
		oss << std::put_time(&tm, _timeFormat.c_str());
		return oss.str();
		//	std::time_t t = std::mktime(&tm);
		//			oss << "UTC:   " << std::put_time(std::gmtime(&t), "%T") << '\n';
		//			oss << "local: " << std::put_time(std::localtime(&t), "%T") << '\n';
		////	%a	Abbreviated weekday name *	Thu
		////	%A	Full weekday name * 	Thursday
		////	%b	Abbreviated month name *	Aug
		////	%B	Full month name *	August
		////	%C	Year divided by 100 and truncated to integer (00-99)	20
		////	%d	Day of the month, zero-padded (01-31)	23
		////	%D	Short MM/DD/YY date, equivalent to %m/%d/%y	08/23/01
		////	%e	Day of the month, space-padded ( 1-31)	23
		////	%F	Short YYYY-MM-DD date, equivalent to %Y-%m-%d	2001-08-23
		////	%g	Week-based year, last two digits (00-99)	01
		////	%G	Week-based year	2001
		////	%h	Abbreviated month name * (same as %b)	Aug
		////	%j	Day of the year (001-366)	235
		////	%m	Month as a decimal number (01-12)	08
		////	%u	ISO 8601 weekday as number with Monday as 1 (1-7)	4
		////	%U	Week number with the first Sunday as the first day of week one (00-53)	33
		////	%V	ISO 8601 week number (01-53)	34
		////	%w	Weekday as a decimal number with Sunday as 0 (0-6)	4
		////	%W	Week number with the first Monday as the first day of week one (00-53)	34
		////	%x	Date representation *	08/23/01
		////	%y	Year, last two digits (00-99)	01
		////	%Y	Year	2001
	}
	//		else if ( _type == Format::Time )
	//		{
	//			////	%H	Hour in 24h format (00-23)	14
	//			////	%I	Hour in 12h format (01-12)	02
	//			////	%M	Minute (00-59)	55
	//			////	%p	AM or PM designation	PM
	//			////	%r	12-hour clock time *	02:55:02 pm
	//			////	%R	24-hour HH:MM time, equivalent to %H:%M	14:55
	//			////	%S	Second (00-61)	02
	//			////	%T	ISO 8601 time format (HH:MM:SS), equivalent to %H:%M:%S	14:55:02
	//			////	%X	Time representation *	14:55:02
	//		}
	//		else if ( _type == Format::TimeDate )
	//		{
	//			////	%c	Date and time representation *	Thu Aug 23 14:55:02 2001
	//			////	%z	ISO 8601 offset from UTC in timezone (1 minute=1, 1 hour=100)
	//			////	If timezone cannot be determined, no characters	+100
	//			////	%Z	Timezone name or abbreviation *
	//			////	If timezone cannot be determined, no characters	CDT
	//			////	%%	A % sign	%
	//		}
	return tmp;
}

ustring Format::convert(int number)
{
	ustring tmp = std::to_string( number );
	if ( _nbIntegerDigits > (int)tmp.size() )
	{
		for ( int it = tmp.size(); it < _nbIntegerDigits; ++it )
			tmp = _integerFiller + tmp;
	}
	//		if ( _forceDecimal && _nbDecimal > 0 )
	//		{
	//			tmp += _decimalMaker;
	//			for ( int it = 0; it < _nbDecimal; ++it )
	//				tmp += "0";
	//		}
	if ( _forceSign && number >= 0)
		tmp = '+' + tmp;
	return tmp;
}
