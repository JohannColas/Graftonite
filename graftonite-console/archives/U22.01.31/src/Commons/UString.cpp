#include "UString.h"
/**********************************************/
#include "Styles.h"
#include "Key.h"
/**********************************************/
/**********************************************/
ustring::ustring() : std::string()
{

}
/**********************************************/
ustring::ustring( const double d ) : std::string()
{
	ustring strd = std::to_string(d);
	char dec = 0;
	if ( strd.contains(".") ) dec = '.';
	else if ( strd.contains(",") ) dec = ',';
	while ( strd.back() == '0' )
		strd.pop_back();
	if ( strd.back() == dec )
		strd.pop_back();
	if ( strd == "-0" )
		strd = "0";
	(*this) = strd;
}
ustring::ustring( const char c ) : std::string()
{
	(*this) += c;
}
/**********************************************/
ustring::ustring( const int i ) : std::string()
{
	ustring strd = std::to_string(i);
	if ( strd == "-0" )
		strd = "0";
	(*this) = strd;
}
/**********************************************/
ustring::ustring( const unsigned u ) : std::string()
{
	ustring strd = std::to_string(u);
	if ( strd == "-0" )
		strd = "0";
	(*this) = strd;
}
/**********************************************/
ustring::ustring( const long l ) : std::string()
{
	ustring strd = std::to_string(l);
	if ( strd == "-0" )
		strd = "0";
	(*this) = strd;
}
/**********************************************/
ustring::ustring( const char* c_str ) : std::string(c_str)
{

}
/**********************************************/
ustring::ustring( const std::string& str ) : std::string(str)
{

}
/**********************************************/
ustring::ustring( const ustring& str ) : std::string(str)
{

}
/**********************************************/
/**********************************************/
bool ustring::contains( const std::string& str ) const
{
	return (this->find(str) != std::string::npos);
}
/**********************************************/
/**********************************************/
bool ustring::contains( const char* c_str ) const
{
	return (this->find(c_str) != std::string::npos);
}
UList<int> ustring::indexesOf( const ustring& str ) const
{
	UList<int> indexes;
	unsigned long ind = -1;
	do
	{
		ind = this->find( str, ind+1 );
		if ( ind != std::string::npos )
			indexes.append( ind );
	}
	while ( ind != std::string::npos );
	return indexes;
}
/**********************************************/
/**********************************************/
ustring ustring::remove( const ustring& str ) const
{
	ustring tmp;
	for ( unsigned long it = 0; it < this->length(); ++it )
	{
		ustring tmp2 = this->substr(it, str.size());
		if ( tmp2 != str )
			tmp += this->at(it);
		else
			it += str.size()-1;
	}
	return tmp;
}

ustring ustring::right(int n) const
{
	ustring tmp;
	for ( int it = 0; it < n; ++it  )
		tmp += this->at( this->size()-1-it );
	return tmp;
}

bool ustring::isInteger() const
{
	for ( char c : *this )
		if ( c < '0' || '9' < c )
			return false;
	return true;
}

bool ustring::isDouble() const
{
	for ( char c : *this )
		if ( (c < '0' || '9' < c) && c != '.' )
			return false;
	return this->contains(".");
}

double ustring::toDouble() const
{
	double d = 0;
	if ( !empty() )
		d = stod(*this);
	return d;
}

std::deque<double> ustring::toDoubleList() const
{
	UList<ustring> lst = this->split(",");
	std::deque<double> d_lst;
	for ( const ustring& el : lst )
		d_lst.push_back( el.toDouble() );
	return d_lst;
}

bool ustring::toBool() const
{
	if ( this->isInteger() )
		return stoi(*this);
	else if ( *this == "true" || *this == "t" )
		return true;
	return false;
}

int ustring::toInt() const
{
	return stoi(*this);
}

unsigned int ustring::toUInt() const
{
	return stoi(*this);
}

long ustring::toLong() const
{
	return stol(*this);
}

unsigned long ustring::toULong() const
{
	return stoul(*this);
}

char ustring::toChar() const
{
	return this->at(0);
}

unsigned char ustring::toUChar() const
{
	return 0;
}
/**********************************************/
/**********************************************/
Color ustring::toColor() const
{
	return Color(*this);
}

ustring ustring::toUpper() const
{
	ustring str;
	for ( char c : *this )
	{
		if ( 'a' <= c &&  c <= 'z' )
			str += (c+'A'-'a');
		else
			str += c;
	}
	return str;
}

ustring ustring::toFirstUpper() const
{
	ustring str;
	for ( unsigned it = 0; it < this->size(); ++it)
	{
		char c = this->at(it);
		if ( it == 0 && 'a' <= c &&  c <= 'z' )
			str += (c+'A'-'a');
		else
			str += c;
	}
	return str;
}

ustring ustring::toLower() const
{
	ustring str;
	for ( char c : *this )
	{
		if ( 'A' <= c &&  c <= 'Z' )
			str += (c+'a'-'A');
		else
			str += c;
	}
	return str;
}
/**********************************************/
/**********************************************/
UList<ustring> ustring::split( const ustring& separator, bool keepEmptyParts ) const
{
	UList<ustring> list;
	unsigned long ind0 = 0,
	        ind1,
	        strsz = separator.size();
	do
	{
		ind1 = this->find( separator, ind0 );
		std::string tmp = this->substr(ind0, ind1-ind0);

		if ( !tmp.empty() || (tmp.empty() && keepEmptyParts) )
			list.append(tmp);

		ind0 = ind1+strsz;
	}
	while ( ind1 != std::string::npos );

	return list;
}
UList<ustring> ustring::decomposeCap() const
{
	UList<ustring> list;
	ustring tmp;
	char old_c;
	for ( char c : *this )
	{
		if ( (('a' <= c && c <= 'z') && (old_c < 'a' || 'z' < old_c)) ||
		     (('a' <= old_c && old_c <= 'z') && (c < 'a' || 'z' < c)) )
		{
			if ( !tmp.empty() )
			{
				list.append( tmp );
				tmp.clear();
			}
		}
		tmp += c;
		old_c = c;
	}
	if ( !tmp.empty() )
		list.append( tmp );
	return list;
}

bool ustring::operator==(const UList<ustring>& list) const
{
	for ( const ustring& el : list )
		if ( *this == el || *this == ("-"+el) )
			return true;
	return false;
}

ustring& ustring::operator=(char c)
{
	(*this).clear();
	(*this) += c;
	return *this;
}

ustring& ustring::operator=(const char* s)
{
	(*this).clear();
	(*this) += s;
	return *this;
}

ustring& ustring::operator=(const std::string& str)
{
	(*this).clear();
	(*this) += str;
	return *this;
}

ustring& ustring::operator=(const ustring& str)
{
	(*this).clear();
	(*this) += str;
	return *this;
}
/**********************************************/
/**********************************************/
std::ostream& operator<<( std::ostream& os, const ustring& str )
{
	for ( auto c : str )
		os << c;
	return os;
}
/**********************************************/
/**********************************************/
