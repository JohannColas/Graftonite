#include "UConsole.h"
/**********************************************/
#include <unistd.h>
/**********************************************/
#include "Graftonite.h"
/**********************************************/
/**********************************************/
/**********************************************/
/* Terminal Core */
/**********************************************/
void UConsole::init()
{
	//	struct termios old_tio, new_tio;
	/* get the terminal settings for stdin */
	tcgetattr( STDIN_FILENO, &old_tio );
	/* we want to keep the old setting to restore them a the end */
	new_tio = old_tio;
	/* disable canonical mode (buffered i/o) and local echo */
	new_tio.c_lflag &=( ~ICANON & ~ECHO );
	/* set the new settings immediately */
	tcsetattr( STDIN_FILENO, TCSANOW, &new_tio );
	/*--------------------*/
}
/**********************************************/
void UConsole::close()
{
	/* restore the former settings */
	tcsetattr( STDIN_FILENO, TCSANOW, &old_tio );
	/*--------------------*/
}
/**********************************************/
void UConsole::exec( Graftonite* app )
{
	_app = app;
	/* --------------------------- */
	// Terminal Initialisation
	UConsole::init();
	/* --------------------------- */
	unsigned char c;
	ustring cur_cmd;
	int history_index = -1;
	int cursor_pos = 0;
	bool exit = false;
	UConsole::startCommandLine();
	do
	{
		if ( !cur_cmd.empty() )
			UConsole::startCommandLine();
		cursor_pos = 0;
		history_index = -1;
		cur_cmd.clear();
		do
		{
			c = getchar();
			if ( c == KEYS::ESC ) // escape key
			{
				char new_c = getchar();
				if ( new_c == 91 )
				{
					new_c = getchar();
					if ( new_c == KEYS::UP_ARROW ) // Up Arrow
					{
						UConsole::clearCommandLine();
						cur_cmd.clear();
						if ( history_index == -1 )
						{
							history_index = cmd_history.size()-1;
						}
						else
							--history_index;
						if ( history_index < 0 )
							history_index = 0;
						if ( history_index != -1 && cmd_history.size() > 0 )
						{
							cur_cmd = cmd_history.at(history_index);
							UConsole::clearCommandLine();
							UConsole::drawCommandLine( cur_cmd );
							cursor_pos = cur_cmd.length();
						}
					}
					else if ( new_c == KEYS::DOWN_ARROW ) // Down Arrow
					{
						UConsole::clearCommandLine();
						cur_cmd.clear();
						cursor_pos = 0;
						if ( history_index > -1 )
							++history_index;
						if ( history_index > (int)cmd_history.size()-1 )
							history_index = -1;
						if ( history_index != -1 && cmd_history.size() > 0 )
						{
							cur_cmd = cmd_history.at(history_index);
							UConsole::clearCommandLine();
							UConsole::drawCommandLine( cur_cmd );
							cursor_pos = cur_cmd.length();
						}
					}
					else if ( new_c == KEYS::RIGHT_ARROW ) // Right Arrow
					{
						if ( !cur_cmd.empty() )
						{
							++cursor_pos;
							if ( cursor_pos < (int)cur_cmd.length()+1 )
								UConsole::moveCursorForward(1);
							if ( cursor_pos > (int)cur_cmd.length() )
								cursor_pos = cur_cmd.length();
						}
					}
					else if ( new_c == KEYS::LEFT_ARROW )// Left Arrow
					{
						if ( !cur_cmd.empty() )
						{
							--cursor_pos;
							if ( cursor_pos >= 0 )
								UConsole::moveCursorBackward(1);
							if ( cursor_pos < 0 )
								cursor_pos = 0;
						}
					}
					else if ( new_c == KEYS::FN_RIGHT )// FN + Right Arrow
					{
						if ( !cur_cmd.empty() && cursor_pos < (int)cur_cmd.length() )
						{
							int diff = cur_cmd.length()-cursor_pos;
							if ( diff > 0 )
							{
								UConsole::moveCursorForward(diff);
								cursor_pos = cur_cmd.length();
							}
						}
					}
					else if ( new_c == KEYS::FN_LEFT )// FN + Left Arrow
					{
						if ( !cur_cmd.empty() && cursor_pos > 0 )
						{
								UConsole::moveCursorBackward(cursor_pos);
								cursor_pos=0;
						}
					}
					else if ( new_c == 51 )
					{
						new_c = getchar();
						if ( new_c == KEYS::DEL ) // delete key
						{
							if ( cursor_pos < (int)cur_cmd.length() )
							{
								int old_dif = cur_cmd.length() - cursor_pos;
								cur_cmd.erase( cursor_pos, 1 );
								UConsole::clearCommandLine();
								UConsole::drawCommandLine( cur_cmd );
								if ( old_dif > 0 )
								{
									UConsole::moveCursorBackward(old_dif);
									UConsole::moveCursorForward(1);
								}
							}
						}
					}
				}
				else // escape key
				{
					exit = true;
					break;
				}
			}
			else if ( c == KEYS::RETURN )
			{
				UConsole::normalColor();
				if ( _app != nullptr && !_app->runCommand( cur_cmd ) )
					exit = true;
				history_index = -1;
				cursor_pos = 0;
			}
			else if ( c == KEYS::BACKSPACE ) // BACKSPACE KEY
			{
				// Reset Current Cmd Index
				history_index = -1;
				//
				int old_dif = cur_cmd.length() - cursor_pos;
				--cursor_pos;
				if ( cur_cmd.length() > 0 && cursor_pos >= 0 )
				{
					cur_cmd.erase( cursor_pos, 1 );
					UConsole::clearCommandLine();
					UConsole::drawCommandLine( cur_cmd );
					UConsole::moveCursorBackward(old_dif);
				}
				if ( cursor_pos < 0 )
					cursor_pos = 0;
			}
			else
			{
				int old_dif = cur_cmd.length() - cursor_pos;
				cur_cmd.insert( cursor_pos, 1, c );
				++cursor_pos;
				UConsole::clearCommandLine();
				UConsole::drawCommandLine( cur_cmd );
				UConsole::moveCursorBackward(old_dif);
			}
		}
		while ( c != KEYS::RETURN );
	}
	while( cur_cmd.substr(0,4) != "exit" && !exit );

	UConsole::close();

}
/**********************************************/
void UConsole::clear()
{
	printf( "\033[2J" );
	printf( "\033[0;0f" );
}

UList<ustring> UConsole::CommandHistory()
{
	return cmd_history;
}
/**********************************************/
/**********************************************/
/**********************************************/
/* Cursor Positionning */
/**********************************************/
void UConsole::changeCursorCol( int delta )
{
	if ( delta < 0 )
		moveCursorBackward( delta );
	else
		moveCursorForward( delta );
}
/**********************************************/
void UConsole::moveCursorBackward( int delta )
{
	if ( delta > 0 )
		printf( "\033[%dD", delta );
}
/**********************************************/
void UConsole::moveCursorForward( int delta )
{
	if ( delta > 0 )
		printf( "\033[%dC", delta );
}
/**********************************************/
void UConsole::saveCursorPos()
{
	printf("\033[s"); // save cursor position
}
/**********************************************/
void UConsole::restoreCursorPos()
{
	printf("\033[u"); // restore cursor position
}

void UConsole::level1(const ustring& str)
{
	std::cout << "  " << str;
}

void UConsole::level2(const ustring& str)
{
	std::cout << "    " << str;
}

void UConsole::level3(const ustring& str)
{
	std::cout << "      " << str;
}
/**********************************************/
/**********************************************/
/**********************************************/
/* Colors */
/**********************************************/
void UConsole::normalColor()
{
	printf( "\033[0m" );
}
/**********************************************/
void UConsole::cmdInputColor()
{
	printf( "\033[1;36m" );
}
/**********************************************/
void UConsole::cmdColor()
{
	printf( "\x1B[93m" );
}
/**********************************************/
void UConsole::warningColor()
{
	printf( "\x1B[33m" );
}
/**********************************************/
void UConsole::errorColor()
{
	printf( "\x1B[31m" );
}
/**********************************************/
void UConsole::testColors()
{
	printf("\n");
	printf("\x1B[31mTexting\033[0m\t\t");//ROUGE
	printf("\x1B[32mTexting\033[0m\t\t");//VERT
	printf("\x1B[33mTexting\033[0m\t\t");//ORANGE
	printf("\x1B[34mTexting\033[0m\t\t");//BLEU
	printf("\x1B[35mTexting\033[0m\n");//VIOLET

	printf("\x1B[36mTexting\033[0m\t\t");//BLEUVERT
	printf("\x1B[36mTexting\033[0m\t\t");//BLEUVERT
	printf("\x1B[36mTexting\033[0m\t\t");//BLEUVERT
	printf("\x1B[37mTexting\033[0m\t\t");//BLANC
	printf("\x1B[93mTexting\033[0m\n");//JAUNE

	printf("\033[3;42;30mTexting\033[0m\t\t");
	printf("\033[3;43;30mTexting\033[0m\t\t");
	printf("\033[3;44;30mTexting\033[0m\t\t");
	printf("\033[3;104;30mTexting\033[0m\t\t");
	printf("\033[3;100;30mTexting\033[0m\n");

	printf("\033[3;47;35mTexting\033[0m\t\t");
	printf("\033[2;47;35mTexting\033[0m\t\t");
	printf("\033[1;47;35mTexting\033[0m\t\t");
	printf("\n");
}
/**********************************************/
/**********************************************/
/**********************************************/
/* Messages */
/**********************************************/
void UConsole::warning_message( const ustring& message )
{
	UConsole::warningColor();
	printf( "Warning: %s\n", message.c_str() );
	UConsole::normalColor();
}
/**********************************************/
void UConsole::error_message( const ustring& message )
{
	UConsole::errorColor();
	printf( "Error: %s\n", message.c_str() );
	UConsole::normalColor();
}
/**********************************************/
/**********************************************/
/**********************************************/
/* Dialogs */
/**********************************************/
bool UConsole::confirmationDialog( const ustring& message )
{
	std::cout << std::endl << message;
	char c = 'n';
	std::cin >> c;
	std::cout << c << std::endl;
	return (c == 'Y' || c == 'y');
}
/**********************************************/
/**********************************************/
/**********************************************/
/* Command Lines Format */
/**********************************************/
void UConsole::startCommandLine()
{
	UConsole::cmdInputColor();
	printf( "%s> ", _progname.c_str() );
	UConsole::normalColor();
	UConsole::saveCursorPos();
}
/**********************************************/
void UConsole::clearCommandLine()
{
	UConsole::restoreCursorPos();
	UConsole::deleteToEndLine();
}
/**********************************************/
void UConsole::drawCommandLine( const ustring& cmdline )
{
	bool first_space_found = false;
	UConsole::cmdColor();
	char last_c = '&';
	for ( char c : cmdline )
	{
		if ( c == ' ' && last_c != '&' && !first_space_found )
		{
			UConsole::normalColor();
			printf( " " );
			first_space_found = true;
		}
		else
		{
			printf( "%c", c );
		}
		if ( c == '&' && last_c == '&' )
		{
			UConsole::cmdColor();
			first_space_found = false;
		}
		last_c = c;
	}
	UConsole::normalColor();
}
/**********************************************/
void UConsole::deleteToEndLine()
{
	printf("\033[K"); // delete char from cursor position to end line
}
/**********************************************/
void UConsole::addCommandToHistory(const ustring& cmdline)
{
	cmd_history.append( cmdline );
}
/**********************************************/
/**********************************************/
/**********************************************/
