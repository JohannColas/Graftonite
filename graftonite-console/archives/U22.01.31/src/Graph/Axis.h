#ifndef AXIS_H
#define AXIS_H
/**********************************************/
#include "Element.h"
#include <vector>
/**********************************************/
/**********************************************/
/**********************************************/
class Axis : public Element
{
	Rect _frameRect = {100, 100, 1200, 800};
	ustring name = "axis";
	double _min = 0;
	double _max = 1;
//	Key _scale = Key::Linear;
	double _scale_option = 0;
	double scale( double rv )
	{
//		if ( _scale == Key::Log10 )
//			return log10(rv);
//		else if ( _scale == Key::Log )
//			return log(rv);
//		else if ( _scale == Key::LogX )
//			return log(rv)/log(_scale_option);
//		else if ( _scale == Key::Reciprocal )
//			return (1/rv);
//		else if ( _scale == Key::OffsetReciprocal )
//			return (1/(rv+_scale_option));
		return rv;
	}
	double a_x, b_x;
	double a_y, b_y;
	double _scaleA = 0.1;
	double _scaleB = 0;
	double _tickinterval = 0.1;
	double _minortickinterval = 0.05;
	Line _line;
	UList<Line> _ticks;
	UList<Line> _minor_ticks;
	LineStyle _line_style;
	UList<StyledText> _labels;
	TextStyle _Labels_style;
	StyledText _title;
	UList<StyledText> _titles;
	TextStyle _title_style;
	UList<Line> _girds;
	LineStyle _grids_style;
	UList<Line> _minor_grids;
	LineStyle _minor_grids_style;
	//
public:
	~Axis();
	Axis( Graph* parent = nullptr );
	Axis( const ustring& name, unsigned index, Graph* parent = nullptr, bool log = false );
	void init();
	/**********************************************/
	bool set( const Key& key, const ustring& value ) override;
	void prepare( Cairo* drawing ) override;
	void draw( Cairo* drawing ) override;
	/**********************************************/
	void CalculateTicksPosition( UList<double>& tickspos, UList<double>& minortickspos, UList<ustring>& labels, bool isYaxis = false );
	double round( double value );
	void CalculateScaleCoef();
	double GetGraphCoord( double coord );
	Point deviceCoordinates( double real_value );
	/**********************************************/
	Rect frameRect() const;
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // AXIS_H
