#include "Title.h"
/**********************************************/
/**********************************************/
/**********************************************/
Title::Title( Graph* parent )
	: Element(parent)
{
	init();
}
Title::Title(const ustring& name, unsigned index, Graph* parent, bool log)
	: Element(parent)
{
	init();
	Element::init( name, index, log );
}
void Title::init()
{
	_type = Key::Title;
}
/**********************************************/
/**********************************************/
bool Title::set( const Key& key, const ustring& value )
{
	if ( key == Key::Name ||
		 key == Key::Position ||
		 key == Key::Anchor ||
		 key == Key::Shift ||
		 key == Key::Template ||
		 key == Key::Text ||
		 key.isFontStyle() ||
		 key.isFillStyle(Key::Background) ||
		 key.isLineStyle({Key::Borders}, false) )
	{
		return Element::set( key, value );
	}
	return false;
}
/**********************************************/
/**********************************************/
void Title::prepare( Cairo* /*drawing*/ )
{
	Element::getTemplate();
	//
	// Building Entry Geometry
	// Except for entry part position
	double w_title = 0, h_title = 0;


	//
	// Set Title size
	Point margins = {5,5};
	get( margins, Key::Margins );
	_boundingRect = {0, 0, w_title+2*margins.x(), h_title+2*margins.y() };
	// Pos
	Point pos = {0,0};
	getPosition( pos );
	// Diff
	Key anchor = Key::TopLeft;
	get( anchor, Key::Anchor );
	Point diff = _boundingRect.point(anchor);
	// Shift
	Point shift = {0,0};
	get( shift, Key::Shift );
	pos -= diff - shift;
	// Set Title position
	_boundingRect.setPoint( pos );
	//
}
/**********************************************/
/**********************************************/
void Title::draw( Cairo* /*drawing*/ )
{

}
/**********************************************/
/**********************************************/
