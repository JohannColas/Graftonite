#ifndef LEGEND_H
#define LEGEND_H
/**********************************************/
#include "Element.h"
/**********************************************/
/**********************************************/
/**********************************************/
class Legend : public Element
{
	UList<UList<StyledText>> _all_entries;
	UList<Size> _entries_sizes;
//	Rect _legend_rect;
public:
	Legend( Graph* parent = nullptr );
	Legend( const ustring& name, unsigned index, Graph* parent = nullptr, bool log = false );
	void init();
	/**********************************************/
	bool set( const Key& key, const ustring& value ) override;
	void prepare( Cairo* drawing ) override;
	void draw( Cairo* drawing ) override;
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LEGEND_H
