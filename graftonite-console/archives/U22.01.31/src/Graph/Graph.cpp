#include "Graph.h"
/**********************************************/
#include "Commons/Cairo.h"
/**********************************************/
#include <iostream>
#include <fstream>
#include "Commons/UConsole.h"
//using namespace std;
/**********************************************/
/**********************************************/
Graph::~Graph()
{
	clearTemplates();
	clearLayers();
	clearFrames();
	clearAxes();
	clearPlots();
	clearLegends();
	clearTitles();
	clearData();
}
/**********************************************/
/**********************************************/
Graph::Graph()
	: Element()
{
	_type = Key::Graph;
	Element::set( Key::Name, "Graph");
	Element::set( Key::Width, "1600");
	Element::set( Key::Height, "1000");
	Element::set( Key::Format, "pdf");
	Element::set( Key(Key::Borders, Key::Color), "black" );
	Element::set( Key(Key::Borders, Key::Width), "6" );
	Element::set( Key(Key::Borders, Key::Join), "bevel" );
	Element::set( Key(Key::Background, Key::Color), "white" );

	Frame* layer = new Frame(this);
	layer->set( Key::Name, "frame1" );
	_frames.append( layer );
	layer->setIndex(0);
	//	Frame* layer1 = new Frame(this);
	//	layer1->set( "name", "This is a frame" );
	//	layer1->set( "width", "42" );
	//	layer1->set( "height", "1440" );
	//	_frames.append( layer1 );
	//	layer1->setIndex(1);
	Axis* axis = new Axis(this);
	axis->set( Key::Name, "AxisX" );
	axis->set( Key::Frame, "frame1" );
	//	axis->set( Key::Min, "0" );
	//	axis->set( Key::Max, "1" );
	axis->set( Key::Type, "X" );
	//	axis->set( Key::Position, "bottom" );
	axis->setIndex(0);
	_axes.append( axis );
	Axis* axisy = new Axis(this);
	axisy->set( Key::Name, "AxisY" );
	axisy->set( Key::Frame, "frame1" );
	//	axisy->set( Key::Min, "0" );
	//	axisy->set( Key::Max, "1" );
	axisy->set( Key::Type, "Y" );
	//	axisy->set( Key::Position, "center" );
	axisy->setIndex(1);
	_axes.append( axisy );

	Data* data = new Data( "data1", 0, this );
	data->set( Key::Coordinates, "0,0/0.1,0.2/0.3,0.5/0.5,0.7/0.7,0.43/0.9,0.85/1.0,0.85" );
	_data.append( data );

	Plot* plot = new Plot( "plot1", 0, this );
	plot->set( Key::Data1, "data1:c0" );
	plot->set( Key::Data2, "data1:c1" );
	_plots.append( plot );

	Legend* legend = new Legend( "leg1", 0, this );
	_legends.append( legend );
}
/**********************************************/
/**********************************************/
/* Element Management */
Element* Graph::element( const Key::Keys& type, const ustring& name )
{
	Element* el = nullptr;
	if ( type == Key::Graph )
		el = this;
	else if ( type == Key::Template )
		el = this->templateAt(name);
	else if ( type == Key::Layer )
		el = this->layer(name);
	else if ( type == Key::Frame )
		el = this->frame(name);
	else if ( type == Key::Axis )
		el = this->axis(name);
	else if (type == Key::Plot )
		el = this->plot(name);
	else if ( type == Key::Legend )
		el = this->legend(name);
	else if ( type == Key::Title )
		el = this->title(name);
	else if ( type == Key::Data )
		el = this->data(name);
	else
		el = _currentEl;
	return el;
}
Element* Graph::element( const ustring& ref	)
{
	if ( ref.empty() )
		return nullptr;

	ustring newref = ref;
	if ( newref.at(0) == '{' )
	{
		newref = newref.substr(1);
	}
	if ( newref.substr(0,2) == "\\#" )
	{
		newref = newref.substr(2);
	}
	if ( newref.substr(0,2) == "\\@" )
	{
		newref = newref.substr(2);
	}
	if ( newref.at(newref.size()-1) == '}' )
	{
		newref = newref.substr(0,newref.size()-1);
	}
	if ( newref.contains(";") )
	{
		newref = newref.substr(0,newref.find(";"));
	}
	UList<ustring> tmp = newref.split(":");
	if ( tmp.size() == 2)
		return element( tmp );
	else
		return element( Key::Plot, newref );
}
Element* Graph::element( UList<ustring>& args )
{
	if ( args.isEmpty() )
		return nullptr;
	Key::Keys type = Element::getType( args.first() );
	if ( type == Key::Graph )
	{
		args.removeFirst();
		return this;
	}
	else if ( type == Key::Current )
	{
		args.removeFirst();
		return _currentEl;
	}
	else if ( type == Key::Template )
	{
		args.removeFirst();
		if ( !args.isEmpty() )
			return this->templateAt(args.takeFirst());
	}
	else if ( type == Key::Layer )
	{
		args.removeFirst();
		if ( !args.isEmpty() )
			return this->layer(args.takeFirst());
	}
	else if ( type == Key::Frame )
	{
		args.removeFirst();
		if ( !args.isEmpty() )
			return this->frame(args.takeFirst());
	}
	else if ( type == Key::Axis )
	{
		args.removeFirst();
		if ( !args.isEmpty() )
			return this->axis(args.takeFirst());
	}
	else if (type == Key::Plot )
	{
		args.removeFirst();
		if ( !args.isEmpty() )
			return this->plot(args.takeFirst());
	}
	else if ( type == Key::Legend )
	{
		args.removeFirst();
		if ( !args.isEmpty() )
			return this->legend(args.takeFirst());
	}
	else if ( type == Key::Title )
	{
		args.removeFirst();
		if ( !args.isEmpty() )
			return this->title(args.takeFirst());
	}
	else if ( type == Key::Data )
	{
		args.removeFirst();
		if ( !args.isEmpty() )
			return this->data(args.takeFirst());
	}
	return _currentEl;
}
UList<Element*> Graph::elements( const Key::Keys& type )
{
	UList<Element*> els;
	if ( type == Key::Layer )
		for ( Layer* layer : this->layers() )
			els.append( layer );
	if ( type == Key::Frame || type == Key::NoKey )
		for ( Frame* frame : this->frames() )
			els.append( frame );
	if ( type == Key::Axis || type == Key::NoKey )
		for ( Axis* axis : this->axes() )
			els.append( axis );
	if (type == Key::Plot || type == Key::NoKey )
		for ( Plot* plot : this->plots() )
			els.append( plot );
	if ( type == Key::Legend || type == Key::NoKey )
		for ( Legend* legend : this->legends() )
			els.append( legend );
	if ( type == Key::Title || type == Key::NoKey )
		for ( Title* title : this->titles() )
			els.append( title );
	if ( type == Key::Data )
		for ( Data* data : this->datalst() )
			els.append( data );
	return els;
}
/**********************************************/
void Graph::newElement( const Key::Keys& type, const ustring& name, bool log )
{
	if ( type == Key::Graph )
	{
	}
	else if ( Key::isElementType(type) && !name.empty() )
	{
		if ( type == Key::Layer && (element(Key::Layer, name) == nullptr) )
			return _layers.append( new Layer(name, _layers.size(), this, log) );
		else if ( type == Key::Layer )
			return UConsole::error_message("Layer \""+name+"\" exists !");
		if ( type == Key::Frame && (element(Key::Frame, name) == nullptr) )
			return _frames.append( new Frame(name, _frames.size(), this, log) );
		else if ( type == Key::Frame )
			return UConsole::error_message("Frame \""+name+"\" exists !");
		if ( type == Key::Axis && (element(Key::Axis, name) == nullptr) )
			return _axes.append( new Axis(name, _axes.size(), this, log) );
		else if ( type == Key::Axis )
			return UConsole::error_message("Axis \""+name+"\" exists !");
		if ( type == Key::Plot && (element(Key::Plot, name) == nullptr) )
			return _plots.append( new Plot(name, _plots.size(), this, log) );
		else if ( type == Key::Plot )
			return UConsole::error_message("Plot \""+name+"\" exists !");
		if ( type == Key::Legend && (element(Key::Legend, name) == nullptr) )
			return _legends.append( new Legend(name, _legends.size(), this, log) );
		else if ( type == Key::Legend )
			return UConsole::error_message("Legend \""+name+"\" exists !");
		if ( type == Key::Title && (element(Key::Title, name) == nullptr) )
			return _titles.append( new Title(name, _titles.size(), this, log) );
		else if ( type == Key::Title )
			return UConsole::error_message("Title \""+name+"\" exists !");
		if ( type == Key::Data && (element(Key::Data, name) == nullptr) )
			return _data.append( new Data(name, _data.size(), this, log) );
		else if ( type == Key::Data )
			return UConsole::error_message("Data \""+name+"\" exists !");
	}
	else if ( name.empty() )
		UConsole::error_message("Name is empty !");
	else
		UConsole::error_message("Type is unknown !");
}
void Graph::removeElement( const Key::Keys& type, const ustring& name )
{
	if ( type == Key::Template )
	{
		if ( name.empty() )
		{
			if ( UConsole::confirmationDialog("Are you sure to delete all templates ? (y/n) : ") )
				clearTemplates();
		}
		else
			removeTemplate( templateAt(name) );
	}
	if ( type == Key::Layer || type == Key::NoType )
	{
		if ( name.empty() )
		{
			if ( UConsole::confirmationDialog("Are you sure to delete all layers ? (y/n) : ") )
				clearLayers();
		}
		else
			removeLayer( layer(name) );
	}
	if ( type == Key::Frame || type == Key::NoType )
	{
		if ( name.empty() )
		{
			if ( UConsole::confirmationDialog("Are you sure to delete all frames ? (y/n) : ") )
				clearFrames();
		}
		else
			removeFrame( frame(name) );
	}
	if ( type == Key::Axis || type == Key::NoType )
	{
		if ( name.empty() )
		{
			if ( UConsole::confirmationDialog("Are you sure to delete all axes ? (y/n) : ") )
				clearAxes();
		}
		else
			removeAxis( axis(name) );
	}
	if ( type == Key::Plot || type == Key::NoType )
	{
		if ( name.empty() )
		{
			if ( UConsole::confirmationDialog("Are you sure to delete all plots ? (y/n) : ") )
				clearPlots();
		}
		else
			removePlot( plot(name) );
	}
	if ( type == Key::Legend || type == Key::NoType )
	{
		if ( name.empty() )
		{
			if ( UConsole::confirmationDialog("Are you sure to delete all legends ? (y/n) : ") )
				clearLegends();
		}
		else
			removeLegend( legend(name) );
	}
	if ( type == Key::Title || type == Key::NoType )
	{
		if ( name.empty() )
		{
			if ( UConsole::confirmationDialog("Are you sure to delete all titles ? (y/n) : ") )
				clearTitles();
		}
		else
			removeTitle( title(name) );
	}
	if ( type == Key::Data || type == Key::NoType )
	{
		if ( name.empty() )
		{
			if ( UConsole::confirmationDialog("Are you sure to delete all data ? (y/n) : ") )
				clearData();
		}
		else
			removeData( data(name) );
	}
}
/**********************************************/
void Graph::setCurrent( UList<ustring>& args )
{
	_currentEl = element( args );
	if ( _currentEl == nullptr )
		return UConsole::error_message("Unable to define a current element !");
	std::cout << "Current element : " << Key::toString(_currentEl->type())
		 << " \"" << _currentEl->get(Key::Name) << "\"" << std::endl;
}
/**********************************************/
Element* Graph::templateAt( const ustring& name )
{
	for ( int it= 0; it < _templates.size(); ++it )
		if ( _templates.at(it)->get("name") == name || ustring(it) == name  )
			return _templates.at(it);
	return nullptr;
}
UList<Element*> Graph::templates()
{
	return _templates;
}
void Graph::removeTemplate( Element* templat )
{
	_templates.remove(templat);
	delete templat;
}
void Graph::clearTemplates()
{
	while ( !_templates.isEmpty() )
		delete _templates.takeFirst();
}
/**********************************************/
Layer* Graph::layer( const ustring& name )
{
	for ( int it= 0; it < _layers.size(); ++it )
		if ( _layers.at(it)->get("name") == name || ustring(it) == name  )
			return _layers.at(it);
	return nullptr;
}
UList<Layer*> Graph::layers()
{
	return _layers;
}
void Graph::removeLayer( Layer* layer )
{
	_layers.remove(layer);
	delete layer;
}
void Graph::clearLayers()
{
	while ( !_layers.isEmpty() )
		delete _layers.takeFirst();
}
/**********************************************/
Frame* Graph::frame( const ustring& name )
{
	for ( int it= 0; it < _frames.size(); ++it )
		if ( _frames.at(it)->get("name") == name || ustring(it) == name  )
			return _frames.at(it);
	return nullptr;
}
UList<Frame*> Graph::frames()
{
	return _frames;
}
void Graph::removeFrame( Frame* frame )
{
	_frames.remove(frame);
	delete frame;
}
void Graph::clearFrames()
{
	while ( !_frames.isEmpty() )
		delete _frames.takeFirst();
}
/**********************************************/
Axis* Graph::axis( const ustring& name )
{
	for ( int it= 0; it < _axes.size(); ++it )
		if ( _axes.at(it)->get("name") == name || ustring(it) == name  )
			return _axes.at(it);
	return nullptr;
}
UList<Axis*> Graph::axes()
{
	return _axes;
}
void Graph::removeAxis( Axis* axis )
{
	_axes.remove(axis);
	delete axis;
}
void Graph::clearAxes()
{
	while ( !_axes.isEmpty() )
		delete _axes.takeFirst();
}
/**********************************************/
Plot* Graph::plot( const ustring& name )
{
	for ( int it= 0; it < _plots.size(); ++it )
		if ( _plots.at(it)->get("name") == name || ustring(it) == name  )
			return _plots.at(it);
	return nullptr;
}
UList<Plot*> Graph::plots()
{
	return _plots;
}

void Graph::removePlot( Plot* plot )
{
	_plots.remove(plot);
	delete plot;
}
void Graph::clearPlots()
{
	while ( !_plots.isEmpty() )
		delete _plots.takeFirst();
}
/**********************************************/
Legend* Graph::legend( const ustring& name )
{
	for ( int it= 0; it < _legends.size(); ++it )
		if ( _legends.at(it)->get("name") == name || ustring(it) == name  )
			return _legends.at(it);
	return nullptr;
}
UList<Legend*> Graph::legends()
{
	return _legends;
}
void Graph::removeLegend( Legend* legend )
{
	_legends.remove(legend);
	delete legend;
}
void Graph::clearLegends()
{
	while ( !_legends.isEmpty() )
		delete _legends.takeFirst();
}
/**********************************************/
Title* Graph::title( const ustring& name )
{
	for ( int it= 0; it < _titles.size(); ++it )
		if ( _titles.at(it)->get(Key::Name) == name || ustring(it) == name  )
			return _titles.at(it);
	return 0;
}
UList<Title*> Graph::titles()
{
	return _titles;
}
void Graph::removeTitle(Title* title)
{
	_titles.remove(title);
	delete title;
}
void Graph::clearTitles()
{
	while ( !_titles.isEmpty() )
		delete _titles.takeFirst();
}
/**********************************************/
Data* Graph::data( const ustring& name )
{
	for ( int it = 0; it < _data.size(); ++it )
		if ( _data.at(it)->get(Key::Name) == name || ustring(it) == name  )
			return _data.at(it);
	return nullptr;

}
UList<Data*> Graph::datalst()
{
	return _data;
}
void Graph::removeData( Data* data )
{
	_data.remove(data);
	delete data;
}
void Graph::clearData()
{
	while ( !_data.isEmpty() )
		delete _data.takeFirst();
}
/**********************************************/
/**********************************************/
/* Modifiers */
bool Graph::set( const Key& key, const ustring& value )
{
	if ( key == Key::Name ||
		 key == Key::Format ||
		 key == Key::Path ||
		 key == Key::FileName ||
		 key == Key::Template ||
		 key == Key::Width ||
		 key == Key::Height ||
		 key.isLineStyle({Key::Borders}, false) ||
		 key.isFillStyle(Key::Background)
		 )
	{

		return Element::set( key, value );
	}
	else if ( key == Key::Size )
	{
		if ( !value.contains("x") ) return false;
		UList<ustring> list = value.split("x");
		set( Key::Width, list.at(0) );
		set( Key::Height, list.at(1) );
	}
	else if ( key == Key::Background )
	{
	}
	else if ( key == Key::Borders )
	{
	}
	return false;
}
bool Graph::set( UList<ustring>& args, bool log )
{
	Element* el = element( args );
	if ( el == nullptr )
		UConsole::error_message("Unable to find element !");
	else if ( el == this )
		return Element::set(args, log);
	else
		return el->set(args, log);
	return false;
}
bool Graph::reset( UList<ustring>& args, bool log )
{
	Element* el = element( args );
	if ( el == nullptr )
		UConsole::error_message("Unable to find element !");
	else if ( el == this )
	{
		clearLayers();
		clearFrames();
		clearAxes();
		clearPlots();
		clearLegends();
		clearTitles();
		clearData();
		this->_currentEl = nullptr;
		return true;
	}
	else
		return el->reset( args, log );
	return false;
}
void Graph::unset( const UList<ustring>& args, bool log )
{
	if ( args.isEmpty() )
		return;
	UList<ustring> args2 = args;
	Key::Keys type = Element::getType( args2.takeFirst() );

	Element* el = 0;
	if ( type == Key::NoType )
	{
		el = _currentEl;
	}
	else if ( type == Key::Graph )
	{
		Element::unset(args2, log);
	}
	else if ( !args2.isEmpty() )
	{
		ustring name = args2.takeFirst();
		el = element( type, name );
	}

	if ( el )
		el->unset(args2, log);
}
/**********************************************/
/**********************************************/
/* Displays */
void Graph::showAll( const Key::Keys& type, const ustring& name )
{
	//	std::cout << std::endl;
	if ( type == Key::Graph )
	{
		this->show();
	}
	else if ( type == Key::NoKey )
	{
		std::cout << "  " << "GRAPH : " << get(Key::Name) << std::endl;
		std::cout << "    LAYERS   -----------------" << std::endl;
		if ( layers().isEmpty() )
			std::cout << "      " << "NO LAYERS" << std::endl;
		for ( Layer* layer : layers() )
			layer->shortShow();
		std::cout << "    FRAMES   -----------------" << std::endl;
		if ( _frames.isEmpty() )
			std::cout << "      " << "NO FRAMES" << std::endl;
		for ( Frame* frame : frames() )
			frame->shortShow();
		std::cout << "    AXES     -----------------" << std::endl;
		if ( axes().isEmpty() )
			std::cout << "      " << "NO AXES" << std::endl;
		for ( Axis* axis : axes() )
			axis->shortShow();
		std::cout << "    PLOTS    -----------------" << std::endl;
		if ( plots().isEmpty() )
			std::cout << "      " << "NO PLOTS" << std::endl;
		for ( Plot* plot : plots() )
			plot->shortShow();
		std::cout << "    LEGENDS  -----------------" << std::endl;
		if ( legends().isEmpty() )
			std::cout << "      " << "NO LEGENDS" << std::endl;
		for ( Legend* legend : legends() )
			legend->shortShow();
		std::cout << "    TITLES   -----------------" << std::endl;
		if ( titles().isEmpty() )
			std::cout << "      " << "NO TITLE" << std::endl;
		for ( Title* title : titles() )
			title->shortShow();
		std::cout << "    DATA     -----------------" << std::endl;
		if ( datalst().isEmpty() )
			std::cout << "      " << "NO DATA" << std::endl;
		for ( Data* data : datalst() )
			data->shortShow();
	}
	else
	{
		if ( name.empty() )
		{
			for ( Element* el : elements(type) )
				el->show();
		}
		else
		{
			Element* element = this->element( type, name );
			if ( element )
				element->show();
		}
	}
}
void Graph::showCurrent()
{
	if ( _currentEl )
		showAll( _currentEl->type(), _currentEl->get(Key::Name) );
}
/**********************************************/
/**********************************************/
/* Drawing functions */
void Graph::prepare( Cairo* drawing )
{
	//
	Element::getTemplate();
	//
	UList<Element*> _parent_pos_refs;
	for ( Element* el : this->elements(Key::NoKey) )
	{
		el->initParentPosRef();
		if ( el->parentPosRef() == nullptr &&
			 !_parent_pos_refs.contains(el) )
			_parent_pos_refs.append(el);
	}
	for ( Element* el : _parent_pos_refs )
	{
		el->prepare( drawing );
		prepare( el, drawing );
	}
//	for ( Frame* frame : _frames )
//		frame->prepare( drawing );
//	for ( Axis* axis : _axes )
//		axis->prepare( drawing );
//	for ( Plot* plot : _plots )
//		plot->prepare( drawing );
//	for ( Legend* legend : _legends )
//		legend->prepare( drawing );
//	for ( Title* title : _titles )
//		title->prepare( drawing );
}
void Graph::prepare( Element* el, Cairo* drawing )
{
	for ( Element* c_el : el->childrenPosRef() )
	{
		c_el->prepare(drawing);
		prepare( c_el, drawing );
	}
}
void Graph::draw( Cairo* drawing )
{
	double width = stod( get(Key::Width) );
	double height = stod( get(Key::Height) );

	FillStyle fill_style = getFillStyle( {Key::Background} );
	LineStyle line_style = getLineStyle( {Key::Borders} );
	double bd_width = line_style.width();
	ShapeStyle style;
	style.setFill( fill_style );
	style.setLine( line_style );

	drawing->drawRect( {0.5*bd_width,0.5*bd_width}, Size({width-bd_width, height-bd_width}), style );

	for ( Frame* frame : _frames )
		frame->draw( drawing );
	for ( Axis* axis : _axes )
		axis->draw( drawing );
	for ( Plot* plot : _plots )
		plot->draw( drawing );
	for ( Legend* legend : _legends )
		legend->draw( drawing );
	for ( Title* title : _titles )
		title->draw( drawing );

	// TEST
	ustring test = "<style=family:fsdhfhjsd;size:24;color:0.15,0.48,0.75;>Ti<b>tle   </style> <style=>i.</style><style=italic>E</style> <style=super;bold>(MPa)</style> dgdsghds";
	TextStyle tx_sty;
	tx_sty.setSize( 28 );
	tx_sty.setCap( TextStyle::SmallCap );
	Anchor anc = Key::BaselineLeft;
	tx_sty.setAnchor(anc);
	Point pos = {200, 40};
	/*Rect rec = */drawing->drawText( test, pos.x(), pos.y(), tx_sty );
//	drawing->drawCercle( pos, 2 );
//	LineStyle styl;
//	styl.setColor( Color("red6") );
//	styl.setWidth( 0.5 );
//	ShapeStyle sthy;
//	sthy.setLine( styl );
//	drawing->drawRect( rec, sthy );
//	ustring text = "Un label parmis tant d'autres !";
//	text = text.toUpper();
//	double ny = 140;
//	Label lb ( text, {180,ny}, tx_sty.anchor() );
//	lb.prepare( drawing.drawing(),tx_sty );
//	LineStyle styl;
//	styl.setColor( Color("red6") );
//	styl.setWidth( 1 );
//	ShapeStyle sty;
//	sty.setLine( styl );
//	drawing.drawCercle( {180,140}, 3, sty );
//	drawing.drawRect( lb.boundingRect(), sty );
//	lb.draw( drawing,tx_sty );

//	text = "atzer";
//	lb.setText( text );
//	lb.setPos( {lb.boundingRect().x()+lb.boundingRect().width(), lb.pos().y()} );
//	lb.prepare( drawing.drawing(),tx_sty );
//	drawing.drawRect( lb.boundingRect(), sty );
//	lb.draw( drawing,tx_sty );

//	text = "Un label parmis tant d'autres !atzer";
//	tx_sty.setAnchor(anc);
//	lb.setText( text );
//	lb.setPos( {180,155} );
//	lb.prepare( drawing.drawing(),tx_sty );
//	drawing.drawRect( lb.boundingRect(), sty );
//	drawing.drawSimpleText( text, lb.textPos(), tx_sty );
	// END TEST

	ustring test2 = "gdsghds";
	TextStyle tx_sty2;
	tx_sty2.setSize( 25 );
	tx_sty2.setRotation( 33 );
	Anchor anc2 = Key::BaselineLeft;
	tx_sty2.setAnchor(anc2);
	Point pos2 = {200, 350};
	/*Rect rec2 = */drawing->drawText( test2, pos2.x(), pos2.y(), tx_sty2 );
	drawing->drawCercle( pos2, 2 );
//	LineStyle styl2;
//	styl2.setColor( Color("red6") );
//	styl2.setWidth( 0.5 );
//	ShapeStyle sthy2;
//	sthy2.setLine( styl2 );
//	drawing->drawRect( rec2, sthy2 );

//	Line l = {0, 0,100,0};
//	drawing->moveTo( 500,200 );
//	drawing->rotate( 0 );

//	drawing->drawLine( l );

//	drawing->rotate( 45 );

//	drawing->drawLine( l );

//	drawing->rotate( 90 );
//	drawing->drawLine( l );

//	drawing->rotate( 135 );
//	drawing->drawLine( l );

//	drawing->rotate( 180 );
//	drawing->drawLine( l );

	drawing->close();
}
void Graph::exportGraph( UList<ustring>& args )
{
	ustring path;
	ustring filename;
	ustring format;
	if ( args.isEmpty() )
	{
		path = get(Key::Path);
		if ( path.empty() )
			path = "/home/colas/Bureau";
		filename = get(Key::FileName);
		if ( filename.empty() )
			filename = "A graph";
		format = get(Key::Format);
		if ( format.empty() )
			format = "png";
	}
	else
	{
		bool wrongFilepath = false;
		ustring fullpath = args.first();
		unsigned long s_ind = fullpath.find_last_of( "/" );
		if ( s_ind != std::string::npos )
			path = fullpath.substr( 0, s_ind );
		else
			wrongFilepath = true;
		unsigned long p_ind = fullpath.find_last_of( "." );
		if ( p_ind != std::string::npos )
		{
			filename = fullpath.substr( s_ind+1, p_ind - s_ind-1 );
			format = fullpath.substr( p_ind+1 );
		}
		else
			wrongFilepath = true;
		if ( wrongFilepath )
			return UConsole::error_message("Error detected in filepath !");
	}

	Cairo* cairo = new Cairo( format,
						 get(Key::Width),
						 get(Key::Height) );

	cairo->setPath( path );
	cairo->setFileName( filename );
	_exportpath = path + "/" + filename + "." + format;
	cairo->init();

	this->prepare( cairo );
	this->draw( cairo );

	delete cairo;
	cairo = nullptr;

	std::cout << "   Your graph was export successfully at :" << std::endl;
	std::cout << "       \"" << path << "/" << filename << "." << format << "\"" << std::endl;
}
/**********************************************/
/**********************************************/
