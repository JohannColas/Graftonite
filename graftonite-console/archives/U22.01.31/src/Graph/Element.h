#ifndef ELEMENT_H
#define ELEMENT_H
/**********************************************/
#include <map>
/**********************************************/
#include "Commons/Key.h"
/**********************************************/
#include "Commons/Styles.h"
class Graph;
class Cairo;
/**********************************************/
/**********************************************/
/**********************************************/
class Element
{
public:
	/**********************************************/
	std::map<Key,ustring> _settings;
	Element* _template = nullptr;
	Key::Keys _type = Key::NoType;
	unsigned _index = -1;
	Graph* _parent = nullptr;
	Rect _boundingRect;
	Element* _parent_pos_ref = nullptr;
	UList<Element*> _children_pos_ref;
	/**********************************************/
public:
	virtual ~Element();
	Element( Graph* parent = nullptr );
	void init( const ustring& name, unsigned index, bool log = false );
	/**********************************************/
	Key::Keys type() const;
	ustring typeString() const;
	void setType( const Key::Keys& type );
	unsigned index() const;
	void setIndex( unsigned index);
	void getTemplate();
	LineStyle getLineStyle( const UList<Key::Keys>& firstKeys = {Key::Line} ) const;
	FillStyle getFillStyle( const UList<Key::Keys>& firstKeys = {Key::Background} ) const;
	ShapeStyle getShapeStyle( const UList<Key::Keys>& firstKeys = {Key::Line} ) const;
	TextStyle getTextStyle( const Key::Keys& firstKeys ) const;
	/**********************************************/
	/* Positionning Referencing */
	void initParentPosRef();
	void getPosition( Point& pos );
	Element* parentPosRef();
	bool isLoopPosReferencing( Element* el );
	void setParentPosRef( const ustring& ref );
	void removeParentPosRef();
	UList<Element*> childrenPosRef();
	void addChildPosRef( Element* child );
	void addChildPosRef( const ustring& ref );
	void removeChildPosRef( const ustring& ref );
	void removeChildPosRef( Element* child );
	/**********************************************/
	/* Checkers */
	bool hasKey( const Key& key ) const;
	/**********************************************/
	/* Accessors */
	ustring get( const ustring& key ) const;
	ustring get( const Key& key ) const;
	ustring get( const Key::Keys& key, const Key::Keys& key2 = Key::NoKey, const Key::Keys& key3 = Key::NoKey ) const;
	ustring get( const UList<Key::Keys>& key ) const;
	bool get( ustring& str, const Key& key ) const;
	bool get( ustring& str, const Key::Keys& key, const Key::Keys& key2 = Key::NoKey, const Key::Keys& key3 = Key::NoKey ) const;
	bool get( Key& value, const Key& key ) const;
	bool get( Key& value, const Key::Keys& key, const Key::Keys& key2 = Key::NoKey, const Key::Keys& key3 = Key::NoKey ) const;
	bool get( bool& i, const Key& key ) const;
	bool get( bool& i, const Key::Keys& key, const Key::Keys& key2 = Key::NoKey, const Key::Keys& key3 = Key::NoKey ) const;
	bool get( int& i, const Key& key ) const;
	bool get( int& i, const Key::Keys& key, const Key::Keys& key2 = Key::NoKey, const Key::Keys& key3 = Key::NoKey ) const;
	bool get( double& d, const Key& key ) const;
	bool get( double& d, const Key::Keys& key, const Key::Keys& key2 = Key::NoKey, const Key::Keys& key3 = Key::NoKey ) const;
	bool get( Point& point, const Key& key ) const;
	bool get( Point& point, const Key::Keys& key, const Key::Keys& key2 = Key::NoKey, const Key::Keys& key3 = Key::NoKey ) const;
	bool get( Size& size, const Key& key ) const;
	bool get( Size& size, const Key::Keys& key, const Key::Keys& key2 = Key::NoKey, const Key::Keys& key3 = Key::NoKey ) const;
	bool get( Rect& rect, const Key& key ) const;
	bool get( Rect& rect, const Key::Keys& key, const Key::Keys& key2 = Key::NoKey, const Key::Keys& key3 = Key::NoKey ) const;
	bool get( LineStyle& line_style, const Key& key ) const;
	bool get( LineStyle& line_style, const Key::Keys& key, const Key::Keys& key2 = Key::NoKey, const Key::Keys& key3 = Key::NoKey ) const;
	//	void get( LineStyle& rect, const Key::Keys& key, const Key::Keys& key2 = Key::NoKey, const Key::Keys& key3 = Key::NoKey ) const;
	//	void get( FillStyle& rect, const Key::Keys& key, const Key::Keys& key2 = Key::NoKey, const Key::Keys& key3 = Key::NoKey ) const;
	//	void get( ShapeStyle& rect, const Key::Keys& key, const Key::Keys& key2 = Key::NoKey, const Key::Keys& key3 = Key::NoKey ) const;
	//	void get( TextStyle& rect, const Key::Keys& key, const Key::Keys& key2 = Key::NoKey, const Key::Keys& key3 = Key::NoKey ) const;
	Key getKey( const Key::Keys& key, const Key::Keys& key2 = Key::NoKey, const Key::Keys& key3 = Key::NoKey ) const;
	/**********************************************/
	/* Modifiers */
	virtual bool set( const Key& key, const ustring& value );
	bool set( const Key::Keys& key, const ustring& value );
	bool set( const UList<Key::Keys>& key, const ustring& value );
	bool set( const ustring& key, const ustring& value );
	virtual bool set( UList<ustring>& args, bool log = false );
	virtual bool reset( UList<ustring>& args, bool log = false );
	virtual bool unset( const ustring& key );
	virtual void unset( const UList<ustring>& args, bool log = false );
	virtual void remove( const ustring& key );
	void clear();
	/**********************************************/
	/* Displays */
	void show();
	void shortShow();
	/**********************************************/
	/* Drawing functions */
	Rect boundingRect() const;
	virtual void prepare( Cairo* drawing );
	virtual void draw( Cairo* drawing );
	/**********************************************/
	/* Static functions */
	static Key::Keys getType( const ustring& cmd );
	/**********************************************/
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // ELEMENT_H
