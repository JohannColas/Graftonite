#include "Frame.h"
/**********************************************/
#include "Commons/Cairo.h"
/**********************************************/
/**********************************************/
Frame::Frame( Graph* parent )
	: Element(parent)
{
	init();
}
Frame::Frame( const ustring& name, unsigned index, Graph* parent, bool log )
	: Element(parent)
{
	init();
	Element::init( name, index, log );
}
void Frame::init()
{
	_type = Key::Frame;
	// Default Settings
	Element::set( Key::Position, "150x100");
	Element::set( Key::Size, "1200x775");
	Element::set( Key::Hide, "true");
}
/**********************************************/
/**********************************************/
bool Frame::set( const Key& key, const ustring& value )
{
	if ( key == Key::Name ||
		 key == Key::Template ||
		 key == Key::Position ||
		 key == Key::Anchor ||
		 key == Key::Shift ||
		 key == Key::Size ||
		 key.isLineStyle({Key::Borders}, false) ||
		 key.isFillStyle(Key::Background)
		 )
	{
		return Element::set( key, value );
	}
	else if ( key == Key::Hide )
	{
		if ( value.toBool() )
		{
			if ( !hasKey(Key(Key::Background, Key::Color)) )
				Element::set( Key(Key::Background, Key::Color), "transparent");
			if ( !hasKey(Key(Key::Borders, Key::Color)) )
				Element::set( Key(Key::Borders, Key::Color), "black");
		}
		return Element::set( key, value );
	}
	else if ( key == Key::Background )
	{
	}
	else if ( key == Key::Borders )
	{
	}
	return false;
}
/**********************************************/
/**********************************************/
void Frame::prepare( Cairo* /*drawing*/ )
{
	Element::getTemplate();
	// Get Size
	Size size = {1200,775};
	get( size, Key::Size );
	// Get Pos
	Point pos = {150,100}; Element::getPosition( pos );
	Key anchor = Key::TopLeft; get( anchor, Key::Anchor );
	Point diff = size.point(anchor);
	Point shift(0,0); get(shift, Key::Shift);
	pos -= diff - shift;
	// Set BoundingRect
	_boundingRect = {pos,size};
	// Set Style
	FillStyle fill_style = getFillStyle();
	LineStyle line_style = getLineStyle( {Key::Borders} );
	_style.setFill( fill_style );
	_style.setLine( line_style );
}
/**********************************************/
/**********************************************/
void Frame::draw( Cairo* drawing )
{
	bool hide = false; get( hide, Key::Hide );
	// Draw Rect
	if ( !hide )
		drawing->drawRect( boundingRect(), style() );
}
/**********************************************/
/**********************************************/
ShapeStyle Frame::style() const
{
	return _style;
}
/**********************************************/
/**********************************************/
