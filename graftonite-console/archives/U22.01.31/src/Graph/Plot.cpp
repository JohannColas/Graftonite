#include "Plot.h"
/**********************************************/
#include "Commons/Cairo.h"
#include "Data.h"
/**********************************************/
#include "Graph.h"
/**********************************************/
/**********************************************/
Plot::Plot( Graph* parent )
	: Element(parent)
{
	init();
}
Plot::Plot( const ustring& name, unsigned index, Graph* parent, bool log )
	: Element(parent)
{
	init();
	Element::init( name, index, log );
}
void Plot::init()
{
	_type = Key::Plot;
	Element::set( Key::Name, "plot1" );
	Element::set( Key::Axis1, "0" );
	Element::set( Key::Axis2, "1" );
	Element::set( Key::Data1, "0:c0" );
	Element::set( Key::Data2, "0:c1" );
	Element::set( Key(Key::Line, Key::Gap), "15" );
}
/**********************************************/
/**********************************************/
bool Plot::set( const Key& key, const ustring& value )
{
	if ( key == Key::Name ||
		 key == Key::Template ||
		 key == Key::Type ||
		 key == Key::Axis1 ||
		 key == Key::Axis2 ||
	     key == Key::Axis3 ||
		 key == Key::Data1 ||
		 key == Key::Data2 ||
		 key == Key::Data3 ||
		 //
		 key == Key(Key::Line, Key::Hide) ||
		 key.isLineStyle(Key::Line) ||
		 key == Key(Key::Line, Key::Gap) ||
		 //
		 key == Key(Key::Symbols, Key::Hide) ||
		 key.isLineStyle({Key::Symbols, Key::Line}, false) ||
		 key.isFillStyle(Key::Symbols, Key::Fill) ||
		 key == Key(Key::Symbols, Key::Size) ||
		 key == Key(Key::Symbols, Key::Type) ||
		 key == Key(Key::Symbols, Key::Option) ||
		 key == Key(Key::Symbols, Key::Option, Key::Size) ||
		 key == Key(Key::Symbols, Key::BeginAngle) ||
		 key == Key(Key::Symbols, Key::EndAngle) ||
		 key == Key(Key::Symbols, Key::Rotation) ||
		 key == Key(Key::Symbols, Key::Covering) ||
		 //
		 key == Key(Key::Area, Key::Hide) ||
		 key == Key(Key::Area, Key::Position) ||
		 key.isLineStyle({Key::Area, Key::Line}, false) ||
		 key.isFillStyle(Key::Area, Key::Fill) ||
		 //
		 key == Key(Key::Bars, Key::Hide) ||
		 key == Key(Key::Bars, Key::Position) ||
		 key.isLineStyle({Key::Bars, Key::Line}, false) ||
		 key.isFillStyle(Key::Bars, Key::Fill) ||
		 //
		 key == Key(Key::ErrorBars, Key::Hide) ||
		 key.isLineStyle(Key::ErrorBars, Key::Line) ||
		 //
		 key == Key(Key::Labels, Key::Format) ||
		 key == Key(Key::Labels, Key::Anchor) ||
		 key == Key(Key::Labels, Key::Hide) ||
		 key.isFontStyle(Key::Labels)
		 )
	{
		return Element::set( key, value );
	}
	else if ( key == Key(Key::Line) )
	{

	}
	else if ( key == Key(Key::Symbols) )
	{

	}
//	else if ( key == Key(Key::Symbols, Key::Size) )
//	{

//	}
//	else if ( key == Key(Key::Symbols, Key::Option, Key::Size) )
//	{

//	}
	else if ( key == Key(Key::Area) )
	{

	}
	else if ( key == Key(Key::Bars) )
	{

	}
	else if ( key == Key(Key::ErrorBars) )
	{

	}
	else if ( key == Key(Key::Labels) )
	{

	}
	return false;
}
/**********************************************/
/**********************************************/
void Plot::prepare( Cairo* /*drawing*/ )
{
	Element::getTemplate();


	// Set legend position
//	_boundingRect = ;
	//
}
/**********************************************/
/**********************************************/
void Plot::draw( Cairo* drawing )
{
	if ( !_parent ||
		 !hasKey(Key::Axis1) ||
		 !hasKey(Key::Axis2) ||
		 !hasKey(Key::Data1) ||
		 !hasKey(Key::Data2)
		 )
		return;
	Axis* axis1 = nullptr;
	Axis* axis2 = nullptr;
	UList<Axis*> _axes;
	axis1 = _parent->axis( get(Key::Axis1) );
	axis2 = _parent->axis( get(Key::Axis2) );
	if ( !axis1 || !axis2 )
		return;

	UList<ustring> data1_ref = get(Key::Data1).split(":");
	Data* data1 = nullptr;
	if ( data1_ref.size() > 1 )
		data1 = _parent->data( data1_ref.first() );
	UList<ustring> data2_ref = get(Key::Data2).split(":");
	Data* data2 = nullptr;
	if ( data2_ref.size() > 1 )
		data2 = _parent->data( data2_ref.first() );
	if ( !data1 || !data2 )
		return;
	UList<ustring> data3_ref = get(Key::Data3).split(":");
	Data* data3 = nullptr;
	if ( data3_ref.size() > 1 )
		data3 = _parent->data( data3_ref.first() );
	UList<ustring> data1_val = data1->getData( data1_ref.at(1) );
	UList<ustring> data2_val = data2->getData( data2_ref.at(1) );
	UList<ustring> data3_val;
	if ( data3 && (data3_ref.size() > 1) )
		data3_val = data3->getData( data3_ref.at(1) );
	//
	UList<Point> graphData;
	int size = data1_val.size();
	if ( size > data2_val.size() )
		size = data2_val.size();
	for ( int it = 0; it < size; ++it )
	{
		double x = axis1->GetGraphCoord(data1_val.at(it).toDouble());
		double y = axis2->GetGraphCoord(data2_val.at(it).toDouble());
		graphData.append( {x, y} );
	}

	cairo_set_source_rgba( drawing->drawing(), 0, 0, 0, 0 );
	cairo_rectangle( drawing->drawing(), axis1->boundingRect().x(),  axis2->boundingRect().y(),  axis1->boundingRect().width(),  axis2->boundingRect().height() );
	cairo_clip( drawing->drawing() );
	cairo_new_path( drawing->drawing() );
	//
	// Plot Line
	bool off = false;
	if ( !get(Key::Line, Key::Hide).empty() )
		off = true;
	if ( !off )
	{
		double gap = -1;
		if ( hasKey({Key::Line, Key::Gap}) )
			gap = get(Key::Line, Key::Gap).toDouble();
		line_gap = gap;
		LineStyle style = getLineStyle();
		this->line_style = style;
		drawing->drawGappedPath( graphData, gap, style );
	}
	//
	// Plot Symbols
	off = false;
//	if ( !get(Key::Symbols, Key::Hide).empty() )
//		off = true;
	if ( !off )
	{
		Symbol symbol;
		//
		ustring tmp;
		tmp = get(Key::Symbols, Key::Type);
		if ( !tmp.empty() )
			symbol.setType( tmp );
		tmp = get(Key::Symbols, Key::Size);
		if ( !tmp.empty() )
			symbol.setSize( tmp );
		tmp = get(Key::Symbols, Key::Option);
		if ( !tmp.empty() )
			symbol.setOption( tmp );
		tmp = get(Key::Symbols, Key::Option, Key::Size);
		if ( !tmp.empty() )
			symbol.setOptionSize( tmp );
		tmp = get(Key::Symbols, Key::BeginAngle);
		if ( !tmp.empty() )
			symbol.setBeginAngle( tmp.toDouble() );
		tmp = get(Key::Symbols, Key::EndAngle);
		if ( !tmp.empty() )
			symbol.setEndAngle( tmp.toDouble() );
		tmp = get(Key::Symbols, Key::Rotation);
		if ( !tmp.empty() )
			symbol.setRotation( tmp.toDouble() );
		tmp = get(Key::Symbols, Key::Covering);
		if ( !tmp.empty() )
			symbol.setCovering( tmp.toDouble() );
		this->symbol = symbol;

		LineStyle line_style = getLineStyle( {Key::Symbols, Key::Line} );
		FillStyle fill_style= getFillStyle( {Key::Symbols, Key::Fill} );
		if ( fill_style.color().alpha() == 0.0 )
			fill_style.setColor(Color("black"));
		ShapeStyle style;
		style.setLine( line_style );
		style.setFill( fill_style );
		this->symbol_style = style;
		for ( const Point& p : graphData )
			drawing->drawSymbols( symbol, p, symbol.size(), style );
	}
	//
	// Plot Area
	off = false;
	if ( !get(Key::Area, Key::Hide).empty() )
		off = true;
	if ( !off )
	{
		LineStyle line_style = getLineStyle( {Key::Area, Key::Line} );
		FillStyle fill_style = getFillStyle( {Key::Area, Key::Fill} );
		ShapeStyle style;
		style.setLine( line_style );
		style.setFill( fill_style );
	}
	//
	// Plot Bars
	off = false;
	if ( !get(Key::Bars, Key::Hide).empty() )
		off = true;
	if ( !off )
	{
		LineStyle line_style = getLineStyle( {Key::Bars, Key::Line} );
		FillStyle fill_style = getFillStyle( {Key::Bars, Key::Fill} );
		ShapeStyle style;
		style.setLine( line_style );
		style.setFill( fill_style );
	}
	//
	// Plot ErrorBars
	off = false;
	if ( !get(Key::ErrorBars, Key::Hide).empty() )
		off = true;
	if ( !off )
	{
		LineStyle style = getLineStyle( {Key::ErrorBars, Key::Line} );
	}
	//
	// Plot Labels
	off = false;
	if ( !get(Key::Labels, Key::Hide).empty() )
		off = true;
	if ( !off )
	{
		TextStyle style = getTextStyle( Key::Labels );
	}
	//
	cairo_reset_clip( drawing->drawing() );
}
/**********************************************/
/**********************************************/
