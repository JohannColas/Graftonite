#ifndef UMATRIX_H
#define UMATRIX_H
/**********************************************/
#include "UList.h"
/**********************************************/
/**********************************************/
/**********************************************/
template <class T>
class UMatrix
{
private:
	UList<UList<T>> _data;
public:
	UMatrix();
	unsigned long rows() const;
	unsigned long columns() const;
	unsigned long size() const;
	T& at( unsigned long row, unsigned long col );
	T at( unsigned long row, unsigned long col ) const;
	void append( T const& );
	void append( UList<T> const& );
	void replace( unsigned long row, unsigned long col, const T& el );
	void replaceRow( unsigned long row, const UList<T>& el );
	void replaceColumn( unsigned long col, const UList<T>& el );
	bool contains( const T& el ) const;
	void clear();
	void deleteAll();
	bool isEmpty() const
	{
		return _data.empty();
	}
	bool isValid( unsigned index ) const
	{
		return (index >= 0 || index < size());
	}
	// -----------------------------------
	UList<T>& operator= ( const UList<T>& list )
	{
		(*this).clear();
		for ( auto el : list )
			(*this).append(el);
		return *this;
	}
	T & operator()( unsigned long row, unsigned long col );
	T operator()( unsigned long row, unsigned long col ) const;
	// -----------------------------------
};
/**********************************************/
template<class T>
UMatrix<T>::UMatrix()
{

}
/**********************************************/
template<class T>
unsigned long UMatrix<T>::rows() const
{
	return _data.size();
}
/**********************************************/
template<class T>
unsigned long UMatrix<T>::columns() const
{

}
/**********************************************/
template<class T>
unsigned long UMatrix<T>::size() const
{

}
/**********************************************/
template<class T>
T& UMatrix<T>::at( unsigned long row, unsigned long col )
{
	return _data.at(row).at(col);
}
/**********************************************/
template<class T>
T UMatrix<T>::at( unsigned long row, unsigned long col ) const
{
	return _data.at(row).at(col);
}
/**********************************************/
template<class T>
void UMatrix<T>::append( const UList<T>& row )
{
	if ( this->isEmpty() )
		_data.append(row);
	else if ( _data.at(0).size() == row.size() )
		_data.append(row);
}
/**********************************************/
template<class T>
void UMatrix<T>::replace( unsigned long row, unsigned long col, const T& el )
{
	this->at(row, col) = el;
}
template<class T>
void UMatrix<T>::replaceRow( unsigned long row, const UList<T>& el )
{

}
template<class T>
void UMatrix<T>::replaceColumn( unsigned long col, const UList<T>& el )
{

}
/**********************************************/
template<class T>
bool UMatrix<T>::contains( const T& el ) const
{
	for ( int rt = 0; rt < rows(); ++rt )
		for ( int ct = 0; ct < columns(); ++ct )
			if ( this->at(rt,ct) == el )
				return true;
	return false;
}
/**********************************************/
template<class T>
void UMatrix<T>::clear()
{
	_data.clear();
}
/**********************************************/
template<class T>
void UMatrix<T>::deleteAll()
{

}
/**********************************************/
template<typename T>
inline T& UMatrix<T>::operator()( unsigned long row, unsigned long col )
{
	return this->at(row,col);
}
/**********************************************/
template<typename T>
inline T UMatrix<T>::operator()( unsigned long row, unsigned long col ) const
{
	return this->at(row,col);
}
/**********************************************/
/**********************************************/
/**********************************************/
#endif // UMATRIX_H
