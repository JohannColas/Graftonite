#include "Color.h"
/**********************************************/
#include "UString.h"
/**********************************************/
/**********************************************/
/**********************************************/
Color::Color()
{
}
Color::Color( double red, double green, double blue, double alpha )
{
	setRed( red );
	setGreen( green );
	setBlue( blue );
	setAlpha( alpha );
}
Color::Color( int red, int green, int blue, int alpha )
{
	setRed( red );
	setGreen( green );
	setBlue( blue );
	setAlpha( alpha );
}
Color::Color( const ustring& color )
{
	setColor( color );
}
double Color::red() const
{
	return _red;
}
void Color::setRed( double red )
{
	_red = red;
}
void Color::setRed( int red )
{
	_red = ((double)red)/255.0;
}
double Color::green() const
{
	return _green;
}
void Color::setGreen( double green )
{
	_green = green;
}
void Color::setGreen( int green )
{
	_green = ((double)green)/255.0;
}
double Color::blue() const
{
	return _blue;
}
void Color::setBlue( double blue )
{
	_blue = blue;
}
void Color::setBlue( int blue )
{
	_blue = ((double)blue)/255.0;
}
double Color::alpha() const
{
	return _alpha;
}
void Color::setAlpha( double alpha )
{
	_alpha = alpha;
}
void Color::setAlpha( int alpha )
{
	_alpha = ((double)alpha)/255.0;
}
void Color::setColor( const ustring& color )
{
	ustring color_tmp = color;
	if ( !color_tmp.contains(",") )
		color_tmp = Color::getColorByName(color);

	UList<ustring> rgba = color_tmp.split(",");
	if ( rgba.size() < 3 )
		return;
	for ( int it = 0; it < rgba.size(); ++it )
	{
		ustring tmp = rgba.at(it);
		if ( it == 0 )
		{
			if ( tmp.isInteger() )
				setRed( tmp.toInt() );
			else if ( tmp.isDouble() )
				setRed( tmp.toDouble() );
		}
		else if ( it == 1 )
		{
			if ( tmp.isInteger() )
				setGreen( tmp.toInt() );
			else if ( tmp.isDouble() )
				setGreen( tmp.toDouble() );
		}
		else if ( it == 2 )
		{
			if ( tmp.isInteger() )
				setBlue( tmp.toInt() );
			else if ( tmp.isDouble() )
				setBlue( tmp.toDouble() );
		}
		else if ( it == 3 )
		{
			if ( tmp.isInteger() )
				setAlpha( tmp.toInt() );
			else if ( tmp.isDouble() )
				setAlpha( tmp.toDouble() );
		}
		else
			break;
	}
}
ustring Color::getColorByName( const ustring& name )
{
	if ( name == "transparent" )
		return "0,0,0,0";
	else if ( name == "white" )
		return "255,255,255,255";
	else if ( name == "black" )
		return "0,0,0,255";
	else if ( name == "red" )
		return "244,67,54,255";
	else if ( name == "pink" )
		return "233,30,99,255";
	else if ( name == "purple" )
		return "156,39,176,255";
	else if ( name == "deeppurple" )
		return "103,58,183,255";
	else if ( name == "indigo" )
		return "63,81,181,255";
	else if ( name == "blue" )
		return "33,150,243,255";
	else if ( name == "lightblue" )
		return "3,169,244,255";
	else if ( name == "cyan" )
		return "0,188,212,255";
	else if ( name == "teal" )
		return "0,150,136,255";
	else if ( name == "green" )
		return "76,175,80,255";
	else if ( name == "lightgreen" )
		return "139,195,74,255";
	else if ( name == "lime" )
		return "205,220,57,255";
	else if ( name == "yellow" )
		return "255,235,59,255";
	else if ( name == "amber" )
		return "255,193,7,255";
	else if ( name == "orange" )
		return "255,152,0,255";
	else if ( name == "deeporange" )
		return "255,87,34,255";
	else if ( name == "brown" )
		return "121,85,72,255";
	else if ( name == "grey" )
		return "158,158,158,255";
	else if ( name == "bluegrey" )
		return "96,125,139,255";
	// from https://materialui.co/colors
	// Red
	else if ( name == "red0" )
		return "255,235,238,255";
	else if ( name == "red1" )
		return "255,205,210,255";
	else if ( name == "red2" )
		return "239,154,154,255";
	else if ( name == "red3" )
		return "229,115,115,255";
	else if ( name == "red4" )
		return "239,83,80,255";
	else if ( name == "red5" )
		return "244,67,54,255";
	else if ( name == "red6" )
		return "229,57,53,255";
	else if ( name == "red7" )
		return "211,47,47,255";
	else if ( name == "red8" )
		return "198,40,40,255";
	else if ( name == "red9" )
		return "183,28,28,255";
	else if ( name == "redA1" )
		return "255,138,128,255";
	else if ( name == "redA2" )
		return "255,82,82,255";
	else if ( name == "redA3" )
		return "255,23,68,255";
	else if ( name == "redA4" )
		return "213,0,0,255";
	// Pink
	else if ( name == "pink0" )
		return "252,228,236,255";
	else if ( name == "pink1" )
		return "248,187,208,255";
	else if ( name == "pink2" )
		return "244,143,177,255";
	else if ( name == "pink3" )
		return "240,98,146,255";
	else if ( name == "pink4" )
		return "236,64,122,255";
	else if ( name == "pink5" )
		return "233,30,99,255";
	else if ( name == "pink6" )
		return "216,27,96,255";
	else if ( name == "pink7" )
		return "194,24,91,255";
	else if ( name == "pink8" )
		return "173,20,87,255";
	else if ( name == "pink9" )
		return "136,14,79,255";
	else if ( name == "pinkA1" )
		return "255,128,171,255";
	else if ( name == "pinkA2" )
		return "255,64,129,255";
	else if ( name == "pinkA3" )
		return "245,0,87,255";
	else if ( name == "pinkA4" )
		return "197,17,98,255";
	// Purple
	else if ( name == "purple0" )
		return "243,229,245,255";
	else if ( name == "purple1" )
		return "225,190,231,255";
	else if ( name == "purple2" )
		return "206,147,216,255";
	else if ( name == "purple3" )
		return "186,104,200,255";
	else if ( name == "purple4" )
		return "171,71,188,255";
	else if ( name == "purple5" )
		return "156,39,176,255";
	else if ( name == "purple6" )
		return "142,36,170,255";
	else if ( name == "purple7" )
		return "123,31,162,255";
	else if ( name == "purple8" )
		return "106,27,154,255";
	else if ( name == "purple9" )
		return "74,20,140,255";
	else if ( name == "purpleA1" )
		return "234,128,252,255";
	else if ( name == "purpleA2" )
		return "224,64,251,255";
	else if ( name == "purpleA3" )
		return "213,0,249,255";
	else if ( name == "purpleA4" )
		return "170,0,255,255";
	// DeepPurple
	else if ( name == "deeppurple0" )
		return "237,231,246,255";
	else if ( name == "deeppurple1" )
		return "209,196,233,255";
	else if ( name == "deeppurple2" )
		return "179,157,219,255";
	else if ( name == "deeppurple3" )
		return "149,117,205,255";
	else if ( name == "deeppurple4" )
		return "126,87,194,255";
	else if ( name == "deeppurple5" )
		return "103,58,183,255";
	else if ( name == "deeppurple6" )
		return "94,53,177,255";
	else if ( name == "deeppurple7" )
		return "81,45,168,255";
	else if ( name == "deeppurple8" )
		return "69,39,160,255";
	else if ( name == "deeppurple9" )
		return "49,27,146,255";
	else if ( name == "deeppurpleA1" )
		return "179,136,255,255";
	else if ( name == "deeppurpleA2" )
		return "124,77,255,255";
	else if ( name == "deeppurpleA3" )
		return "101,31,255,255";
	else if ( name == "deeppurpleA4" )
		return "98,0,234,255";
	// Indigo
	else if ( name == "indigo0" )
		return "232,234,246,255";
	else if ( name == "indigo1" )
		return "197,202,233,255";
	else if ( name == "indigo2" )
		return "159,168,218,255";
	else if ( name == "indigo3" )
		return "121,134,203,255";
	else if ( name == "indigo4" )
		return "92,107,192,255";
	else if ( name == "indigo5" )
		return "63,81,181,255";
	else if ( name == "indigo6" )
		return "57,73,171,255";
	else if ( name == "indigo7" )
		return "48,63,159,255";
	else if ( name == "indigo8" )
		return "40,53,147,255";
	else if ( name == "indigo9" )
		return "26,35,126,255";
	else if ( name == "indigoA1" )
		return "140,158,255,255";
	else if ( name == "indigoA2" )
		return "83,109,254,255";
	else if ( name == "indigoA3" )
		return "61,90,254,255";
	else if ( name == "indigoA4" )
		return "48,79,254,255";
	// Blue
	else if ( name == "blue0" )
		return "227,242,253,255";
	else if ( name == "blue1" )
		return "187,222,251,255";
	else if ( name == "blue2" )
		return "144,202,249,255";
	else if ( name == "blue3" )
		return "100,181,246,255";
	else if ( name == "blue4" )
		return "66,165,245,255";
	else if ( name == "blue5" )
		return "33,150,243,255";
	else if ( name == "blue6" )
		return "30,136,229,255";
	else if ( name == "blue7" )
		return "25,118,210,255";
	else if ( name == "blue8" )
		return "21,101,192,255";
	else if ( name == "blue9" )
		return "13,71,161,255";
	else if ( name == "blueA1" )
		return "130,177,255,255";
	else if ( name == "blueA2" )
		return "68,138,255,255";
	else if ( name == "blueA3" )
		return "41,121,255,255";
	else if ( name == "blueA4" )
		return "41,98,255,255";
	// LightBlue
	else if ( name == "lightblue0" )
		return "225,245,254,255";
	else if ( name == "lightblue1" )
		return "179,229,252,255";
	else if ( name == "lightblue2" )
		return "129,212,250,255";
	else if ( name == "lightblue3" )
		return "79,195,247,255";
	else if ( name == "lightblue4" )
		return "41,182,246,255";
	else if ( name == "lightblue5" )
		return "3,169,244,255";
	else if ( name == "lightblue6" )
		return "3,155,229,255";
	else if ( name == "lightblue7" )
		return "2,136,209,255";
	else if ( name == "lightblue8" )
		return "2,119,189,255";
	else if ( name == "lightblue9" )
		return "1,87,155,255";
	else if ( name == "lightblueA1" )
		return "128,216,255,255";
	else if ( name == "lightblueA2" )
		return "64,196,255,255";
	else if ( name == "lightblueA3" )
		return "0,176,255,255";
	else if ( name == "lightblueA4" )
		return "0,145,234,255";
	// Cyan
	else if ( name == "cyan0" )
		return "224,247,250,255";
	else if ( name == "cyan1" )
		return "178,235,242,255";
	else if ( name == "cyan2" )
		return "128,222,234,255";
	else if ( name == "cyan3" )
		return "77,208,225,255";
	else if ( name == "cyan4" )
		return "38,198,218,255";
	else if ( name == "cyan5" )
		return "0,188,212,255";
	else if ( name == "cyan6" )
		return "0,172,193,255";
	else if ( name == "cyan7" )
		return "0,151,167,255";
	else if ( name == "cyan8" )
		return "0,131,143,255";
	else if ( name == "cyan9" )
		return "0,96,100,255";
	else if ( name == "cyanA1" )
		return "132,255,255,255";
	else if ( name == "cyanA2" )
		return "24,255,255,255";
	else if ( name == "cyanA3" )
		return "0,229,255,255";
	else if ( name == "cyanA4" )
		return "0,184,212,255";
	// Teal
	else if ( name == "teal0" )
		return "224,242,241,255";
	else if ( name == "teal1" )
		return "178,223,219,255";
	else if ( name == "teal2" )
		return "128,203,196,255";
	else if ( name == "teal3" )
		return "77,182,172,255";
	else if ( name == "teal4" )
		return "38,166,154,255";
	else if ( name == "teal5" )
		return "0,150,136,255";
	else if ( name == "teal6" )
		return "0,137,123,255";
	else if ( name == "teal7" )
		return "0,121,107,255";
	else if ( name == "teal8" )
		return "0,105,92,255";
	else if ( name == "teal9" )
		return "0,77,64,255";
	else if ( name == "tealA1" )
		return "167,255,235,255";
	else if ( name == "tealA2" )
		return "100,255,218,255";
	else if ( name == "tealA3" )
		return "29,233,182,255";
	else if ( name == "tealA4" )
		return "0,191,165,255";
	// Green
	else if ( name == "green0" )
		return "232,245,233,255";
	else if ( name == "green1" )
		return "200,230,201,255";
	else if ( name == "green2" )
		return "165,214,167,255";
	else if ( name == "green3" )
		return "129,199,132,255";
	else if ( name == "green4" )
		return "102,187,106,255";
	else if ( name == "green5" )
		return "76,175,80,255";
	else if ( name == "green6" )
		return "67,160,71,255";
	else if ( name == "green7" )
		return "56,142,60,255";
	else if ( name == "green8" )
		return "46,125,50,255";
	else if ( name == "green9" )
		return "27,94,32,255";
	else if ( name == "greenA1" )
		return "185,246,202,255";
	else if ( name == "greenA2" )
		return "105,240,174,255";
	else if ( name == "greenA3" )
		return "0,230,118,255";
	else if ( name == "greenA4" )
		return "0,200,83,255";
	// LightGreen
	else if ( name == "lightgreen0" )
		return "241,248,233,255";
	else if ( name == "lightgreen1" )
		return "220,237,200,255";
	else if ( name == "lightgreen2" )
		return "197,225,165,255";
	else if ( name == "lightgreen3" )
		return "174,213,129,255";
	else if ( name == "lightgreen4" )
		return "156,204,101,255";
	else if ( name == "lightgreen5" )
		return "139,195,74,255";
	else if ( name == "lightgreen6" )
		return "124,179,66,255";
	else if ( name == "lightgreen7" )
		return "104,159,56,255";
	else if ( name == "lightgreen8" )
		return "85,139,47,255";
	else if ( name == "lightgreen9" )
		return "51,105,30,255";
	else if ( name == "lightgreenA1" )
		return "204,255,144,255";
	else if ( name == "lightgreenA2" )
		return "178,255,89,255";
	else if ( name == "lightgreenA3" )
		return "118,255,3,255";
	else if ( name == "lightgreenA4" )
		return "100,221,23,255";
	// Lime
	else if ( name == "lime0" )
		return "249,251,231,255";
	else if ( name == "lime1" )
		return "240,244,195,255";
	else if ( name == "lime2" )
		return "230,238,156,255";
	else if ( name == "lime3" )
		return "220,231,117,255";
	else if ( name == "lime4" )
		return "212,225,87,255";
	else if ( name == "lime5" )
		return "205,220,57,255";
	else if ( name == "lime6" )
		return "192,202,51,255";
	else if ( name == "lime7" )
		return "175,180,43,255";
	else if ( name == "lime8" )
		return "158,157,36,255";
	else if ( name == "lime9" )
		return "130,119,23,255";
	else if ( name == "limeA1" )
		return "244,255,129,255";
	else if ( name == "limeA2" )
		return "238,255,65,255";
	else if ( name == "limeA3" )
		return "198,255,0,255";
	else if ( name == "limeA4" )
		return "174,234,0,255";
	// Yellow
	else if ( name == "yellow0" )
		return "255,253,231,255";
	else if ( name == "yellow1" )
		return "255,249,196,255";
	else if ( name == "yellow2" )
		return "255,245,157,255";
	else if ( name == "yellow3" )
		return "255,241,118,255";
	else if ( name == "yellow4" )
		return "255,238,88,255";
	else if ( name == "yellow5" )
		return "255,235,59,255";
	else if ( name == "yellow6" )
		return "253,216,53,255";
	else if ( name == "yellow7" )
		return "251,192,45,255";
	else if ( name == "yellow8" )
		return "249,168,37,255";
	else if ( name == "yellow9" )
		return "245,127,23,255";
	else if ( name == "yellowA1" )
		return "255,255,141,255";
	else if ( name == "yellowA2" )
		return "255,255,0,255";
	else if ( name == "yellowA3" )
		return "255,234,0,255";
	else if ( name == "yellowA4" )
		return "255,214,0,255";
	// Amber
	else if ( name == "amber0" )
		return "255,248,225,255";
	else if ( name == "amber1" )
		return "255,236,179,255";
	else if ( name == "amber2" )
		return "255,224,130,255";
	else if ( name == "amber3" )
		return "255,213,79,255";
	else if ( name == "amber4" )
		return "255,202,40,255";
	else if ( name == "amber5" )
		return "255,193,7,255";
	else if ( name == "amber6" )
		return "255,179,0,255";
	else if ( name == "amber7" )
		return "255,160,0,255";
	else if ( name == "amber8" )
		return "255,143,0,255";
	else if ( name == "amber9" )
		return "255,111,0,255";
	else if ( name == "amberA1" )
		return "255,229,127,255";
	else if ( name == "amberA2" )
		return "255,215,64,255";
	else if ( name == "amberA3" )
		return "255,196,0,255";
	else if ( name == "amberA4" )
		return "255,171,0,255";
	// Orange
	else if ( name == "orange0" )
		return "255,243,224,255";
	else if ( name == "orange1" )
		return "255,224,178,255";
	else if ( name == "orange2" )
		return "255,204,128,255";
	else if ( name == "orange3" )
		return "255,183,77,255";
	else if ( name == "orange4" )
		return "255,167,38,255";
	else if ( name == "orange5" )
		return "255,152,0,255";
	else if ( name == "orange6" )
		return "251,140,0,255";
	else if ( name == "orange7" )
		return "245,124,0,255";
	else if ( name == "orange8" )
		return "239,108,0,255";
	else if ( name == "orange9" )
		return "230,81,0,255";
	else if ( name == "orangeA1" )
		return "255,209,128,255";
	else if ( name == "orangeA2" )
		return "255,171,64,255";
	else if ( name == "orangeA3" )
		return "255,145,0,255";
	else if ( name == "orangeA4" )
		return "255,109,0,255";
	// DeepOrange
	else if ( name == "deeporange0" )
		return "251,233,231,255";
	else if ( name == "deeporange1" )
		return "255,204,188,255";
	else if ( name == "deeporange2" )
		return "255,171,145,255";
	else if ( name == "deeporange3" )
		return "255,138,101,255";
	else if ( name == "deeporange4" )
		return "255,112,67,255";
	else if ( name == "deeporange5" )
		return "255,87,34,255";
	else if ( name == "deeporange6" )
		return "244,81,30,255";
	else if ( name == "deeporange7" )
		return "230,74,25,255";
	else if ( name == "deeporange8" )
		return "216,67,21,255";
	else if ( name == "deeporange9" )
		return "191,54,12,255";
	else if ( name == "deeporangeA1" )
		return "255,158,128,255";
	else if ( name == "deeporangeA2" )
		return "255,110,64,255";
	else if ( name == "deeporangeA3" )
		return "255,61,0,255";
	else if ( name == "deeporangeA4" )
		return "221,44,0,255";
	// Brown
	else if ( name == "brown0" )
		return "239,235,233,255";
	else if ( name == "brown1" )
		return "215,204,200,255";
	else if ( name == "brown2" )
		return "188,170,164,255";
	else if ( name == "brown3" )
		return "161,136,127,255";
	else if ( name == "brown4" )
		return "141,110,99,255";
	else if ( name == "brown5" )
		return "121,85,72,255";
	else if ( name == "brown6" )
		return "109,76,65,255";
	else if ( name == "brown7" )
		return "93,64,55,255";
	else if ( name == "brown8" )
		return "78,52,46,255";
	else if ( name == "brown9" )
		return "62,39,35,255";
	// Grey
	else if ( name == "grey0" )
		return "250,250,250,255";
	else if ( name == "grey1" )
		return "245,245,245,255";
	else if ( name == "grey2" )
		return "238,238,238,255";
	else if ( name == "grey3" )
		return "224,224,224,255";
	else if ( name == "grey4" )
		return "189,189,189,255";
	else if ( name == "grey5" )
		return "158,158,158,255";
	else if ( name == "grey6" )
		return "117,117,117,255";
	else if ( name == "grey7" )
		return "97,97,97,255";
	else if ( name == "grey8" )
		return "66,66,66,255";
	else if ( name == "grey9" )
		return "33,33,33,255";
	// BlueGrey
	else if ( name == "bluegrey0" )
		return "236,239,241,255";
	else if ( name == "bluegrey1" )
		return "207,216,220,255";
	else if ( name == "bluegrey2" )
		return "176,190,197,255";
	else if ( name == "bluegrey3" )
		return "144,164,174,255";
	else if ( name == "bluegrey4" )
		return "120,144,156,255";
	else if ( name == "bluegrey5" )
		return "96,125,139,255";
	else if ( name == "bluegrey6" )
		return "84,110,122,255";
	else if ( name == "bluegrey7" )
		return "69,90,100,255";
	else if ( name == "bluegrey8" )
		return "55,71,79,255";
	else if ( name == "bluegrey9" )
		return "38,50,56,255";
	//
	else
		return "0,0,0";
}
/**********************************************/
/**********************************************/
/**********************************************/
