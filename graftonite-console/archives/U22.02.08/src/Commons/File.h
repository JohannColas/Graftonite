#ifndef FILE_H
#define FILE_H
/**********************************************/
#include "UString.h"
#include <fstream>
/**********************************************/
/**********************************************/
/**********************************************/
class File
{
	std::ofstream _file;
public:
	static ustring read( const ustring& path );
	static bool read( const ustring& path, UList<ustring>& contents );
	static void write( const ustring& path, const ustring& contents );
	static void write( const ustring& path, const UList<ustring>& contents );
	static void append( const ustring& path, const ustring& contents );
	static bool exists( const ustring& path );
	static void remove( const ustring& path );
	/**********************************************/
	File();
	File( const ustring& path );
	void setPath( const ustring& path );
	std::ofstream& write();
	void close();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // FILE_H
