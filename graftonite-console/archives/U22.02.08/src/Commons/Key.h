#ifndef KEY_H
#define KEY_H
/**********************************************/
#include "UString.h"
/**********************************************/
/**********************************************/
/**********************************************/
class Key
{
public:
	enum Keys
	{
		NoKey,
		Name,
		FileName,
		Path,
		Working,
		Command,
		History,
		Lang,
		PNG,
		PDF,
		SVG,
		PS,
		NoType,
		Current,
		Graph,
		Template,
		Layer,
		Frame,
		Axis,
		Plot,
		Legend,
		Layout,
		Vertical,
		Horizontal,
		Table,
		Row,
		Column,
		Spacing,
		Margins,
		Entry,
		AddEntry,
		Title,
		X,
		Y,
		Position,
		Width,
		Height,
		Size,
		Type,
		Format,
		Background,
		Fill,
		Borders,
		Center,
		Bottom,
		Top,
		Right,
		Left,
		Outside,
		Inside,
		BottomLeft,
		BottomRight,
		TopLeft,
		TopRight,
		BaselineLeft,
		Baseline,
		BaselineRight,
		Hide,
		Min,
		Max,
		Scale,
		Linear,
		Log10,
		Log,
		LogX,
		Reciprocal,
		OffsetReciprocal,
		LinkTo,
		Line,
		Dash,
		Offset,
		Color,
		Join, Round, Miter, Bevel,
		MiterLimit,
		Cap, Butt, Square,
		Ticks,
		Labels,
		Family,
		Weight,
		Slant,
		Underline,
		Capitalize,
		Alignment,
		ScriptOpt,
		Grids,
		Axis1,
		Axis2,
		Axis3,
		General,
		Option,
		Increment,
		Numbers,
		Minor,
		Font,
		Anchor,
		Shift,
		Transform,
		Text,
		Separator,
		Coordinates,
		Function,
		Image,
		Data,
		Data1,
		Data2,
		Data3,
		Gap,
		Symbols,
		BeginAngle,
		EndAngle,
		Rotation,
		Covering,
		Area,
		Bars,
		ErrorBars,
		Shape
	};
	static ustring toString( const Key::Keys& key );
	static Key::Keys toKeys( const ustring& keys );
	static UList<Key::Keys> toKeysList( const ustring& key );
	static void setKeysList( const ustring& keys, UList<Key::Keys>& key_list );
	static bool isElementType( const Key::Keys& key );
	//
private:
	UList<Key::Keys> _keys;
	ustring _fullkeys;
	//
public:
	Key( const ustring& keys );
	Key( const Key::Keys& key, const Key::Keys& key2 = Key::NoKey, const Key::Keys& key3 = Key::NoKey );
	Key( const UList<Key::Keys>& keys );
	Key( const UList<Key::Keys>& keys, const Key::Keys& subkey, const Key::Keys& subkey2 = Key::NoKey );
	//
	UList<Key::Keys> keys() const;
	ustring fullKeys() const;
	//
	bool isLineStyle( const Key::Keys& key, const Key::Keys& key2 = Key::NoKey ) const;
	bool isLineStyle( const UList<Key::Keys>& keys, bool include_cap = true, bool include_join = true ) const;
	bool isFillStyle( const Key::Keys& key, const Key::Keys& key2 = Key::NoKey ) const;
	bool isTextStyle( const Key::Keys& key = Key::NoKey, const Key::Keys& key2 = Key::NoKey ) const;
	// Operators
	bool operator<( const Key& key ) const;
	bool operator==( const Key& key ) const;
	bool operator!=( const Key& key ) const;
	bool operator==( const Key::Keys& key ) const;
	bool operator!=( const Key::Keys& key ) const;
	bool operator==( const UList<Key::Keys>& keys ) const;
	friend std::ostream& operator<<( std::ostream& os, const Key& key )
	{
		os << key.fullKeys();
		return os;
	}
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // KEY_H
