#ifndef UCAIRO_H
#define UCAIRO_H
/**********************************************/
#include <cairo/cairo.h>
#include <cairo/cairo-pdf.h>
#include <cairo/cairo-svg.h>
#include <cairo/cairo-ps.h>
/**********************************************/
#include "Styles.h"
/**********************************************/
#include <math.h>
/**********************************************/
/**********************************************/
/**********************************************/
class Cairo
{
private:
	Key _format = Key::PNG;
	cairo_surface_t* _surface = nullptr;
	cairo_t* _drawing = nullptr;
	ustring path = "/home/colas/Bureau";
	ustring filename = "graph";
	Size size = {100,100};
	Point _current_pos = {0,0};
	UList<Point> _saved_pos;
	UList<double> _saved_angles;
	double degree = M_PI/180.0;
public:
	double _current_angle = 0.0;
	~Cairo();
	Cairo( const Key& format = Key::PNG );
	Cairo( const ustring& format, const ustring& width, const ustring& height );
	void setPath( const ustring& path );
	void setFileName( const ustring& filename );
	void setFormat( const Key& format );
	void setSize( const Size& size );
	cairo_t* create();
	void init();
	cairo_surface_t* surface();
	cairo_t* drawing();
	void saveState();
	void restoreState();
	void beginGroup();
	void endGroup();
	void moveTo( double x, double y );
	void moveTo( const Point& point );
	void compose();
	void clip();
	void mask( cairo_surface_t* surface );
	void transform();
	void rotate( double angle );
	void scale( double scale );
	Point getTransformPoint( const Point& point );
	Line getTransformLine( const Line& line );
	Rect getTransformRect( const Rect& rect );
	void setLineStyle( const LineStyle& style = LineStyle() );
	void setFillStyle( const FillStyle& style = FillStyle() );
	void setTextStyle( const TextStyle& style = TextStyle() );
	void drawLine( const Line& line, const LineStyle& style = LineStyle() );
	void drawLine( const Point& p1, const Point& p2, const LineStyle& style = LineStyle() );
	void drawPath( const UList<Point>& points, const LineStyle& style = LineStyle() );
	void drawGappedPath( const UList<Point>& points, double gap, const LineStyle& style = LineStyle() );
	void drawClosedPath( const UList<Point>& points, const ShapeStyle& style = ShapeStyle() );
	void drawRect( const Rect& rect, const ShapeStyle& style = ShapeStyle() );
	void drawRect( const Point& pos, const Size& size, const ShapeStyle& style = ShapeStyle() );
	void drawRect( const Point& p1, const Point& p2, const ShapeStyle& style = ShapeStyle() );
	void drawRoundedRect( const Point& p1, const Point& p2, double radius, const ShapeStyle& style = ShapeStyle() );
	void drawArc( const Point& center, double radius, double angle_start, double angle_end, const ShapeStyle& style = ShapeStyle() );
	void drawArc( const Point& center, const Size& size, double angle_start, double angle_end, const ShapeStyle& style = ShapeStyle() );
	void drawCercle( const Point& center, double radius, const ShapeStyle& style = ShapeStyle() );
	void drawEllipse( const Point& center, double x_radius, double y_radius, const ShapeStyle& style = ShapeStyle() );
	void drawShape( const ShapeStyle::Shape& shape, const Size& size, const ShapeStyle& style = ShapeStyle() );
	Rect drawText( const UList<UList<StyledText>>& text, const Point& pos, const TextStyle& style );
	Rect textBoundingRect( const UList<UList<StyledText>>& text, const Point& pos, const TextStyle& style );
	Rect drawText( const ustring& text, double x, double y, const TextStyle& style = TextStyle() );
	Rect boundingRect( const ustring& text, const Point& textpos, const TextStyle& style );
	Rect drawText( const ustring& text, const Point& pos, const TextStyle& style );
	FontMetrics fontMetrics( const ustring& text, const TextStyle& style );
	Rect drawText( StyledText& styledtext );
	Rect drawSimpleText( const ustring& text, const Point& pos, const TextStyle& style = TextStyle() );
	void drawImage( const ustring& imagepath, const Point& pos );
	void drawSymbols( const Symbol& symbol, const Point& pos, const Size& size, const ShapeStyle& style = ShapeStyle() );
	void close();
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // UCAIRO_H
