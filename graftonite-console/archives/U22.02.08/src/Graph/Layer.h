#ifndef LAYER_H
#define LAYER_H
/**********************************************/
#include "Element.h"
/**********************************************/
/**********************************************/
/**********************************************/
class Layer
        : public Element
{
	UList<Element*> _children;
public:
	~Layer();
	Layer( Graph* parent = nullptr );
	Layer( const ustring& name, unsigned index, Graph* parent = nullptr, bool log = false );
	void init();
	/**********************************************/
	UList<Element*> children() const;
	void addChild( Element* child );
	void clear();
	/**********************************************/
	bool set( const Key& key, const ustring& value ) override;
	/**********************************************/
};
/**********************************************/
/**********************************************/
/**********************************************/
#endif // LAYER_H
