#include "Graph.h"
/**********************************************/
#include "../Commons/Cairo.h"
/**********************************************/
#include <iostream>
#include <fstream>
#include "../Commons/UConsole.h"
#include "../Commons/System.h"
//using namespace std;
/**********************************************/
/**********************************************/
Graph::~Graph()
{
	clearTemplates();
	clearLayers();
	clearFrames();
	clearAxes();
	clearData();
	clearPlots();
	clearLegends();
	clearTitles();
	clearShapes();
}
/**********************************************/
/**********************************************/
Graph::Graph()
	: Element()
{
	_type = Key::Graph;
	Element::set( Key::Name, "Graph");
	Element::set( Key::Width, "1600");
	Element::set( Key::Height, "1000");
	Element::set( Key::Format, "pdf");
	Element::set( Key::Path, System::HomePath() );
	Element::set( Key::FileName, "Graph" );
	Element::set( Key(Key::Borders, Key::Color), "black" );
	Element::set( Key(Key::Borders, Key::Width), "6" );
	Element::set( Key(Key::Borders, Key::Join), "bevel" );
	Element::set( Key(Key::Background, Key::Color), "white" );

	ustring log = "# Graftonite-Console - Version U";
	log += GRAFTONITE_VERSION;
	Graph::addLogLine( log+"\n" );


	Frame* frame = new Frame(this);
	frame->set( Key::Name, "frame1" );
	_frames.append( frame );
	frame->setIndex(0);
	//	Frame* layer1 = new Frame(this);
	//	layer1->set( "name", "This is a frame" );
	//	layer1->set( "width", "42" );
	//	layer1->set( "height", "1440" );
	//	_frames.append( layer1 );
	//	layer1->setIndex(1);
	Axis* axis = new Axis(this);
	axis->set( Key::Name, "AxisX" );
	axis->set( Key::Frame, "frame1" );
	axis->set( Key::Type, "X" );
	axis->setIndex(0);
	_axes.append( axis );
	Axis* axisy = new Axis(this);
	axisy->set( Key::Name, "AxisY" );
	axisy->set( Key::Frame, "frame1" );
	axisy->set( Key::Type, "Y" );
	axisy->setIndex(1);
	_axes.append( axisy );

	Data* data = new Data( "data1", 0, this );
	data->set( Key::Coordinates, "0,0/0.1,0.2/0.3,0.5/0.5,0.7/0.7,0.43/0.9,0.85/1.0,0.85" );
	_data.append( data );

	Plot* plot = new Plot( "plot1", 0, this );
	plot->set( Key::Data1, "data1:c0" );
	plot->set( Key::Data2, "data1:c1" );
	_plots.append( plot );

	Legend* legend = new Legend( "leg1", 0, this );
	legend->set( Key::Anchor, "top" );
	_legends.append( legend );


	Title* title = new Title( "title1", 0, this );
	title->set( Key::Position, "\\@lg:leg1;bottomright" );
	title->set( Key::Anchor, "topright" );
	title->set( Key::Text, "This is my first title !" );
	title->set( Key(Key::Font, Key::Size), 30 );
	title->set( Key(Key::Font, Key::Color), "red2" );
	title->set( Key(Key::Background, Key::Color), "blue8" );
	title->set( Key(Key::Margins), "25,25" );
	title->set( Key(Key::Rotation), 45 );
	_titles.append( title );

	Title* title2 = new Title( "title1", 0, this );
	title2->set( Key::Position, "\\@f:0;bottomright" );
	title2->set( Key::Anchor, "bottomright" );
	title2->set( Key::Text, "<style=family:fsdhfhjsd;size:24;color:0.15,0.48,0.75;>Ti<b>t\nle  p </style>p <style=>i.</style><style=italic>E</style> y <style=bold;smallcap>(MPa)</style> dgddf dsf dsf d<style=smallcap>f\n2- fdsh dksj</style>fhhf  \n3- fdfksghds a\\^2\\_cold;" );
	title2->set( Key(Key::Font, Key::Size), 30 );
	title2->set( Key(Key::Font, Key::Color), "red2" );
	title2->set( Key(Key::Alignment), "center" );
	title2->set( Key(Key::Rotation), 0 );
	_titles.append( title2 );
}
/**********************************************/
/**********************************************/
/* Element Management */
Element* Graph::element( const Key::Keys& type, const ustring& name )
{
	Element* el = nullptr;
	if ( type == Key::Graph )
		el = this;
	else if ( type == Key::Template )
		el = this->templateAt(name);
	else if ( type == Key::Layer )
		el = this->layer(name);
	else if ( type == Key::Frame )
		el = this->frame(name);
	else if ( type == Key::Axis )
		el = this->axis(name);
	else if (type == Key::Plot )
		el = this->plot(name);
	else if ( type == Key::Legend )
		el = this->legend(name);
	else if ( type == Key::Title )
		el = this->title(name);
	else if ( type == Key::Data )
		el = this->data(name);
	else
		el = _currentEl;
//	if ( el == nullptr )
//		Graph::addLogLine( "\n# "+Key::toString(type).toFirstUpper() + " \"" + name +"\" is not found !\n" );
	return el;
}
Element* Graph::element( const ustring& ref	)
{
	if ( ref.empty() )
		return nullptr;

	ustring newref = ref;
	if ( newref.at(0) == '{' )
	{
		newref = newref.substr(1);
	}
	if ( newref.substr(0,2) == "\\#" )
	{
		newref = newref.substr(2);
	}
	if ( newref.substr(0,2) == "\\@" )
	{
		newref = newref.substr(2);
	}
	if ( newref.at(newref.size()-1) == '}' )
	{
		newref = newref.substr(0,newref.size()-1);
	}
	if ( newref.contains(";") )
	{
		newref = newref.substr(0,newref.find(";"));
	}
	UList<ustring> tmp = newref.split(":");
	if ( tmp.size() == 2)
		return element( tmp );
	else
		return element( Key::Plot, newref );
}
Element* Graph::element( UList<ustring>& args )
{
	Element* el = nullptr;
	if ( !args.isEmpty() )
	{
		Key::Keys type = Element::getType( args.first() );
		if ( type == Key::Graph )
		{
			args.removeFirst();
			el = this;
		}
		else if ( type == Key::Current )
		{
			args.removeFirst();
			el = _currentEl;
		}
		else if ( type == Key::Template )
		{
			args.removeFirst();
			if ( !args.isEmpty() )
				el = this->templateAt(args.takeFirst());
		}
		else if ( type == Key::Layer )
		{
			args.removeFirst();
			if ( !args.isEmpty() )
				el = this->layer(args.takeFirst());
		}
		else if ( type == Key::Frame )
		{
			args.removeFirst();
			if ( !args.isEmpty() )
				el = this->frame(args.takeFirst());
		}
		else if ( type == Key::Axis )
		{
			args.removeFirst();
			if ( !args.isEmpty() )
				el = this->axis(args.takeFirst());
		}
		else if (type == Key::Plot )
		{
			args.removeFirst();
			if ( !args.isEmpty() )
				el = this->plot(args.takeFirst());
		}
		else if ( type == Key::Legend )
		{
			args.removeFirst();
			if ( !args.isEmpty() )
				el = this->legend(args.takeFirst());
		}
		else if ( type == Key::Title )
		{
			args.removeFirst();
			if ( !args.isEmpty() )
				el = this->title(args.takeFirst());
		}
		else if ( type == Key::Data )
		{
			args.removeFirst();
			if ( !args.isEmpty() )
				el = this->data(args.takeFirst());
		}
		else
			el = _currentEl;
	}
//	if ( el == nullptr )
//		Graph::addLogLine( "\n# Unable to find element !\n" );
	return el;
}
UList<Element*> Graph::elements( const Key::Keys& type )
{
	UList<Element*> els;
	if ( type == Key::Layer )
		for ( Layer* layer : this->layers() )
			els.append( layer );
	if ( type == Key::Frame || type == Key::NoKey )
		for ( Frame* frame : this->frames() )
			els.append( frame );
	if ( type == Key::Axis || type == Key::NoKey )
		for ( Axis* axis : this->axes() )
			els.append( axis );
	if (type == Key::Plot || type == Key::NoKey )
		for ( Plot* plot : this->plots() )
			els.append( plot );
	if ( type == Key::Legend || type == Key::NoKey )
		for ( Legend* legend : this->legends() )
			els.append( legend );
	if ( type == Key::Title || type == Key::NoKey )
		for ( Title* title : this->titles() )
			els.append( title );
	if ( type == Key::Data )
		for ( Data* data : this->datalst() )
			els.append( data );
	return els;
}
/**********************************************/
void Graph::newElement( const Key::Keys& type, const ustring& name, bool log )
{
	if ( type == Key::Graph )
	{
	}
	else if ( Key::isElementType(type) && !name.empty() )
	{
		if ( type == Key::Layer && (element(Key::Layer, name) == nullptr) )
			return _layers.append( new Layer(name, _layers.size(), this, log) );
		else if ( type == Key::Layer )
			return UConsole::error_message("Layer \""+name+"\" exists !");
		if ( type == Key::Frame && (element(Key::Frame, name) == nullptr) )
			return _frames.append( new Frame(name, _frames.size(), this, log) );
		else if ( type == Key::Frame )
			return UConsole::error_message("Frame \""+name+"\" exists !");
		if ( type == Key::Axis && (element(Key::Axis, name) == nullptr) )
			return _axes.append( new Axis(name, _axes.size(), this, log) );
		else if ( type == Key::Axis )
			return UConsole::error_message("Axis \""+name+"\" exists !");
		if ( type == Key::Plot && (element(Key::Plot, name) == nullptr) )
			return _plots.append( new Plot(name, _plots.size(), this, log) );
		else if ( type == Key::Plot )
			return UConsole::error_message("Plot \""+name+"\" exists !");
		if ( type == Key::Legend && (element(Key::Legend, name) == nullptr) )
			return _legends.append( new Legend(name, _legends.size(), this, log) );
		else if ( type == Key::Legend )
			return UConsole::error_message("Legend \""+name+"\" exists !");
		if ( type == Key::Title && (element(Key::Title, name) == nullptr) )
			return _titles.append( new Title(name, _titles.size(), this, log) );
		else if ( type == Key::Title )
			return UConsole::error_message("Title \""+name+"\" exists !");
		if ( type == Key::Data && (element(Key::Data, name) == nullptr) )
			return _data.append( new Data(name, _data.size(), this, log) );
		else if ( type == Key::Data )
			return UConsole::error_message("Data \""+name+"\" exists !");
	}
	else if ( name.empty() )
		UConsole::error_message("Name is empty !");
	else
		UConsole::error_message("Type is unknown !");
}
void Graph::removeElement( const Key::Keys& type, const ustring& name )
{
	if ( type == Key::Template )
	{
		if ( name.empty() )
		{
			if ( UConsole::confirmationDialog("Are you sure to delete all templates ? (y/n) : ") )
				clearTemplates();
		}
		else
			removeTemplate( templateAt(name) );
	}
	if ( type == Key::Layer || type == Key::NoType )
	{
		if ( name.empty() )
		{
			if ( UConsole::confirmationDialog("Are you sure to delete all layers ? (y/n) : ") )
				clearLayers();
		}
		else
			removeLayer( layer(name) );
	}
	if ( type == Key::Frame || type == Key::NoType )
	{
		if ( name.empty() )
		{
			if ( UConsole::confirmationDialog("Are you sure to delete all frames ? (y/n) : ") )
				clearFrames();
		}
		else
			removeFrame( frame(name) );
	}
	if ( type == Key::Axis || type == Key::NoType )
	{
		if ( name.empty() )
		{
			if ( UConsole::confirmationDialog("Are you sure to delete all axes ? (y/n) : ") )
				clearAxes();
		}
		else
			removeAxis( axis(name) );
	}
	if ( type == Key::Plot || type == Key::NoType )
	{
		if ( name.empty() )
		{
			if ( UConsole::confirmationDialog("Are you sure to delete all plots ? (y/n) : ") )
				clearPlots();
		}
		else
			removePlot( plot(name) );
	}
	if ( type == Key::Legend || type == Key::NoType )
	{
		if ( name.empty() )
		{
			if ( UConsole::confirmationDialog("Are you sure to delete all legends ? (y/n) : ") )
				clearLegends();
		}
		else
			removeLegend( legend(name) );
	}
	if ( type == Key::Title || type == Key::NoType )
	{
		if ( name.empty() )
		{
			if ( UConsole::confirmationDialog("Are you sure to delete all titles ? (y/n) : ") )
				clearTitles();
		}
		else
			removeTitle( title(name) );
	}
	if ( type == Key::Data || type == Key::NoType )
	{
		if ( name.empty() )
		{
			if ( UConsole::confirmationDialog("Are you sure to delete all data ? (y/n) : ") )
				clearData();
		}
		else
			removeData( data(name) );
	}
}
/**********************************************/
void Graph::setCurrent( UList<ustring>& args )
{
	_currentEl = element( args );
	if ( _currentEl == nullptr )
	{
		ustring message = "Unable to define a current element !";
		if ( _log )
			addLogLine( message );
		return UConsole::error_message( message );
	}
	ustring message = "Current element : " +
	                  Key::toString(_currentEl->type()) +
	                  " \"" + _currentEl->get(Key::Name) + "\"";
	if ( _log )
		addLogLine( message );
	std::cout << message << std::endl;
}
/**********************************************/
Element* Graph::templateAt( const ustring& name )
{
	for ( int it= 0; it < _templates.size(); ++it )
		if ( _templates.at(it)->get("name") == name || ustring(it) == name  )
			return _templates.at(it);
	Graph::addLogLine( "  Template \"" + name +"\" is not found !" );
	return nullptr;
}
UList<Element*> Graph::templates()
{
	return _templates;
}
void Graph::removeTemplate( Element* templat )
{
	_templates.remove(templat);
	delete templat;
}
void Graph::clearTemplates()
{
	while ( !_templates.isEmpty() )
		delete _templates.takeFirst();
}
/**********************************************/
Layer* Graph::layer( const ustring& name )
{
	for ( int it= 0; it < _layers.size(); ++it )
		if ( _layers.at(it)->get("name") == name || ustring(it) == name  )
			return _layers.at(it);
	Graph::addLogLine( "  Layer \"" + name +"\" is not found !" );
	return nullptr;
}
UList<Layer*> Graph::layers()
{
	return _layers;
}
void Graph::removeLayer( Layer* layer )
{
	_layers.remove(layer);
	delete layer;
}
void Graph::clearLayers()
{
	while ( !_layers.isEmpty() )
		delete _layers.takeFirst();
}
/**********************************************/
Frame* Graph::frame( const ustring& name )
{
	for ( int it= 0; it < _frames.size(); ++it )
		if ( _frames.at(it)->get("name") == name || ustring(it) == name  )
			return _frames.at(it);
	Graph::addLogLine( "  Frame \"" + name +"\" is not found !" );
	return nullptr;
}
UList<Frame*> Graph::frames()
{
	return _frames;
}
void Graph::removeFrame( Frame* frame )
{
	_frames.remove(frame);
	delete frame;
}
void Graph::clearFrames()
{
	while ( !_frames.isEmpty() )
		delete _frames.takeFirst();
}
/**********************************************/
Axis* Graph::axis( const ustring& name )
{
	for ( int it= 0; it < _axes.size(); ++it )
		if ( _axes.at(it)->get("name") == name || ustring(it) == name  )
			return _axes.at(it);
	Graph::addLogLine( "  Axis \"" + name +"\" is not found !" );
	return nullptr;
}
UList<Axis*> Graph::axes()
{
	return _axes;
}
void Graph::removeAxis( Axis* axis )
{
	_axes.remove(axis);
	delete axis;
}
void Graph::clearAxes()
{
	while ( !_axes.isEmpty() )
		delete _axes.takeFirst();
}
/**********************************************/
Data* Graph::data( const ustring& name )
{
	for ( int it = 0; it < _data.size(); ++it )
		if ( _data.at(it)->get(Key::Name) == name || ustring(it) == name  )
			return _data.at(it);
	Graph::addLogLine( "  Data \"" + name +"\" is not found !" );
	return nullptr;

}
UList<Data*> Graph::datalst()
{
	return _data;
}
void Graph::removeData( Data* data )
{
	_data.remove(data);
	delete data;
}
void Graph::clearData()
{
	while ( !_data.isEmpty() )
		delete _data.takeFirst();
}
/**********************************************/
Plot* Graph::plot( const ustring& name )
{
	for ( int it= 0; it < _plots.size(); ++it )
		if ( _plots.at(it)->get("name") == name || ustring(it) == name  )
			return _plots.at(it);
	Graph::addLogLine( "  Plot \"" + name +"\" is not found !" );
	return nullptr;
}
UList<Plot*> Graph::plots()
{
	return _plots;
}
void Graph::removePlot( Plot* plot )
{
	_plots.remove(plot);
	delete plot;
}
void Graph::clearPlots()
{
	while ( !_plots.isEmpty() )
		delete _plots.takeFirst();
}
/**********************************************/
Legend* Graph::legend( const ustring& name )
{
	for ( int it= 0; it < _legends.size(); ++it )
		if ( _legends.at(it)->get("name") == name || ustring(it) == name  )
			return _legends.at(it);
	Graph::addLogLine( "  Legend \"" + name +"\" is not found !" );
	return nullptr;
}
UList<Legend*> Graph::legends()
{
	return _legends;
}
void Graph::removeLegend( Legend* legend )
{
	_legends.remove(legend);
	delete legend;
}
void Graph::clearLegends()
{
	while ( !_legends.isEmpty() )
		delete _legends.takeFirst();
}
/**********************************************/
Title* Graph::title( const ustring& name )
{
	for ( int it= 0; it < _titles.size(); ++it )
		if ( _titles.at(it)->get(Key::Name) == name || ustring(it) == name  )
			return _titles.at(it);
	Graph::addLogLine( "  Title \"" + name +"\" is not found !" );
	return 0;
}
UList<Title*> Graph::titles()
{
	return _titles;
}
void Graph::removeTitle(Title* title)
{
	_titles.remove(title);
	delete title;
}
void Graph::clearTitles()
{
	while ( !_titles.isEmpty() )
		delete _titles.takeFirst();
}
/**********************************************/
Shape* Graph::shape( const ustring& name )
{
	for ( int it = 0; it < _data.size(); ++it )
		if ( _shapes.at(it)->get(Key::Name) == name || ustring(it) == name  )
			return _shapes.at(it);
	Graph::addLogLine( "  Shape \"" + name +"\" is not found !" );
	return nullptr;

}
UList<Shape*> Graph::shapes()
{
	return _shapes;
}
void Graph::removeShape( Shape* data )
{
	_shapes.remove(data);
	delete data;
}
void Graph::clearShapes()
{
	while ( !_shapes.isEmpty() )
		delete _shapes.takeFirst();
}
/**********************************************/
/**********************************************/
/* Modifiers */
bool Graph::set( const Key& key, const ustring& value )
{
	if ( key == Key::Name ||
		 key == Key::Format ||
		 key == Key::Path ||
		 key == Key::FileName ||
		 key == Key::Template ||
		 key == Key::Width ||
		 key == Key::Height ||
		 key.isLineStyle({Key::Borders}, false) ||
	     key.isFillStyle(Key::Background) ||
	     key == Key(Key::Working, Key::Path)
		 )
	{

		return Element::set( key, value );
	}
	else if ( key == Key::Size )
	{
		if ( !value.contains("x") ) return false;
		UList<ustring> list = value.split("x");
		set( Key::Width, list.at(0) );
		set( Key::Height, list.at(1) );
	}
	else if ( key == Key::Background )
	{
	}
	else if ( key == Key::Borders )
	{
	}
	else
		Graph::warningLog("Unknown key \""+key.fullKeys()+"\" for graph element");
	return false;
}
bool Graph::set( UList<ustring>& args, bool log )
{
	Element* el = element( args );
	if ( el == nullptr )
		Graph::errorLog("Unable to find element !");
	else if ( el == this )
		return Element::set(args, log);
	else
		return el->set(args, log);
	return false;
}
bool Graph::reset( UList<ustring>& args, bool log )
{
	Element* el = element( args );
	if ( el == nullptr )
		Graph::errorLog("Unable to find element !");
	else if ( el == this )
	{
		clearLayers();
		clearFrames();
		clearAxes();
		clearPlots();
		clearLegends();
		clearTitles();
		clearData();
		this->_currentEl = nullptr;
		return true;
	}
	else
		return el->reset( args, log );
	return false;
}
void Graph::unset( const UList<ustring>& args, bool log )
{
	if ( args.isEmpty() )
		return;
	UList<ustring> args2 = args;
	Key::Keys type = Element::getType( args2.takeFirst() );

	Element* el = 0;
	if ( type == Key::NoType )
	{
		el = _currentEl;
	}
	else if ( type == Key::Graph )
	{
		Element::unset(args2, log);
	}
	else if ( !args2.isEmpty() )
	{
		ustring name = args2.takeFirst();
		el = element( type, name );
	}

	if ( el )
		el->unset(args2, log);
}
/**********************************************/
/**********************************************/
/* Displays */
void Graph::showAll( const Key::Keys& type, const ustring& name )
{
	//	std::cout << std::endl;
	if ( type == Key::Graph )
	{
		this->show();
	}
	else if ( type == Key::NoKey )
	{
		UConsole::println(  "GRAPH : " + get(Key::Name), 2 );

		UConsole::print2ln( "LAYERS", 8, 4, " ", 10, "-" );
		if ( layers().isEmpty() )
			UConsole::println( "NO LAYER", 6 );
		for ( Layer* layer : layers() )
			layer->shortShow();

		UConsole::print2ln( "FRAMES", 8, 4, " ", 10, "-" );
		if ( _frames.isEmpty() )
			UConsole::println( "NO FRAME", 6 );
		for ( Frame* frame : frames() )
			frame->shortShow();

		UConsole::print2ln( "AXES", 8, 4, " ", 10, "-" );
		if ( axes().isEmpty() )
			UConsole::println( "NO AXIS", 6 );
		for ( Axis* axis : axes() )
			axis->shortShow();

		UConsole::print2ln( "PLOTS", 8, 4, " ", 10, "-" );
		if ( plots().isEmpty() )
			UConsole::println( "NO PLOT", 6 );
		for ( Plot* plot : plots() )
			plot->shortShow();

		UConsole::print2ln( "LEGENDS", 8, 4, " ", 10, "-" );
		if ( legends().isEmpty() )
			UConsole::println( "NO LEGEND", 6 );
		for ( Legend* legend : legends() )
			legend->shortShow();

		UConsole::print2ln( "TITLES", 8, 4, " ", 10, "-" );
		if ( titles().isEmpty() )
			UConsole::println( "NO TITLE", 6 );
		for ( Title* title : titles() )
			title->shortShow();

		UConsole::print2ln( "DATA", 8, 4, " ", 10, "-" );
		if ( datalst().isEmpty() )
			UConsole::println( "NO DATA", 6 );
		for ( Data* data : datalst() )
			data->shortShow();
	}
	else
	{
		if ( name.empty() )
		{
			for ( Element* el : elements(type) )
				el->show();
		}
		else
		{
			Element* element = this->element( type, name );
			if ( element )
				element->show();
		}
	}
}
void Graph::showCurrent()
{
	if ( _currentEl )
		showAll( _currentEl->type(), _currentEl->get(Key::Name) );
}
/**********************************************/
/**********************************************/
/* Drawing functions */
void Graph::prepare( Cairo* drawing )
{
	//
	Element::getTemplate();
	//
	for ( Data* data : _data )
		data->initData();
	//
	UList<Element*> _parent_pos_refs;
	for ( Element* el : this->elements(Key::NoKey) )
	{
		el->initParentPosRef();
		if ( el->parentPosRef() == nullptr &&
			 !_parent_pos_refs.contains(el) )
			_parent_pos_refs.append(el);
	}
	for ( Element* el : _parent_pos_refs )
	{
		el->prepare( drawing );
		prepare( el, drawing );
	}
}
void Graph::prepare( Element* el, Cairo* drawing )
{
	for ( Element* c_el : el->childrenPosRef() )
	{
		c_el->prepare(drawing);
		prepare( c_el, drawing );
	}
}
void Graph::draw( Cairo* drawing )
{
	double width = stod( get(Key::Width) );
	double height = stod( get(Key::Height) );

	FillStyle fill_style = getFillStyle( {Key::Background} );
	LineStyle line_style = getLineStyle( {Key::Borders} );
	double bd_width = line_style.width();
	ShapeStyle style;
	style.setFill( fill_style );
	style.setLine( line_style );

	drawing->drawRect( {0.5*bd_width,0.5*bd_width}, Size({width-bd_width, height-bd_width}), style );

	for ( Frame* frame : _frames )
		frame->draw( drawing );
	for ( Axis* axis : _axes )
		axis->draw( drawing );
	for ( Plot* plot : _plots )
		plot->draw( drawing );
	for ( Legend* legend : _legends )
		legend->draw( drawing );
	for ( Title* title : _titles )
		title->draw( drawing );

	// TEST
//	ustring formattedtext = "<style=family:fsdhfhjsd;size:24;color:0.15,0.48,0.75;>Ti<b>t\nle  p </style>p <style=>i.</style><style=italic>E</style> y <style=bold;smallcap>(MPa)</style> dgddf dsf dsf d<style=smallcap>f\n2- fdsh dksj</style>fhhf  \n3- fdfksghds a\\^2\\_cold;";
//	TextStyle text_style;
//	text_style.setSize( 28 );
//	text_style.setAnchor(Key::Top);
//	text_style.setAlign(TextStyle::Center);
//	text_style.setRotation(45);

//	UList<UList<StyledText>> list;
//	StyledText::getStyledTextList( formattedtext, text_style, list );


//	UList<UList<StyledText>> text;
	TextStyle tx_sty;
	tx_sty.setSize( 28 );
	tx_sty.setAnchor(Key::Top);
	tx_sty.setAlign(TextStyle::Center);
	tx_sty.setRotation(45);
//	StyledText::buildStyledTextList( formattedtext, tx_sty, text );

	Point pos = {650, 550};
//	Rect rec = drawing->drawText( list, pos, text_style );
	pos = {850, 550};
//	rec = drawing->drawText( text, pos, tx_sty );
	LineStyle styl;
	styl.setColor( Color("red6") );
	styl.setWidth( 1 );
	ShapeStyle sty;
	sty.setLine( styl );
//	drawing->drawRect( rec, sty );
//	drawing->drawCercle( pos, 2 );

	pos = {250, 250};
	tx_sty.setRotation(0);
	Rect rec2 = drawing->drawText( "p", pos, tx_sty );
	tx_sty.setSize( 14 );
	tx_sty.setAnchor(Key::TopLeft);
	tx_sty.setCap( TextStyle::SmallCap );
	drawing->drawText( "2p", rec2.topRight(), tx_sty );
	tx_sty.setAnchor(Key::BottomLeft);
	tx_sty.setSlant( "italic" );
	drawing->drawText( "cTld", rec2.bottomRight(), tx_sty );
	// END TEST

}
void Graph::exportGraph( UList<ustring>& args )
{
	Graph::addLogLine("# Export Step");
	ustring path;
	ustring filename;
	ustring format;
	if ( args.isEmpty() )
	{
		path = get(Key::Path);
		if ( path.empty() )
			path = "/home/colas/Bureau";
		filename = get(Key::FileName);
		if ( filename.empty() )
			filename = "graph";
		format = get(Key::Format);
		if ( format.empty() )
			format = "png";
	}
	else
	{
		bool wrongFilepath = false;
		ustring fullpath = args.first();
		unsigned long s_ind = fullpath.find_last_of( "/" );
		if ( s_ind != std::string::npos )
			path = fullpath.substr( 0, s_ind );
		else
			wrongFilepath = true;
		unsigned long p_ind = fullpath.find_last_of( "." );
		if ( p_ind != std::string::npos )
		{
			filename = fullpath.substr( s_ind+1, p_ind - s_ind-1 );
			format = fullpath.substr( p_ind+1 );
		}
		else
			wrongFilepath = true;
		if ( wrongFilepath )
			return Graph::errorLog("Error detected in filepath !");
	}

	Cairo* cairo = new Cairo( format,
						 get(Key::Width),
						 get(Key::Height) );

	cairo->setPath( path );
	cairo->setFileName( filename );
	_exportpath = path + "/" + filename + "." + format;
	cairo->init();


	this->prepare( cairo );
	this->draw( cairo );

	cairo->close();

	delete cairo;
	cairo = nullptr;


	ustring log = "  Your graph was successfully export at :\n"
	              "    \"" +path+ "/" + filename + "." + format + "\"";
	Graph::normalLog(log);

//	if ( _log )
	Graph::saveLog( path + "/" + filename + ".log" );
	Graph::clearLog();
}
/**********************************************/
/**********************************************/
/* Log functions */
void Graph::addLogLine( const ustring& line )
{
	_log_contents.append( line );
}
void Graph::clearLog()
{
	_log_contents.clear();
}
void Graph::saveLog( const ustring& path )
{
	File::write( path, _log_contents );
}
void Graph::normalLog( const ustring& message )
{
	Graph::addLogLine(message);
	std::cout << message << std::endl;
}
void Graph::errorLog( const ustring& message )
{
	Graph::addLogLine("ERROR: "+message);
	UConsole::error_message(message);
}
void Graph::warningLog( const ustring& message )
{
	Graph::addLogLine("WARNING: "+message);
	UConsole::warning_message(message);
}
/**********************************************/
/**********************************************/
