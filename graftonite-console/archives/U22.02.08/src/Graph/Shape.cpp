#include "Shape.h"
/**********************************************/
#include "Graph.h"
/**********************************************/
/**********************************************/
Shape::~Shape()
{

}
Shape::Shape( Graph* parent )
    : Element(parent)
{

}
Shape::Shape( const ustring& name, unsigned index, Graph* parent, bool log )
    : Element(parent)
{
	init();
	Element::init( name, index, log );
}
void Shape::init()
{
	_type = Key::Shape;
}
/**********************************************/
/**********************************************/
bool Shape::set( const Key& key, const ustring& value )
{
	if ( key == Key::Name ||
	     key == Key::Type )
		return Element::set( key, value );
	else
		Graph::warningLog( "Unknown key \""+key.fullKeys()+"\" for shpae element !" );
	return false;
}
/**********************************************/
/**********************************************/
void Shape::prepare( Cairo* drawing )
{

}
/**********************************************/
/**********************************************/
void Shape::draw( Cairo* drawing )
{

}
/**********************************************/
/**********************************************/
