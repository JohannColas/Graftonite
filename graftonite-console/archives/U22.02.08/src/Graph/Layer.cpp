#include "Layer.h"
/**********************************************/
#include "Graph.h"
/**********************************************/
/**********************************************/
Layer::~Layer()
{

}
/**********************************************/
/**********************************************/
Layer::Layer( Graph* parent )
	: Element(parent)
{
	init();
}
Layer::Layer( const ustring& name, unsigned index, Graph* parent, bool log )
	: Element(parent)
{
	init();
	Element::init( name, index, log );
}
void Layer::init()
{
	_type = Key::Layer;
}
/**********************************************/
/**********************************************/
UList<Element*> Layer::children() const
{
	return _children;
}
void Layer::addChild(Element* child)
{
	_children.append( child );
}
void Layer::clear()
{
	_children.clear();
}
/**********************************************/
/**********************************************/
bool Layer::set( const Key& key, const ustring& value )
{
	if ( key == Key::Name )
		return Element::set( key, value );

	else
		Graph::warningLog( "Unknown key \""+key.fullKeys()+"\" for layer element !" );
	return false;
}
/**********************************************/
/**********************************************/
